<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class komisikhusus extends authcontroller {

	//put your code here
	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		//semua meanggunakan model komisi_model
		$this->load->model('transaksi/keuangan/komisi_model');

		//model untuk komisi khusus
		$this->load->model('transaksi/keuangan/komisikhusus_model');
		
		//ini untuk ambil kas bank aja
		$this->load->model('transaksi/keuangan/paymentmodel');
	}

	function index()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");
		if($sign == "Y"){
			$segs = $this->uri->segment_array();
			$arr  = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
			$data['link'] = $mylib->restrictLink($arr);
			$id   = addslashes(trim($this->input->post('stSearchingKey')));
			$id2  = $this->input->post('date1');
			$with = $this->input->post('searchby');
			if($with == "TglTransaksi"){
				$id = $mylib->ubah_tanggal($id2);
			}
			$this->load->library('pagination');

			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = '<span class="current">';
			$config['cur_tag_close'] = '</span>';
			$config['per_page'] = '10';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['num_links'] = 2;
			$config['base_url'] = base_url() . 'index.php/keuangan/komisikhusus/index/';
			$page = $this->uri->segment(4);
			$config['uri_segment'] = 4;
			$flag1 = "";
			if($with != ""){
				if($id != "" && $with != ""){
					$config['base_url'] = base_url() . 'index.php/keuangan/komisikhusus/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
				else
				{
					$page = "";
				}
			}
			else
			{
				if($this->uri->segment(5) != ""){
					$with = $this->uri->segment(4);
					$id   = $this->uri->segment(5);
					if($with == "TglTransaksi"){
						$id = $mylib->ubah_tanggal($id);
					}
					$config['base_url'] = base_url() . 'index.php/keuangan/komisikhusus/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
			}
			
			$user = $this->session->userdata('username');
			$data['kasbank']   = $this->paymentmodel->getKasBank($user);
			
			$data['header'] = array("No Transaksi","Tanggal","Nama","Keterangan","Total","Kd Agent");
			$config['total_rows'] = $this->komisikhusus_model->num_komisi_row(addslashes($id), $with);
			$data['data'] = $this->komisikhusus_model->getKomisiList($config['per_page'], $page, addslashes($id), $with);
			$data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
			$this->load->view('transaksi/keuangan/komisikhusus/komisi_list', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function add_new()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("add");
		if($sign == "Y"){
			$aplikasi = $this->komisikhusus_model->getDate();
			//$data['mperusahaan'] = $this->retur_barangmodel->getPerusahaan();
			//$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			//$user = $this->session->userdata('username');
			$user = $this->session->userdata('username');
			$data['kasbank']   = $this->paymentmodel->getKasBank($user);
			$data['aplikasi'] = $aplikasi;
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/komisikhusus/add_komisi', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function save_new_komisi()
	{
		$mylib       = new globallib();
		//$mylib->arrprint($_POST);
		//echo "<pre>";print_r($_POST);echo "</pre>";die();
		$user        = $this->session->userdata('userid');
		$flag        = $this->input->post('flag');
		$no          = $this->input->post('nodok');
		$tgl         = $this->input->post('tgl');
		$nama        = $this->input->post('namak');
		$kdagent     = $this->input->post('kdagent');
		$ket         = trim(strtoupper(addslashes($this->input->post('ket'))));
		$total       = $this->input->post('total');
		$totaljual   = $this->input->post('totjual');
		$nostruk1    = $this->input->post('nostruk');
		$tgljual1    = $this->input->post('tgljual');
		$pcode1      = $this->input->post('pcode');
		$potongan1   = $this->input->post('potongan');
		$harga1      = $this->input->post('harga');
		$qty1        = $this->input->post('qty');
		$persentase1 = $this->input->post('persentase');
		$pcodesave1  = $this->input->post('savepcode');
		$komisia1  = $this->input->post('komisia');
		// $komisib1  = $this->input->post('komisib');
		// $komisic1  = $this->input->post('komisic');
		// $komisid1  = $this->input->post('komisid');
		$hrgPPN1  = $this->input->post('hrgPPN');
		$nosticker = $this->input->post('nosticker');
		
		
		$notrans1    = $this->input->post('notrans');
		$tglticket1    = $this->input->post('tgl_berlaku');
		$pcodeticket1  = $this->input->post('pcodeticket');
		$potongans1   = $this->input->post('potongans');
		$hargaticket1      = $this->input->post('hargaticket');
		$qtyticket1        = $this->input->post('qtyticket');
		$persentaseticket1 = $this->input->post('persentaseticket');
		$pcodesaveticket1  = 0;
		$komisiticketa1  = 0;
		$komisiticketb1  = 0;
		$komisiticketc1  = 0;
		$komisiticketd1  = 0;
		$hrgPPNticket1  = 0;
		
				
		$waktu       = date("h:i:s");//$this->input->post('waktu');
		$bulan = substr($tgl, 3, 2);
		$tahun = substr($tgl, - 4);
		
		//ini adalah untuk insert di finance_komisi_header dan finance_komisi_detail ------------------------------------------------------------
		if($no == ""){
			$new_no= $this->komisikhusus_model->getNewNo($tahun,$bulan);
			$no    = $new_no->NoKomisi;
			$this->db->update('counter', array("NoKomisi"=> $new_no->NoKomisi + 1), array("Tahun"=> $tahun,"Bulan"=> $bulan));
						
			//insert new detail komisi
			$ttl = 0;
			$ttj = 0;
			for($x = 0; $x < count($pcode1); $x++){
				$pcode      = strtoupper(addslashes(trim($pcode1[$x])));
				$nostruk    = trim($nostruk1[$x]);
				$tgljual    = trim($tgljual1[$x]);
				$harga      = trim($harga1[$x]);
				$qty        = trim($qty1[$x]);
				$komisia    = trim($komisia1[$x]);
				// $komisib    = trim($komisib1[$x]);
				// $komisic    = trim($komisic1[$x]);
				// $komisid    = trim($komisid1[$x]);
				$hrgPPN     = trim($hrgPPN1[$x]);
				$potongan   = trim($potongan1[$x]);
				
				$persentase = trim($persentase1[$x]);
				$pcodesave  = $pcodesave1[$x];
				if($pcode != ""){
					$this->insertkomisidetail($no, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user,$komisia,$hrgPPN,$potongan);
					$ttl += ($qty * $harga - $potongan)*($persentase/100);
					$ttj += ($qty * $harga - $potongan);
				}				
			}
			
			$ttl_ticket = 0;
			$ttj_ticket = 0;
			for($y = 0; $y < count($pcodeticket1); $y++){
				$pcodeticket      = strtoupper(addslashes(trim($pcodeticket1[$y])));
				$notrans    = trim($notrans1[$y]);
				$tglticket    = trim($tglticket1[$y]);
				$hargaticket      = trim($hargaticket1[$y]);
				$qtyticket        = trim($qtyticket1[$y]);
				$komisiticketa    = trim($komisiticketa1[$y]);
				$komisiticketb    = trim($komisiticketb1[$y]);
				$komisiticketc    = trim($komisiticketc1[$y]);
				$komisiticketd    = trim($komisiticketd1[$y]);
				$hrgPPNticket     = trim($hrgPPNticket1[$y]);
				$potongans   = trim($potongans1[$y]);  
				
				$persentaseticket = trim($persentaseticket1[$y]);
				$pcodesaveticket  = $pcodesaveticket1[$y];
				if($pcodeticket != ""){
					$this->insertkomisidetail($no, $notrans, $tglticket, $pcodeticket, $qtyticket, $persentaseticket, $hargaticket, $user,$komisiticketa,$komisiticketb,$komisiticketc,$komisiticketd,$hrgPPNticket,$potongans);
					$ttl_ticket += ($qtyticket * $hargaticket)*($persentaseticket/100);
					$ttj_ticket += ($qtyticket * $hargaticket);
					
					$this->db->update('ticket', array('statuskomisi_khusus'=>'1'), array('notrans'=> $notrans));
					
				}				
			}
			
			$ttls = $ttl;
			$ttjs = $ttj;
			//insert new header komisi
			$this->insertkomisiheader($flag, $nama, $tgl, $kdagent, $ket, $ttls, $user,$waktu,$ttjs, $nosticker, $no);
		}
		else
		{
			$this->updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user);
			for($x = 0; $x < count($pcode1); $x++){
				$pcode     = strtoupper(addslashes(trim($pcode1[$x])));
				$ketdet    = trim($ketdet1[$x]);
				$qty       = trim($qty1[$x]);
				$pcodesave = $pcodesave1[$x];
				//if ($pcode != "") {
				$this->updateDetail($flag, $no, $pcode, $ketdet, $qty, $tgl, $user);
				//}
			}
		}

		redirect('/keuangan/komisikhusus/');
	}
	
	
	function insertpaymentdetail($flag,$NoDokumen,$tgl,$NoBukti,$jumlah,$counter,$keterangan,$user,$savekdrekening,$subdivisi)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_detail');
		$data = array(
			'NoDokumen'	=> $NoDokumen,
			'TglDokumen' => $tgl,
			'KdRekening' => $savekdrekening,//komisi marketing
			'Jumlah' => $jumlah,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan,
			'Urutan'	=> $counter,
			'NoBukti' => $NoBukti,
			'Status' => '1',
			'AddDate' => $tgl1,
			'AddUser' => $user
		);
		$this->db->insert('trans_payment_detail', $data);
		
		$this->paymentmodel->unlocktables();
	}
	
	//untuk mendapatkan NoDokumen
	//( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun2,$bulan2);
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 6, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

	// fungsi insertNewHeader yang ada diatas
	function insertkomisiheader($flag, $nama, $tgl, $kdagent, $ket, $total, $user,$waktu,$totaljual, $nosticker, $no)
	{
		//$this->komisi_model->locktables('keuangan_pvheader');
		$mylib = new globallib();
		
		$data = array(
			'NoTransaksi' => $no,
			'TglTransaksi'=> $mylib->ubah_tanggal($tgl),
			'KdAgent'     => $kdagent,
			'Nama'        => $nama,
			'Keterangan'  => $ket,
			'Total'       => $total,
			'AddDate'     => date("Y-m-d"),
			'AddUser'     => $user,
			'Waktu'       => $waktu,
			'TotalSales'  => $totaljual,
			'NoSticker' => $nosticker,
			'Kategori' => '1'
		);
		
		//sejak 16-03-2017 dibawah ini ga ada di pindah pake modal terpisah
		    /*'KdKasBank'   => $KdKasBank,
			'Penerima'    => $Penerima,
			'NoBukti'     => $NoBukti,
			'NoDokumen'   => $NoDokumen*/
		
		$this->db->insert('finance_komisi_khusus_header', $data);
		$this->db->update('register', array('statuskomisi_khusus'=>'1'),array('KdTourLeader'=> $kdagent));
		//$this->komisi_model->unlocktables();
		return $no;
	
	}
	
	
	function insertpaymentheader($no,$tgl,$NoBukti,$ket,$user,$totkomisi,$Penerima,$KdKasBank, $NoDokumen)
	{
		$this->paymentmodel->locktables('counter,trans_payment_header');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		
		//die;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	    => $NoDokumen,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'JumlahPayment' => $totkomisi,
			'Status' => '1'
		);
		$this->db->insert('trans_payment_header', $data);
		$this->db->update('finance_komisi_khusus_header',$data=array('NoDokumen'=>$NoDokumen),$data=array('NoTransaksi'=>$no));
		$this->paymentmodel->unlocktables();
		return $no;
	}
	
	

	function updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user)
	{
		// $tgl = $this->session->userdata('Tanggal_Trans');
		// $this->komisi_model->locktables('keuangan_pvheader,keuangan_pvdetail');
		$data = array(
			'NoTransaksi'=> $no,
			'NoBukti'    => $nobukti,
			'Keterangan' => $ket,
			'Total'      => $total,
			'EditDate'   => date("Y-m-d"),
			'EditUser'   => $user
		);
		if($flag == "edit"){
			$data['EditDate'] = date("Y-m-d");
			$data['EditUser'] = $user;
			$this->db->update('finance_komisi_khusus_detail', array('EditDate'=> date("Y-m-d"),'EditUser'=> $user), array('NoTransaksi'=> $no));
		}
		$this->db->update('finance_komisi_khusus_header', $data, array('NoTransaksi'=> $no));
		$this->komisikhusus_model->unlocktables();
	}
	
	
	
	//ini yang insertNewDetail yang diatas
	function insertkomisidetail($no, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user,$komisia,$hrgPPN,$potongan)
	{
		$data = array(
			'NoTransaksi'=> $no,
			'PCode'      => $pcode,
			'NoStruk'    => $nostruk,
			'TglJual'    => $tgljual,
			'Qty'        => $qty,
			'Harga'      => $harga,
			'Persentase' => $persentase,
			//'Komisi4bag' => $persentase,
			'Potongan' => $potongan,
			'Komisi1' => $komisia,
			//'Komisi2' => $komisib,
			//'Komisi3' => $komisic,
			//'Komisi4' => $komisid,
			'hargaPPN' => $hrgPPN,
			'AddDate'    => date("Y-m-d"),
			'AddUser'    => $user
		);
	
		$this->db->insert('finance_komisi_khusus_detail', $data);
		$this->db->update('transaksi_header', array('statuskomisi_khusus'=>'1'), array('NoStruk'=> $nostruk));
		//$this->komisi_model->unlocktables();
		return $no;
	}

	function getlistBarang1()
	{
		$this->load->model('proses/tutup_pos_model');
		$field   = $this->input->post('kdagent');
		$nosticker  = $this->input->post('nosticker');
		$tgl     = $this->tutup_pos_model->getLastDate();
		$tgl2    = $tgl->TglTrans;
		//ambil data register yang blm diambil
		
		$req = $this->komisikhusus_model->ambilReq($field,$tgl2,$nosticker);// ambil tanggal transaksi dan nomor stiker
		$valagen = $this->komisikhusus_model->ValAgent($field);
		$cekagen = $this->komisikhusus_model->CekAgent($field, $nosticker);//c group harga ada / tidak
		
		$tglagen ="";
		$tanggal ="( a.Tanggal BETWEEN DATE_SUB('".$tgl2."', INTERVAL 30 DAY) and '".$v['Tanggal']."' AND a.Tanggal >='2018-08-10')";
		if(!empty($req)){
			$bb = 1;
			foreach ($req as $k => $v){
				$tglagen .= "( a.Tanggal = '".$v['Tanggal']."'and a.KdAgent = '".$v['NoStiker']."')";
				// $tanggal .= "( a.Tanggal BETWEEN DATE_SUB('".$v['Tanggal']."', INTERVAL 30 DAY) and '".$v['Tanggal']."' AND a.Tanggal >='2018-08-10')";
				if($bb != count($req)){
					$tglagen .= "or";
					// $tanggal .= "or";
				}
				$bb ++;
			}
		}
		//print_r($valagen);	
		if(!empty($valagen)){
			if(empty($nosticker)){
				$detail = $this->komisikhusus_model->getKomisiOH30($field,$tanggal);
			}else{
				$detail = $this->komisikhusus_model->getKomisiOH($field,$tglagen);
			}
			
		}else{
			$detail = NULL;
			
			
		}
		
		// if(!empty($cekagen)){
		// 	//echo "masuk";die;
		// 	$kdtravel = $cekagen[0]['KdTravel'];
		// 	$detail= $this->komisikhusus_model->getKomisiTravel($kdtravel,$tglagen);
		// 	$data['ticketing']= $this->komisihusus_model->getKomisiTicketAgent($kdtravel,$tanggal);
		// }else{
			
		// 	$detail= $this->komisikhusus_model->getKomisi($field,$tglagen);
		// 	$data['ticketing']= $this->komisi_model->getKomisiTicket($field,$tanggal,$nosticker);
		// }
		
		$data['data'] = $detail;
		
		
		
		$this->load->view('transaksi/keuangan/komisikhusus/add_komisi_detail', $data);	
	}


function getlistBarang()
	{
		$this->load->model('proses/tutup_pos_model');
		$field   = $this->input->post('kdagent');
		$nosticker  = $this->input->post('nosticker');
		$tgl     = $this->tutup_pos_model->getLastDate();
		$tgl2    = $tgl->TglTrans;
		//ambil data register yang blm diambil
		
		$req = $this->komisikhusus_model->ambilReq($field,$tgl2,$nosticker);// ambil tanggal transaksi dan nomor stiker
		$valagen = $this->komisikhusus_model->ValAgent($field);
		$cekagen = $this->komisikhusus_model->CekAgent($field, $nosticker);//c group harga ada / tidak
		

		// print_r($req);
		if(!$req){
			echo '<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Terjadi Kesalahan!!!</h4>
				Kode Agen Salah / Status Agen Sudah Mengambil Komisi.
			</div>';

			die();
		}
		$tglagen ="";
		$tanggal ="";
		$tanggal2 ="( a.Tanggal BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) and NOW() AND a.Tanggal >= '2018-08-10')";
		if(!empty($req)){
			$bb = 1;
			foreach ($req as $k => $v){
				$tglagen .= "( a.Tanggal = '".$v['Tanggal']."' and a.KdAgent = '".$v['NoStiker']."')";
				// $tanggal .= "( a.Tanggal BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) and NOW() AND a.Tanggal >= '2018-08-10')";
				$tanggal .= "( a.Tanggal = '".$v['Tanggal']."'and a.NoStiker = '".$v['NoStiker']."')";
				if($bb != count($req)){
					$tglagen .= "or";
					$tanggal .= "or";
				}
				$bb ++;
			}
		}
		//print_r($valagen);	
		if(!empty($valagen)){
			// if(empty($nosticker)){
			// 	$detail = $this->komisikhusus_model->getKomisiOH30($field,$tanggal2);
			// }else{
			// 	$detail = $this->komisikhusus_model->getKomisiOH($field,$tglagen);
			// }

			$detail = $this->komisikhusus_model->getKomisiOH($field,$tglagen);
			$data['ticketing']= $this->komisikhusus_model->getKomisiTicket($field,$tanggal,$nosticker);
			// if(!$detail){
			// 	print_r($detail);
			// }
			// $detail = $this->komisikhusus_model->getKomisiOH30($field,$tanggal2);
			
		}else{
			$detail = NULL;
			
				echo '<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Maaf, Data Tidak Ditemukan!!</h4>
				Kode Agen Salah. Silakan Cek Kode Agen. Terimakasih.
			</div>';
			
			
			
		}
		
		// if(!empty($cekagen)){
		// 	//echo "masuk";die;
		// 	$kdtravel = $cekagen[0]['KdTravel'];
		// 	$detail= $this->komisikhusus_model->getKomisiTravel($kdtravel,$tglagen);
		// 	$data['ticketing']= $this->komisihusus_model->getKomisiTicketAgent($kdtravel,$tanggal);
		// }else{
			
		// 	$detail= $this->komisikhusus_model->getKomisi($field,$tglagen);
		// 	$data['ticketing']= $this->komisi_model->getKomisiTicket($field,$tanggal,$nosticker);
		// }
		
		$data['data'] = $detail;
		
		
		
		$this->load->view('transaksi/keuangan/komisikhusus/add_komisi_detail', $data);	
	}

	function getPCode()
	{
		$kode   = $this->input->post('pcode');
		$name   = $this->input->post('nama');
		$tgl    = $this->input->post('tgl');
		$bulan  = substr($tgl, 3, 2);
		$tahun  = substr($tgl, 6, 4);
		//$fieldmasuk = "QtyMasuk" . $bulan;
		//$fieldakhir = "QtyAkhir" . $bulan;
		//$fieldkeluar = "QtyKeluar" . $bulan;
		$detail = $this->komisikhusus_model->getPCodeDet($kode, $name);
		//                print_r($detail);
		if(!empty($detail)){
			$nilai = $detail->NamaRekening . "*&^%" . $detail->KdRekening; // . "*&^%" . $detail->HargaBeliAkhir;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}

	function getRealPCode()
	{
		$kode = $this->input->post('pcode');
		if(strlen($kode) == 13){
			$mylib = new globallib();
			$hasil = $mylib->findBarcode($kode);
			
			$pcode_hasil = $hasil['nilai'];
			if(count($pcode_hasil) != 0){
				$pcode = $pcode_hasil[0]['PCode'];
			}
			else
			{
				$pcode = "";
			}
		}
		else
		{
			$valpcode = $this->komisikhusus_model->ifPCodeBarcode($kode);
			if(count($valpcode) != 0){
				$pcode = $valpcode->KdRekening;
			}
			else
			{
				$pcode = "";
			}
		}
		echo $pcode;
	}

	function edit_komisi($id)
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
		if($sign == "Y"){
			$id = $this->uri->segment(4);
			$data['header'] = $this->komisikhusus_model->getHeader($id);
			$data['detail'] = $this->komisikhusus_model->getDetail($id);

			$this->load->view('keuangan/komisi/edit_komisi', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	//    function delete_komisi() {
	//        $mylib = new globallib();
	//        $id = $this->input->post('kode');
	//        //$header = $this->retur_barangmodel->getSumber($id);
	//        $user = $this->session->userdata('userid');
	//        $tgl2 = $this->session->userdata('Tanggal_Trans');
	//        $tgl = $mylib->ubah_tanggal($tgl2);
	//        //$getHeader = $this->retur_barangmodel->getHeader($id);
	//        $getDetail = $this->komisi_model->getDetail($id);
	//        $tahun = substr($getHeader->TglTransaksi, 6, 4);
	//        $lastNo = $this->komisi_model->getNewNo($tahun);
	//        $NoDelete = $id;
	//        $pcode1 = $this->input->post('pcode');
	//        $qty1 = $this->input->post('qty');
	//        $pcodesave1 = $this->input->post('savepcode');
	//
	//        if ((int) $lastNo->NoPaymentv == (int) $NoDelete + 1) {
	//            $this->db->update("setup_no", array("NoPaymentv" => $NoDelete[1]), array("Tahun" => $tahun));
	//        }
	//        $this->komisi_model->locktables('keuangan_pvheader,keuangan_pvdetail');
	//
	//        for ($x = 0; $x < count($pcode1); $x++) {
	//            $pcode = strtoupper(addslashes(trim($pcode1[$x])));
	//            $qty = trim($qty1[$x]);
	//            $pcodesave = $pcodesave1[$x];
	//        }
	//
	//        $this->db->delete('keuangan_pvheader', array('NoTransaksi' => $id));
	//        $this->db->delete('keuangan_pvdetail', array('NoTransaksi' => $id));
	//        $this->komisi_model->unlocktables();
	//    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_komisi', $data);
	}
	
	function CetakExcel()
	{
		$id          = $this->uri->segment(4);
		$data['header']      = $this->komisikhusus_model->getHeaderForExcel($id);
		$data['detail'] = $this->komisikhusus_model->getDetailForPrint($id);
		
		$html = $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_komisi_excel', $data, true);
		
		header('Content-Type: application/vnd.ms-excel');
    	header('Content-Disposition: attachment; filename="komisi.xls"');
    	print $html;
	}

	function versistruk()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip         = "192.168.0.193";
		$printer    = $this->komisikhusus_model->NamaPrinter($ip);
		
		$data['store'] = $this->komisikhusus_model->aplikasi();
		$data['header'] = $this->komisikhusus_model->getHeader($no);
		$data['detail'] = $this->komisikhusus_model->getHeaderForPrint2($no);
		
		/*print_r($data['header']);die;
		if(!empty($data['header'])){
			$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi', $data); // jika ada printernya
		}*/
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id   = $this->uri->segment(4);
		$data['fileName2'] = "lainlain.sss";
		$data['fontstyle'] = chr(27) . chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
	}
	function versistruk2()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		$data['store'] = $this->komisikhusus_model->aplikasi();
		$data['header'] = $this->komisikhusus_model->getHeader($no);
		$data['detail'] = $this->komisikhusus_model->getHeaderForPrint2($no);
		
		$data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		
		//if(!empty($data['header'])){
	//		//$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
	//		$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi2_new', $data); // jika ada printernya
	//	}
		
		$this->load->helper('text');
		$this->load->helper('print');
		$data['nl']	= "\r\n";
		$html		= $this->load->view('transaksi/keuangan/komisikhusus/cetak_strukkomisi2 _new', $data,TRUE);
		$filename	='komisi'.$no;
		$ext		='ctk';
		header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Pragma: no-cache');
		print $html;
	}
	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib       = new globallib();
		$printreport = new printreportlib();
		$id          = $this->uri->segment(4);
		$header      = $this->komisikhusus_model->getHeader($id);
		$data['header'] = $header;
		$detail = $this->komisikhusus_model->getDetailForPrint($id);
		$data['judul1'] = array("NoTransaksi","Tanggal","Keterangan","Sticker","Total Sales","Total Komisi");
		$data['niljudul1'] = array($header->NoTransaksi,$header->TglTransaksi,stripslashes($header->Keterangan),$header->KdAgent,number_format($header->TotalSales,0,'','.'),number_format($header->Total,0,'','.'));
		$data['judul2'] = "";
		$data['niljudul2'] = "";
		$data['judullap'] = "Pembayaran Komisi";
		$data['colspan_line'] = 4;
		$data['url'] = "komisikhusus/printThis/" . $id;
		$data['url2'] = "komisikhusus/versistruk/" . $id;
		$data['url3'] = "komisikhusus/versistruk2/" . $id;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
		$data['panjang_kertas'] = 30;
		$jmlh_baris_lain = 19;
		$data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
		$jml_baris_detail = count($detail);
		if($data['panjang_per_hal'] == 0){
			$data['tot_hal'] = 1;
		}
		else
		{
			$data['tot_hal'] = ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
		}
		$list_detail = array();
		$detail_attr = array();
		$list_detail_attr = array();
		$detail_page = array();
		$new_array = array();
		$counterBaris = 0;
		$counterRow   = 0;
		$max_field_len= array(0,0,0);
		$sum_netto = 0;
		//                print_r($detail);
		  for ($m = 0; $m < count($detail); $m++) {
		//			$attr = $this->komisi_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
		unset($list_detail);
		$counterRow++;
		$list_detail[] = stripslashes($detail[$m]['PCode']);
		$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
		$list_detail[] = stripslashes($detail[$m]['Qty']);
		$list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
		$detail_page[] = $list_detail;
		$max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
		if ($data['panjang_per_hal'] != 0) {
		if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
		$data['detail'][] = $detail_page;
		if ($m != count($detail) - 1) {
		unset($detail_page);
		}
		}
		}
		$netto = $detail[$m]['Harga'];
		$sum_netto = $sum_netto + ($netto);
		}
		$data['judul_netto'] = array("Total");
		$data['isi_netto'] = array(number_format($sum_netto, 0, '', '.'));
		$data['detail'][] = $detail_page;
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}
	
	function kasir_payment($id) 
    {
		$data = $this->komisikhusus_model->getTrans($id);
		echo json_encode($data);			
    }
    
    function bayar_payment() 
    {
    	
    	$mylib = new globallib();
    	//echo "<pre>";print_r($_POST);echo "</pre>";die();
    	$no_  = $this->input->post('NoTransaksi');
    	$TglPayment  = $this->input->post('v_tgl_payment');
		$KdKasBank  = $this->input->post('KdKasBank');
		$Penerima  = $this->input->post('Penerima');
		$NoBukti  = $this->input->post('NoBukti');
		$Total_komisi  = $this->input->post('Total_komisi');
		$Pembulatan_komisi  = $this->input->post('Pembulatan_komisi');
		$user        = $this->session->userdata('userid');
		
		$selisih_komisi = $Pembulatan_komisi - $Total_komisi ;
		
		
     	$bulan2 = substr($TglPayment, 3, 2);
		$tahun2 = substr($TglPayment, -2);
		
		if (date('Y-m-d',strtotime($TglPayment)) > date('Y-m-d',strtotime('2016-10-31'))){
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun2,$bulan2);
		}else{
		    $new_no = $this->paymentmodel->getNewNo($TglPayment);
		    $NoDokumen = $new_no->NoPayment;
		}
		
		
		//update dulu ke komisi
		$data_=array(
			'KdKasBank'=>$KdKasBank,
			'Penerima'=>$Penerima,
			'NoDokumen'=>$NoDokumen,
			'NoBukti'=>$NoBukti,
			'Pembulatan'=>$Pembulatan_komisi
		);
		$this->db->update('finance_komisi_khusus_header',$data_,array('NoTransaksi'=>$no_));
		
                 
    	$sql="SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi`,ROUND(SUM((c.Qty * c.Harga - c.Potongan)*(c.`Persentase`/100))) AS jumlah FROM `finance_komisi_khusus_header` b 
              INNER JOIN `finance_komisi_khusus_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
              WHERE b.`NoTransaksi`='$no_' GROUP BY d.`KdSubDivisi` having(jumlah>0);";
		
		//echo $sql;die;
		
		$rek = $this->komisikhusus_model->Querydata($sql);		
	
		$savekdrekening='42040050';
		$no=1;
		$totkomisi=0;
		for($i=0;$i<count($rek);$i++){
			
			$counter     =$no;
			$jumlah1 	=$rek[$i]['jumlah'];
			$NoBukti 	=$rek[$i]['NoBukti'];
			$keterangan =$rek[$i]['Keterangan'];
			$subdivisi 	=$rek[$i]['KdSubDivisi'];
			$NoDokumen 	=$rek[$i]['NoDokumen'];
			
			$totkomisi += $jumlah1;
			if($i== (count($rek)-1)){
				$jumlah1 += $Total_komisi-$totkomisi;
			}
			
			if($jumlah1>0){
				$this->insertpaymentdetail("add", $NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$jumlah1,$counter,$keterangan,$user,$savekdrekening,$subdivisi);
				$no=$no+1;	
			}
			
		}
		
		//Selisih pembulatan
		if($selisih_komisi>0){
			$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'44010025','25');
		}elseif($selisih_komisi<0){
			$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'43010020','25');	
		}
		$sql_="SELECT * from finance_komisi_khusus_header a where a.NoTransaksi='".$no_."'";
		$cek = $this->komisikhusus_model->Querydata($sql_);
		$ket = $cek[0]['Keterangan'];
		$total_ = $cek[0]['Total'];
		//insert new header payment	
		$this->insertpaymentheader($no,$mylib->ubah_tanggal($TglPayment),$NoBukti,$ket,$user,$Pembulatan_komisi,$Penerima,$KdKasBank, $NoDokumen);
		 
		//update ke finance komisi header
		$this->db->update('finance_komisi_khusus_header',array('TglPayment'=>$mylib->ubah_tanggal($TglPayment)),array('NoTransaksi'=>$no_));
					
    }
    
    function editpaymentheader($nodokumen,$jumlah){
		$data = array(
		    'JumlahPayment' => $jumlah
		);
		
		$this->db->update('trans_payment_header', $data, array('NoDokumen' => $nodokumen));
	}
    
    function test()
	{
		$Total_komisi  = 252000;
		$mylib = new globallib();
		$TglPayment='01-05-2017';
		$user ='tonny';
		
		$sql="SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi`,ROUND(SUM((c.Qty * c.Harga - c.Potongan)*(c.`Persentase`/100))) AS jumlah FROM `finance_komisi_khusus_header` b 
              INNER JOIN `finance_komisi_khusus_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
              WHERE b.`NoTransaksi`='17053100020' GROUP BY d.`KdSubDivisi` having(jumlah>0);";
		
		$rek = $this->komisikhusus_model->Querydata($sql);		
		
		$savekdrekening='42040050';
		$no=1;
		$totkomisi=0;
		for($i=0;$i<count($rek);$i++){
			
			$counter     =$no;
			$jumlah1 	=$rek[$i]['jumlah'];
			$NoBukti 	=$rek[$i]['NoBukti'];
			$keterangan =$rek[$i]['Keterangan'];
			$subdivisi 	=$rek[$i]['KdSubDivisi'];
			$NoDokumen 	=$rek[$i]['NoDokumen'];
			echo 'jumlah ' . $jumlah1.'<br>';
			
			$totkomisi += $jumlah1;
			if($i== (count($rek)-1)){
				$jumlah1 += $Total_komisi-$totkomisi;
			}
			
			if($jumlah1>0){
				echo 'ini';
				$this->insertpaymentdetail("add", $NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$jumlah1,$counter,$keterangan,$user,$savekdrekening,$subdivisi);
				$no=$no+1;	
			}
			
		}
			//biaya pembulatan marketing
		//$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$Pembulatan_komisi,'98','Biaya Marketing '.$Pembulatan_komisi,$user,'','');
		
		//Selisih pembulatan
		if($selisih_komisi>0){
		$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'4401002','25');
		}else{
		$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'43010020','25');	
		}
		$sql_="SELECT * from finance_komisi_khusus_header a where a.NoTransaksi='".$no_."'";
		$cek = $this->komisikhusus_model->Querydata($sql_);
		$ket = $cek[0]['Keterangan'];
		$total_ = $cek[0]['Total'];
		//insert new header payment	
		$this->insertpaymentheader($no,$mylib->ubah_tanggal($TglPayment),$NoBukti,$ket,$user,$Pembulatan_komisi,$Penerima,$KdKasBank, $NoDokumen);
		 
		//update ke finance komisi header
		$this->db->update('finance_komisi_khusus_header',array('TglPayment'=>$mylib->ubah_tanggal($TglPayment)),array('NoTransaksi'=>$no_));
		
	}
	
	function test2(){
		$sql1 ="delete d FROM trans_payment_detail d INNER JOIN finance_komisi_khusus_header h ON h.`NoDokumen`=d.`NoDokumen`
				WHERE h.`TglTransaksi`>='2017-04-01'";
		$this->db->query($sql1);
		
		$sql2 = "SELECT NoTransaksi FROM finance_komisi_khusus_header WHERE TglTransaksi>='2017-04-01'";
		$qry = $this->db->query($sql2);
        $row = $qry->result_array();
        
        for($x=0;$x<count($row);$x++){
			$notrans = $row[$x]['NoTransaksi'];
			
			$sql="SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi`,ROUND(SUM((c.Qty * c.Harga - c.Potongan)*(c.`Persentase`/100))) AS jumlah, 
				b.TglTransaksi,	b.AddUser, b.Total, b.Pembulatan
				FROM `finance_komisi_khusus_header` b 
              INNER JOIN `finance_khusus_komisi_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
              WHERE b.NoTransaksi='$notrans' GROUP BY d.`KdSubDivisi` HAVING(jumlah>0)";
		
			$rek = $this->komisikhusus_model->Querydata($sql);		
		
			$savekdrekening='42040050';
			$no=1;
			$totkomisi=0;
			$selisih_komisi = 0;
			for($i=0;$i<count($rek);$i++){
				
				$counter     =$no;
				$user = $rek[$i]['AddUser'];
				$TglPayment =$rek[$i]['TglTransaksi'];
				$jumlah1 	=$rek[$i]['jumlah'];
				$NoBukti 	=$rek[$i]['NoBukti'];
				$keterangan =$rek[$i]['Keterangan'];
				$subdivisi 	=$rek[$i]['KdSubDivisi'];
				$NoDokumen 	=$rek[$i]['NoDokumen'];
				$Total_komisi 	=$rek[$i]['Total'];
				$selisih_komisi = $rek[$i]['Pembulatan'] - $rek[$i]['Total'];
							
				$totkomisi += $jumlah1;
				if($i== (count($rek)-1)){
					$jumlah1 += $Total_komisi-$totkomisi;
				}
				
				if($jumlah1>0){
					$this->insertpaymentdetail("add", $NoDokumen,$TglPayment,$NoBukti,$jumlah1,$counter,$keterangan,$user,$savekdrekening,$subdivisi);
					$no=$no+1;	
				}
				
			}
			
			//Selisih pembulatan
			
			if($selisih_komisi>0){
				$this->insertpaymentdetail("add",$NoDokumen,$TglPayment,$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'44010025','25');
			}elseif($selisih_komisi<0){
				$this->insertpaymentdetail("add",$NoDokumen,$TglPayment,$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'43010020','25');	
			}
		}
    }
    
    

}

?>
