<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_mutasi_barang extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_mutasi_barang_model');
    }

    function index()
	{
        $mylib = new globallib();
        	
		$pcode = $this->uri->segment(4);
		$bulan = $this->uri->segment(5);
		$tahn = $this->uri->segment(6);
		
		if($bulan=="01"){
			$thn = $tahn-1;
		}else{
			$thn = $tahn;
		}
		
		$gudang = $this->uri->segment(7);
		$tipe = $this->uri->segment(8);
		$user = $this->session->userdata('username');
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        $tglawal = $tahn."-".$bulan."-01";
        $jmlhari = $this->pop_up_mutasi_barang_model->jmlhari($tglawal);
        $tglakhir = $jmlhari[0]['tgl'];
        
        
        //cari stock
        if($bulan=='01'){
        	
        	$data['selected01'] = "selected='selected'";
        	$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal12';
			$tabel_field2='GAkhir12';
		}else if($bulan=='02'){
			
			$data['selected01']="";
			$data['selected02'] = "selected='selected'";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal01';
			$tabel_field2='GAkhir01';
		}else if($bulan=='03'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03'] = "selected='selected'";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal02';
			$tabel_field2='GAkhir02';
		}else if($bulan=='04'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04'] = "selected='selected'";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal03';
			$tabel_field2='GAkhir03';
		}else if($bulan=='05'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05'] = "selected='selected'";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal04';
			$tabel_field2='GAkhir04';
		}else if($bulan=='06'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06'] = "selected='selected'";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal05';
			$tabel_field2='GAkhir05';
		}else if($bulan=='07'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07'] = "selected='selected'";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal06';
			$tabel_field2='GAkhir06';
		}else if($bulan=='08'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08'] = "selected='selected'";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal07';
			$tabel_field2='GAkhir07';
		}else if($bulan=='09'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09'] = "selected='selected'";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal08';
			$tabel_field2='GAkhir08';
		}else if($bulan=='10'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10'] = "selected='selected'";
			$data['selected11']="";
			$data['selected12']="";
			$tabel_field='GAwal09';
			$tabel_field2='GAkhir09';
		}else if($bulan=='11'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11'] = "selected='selected'";
			$data['selected12']="";
			$tabel_field='GAwal10';
			$tabel_field2='GAkhir10';
		}else if($bulan=='12'){
			
			$data['selected01']="";
			$data['selected02']="";
			$data['selected03']="";
			$data['selected04']="";
			$data['selected05']="";
			$data['selected06']="";
			$data['selected07']="";
			$data['selected08']="";
			$data['selected09']="";
			$data['selected10']="";
			$data['selected11']="";
			$data['selected12'] = "selected='selected'";
			$tabel_field='GAwal11';
			$tabel_field2='GAkhir11';
		}
        
        $data['header'] = $this->pop_up_mutasi_barang_model->getmutasiList($pcode);
        $data['detail_list'] = $this->pop_up_mutasi_barang_model->getDetailList($pcode,$tglawal,$tglakhir,$gudang,$tipe);
        if($tipe=="gdg"){
        $data['saldo'] = $this->pop_up_mutasi_barang_model->saldo($pcode,$thn,$gudang,$tabel_field,$tabel_field2);
        }else{
		$data['saldo'] = $this->pop_up_mutasi_barang_model->saldodvs($pcode,$thn,$gudang,$tabel_field,$tabel_field2);	
		}
		//$data['total_harga_detail'] = $this->pop_up_mutasi_barang_model->getTotalList($empid);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_mutasi_barang_view', $data); 
    }
	
	function index_()
	{
        $mylib = new globallib();
        	
		$empid = $this->uri->segment(4);
		$thn = $this->uri->segment(5);
		$bln = $this->uri->segment(6);
		$user = $this->session->userdata('username');
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        $data['header'] = $this->pop_up_mutasi_barang_model->getgsaList($empid);
        $data['detail_list2'] = $this->pop_up_mutasi_barang_model->getDetailList2($empid,$thn,$bln);
		//$data['total_harga_detail'] = $this->pop_up_mutasi_barang_model->getTotalList($empid);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_gsa_view_', $data); 
    }

}
?>
