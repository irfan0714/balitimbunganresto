<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class discount2 extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/discount2model');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination'); 

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        //$owner					  = $this->uri->segment(4);
		//$pcode                    = explode("X_X",$owner);
		//$row					  = $this->uri->segment(5);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/discount2/index/';
		$page					  = $this->uri->segment(4);
		$config['uri_segment']    = 4;
		$data['jenis_kartu']	  = $this->discount2model->jenis_kartu();

        $this->load->view('pop/discount2list', $data);
    }
	
	function Detaildiscount($id_discount)
    {
		$discount1 = explode('~',$id_discount);
		$mmaks = count($discount1);
		if($mmaks==2)
		{
		   $qty0 = $discount1[0];
		   $discount0 = $discount1[1];
		}else
		{
		   $qty0 = 1;
		   $discount0 = $discount1[0];
		}
        $this->discount2model->discount($discount0,$qty0);
    }
    
    function ajax_edit($nomor) {
        $data= $this->discount2model->getlistDetaildata($nomor);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
    }
    
    function ajax_cek_ktp($ktp) {
        $data= $this->discount2model->getlistDetaildataKTP($ktp);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
    }
    
    function ajax_edit_hp($nomor) {
        $data= $this->discount2model->getlistDetaildataHp($nomor);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
    }
    
    function card($jenis_kartu,$kode_store,$table) {
    	$takeaway = $this->discount2model->getTakeAway($table);
        $data= $this->discount2model->getDiscountCard($jenis_kartu,$kode_store);
        
        if(!empty($data)){
        	$data->TakeAway=$takeaway;
			echo json_encode($data);
		}else{
			echo json_encode("NO");
			
		}
		
    }
}
?>