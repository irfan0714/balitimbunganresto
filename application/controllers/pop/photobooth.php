<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class PhotoBooth extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
    }

    function index()
	{
     	
		$config['base_url']       = base_url().'index.php/pop/photobooth/index/';
		
        $this->load->view('pop/photobooth');
    }
	
	function Detaildiscount($id_discount)
    {
		$discount1 = explode('~',$id_discount);
		$mmaks = count($discount1);
		if($mmaks==2)
		{
		   $qty0 = $discount1[0];
		   $discount0 = $discount1[1];
		}else
		{
		   $qty0 = 1;
		   $discount0 = $discount1[0];
		}
        $this->discount2model->discount($discount0,$qty0);
    }
}
?>