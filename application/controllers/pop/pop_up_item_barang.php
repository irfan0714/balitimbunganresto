<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_item_barang extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_item_barang_model');
    }
    
    function index()
	{
        $mylib = new globallib();
        $id = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		
		$data["search_keyword"] = "";
		
		$resSearch = "";
		$arr_search["search"]= array();		
		$id_search = "";
		if($id*1>0)
		{
			$resSearch = $this->globalmodel->getSearch($id,"pop_up_item_barang",$user);	
			$arrSearch = explode("&",$resSearch->query_string);
			
			$id_search = $resSearch->id;
			if($id_search)
			{
				$search_keyword = explode("=", $arrSearch[0]); // search keyword
				$arr_search["search"]["keyword"] = $search_keyword[1];
				
				$data["search_keyword"] = $search_keyword[1];
			}
		}
		
        $data['item_barang'] = $this->pop_up_item_barang_model->getItemBarang($arr_search["search"]);
   
        $this->load->view('pop/pop_up_item_barang_view', $data); 
        
    }

	function search()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
		$user = $this->session->userdata('username');
			
		// Delete
		$this->db->delete('ci_query', array('module' => 'pop_up_item_barang','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pop_up_item_barang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/pop/pop_up_item_barang/index/'.$query_id.'');
	}
	
	function add_temp(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$user = $this->session->userdata('username');
		
		//detail
		$pcode1 = $this->input->post('pcode');
		$nama_barang1 = $this->input->post('nama_barang');
		$harga1 = $this->input->post('harga');
		
		for ($x = 0; $x < count($pcode1); $x++)
			{
	            $pcode = $pcode1[$x];
	            $nama_barang = $nama_barang1[$x];
	            $harga = $harga1[$x];
	            
	            if($harga!="" OR $harga!=0 OR $harga!='0'){
	            	
	                   $data_detail=array(
									  'id_temp' => 'XXXXX',
									  'PCode' => $pcode,
									  'Harga'=>$harga,
									  'adduser' => $user,
									  'adddate' => date('Y-m-d')						  
						            );				
					
						$this->db->insert('groupharga_temp', $data_detail);
					}	
			}
			
		echo json_encode(array("status" => TRUE));	
	}
}
?>
