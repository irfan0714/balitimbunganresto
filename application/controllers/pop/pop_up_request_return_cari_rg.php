<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_request_return_cari_rg extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_request_return_cari_rg_model','pop_up_reqreturnrg');
    }

    function index()
	{
        $mylib = new globallib();
        
		$id = $this->uri->segment(4);	
		$user = $this->session->userdata('username');
		
		$data["search_gudang"] = "";
		$data["search_supplier"] = "";
		
		$resSearch = "";
		$arr_search["search"]= array();
		
		$id_search = "";
		if($id*1>0)
		{
			$resSearch = $this->globalmodel->getSearch($id,"pop_up_request_return_cari_rg",$user);	
			
			$arrSearch = explode("&",$resSearch->query_string);
			
			$id_search = $resSearch->id;
			if($id_search)
			{
				$search_keyword = explode("=", $arrSearch[0]); // search keyword
				$arr_search["search"]["keyword"] = $search_keyword[1];
				$search_gudang = explode("=", $arrSearch[1]); // search gudang
				$arr_search["search"]["gudang"] = $search_gudang[1];
				$search_supplier = explode("=", $arrSearch[2]); // search supplier
				$arr_search["search"]["supplier"] = $search_supplier[1];
				
				$data["search_keyword"] = $search_keyword[1];
				$data["search_gudang"] = $search_gudang[1];
				$data["search_supplier"] = $search_supplier[1];
			}
		}
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        if($id_search)
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_up_request_return_cari_rg/index/'.$id_search.'/';
            $config['uri_segment'] = 5;
            $page = $this->uri->segment(5);
		}
		else
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_up_request_return_cari_rg/index/';
            $config['uri_segment'] = 4;
            $page = $this->uri->segment(4);
		}
		
        $data['mgudang'] = $this->pop_up_reqreturnrg->getGudang();
        $data['msupplier'] = $this->pop_up_reqreturnrg->getSupplier();
		
		$config['total_rows'] = $this->pop_up_reqreturnrg->num_barang_row($arr_search["search"]);
        $data['barangdata'] = $this->pop_up_reqreturnrg->getbarangList($config['per_page'], $page, $arr_search["search"]);
        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_request_return_cari_rg', $data); 
    }

	function search()
	{
		$mylib = new globallib();
		
		$user = $this->session->userdata('username');
		
		// Delete
		$this->db->delete('ci_query', array('module' => "pop_up_request_return_cari_rg",'AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pop_up_request_return_cari_rg",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/pop/pop_up_request_return_cari_rg/index/'.$query_id.'');
	}
}
?>
