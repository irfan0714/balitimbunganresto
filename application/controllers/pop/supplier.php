<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class supplier extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/suppliermodel');   
    }
	
    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');
		
        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
		$with 					  = $this->input->post('searchby');
		$id   					  = "";
		$flag1					  = "";
		$owner					  = $this->uri->segment(4);
		$code                     = explode("X_X",$owner);
		
		$config['base_url']       = base_url().'index.php/pop/supplier/index/'.$owner."/";
		$page					  = $this->uri->segment(5);		
		$config['uri_segment']    = 5;
		if($with!=""){
			$id    = $this->input->post('stSearchingKey');
			if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/supplier/index/'.$owner."/".$with."/".$id."/";
				$page 					= $this->uri->segment(7);
				$config['uri_segment']  = 7;
			}
			else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(6)!=""){
				$with 					= $this->uri->segment(5);
				$id 					= $this->uri->segment(6);
				$config['base_url']     = base_url().'index.php/pop/supplier/index/'.$owner."/".$with."/".$id."/";
				$page 					= $this->uri->segment(7);
				$config['uri_segment']  = 7;
			}
		}
		
		if($with!="")
		{
		$config['total_rows']    = $this->suppliermodel->num_supplier_row($id,$with);
        $data['supplierdata']    = $this->suppliermodel->getsupplierList($config['per_page'],$page,$id,$with);
		}
		else
		{
		$config['total_rows']    = $this->suppliermodel->num_supplier_row2($id,"");
		$data['supplierdata']    = $this->suppliermodel->getsupplierList2($config['per_page'],$page,$id,"");
		}
			$data['row_no']			 = "0";
			$data['module']          = $owner;
			$this->pagination->initialize($config);
		$this->load->view('pop/supplierlist', $data);	    
    }   
}
?>