<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class discount extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/discountmodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        //$owner					  = $this->uri->segment(4);
		//$pcode                    = explode("X_X",$owner);
		//$row					  = $this->uri->segment(5);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/discount/index/';
		$page					  = $this->uri->segment(4);
		$config['uri_segment']    = 4;
		$data['jenis_kartu']	  = $this->discountmodel->jenis_kartu();

        $this->load->view('pop/discountlist',$data);
    }
	
	function Detaildiscount($id_discount)
    {
		$discount1 = explode('~',$id_discount);
		$mmaks = count($discount1);
		if($mmaks==2)
		{
		   $qty0 = $discount1[0];
		   $discount0 = $discount1[1];
		}else
		{
		   $qty0 = 1;
		   $discount0 = $discount1[0];
		}
        $this->discountmodel->discount($discount0,$qty0);
    }
    
    function ajax_edit($nomor) {
        $data= $this->discountmodel->getlistDetaildata($nomor);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
		
    }
    
    function ajax_edit_hp($nomor) {
        $data= $this->discountmodel->getlistDetaildataHp($nomor);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
		
    }
    
    function card($jenis_kartu,$kode_store) {
        $data= $this->discountmodel->getDiscountCard($jenis_kartu,$kode_store);
        if(!empty($data)){
			echo json_encode($data);
		}else{
			echo json_encode($data['cek']="NO");
		}
		
    }
    
    function kuota_card($jenis_kartu,$no_card) {
        $cek  = $this->discountmodel->getKuotaCard($jenis_kartu);
        $cek2 = $this->discountmodel->getSisaKuotaCard($jenis_kartu);
        $cek3 =  $this->discountmodel->cekDiHariYangSama($jenis_kartu,$no_card);
        
        if(!empty($cek)){
			$data['MinimumBelanja']=$cek->MinimumBelanja;
			$data['Kuota']=$cek->Kuota;
			$data['maxdiscount']=$cek->MaxDiscount;
		}else{
			$data['MinimumBelanja']=0;
			$data['Kuota']=0;
			$data['maxdiscount']=0;
		}
		
		if(!empty($cek2)){
			$data['terpakai_kuota']=$cek2->terpakai_kuota;
		}else{
			$data['terpakai_kuota']=0;
		}
		
		if(!empty($cek3)){
			$data['sudah_digunakan']='Y';
			$data['namastore']=$cek3->NamaStore;
		}else{
			$data['sudah_digunakan']='T';
			$data['namastore']='';
		}
		
		echo json_encode($data);
		
    }
    
}
?>