<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_gsa extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_gsa_model');
    }

    function index()
	{
        $mylib = new globallib();
        	
		$empid = $this->uri->segment(4);
		$tgl = $this->uri->segment(5);
		$user = $this->session->userdata('username');
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        $data['header'] = $this->pop_up_gsa_model->getgsaList($empid);
        $data['detail_list'] = $this->pop_up_gsa_model->getDetailList($empid,$tgl);
		//$data['total_harga_detail'] = $this->pop_up_gsa_model->getTotalList($empid);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_gsa_view', $data); 
    }
	
	function index_()
	{
        $mylib = new globallib();
        	
		$empid = $this->uri->segment(4);
		$thn = $this->uri->segment(5);
		$bln = $this->uri->segment(6);
		$user = $this->session->userdata('username');
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        $data['header'] = $this->pop_up_gsa_model->getgsaList($empid);
        $data['detail_list2'] = $this->pop_up_gsa_model->getDetailList2($empid,$thn,$bln);
		//$data['total_harga_detail'] = $this->pop_up_gsa_model->getTotalList($empid);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_gsa_view_', $data); 
    }

}
?>
