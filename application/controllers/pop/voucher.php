<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class voucher extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/vouchermodel');
		$this->load->model('transaksi/pos_touch_model');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        //$owner					  = $this->uri->segment(4);
		//$pcode                    = explode("X_X",$owner);
		//$row					  = $this->uri->segment(5);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/voucher/index/';
		$page					  = $this->uri->segment(4);
		$config['uri_segment']    = 4;
		
		$data['kdtravel'] = $this->vouchermodel->getTourtravel();
		$ipaddres         = $this->session->userdata('ip_address');
		$nokassa          = $this->pos_touch_model->getnokassa($ipaddres);
		$datakassa        = $this->pos_touch_model->getkassa($ipaddres);
		$data['NoKassa']  = $datakassa[0]['KdStore'];

        $this->load->view('pop/voucherlist',$data);
    }
	
	function DetailVoucher($id_voucher)
    {
    	$tgl = $this->vouchermodel->aplikasi();
    	$tgltransaksi = $tgl[0]['TglTrans'];
    	
		$voucher1 = explode('~',$id_voucher);
		$mmaks = count($voucher1);
		if($mmaks==2)
		{
		   $qty0 = $voucher1[0];
		   $voucher0 = $voucher1[1];
		}else
		{
		   $qty0 = 1;
		   $voucher0 = $voucher1[0];
		}
        $this->vouchermodel->voucher($voucher0,$qty0, $tgltransaksi);
    }

	function DetailVoucherNominal($id_voucher)
    {
	
    	$tgl = $this->vouchermodel->aplikasi();
    	$tgltransaksi = $tgl[0]['TglTrans'];
    	
		$voucher1 = explode('~',$id_voucher);
		$mmaks = count($voucher1);
		if($mmaks==2)
		{
		   $qty0 = $voucher1[0];
		   $voucher0 = $voucher1[1];
		}else
		{
		   $qty0 = 1;
		   $voucher0 = $voucher1[0];
		}
        $this->vouchermodel->vouchernominal($voucher0,$qty0, $tgltransaksi);
    }

    function CekVoucherTerpakai()
    {
		$id_voucher = $this->input->post('id_voucher');
        $kassa 		= $this->input->post('kassa');
		if($id_voucher=="-"){
			return;
		}
        $this->vouchermodel->cekvoucher($id_voucher,$kassa);
    }  

    function DetailVoucherTrv($id_voucher)
    {
    	$tgl = $this->vouchermodel->aplikasi();
    	$tgltransaksi = $tgl[0]['TglTrans'];
    	
		$voucher1 = explode('~',$id_voucher);
		$mmaks = count($voucher1);
		if($mmaks==2)
		{
		   $qty0 = $voucher1[0];
		   $voucher0 = $voucher1[1];
		}else
		{
		   $qty0 = 1;
		   $voucher0 = $voucher1[0];
		}
        $this->vouchermodel->vouchertrv($voucher0,$qty0, $tgltransaksi);
    }
    
    function getVoucherTravel()
    {       
    	 $tanggal = date('Y')."-".date('m')."-01"; 
	     $travel = $this->input->post('trv');
	     $query = $this->vouchermodel->getAmbilVoucherTravel($travel,$tanggal);
	     
	      echo "<option value=''> -- Pilih Voucher --</option>";
	      foreach ($query as $cetak) {
		  echo "<option value=$cetak[novoucher]>$cetak[novoucher_travel] - $cetak[novoucher] - $cetak[BEO]</option>";
		  }   
    }
    
    function ajax_travel(){
		$var = $this->input->post('id');
		$query = $this->vouchermodel->getTravelList($var);
		 
	 echo "<option value=''> -- Pilih Travel --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdTravel]>$cetak[Nama]</option>";
	 
	    }
    }
}
?>