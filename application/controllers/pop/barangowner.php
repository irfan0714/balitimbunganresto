<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class barangowner extends authcontroller {
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/barangownermodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '10';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        $owner					  = $this->uri->segment(4);
		$row					  = $this->uri->segment(5);
		if($this->uri->segment(5)=="")
		{
		   $row = $this->uri->segment(4);
		   $owner = "0";
		}
		//echo $owner.'*';
		//echo $row.'**';
		$config['base_url']       = base_url().'index.php/pop/barangowner/index/'.$owner."/".$row."/";
		$page					  = $this->uri->segment(6);
		$config['uri_segment']    = 6;
		$flag1					  = "";
		if($with!=""){
	        if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/barangowner/index/'.$owner."/".$row."/".$with."/".$id."/";
				$page 					= $this->uri->segment(8);
				$config['uri_segment']  = 8;
			}
		 	else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(7)!=""){
				$with 					= $this->uri->segment(6);
			 	$id 					= $this->uri->segment(7);
			 	$config['base_url']     = base_url().'index.php/pop/barangowner/index/'.$owner."/".$row."/".$with."/".$id."/";
				$page 					= $this->uri->segment(8);
				$config['uri_segment']  = 8;
			}
		}

        $config['total_rows']	= $this->barangownermodel->num_barang_row($id,$with,$owner);
        $data['barangdata'] 	= $this->barangownermodel->getbarangList($config['per_page'],$page,$id,$with,$owner);
        $data['row_no']			= $row;
        $this->pagination->initialize($config);

        $this->load->view('pop/barangownerlist', $data);
    }
}
?>