<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class pop_up_bukubesar extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/bukubesar_model');
    }

    
	function index($bulanaktif1,$bulanaktif2,$tahunaktif,$kddivisi,$kdrekening1,$kdrekening2)
	{
        //print_r($_POST);die();
		$mylib = new globallib();
		$data['store']		= $this->bukubesar_model->aplikasi();
		
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif1 s/d $bulanaktif2 - $tahunaktif";
		
		$data['hasil'] = $this->bukubesar_model->getDetailTrans($tahunaktif,$bulanaktif1,$bulanaktif2,$kddivisi,$kdrekening1, $kdrekening2);
        $data['excel'] = false;
		$data['judul'] = $judul;;
		$this->load->view("report/bukubesar/reportRT", $data);
	}
	
}
?>