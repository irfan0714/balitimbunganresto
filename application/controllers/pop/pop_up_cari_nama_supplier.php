<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_cari_nama_supplier extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_cari_nama_supplier_model');
    }

    function index($barang)
	{
		
        $data['supplierdata'] = $this->pop_up_cari_nama_supplier_model->getsupplierList($barang);
        $this->load->view('pop/pop_up_cari_nama_supplier_view', $data); 
    }
}
?>
