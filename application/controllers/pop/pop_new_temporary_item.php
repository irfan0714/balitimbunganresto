<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_new_temporary_item extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_new_temporary_item_model');
    }

    function index()
	{
		//echo "test";
        $mylib = new globallib();

		$sid_detail = $this->uri->segment(4);
        $id = $this->uri->segment(5);
		$user = $this->session->userdata('username');

		$data["search_keyword"] = "";

		$resSearch = "";
		$arr_search["search"]= array();
		$id_search = "";
		if($id*1>0)
		{
			$resSearch = $this->globalmodel->getSearch($id,"pop_new_temporary_item",$user);
			$arrSearch = explode("&",$resSearch->query_string);

			$id_search = $resSearch->id;
			if($id_search)
			{
				$search_keyword = explode("=", $arrSearch[0]); // search keyword
				$arr_search["search"]["keyword"] = $search_keyword[1];

				$data["search_keyword"] = $search_keyword[1];
			}
		}

		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '20';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;

        if($id_search)
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_new_temporary_item/index/'.$sid_detail.'/'.$id_search.'/';
            $config['uri_segment'] = 6;
            $page = $this->uri->segment(6);
		}
		else
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_new_temporary_item/index/'.$sid_detail.'/';
            $config['uri_segment'] = 5;
            $page = $this->uri->segment(5);
		}

		$config['total_rows'] = $this->pop_new_temporary_item_model->num_menu_row($arr_search["search"]);
        $data['barangdata'] = $this->pop_new_temporary_item_model->getMenuList($sid_detail,$config['per_page'], $page, $arr_search["search"]);

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $this->load->view('pop/pop_new_temporary_item', $data);
    }


		function simpan(){
		$pcode		= $this->input->post('pcode');
				$harga_baru	= $this->input->post('harga1c');
				$this->db->update('masterbarang',array('Harga1c'=>$harga_baru),array('PCode'=>$pcode));
				$this->db->update('masterbarang_touch',array('Harga1c'=>$harga_baru),array('PCode'=>$pcode));
				$this->db->update('trans_order_detail',array('MenuBaru'=>0),array('PCode'=>$pcode));
				$data=array('status'=>true);
			echo json_encode($data);
		}

	function search()
	{
        $mylib = new globallib();

		$sid_detail = $this->input->post('v_nilai');
		$user = $this->session->userdata('username');

		// Delete
		$this->db->delete('ci_query', array('module' => 'pop_new_temporary_item','AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));

		$data = array(
            'query_string' => $search_value,
            'module' => "pop_new_temporary_item",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

		$query_id = $this->db->insert_id();

        redirect('/pop/pop_new_temporary_item/index/'.$sid_detail.'/'.$query_id.'');
	}
}
?>
