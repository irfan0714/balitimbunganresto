<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_penerimaan_lain extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_penerimaanlain_model','pop_up_penerimaanlain');
    }

    function index()
	{
        $mylib = new globallib();
        
		$gudang_asal = $this->uri->segment(4);	
		$sid_detail = $this->uri->segment(5);
        $id = $this->uri->segment(6);
		$user = $this->session->userdata('username');
		
		$data["search_gudang"] = $gudang_asal;
		$data["search_keyword"] = "";
		$data["search_divisi"] = "";
		$data["search_kategori"] = "";
		$data["search_brand"] = "";
		
		$resSearch = "";
		$arr_search["search"]= array();
		
		$arr_search["search"]["gudang"] = $gudang_asal;
		
		$id_search = "";
		if($id*1>0)
		{
			$resSearch = $this->globalmodel->getSearch($id,"pop_up_penerimaan_lain",$user);	
			
			$arrSearch = explode("&",$resSearch->query_string);
			
			$id_search = $resSearch->id;
			if($id_search)
			{
				$search_keyword = explode("=", $arrSearch[0]); // search keyword
				$arr_search["search"]["keyword"] = $search_keyword[1];
				$search_gudang = explode("=", $arrSearch[1]); // search gudang
				$arr_search["search"]["gudang"] = $search_gudang[1];
				$search_divisi = explode("=", $arrSearch[2]); // search divisi
				$arr_search["search"]["divisi"] = $search_divisi[1];
				$search_kategori = explode("=", $arrSearch[3]); // search kategori
				$arr_search["search"]["kategori"] = $search_kategori[1];
				$search_brand = explode("=", $arrSearch[4]); // search brand
				$arr_search["search"]["brand"] = $search_brand[1];
				
				$data["search_keyword"] = $search_keyword[1];
				$data["search_gudang"] = $search_gudang[1];
				$data["search_divisi"] = $search_divisi[1];
				$data["search_kategori"] = $search_kategori[1];
				$data["search_brand"] = $search_brand[1];
			}
		}
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        if($id_search)
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_up_penerimaan_lain/index/'.$gudang_asal.'/'.$sid_detail.'/'.$id_search.'/';
            $config['uri_segment'] = 7;
            $page = $this->uri->segment(7);
		}
		else
		{
			$config['base_url'] = base_url() . 'index.php/pop/pop_up_penerimaan_lain/index/'.$gudang_asal.'/'.$sid_detail.'/';
            $config['uri_segment'] = 6;
            $page = $this->uri->segment(6);
		}
		
        $data['mgudang'] = $this->pop_up_penerimaanlain->getGudang();
        $data['mdivisi'] = $this->pop_up_penerimaanlain->getDivisi();
        $data['mkategori'] = $this->pop_up_penerimaanlain->getKategori();
        $data['mbrand'] = $this->pop_up_penerimaanlain->getBrand();
		
		$config['total_rows'] = $this->pop_up_penerimaanlain->num_barang_row($arr_search["search"]);
        $data['barangdata'] = $this->pop_up_penerimaanlain->getbarangList($config['per_page'], $page, $arr_search["search"]);
        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_penerimaan_lain', $data); 
    }

	function search()
	{
		$mylib = new globallib();
		
		$sid_detail = $this->input->post('v_nilai');
		$user = $this->session->userdata('username');
		
		if($this->input->post('search_gudang')=="")
		{
			$search_gudang = 0;	
		}
		else
		{
			$search_gudang = $this->input->post('search_gudang');	
		}
		
		
		// Delete
		$this->db->delete('ci_query', array('module' => "pop_up_penerimaan_lain",'AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_divisi=".$this->input->post('search_divisi');
		$search_value .= "&search_kategori=".$this->input->post('search_kategori');
		$search_value .= "&search_brand=".$this->input->post('search_brand');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pop_up_penerimaan_lain",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
		redirect('/pop/pop_up_penerimaan_lain/index/'.$search_gudang.'/'.$sid_detail.'/'.$query_id.'');	
	}
}
?>
