<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class barangbeli extends authcontroller {
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/barangbelimodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
      $with = $this->input->post('searchby');
      $this->load->library('pagination');

      $config['full_tag_open']  = '<div class="pagination">';
      $config['full_tag_close'] = '</div>';
      $config['cur_tag_open']   = '<span class="current">';
      $config['cur_tag_close']  = '</span>';
      $config['per_page']       = '10';
      $config['first_link'] 	  = 'First';
      $config['last_link'] 	  = 'Last';
      $config['num_links']  	  = 2;
      $owner					  	= $this->uri->segment(4);
		$row					  		= $this->uri->segment(4);
		$kdgroupext				  	= $this->uri->segment(5);
		//echo $row;die();
		$config['base_url']       = base_url().'index.php/pop/barangbeli/index/'.$owner."/".$row."/".$kdgroupext."/";
		$page					  = $this->uri->segment(7);
		$config['uri_segment']    = 7;
		$flag1					  = "";
		if($with!=""){
	        if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/barangbeli/index/'.$owner."/".$row."/".$kdgroupext."/".$with."/".$id."/";
				$page 					= $this->uri->segment(9);
				$config['uri_segment']  = 9;
			}
		 	else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(8)!=""){
				$with 					= $this->uri->segment(7);
			 	$id 					= $this->uri->segment(8);
			 	$config['base_url']     = base_url().'index.php/pop/barangbeli/index/'.$owner."/".$row."/".$kdgroupext."/".$with."/".$id."/";
				$page 					= $this->uri->segment(9);
				$config['uri_segment']  = 9;
			}
		}

        $config['total_rows']	= $this->barangbelimodel->num_barang_row($id,$with,$owner,$kdgroupext);
        $data['barangdata'] 	= $this->barangbelimodel->getbarangList($config['per_page'],$page,$id,$with);
        $data['row_no']			= $row;
        $this->pagination->initialize($config);

        $this->load->view('pop/barangbelilist', $data);
    }
}
?>
