<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class kreditedc extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
    }

    function index()
	{
		$config['base_url']       = base_url().'index.php/pop/kreditedc/index/';
		$sql = "SELECT * FROM kartu WHERE tipe<>'EDC' AND status = 1 ORDER BY nama ASC";
		$data['kartu'] = $this->getArrayResult($sql);
		$zql = "SELECT * FROM kartu WHERE tipe='EDC'  AND status = 1 ORDER BY nama ASC";
		$data['edc'] = $this->getArrayResult($zql);
        $this->load->view('pop/kreditedc', $data);
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
}
?>