<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_detail_menu extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_reservasi_model');
    }

    function index()
	{
        $mylib = new globallib();
        $tipe = $this->uri->segment(4);
        if($tipe=="SGV"){
			$thn = $this->uri->segment(7);
	        $bln = $this->uri->segment(8);
	        $no = $this->uri->segment(9);
         	$nodok = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;	
         	$booking = "T";
		}else{
		 	$nodok = $this->uri->segment(4);
		 	$booking = "Y";
		}
		
        
		$data['nodokumen'] = $nodok;
		$data['booking'] = $booking;
		
		$data['detailMenu'] = $this->globalmodel->getQuery(" * from trans_reservasi_detail where NoDokumen='".$nodok."'");
		
        $this->load->view('pop/pop_up_detail_menu',$data); 
    }
	
	function hapus_trans_detail($id){
		
		$tipe = $this->uri->segment(5);
        if($tipe=="SGV"){
			$thn = $this->uri->segment(8);
	        $bln = $this->uri->segment(9);
	        $no = $this->uri->segment(10);
         	$nodok = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;	
         	$booking = "T";
		}else{
		 	$nodok = $this->uri->segment(5);
		 	$booking = "Y";
		}
		
		$where = array("sid" => $id);
		$this->globalmodel->queryDelete("trans_reservasi_detail",$where);
		redirect("pop/pop_up_detail_menu/index/".$nodok."");
	}
	
	// function keluar_update_total(){
		// $thn = $this->uri->segment(7);
        // $bln = $this->uri->segment(8);
        // $no = $this->uri->segment(9);
        // $v_grand_total = $this->uri->segment(10);
		// echo $v_grand_total;die;
        // $nodok = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
		
        //$v_grand_total = $this->input->post('gtotal');
		// $data2 = array("Total" => $v_grand_total);
		// $where = array("NoDokumen" => $nodok);
		// $this->globalmodel->queryUpdate("trans_reservasi",$data2,$where);
		
		// echo "<script>self.close();</script>";
	// }
	
	function dateRomawi($bulan) {
		switch ($bulan) {
		case 1:
		$blnRom = 'I';
		break;
		case 2:
		$blnRom = 'II';
		break;
		case 3:
		$blnRom = 'III';
		break;
		case 4:
		$blnRom = 'IV';
		break;
		case 5:
		$blnRom = 'V';
		break;
		case 6:
		$blnRom = 'VI';
		break;
		case 7:
		$blnRom = 'VII';
		break;
		case 8:
		$blnRom = 'VIII';
		break;
		case 9:
		$blnRom = 'IX';
		break;
		}
		return $blnRom;
	}
}
?>
