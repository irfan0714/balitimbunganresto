<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_cari_nama_barang extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_cari_nama_barang_model');
    }

    function index($barang)
	{
		
        $data['barangdata'] = $this->pop_up_cari_nama_barang_model->getbarangList($barang);
        $this->load->view('pop/pop_up_cari_nama_barang_view', $data); 
    }
}
?>
