<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Baranglainlain extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('pop/baranglain_model');
        $this->load->model('pop/barangmodel');
    }

    function index() {
        $id = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');
//echo "ok";
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $config['per_page'] = '14';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
//        $tipe = $this->uri->segment(4); // masuk / keluar
//        $baris = $this->uri->segment(4);
        $owner = $this->uri->segment(4);
        $pcode = explode("X_X", $owner);
        $row = $this->uri->segment(5);
        $config['base_url'] = base_url() . 'index.php/pop/baranglainlain/index/' . $owner . "/" . $row . "/";
        $page = $this->uri->segment(6);
        $config['uri_segment'] = 6;
        $flag1 = "";

        if ($with != "") {
            if ($id != "" && $with != "") {
                $config['base_url'] = base_url() . 'index.php/pop/baranglainlain/index/' . $owner . "/" . $row . "/" . $with . "/" . $id . "/";
                $page = $this->uri->segment(8);
                $config['uri_segment'] = 8;
            } else {
                $page = "";
            }
        } else {
            if ($this->uri->segment(7) != "") {
                $with = $this->uri->segment(6);
                $id = $this->uri->segment(7);
                $config['base_url'] = base_url() . 'index.php/pop/baranglainlain/index/' . $owner . "/" . $row . "/" . $with . "/" . $id . "/";
                $page = $this->uri->segment(8);
                $config['uri_segment'] = 8;
            }
        }
//echo $with;
        $config['total_rows'] = $this->baranglain_model->num_barang_row(addslashes($id), $with);
        $data['barangdata'] = $this->baranglain_model->getbarangList($config['per_page'], $page, addslashes($id), $with);
        $data['row_no'] = $row;
        $this->pagination->initialize($config);

        $this->load->view('pop/baranglist', $data);
    }

}

?>