<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class barang extends authcontroller {
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/barangmodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '20';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
		$owner					  = $this->uri->segment(4);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/barang/index/'.$owner."/";
		$page					  = $this->uri->segment(5);
		$config['uri_segment']    = 5;
		$flag1					  = "";
		if($with!=""){
	        if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/barang/index/'.$owner."/".$with."/".$id."/";
				$page 					= $this->uri->segment(7);
				$config['uri_segment']  = 7;
			}
		 	else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(6)!=""){
				$with 					= $this->uri->segment(5);
			 	$id 					= $this->uri->segment(6);
			 	$config['base_url']     = base_url().'index.php/pop/barang/index/'.$owner."/".$with."/".$id."/";
				$page 					= $this->uri->segment(7);
				$config['uri_segment']  = 7;
			}
		}

        $config['total_rows']	= $this->barangmodel->num_barang_row($id,$with,$owner);
        $data['barangdata'] 	= $this->barangmodel->getbarangList($config['per_page'],$page,$id,$with,$owner);
        $this->pagination->initialize($config);

        $this->load->view('pop/baranglist', $data);
    }
}
?>