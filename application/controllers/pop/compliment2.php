<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class compliment2 extends authcontroller {
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/compliment2model');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        //$owner					  = $this->uri->segment(4);
		//$pcode                    = explode("X_X",$owner);
		//$row					  = $this->uri->segment(5);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/compliment2/index/';
		$page					  = $this->uri->segment(4);
		$config['uri_segment']    = 4;

        $this->load->view('pop/compliment2list');
    }
	
	function Detailcompliment2($id_compliment2)
    {
		$compliment21 = explode('~',$id_compliment2);
		$mmaks = count($compliment21);
		if($mmaks==2)
		{
		   $qty0 = $compliment21[0];
		   $compliment20 = $compliment21[1];
		}else
		{
		   $qty0 = 1;
		   $compliment20 = $compliment21[0];
		}
        $this->compliment2model->compliment2($compliment20,$qty0);
    }
}
?>