<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class nobukti extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/nobuktimodel');
    }

    function index()
	{
     	$id2  = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '10';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
		$row					  = $this->uri->segment(4);
		$id  				      = $this->uri->segment(5);
		if(($id2<>$id)&&($id2<>""))
		   $id=$id2;
		$prow                     = substr($row,0,1);
		$lrow                     = substr($row,1,strlen($row)-1);
		$config['base_url']       = base_url().'index.php/pop/nobukti/index/'.$row."/".$id."/";
		$page					  = $this->uri->segment(6);
		$config['uri_segment']    = 6;
		
		if(($prow=="C")||($prow=="H")||($prow=="D")||($prow=="S"))
		{
		   $kode = $this->uri->segment(6);
		   $config['base_url']       = base_url().'index.php/pop/nobukti/index/'.$row."/".$id."/".$kode."/";
		   $page					 = $this->uri->segment(7);
		   $config['uri_segment']    = 7;
		}
		else
		   $kode = "";
        if($id=="1234567890")
		   $id="";
        $config['total_rows']	= $this->nobuktimodel->num_nobukti_row($id,$prow,$kode);
		
        $data['nobuktidata'] 	= $this->nobuktimodel->getnobuktiList($config['per_page'],$page,$id,$prow,$kode);
        $data['row_no']			= $lrow;
		$data['module'] 	    = $prow;
        $this->pagination->initialize($config);

        $this->load->view('pop/nobuktilist', $data);
    }
}
?>