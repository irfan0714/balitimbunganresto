<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_no_rek_master_barang extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);
        $this->load->model('pop/pop_up_no_rek_model');
    }

    function index()
	{
        $mylib = new globallib();
        	
		$nourut = $this->uri->segment(4);	
		$namarek = $this->uri->segment(5);
		
        $data['data'] = $this->pop_up_no_rek_model->getNoRekListMasterBarang($namarek);
                
        $this->load->view('pop/pop_up_no_rek_views', $data); 
    }
}
?>
