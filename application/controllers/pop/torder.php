<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class torder extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/tordermodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '20';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
		$page					  = $this->uri->segment(4);
		$config['base_url']       = base_url().'index.php/pop/torder/index/';
		$config['uri_segment']    = 4;
		$flag1					  = "";
		if($with!=""){
	        if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/torder/index/'.$with."/".$id."/";
				$page 					= $this->uri->segment(6);
				$config['uri_segment']  = 6;
			}
		 	else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(5)!=""){
				$with 					= $this->uri->segment(4);
			 	$id 					= $this->uri->segment(5);
			 	$config['base_url']     = base_url().'index.php/pop/torder/index/'.$with."/".$id."/";
				$page 					= $this->uri->segment(6);
				$config['uri_segment']  = 7;
			}
		}
		$data['bulan'] = $this->session->userdata('bulanaktif');
		$data['tahun'] = $this->session->userdata('tahunaktif');
		$thnbln = $data['tahun'].$data['bulan'];
        $config['total_rows']	= $this->tordermodel->num_order_row($id,$with,$thnbln);
        $data['data']			= $this->tordermodel->getorderList($config['per_page'],$page,$id,$with,$thnbln);
        $this->pagination->initialize($config);

        $this->load->view('pop/torderlist', $data);
    }
}
?>