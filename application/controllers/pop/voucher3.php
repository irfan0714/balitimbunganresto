<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Voucher3 extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/voucher3model');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '12';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
        //$owner					  = $this->uri->segment(4);
		//$pcode                    = explode("X_X",$owner);
		//$row					  = $this->uri->segment(5);
		//echo $row;
		$config['base_url']       = base_url().'index.php/pop/voucher3/index/';
		$page					  = $this->uri->segment(4);
		$config['uri_segment']    = 4;

        $this->load->view('pop/voucher3list');
    }
	
	function DetailVoucher($id_voucher)
    {
    	$tgl = $this->voucher3model->aplikasi();
    	$tgltransaksi = $tgl[0]['TglTrans'];
    	
		$voucher1 = explode('~',$id_voucher);
		$mmaks = count($voucher1);
		if($mmaks==2)
		{
		   $qty0 = $voucher1[0];
		   $voucher0 = $voucher1[1];
		}else
		{
		   $qty0 = 1;
		   $voucher0 = $voucher1[0];
		}
        $this->voucher3model->voucher($voucher0,$qty0, $tgltransaksi);
    }
    
    function DetailVoucherTrv($id_voucher)
    {
    	$tgl = $this->voucher3model->aplikasi();
    	$tgltransaksi = $tgl[0]['TglTrans'];
    	
		$voucher1 = explode('~',$id_voucher);
		$mmaks = count($voucher1);
		if($mmaks==2)
		{
		   $qty0 = $voucher1[0];
		   $voucher0 = $voucher1[1];
		}else
		{
		   $qty0 = 1;
		   $voucher0 = $voucher1[0];
		}
        $this->voucher3model->vouchertrv($voucher0,$qty0, $tgltransaksi);
    }
}
?>