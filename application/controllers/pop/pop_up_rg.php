<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pop_up_rg extends authcontroller
{
	function __construct()
	{
        parent::__construct();
        error_reporting(0);       
		$this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('pop/pop_up_rg_model');
    }

    function index()
	{
        $mylib = new globallib();
        	
		$nodokumen = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		
		// pagingtion
        $this->load->library('pagination');
		$config = array();
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['per_page'] = '10';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['num_links'] = 2;
        
        $data['header'] = $this->pop_up_rg_model->getrgList($nodokumen);
        $data['detail_list'] = $this->pop_up_rg_model->getDetailList($nodokumen);
		$data['total_harga_detail'] = $this->pop_up_rg_model->getTotalList($nodokumen);
        $data['mgudang'] = $this->pop_up_rg_model->getGudang();
		$data['mcustomer'] = $this->pop_up_rg_model->getCustomer();
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('pop/pop_up_rg_view', $data); 
    }

}
?>
