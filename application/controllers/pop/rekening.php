<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class rekening extends authcontroller
{
	function __construct()
	{
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('pop/rekeningmodel');
    }

    function index()
	{
     	$id   = $this->input->post('stSearchingKey');
        $with = $this->input->post('searchby');
        $this->load->library('pagination');

        $config['full_tag_open']  = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open']   = '<span class="current">';
        $config['cur_tag_close']  = '</span>';
        $config['per_page']       = '10';
        $config['first_link'] 	  = 'First';
        $config['last_link'] 	  = 'Last';
        $config['num_links']  	  = 2;
		
		$owner					  = $this->uri->segment(4);
		$pcode                    = explode("X_X",$owner);
		$row					  = $this->uri->segment(5);
		$config['base_url']       = base_url().'index.php/pop/rekening/index/'.$owner."/".$row."/";
		$page					  = $this->uri->segment(6);
		$config['uri_segment']    = 6;
		$flag1					  = "";
		if($with!=""){
	        if($id!=""&&$with!=""){
				$config['base_url']     = base_url().'index.php/pop/rekening/index/'.$owner."/".$row."/".$with."/".$id."/";
				$page 					= $this->uri->segment(8);
				$config['uri_segment']  = 8;
			}
		 	else{
				$page ="";
			}
		}
		else{
			if($this->uri->segment(7)!=""){
				$with 					= $this->uri->segment(6);
			 	$id 					= $this->uri->segment(7);
			 	$config['base_url']     = base_url().'index.php/pop/rekening/index/'.$owner."/".$row."/".$with."/".$id."/";
				$page 					= $this->uri->segment(8);
				$config['uri_segment']  = 8;
			}
		}
        //echo $pcode[1];die();
        $config['total_rows']	= $this->rekeningmodel->num_rekening_row($id,$with,$pcode[1]);
        $data['rekeningdata'] 	= $this->rekeningmodel->getrekeningList($config['per_page'],$page,$id,$with,$pcode[1]);
        $data['row_no']			= $row;
		$data['module']			= $pcode[0];
        $this->pagination->initialize($config);

        $this->load->view('pop/rekeninglist', $data);
    }
}
?>