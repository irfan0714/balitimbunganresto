<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class do_backup extends CI_Controller
{
  // backup files in directory
  function files()
  {
     $opt = array(
       'src' => '../dataroom/assets/', // dir name to backup
       'dst' => './backup/files/' // dir name backup output destination
     );
     
     /* Codeigniter v3x */
     $this->load->library('recurseZip_lib', $opt);
     $download = $this->recursezip_lib->compress();
     
    // Codeigniter v2x
    // $zip    = $this->load->library('recurseZip_lib', $opt);     
    // $download = $zip->compress();
     
     redirect(base_url($download));
  }
   
  // backup database.sql
  public function db()
  {  
    ini_set('memory_limit', '900M');
    ini_set('upload_max_filesize', '565M');
    $this->load->dbutil();
    $dbs = $this->dbutil->list_databases();
    // echo "<pre>";print_r($dbs);die;

    $arr_dbname = array('db_natura','hrd');
    foreach ($dbs as $db)
    {
        if(in_array($db, $arr_dbname))
        {
          $prefs = array(
             'format' => 'zip',
             'filename' => 'db_natura.sql'
           );
         
           $backup =& $this->dbutil->backup($prefs);
         
           $db_name = 'backup-dbnatura-' . date("Y-m-d-H-i-s") . '.zip'; // file name
           $save  = 'D:/BackupNPM/' . $db_name; // dir name backup output destination
         
           $this->load->helper('file');
           write_file($save, $backup);
         
           // $this->load->helper('download');
           // force_download($db_name, $backup);

           echo "Selesai";
        }
    }

     
  }

  function test(){
     $this->load->dbutil();
    $dbs = $this->dbutil->list_databases();

    foreach ($dbs as $db)
    {
            echo $db."<br>";
    }
  }
   
}