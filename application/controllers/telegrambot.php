<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class TelegramBot extends authcontroller {
    var $isusermodify;
    var $TOKEN = "417769536;AAGwPnLiMGGhgNST3Fd3cABwm4bui5XC3ak";
    
	function __construct(){
        parent::__construct();
        $this->load->model('telegram_model');
	}
	
	function index()
	{
	 	while (true) {
			$this->process_one();
		}
	}
	
	function process_one()
	{
		$update_id  = 0;
		if (file_exists("last_update_id")) {
			$update_id = (int)file_get_contents("last_update_id");
		}
		$updates = $this->get_updates($update_id);
		foreach ($updates as $message)
		{
	     		$update_id = $this->process_message($message);
		}
		file_put_contents("last_update_id", $update_id + 1);
	}
	
	function process_message($message)
	{
	    $updateid = $message["update_id"];
	    $message_data = $message["message"];
	    if (isset($message_data["text"])) {
			$chatid = $message_data["chat"]["id"];
	        $message_id = $message_data["message_id"];
	        $userid = $message_data["user_id"];
	        $text = $message_data["text"];
	        $date = $message_data["date"];
	        echo "@". $userid.'-'.$chatid . "-". date('d-m-Y H:i:s',$date+18000) . " => " . $text . "\n";
	        $response = $this->create_response($text,$chatid);
	        //send_reply($chatid, $message_id, $response);
	        //echo "@".$chatid." <= " . $response . "\n";
	    }
	    return $updateid;
	}

	function request_url($method)
	{
		return "https://api.telegram.org/bot" . $this->TOKEN . "/". $method;
	}
	
	function get_updates($offset) 
	{
		$url = $this->request_url("getUpdates")."?offset=".$offset;
		$aContext = array(
	    	'http' => array(
	        	'request_fulluri' => true
	    	),
		);
		$cxContext = stream_context_create($aContext);

		try {
	     	$resp = file_get_contents($url, False, $cxContext);
	        $result = json_decode($resp, true);
	        if ($result["ok"]==1)
	            return $result["result"];
		}
		catch (Exception $e) {
	    	//echo $e->getMessage();
	    	echo "Koneksi gagal.";
		}
		restore_error_handler();
	 	return array();
	       
	}

	function send_reply($chatid, $msgid, $text)
	{
	    $data = array(
	        'chat_id' => $chatid,
	        'text'  => $text
	        // 'reply_to_message_id' => $msgid
	    );
	    // use key 'http' even if you send the request to https://...
	    $options = array(
	    	'http' => array(
	        	'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        	'method'  => 'POST',
	        	'content' => http_build_query($data),
	    	),
	    );
	    $context  = stream_context_create($options);
	    $result = file_get_contents($this->request_url('sendMessage'), false, $context);
	    //print_r($result);
	}

	function create_response($text, $chatid)
	{
	   $input = strtolower($text);
	   switch ($input) {
	    case "kode":
	        $hasil = "Kode : " . $chatid;
	        break;
	    case "s":
	    	$hasil = $this->telegrambotmodel->getSaldo($chatid);
	        break;
	    default:
	        $hasil = "Perintah tidak dikenal.";
	   		break;
	   	}
	   return $hasil;
	}	
}
