<?php
class ExportContract extends CI_Controller {

	function __construct(){
			parent::__construct();
            $this->load->library('globallib');

	}

    function import()
    {


        $mylib = new globallib();

         $this->load->library('excel');

        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        if(isset($_FILES["file"]["name"]))
        {
            // print_r($_POST);
            // die();
            //tes2
            $target_file = './upload/temp/';
            $buat_folder_temp = !is_dir($target_file) ? @mkdir("./upload/temp/") : false;
        
            move_uploaded_file($_FILES["file"]["tmp_name"], $target_file.$_FILES['file']['name']);

            $file   = explode('.',$_FILES['file']['name']);
            $length = count($file);

        // if($file[$length -1] !== 'xlsx' || $file[$length -1] !== 'xls') {
        //     print_r($file[$length -1]);
        //     exit("Bukan FileExcel..");
        //     // die();
        // }

            $tmp    = './upload/temp/'.$_FILES['file']['name'];
            //tes2
            //tes
        // $user = $this->session->userdata('username');
        $user = "ImportExcel";

        $tgltransaksi   = $mylib->ubah_tanggal($_POST['tgltransaksi']);
        $supplier       = $_POST['supplier'];
        $nokontrak      = $_POST['nokontrak'];
        $tgl1           = $mylib->ubah_tanggal($_POST['tgl1']);
        $tgl2           = $mylib->ubah_tanggal($_POST['tgl2']);

        // $pcode          = $_POST['pcode'];
        // $satuan         = $_POST['v_satuan'];
        // $hargadefault   = ubah_awal($_POST['hargadefault']);
        // $hargacontract  = ubah_awal($_POST['hargacontract']);

        $tgx = explode("-",$tgltransaksi);
        $notrans_awal = substr($tgx[0],2,2).$tgx[1];

        $q = "SELECT NoTransaksi
                FROM contract_supplier_header
                WHERE NoTransaksi LIKE '$notrans_awal%'
                ORDER BY NoTransaksi DESC
                LIMIT 1";
        $qry = $this->db->query($q);
        $num = $qry->num_rows();
        if($num>0){
            $hsl = $qry->result_array();
            // print_r($hsl);
            // print_r($hsl[0]['NoTransaksi']);
            // die();
            $notrans_new = $hsl[0]['NoTransaksi'] + 1;
           
        }
        else{
                $notrans_new = $notrans_awal."0001";
        }

        $qh_insert = "INSERT INTO contract_supplier_header(`NoTransaksi`,`TglTransaksi`,`KdSupplier`,NoKontrak,`PeriodeAwal`,`PeriodeAkhir`,`Status`,`AddDate`,`AddUser`)
                        VALUES ('$notrans_new','$tgltransaksi','$supplier','$nokontrak','$tgl1','$tgl2','0',NOW(),'$user')";
        // $qd_insert = "INSERT INTO contract_supplier_detail(`sid`,`NoTransaksi`,`PCode`,Satuan,`HargaDefault`,`HargaContract`)
        //                 VALUES (NULL,'$notrans_new','$pcode','$satuan','$hargadefault','$hargacontract')";
        //echo $qd_insert;die();
        $ekse1 = $this->db->query($qh_insert);

        // print_r($ekse1);

        // die();
        // $ekse1 = mysql_query($qh_insert) or die("$qh_insert ".mysql_error());
        // $ekse2 = mysql_query($qd_insert) or die("$qd_insert ".mysql_error());

        // if($ekse1 and $ekse2){
        //echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_edit.php?flag=edit&&notransaksi=$notrans_new'>";
        // }
            //tes


            // $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($tmp);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $pcode = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $satuan = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $harga = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    // $postal_code = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    // $country = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $data[] = array(
                        'NoTransaksi'       =>  $notrans_new,
                        'PCode'             =>  $pcode,
                        'Satuan'            =>  $satuan,
                        'HargaContract'     =>  $harga
                   
                    );
                }
            }
            $this->db->insert_batch('contract_supplier_detail', $data);
            $result ='Data Imported successfully';
            echo json_encode($result);
            // redirect('contract_supplier_edit.php?flag=edit&&notransaksi=$notrans_new');
            // header("http://sys.bebektimbungan.com/inventory/contract_supplier_edit.php?flag=edit&notransaksi=$notrans_new");
            // echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_edit.php?flag=edit&&notransaksi=$notrans_new'>";
        }   
    }

	public function ExportContract()
    {
        $notransaksi = $this->input->post('notrans');
				// $notransaksi ='18090004';
				//18090004
        // $siteSettingData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        // $site_dateformat = $siteSettingData[0]->datetime_format;
        // $site_currency = $siteSettingData[0]->currency;
				//
        // $custDtaData = $this->Constant_model->getDataOneColumn('customers', 'id', "$cust_id");
        // $cust_fn = $custDtaData[0]->fullname;

        $this->load->library('excel');

        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        $default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        );

        $acc_default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => 'c7c7c7'),
        );
        $outlet_style_header = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 10,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $top_header_style = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 15,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $account_value_style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $text_align_style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
        );

        $lang_sales_history = "Sales History";
        $lang_sale_id = $this->lang->line('sale_id');
        $lang_type = $this->lang->line('type');
        $lang_date_time = $this->lang->line('date_time');
        $lang_products = $this->lang->line('products');
        $lang_qty = $this->lang->line('quantity');
        $lang_total_qty = $this->lang->line('total_quantity');
        $lang_sub_total = $this->lang->line('sub_total');
        $lang_tax = $this->lang->line('tax');
        $lang_grand_total = $this->lang->line('grand_total');
        $lang_total = $this->lang->line('total');

				$sql2 ="SELECT a.PeriodeAwal as Awal, a.PeriodeAkhir as Akhir, b.Nama as Supplier FROM contract_supplier_header a INNER JOIN supplier b ON a.KdSupplier=b.KdSupplier  WHERE a.NoTransaksi = '$notransaksi'";
				$row = $this->db->query($sql2);
				$headerc = $row->row_array();
				$namasupplier = $headerc["Supplier"];
				$awal = $headerc["Awal"];
				$akhir = $headerc["Akhir"];

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "PT NATURA PESONA MANDIRI");
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:E2');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "CONTRACT ".$namasupplier);
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:E3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', "PERIODE ".$awal." sd ".$akhir);

        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($top_header_style);
				$objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray($top_header_style);
        // $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($top_header_style);
        // $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($top_header_style);
        // $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($top_header_style);
        // $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($top_header_style);

        $objPHPExcel->getActiveSheet()->setCellValue('A4', "PCODE");
        $objPHPExcel->getActiveSheet()->setCellValue('B4', "NAMA LENGKAP");
        $objPHPExcel->getActiveSheet()->setCellValue('C4', "SATUAN");
        $objPHPExcel->getActiveSheet()->setCellValue('D4', "HARGA BARANG");
        $objPHPExcel->getActiveSheet()->setCellValue('E4', "HARGA KONTRAK");
        // $objPHPExcel->getActiveSheet()->setCellValue('F2', "QTY");
        // $objPHPExcel->getActiveSheet()->setCellValue('G2', "SUBTOTAL");
        // $objPHPExcel->getActiveSheet()->setCellValue('H2', "PAJAK");
        // $objPHPExcel->getActiveSheet()->setCellValue('I2', "TOTAL");

        $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($style_header);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        // $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        // $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        // $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        // $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        $jj = 5;

        $total_sub_amt = 0;
        $total_tax_amt = 0;
        $total_grand_amt = 0;

        $orderResult = $this->db->query("	SELECT
							d.PCode, m.NamaLengkap, d.Satuan, d.HargaDefault, d.HargaContract
						FROM
							contract_supplier_detail d, masterbarang m
						WHERE 1
						AND d.PCode=m.PCode
						AND d.NoTransaksi='".$notransaksi."'
						ORDER BY sid ASC");
        $orderData = $orderResult->result();
        for ($d = 0; $d < count($orderData); ++$d) {
            $pcode = $orderData[$d]->PCode;
            $namalengkap = $orderData[$d]->NamaLengkap;
            $satuan = $orderData[$d]->Satuan;
            $hargad = $orderData[$d]->HargaDefault;
            $hargac = $orderData[$d]->HargaContract;
            // $total_item_qty = $orderData[$d]->PeriodeAkhir;
            // $order_type = $orderData[$d]->Status;

            // $total_sub_amt += $subTotal;
            // $total_tax_amt += $gstTotal;
            // $total_grand_amt += $grandTotal;

            // $pcodeArray = array();
            // $pnameArray = array();
            // $qtyArray = array();
            // $type_name = '';
						//
            // if ($order_type == '1') {                 // Order;
						//
            //     $type_name = 'Sale';
						//
            //     $oItemResult = $this->db->query("SELECT * FROM contract_supplier_detail WHERE NoTransaksi = '$notransaksi'");
            //     $oItemRows = $oItemResult->num_rows();
            //     if ($oItemRows > 0) {
            //         $oItemData = $oItemResult->result();
						//
            //         for ($t = 0; $t < count($oItemData); ++$t) {
            //             $oItem_pcode = $oItemData[$t]->PCode;
            //             $oItem_pname = $oItemData[$t]->Satuan;
            //             $oItem_qty = $oItemData[$t]->HargaContract;
						//
            //             array_push($pcodeArray, $oItem_pcode);
            //             array_push($pnameArray, $oItem_pname);
            //             array_push($qtyArray, $oItem_qty);
						//
            //             unset($oItem_pcode);
            //             unset($oItem_pname);
            //             unset($oItem_qty);
            //         }
						//
            //         unset($oItemData);
            //     }
            //     unset($oItemResult);
            //     unset($oItemRows);
            // }
						// elseif ($order_type == '2') {    // Return;
            //     $type_name = 'Return';
						//
            //     $rItemResult = $this->db->query("SELECT * FROM return_items WHERE order_id = '$order_id' ORDER BY id ");
            //     $rItemRows = $rItemResult->num_rows();
            //     if ($rItemRows > 0) {
            //         $rItemData = $rItemResult->result();
            //         for ($r = 0; $r < count($rItemData); ++$r) {
            //             $rItem_pcode = $rItemData[$r]->product_code;
            //             $rItem_qty = $rItemData[$r]->qty;
						//
            //             $productData = $this->Constant_model->getDataOneColumn('products', 'code', $rItem_pcode);
            //             $rItem_pname = $productData[0]->name;
						//
            //             array_push($pcodeArray, $rItem_pcode);
            //             array_push($pnameArray, $rItem_pname);
            //             array_push($qtyArray, $rItem_qty);
						//
            //             unset($rItem_pcode);
            //             unset($rItem_qty);
            //             unset($rItem_pname);
            //         }
            //         unset($rItemData);
            //     }
            //     unset($rItemResult);
            //     unset($rItemRows);
            // }

            $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "$pcode");
            $objPHPExcel->getActiveSheet()->setCellValue("B$jj", "$namalengkap");
            $objPHPExcel->getActiveSheet()->setCellValue("C$jj", "$satuan");
						$objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$hargad");
						$objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$hargac");

            // if (count($pcodeArray) > 0) {
            //     $f_pcode = '';
            //     $f_pcode = $pnameArray[0].' ['.$pcodeArray[0].']';
            //     $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$f_pcode");
            // } else {
            //     $objPHPExcel->getActiveSheet()->setCellValue("D$jj", '');
            // }

            // if (count($qtyArray) > 0) {
            //     $f_qty = '';
            //     $f_qty = $qtyArray[0];
            //     $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$f_qty");
            // } else {
            //     $objPHPExcel->getActiveSheet()->setCellValue("E$jj", '');
            // }

            // $objPHPExcel->getActiveSheet()->setCellValue("F$jj", "$total_item_qty");
            // $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "$subTotal");
            // $objPHPExcel->getActiveSheet()->setCellValue("H$jj", "$gstTotal");
            // $objPHPExcel->getActiveSheet()->setCellValue("I$jj", "$grandTotal");

            $objPHPExcel->getActiveSheet()->getStyle("A$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("B$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("C$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("D$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("E$jj")->applyFromArray($account_value_style_header);
            // $objPHPExcel->getActiveSheet()->getStyle("F$jj")->applyFromArray($account_value_style_header);
            // $objPHPExcel->getActiveSheet()->getStyle("G$jj")->applyFromArray($account_value_style_header);
            // $objPHPExcel->getActiveSheet()->getStyle("H$jj")->applyFromArray($account_value_style_header);
            // $objPHPExcel->getActiveSheet()->getStyle("I$jj")->applyFromArray($account_value_style_header);

            ++$jj;

            // if (count($pcodeArray) > 1) {
            //     for ($p = 1; $p < count($pcodeArray); ++$p) {
            //         $s_pcode = '';
            //         $s_qty = '';
						//
            //         $s_pcode = $pnameArray[$p].' ['.$pcodeArray[$p].']';
            //         $s_qty = $qtyArray[$p];
						//
            //         $objPHPExcel->getActiveSheet()->setCellValue("A$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("B$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("C$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$s_pcode");
            //         $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$s_qty");
            //         $objPHPExcel->getActiveSheet()->setCellValue("F$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("G$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("H$jj", '');
            //         $objPHPExcel->getActiveSheet()->setCellValue("I$jj", '');
						//
            //         $objPHPExcel->getActiveSheet()->getStyle("A$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("B$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("C$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("D$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("E$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("F$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("G$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("H$jj")->applyFromArray($account_value_style_header);
            //         $objPHPExcel->getActiveSheet()->getStyle("I$jj")->applyFromArray($account_value_style_header);
						//
            //         ++$jj;
            //     }
            // }

            unset($order_id);
            unset($ordered_dtm);
            unset($subTotal);
            unset($gstTotal);
            unset($grandTotal);
            unset($total_item_qty);
        }
        unset($orderResult);
        unset($orderData);

        // $total_sub_amt = number_format($total_sub_amt, 2, '.', '');
        // $total_tax_amt = number_format($total_tax_amt, 2, '.', '');
        // $total_grand_amt = number_format($total_grand_amt, 2, '.', '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A$jj:F$jj");
        // $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "TOTAL");
        // $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "TES");
        // $objPHPExcel->getActiveSheet()->setCellValue("H$jj", "TES");
        // $objPHPExcel->getActiveSheet()->setCellValue("I$jj", "TES");

        $objPHPExcel->getActiveSheet()->getStyle("A$jj")->applyFromArray($text_align_style);
        $objPHPExcel->getActiveSheet()->getStyle("B$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E$jj")->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle("F$jj")->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle("G$jj")->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle("H$jj")->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->getStyle("I$jj")->applyFromArray($style_header);

        $objPHPExcel->getActiveSheet()->getRowDimension("$jj")->setRowHeight(30);
				// print_r($orderData);

				// return $orderData;
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Contract Supplier No : '.$notransaksi.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');


    }



	function verified_system($id,$passw)
	{
		//print_r($_POST);die();
		//$id = trim($this->input->post('kode'));
    	//$passw = md5(trim($this->input->post('nama')));
    	$result = $this->Loginmodel->loginquery($id,$passw);
    	$number = $this->Loginmodel->num_user($id,$passw);

    	if($number==1){
			$main = $this->Loginmodel->findTglGudang();
    	 	//$this->db->update('user', array('Active' =>'Y'), array('Id' => $result->Id));
    	 	$last_page = $this->session->userdata('last_page');
    	 	$last_page2 = $this->nativesession->get('last_page');
    	 	$sessiondata = array(
                   'username'  => $id,
                   'userlevel' => $result->UserLevel,
                   'userid'    => $result->Id,
				   'Tanggal_Trans' => $main->TglTrans,
				   'bulanaktif' => $result->Bulan,
				   'tahunaktif' => $result->Tahun,
				   'last_page' => ''
               );
			$this->session->set_userdata($sessiondata);

			$this->nativesession->set('username', $id);
			$this->nativesession->set('userlevel',$result->UserLevel);
			$this->nativesession->delete('last_page');

			$main = $this->Loginmodel->findAddress($result->MainPage);
			$address = explode("/",$main->url);
			$str = "";
			for($s =1;$s<count($address);$s++)
			{
				$str = $str."/".$address[$s];
			}
			$date = date("Y-m-d H:i:s");
			$data = array
			(
				"IDUser"    => $result->Id,
				"DateLogin" => $date
			);

			$this->db->insert("log_user",$data);
			//echo $str;die();
			if($last_page!='')
				redirect($last_page);
			elseif($last_page2!='')
				redirect($last_page2);
			else
				redirect($str);
		}
		else{
		    $result = $this->Loginmodel->findNamaLogo();
			$data['namaPT'] = $result->NamaPT;
			$data['logoPT'] = $result->Logo;
		 	$data['id']="";
			$data['msg'] =  "<b>User Name Atau Password Salah</b>";
			$this->load->view("login",$data);
		}
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>
