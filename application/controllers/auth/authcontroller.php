<?php
Class Authcontroller extends CI_Controller {
		
	var $companynamedbs;
	
    function __construct() {
        parent::__construct();
        //native session
        $this->load->library('nativesession');
        
		$dbs		= $this->config->item('dbs');
		$i			= $this->session->userdata('CompanyID');
		$this->companynamedbs = $dbs[$i]['dbscfg'];
		$this->load->database($dbs[$i]['settingdb'], FALSE, TRUE);
		$this->session->sess_update();
    }
    
    function index(){
    	$data['corporate'] = $this->authmodel->GetCorp();
		$UserName		= $this->session->userdata('UserName');
		$UserPwd		= $this->session->userdata('UserPwd');
		$datauserlogin	= $this->authmodel->getValidUser($UserName,$UserPwd);
		
        if (!empty($datauserlogin)){
			$this->session->set_userdata($datauserlogin[0]);
			$userid		= $this->session->userdata('userid');
			
			//native
			$this->nativesession->set('username', $this->session->userdata('username'));
			$this->nativesession->set('userlevel',$this->session->userdata('userlevel'));
			$this->nativesession->set('database',$this->session->userdata('database'));
			$this->nativesession->set('ses_login',$this->session->userdata('active'));
			$this->nativesession->delete('last_page');
			
			$this->_insertLastLogin($userid);			
			$defaultpage = $this->_getDefaultPage($userid);
			redirect($defaultpage);
        }
        else {
			$dataunset = array('NaturaSession' => FALSE, 'LogedIn' => FALSE);
			$this->session->unset_userdata($dataunset);
			//hapus session
			$this->session->sess_destroy();
			$data['msg']		= 'salah username/password !!!';
			$data['company']	= $this->config->item('dbs');
			$this->load->view('login',$data);
        }
	}

	function _insertLastLogin($userid){
		$this->load->helper('date');
		$date = date("Y-m-d H:i:s");
		$now = standard_date($fmt = 'DATETIME', now());
		$remote_address	= $_SERVER['REMOTE_ADDR'];
		$this->db->where('IDUser',$userid);
		$this->db->update('log_user',array('DateLogin' => $date));
	}

	function _getDefaultPage($userid){
		$sql = "SELECT MainPage AS DefaultPage FROM user WHERE Id=?";
		$query = $this->db->query($sql,array($userid));
		$result = $query->result_array();
		return $result[0]['DefaultPage'];
	}
	
}   
?>
