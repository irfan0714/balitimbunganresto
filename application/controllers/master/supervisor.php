<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Supervisor extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/supervisormodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/supervisor/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/supervisor/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/supervisor/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->supervisormodel->num_supervisor_row($id,$with);
	        $data['supervisor_data']  = $this->supervisormodel->get_supervisor_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/supervisor/supervisor_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	//$data['tipe']  = $this->supervisormodel->getTipe();
	     	$data['niltipe'] = "";
			//$data['personal']  = $this->supervisormodel->getPersonal();
			$data['nilsupervisor'] = "";
			$data['supervisor']  = $this->supervisormodel->getSupervisor();
	     	$data['nilpersonal'] = "";
			//$data['gudang']  = $this->supervisormodel->getGudang();
	     	$data['nilgudang'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supervisor/add_supervisor',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_supervisor($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_supervisor'] = $this->supervisormodel->getDetail($id);
	    	$data['supervisor']  = $this->supervisormodel->getSupervisor();
	    	//$data['tipe'] 	  = $this->supervisormodel->getTipe();
			//$data['personal']  = $this->supervisormodel->getPersonal();
			//$data['gudang']	  = $this->supervisormodel->getGudang();
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supervisor/view_edit_supervisor', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_supervisor($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_supervisor'] = $this->supervisormodel->getDetail($id);
			$data['cekAda'] = $this->supervisormodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supervisor/delete_supervisor', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('supervisor', array('KdSupervisor' => $id));
		redirect('/master/supervisor/');
	}

    function edit_supervisor($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_supervisor'] = $this->supervisormodel->getDetail($id);
	    	//$data['tipe'] 	  = $this->supervisormodel->getTipe();
			//$data['personal']  = $this->supervisormodel->getPersonal();
			//$data['gudang']	  = $this->supervisormodel->getGudang();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supervisor/view_edit_supervisor', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_supervisor(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$KdSupervisor	 = $this->input->post('kode');
    	$Namasupervisor 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$Status 	 = strtoupper(addslashes(trim($this->input->post('status'))));
		$user = $this->session->userdata('username');
    	$data = array(
    		  'NamaSupervisor'	=> $Namasupervisor ,
			  'Status' => $Status,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('supervisor', $data, array('KdSupervisor' => $KdSupervisor));
    	redirect('/master/supervisor/');
    }
    
    
    function save_new_supervisor(){
    	
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$personal = strtoupper(trim($this->input->post('personal')));
		$gudang = strtoupper(trim($this->input->post('gudang')));
    	$num 	 = $this->supervisormodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'Kdsupervisor'   => $id,
               'Namasupervisor' => $nama,
			   'KdPersonal' => $personal,
               'KdTipesupervisor' => $tipe,
			   'KdGudang' => $gudang,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('supervisor', $data);
			redirect('master/supervisor');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilpersonal'] = $personal;
			$data['personal']  = $this->supervisormodel->getPersonal();
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->supervisormodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/supervisor/add_supervisor', $data);
		}
	}
	
	function save_new_supervisor_new(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
		//$KdSupervisor	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$NamaSupervisor 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$Status 	 = strtoupper(addslashes(trim($this->input->post('status'))));
    	$num 	 = $this->supervisormodel->get_id($NamaSupervisor);
    	
    	$idnama  = substr($NamaSupervisor,0,2);
		$id      = $idnama.substr("00000".$this->supervisormodel->getidcounter($idnama)->Counter,-4);
		
    	$user = $this->session->userdata('username');
    	if($num == 0)
		{
			
		 	$data = array(
		 	   'KdSupervisor'=> $id,              
               'NamaSupervisor' => $NamaSupervisor,
               'Status'		=> $Status,
               'AddDate' 	=> date('Y-m-d'),
               'AddUser' 	=> $user
            );
            $this->db->insert('supervisor', $data);
			redirect('master/supervisor');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilsupervisor'] = $supervisor;
			$data['supervisor']  = $this->supervisormodel->getSupervisor();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/supervisor/add_supervisor', $data);
		}
	}
	
}
?>