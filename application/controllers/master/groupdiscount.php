<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class groupdiscount extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/groupdiscountmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/groupdiscount/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/groupdiscount/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/groupdiscount/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->groupdiscountmodel->num_groupdiscount_row($id,$with);
	        $data['groupdiscount_data']  = $this->groupdiscountmodel->get_groupdiscount_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/groupdiscount/groupdiscount_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	$data['tipe']  = $this->groupdiscountmodel->getTipe();
	     	$data['niltipe01'] = "";
			$data['niltipe02'] = "";
			$data['niltipe03'] = "";
			$data['niltipe04'] = "";
			$data['niltipe05'] = "";
			$data['niltipe06'] = "";
			$data['niltipe07'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupdiscount/add_groupdiscount',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_groupdiscount($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_groupdiscount'] = $this->groupdiscountmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->groupdiscountmodel->getTipe();
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupdiscount/view_edit_groupdiscount', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_groupdiscount($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_groupdiscount'] = $this->groupdiscountmodel->getDetail($id);
			$data['cekAda'] = $this->groupdiscountmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupdiscount/delete_groupdiscount', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('groupdiscount', array('KdGroupDiscount' => $id));
		redirect('/master/groupdiscount/');
	}

    function edit_groupdiscount($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_groupdiscount'] = $this->groupdiscountmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->groupdiscountmodel->getTipe();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupdiscount/view_edit_groupdiscount', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_groupdiscount(){
    	$id 	 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe01 = strtoupper(trim($this->input->post('tipe01')));
		$tipe02 = strtoupper(trim($this->input->post('tipe02')));
		$tipe03 = strtoupper(trim($this->input->post('tipe03')));
		$tipe04 = strtoupper(trim($this->input->post('tipe04')));
		$tipe05 = strtoupper(trim($this->input->post('tipe05')));
		$tipe06 = strtoupper(trim($this->input->post('tipe06')));
		$tipe07 = strtoupper(trim($this->input->post('tipe07')));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaGroupDiscount'	=> $nama,
    		  'Ketentuan01' => $tipe01,
			  'Ketentuan02' => $tipe02,
			  'Ketentuan03' => $tipe03,
			  'Ketentuan04' => $tipe04,
			  'Ketentuan05' => $tipe05,
			  'Ketentuan06' => $tipe06,
			  'Ketentuan07' => $tipe07,
              'EditDate'	=> $tgltrans
			);
		$this->db->update('groupdiscount', $data, array('KdGroupDiscount' => $id));
    	redirect('/master/groupdiscount/');
    }
    function save_new_groupdiscount(){
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe01 = strtoupper(trim($this->input->post('tipe01')));
		$tipe02 = strtoupper(trim($this->input->post('tipe02')));
		$tipe03 = strtoupper(trim($this->input->post('tipe03')));
		$tipe04 = strtoupper(trim($this->input->post('tipe04')));
		$tipe05 = strtoupper(trim($this->input->post('tipe05')));
		$tipe06 = strtoupper(trim($this->input->post('tipe06')));
		$tipe07 = strtoupper(trim($this->input->post('tipe07')));
    	$num 	 = $this->groupdiscountmodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdGroupDiscount'   => $id ,
               'NamaGroupDiscount' => $nama ,
               'Ketentuan01' => $tipe01,
			   'Ketentuan02' => $tipe02,
			   'Ketentuan03' => $tipe03,
			   'Ketentuan04' => $tipe04,
			   'Ketentuan05' => $tipe05,
			   'Ketentuan06' => $tipe06,
			   'Ketentuan07' => $tipe07,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('groupdiscount', $data);
			redirect('master/groupdiscount');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['niltipe01'] = $tipe01;
			$data['niltipe02'] = $tipe02;
			$data['niltipe03'] = $tipe03;
			$data['niltipe04'] = $tipe04;
			$data['niltipe05'] = $tipe05;
			$data['niltipe06'] = $tipe06;
			$data['niltipe07'] = $tipe07;
			$data['tipe']  = $this->groupdiscountmodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/groupdiscount/add_groupdiscount', $data);
		}
	}
}
?>