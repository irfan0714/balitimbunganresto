<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tipe_personal extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/tipe_personalmodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $sign = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/tipe_personal/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/tipe_personal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/tipe_personal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']     = $this->tipe_personalmodel->num_tipe_personal_row($id,$with);
	        $data['tipe_personaldata']       = $this->tipe_personalmodel->get_tipe_personal_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/tipe_personal/tipe_personal_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  = "";
	     	$data['id']   = "";
	     	$data['nama'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/tipe_personal/add_tipe_personal',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function view_tipe_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
     		$id 				= $this->uri->segment(4);
	    	$data['tipe_personal_data'] = $this->tipe_personalmodel->getDetail($id);
	    	$data['edit'] 		= false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/tipe_personal/view_edit_tipe_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_tipe_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['tipe_personal_data'] = $this->tipe_personalmodel->getDetail($id);
			$data['cekAda'] = $this->tipe_personalmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/tipe_personal/delete_tipe_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('tipe_personal', array('KdTipePersonal' => $id));
		redirect('/master/tipe_personal/');
	}
    
    function edit_tipe_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
	    	$data['tipe_personal_data'] = $this->tipe_personalmodel->getDetail($id);
	    	$data['edit'] 		= true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/tipe_personal/view_edit_tipe_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_tipe_personal(){
    	$id   = $this->input->post('kode');
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaTipePersonal'	=> $nama,
              'EditDate'	=> $tgltrans
			);
		$this->db->update('tipe_personal', $data, array('KdTipePersonal' => $id));
    	redirect('/master/tipe_personal/');
    }
    function save_new_tipe_personal(){
		$id   = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$num  = $this->tipe_personalmodel->get_id($id);
    	if($num!=0){
			$data['id']   = $this->input->post('kode');
			$data['nama'] = $this->input->post('nama');
			$data['msg']  = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/tipe_personal/add_tipe_personal', $data);
		}
		else{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdTipePersonal' => $id ,
               'NamaTipePersonal' => $nama ,
               'AddDate'	=> $tgltrans
            );
            $this->db->insert('tipe_personal', $data);
			redirect('/master/tipe_personal/');
		}
	}
}
?>