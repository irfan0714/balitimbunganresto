<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Master_barang extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('master/master_barang_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        //print_r($sign);die;
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $data["search_divisi"] = "";
            $data["search_kategori"] = "";
            $data["search_urut"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "master_barang", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_divisi = explode("=", $arrSearch[1]); // search divisi
                    $arr_search["search"]["divisi"] = $search_divisi[1];
                    $search_kategori = explode("=", $arrSearch[2]); // search kategori
                    $arr_search["search"]["kategori"] = $search_kategori[1];
                    $search_urut = explode("=", $arrSearch[3]); // search urut
                    $arr_search["search"]["urut"] = $search_urut[1];
                    $search_status = explode("=", $arrSearch[4]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_divisi"] = $search_divisi[1];
                    $data["search_kategori"] = $search_kategori[1];
                    $data["search_urut"] = $search_urut[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = 100;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/master/master_barang/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/master/master_barang/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
            $config['total_rows'] = $this->master_barang_model->num_master_barang_row($arr_search["search"]);
            $data['data'] = $this->master_barang_model->getMasterBarangList($config['per_page'], $page, $arr_search["search"]);
			$data['kategori'] = $this->master_barang_model->getKategori();
			$data['divisi'] = $this->master_barang_model->getDivisi();
			
            $data['track'] = $mylib->print_track();
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('master/master_barang/master_barang_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'master_barang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_divisi=".$this->input->post('search_divisi');
		$search_value .= "&search_kategori=".$this->input->post('search_kategori');
		$search_value .= "&search_urut=".$this->input->post('search_urut');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "master_barang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/master/master_barang/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        $otorisasi = $mylib->menu_validation("masterbarang");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            $data['otorisasi'] = $otorisasi;
          
			$data['divisi'] = $this->master_barang_model->getDivisi();
			$data['kategori'] = $this->master_barang_model->getKategori();
			$data['subkategoripos'] = $this->master_barang_model->getSubkategoriPos();
            $data['satuan'] = $this->master_barang_model->getSatuan();
            $data['kdgroupbarang'] = $this->master_barang_model->getGroupBarang();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('master/master_barang/add_master_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_data($pcode) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
		$otorisasi = $mylib->menu_validation("masterbarang");
        $hak_approve = $mylib->hak_approve_reject("masterbarang");
        $user_approve_1 = $this->master_barang_model->level_approve('masterbarang_level_1');
		$user_approve_2 = $this->master_barang_model->level_approve('masterbarang_level_2');

		$arrEmail = $this->globalmodel->function_email("masterbarang");
		
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
			$data['divisi'] = $this->master_barang_model->getDivisi();
			$data['kategori'] = $this->master_barang_model->getKategori();
			$data['subkategoripos'] = $this->master_barang_model->getSubkategoriPos();
            $data['satuan'] = $this->master_barang_model->getSatuan();
            
            $data['detail'] = $this->master_barang_model->getDetailBarang($pcode);
			$data['otorisasi'] = $otorisasi;
            $data['hak_approve'] = $hak_approve;
            $data['user_approve_1'] = $user_approve_1;
			$data['user_approve_2'] = $user_approve_2;
			
			$to_name="";
			for($x=0;$x<=(count($arrEmail)-1);$x++)
            {
				if($x==(count($arrEmail)-1)){
				$tanda =".";
				}else{
				$tanda =" atau ";
				}		
                $to_name.= $arrEmail[$x]["employee_name"].$tanda;
			}
			$data['ke'] = $to_name;
			
			$data['rekpersediaan'] = $this->master_barang_model->getRekPersediaan($pcode);
			$data['rekhpp'] = $this->master_barang_model->getRekHpp($pcode);
			$data['rekpenjualan'] = $this->master_barang_model->getRekPenjualan($pcode);
			$data['rekreturn'] = $this->master_barang_model->getRekReturn($pcode);
			$data['rekdisc'] = $this->master_barang_model->getRekDisc($pcode);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('master/master_barang/edit_master_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function save_data(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		
		$mylib = new globallib();
		$user = $this->session->userdata('username');
		
        $v_Auto = $this->input->post('v_Auto');
        $v_KdBarang = $this->input->post('v_PCode');
        $v_NamaLengkap = $this->input->post('v_NamaLengkap');
        $v_Barcodes = $this->input->post('v_Barcode1');
        $v_KdDivisi = $this->input->post('v_KdDivisi');
		$v_KdSubDivisi = $this->input->post('v_KdSubDivisi');
		$v_KdKategori = $this->input->post('v_KdKategori');
        $v_KdSubKategori = $this->input->post('v_KdSubKategori');
        $v_KdGroupBarang = $this->input->post('v_groupbarang');
		$v_SatuanSt = $this->input->post('v_SatuanSt');
        $v_Status = $this->input->post('v_Status');
        
		$v_Stock = $this->input->post('v_Stock');
		$v_Pembelian = $this->input->post('v_Pembelian');
		$v_penjualan_pos= $this->input->post('v_penjualan_pos');
		$v_penjualan_pos_touch = $this->input->post('v_penjualan_pos_touch');
        $v_subkategori_pos_touch = $this->input->post('v_subkategori_pos_touch');		
        
        
        $v_Harga1c = $this->input->post('v_hidden_harga1c');
		$v_PPN = $this->input->post('v_PPN');
		$v_Service_charge = $this->input->post('v_Service_charge');
        $v_komisi = $this->input->post('v_komisi');
        $v_DiscInternal = $this->input->post('v_DiscInternal');
        $v_KdRekening1 = $this->input->post('v_KdRekening1'); //rek biasa persediaan
        $v_KdRekening2 = $this->input->post('v_KdRekening2'); //rek HPP
        $v_KdRekening3 = $this->input->post('v_KdRekening3'); //rek penjualan
        $v_KdRekening4 = $this->input->post('v_KdRekening4'); //rek return
        $v_KdRekening5 = $this->input->post('v_KdRekening5'); //rek disc
		$v_Konsinyasi = $this->input->post('v_Konsinyasi');
		
		$v_otorisasi= $this->input->post('v_otorisasi');
		$v_del= $this->input->post('v_del');
        $v_approve= $this->input->post('v_approve');
		$v_approve_2= $this->input->post('v_approve_2');
        $v_reject= $this->input->post('v_reject');
		$v_reject_2= $this->input->post('v_reject_2');
        $v_kirim= $this->input->post('v_kirim');
        $v_Approval_Remarks= $this->input->post('v_Approval_Remarks');
		$v_Approval_Remarks_2= $this->input->post('v_Approval_Remarks_2');
						
        $flag = $this->input->post('flag');
        
       //jika auto
      
   		if($v_Auto=="Y"){
	   	  $v_PCode = $this->get_code_PCode($v_KdSubDivisi, $v_KdKategori, $v_KdSubKategori);
	   	  $v_Barcode1 = $v_PCode;
	   	}else{
	   	  $v_PCode = $v_KdBarang;
	   	  $v_Barcode1 = $v_Barcodes;
	   	}
						   
        
        if($flag=="add"){
        	//insert masterbarang mutlak
		   $data=array(
						  'PCode' 				=> $v_PCode,
                          'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
                          'NamaStruk' 			=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
                          'NamaInitial' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
                          'SatuanSt' 			=> $v_SatuanSt,
                          'harga1c' 			=> $v_Harga1c,
                          'Barcode1' 			=> $v_Barcode1,
                          'KdDivisi' 			=> $v_KdDivisi,
                          'KdSubDivisi' 		=> $v_KdSubDivisi,
                          'KdKategori' 			=> $v_KdKategori,
                          'KdSubKategori' 		=> $v_KdSubKategori,
                          'KdGroupBarang'		=> $v_KdGroupBarang,
                          'Status' 				=> $v_Status,
                          'AddDate' 			=> date('Y-m-d h:i:s'),
                          'Service_charge' 		=> $v_Service_charge,
                          'KdRekeningPersediaan'=> $v_KdRekening1,
                          'KdRekeningHpp' 		=> $v_KdRekening2,
                          'KdRekeningPenjualan' => $v_KdRekening3,
                          'KdRekeningReturn' 	=> $v_KdRekening4,
                          'KdRekeningDisc' 		=> $v_KdRekening5,
                          'FlagStock' 			=> $v_Stock,
                          'FlagPembelian' 		=> $v_Pembelian,
                          'FlagPenjualanPOS' 	=> $v_penjualan_pos,
                          'FlagPenjualanPOSTouch'=> $v_penjualan_pos_touch,
                          'SubKategoriPOSTouch' => $v_subkategori_pos_touch,
                          'PPN' 				=> $v_PPN,
                          'komisi' 				=> $v_komisi,
                          'DiscInternal' 		=> $v_DiscInternal,
                          'AddUser' 			=> $user
		   			   );
		   	$this->db->insert('masterbarang',$data);
			
		}else if($flag=="edit"){
		    $v_PCode = $v_KdBarang;
		    $v_Barcode1 = $v_Barcodes;
		    //update masterbarang selain finance
			//masterbarang
			$datax1=array(
						    'NamaLengkap' => preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap),
                            'Barcode1' => $v_Barcode1,
                            'SatuanSt' => $v_SatuanSt,
                            'Status'   => $v_Status,
                            'FlagStock' => $v_Stock,
	                        'FlagPembelian' => $v_Pembelian,
	                        'FlagPenjualanPOS' 	=> $v_penjualan_pos,
	                        'FlagPenjualanPOSTouch'=> $v_penjualan_pos_touch,
	                        'SubKategoriPOSTouch' => $v_subkategori_pos_touch,
                            'EditDate' => date('Y-m-d h:i:s'),
                            'EditUser' => $user,
                            'Approval_By' => $this->master_barang_model->level_approve('masterbarang_level_1'),
                            'Approval_By_2' => $this->master_barang_model->level_approve('masterbarang_level_2')
						 );
			$this->db->update('masterbarang',$datax1,array('PCode'=>$v_PCode));
				
				
			//yang boleh update finance
			if($v_otorisasi=="Y"){
			//update finance masterbarang
							//masterbarang
							$datak1=array(
											'Harga1c' => $v_Harga1c,
			                                'komisi' => $v_komisi,
			                                'DiscInternal' => $v_DiscInternal,
			                                'PPN' => $v_PPN,
			                                'Service_charge' => $v_Service_charge,
			                                'EditDate' => date('Y-m-d h:i:s'),
			                                'EditUser' => $user,
			                                'KdRekeningPersediaan'=> $v_KdRekening1,
					                        'KdRekeningHpp' 		=> $v_KdRekening2,
					                        'KdRekeningPenjualan' => $v_KdRekening3,
					                        'KdRekeningReturn' 	=> $v_KdRekening4,
					                        'KdRekeningDisc' 		=> $v_KdRekening5,
			                                'Konsinyasi'=>$v_Konsinyasi
										 );
							$this->db->update('masterbarang',$datak1,array('PCode'=>$v_PCode));	
				
							//jika approve status sudah 1
							
							$cek_sudah_diapprove = $this->master_barang_model->getSdhApprove($v_PCode);
							
							if($cek_sudah_diapprove->Approval_Status_2 == 1){	
													
									if($v_penjualan_pos=="Y"){
								               /*$data_updt1 = array(
						                                  'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
						                                  'harga1c' 			=> $v_Harga1c,
						                                  'Barcode1' 			=> $v_Barcode1,
						                                  'Service_charge' 		=> $v_Service_charge,
						                                  'komisi' 				=> $v_komisi,
						                                  'PPN' 				=> $v_PPN,
						                                  'KdKategori' 			=> $v_KdKategori,
						                                  'KdSubKategori' 		=> $v_KdSubKategori,
						                                  'Satuan1' 			=> $v_SatuanSt,
						                                  'DiscInternal' 		=> $v_DiscInternal
						        					  );
						        					  
						        		       $this->db->update('masterbarang_pos',$data_updt1,array('PCode'=>$v_PCode));*/
						        		       
						        		       $this->db->delete('masterbarang_pos',array('PCode'=>$v_PCode));
						        		       $datax = array(
						        						  'PCode' 				=> $v_PCode,
						                                  'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
						                                  'harga1c' 			=> $v_Harga1c,
						                                  'Barcode1' 			=> $v_Barcode1,
						                                  'Service_charge' 		=> $v_Service_charge,
						                                  'komisi' 				=> $v_komisi,
						                                  'PPN' 				=> $v_PPN,
						                                  'KdKategori' 			=> $v_KdKategori,
						                                  'KdSubKategori' 		=> $v_KdSubKategori,
						                                  'Satuan1' 			=> $v_SatuanSt,
						                                  'DiscInternal' 		=> $v_DiscInternal
						        					  );
						        					  
						        		       $this->db->insert('masterbarang_pos',$datax);
				        		       }
				        		       
				        		       
				        		       if($v_penjualan_pos_touch=="Y"){
						        		       /*$data_updt2 = array(
						                                  'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
						                                  'harga1c' 			=> $v_Harga1c,
						                                  'Barcode1' 			=> $v_Barcode1,
						                                  'Service_charge' 		=> $v_Service_charge,
						                                  'komisi' 				=> $v_komisi,
						                                  'Jenis' 				=> '2',
						                                  'PPN' 				=> $v_PPN,
						                                  'KdKategori' 			=> $v_KdKategori,
						                                  'KdSubKategori' 		=> $v_KdSubKategori,
						                                  'Satuan1' 			=> $v_SatuanSt,
						                                  'FlagReady' 			=> 'Y',
						                                  'FlagStock' 			=> $v_Stock,
						                                  'Printer'				=> '01',
						                                  'DiscInternal' 		=> $v_DiscInternal
						        					  );
						        					  
						        				$this->db->update('masterbarang_touch',$data_updt2,array('PCode'=>$v_PCode));*/
						        				$this->db->delete('masterbarang_touch',array('PCode'=>$v_PCode));
						        		        $datak = array(
						        						  'PCode' 				=> $v_PCode,
						                                  'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
						                                  'harga1c' 			=> $v_Harga1c,
						                                  'Barcode1' 			=> $v_Barcode1,
						                                  'Service_charge' 		=> $v_Service_charge,
						                                  'komisi' 				=> $v_komisi,
						                                  'Jenis' 				=> '2',
						                                  'PPN' 				=> $v_PPN,
						                                  'KdKategori' 			=> $this->master_barang_model->getDataKategoriTouch($v_subkategori_pos_touch),
						                                  'KdSubKategori' 		=> $v_subkategori_pos_touch,
						                                  'Satuan1' 			=> $v_SatuanSt,
						                                  'FlagReady' 			=> 'Y',
						                                  'FlagStock' 			=> $v_Stock,
						                                  'Printer'				=> '01',
						                                  'DiscInternal' 		=> $v_DiscInternal
						        					  );
						        					  
						        		$this->db->insert('masterbarang_touch',$datak);
				        		} 
		        		
		        		}     
			}
			
			//request kirim masterbarang baru untuk di approve
			if($v_kirim=="1"){
		    //masterbarang kirim
		        $dataf=array(
                                'status_mail' => 1
							 );
				$this->db->update('masterbarang',$dataf,array('PCode'=>$v_PCode));
				
		        //emails
		        $subject = "Request Approval Master Barang Baru PCode ".$v_PCode;
                $author  = "Auto System";
                               
                $url = base_url()."index.php/master/master_barang/edit_data/".$v_PCode;

                $aprv_level_1 = $this->db->get_where('otorisasi_user',array('Tipe'=>'masterbarang_level_1'))->row();

                $arrEmail = $this->master_barang_model->getEmail('masterbarang',$aprv_level_1->UserName);
				
                //$to = "eno@vci.co.id;trisno@secretgarden.co.id;";
                $to = $arrEmail->email_address.";";
                $to_name = $arrEmail->email_name.";";
                                 
                $body  = "Dear Bapak ".$arrEmail->email_name.", <br><br>";
                $body .= "Mohon lakukan approval untuk master barang baru dengan PCode ".$v_PCode."<br><br>";
                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
				
				$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		    }	
		
		//jika approve maka insert ke pos dan touch
		if($v_approve=="1"){
		//masterbarang approve_by level 1
            $datag=array(
    			        'Approval_By' => $user,
                        'Approval_Date' => date('Y-m-d h:i:s'),
                        'Approval_Status' => '1'
    			       );
    	    $this->db->update('masterbarang',$datag,array('PCode'=>$v_PCode));
    	   

            $subject = "Request Approval Master Barang Baru PCode ".$v_PCode;
            $author  = "Auto System";
                           
            $url = base_url()."index.php/master/master_barang/edit_data/".$v_PCode;

            $aprv_level_2 = $this->db->get_where('otorisasi_user',array('Tipe'=>'masterbarang_level_2'))->row();

            $arrEmail = $this->master_barang_model->getEmail('masterbarang',$aprv_level_2->UserName);
            
            //$to = "eno@vci.co.id;trisno@secretgarden.co.id;";
            $to = $arrEmail->email_address.";";
            $to_name = $arrEmail->email_name.";";
                             
            $body  = "Dear Bapak ".$arrEmail->email_name.", <br><br>";
            $body .= "Mohon lakukan approval untuk master barang baru dengan PCode ".$v_PCode."<br><br>";
            $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
            
            $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
        }

        if($v_approve_2=="1"){
        //masterbarang approve_by level 2
            $datag=array(
                        'Approval_By_2' => $user,
                        'Approval_Date_2' => date('Y-m-d h:i:s'),
                        'Approval_Status_2' => '1',
                        'Status' => 'A'
                       );
            $this->db->update('masterbarang',$datag,array('PCode'=>$v_PCode));
            
            //insert masterbarang_pos jika Y
                if($v_penjualan_pos=="Y"){
                    $this->db->delete('masterbarang_pos',array('PCode'=>$v_PCode));
                    $datax = array(
                                      'PCode'               => $v_PCode,
                                      'NamaLengkap'         => strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
                                      'harga1c'             => $v_Harga1c,
                                      'Barcode1'            => $v_Barcode1,
                                      'Service_charge'      => $v_Service_charge,
                                      'komisi'              => $v_komisi,
                                      'PPN'                 => $v_PPN,
                                      'KdKategori'          => $v_KdKategori,
                                      'KdSubKategori'       => $v_KdSubKategori,
                                      'Satuan1'             => $v_SatuanSt,
                                      'DiscInternal'        => $v_DiscInternal
                                  );
                                  
                    $this->db->insert('masterbarang_pos',$datax);
                   
                }
                
                //insert masterbarang_pos_touch jika Y
                if($v_penjualan_pos_touch=="Y"){
                    $this->db->delete('masterbarang_touch',array('PCode'=>$v_PCode));
                    $datak = array(
                                      'PCode'               => $v_PCode,
                                      'NamaLengkap'         => strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $v_NamaLengkap)),
                                      'harga1c'             => $v_Harga1c,
                                      'Barcode1'            => $v_Barcode1,
                                      'Service_charge'      => $v_Service_charge,
                                      'komisi'              => $v_komisi,
                                      'Jenis'               => '2',
                                      'PPN'                 => $v_PPN,
                                      'KdKategori'          => $v_KdKategori,
                                      'KdSubKategori'       => $v_subkategori_pos_touch,
                                      'Satuan1'             => $v_SatuanSt,
                                      'FlagReady'           => 'Y',
                                      'FlagStock'           => $v_Stock,
                                      'Printer'             => '01',
                                      'DiscInternal'        => $v_DiscInternal
                                  );
                                  
                    $this->db->insert('masterbarang_touch',$datak);
                    
                }
            
        }
		      
		      
		      //jika reject
		      if($v_reject=="1"){
		      //masterbarang approve_by
		        $datah=array(
					            'Approval_By' => $user,
                                'Approval_Date' => date('Y-m-d h:i:s'),
                                'Approval_Status' => '2',
                                'Status'=>'T',
                                'Approval_Remarks' => $v_Approval_Remarks
					       );
			    $this->db->update('masterbarang',$datah,array('PCode'=>$v_PCode));
		       }

               if($v_reject_2=="1"){
              //masterbarang approve_by
                $datah2=array(
                                'Approval_By_2' => $user,
                                'Approval_Date_2' => date('Y-m-d h:i:s'),
                                'Approval_Status_2' => '2',
                                'Status'=>'T',
                                'Approval_Remarks_2' => $v_Approval_Remarks_2
                           );
                $this->db->update('masterbarang',$datah2,array('PCode'=>$v_PCode));
               }
		       
		    if($v_del=="1"){
		     //pertama cek apakah barang ada di PB?
			$cek_pb = $this->master_barang_model->getCekPB($v_PCode);
			if(!empty($cek_pb)){
				$msg = " Tidak bisa dihapus karena sudah ada Permintaan Barang";
				echo "<script>alert('".$msg."');</script>";
				die();
			}
			
		    //kedua cek apakah barang transaksi detail?
		    $cek_transaksi = $this->master_barang_model->getCekTransaksi($v_PCode);
			if(!empty($cek_transaksi)){
				$msg = "Tidak bisa dihapus karena sudah ada Transaksi";
				echo "<script>alert('".$msg."');</script>";
				die();
			}
			
			  //ketiga hapus barang
			  //masterbarang
			  $this->db->delete('masterbarang',array('PCode'=>$v_PCode));
			  
			  //masterbarang_pos
			  $this->db->delete('masterbarang_pos',array('PCode'=>$v_PCode));
			  
			  //masterbarang_pos_touch
			  $this->db->delete('masterbarang_touch',array('PCode'=>$v_PCode));
			}
					
		}
		
		redirect('/master/master_barang/');
        
	}

	function getDataSubDivisi()
    {        
	     $divisi = $this->input->post('divisi');
	     $query = $this->master_barang_model->getDataSubDivisis($divisi);
	     
	     echo "
	     &nbsp;&nbsp;&nbsp;&nbsp;<b>SubDivisi</b>&nbsp;&nbsp;";?>
	     <!--<select name="v_KdSubDivisi" id="v_KdSubDivisi" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_subdivisi', this.value)">-->
	     <select name="v_KdSubDivisi" id="v_KdSubDivisi" class="form-control-new" style="width: 200px;">
	     <? echo " <option value=''> -- Pilih SubDivisi -- </option>
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[KdSubDivisi]>$cetak[NamaSubDivisi]</option>";
	      } 
	     echo "</select>";    
    }
    
    function getDataSubKategori()
    {        
	     $kategori = $this->input->post('kategori');
	     $query = $this->master_barang_model->getDataSubKategoris($kategori);
	     
	     echo "
	     &nbsp;&nbsp;&nbsp;&nbsp;<b>SubKategori</b>&nbsp;&nbsp;
	     <select name='v_KdSubKategori' id='v_KdSubKategori' class='form-control-new' style='width: 181px;'>
	     <option value=''> -- Pilih SubKategori-- </option>
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[KdSubKategori]>$cetak[NamaSubKategori]</option>";
	      } 
	     echo "</select>";    
    }
    
    function getDataSubKategoriPos()
    {        
	     $kategori = $this->input->post('kategori');
	     $query = $this->master_barang_model->getDataSubKategoriPos($kategori);
	     
	     echo "
	     &nbsp;&nbsp;&nbsp;&nbsp;<b>SubKategori</b>&nbsp;&nbsp;
	     <select name='v_KdSubKategori' id='v_KdSubKategori' class='form-control-new' style='width: 181px;'>
	     <option value=''> -- Pilih SubKategori -- </option>
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[KdSubKategori]>$cetak[NamaSubKategori]</option>";
	      } 
	     echo "</select>";    
    }
    
    function cek_duplicate_pcode(){
			 $pcode = $this->input->post('kdbrg');
			 
		     $query = $this->master_barang_model->getCekDuplicatePcode($pcode);
		     
		     if($query->PCode == $pcode){
			 	$hasil="1";
			 }else{
			 	$hasil="0";
			 }
			 
			 echo $hasil;
	}
	
	function cek_duplicate_barang(){
			 $barang = $this->input->post('brg');
			 
		     $query = $this->master_barang_model->getCekDuplicateBarang($barang);
		    
		     if($query->NamaLengkap == strtoupper($barang)){
			 	$hasil="1";
			 }else{
			 	$hasil="0";
			 }
			 
			 echo $hasil;
	}
	
	function cek_duplicate_barcode(){
			 $barcode = $this->input->post('brc');
			 
		     $query = $this->master_barang_model->getCekDuplicateBarcode($barcode);
		    
		     if($query->Barcode1 == strtoupper($barcode)){
			 	$hasil="1";
			 }else{
			 	$hasil="0";
			 }
			 
			 echo $hasil;
	}
    
    function getDataKategori()
    {        
	     $v_KdSubDivisi = $this->input->post('subdivisi');
	     
	     if($v_KdSubDivisi==13 || $v_KdSubDivisi==19 || $v_KdSubDivisi==20)
	        {
	            // resto
	            if($v_KdSubDivisi==19 || $v_KdSubDivisi==20)
	            {
	                $q = "
	                        SELECT
	                            subkategoripos.KdSubKategori,
	                            subkategoripos.NamaSubKategori
	                        FROM
	                            subkategoripos
	                        WHERE
	                            1
	                            AND LEFT(subkategoripos.KdSubKategori,3) = '010'
	                        ORDER BY                                                  
	                            subkategoripos.KdSubKategori ASC
	                ";
	                $qry = mysql_query($q);
	                while($row = mysql_fetch_array($qry))
	                {
	                    list($KdSubKategori, $NamaSubKategori) = $row;    
	                    
	                    $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
	                    $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
	                } 
	                if($v_KdSubDivisi==19){
	                ?>	
	                <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
	                    <option value="010">Foods</option>
	                    <option value="011">Beverages</option>
	                </select>
	                <?php
	                }else{
	                ?>
					<select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
	                    <option value="020">Foods</option>
	                    <option value="021">Beverages</option>
	                </select>
	                <?php
					}
					?>
	                <span id="td_KdSubKategori">
	                &nbsp;&nbsp;&nbsp;&nbsp;<b>SubKategori</b>&nbsp;&nbsp;
	                <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 181px;">  
	                    <?php 
	                        foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
	                        {
	                            $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
	                    ?>
	                    <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
	                    <?php 
	                        }
	                    ?>
	                </select> 
	                </span>  
	                <?php
	            }  
	            else if($v_KdSubDivisi==13)
	            {
	                $q = "
	                        SELECT
	                            subkategoripos.KdSubKategori,
	                            subkategoripos.NamaSubKategori
	                        FROM
	                            subkategoripos
	                        WHERE
	                            1
	                            AND LEFT(subkategoripos.KdSubKategori,3) = '031'
	                        ORDER BY                                                  
	                            subkategoripos.KdSubKategori ASC
	                ";
	                $qry = mysql_query($q);
	                while($row = mysql_fetch_array($qry))
	                {
	                    list($KdSubKategori, $NamaSubKategori) = $row;    
	                    
	                    $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
	                    $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
	                } 
	                
	                ?>
	                <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
	                    <option value="031">Black Eye</option>
	                </select>
	                
	                <span id="td_KdSubKategori">
	                &nbsp;&nbsp;&nbsp;&nbsp;<b>SubKategori</b>&nbsp;&nbsp;
	                <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 181px;">  
	                    <?php 
	                        foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
	                        {
	                            $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
	                    ?>
	                    <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
	                    <?php 
	                        }
	                    ?>
	                </select> 
	                </span>  
	                <?php
	            }   
	        }
	        else
	        {
	             $q = "
	                    SELECT
	                        kategori.KdKategori,
	                        kategori.NamaKategori
	                    FROM
	                        kategori
	                    WHERE
	                        1
	                    ORDER BY
	                        kategori.KdKategori ASC
	                ";
	                $qry_kategori = mysql_query($q);
	                while($row_kategori = mysql_fetch_array($qry_kategori))
	                { 
	                    list($KdKategori, $NamaKategori) = $row_kategori;    
	                    
	                    $arr_data["list_kategori"][$KdKategori] = $KdKategori;
	                    $arr_data["NamaKategori"][$KdKategori] = $NamaKategori;
	                    
	                } 
	            
	            ?>
	            <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori', this.value)">  
	                <option value=""> -- Pilih Kategori -- </option>
	                <?php 
	                    foreach($arr_data["list_kategori"] as $KdKategori=>$val)
	                    {
	                        $NamaKategori = $arr_data["NamaKategori"][$KdKategori];
	                        ?>
	                        <option value="<?php echo $KdKategori; ?>"><?php echo $NamaKategori; ?></option>
	                        <?php
	                    }
	                ?>
	            </select>
	            
	            <span id="td_KdSubKategori"></span> 
	            <?php
	        }
        
    }
    
    function get_code_PCode($v_KdSubDivisi, $v_KdKategori, $v_KdSubKategori)
	{
		//echo $v_KdSubDivisi." - ". $v_KdKategori." - ". $v_KdSubKategori;die;
	    //2 sub divisi, 1 digit kategori, digit sub kategori, 4 angka counter 
	    
	    if($v_KdSubDivisi==13 || $v_KdSubDivisi==19 || $v_KdSubDivisi==20)
	    {
	    	$jml_string = strlen($v_KdSubDivisi.$v_KdSubKategori)*1;
	        $q = "
	                SELECT
	                    RIGHT(masterbarang.PCode,3) AS last_counter
	                FROM
	                    masterbarang
	                WHERE
	                    1
	                    AND LEFT(masterbarang.PCode, $jml_string) = '".$v_KdSubDivisi.$v_KdSubKategori."'
	                ORDER BY
	                    masterbarang.PCode DESC
	                LIMIT
	                    0,1
	        ";
	        $qry = mysql_query($q);
	        $row = mysql_fetch_array($qry);
	        list($last_counter) = $row; 
	        
	        $counternya = ($last_counter*1) + 1;
	        $PCode_oke  = $v_KdSubDivisi.$v_KdSubKategori.sprintf("%03s", $counternya);   
	    }
	    else
	    {
	        $strlen_KdSubDivisi   = strlen($v_KdSubDivisi);
	        $strlen_KdKategori    = strlen($v_KdKategori);
	        $strlen_KdSubKategori = strlen($v_KdSubKategori);
	        
	        $total_digit = ($strlen_KdSubDivisi*1)+($strlen_KdKategori*1)+($strlen_KdSubKategori*1);
	        $digit = $v_KdSubDivisi.$v_KdKategori.$v_KdSubKategori;
	        
	        $q = "
	                SELECT
	                    RIGHT(masterbarang.PCode,4) AS last_counter
	                FROM
	                    masterbarang
	                WHERE
	                    1
	                    AND LEFT(masterbarang.PCode, ".$total_digit.") = '".$digit."'
	                ORDER BY
	                    masterbarang.PCode*1 DESC
	                LIMIT
	                    0,1
	        ";
	        $qry = mysql_query($q);
	        $row = mysql_fetch_array($qry);
	        list($last_counter) = $row;
	        //die();
	        
	        $counternya = ($last_counter*1) + 1;
	        $PCode_oke  = $digit.sprintf("%04s", $counternya);
	    }
	                                                 
	    return $PCode_oke;
	}
	
    function materbarangToWeb() {
        $this->load->helper('apin_helper');
        $API_APIN =  apiApin();
        $url =  $API_APIN."/productnew";
        $this->db->select('*');
        $this->db->from('masterbarang');
        $this->db->where('KdKategori', 3);
        $body_content = $this->db->get()->result_array();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body_content),
            CURLOPT_HTTPHEADER => array(
                "Content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        echo "<pre>";
        print_r($response);
        echo "</pre>";
    }	

    function tableToWeb() {
        $this->load->helper('apin_helper');
        $API_APIN =  apiApin();
        $url =  $API_APIN."/tablenew";
        $this->db->select('KdLokasi AS code, Keterangan AS name');
        $this->db->from('lokasipos');
        $this->db->where('KdDivisi', '01');
        $body_content = $this->db->get()->result_array();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body_content),
            CURLOPT_HTTPHEADER => array(
                "Content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        echo "<pre>";
        print_r($response);
        echo "</pre>";
    }
    		
}

?>