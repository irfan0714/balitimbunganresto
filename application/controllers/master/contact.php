<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class contact extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/contactmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/contact/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/contact/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/contact/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->contactmodel->num_contact_row($id,$with);
	        $data['contactdata'] = $this->contactmodel->getcontactList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Kontak","Tipe Kontak","Nama","Alamat","Kota","Telepon","Payment","TOP");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/contact/viewcontactlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  	 = "";
	     	$data['id']   	 = "";
	     	$data['nama']	 = "";
	     	$data['alm']	 = "";
	     	$data['kota']	 = "";
	     	$data['telp']	 = "";
			$data['almkirim']	 = "";
	     	$data['kotakirim']	 = "";
			$data['telpkirim']	 = ""; //new
			$data['namapajak']	 = "";
	     	$data['almpajak']	 = "";
	     	$data['kotapajak']	 = "";
			$data['npwp']	 = ""; 
			$data['tglpkp']	 = ""; //new
			$data['tipekontak']	= $this->contactmodel->getTipeKontak();
			$data['stataktif']	= "Y";
			$data['niltipe'] = "";
			$data['sumber'] = "C";
			$data['top'] = "0";
			$data['limitkredit'] = "0";
			$data['limitfaktur'] = "0";
	    	$this->load->view('master/contact/addcontact',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_contact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewcontact'] = $this->contactmodel->getDetail($id);
			$data['tipekontak']	= $this->contactmodel->getTipeKontak();
	    	$data['edit'] 		  = false;
			if($data['viewcontact']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
	    	$this->load->view('master/contact/vieweditcontact', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_contact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewcontact'] = $this->contactmodel->getDetail($id);
			$data['cekAda'] = $this->contactmodel->cekDelete($id);
	    	$this->load->view('master/contact/deletecontact', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('contact', array('Kdcontact' => $id));
		redirect('/master/contact/');
	}

    function edit_contact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['viewcontact'] = $this->contactmodel->getDetail($id);
			$data['tipekontak']	 = $this->contactmodel->getTipeKontak();
	    	$data['edit'] 		  = true;
			if($data['viewcontact']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
	    	$this->load->view('master/contact/vieweditcontact', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_contact(){
     	$mylib   = new globallib();
    	$id		 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
		$almkirim 	 = strtoupper(addslashes(trim($this->input->post('almkirim'))));
    	$kotakirim 	 = strtoupper(addslashes(trim($this->input->post('kotakirim'))));
		$telpkirim	 = $this->input->post('telpkirim');
		$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('almpajak'))));
		$kotapajak = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	$npwp 	 = $this->input->post('npwp');
		$tglpkp	 = $this->input->post('tglpkp');
    	$tipe	 = $this->input->post('tipe');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		$sumber  = $this->input->post('sumber');
		$top     = $this->input->post('top');
		$limitkredit     = $this->input->post('limitkredit');
		$limitfaktur     = $this->input->post('limitfaktur');
		$stataktif  = $this->input->post('stataktif');
    	$data = array(
			'Nama' 		 => $nama,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'AlamatKirim'	 => $almkirim,
			'KotaKirim'		 => $kotakirim,
			'TelpKirim'		 => $telpkirim,
			'NamaPajak'		 => $namapajak,
			'AlamatPajak'	 => $almpajak,
			'KotaPajak'		 => $kotapajak,
			'NPWP'		 => $npwp,
			'TglPKP'	 => $tglpkp,
			'Telepon'	 => $telp,
			'KdTipeContact'	 => $tipe,
			'Payment'    => $sumber,
			'Top'        => $top,
			'LimitKredit' => $limitkredit,
			'LimitFaktur' => $limitfaktur,
			'StatAktif' => $stataktif,
            'EditDate'	 => $tgltrans
			);
		$this->db->update('contact', $data, array('KdContact' => $id));
    	redirect('/master/contact/');
    }
    function save_new_contact(){
     	$mylib = new globallib();
		$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$idnama  = substr($nama,0,2);
		$id      = $idnama.substr("00000".$this->contactmodel->getidcounter($idnama)->Counter,-4);
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
    	$tipe	 = $this->input->post('tipe');
		$almkirim  = strtoupper(addslashes(trim($this->input->post('almkirim'))));
    	$kotakirim = strtoupper(addslashes(trim($this->input->post('kotakirim'))));
		$telpkirim	 = $this->input->post('telpkirim');
		$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('almpajak'))));
		$kotapajak = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	$npwp 	 = $this->input->post('npwp');
		$tglpkp	 = $this->input->post('tglpkp');
		$sumber  = $this->input->post('sumber');
		$top     = $this->input->post('top');
		$limitkredit     = $this->input->post('limitkredit');
		$limitfaktur     = $this->input->post('limitfaktur');
		$stataktif  = $this->input->post('stataktif');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'KdContact' => $id,
			'Nama' 		 => $nama,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'AlamatKirim'	 => $almkirim,
			'KotaKirim'		 => $kotakirim,
			'TelpKirim'		 => $telpkirim,
			'NamaPajak'		 => $namapajak,
			'AlamatPajak'	 => $almpajak,
			'KotaPajak'		 => $kotapajak,
			'NPWP'		 => $npwp,
			'TglPKP'	 => $tglpkp,
			'KdTipeContact'	 => $tipe,
			'Payment'    => $sumber,
			'Top'        => $top,
			'LimitKredit' => $limitkredit,
			'LimitFaktur' => $limitfaktur,
			'StatAktif' => $stataktif,
			'AddDate'    => $tgltrans
		);
		$this->db->insert('contact', $data);
		redirect('/master/contact/');
	
	}
}
?>