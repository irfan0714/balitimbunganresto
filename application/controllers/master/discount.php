<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class discount extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/discountmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '13';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/discount/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/discount/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/discount/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->discountmodel->num_discount_row($id,$with);
	        $data['discountdata'] = $this->discountmodel->getdiscountList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Nama Discount","Jenis","Tipe Discount","Perhitungan","Periode 1","Periode 2");
	        $this->pagination->initialize($config);
	        $this->load->view('master/discount/viewdiscountlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']     = "";
	     	$data['id']      = "";
	     	$data['nama']      = "";
	     	$data['mjenis']  = array("R"=>"Reguler","P"=>"Promosi");
	     	$data['jenis']   = "";
	     	$data['mrup']    = array("P"=>"Persentase","R"=>"Rupiah","B"=>"Barang");
	     	$data['rup']     = "";
	     	$data['hitung']  = "";
	     	$data['period1'] = "";
	     	$data['period2'] = "";
	     	$data['mbeban']  = array("M"=>"Marketing","S"=>"Sales","L"=>"Lain - Lain");
	     	$data['beban']   = array("","","");
	     	$data['mrek']    = $this->discountmodel->getRekening();
	     	$data['rek']     = array("","","");
	     	$data['persen']  = array(0,0,0);
	     	$data['nilai']	 = "";
	     	$data['mhadiah'] = array("S"=>"Barang Yang Sama","B"=>"Salah Satu Dari Kode Barang");
	     	$data['hadiah']	 = "";
			$data['mfooterdetail']  = array("F"=>"Discount Footer","D"=>"Discount Detail");
	     	$data['footerdetail']   = "";
	     	$data['judul'] 	 = array("Divisi"=>array("KdDivisi","NamaDivisi","divisi"),
			                         "Sub Divisi"=>array("KdSubDivisi","NamaSubDivisi","subdivisi"),
									 "Kategori"=>array("KdKategori","NamaKategori","kategori"),
									 "Sub Kategori"=>array("KdSubKategori","NamaSubKategori","subkategori"),
									 "Brand"=>array("KdBrand","NamaBrand","brand"),
									 "Sub Brand"=>array("KdSubBrand","NamaSubBrand","subbrand"),
									 "Barang"=>array("PCode","NamaStruk","masterbarang"),
									 "Contact"=>array("KdContact","Nama","contact"),
									 "Hari"=>array("KdHari","NamaHari","hari"));
	     	$data['opr']	 = array("NIL",">=",">","=","<=","<");
	     	$data['mcampur']  = array("Y"=>"Ya","T"=>"Tidak");
	    	$this->load->view('master/discount/adddiscount',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_discount($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$headerdisc         = $this->discountmodel->getHeader($id);
			$detaildisc         = $this->discountmodel->getDetail($id);
			
			//================================================
			//	for($a=0;$a<8;$a++){
			/*
			$namaopr  = "opr1".$a;
			$namaopr2 = "opr2".$a;
			$namanil  = "nil1".$a;
			$namanil2 = "nil2".$a;
			$campuran = "campur".$a;
			$pilih    = "sel".$a;
			$pilih1   = "kec".$a;
			
			$pcodearr = array();
			$contactarr = array();
			for($a=0;$a<count($datadetail);$a++)
			{
				if($datadetail[$a]['PCode']!="")
				{
					$pcodearr[] = $datadetail[$a]['PCode'];
				}
				else if($datadetail[$a]['KdContact']!="")
				{
					$contactarr[] = $datadetail[$a]['KdContact'];
				}
			}
			*/
			
			//================================================
			$data['msg']     = "";
	     	$data['id']      = $id;
	     	$data['nama']    = $headerdisc->NamaDisc;
	     	$data['mjenis']  = array("R"=>"Reguler","P"=>"Promosi");
	     	$data['jenis']   = $headerdisc->Jenis;
	     	$data['mrup']    = array("P"=>"Persentase","R"=>"Rupiah","B"=>"Barang");
	     	$data['rup']     = $headerdisc->RupBar;
	     	$data['hitung']  = $headerdisc->Perhitungan;
			if($headerdisc->RupBar=="P")
			{
			   $data['mhitung']  = array("B"=>"Bertingkat","S"=>"Sejajar");
			   $data['satuan'] = "Persen";
			}
			else
			{
			   $data['mhitung']  = array("K"=>"Kelipatan","T"=>"Tidak");
			   if($headerdisc->RupBar=="R")
			   $data['satuan'] = "Rupiah";
			   else
			   $data['satuan'] = "Barang";
			}
	     	$data['period1'] = $headerdisc->Period1;
	     	$data['period2'] = $headerdisc->Period2;
	     	$data['mbeban']  = array("M"=>"Marketing","S"=>"Sales","L"=>"Lain - Lain");
	     	$data['beban']   = array($headerdisc->Beban,$headerdisc->Beban2,$headerdisc->Beban3);
	     	$data['mrek']    = $this->discountmodel->getRekening();
	     	$data['rek']     = array($headerdisc->NoRekening,$headerdisc->NoRekening2,$headerdisc->NoRekening3);
	     	$data['persen']  = array($headerdisc->Persen1,$headerdisc->Persen2,$headerdisc->Persen3);
	     	$data['nilai']	 = $headerdisc->Nilai;
	     	$data['mhadiah'] = array("S"=>"Barang Yang Sama","B"=>"Salah Satu Dari Kode Barang");
	     	$data['hadiah']	 = $headerdisc->HadiahBarang;
			$data['mfooterdetail']  = array("F"=>"Discount Footer","D"=>"Discount Detail");
	     	$data['footerdetail']   = $headerdisc->FooterDetail;
	     	$data['judul'] 	 = array("Divisi"=>array("KdDivisi","NamaDivisi","divisi"),
			                         "Sub Divisi"=>array("KdSubDivisi","NamaSubDivisi","subdivisi"),
									 "Kategori"=>array("KdKategori","NamaKategori","kategori"),
									 "Sub Kategori"=>array("KdSubKategori","NamaSubKategori","subkategori"),
									 "Brand"=>array("KdBrand","NamaBrand","brand"),
									 "Sub Brand"=>array("KdSubBrand","NamaSubBrand","subbrand"),
									 "Barang"=>array("PCode","NamaStruk","masterbarang"),
									 "Contact"=>array("KdContact","Nama","contact"),
									 "Hari"=>array("KdHari","NamaHari","hari"));
			$data['detaildisc'] = $detaildisc;						 
	     	$data['opr']	 = array("NIL",">=",">","=","<=","<");
	     	$data['mcampur']  = array("Y"=>"Ya","T"=>"Tidak");
			//=====================
	    	$data['edit'] 	= false;
	    	$this->load->view('master/discount/vieweditdiscount', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('area', array('KdArea' => $id));
		redirect('/master/area/');
	}

    function save_new_discount(){
	    $mylib = new globallib();
    	$nama  = strtoupper(trim($this->input->post('nama')));
    	$jenis = $this->input->post('jenis');
    	$rup = $this->input->post('rup');
    	$hitung = $this->input->post('hitung');
    	$perio1 = $this->input->post('periode1');
    	$perio2 = $this->input->post('periode2');
    	if($perio1==''){ $periode1 = "0000-00-00"; }
    	else
		{
			$periode1 = $mylib->ubah_tanggal($perio1);
		}
    	if($perio2==''){ $periode2 = "0000-00-00"; }
    	else
		{
			$periode2 = $mylib->ubah_tanggal($perio2);
		}
    	$beban1 = $this->input->post('beban1');
    	$beban2 = $this->input->post('beban2');
    	$beban3 = $this->input->post('beban3');
		$rek1 = $this->input->post('rek1');
    	$rek2 = $this->input->post('rek2');
    	$rek3 = $this->input->post('rek3');
    	$persen1  = trim($this->input->post('persen1'));
    	$persen2  = trim($this->input->post('persen2'));
    	$persen3  = trim($this->input->post('persen3'));
    	$nilai = trim($this->input->post("nilai"));
		$baranghadiah = $this->input->post('brg');
		$hadiah = $this->input->post('bonus');
		$footerdetail = $this->input->post('footerdetail');

	 	$data = array(
           'NamaDisc' => $nama,
           'Jenis'	=> $jenis,
           'RupBar'	=> $rup,
           'Perhitungan'	=> $hitung,
           'Period1'	=> $periode1,
           'Period2'	=> $periode2,
		   'Beban'	=> $beban1,
		   'NoRekening'	=> $rek1,
		   'Persen1'	=> $persen1,
		   'Beban2'	=> $beban2,
		   'NoRekening2'	=> $rek2,
		   'Persen2'	=> $persen2,
		   'Beban3'	=> $beban3,
		   'NoRekening3'	=> $rek3,
		   'Persen3'	=> $persen3,
		   'Nilai'	=> $nilai,
		   'HadiahBarang' => $hadiah,
		   'FooterDetail' => $footerdetail,
           'AddDate'    => date("Y-m-d")
        );
		$this->db->insert('discount_header', $data);
		//echo"<pre>";print_r($baranghadiah);echo"</pre>";die();
		IF($rup=="B"&&$hadiah=="B")
		{
			for ($i = 0; $i < count($baranghadiah); $i++) {
			  $brg =  $baranghadiah[$i];
			  $data = array(
				 'NamaDisc'   => $nama,
				 'Jenis'	=> "00",
				 'List' => $brg,
				 'Status' => "",
				 'Opr1' => "",
				 'Nilai1' => "0",
				 'Opr2' => "",
				 'Nilai2' => "0",
				 'Campur' => "0",
				 'AddDate' => date("Y-m-d")
			  );
			  if(!empty($brg))
			     $this->db->insert('discount_detail', $data);
			}
		}
		
		for($a=0;$a<9;$a++){
			$namaopr = "opr1".$a;
			$namaopr2 = "opr2".$a;
			$namanil = "nil1".$a;
			$namanil2 = "nil2".$a;
			$campuran = "campur".$a;
			$pilih = "sel".$a;
			$pilih1 = "kec".$a;
			$sel0 = $this->input->post($pilih);
			
			$sel1 = $this->input->post($pilih1);
			
			$opr1 = $this->input->post($namaopr);
			$opr2 = $this->input->post($namaopr2);
			$nil1 = $this->input->post($namanil);
			$nil2 = $this->input->post($namanil2);
			$campur = $this->input->post($campuran);
			$jmljenis = $a + 1;
		   if(strlen($jmljenis)==1)
		   {
			  $jenis = "0".$jmljenis ;
			}
		   else{
			  $jenis = $jmljenis;
			}
			if($nil1=='')$nil1 =0;
			if($nil2=='')$nil2 =0;
			/*
			echo $pilih."<br>";
			echo count($sel0)."<br>";
			echo"<pre>";print_r($sel0);echo"</pre>";
			echo empty($sel0[0])."<br>";
			echo $pilih1."<br>";
			echo count($sel1)."<br>";
			echo"<pre>";print_r($sel1);echo"</pre>";
			echo empty($sel1[0])."<br>";
			*/
			for($i=0;$i<count($sel0);$i++){
				$p  = $sel0[$i];	// status P untuk list, K untuk kecuali
				$data = array(
				 'NamaDisc'   => $nama,
				 'Jenis'	=> $jenis,
				 'List' => $p,
				 'Status' => "P",
				 'Opr1' => $opr1,
				 'Nilai1' => $nil1,
				 'Opr2' => $opr2,
				 'Nilai2' => $nil2,
				 'Campur' => $campur,
				 'AddDate' => date("Y-m-d"),
				 'EditDate' => "0000-00-00"
				);
				if(!empty($p))
				   $this->db->insert('discount_detail', $data);
		   }
		   for($i=0;$i<count($sel1);$i++){
				$q=$sel1[$i];
				$data = array(
				 'NamaDisc'   => $nama,
				 'Jenis'	=> $jenis,
				 'List' => $q,
				 'Status' => "K",
				 'Opr1' => $opr1,
				 'Nilai1' => $nil1,
				 'Opr2' => $opr2,
				 'Nilai2' => $nil2,
				 'Campur' => $campur,
				 'AddDate' => date("Y-m-d"),
				 'EditDate' => "0000-00-00"
				);
				if(!empty($q))
				  $this->db->insert('discount_detail', $data); //sampe sini
		   }
		}
		//die();
		redirect('/master/discount/');
	}
	function getItemHadiah()
	{
		$pcode = $this->input->post("pcode");
		$num   = $this->discountmodel->HadiahExist($pcode);
		$data1 = "";
		if($num==0)
		{
			$data1 = "empty";
		}
		echo $data1;
	}
	function cekNama()
	{
		$nama = $this->input->post("nama");
		$num   = $this->discountmodel->get_id($nama);
		echo $num;
	}
}
?>