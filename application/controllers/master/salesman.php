<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class salesman extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/salesmanmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/salesman/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/salesman/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/salesman/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->salesmanmodel->num_salesman_row($id,$with);
	        $data['salesman_data']  = $this->salesmanmodel->get_salesman_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/salesman/salesman_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	//$data['tipe']  = $this->salesmanmodel->getTipe();
	     	$data['niltipe'] = "";
			//$data['personal']  = $this->salesmanmodel->getPersonal();
			$data['nilsupervisor'] = "";
			$data['supervisor']  = $this->salesmanmodel->getSupervisor();
	     	$data['nilpersonal'] = "";
			//$data['gudang']  = $this->salesmanmodel->getGudang();
	     	$data['nilgudang'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/salesman/add_salesman',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_salesman($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_salesman'] = $this->salesmanmodel->getDetail($id);
	    	$data['supervisor']  = $this->salesmanmodel->getSupervisor();
	    	//$data['tipe'] 	  = $this->salesmanmodel->getTipe();
			//$data['personal']  = $this->salesmanmodel->getPersonal();
			//$data['gudang']	  = $this->salesmanmodel->getGudang();
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/salesman/view_edit_salesman', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_salesman($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_salesman'] = $this->salesmanmodel->getDetail($id);
			$data['cekAda'] = $this->salesmanmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/salesman/delete_salesman', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('salesman', array('KdSalesman' => $id));
		redirect('/master/salesman/');
	}

    function edit_salesman($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_salesman'] = $this->salesmanmodel->getDetail($id);
	    	//$data['tipe'] 	  = $this->salesmanmodel->getTipe();
	    	$data['supervisor']  = $this->salesmanmodel->getSupervisor();
			//$data['personal']  = $this->salesmanmodel->getPersonal();
			//$data['gudang']	  = $this->salesmanmodel->getGudang();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/salesman/view_edit_salesman', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_salesman(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$KdSalesman	 = $this->input->post('kode');
    	$NamaSalesman 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$KdSupervisor = strtoupper(trim($this->input->post('supervisor')));
		$Status = strtoupper(trim($this->input->post('status')));
		$user = $this->session->userdata('username');
    	$data = array(
    		  'NamaSalesman'	=> $NamaSalesman ,
			  'KdSupervisor' => $KdSupervisor,
			  'Status'=>$Status,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('salesman', $data, array('KdSalesman' => $KdSalesman));
    	redirect('/master/salesman/');
    }
    
    
    function save_new_salesman(){
    	
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$personal = strtoupper(trim($this->input->post('personal')));
		$gudang = strtoupper(trim($this->input->post('gudang')));
    	$num 	 = $this->salesmanmodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdSalesman'   => $id,
               'NamaSalesman' => $nama,
			   'KdPersonal' => $personal,
               'KdTipeSalesman' => $tipe,
			   'KdGudang' => $gudang,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('salesman', $data);
			redirect('master/salesman');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilpersonal'] = $personal;
			$data['personal']  = $this->salesmanmodel->getPersonal();
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->salesmanmodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/salesman/add_salesman', $data);
		}
	}
	
	function save_new_salesman_new(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$NamaSalasman 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$KdSupervisor = strtoupper(trim($this->input->post('supervisor')));
    	$num 	 = $this->salesmanmodel->get_id($NamaSalasman);
    	$user = $this->session->userdata('username');
    	
    	$idnama  = substr($NamaSalasman,0,2);
		$id      = $idnama.substr("00000".$this->salesmanmodel->getidcounter($idnama)->Counter,-4);
		
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
		 	   'KdSalesman'=>$id,
               'NamaSalesman' => $NamaSalasman,
			   'KdSupervisor' => $KdSupervisor,
               'AddDate' 	=> date('Y-m-d'),
               'AddUser' 	=> $user
            );
            $this->db->insert('salesman', $data);
			redirect('master/salesman');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilsupervisor'] = $supervisor;
			$data['supervisor']  = $this->salesmanmodel->getSupervisor();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/salesman/add_salesman', $data);
		}
	}
	
}
?>