<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class rekening extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/rekeningmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/rekening/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/rekening/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/rekening/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->rekeningmodel->num_rekening_row($id,$with);
	        $data['rekening_data']  = $this->rekeningmodel->get_rekening_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/rekening/rekening_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
			$data['posisi']  = $this->rekeningmodel->getPosisi();
	     	$data['nilposisi'] = "";
			$data['tingkat']  = $this->rekeningmodel->getTingkat();
	     	$data['niltingkat'] = "";
	     	$data['rekparent']  = $this->rekeningmodel->getParent();
	     	$data['nilparent'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/rekening/add_rekening',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_rekening($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_rekening'] = $this->rekeningmodel->getDetail($id);

			$data['posisi']  = $this->rekeningmodel->getPosisi();
			$data['tingkat']  = $this->rekeningmodel->getTingkat();
	     	$data['rekparent']  = $this->rekeningmodel->getParent();
			
	    	$data['edit'] 		  = false;
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/rekening/view_edit_rekening', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_rekening($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_rekening'] = $this->rekeningmodel->getDetail($id);
			$data['cekAda'] = $this->rekeningmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/rekening/delete_rekening', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('rekening', array('KdRekening' => $id));
		redirect('/master/rekening/');
	}

    function edit_rekening($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_rekening'] = $this->rekeningmodel->getDetail($id);
	    	$data['posisi']  = $this->rekeningmodel->getPosisi();
			$data['tingkat']  = $this->rekeningmodel->getTingkat();
	     	$data['rekparent']  = $this->rekeningmodel->getParent();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/rekening/view_edit_rekening', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_rekening(){
    	$id 	= $this->input->post('kode');
    	$nama 	= strtoupper(addslashes(trim($this->input->post('nama'))));
		$posisi = strtoupper(trim($this->input->post('posisi')));
		$tingkat = strtoupper(trim($this->input->post('tingkat')));
    	$parent = strtoupper(trim($this->input->post('rekparent')));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaRekening'	=> $nama,
    		  'Tingkat' => $tingkat,
			  'Posisi' => $posisi,
			  'Parent' => $parent,
              'EditDate' => $tgltrans
			);
		$this->db->update('rekening', $data, array('KdRekening' => $id));
    	redirect('/master/rekening/');
    }
	
    function save_new_rekening(){
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$posisi = strtoupper(trim($this->input->post('posisi')));
		$tingkat = strtoupper(trim($this->input->post('tingkat')));
    	$parent = strtoupper(trim($this->input->post('rekparent')));
    	$num 	 = $this->rekeningmodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdRekening'   => $id ,
               'NamaRekening'	=> $nama,
    		   'Tingkat' => $tingkat,
			   'Posisi' => $posisi,
			   'Parent' => $parent,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('rekening', $data);
			redirect('master/rekening');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['niltingkat'] = $tingkat;
			$data['tingkat']  = $this->rekeningmodel->getTingkat();
			$data['nilposisi'] = $posisi;
			$data['posisi']  = $this->rekeningmodel->getPosisi();
			$data['nilparent'] = $parent;
			$data['rekparent']  = $this->rekeningmodel->getParent();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/rekening/add_rekening', $data);
		}
	}
}
?>