<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Paket extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('master/paketmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $data['track'] = $mylib->print_track();
            $data['barangpaket'] = $this->paketmodel->getbarangList();
            $this->load->view('master/paket/viewbaranglist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['id'] = "";
            $data['nama'] = "";
            $data['track'] = $mylib->print_track();
			$data['masterpkt'] =  $this->paketmodel->getPaket('');
            $this->load->view('master/paket/addbarang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function getPaketByPCode() {
        //$toSearch = $this->input->get('kode',TRUE);
        $term = trim(strip_tags($_GET['term']));
        $query = $this->paketmodel->getPaket($term);
        $results = array();
        if (count($query) > 0) {
            foreach ($query as $row) {
                $results[] = array(
                    'label' => $row->PCode,
                    'pcode' => $row->PCode1,
                    'nama' => $row->NamaLengkap,
                    'barcode' => $row->Barcode1
                );
                // $results[]=$row->Barcode1;
            }
        } else {
            $results[] = array(
                'label' => "Tidak Ada",
                'pcode' => "",
                'nama' => "",
                'barcode' => "");
        }
        echo json_encode($results);
        exit;
    }

    function getBarangByPCode() {
        //$toSearch = $this->input->get('kode',TRUE);
        //$term = trim(strip_tags($_GET['term']));
        $type = $this->input->post('type');
        $name = $this->input->post('name_startsWith');
        $query = $this->paketmodel->getBarang($type, $name);
        $results = array();
        foreach ($query as $row) {
            $name = $row['PCode'] . '|' . $row['NamaLengkap'] . '|' . $row['Barcode1'];
            array_push($results, $name);
        }

        echo json_encode($results);
        exit;
    }

    function delete_barang($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("del");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['viewbarang'] = $this->paketmodel->getDetail($id);
            $this->load->view('master/barang/deletebarang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function delete_This() {
        $id = $this->input->post('kode');
        $this->db->delete('masterbarang', array('PCode' => $id));
        redirect('/master/barang/');
    }

    function edit_barang($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['getPaket'] = $this->paketmodel->getHeader($id);
            $this->load->view('master/paket/vieweditbarang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function view_barang($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $value = $this->paketmodel->getDetail($id);
            $data = $this->isi_data("false", "edit", $id, $value->NamaStruk, $value->NamaLengkap, $value->NamaInitial, $value->HargaJual, $value->MinOrder, $value->KdDivisi, $value->KdSubDivisi, $value->KdKategori, $value->KdSubKategori, $value->KdBrand, $value->KdSubBrand, $value->KdSize, $value->KdSubSize, $value->KdDepartemen, $value->KdKelas, $value->KdType, $value->KdKemasan, $value->KdSupplier, $value->KdPrincipal, $value->KdSatuan, $value->KdGrupHarga, $value->ParentCode, $value->FlagHarga, $value->Status, $value->Konversi, $value->BarCode);
            $this->load->view('master/barang/vieweditbarang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_barang() {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $id1 = $this->input->post('id');
        $mpcode = $this->input->post('kode');
        $pcode1 = $this->input->post('itemNo');
        $qty1 = $this->input->post('quantity');
        $config = array(
            array(
                'field' => 'kode',
                'label' => 'Kode Paket',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($config);
        $this->form_validation->set_message('required', '%s Harus Isi');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        if ($this->form_validation->run() == false) {
            //$mpcode = $this->input->post('kode');
            $this->edit_barang($mpcode);
        } else {
            for ($i = 0; $i < count($pcode1); $i++) {
                //$id = $id1[$i];
                $pcode = $pcode1[$i];
                $qty = $qty1[$i];
                $data = array(
                    'DPcode' => $pcode,
                    'Qty' => $qty,
                    'Editdate' => date("Y-m-d H:i:s")
                );
                if ($pcode != "") {
                	//cek apakah PCode Tersebut sudah ada belum
                	$cek = $this->paketmodel->cekPCode($mpcode,$pcode);
                	if(empty($cek)){
						$datax = array(
							             'MPCode' => $mpcode,
							             'DPcode' => $pcode,
							             'Qty' => $qty,
							             'Adddate' => date("Y-m-d H:i:s"),
							             'Editdate' => date("Y-m-d H:i:s")
										);
						$this->db->insert('barang_paket', $datax);				
					}else{
						//$this->db->update('barang_paket', $data, array('id'=>$id, 'MPCode'=>$mpcode));
						$this->db->update('barang_paket', $data, array('MPCode'=>$mpcode,'DPcode' => $pcode));
					}
                       
                  //  $this->last_query();
                }
            }
            redirect('master/paket');
        }
    }

    function save_new_barang() {
//        echo "<pre>";
//        print_r($_POST);echo "<pre>";die();
        $id = $this->input->post('kode');
        $num = $this->paketmodel->get_id($id);
        $pcode1 = $this->input->post('itemNo');
        $qty1 = $this->input->post('quantity');
        //$this->form_validation->set_rules('kode', 'kode', 'trim|required|callback_kode_paket_check');
        $config = array(
            array(
                'field' => 'kode',
                'label' => 'Kode Paket',
                'rules' => 'trim|required|callback_kode_paket_check'
            )
        );
        //$this->form_validation->set_rules($config);
        //$this->form_validation->set_message('required', '%s Harus Isi');
        //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        //if ($this->form_validation->run() == false) {
        //	echo '<script>window.history.go(-1);</script>';
            //$this->load->view('master/paket/addbarang');
        //} else {
            for ($i = 0; $i < count($pcode1); $i++) {
                $pcode = $pcode1[$i];
                $qty = $qty1[$i];
                $data = array(
                    'MPCode' => $id,
                    'DPcode' => $pcode,
                    'Qty' => $qty,
                    'Adddate' => date("Y-m-d H:i:s")
                );
                if ($pcode != "") {
                    $this->db->insert('barang_paket', $data);
                }
            }
            redirect('master/paket');
        //}
        //  }
    }

    function kode_paket_check($id) {
        $dtpaket = $this->paketmodel->get_id($id);
        foreach ($dtpaket->result() as $value) {
            $kodepaket = $value->MPCode;
            if ($id == $kodepaket) {
                $this->form_validation->set_message('kode_paket_check', 'Paket ' . $kodepaket . ' Sudah Pernah dibuat');
                return false;
            } else {
                return true;
            }
        }
    }

    function getSubDivisiBy() {
        $divisi = $this->input->post("divisi");
        $data = $this->paketmodel->getSubDivBy($divisi);
        for ($a = 0; $a < count($data); $a++) {
            echo "<option value='" . $data[$a]['KdSubDivisi'] . "'>" . $data[$a]['NamaSubDivisi'] . "</option>";
        }
    }

    function getSubKategoriBy() {
        $kategori = $this->input->post("kategori");
        $data = $this->paketmodel->getSubKatBy($kategori);
        for ($a = 0; $a < count($data); $a++) {
            echo "<option value='" . $data[$a]['KdSubKategori'] . "'>" . $data[$a]['NamaSubKategori'] . "</option>";
        }
    }

    function getSubBrandBy() {
        $brand = $this->input->post("brand");
        $data = $this->paketmodel->getSubBrandBy($brand);
        for ($a = 0; $a < count($data); $a++) {
            echo "<option value='" . $data[$a]['KdSubBrand'] . "'>" . $data[$a]['NamaSubBrand'] . "</option>";
        }
    }

    function getSubSizeBy() {
        $size = $this->input->post("size");
        $data = $this->paketmodel->getSubSizeBy($size);
        for ($a = 0; $a < count($data); $a++) {
            echo "<option value='" . $data[$a]['KdSubSize'] . "'>" . $data[$a]['Ukuran'] . "</option>";
        }
    }

    function isi_data($msg, $namaarray, $id, $nama, $nlengkap, $ninitial, $hjual, $minimum, $divisi, $subdiv, $kategori, $subkat, $brand, $subbrand, $size, $subsize, $dept, $class, $tipe, $kemasan, $supplier, $principal, $satuan, $grup, $parent, $flag, $status, $konv, $barcode) {
        $data[$namaarray] = $msg;
        $data['id'] = $id;
        $data['nama'] = $nama;
        $data['nlengkap'] = $nlengkap;
        $data['ninitial'] = $ninitial;
        $data['hjual'] = $hjual;
        $data['konv'] = $konv;
        $data['barcode'] = $barcode;
        $data['minimum'] = $minimum;
        $data['mdivisi'] = $this->paketmodel->getDivisi();
        $data['divisi'] = $divisi;
        $subdivtemp = $this->paketmodel->getSubDivBy($divisi);
        $subdivlagi = "";
        for ($s = 0; $s < count($subdivtemp); $s++) {
            $select = "";
            if ($subdivtemp[$s]['KdSubDivisi'] == $subdiv) {
                $select = "selected";
            }
            $subdivlagi .= "<option " . $select . " value='" . $subdivtemp[$s]['KdSubDivisi'] . "'>" . $subdivtemp[$s]['NamaSubDivisi'] . "</option>";
        }
        $data['subdiv'] = $subdivlagi;

        $data['mkategori'] = $this->paketmodel->getKategori();
        $data['kategori'] = $kategori;
        $subkattemp = $this->paketmodel->getSubKatBy($kategori);
        $subkatlagi = "";
        for ($s = 0; $s < count($subkattemp); $s++) {
            $select = "";
            if ($subkattemp[$s]['KdSubKategori'] == $subkat) {
                $select = "selected";
            }
            $subkatlagi .= "<option " . $select . " value='" . $subkattemp[$s]['KdSubKategori'] . "'>" . $subkattemp[$s]['NamaSubKategori'] . "</option>";
        }
        $data['subkat'] = $subkatlagi;

        $data['mbrand'] = $this->paketmodel->getBrand();
        $data['brand'] = $brand;
        $subbrandtemp = $this->paketmodel->getSubBrandBy($brand);
        $subbrandlagi = "";
        for ($s = 0; $s < count($subbrandtemp); $s++) {
            $select = "";
            if ($subbrandtemp[$s]['KdSubBrand'] == $subbrand) {
                $select = "selected";
            }
            $subbrandlagi .= "<option " . $select . " value='" . $subbrandtemp[$s]['KdSubBrand'] . "'>" . $subbrandtemp[$s]['NamaSubBrand'] . "</option>";
        }
        $data['subbrand'] = $subbrandlagi;

        $data['msize'] = $this->paketmodel->getSize();
        $data['size'] = $size;
        $subsizetemp = $this->paketmodel->getSubSizeBy($size);
        $subsizelagi = "";
        for ($s = 0; $s < count($subsizetemp); $s++) {
            $select = "";
            if ($subsizetemp[$s]['KdSubSize'] == $subsize) {
                $select = "selected";
            }
            $subsizelagi .= "<option " . $select . " value='" . $subsizetemp[$s]['KdSubSize'] . "'>" . $subsizetemp[$s]['Ukuran'] . "</option>";
        }
        $data['subsize'] = $subsizelagi;
        $data['mdept'] = $this->paketmodel->getDept();
        $data['dept'] = $dept;
        $data['mclass'] = $this->paketmodel->getKelas();
        $data['class'] = $class;
        $data['mtipe'] = $this->paketmodel->getTipe();
        $data['tipe'] = $tipe;
        $data['mkemasan'] = $this->paketmodel->getKemasan();
        $data['kemasan'] = $kemasan;
        $data['msupplier'] = $this->paketmodel->getSupplier();
        $data['supplier'] = $supplier;
        $data['mprincipal'] = $this->paketmodel->getPrincipal();
        $data['principal'] = $principal;
        $data['msatuan'] = $this->paketmodel->getSatuan();
        $data['satuan'] = $satuan;
        $data['mgrup'] = $this->paketmodel->getGrup();
        $data['grup'] = $grup;
        $data['mparent'] = $this->paketmodel->getParent();
        $data['parent'] = $parent;
        $data['mflag'] = array("HJ" => "Harga Jual", "GH" => "Grup Harga");
        $data['flag'] = $flag;
        $data['mstatus'] = array("Normal", "Konsinyasi");
        $data['status'] = $status;
        return $data;
    }

}

?>