<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class customer extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/customermodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/customer/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/customer/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/customer/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->customermodel->num_customer_row($id,$with);
	        $data['customerdata'] = $this->customermodel->getcustomerList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Customer","Tipe Customer","Nama","Alamat","Kota","Telepon","Payment","TOP","PPn");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/customer/viewcustomerlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  	 = "";
	     	$data['id']   	 = "";
	     	$data['nama']	 = "";
			$data['contact'] = "";
	     	$data['alm']	 = "";
	     	$data['kota']	 = "";
	     	$data['telp']	 = "";
			$data['almkirim']	 = "";
	     	$data['kotakirim']	 = "";
			$data['telpkirim']	 = ""; //new
			$data['namapajak']	 = "";
	     	$data['almpajak']	 = "";
	     	$data['kotapajak']	 = "";
			$data['npwp']	 = ""; 
			$data['tglpkp']	 = ""; //new
			$data['tipecustomer']	= $this->customermodel->getTipeCustomer();
			$data['groupcustomer']	= $this->customermodel->getGroupCustomer();
			$data['area']	= $this->customermodel->getArea();
			$data['stataktif']	= "Y";
			$data['niltipe'] = "";
			$data['nilgroup'] = "";
			$data['nilarea'] = "";
			$data['sumber'] = "C";
			$data['top'] = "0";
			$data['limitkredit'] = "0";
			$data['limitfaktur'] = "0";
			$data['ppn'] = "0";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/customer/addcustomer',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_customer($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewcustomer'] = $this->customermodel->getDetail($id);
			$data['tipecustomer']	= $this->customermodel->getTipeCustomer();
			$data['groupcustomer']	= $this->customermodel->getGroupCustomer();
			$data['area']	= $this->customermodel->getArea();
	    	$data['edit'] 		  = false;
			if($data['viewcustomer']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/customer/vieweditcustomer', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_customer($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewcustomer'] = $this->customermodel->getDetail($id);
			$data['cekAda'] = $this->customermodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/customer/deletecustomer', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('customer', array('Kdcustomer' => $id));
		redirect('/master/customer/');
	}

    function edit_customer($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['viewcustomer'] = $this->customermodel->getDetail($id);
			$data['tipecustomer']	 = $this->customermodel->getTipeCustomer();
			$data['groupcustomer']	= $this->customermodel->getGroupCustomer();
			$data['area']	= $this->customermodel->getArea();
	    	$data['edit'] 		  = true;
			if($data['viewcustomer']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/customer/vieweditcustomer', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_customer(){
     	$mylib   = new globallib();
    	$id		 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$contact = strtoupper(addslashes(trim($this->input->post('contact'))));
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
		$almkirim 	 = strtoupper(addslashes(trim($this->input->post('almkirim'))));
    	$kotakirim 	 = strtoupper(addslashes(trim($this->input->post('kotakirim'))));
		$telpkirim	 = $this->input->post('telpkirim');
		$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('almpajak'))));
		$kotapajak = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	$npwp 	 = $this->input->post('npwp');
		$tglpkp	 = $mylib->ubah_tanggal($this->input->post('tglpkp'));
    	$tipe	 = $this->input->post('tipe');
		$group	 = $this->input->post('group');
		$area	 = $this->input->post('area');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		$sumber  = $this->input->post('sumber');
		$top     = $this->input->post('top');
		$limitkredit     = $this->input->post('limitkredit');
		$limitfaktur     = $this->input->post('limitfaktur');
		$ppn     = $this->input->post('ppn');
		$stataktif  = $this->input->post('stataktif');
    	$data = array(
			'Nama' 		 => $nama,
			'Contact'	 => $contact,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'AlamatKirim'	 => $almkirim,
			'KotaKirim'		 => $kotakirim,
			'TelpKirim'		 => $telpkirim,
			'NamaPajak'		 => $namapajak,
			'AlamatPajak'	 => $almpajak,
			'KotaPajak'		 => $kotapajak,
			'NPWP'		 => $npwp,
			'TglPKP'	 => $tglpkp,
			'Telepon'	 => $telp,
			'KdTypecust'	 => $tipe,
			'KdGroupcust'	 => $group,
			'KdArea'	 => $area,
			'Payment'    => $sumber,
			'Top'        => $top,
			'LimitKredit' => $limitkredit,
			'LimitFaktur' => $limitfaktur,
			'PPn'        => $ppn,
			'StatAktif' => $stataktif,
            'EditDate'	 => $tgltrans
			);
		$this->db->update('customer', $data, array('KdCustomer' => $id));
    	redirect('/master/customer/');
    }
    function save_new_customer(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
     	$mylib = new globallib();
		$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$contact = strtoupper(addslashes(trim($this->input->post('contact'))));
		$idnama  = substr($nama,0,2);
		$id      = $idnama.substr("00000".$this->customermodel->getidcounter($idnama)->Counter,-4);
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
    	$tipe	 = $this->input->post('tipe');
		$group	 = $this->input->post('group');
		$area	 = $this->input->post('area');
		$almkirim  = strtoupper(addslashes(trim($this->input->post('almkirim'))));
    	$kotakirim = strtoupper(addslashes(trim($this->input->post('kotakirim'))));
		$telpkirim	 = $this->input->post('telpkirim');
		$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('almpajak'))));
		$kotapajak = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	$npwp 	 = $this->input->post('npwp');
		$tglpkp	 = $mylib->ubah_tanggal($this->input->post('tglpkp'));
		$sumber  = $this->input->post('sumber');
		$top     = $this->input->post('top');
		$limitkredit     = $this->input->post('limitkredit');
		$limitfaktur     = $this->input->post('limitfaktur');
		$ppn     = $this->input->post('ppn');
		$stataktif  = $this->input->post('stataktif');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'KdCustomer' => $id,
			'Nama' 		 => $nama,
			'Contact'	 => $contact,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'AlamatKirim'	 => $almkirim,
			'KotaKirim'		 => $kotakirim,
			'TelpKirim'		 => $telpkirim,
			'NamaPajak'		 => $namapajak,
			'AlamatPajak'	 => $almpajak,
			'KotaPajak'		 => $kotapajak,
			'NPWP'		 => $npwp,
			'TglPKP'	 => $tglpkp,
			'Telepon'	 => $telp,
			'KdTypecust'	 => $tipe,
			'KdGroupcust'	 => $group,
			'KdArea'	 => $area,
			'Payment'    => $sumber,
			'Top'        => $top,
			'LimitKredit' => $limitkredit,
			'LimitFaktur' => $limitfaktur,
			'PPn'        => $ppn,
			'StatAktif' => $stataktif,
			'AddDate'    => $tgltrans
		);
		$this->db->insert('customer', $data);
		redirect('/master/customer/');
	
	}
}
?>