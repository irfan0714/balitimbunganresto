<?php

class Ketentuan_diskon extends CI_Controller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('master/ketentuan_diskon_model');
    }
    
    
    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "ketentuan_diskon", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/master/ketentuan_diskon/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/master/ketentuan_diskon/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $config['total_rows'] = $this->ketentuan_diskon_model->num_ketentuan_diskon_row($arr_search["search"]);
            $data['data'] = $this->ketentuan_diskon_model->getDiskonList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('master/ketentuan_diskon/ketentuan_diskon_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
    
    
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'ketentuan_diskon', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "ketentuan_diskon",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/master/ketentuan_diskon/index/' . $query_id . '');
    }

    

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $data['bykategori'] = $this->ketentuan_diskon_model->getByKategori();
            $data['bydivisi'] = $this->ketentuan_diskon_model->getByDivisi();
                        
            $data['track'] = $mylib->print_track();
            $this->load->view('master/ketentuan_diskon/add_ketentuan_diskon', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_data($id) 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            
            $data['bykategori'] = $this->ketentuan_diskon_model->getByKategori();
            $data['bydivisi'] = $this->ketentuan_diskon_model->getByDivisi();
            $data['header'] = $this->ketentuan_diskon_model->getTabelHeaderList($id);
            $data['detail'] = $this->ketentuan_diskon_model->getTabelDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('master/ketentuan_diskon/edit_ketentuan_diskon', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_data($id) 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            
            $data['bykategori'] = $this->ketentuan_diskon_model->getByKategori();
            $data['bydivisi'] = $this->ketentuan_diskon_model->getByDivisi();
            $data['header'] = $this->ketentuan_diskon_model->getTabelHeaderList($id);
            $data['detail'] = $this->ketentuan_diskon_model->getTabelDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('master/ketentuan_diskon/view_ketentuan_diskon', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_periode1 = $mylib->ubah_tanggal($this->input->post('v_tgl_periode1'));
        $v_tgl_periode2 = $mylib->ubah_tanggal($this->input->post('v_tgl_periode2'));
        $v_diskon = $this->input->post('v_diskon');
        $v_min_pembelian = $this->input->post('v_min_pembelian');
        $v_note = $this->input->post('v_note');
        
        $pil_min_pembelian = $this->input->post('v_min_pembelian');
        $harga_minimal_pembelian = $this->input->post('minimal_pembelian');
        $v_pil_input = $this->input->post('v_pil_input');
        $v_pil_by = $this->input->post('v_pil_by');
        $v_pil_by_kategori = $this->input->post('v_pil_by_kategori');
        $v_pil_by_divisi = $this->input->post('v_pil_by_divisi');
        $excludepromo = $this->input->post('excludepromo');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        //detail manual
        $pcode1 = $this->input->post('pcode');
        
        if($flag=="add"){
        
	        //notransdiskon	
	        $tgx = explode("-",$v_tgl_periode1);
			$notrans_awal = substr($tgx[0],2,2).$tgx[1];
	        $no_new = $this->ketentuan_diskon_model->getNoDisc($notrans_awal);
			if(count($no_new)>0){
				$notrans_new = $no_new->NoTrans + 1;	
			}
			else{
				$notrans_new = $notrans_awal."0001";
			}
			
			//minimun pembelian
			if($harga_minimal_pembelian=="" || $harga_minimal_pembelian==0){
				$minimum = 0;
				$tanda = "P";
			}
			else{
				$minimum = $harga_minimal_pembelian;
				$tanda = "X";
			}
			
			//excludepromo
			if($excludepromo=="Y"){
				$exclude_promo="Y";
			}else{
				$exclude_promo="N";
			}
			
			
	        //header
	        $this->ketentuan_diskon_model->insertHeaderDiscount($notrans_new,date('Y-m-d'),$v_note ,$v_tgl_periode1,$v_tgl_periode2,$minimum,'0',$exclude_promo,$v_pil_input,$v_pil_by, $v_pil_by_kategori,$v_pil_by_divisi,$user);
	        	
	        //manual
	        if($v_pil_input=="M"){
	        	
				for ($x = 0; $x < count($pcode1); $x++) 
		        {
	            $pcode = strtoupper(addslashes(trim($pcode1[$x])));
	            $this->ketentuan_diskon_model->insertDetailDiscount($notrans_new,$pcode,$tanda,$v_diskon);
	            }
	        //otomatis
			}else if($v_pil_input=="O"){

				//dengan includepromo
				if($excludepromo=="Y"){
					//by kategori
					if($v_pil_by=="K"){
					   $this->ketentuan_diskon_model->insertDetailBykategoriIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori,$v_tgl_periode1,$v_tgl_periode2);
					}else if($v_pil_by=="D"){
					   $this->ketentuan_diskon_model->insertDetailByDivisiIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi,$v_tgl_periode1,$v_tgl_periode2);
					}
					
				//tanpa includepromo	
				}else{
					//by kategori
					if($v_pil_by=="K"){
					   $this->ketentuan_diskon_model->insertDetailBykategoriTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori);
					}else if($v_pil_by=="D"){
					   $this->ketentuan_diskon_model->insertDetailByDivisiTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi);
					}
				} 	
			}
	
		}else if($flag=="edit"){
			$notrans_new = $this->input->post('v_notrans');
			
			//minimun pembelian
			if($harga_minimal_pembelian=="" || $harga_minimal_pembelian==0){
				$minimum = 0;
				$tanda = "P";
			}
			else{
				$minimum = $harga_minimal_pembelian;
				$tanda = "X";
			}
			
			//excludepromo
			if($excludepromo=="Y"){
				$exclude_promo="Y";
			}else{
				$exclude_promo="N";
			}
			
			
	        //header
	        $this->ketentuan_diskon_model->updateHeaderDiscount($notrans_new,date('Y-m-d'),$v_note ,$v_tgl_periode1,$v_tgl_periode2,$minimum,'0',$exclude_promo,$v_pil_input,$v_pil_by, $v_pil_by_kategori,$v_pil_by_divisi,$user);
	        	
	        //manual
	        if($v_pil_input=="M"){
	        	
				for ($x = 0; $x < count($pcode1); $x++) 
		        {
	            $pcode = strtoupper(addslashes(trim($pcode1[$x])));
	            $this->ketentuan_diskon_model->insertDetailDiscount($notrans_new,$pcode,$tanda,$v_diskon);
	            }
	        //otomatis
			}else if($v_pil_input=="O"){

				//dengan includepromo
				if($excludepromo=="Y"){
					//by kategori
					if($v_pil_by=="K"){
					   $this->ketentuan_diskon_model->insertDetailBykategoriIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori,$v_tgl_periode1,$v_tgl_periode2);
					}else if($v_pil_by=="D"){
					   $this->ketentuan_diskon_model->insertDetailByDivisiIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi,$v_tgl_periode1,$v_tgl_periode2);
					}
					
				//tanpa includepromo	
				}else{
					//by kategori
					if($v_pil_by=="K"){
					   $this->ketentuan_diskon_model->insertDetailBykategoriTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori);
					}else if($v_pil_by=="D"){
					   $this->ketentuan_diskon_model->insertDetailByDivisiTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi);
					}
				} 	
			}
			
		}
        
        $this->session->set_flashdata('msg', array('message' => 'Proses Add <strong>SUKSES</strong>.', 'class' => 'success'));
        redirect('/master/ketentuan_diskon/edit_data/' . $notrans_new . '');
        
    }

    function delete_detail($nodiskon,$pcode){
    	$this->db->delete('discountdetail',array('PCode'=>$pcode,'NoTrans'=>$nodiskon));
		redirect('/master/ketentuan_diskon/edit_data/' . $nodiskon . '');
	}
	
	function close_data($nodiskon){
    	$this->db->update('discountheader',array('Status'=>'1'),array('NoTrans'=>$nodiskon));
		redirect('/master/ketentuan_diskon/');
	}
	
	function delete_trans($nodiskon){
		$this->db->delete('discountheader',array('NoTrans'=>$nodiskon));
    	$this->db->delete('discountdetail',array('NoTrans'=>$nodiskon));
		redirect('/master/ketentuan_diskon/');
	}
		
}

?>