<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Tour_Leader extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/tour_leadermodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);	     	
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/tour_leader/index/';
			$page					  = $this->uri->segment(4);		
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/tour_leader/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/tour_leader/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);

            $data['header'] = array('Kode','Nama','Type Leader','Travel Leader','Phone','Fax','Alamat','Email','Kota','Telepon','Status','AddDate','EditDate');
	        $config['total_rows']     = $this->tour_leadermodel->num_typetour_row($id,$with);
	        $data['detail']       = $this->tour_leadermodel->gettypetourList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/tourleader/viewlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){

     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
            $data=  array(
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'label'  => "Add Member",
	     	    'msg'    => "",
	     	    'kode'   => "",
	     	    'Kode'   => "",
                'Nama'   => "",
                'TypeLeadernya' => "",
                'BiroTournya'   => "",
                'Alamat' => "",
                'Kota'   => "",
                'Phone'  => "",
                'Fax'       => "",
                'Email'     => "",
                'AddDate'   => "",
                'EditDate'  => "",
                'Telepon'   => "",
                'NoRekening'   => "",
                'Bank'   => "",
                'Penerima'   => "",
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                'TypeLeader'  => $this->tour_leadermodel->getTypeTourLeader(),
                'BiroTour'  => $this->tour_leadermodel->getBiroTour(),
                'track'     => $mylib->print_track()
            );
	    	$this->load->view('master/tourleader/add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete(){
     	$id = $this->input->post('kode');
		$this->db->delete('tourleader', array('KdTourLeader' => "$id"));
        echo "sukses";
//		redirect('/master/biro_tour_travel/');
	}
    
    function edit($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){

			$id 		    = $this->uri->segment(4);
	    	$detail         = $this->tour_leadermodel->getDetail($id);//print_r($detail);
	    	$data['detail'] = $this->tour_leadermodel->getDetail($id);
            $data =  array(
                'label'  => "Edit Member",
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'tr3'    => "save_edit",
                'detail' => $detail,
                'msg'    => "",
                'Kode'   => $detail->KdTourLeader,
                'Nama'   => $detail->Nama,
                'JenisIdentitas'   => $detail->JenisIdentitas,
                'NoIdentitas'   => $detail->NoIdentitas,
                'Alamat' => $detail->Alamat,
                'Kota'   => $detail->Kota,
                'Phone'  => $detail->Phone,
                'Fax'       => $detail->Fax,
                'Email'     => $detail->Email,
                'Telepon'   => $detail->Telepon,
                'NoRekening'   => $detail->NoRekening,
                'Bank'   => $detail->Bank,
                'Penerima'   => $detail->Penerima,
                'typenya'   => $detail->KdTypeTourLeader,
                'statusnya'    => $detail->Aktif,
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                'TypeLeader'  => $this->tour_leadermodel->getTypeLeader(),
                'TypeLeadernya' => $detail->KdTypeTourLeader,
                'BiroTour'  => $this->tour_leadermodel->getBiroTour(),
                'BiroTournya'   => $detail->KdTravel
            );
	    	$data['edit'] 	= true;
			$data['track']  = $mylib->print_track();
	    	$this->load->view('master/tourleader/edit', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_edit(){
        //print_r($_POST);die();
        $nama = strtoupper(addslashes(trim($this->input->post('Nama'))));
        $JenisIdentitas = strtoupper(addslashes(trim($this->input->post('v_JenisIdentitas'))));
        $NoIdentitas = strtoupper(addslashes(trim($this->input->post('v_NoIdentitas'))));
        $id = strtoupper(addslashes(trim($this->input->post('Kode'))));
        $Phone = strtoupper(addslashes(trim($this->input->post('Phone'))));
        $Fax = strtoupper(addslashes(trim($this->input->post('Fax'))));
        $alamat = strtoupper(addslashes(trim($this->input->post('Alamat'))));
        $kota = strtoupper(addslashes(trim($this->input->post('Kota'))));
        $telp = strtoupper(addslashes(trim($this->input->post('Telepon'))));
        $Email = strtoupper(addslashes(trim($this->input->post('Email'))));
        $status = strtoupper(addslashes(trim($this->input->post('status'))));
        $typeleader = strtoupper(addslashes(trim($this->input->post('typeleader'))));
        $travelleader = strtoupper(addslashes(trim($this->input->post('travelleader'))));
        $norekening = strtoupper(addslashes(trim($this->input->post('NoRekening'))));
        $bank = strtoupper(addslashes(trim($this->input->post('Bank'))));
        $penerima = strtoupper(addslashes(trim($this->input->post('Penerima'))));

        $tgltrans = $this->session->userdata('Tanggal_Trans');
        $data = array(
            'KdTravel'  => $travelleader,
            'KdTypeTourLeader'  => $typeleader,
            'Nama'      => $nama,
            'NoIdentitas'      => $NoIdentitas,
            'JenisIdentitas'      => $JenisIdentitas,
            'Phone'     => $Phone,
            'Fax'       => $Fax,
            'Alamat'    => $alamat,
            'Email'     => $Email,
            'Kota'      => $kota,
            'Telepon'   => $telp,
            'NoRekening' => $norekening,
            'Bank'		=> $bank,
            'Penerima' => $penerima,
            'Aktif'     => $status,
            'EditDate'   => $tgltrans,
        );
		$this->db->update('tourleader', $data, array('KdTourLeader' => $id));
    	redirect('/master/tour_leader/');
    }
	
    function save(){
        $nama   = strtoupper(addslashes(trim($this->input->post('Nama'))));
        $v_JenisIdentitas   = strtoupper(addslashes(trim($this->input->post('v_JenisIdentitas'))));
        $v_NoIdentitas   = strtoupper(addslashes(trim($this->input->post('v_NoIdentitas'))));
        $idnama = substr($nama,0,2);
        $id     = $idnama.substr("00000".$this->tour_leadermodel->getidcounter($idnama)->Counter,-4);
    	$type   = strtoupper(addslashes(trim($this->input->post('type'))));
    	$Phone  = strtoupper(addslashes(trim($this->input->post('Phone'))));
    	$Fax    = strtoupper(addslashes(trim($this->input->post('Fax'))));
        $alamat = strtoupper(addslashes(trim($this->input->post('Alamat'))));
        $kota   = strtoupper(addslashes(trim($this->input->post('Kota'))));
        $telp   = strtoupper(addslashes(trim($this->input->post('Telepon'))));
        $Email  = strtoupper(addslashes(trim($this->input->post('Email'))));
        $status = strtoupper(addslashes(trim($this->input->post('status'))));
        $typeleader = strtoupper(addslashes(trim($this->input->post('typeleader'))));
        $travelleader = strtoupper(addslashes(trim($this->input->post('travelleader'))));
        $norekening = strtoupper(addslashes(trim($this->input->post('NoRekening'))));
        $bank = strtoupper(addslashes(trim($this->input->post('Bank'))));
        $penerima = strtoupper(addslashes(trim($this->input->post('Penerima'))));

    	$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
                'KdTourLeader'  => $id,
                'KdTravel'  => $travelleader,
                'KdTypeTourLeader'  => $typeleader,
                'Nama'      => $nama,
                'JenisIdentitas'      => $v_JenisIdentitas,
                'NoIdentitas'      => $v_NoIdentitas,
                'Phone'     => $Phone,
                'Fax'       => $Fax,
                'Alamat'    => $alamat,
                'Email'     => $Email,
                'Kota'      => $kota,
                'Telepon'   => $telp,
                'NoRekening' => $norekening,
            	'Bank'		=> $bank,
            	'Penerima' => $penerima,
                'Aktif'     => $status,
                'ADDDATE'   => $tgltrans,
                'EditDate'   => $tgltrans
            );
            $this->db->insert('tourleader', $data);
		redirect('/master/tour_leader/');

	}
}
?>