<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Sub_kategori extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/subkategori_model');   
        $this->load->model('globalmodel');   
    }
	
	function index(){
		$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "subkategori", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                }
            }

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/master/sub_kategori/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/master/sub_kategori/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $config['total_rows'] = $this->subkategori_model->num_data_row($arr_search["search"]);
            $data['data'] = $this->subkategori_model->getDataList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('master/subkategori/listsubkategori', $data);
        } else {
            $this->load->view('denied');
        }
	}
	
	function search() {
        $mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'subkategori', 'AddUser' => $user));

        $search_value = "";
        $search_value .= "search_keyword=" . $mylib->save_char($this->input->post('search_keyword'));

        $data = array(
            'query_string' => $search_value,
            'module' => "subkategori",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('master/sub_kategori/index/' . $query_id . '');
    }
	
	function add(){
		
	}
}
?>