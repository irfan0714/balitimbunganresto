<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class member extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/membermodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/member/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/member/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/member/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->membermodel->num_member_row($id,$with);
	        $data['member_data']  = $this->membermodel->get_member_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/member/member_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
			$data['nick']	 = "";
			$data['alamat1'] = "";
			$data['alamat2'] = "";
			$data['kota']	 = "";
			$data['kotalahir'] = "";
			$data['tgllahir']= "";
			$data['telp']= "";
			$data['email']= "";
			$data['twitter']= "";
	     	$data['tipe']  = $this->membermodel->getTipe();
	     	$data['niltipe'] = "";
	    	$this->load->view('master/member/add_member',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_member($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_member'] = $this->membermodel->getDetail($id);
	    	$data['tipe'] 	  = $this->membermodel->getTipe();
	    	$data['edit'] 		  = false;
	    	$this->load->view('master/member/view_edit_member', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_member($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_member'] = $this->membermodel->getDetail($id);
			$data['cekAda'] = $this->membermodel->cekDelete($id);
	    	$this->load->view('master/member/delete_member', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('member', array('KdMember' => $id));
		redirect('/master/member/');
	}

    function edit_member($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_member'] = $this->membermodel->getDetail($id);
	    	$data['tipe'] 	  = $this->membermodel->getTipe();
	    	$data['edit'] 		 = true;
	    	$this->load->view('master/member/view_edit_member', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_member(){
    	$mylib = new globallib();
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$nick 	 = strtoupper(addslashes(trim($this->input->post('nick'))));
		$alamat1 = strtoupper(addslashes(trim($this->input->post('alamat1'))));
		$alamat2 = strtoupper(addslashes(trim($this->input->post('alamat2'))));
		$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
		$kotalahir = strtoupper(addslashes(trim($this->input->post('kotalahir'))));
		$tgllahir= $this->input->post('tgllahir');
		$telp 	 = trim($this->input->post('telp'));
		$email 	 = strtoupper(addslashes(trim($this->input->post('email'))));
		$twitter = strtoupper(addslashes(trim($this->input->post('twitter'))));
    	$tipe    = strtoupper(trim($this->input->post('tipe')));
		if($tgllahir==''){ $tgllahir = "0000-00-00"; }
    	else
		{
			$tgllahir = $mylib->ubah_tanggal($tgllahir);
		}
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaMember' => $nama,
			  'NickName' => $nick,
			  'Alamat1' => $alamat1,
			  'Alamat2' => $alamat2,
			  'Kota' => $kota,
			  'KotaLahir' => $kotalahir,
			  'TglLahir' => $tgllahir,
			  'Telp' => $telp,
			  'Email' => $email,
			  'Twitter' => $twitter,
              'KdTipemember' => $tipe ,
              'EditDate'		=> $tgltrans
			);
		$this->db->update('member', $data, array('KdMember' => $id));
    	redirect('/master/member/');
    }
    function save_new_member(){
	    $mylib = new globallib();
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$nick 	 = strtoupper(addslashes(trim($this->input->post('nick'))));
		$alamat1 = strtoupper(addslashes(trim($this->input->post('alamat1'))));
		$alamat2 = strtoupper(addslashes(trim($this->input->post('alamat2'))));
		$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
		$kotalahir = strtoupper(addslashes(trim($this->input->post('kotalahir'))));
		$tgllahir= $this->input->post('tgllahir');
		$telp 	 = trim($this->input->post('telp'));
		$email 	 = strtoupper(addslashes(trim($this->input->post('email'))));
		$twitter = strtoupper(addslashes(trim($this->input->post('twitter'))));
    	$tipe    = strtoupper(trim($this->input->post('tipe')));
		if($tgllahir==''){ $tgllahir = "0000-00-00"; }
    	else
		{
			$tgllahir = $mylib->ubah_tanggal($tgllahir);
		}
    	$num 	 = $this->membermodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdMember'   => $id,
               'NamaMember' => $nama,
			   'NickName' => $nick,
			   'Alamat1' => $alamat1,
			   'Alamat2' => $alamat2,
			   'Kota' => $kota,
			   'KotaLahir' => $kotalahir,
			   'TglLahir' => $tgllahir,
			   'Telp' => $telp,
			   'Email' => $email,
			   'Twitter' => $twitter,
               'KdTipemember' => $tipe ,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('member', $data);
			redirect('master/member');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nick']	 = $this->input->post('nick');
			$data['alamat1'] = $this->input->post('alamat1');
			$data['alamat2'] = $this->input->post('alamat2');
			$data['kota']	 = $this->input->post('kota');
			$data['kotalahir'] = $this->input->post('kotalahir');
			$data['tgllahir']= $this->input->post('tgllahir');
			$data['telp']= $this->input->post('telp');
			$data['email']= $this->input->post('email');
			$data['twitter']= $this->input->post('twitter');
			$data['niltipe'] = $tipe;
			
			$data['tipe']  = $this->membermodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/member/add_member', $data);
		}
	}
}
?>