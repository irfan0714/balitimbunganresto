<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Gsa extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/gsamodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			//
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/gsa/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/gsa/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/gsa/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->gsamodel->num_gsa_row($id,$with);
	        $data['gsa_data']  = $this->gsamodel->get_gsa_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/gsa/gsa_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$data['gsa']  = $this->gsamodel->getSupervisor();
			$data['warna']  = $this->gsamodel->getWarna();
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/gsa/add_gsa',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_gsa'] = $this->gsamodel->getdata($id);
	    	$data['gsa']  = $this->gsamodel->getSupervisor();
			$data['warna']  = $this->gsamodel->getWarna();
	    	$data['edit'] 		 = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/gsa/view_edit_gsa', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	    	$this->db->delete('gsa_list',array('KdGsa'=>$id));
	    	redirect('/master/gsa/');
    	}
		else{
			$this->load->view('denied');
		}
    }

    function edit_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_gsa'] = $this->gsamodel->getdata($id);
	    	$data['gsa']  = $this->gsamodel->getSupervisor();
			$data['warna']  = $this->gsamodel->getWarna();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/gsa/view_edit_gsa', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_gsa(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$Kdgsa	 = $this->input->post('KdGsa');
    	$employee_gsa 	 = $this->input->post('gsa');
		$warna	 = $this->input->post('warna');
	    $namaemployee  = $this->gsamodel->getEmployee($employee_gsa);
    	$user = $this->session->userdata('username');
    	
    	$data = array(
    				 'employee_id'=>$employee_gsa,
    				 'nama'=>$namaemployee[0]['employee_name'],
    				 'warna'=>$warna,
    				 'edituser'=>$user,
    				 'editdate'=>date('Y-m-d')
    				 );
    				 
    	$this->db->update('gsa_list',$data,array('KdGsa'=>$Kdgsa));
    	redirect('master/gsa');
    }
    
    
    function save_new_gsa(){
    	
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$warna	 = $this->input->post('warna');
	    $namaemployee  = $this->gsamodel->getEmployee($employee_gsa);
    	$user = $this->session->userdata('username');
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'Kdgsa'   => $id,
               'Namagsa' => $nama,
			   'KdPersonal' => $personal,
               'KdTipegsa' => $tipe,
			   'KdGudang' => $gudang,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('gsa', $data);
			redirect('master/gsa');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilpersonal'] = $personal;
			$data['personal']  = $this->gsamodel->getPersonal();
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->gsamodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/gsa/add_gsa', $data);
		}
	}
	
	function save_new_gsa_new(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$employee_gsa 	 = $this->input->post('gsa');
    	$warna	 = $this->input->post('warna');
	    $namaemployee  = $this->gsamodel->getEmployee($employee_gsa);
    	$user = $this->session->userdata('username');
    	
    	$data = array(
    				 'employee_id'=>$employee_gsa,
    				 'nama'=>$namaemployee[0]['employee_name'],
    				 'warna'=>$warna,
    				 'adduser'=>$user,
    				 'adddate'=>date('Y-m-d')
    				 );
    				 
    	$this->db->insert('gsa_list',$data);
    	redirect('master/gsa');
	}
	
}
?>