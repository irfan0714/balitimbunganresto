<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class typecust extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/typecustmodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);	     	
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/typecust/index/';
			$page					  = $this->uri->segment(4);		
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/typecust/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/typecust/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			
	        $config['total_rows']     = $this->typecustmodel->num_typecust_row($id,$with);
	        $data['typecustdata']       = $this->typecustmodel->getTypecustList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/typecust/viewtypecustlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  = "";
	     	$data['id']   = "";
	     	$data['nama'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/typecust/addtypecust',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function view_typecust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewtypecust'] = $this->typecustmodel->getDetail($id);
	    	$data['edit'] 		= false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/typecust/viewedittypecust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_typecust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewtypecust'] = $this->typecustmodel->getDetail($id);
			$data['cekAda'] = $this->typecustmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/typecust/deletetypecust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('typecust', array('KdTypecust' => $id));
		redirect('/master/typecust/');
	}
    
    function edit_typecust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
	    	$data['viewtypecust'] = $this->typecustmodel->getDetail($id);
	    	$data['edit'] 		= true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/typecust/viewedittypecust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_typecust(){
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$id   = addslashes($this->input->post('kode'));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$data = array(
    		  'NamaTypecust'	=> $nama,
              'EditDate'	=> $tgltrans
			);
		$this->db->update('typecust', $data, array('KdTypecust' => $id));
    	redirect('/master/typecust/');
    }
    function save_new_typecust(){
		$id   = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$num  = $this->typecustmodel->get_id($id);
    	if($num!=0){
			$data['id']   = $this->input->post('kode');
			$data['nama'] = $this->input->post('nama');
			$data['msg']  = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/typecust/addtypecust', $data);
		}
		else{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdTypecust'   => $id ,
               'NamaTypecust' => $nama ,
               'AddDate'    => $tgltrans
            );
            $this->db->insert('typecust', $data);
			redirect('/master/typecust/');
		}
	}
}
?>