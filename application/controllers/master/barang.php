<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class barang extends authcontroller {
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/barangmodel');
    }

    function index()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr  		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with		  = $this->input->post('searchby');
	     	$this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/barang/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/barang/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/barang/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows'] = $this->barangmodel->num_barang_row($id,$with);
	        $data['barangdata']   = $this->barangmodel->getbarangList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Barang","Nama Barang","Divisi","Kateogori","Brand","Satuan Jual","Harga Jual","Harga Beli","PPn","Tipe","Status");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/barang/viewbaranglist', $data);
	    }
		else
		{
			$this->load->view('denied');
		}
    }

    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	   = "";
	     	$data['id']		   = "";
			$data['nstruk']    = "";
	     	$data['mdivisi']   = $this->barangmodel->getDivisi();
	     	$data['divisi']	   = "";
            $data['subdiv']	   = "";
            $data['subdivisi']	= "";
	     	$data['mkategori'] = $this->barangmodel->getKategori();
	     	$data['kategori']  = "";
	     	$data['subkategori']  = "";
	     	$data['mbrand']    = $this->barangmodel->getBrand();
	     	$data['brand']     = "";
	     	$data['subbrand']  = "";
	     	$data['msatuan']   = $this->barangmodel->getSatuan();
			$data['satuanst']  = "";
			$data['tipe']      = $this->barangmodel->getTipeStock();
	     	$data['niltipe']   = "";
			$data['status']    = $this->barangmodel->getStatus();
            $data['mkomisi']    = $this->barangmodel->getKomisi();
            $data['komisi']    = "";
            $data['service_charge']    = "0";
	     	$data['nilstatus'] = "";
			$data['mformula']  = $this->barangmodel->getFormula();
	     	$data['formula']   = "";
	     	$data['satuanbl']  = "";
			$data['konvblst']  = 0;
			$data['satuan1']  = "";
	     	$data['konv1st']  = 0;
			$data['harga1c']  = 0;
			$data['harga1b']  = 0;
			$data['satuan2']  = "";
	     	$data['konv2st']  = 0;
			$data['harga2c']  = 0;
			$data['harga2b']  = 0;
			$data['satuan3']  = "";
	     	$data['konv3st']  = 0;
			$data['harga3c']  = 0;
			$data['harga3b']  = 0;	
			$data['persenpajak']  = 10;
            $data['barcodek']  = "";
            $data['track'] = $mylib->print_track();
	    	$this->load->view('master/barang/addbarang',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_barang($id)
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewbarang'] = $this->barangmodel->getHeader($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/barang/deletebarang', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This()
    {
     	$id = $this->input->post('kode');
     	$this->db->delete('masterbarang', array('PCode' => $id));
		redirect('/master/barang/');
	}

    function edit_barang($id)
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id    = $this->uri->segment(4);
	    	$value = $this->barangmodel->getHeader($id);
			$data  = $this->isi_data(
				"true",
				"edit",
				$id,
				$value->NamaStruk,
				$value->NamaInitial,
				$value->SatuanSt,
				$value->SatuanBl,
				$value->KonvBlSt,
				$value->Harga0b,
				$value->Satuan1,
				$value->Konv1st,
				$value->Harga1c,
				$value->Harga1t,
				$value->Harga1b,
				$value->Satuan2,
				$value->Konv2st,
				$value->Harga2c,
				$value->Harga2t,
				$value->Harga2b,
				$value->Satuan3,
				$value->Konv3st,
				$value->Harga3c,
				$value->Harga3t,
				$value->Harga3b,
				$value->JenisBarang,
				$value->Barcode1,
			  	$value->Barcode2,
			  	$value->Barcode3,
			  	$value->KdDivisi,
			  	$value->KdSubDivisi,
			  	$value->KdKategori,
			  	$value->KdSubKategori,
			  	$value->KdBrand,
			  	$value->KdSubBrand,
			  	$value->Panjang,
			  	$value->Lebar,
			  	$value->Tinggi,
			  	$value->HargaBeli,
			  	$value->PersenPajak,
			  	$value->Tipe,
			  	$value->Status,
			  	$value->KdKomisi,
			  	$value->Service_charge,
			  	$value->JenisPajak,
			  	$value->HPP,
			  	$value->StatusPajak,
			  	$value->FlagReady,
			  	$value->FlagStock,
			  	$value->Printer,
			  	$value->Jenis,
			  	$value->komisi
			);
			$data['edit'] = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/barang/vieweditbarang', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_barang($id)
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id    = $this->uri->segment(4);
	    	$value = $this->barangmodel->getHeader($id);
			$data  = $this->isi_data("true","edit",$id,$value->NamaStruk,
			$value->KdDivisi,$value->KdKategori,$value->KdBrand,
			$value->SatuanSt,$value->SatuanBl,$value->KonvBlSt,
			$value->Satuan1,$value->Konv1st,$value->Harga1c,$value->Harga1b,
			$value->Satuan2,$value->Konv2st,$value->Harga2c,$value->Harga2b,
			$value->Satuan3,$value->Konv3st,$value->Harga3c,$value->Harga3b,
			$value->PersenPajak,$value->Tipe,$value->Status);
			$data['edit'] = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/barang/vieweditbarang', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_barang()
    {
		$mylib = new globallib();
    	$id 	 	= strtoupper(addslashes(trim($this->input->post('kode'))));
		$nstruk 	= strtoupper(addslashes(trim($this->input->post('nstruk'))));
		$nama_initial 	= strtoupper(addslashes(trim($this->input->post('nama_initial'))));
        //$divisi 	= $this->input->post('divisi');
        //$subdivisi 	= $this->input->post('subdivisi');
        //$kategori 	= $this->input->post('kategori');
        //$subkategori= $this->input->post('subkategori');
        $brand 		= $this->input->post('brand');
        $subbrand 	= $this->input->post('subbrand');
		$satuanst 	= trim($this->input->post('satuanst'));
		$harga1c 	= $this->input->post('harga1c');
		$harga1b 	= $this->input->post('harga1b');
		$konvblst 	= $this->input->post('konvblst');
		$satuan2 	= trim($this->input->post('satuan2'));
		$konv2st 	= $this->input->post('konv2st');
		$harga2c 	= $this->input->post('harga2c');
		$harga2b 	= $this->input->post('harga2b');
		$satuan3 	= trim($this->input->post('satuan3'));
		$konv3st 	= $this->input->post('konv3st');
		$harga3c 	= $this->input->post('harga3c');
		$harga3b 	= $this->input->post('harga3b');
		$persenpajak= $this->input->post('persenpajak');
		$barcodek 	= trim($this->input->post('barcodek'));
		$tipe 	= trim($this->input->post('tipe'));
		$status 	= trim($this->input->post('status'));
		$komisi 	= trim($this->input->post('komisi'));
		$service_charge 	= trim($this->input->post('service_charge'));
		$tgl = $this->session->userdata('Tanggal_Trans');
		
		//if($konv1st=="")$konv1st=0;
		if($harga1c=="")$harga1c=0;
		if($harga1b=="")$harga1b=0;
		if($konv2st=="")$konv2st=0;
		if($harga2c=="")$harga2c=0;
		if($harga2b=="")$harga2b=0;
		if($konv3st=="")$konv3st=0;
		if($harga3c=="")$harga3c=0;
		if($harga3b=="")$harga3b=0;
		if($persenpajak=="")$persenpajak=0;

    	$data = array(
				'NamaLengkap' => $nstruk,
				'NamaStruk'	=> $nstruk,
				'NamaInitial' => $nama_initial,
				//'KdDivisi'	=> $divisi,
				//'KdKategori'=> $kategori,
				'KdBrand'	=> $brand,
				'KdSubBrand'	=> $subbrand,
				'SatuanSt' => $satuanst,
				'Harga1c' => $harga1c,
				'Harga1b' => $harga1b,
				'KonvBlSt' => $konvblst,
				'Satuan2' => $satuan2,
				'Konv2st' => $konv2st,
				'Harga2c' => $harga2c,
				'Harga2b' => $harga2b,
				'Satuan3' => $satuan3,
				'Konv3st' => $konv3st,
				'Harga3c' => $harga3c,
				'Harga3b' => $harga3b,
				'PersenPajak' => $persenpajak,
				'Barcode1' => $barcodek,
				'Tipe' => $tipe,
				'Status' => $status,
				'KdKomisi' => $komisi,
				'Service_charge' => $service_charge,
				'EditDate' => $tgl
            );
		$this->db->update('masterbarang', $data, array('PCode' => $id));
    	redirect('/master/barang/');
    }

	function get_no_counter( $table_name, $col_primary, $subdivisi,$kategori,$subkategori)
    {
        $query = "
	        SELECT
	            " . $table_name . "." . $col_primary . "
	        FROM
	            " . $table_name . "
	        WHERE
	            1
	            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1, 2) = '" .$subdivisi. "'
	            AND SUBSTR(" . $table_name . "." . $col_primary . ", 3, 1) = '" .$kategori. "'
	            AND SUBSTR(" . $table_name . "." . $col_primary . ", 4, 2) = '" .$subkategori. "'
	        ORDER BY
	            " .$table_name . "." . $col_primary . " DESC
	        LIMIT
	            0,1
	    ";
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 6, 5) * 1) + 1;
        $counter_fa = sprintf("%02s", $subdivisi) . $kategori . sprintf("%02s", $subkategori) . sprintf("%04s", $counter);

        return $counter_fa;

    }

    function save_new_barang()
    {
		$mylib = new globallib();

		$nstruk 	= strtoupper(addslashes(trim($this->input->post('nstruk'))));
		$nama_initial 	= strtoupper(addslashes(trim($this->input->post('nama_initial'))));
		$divisi 	= $this->input->post('divisi');
        $subdivisi 	= $this->input->post('subdivisi');
    	$kategori 	= $this->input->post('kategori');
        $subkategori= $this->input->post('subkategori');
        $brand 		= $this->input->post('brand');
        $subbrand 	= $this->input->post('subbrand');
        $kdkomisi 	= $this->input->post('komisi');
     	$satuanst 	= trim($this->input->post('satuanst'));
		$satuanbl 	= trim($this->input->post('satuanbl'));
		$konvblst 	= $this->input->post('konvblst');
    	$satuan1 	= trim($this->input->post('satuan1'));
		$konv1st 	= trim($this->input->post('konv1st'));
		$harga1c 	= trim($this->input->post('harga1c'));
		$harga1b 	= trim($this->input->post('harga1b'));
		$satuan2 	= trim($this->input->post('satuan2'));
		$konv2st 	= trim($this->input->post('konv2st'));
		$harga2c 	= trim($this->input->post('harga2c'));
		$harga2b 	= trim($this->input->post('harga2b'));
		$satuan3 	= trim($this->input->post('satuan3'));
		$konv3st 	= trim($this->input->post('konv3st'));
		$harga3c 	= trim($this->input->post('harga3c'));
        $barcode1 	= trim($this->input->post('barcodek'));
        $service_charge 	= trim($this->input->post('service_charge'));

		$harga3b 	= trim($this->input->post('harga3b'));
		$persenpajak= $this->input->post('persenpajak');
		$tipe 	    = trim($this->input->post('tipe'));
		$status 	= trim($this->input->post('status'));
        $id         = $this->get_no_counter('masterbarang','PCode',$subdivisi,$kategori,$subkategori);
    	$num	    = $this->barangmodel->get_id($id);
		$tgl        = $this->session->userdata('Tanggal_Trans');
		
		$save_barcode = $id;
        
		if($konv1st=="")$konv1st=0;
		if($harga1c=="")$harga1c=0;
		if($harga1b=="")$harga1b=0;
		if($konv2st=="")$konv2st=0;
		if($harga2c=="")$harga2c=0;
		if($harga2b=="")$harga2b=0;
		if($konv3st=="")$konv3st=0;
		if($harga3c=="")$harga3c=0;
		if($harga3b=="")$harga3b=0;
		if($persenpajak=="")$persenpajak=0;
		
    	if($num == 0)
		{
            $data = array(
                'PCode'         => $id,
                'NamaLengkap'   => $nstruk,
                'NamaStruk'     => $nstruk,
                'NamaInitial'   => $nama_initial,
                'SatuanSt'      => $satuanst,
                'SatuanBl'      => $satuanst,
                'KonvBlSt'      => 1,
                'Satuan1'       => $satuanst,
                'Konv1st'       => 1,
                'Harga1c'       => $harga1c,
                'Harga1b'       => $harga1b,
                'Harga1t'       => 0,
                'Satuan2'       => $satuan2,
                'Konv2st'       => $konv2st,
                'Harga2c'       => $harga2c,
                'Harga2t'       => 0,
                'Harga2b'       => $harga2b,
                'Satuan3'       => $satuan3,
                'Konv3st'       => $konv3st,
                'Harga3c'       => $harga3c,
                'Harga3t'       => 0,
                'Harga3b'       => $harga3b,
                'Barcode1'      => $save_barcode,
                'KdDivisi'      => $divisi,
                'KdSubDivisi'   => $subdivisi,
                'KdKategori'    => $kategori,
                'KdSubKategori'    => $subkategori,
                'KdBrand'       => $brand,
                'KdSubBrand'       => $subbrand,
                'PersenPajak'   => $persenpajak,
                'service_charge'    => $service_charge,
                'KdKomisi'      => $kdkomisi,
                    'Tipe'      => $tipe,
                    'Status'    => $status,
                    'AddDate'	=> $tgl
            );
			$this->db->insert('masterbarang', $data);
			redirect('master/barang');
		}
		else
		{
		 	$msg 	   = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
	     	$id		   = $this->input->post('kode');
			$nstruk    = $this->input->post('nstruk');
	     	$ninitial  = $this->input->post('ninitial');
	     	$namaarray = "msg";
		 	$data 	   = $this->isi_data($msg,$namaarray,$id,$nstruk,
			$divisi,$kategori,$brand,$satuanst,$satuanbl,$konvblst,
			$satuan1,$konv1st,$harga1c,$harga1b,
			$satuan2,$konv2st,$harga2c,$harga2b,
			$satuan3,$konv3st,$harga3c,$harga3b,$persenpajak,$tipe,$status);
			$this->load->view('master/barang/addbarang', $data);
		}
	}

	function getSubDivisiBy()
	{
		$divisi = $this->input->post("divisi");
		$data   = $this->barangmodel->getSubDivBy($divisi);
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['KdSubDivisi']."'>".stripslashes($data[$a]['NamaSubDivisi'])."</option>";
		}
	}

	function getSubKategoriBy()
	{
		$kategori = $this->input->post("kategori");
		$data     = $this->barangmodel->getSubKatBy($kategori);
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['KdSubKategori']."'>".stripslashes($data[$a]['NamaSubKategori'])."</option>";
		}
	}

	function getSubBrandBy()
	{
		$brand  = $this->input->post("brand");
		$data   = $this->barangmodel->getSubBrandBy($brand);
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['KdSubBrand']."'>".stripslashes($data[$a]['NamaSubBrand'])."</option>";
		}
	}
	
	function isi_data(
		$msg,
		$namaarray,
		$id,
		$nstruk,
		$NamaInitial,
		$SatuanSt,
		$SatuanBl,
		$KonvBlSt,
		$Harga0b,
		$Satuan1,
		$Konv1st,
		$Harga1c,
		$Harga1t,
		$Harga1b,
		$Satuan2,
		$Konv2st,
		$Harga2c,
		$Harga2t,
		$Harga2b,
		$Satuan3,
		$Konv3st,
		$Harga3c,
		$Harga3t,
		$Harga3b,
		$JenisBarang,
		$Barcode1,
	  	$Barcode2,
	  	$Barcode3,
	  	$KdDivisi,
	  	$KdSubDivisi,
	  	$KdKategori,
	  	$KdSubKategori,
	  	$KdBrand,
	  	$KdSubBrand,
	  	$Panjang,
	  	$Lebar,
	  	$Tinggi,
	  	$HargaBeli,
	  	$PersenPajak,
	  	$Tipe,
	  	$Status,
	  	$KdKomisi,
	  	$Service_charge,
	  	$JenisPajak,
	  	$HPP,
	  	$StatusPajak,
	  	$FlagReady,
	  	$FlagStock,
	  	$Printer,
	  	$Jenis,
	  	$komisi
	  )
	{
		$data[$namaarray]  = $msg;
     	$data['id']		   = $id;
		$data['nstruk']    = $nstruk;
		$data['namainitial']    = $NamaInitial;
     	$data['mdivisi']   = $this->barangmodel->getDivisi();
     	$data['divisi']	   = $KdDivisi;
     	$data['msubdivisi']= $this->barangmodel->getSubDivBy();
     	$data['subdivisi'] = $KdSubDivisi;
     	$data['mkategori'] = $this->barangmodel->getKategori();
     	$data['kategori']  = $KdKategori;
     	$data['msubkategori']= $this->barangmodel->getSubKatBy();
     	$data['subkategori']  = $KdSubKategori;
     	$data['mbrand']    = $this->barangmodel->getBrand();
     	$data['brand']     = $KdBrand;
     	$data['msubbrand']= $this->barangmodel->getSubBrandBy();
     	$data['subbrand']  = $KdSubBrand;
      	$data['msatuan']   = $this->barangmodel->getSatuan();
		$data['satuanst']  = $SatuanSt;
	    $data['satuanbl']  = $SatuanBl;
		$data['konvblst']  = $KonvBlSt;
		$data['satuan1']  = $Satuan1;
	    $data['konv1st']  = $Konv1st;
		$data['harga1c']  = $Harga1c;
		$data['harga1b']  = $Harga1b;
		$data['satuan2']  = $Satuan2;
	    $data['konv2st']  = $Konv2st;
		$data['harga2c']  = $Harga2c;
		$data['harga2b']  = $Harga2b;
		$data['satuan3']  = $Satuan3;
	    $data['konv3st']  = $Konv3st;
		$data['harga3c']  = $Harga3c;
		$data['harga3b']  = $Harga3b;	
		$data['persenpajak']  = $PersenPajak;
		$data['barcodek']  = $Barcode1;
		$data['tipe']     = $this->barangmodel->getTipeStock();
	    $data['niltipe']  = $Tipe;
		$data['status']     = $this->barangmodel->getStatus();
	    $data['nilstatus']  = $Status;
	    $data['jenis']  = $Jenis;
        $data['mkomisi']    = $this->barangmodel->getKomisi();
	    $data['kdkomisi']  = $KdKomisi;
	    $data['service_charge']  = $Service_charge;
     	return $data;
	}
}
?>