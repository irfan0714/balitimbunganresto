<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class saldoawal extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/saldoawalmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/saldoawal/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/saldoawal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/saldoawal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['tahunbulan']  = $this->saldoawalmodel->getTahunBulan()->TahunBulan;
	        $config['total_rows']  = $this->saldoawalmodel->num_saldoawal_row($id,$with,$data['tahunbulan']);
	        $data['saldoawal_data']  = $this->saldoawalmodel->get_saldoawal_List($config['per_page'],$page,$id,$with,$data['tahunbulan']);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/saldoawal/saldoawal_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	
	function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
			$data['departemen']  = $this->saldoawalmodel->getDepartemen();
	     	$data['nildepartemen'] = "";
	     	$data['rekening']  = $this->saldoawalmodel->getRekening();
	     	$data['nilrekening'] = "";
			$data['saldoawal']	 = 0;
			$data['track'] = $mylib->print_track();
			$data['tahunbulan']  = $this->saldoawalmodel->getTahunBulan()->TahunBulan;
	    	$this->load->view('master/saldoawal/add_saldoawal',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_saldoawal(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$departemen 			  = $this->uri->segment(4);
			$rekening 				  = $this->uri->segment(5);
			$data['tahunbulan']  = $this->saldoawalmodel->getTahunBulan()->TahunBulan;
	    	$data['view_saldoawal'] = $this->saldoawalmodel->getDetail($departemen,$rekening,$data['tahunbulan']);
			
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/saldoawal/view_edit_saldoawal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function edit_saldoawal(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$departemen 			  = $this->uri->segment(4);
			$rekening 				  = $this->uri->segment(5);
			$data['tahunbulan']  = $this->saldoawalmodel->getTahunBulan()->TahunBulan;
	    	$data['view_saldoawal'] = $this->saldoawalmodel->getDetail($departemen,$rekening,$data['tahunbulan']);
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/saldoawal/view_edit_saldoawal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_saldoawal(){
    	 $mylib = new globallib();
		$departemen 	 = strtoupper(addslashes(trim($this->input->post('departemen'))));
    	$rekening 	 = strtoupper(addslashes(trim($this->input->post('rekening'))));
		$nilaisaldoawal = $this->input->post('nilaisaldoawal');
		$tahunbulan = $this->input->post('tahunbulan');
		$tahun = substr($tahunbulan,0,4);
		$bulan = substr($tahunbulan,-2);
		$user = $this->session->userdata('userid');
		$saldo = 'Awal'.$bulan;
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  $saldo	=> $nilaisaldoawal,
              'EditDate' => $tgltrans
			);
		$this->db->update('saldo_gl', $data, array('Departemen' => $departemen,'Tahun' => $tahun,'KdRekening' => $rekening));
    	redirect('/master/saldoawal/');
    }
	
    function save_new_saldo(){
	    $mylib = new globallib();
		$departemen 	 = strtoupper(addslashes(trim($this->input->post('departemen'))));
    	$rekening 	 = strtoupper(addslashes(trim($this->input->post('rekening'))));
		$nilaisaldoawal = $this->input->post('nilaisaldoawal');
		$tahunbulan = $this->input->post('tahunbulan');
		$tahun = substr($tahunbulan,0,4);
		$bulan = substr($tahunbulan,-2);
		$user = $this->session->userdata('userid');
		$saldo = 'Awal'.$bulan;
    	$num 	 = $this->saldoawalmodel->cekdata($departemen,$rekening,$tahun);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'Departemen'   => $departemen,
               'KdRekening'	=> $rekening,
    		   'Tahun' => $tahun,
			   $saldo => $nilaisaldoawal,
               'AddDate' 	=> $tgltrans,
			   'AddNama' 	=> $user
            );
            $this->db->insert('saldo_gl', $data);
			redirect('master/saldoawal');
		}
		else
		{
		
			$data['msg']	 = "";
			$data['departemen']  = $this->saldoawalmodel->getDepartemen();
	     	$data['nildepartemen'] = $departemen;
	     	$data['rekening']  = $this->saldoawalmodel->getRekening();
	     	$data['nilrekening'] = $rekening;
			$data['saldoawal']	 = $nilaisaldoawal;
			$data['tahunbulan']  = $tahunbulan;
			$data['track'] = $mylib->print_track();
			$data['msg'] 	 = "<font color='red'><b>Error : Saldo Awal Rekening $rekening Sudah Ada</b></font>";
			$this->load->view('master/saldoawal/add_saldoawal', $data);
		}
	}
}
?>