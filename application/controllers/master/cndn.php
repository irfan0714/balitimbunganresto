<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class cndn extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/cndnmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/cndn/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/cndn/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/cndn/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->cndnmodel->num_cndn_row($id,$with);
	        $data['cndn_data']  = $this->cndnmodel->get_cndn_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/cndn/cndn_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	$data['tipe']  = $this->cndnmodel->getTipe();
	     	$data['niltipe'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/cndn/add_cndn',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_cndn($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_cndn'] = $this->cndnmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->cndnmodel->getTipe();
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/cndn/view_edit_cndn', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_cndn($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_cndn'] = $this->cndnmodel->getDetail($id);
			$data['cekAda'] = $this->cndnmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/cndn/delete_cndn', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('cndn', array('KdCNDN' => $id));
		redirect('/master/cndn/');
	}

    function edit_cndn($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_cndn'] = $this->cndnmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->cndnmodel->getTipe();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/cndn/view_edit_cndn', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_cndn(){
    	$id 	 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaCNDN'	=> $nama,
    		  'KdRekening' => $tipe,
              'EditDate'		=> $tgltrans
			);
		$this->db->update('cndn', $data, array('KdCNDN' => $id));
    	redirect('/master/cndn/');
    }
    function save_new_cndn(){
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
    	$num 	 = $this->cndnmodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdCNDN'   => $id ,
               'NamaCNDN' => $nama ,
               'KdRekening' => $tipe ,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('cndn', $data);
			redirect('master/cndn');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->cndnmodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/cndn/add_cndn', $data);
		}
	}
}
?>