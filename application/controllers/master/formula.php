<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class formula extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/formulamodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/formula/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/formula/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/master/formula/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("Tanggal","PCode","Nama Barang","Kode Formula","Nama Formula","Qty","Satuan","Status");
	        $config['total_rows']		= $this->formulamodel->num_formula_row($id,$with);
	        $data['data']	= $this->formulamodel->getformulaList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/formula/formulalist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->formulamodel->getDate();
			$data['tanggal'] = $aplikasi->TglTrans;
			$data['satuanatas'] = $this->formulamodel->getSatuanAtas();
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/formula/add_formula',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('master/cetak_master/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "formula.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('master/cetak_master/cetak_transaksi_printer', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->formulamodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->formulamodel->getDetail($id);
		$data['judul1'] = array("NoOrder","kdcustomer","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdCustomer." - ".stripslashes($header->Nama),stripslashes($header->Keterangan));
		$data['judul2'] = array("Tanggal");
		$data['niljudul2'] = array($header->Tanggal);
		$data['judullap'] = "Sales Order";
		$data['url'] = "formula/printThis/".$id;
		$data['colspan_line'] = 4;
		$data['tipe_judul_detail'] = array("normal","normal","kanan","normal","normal","kanan","kanan");
		$data['judul_detail'] = array("KdBarang","Nama","Qty","","Disc","Harga","Jumlah");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{			
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['PCode']);
			$list_detail[] = stripslashes($detail[$m]['NamaInitial']);
			//$list_detail[] = $hasil;
			$list_detail[] = $detail[$m]['QtyInput'];
			$list_detail[] = $detail[$m]['NamaSatuan'];
			$list_detail[] = $detail[$m]['Disc1']."+".$detail[$m]['Disc2'];
			$list_detail[] = $detail[$m]['HargaB'];
			$list_detail[] = $detail[$m]['TotalB'];
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Order");//,"PPn Jual (".$header->PPn."%)","Total Order");
		$data['footer2']  = array(round($header->Jumlah));//,round($header->PPn/100*$header->Jumlah),round($header->Total));
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

    function delete_formula(){
     	$kode = $this->input->post('kode');
		$pcode = $this->input->post('pcode');
		$this->formulamodel->locktables('formulaheader,formuladetail');
		$this->db->delete('formulaheader', array( 'kdformula' => $kode, 'pcode' => $pcode ));
		$this->db->delete('formuladetail', array( 'kdformula' => $kode, 'pcode' => $pcode ));
		$this->formulamodel->unlocktables();
	}

    function edit_formula($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
			$pcode = $this->uri->segment(5);
			$aplikasi = $this->formulamodel->getDate();
			$data['header']	= $this->formulamodel->getHeader($id,$pcode);
	    	$data['detail']	= $this->formulamodel->getDetail($id,$pcode);	
			$msatuan1 = $this->formulamodel->getSatuan($pcode);
			$data['msatuan'] = array(array("KdSatuan"=>$msatuan1->Satuan1,"NamaSatuan"=>$msatuan1->Nama1),
			                         array("KdSatuan"=>$msatuan1->Satuan2,"NamaSatuan"=>$msatuan1->Nama2),
									 array("KdSatuan"=>$msatuan1->Satuan3,"NamaSatuan"=>$msatuan1->Nama3));
			$data['satuan'] = $data['header']->satuan;
			
			//print_r($data['msatuan']);echo "<br>";
			//print_r($data['detail']);
			
			if($data['header']->status=="D")
			{
			$data['checkedd']     = "checked='checked'";
			$data['checkeda']     = "";
			$data['checkedt']     = "";
			}
			if($data['header']->status=="A")
			{
			$data['checkeda']     = "checked='checked'";
			$data['checkedd']     = "";
			$data['checkedt']     = "";
			}
			if($data['header']->status=="T")
			{
			$data['checkedt']     = "checked='checked'";
			$data['checkedd']     = "";
			$data['checkeda']     = "";
			}
			$data['track'] = $mylib->print_track();
			$this->load->view('master/formula/edit_formula', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	function getPCode()
	{
		$kode = $this->input->post('pcode');
		$detail = $this->formulamodel->getPCodeDet($kode);
		if(count($detail)!=0)
		{	
            //$nilai = $detail->NamaKategori." ".$detail->NamaBrand." ".$detail->NamaStruk."-".$detail->KdSatuanJual."-".$detail->HargaBeli."-".$detail->HargaJualKecil."-".$detail->HargaJualTengah;
			$nilai = '01*_*'.$detail->PCode.'*_*'.$detail->NamaKategori." ".$detail->NamaBrand." ".$detail->NamaStruk.'*_*'.
			   $detail->SatuanSt.'*_*'.$detail->NamaSatuanSt.'*_*'.
			   $detail->Satuan1.'*_*'.$detail->Nama1.'*_*'.
			   $detail->Satuan2.'*_*'.$detail->Nama2.'*_*'.
			   $detail->Satuan2.'*_*'.$detail->Nama3;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}

	function save_new_formula(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
		$pcodeatas = strtoupper($this->input->post('pcodeatas'));
		$namaformula = strtoupper($this->input->post('namaformula'));
		$status = $this->input->post('sumber');
		$qtyatas = $this->input->post('qtyatas');
		$satuanatas = $this->input->post('satuanatas');
		$pcode1 = $this->input->post('pcode');
		$qty1 = $this->input->post('qty');
		$pcodesave1 = $this->input->post('savepcode');
		$satuan1 = $this->input->post('satuan');
		$konversi1 = $this->input->post('konversi');
		$nilsatuan1 = $this->input->post('nilsatuan');
		if($no=="")
		{
			$counter = "1";
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user);
		}
		else
		{
		   	$counter = $this->updateHeader($flag,$mylib->ubah_tanggal($tgl),$no,$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user);
		}
	
		
		
		for($x=0;$x<count($pcode1)-1;$x++)
		{
			$pcode = strtoupper(addslashes(trim($pcode1[$x])));
			$qty = trim($qty1[$x]);
			$pcodesave = $pcodesave1[$x];
			$satuan = $satuan1[$x];
			$nilsatuan = $nilsatuan1[$x];
			if($pcode!=""){
				$this->insertDetail($flag,$no,$counter,$pcode,$qty,$satuan,$user,$pcodesave,$nilsatuan,$pcodeatas,$status,$qtyatas,$satuanatas);
				$counter++;
			}
		}
		redirect('/master/formula/');
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$pcodeatas = strtoupper($this->input->post('pcodeatas'));
		$namaformula = strtoupper($this->input->post('namaformula'));
		$status = $this->input->post('sumber');
		$qtyatas = $this->input->post('qtyatas');
		$satuanatas = $this->input->post('satuanatas');
		$pcode = strtoupper(addslashes(trim($this->input->post('pcode'))));
		$qty = trim($this->input->post('qty'));
		$satuan = $this->input->post('satuan');
		$nilsatuan = $this->input->post('nilsatuan');
		$pcodesave = $this->input->post('pcodesave');
		if($no=="")
		{
			$counter = "1";
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user);
		}
		else
		{
			$counter = $this->updateHeader($flag,$mylib->ubah_tanggal($tgl),$no,$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user);
		}
		$this->insertDetail($flag,$no,$counter,$pcode,$qty,$satuan,$user,$pcodesave,$nilsatuan,$pcodeatas,$status,$qtyatas,$satuanatas);
		echo $no;
	}

	function insertNewHeader($flag,$tgl,$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user)
	{
		$this->formulamodel->locktables('formulalist,formulaheader');
		$no   = "F".substr("00000".$this->formulamodel->getidcounter($pcodeatas)->Counter,-4);
		$data = array(
		    'kdformula' => $no,
			'tglformula'	=> $tgl,
			'namaformula' => $namaformula ,
			'status' => $status,
			'pcode' => $pcodeatas,
			'qty' => $qtyatas,
			'satuan' => $satuanatas,
			'adddate'    => $tgl,
			'adduser'    => $user
		);
		$this->db->insert('formulaheader', $data);
		$this->formulamodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$tgl,$no,$namaformula,$pcodeatas,$status,$qtyatas,$satuanatas,$user)
	{
		$this->formulamodel->locktables('formulaheader');
		$count = $this->formulamodel->getCounter($no,$pcodeatas);
		$counter = (int)$count->counter + 1;
		$data = array(
			'namaformula' => $namaformula ,
			'status' => $status,
			'qty' => $qtyatas,
			'satuan' => $satuanatas
		);
		if($flag=="edit")
		{
			$data['editdate'] = $tgl;
			$data['edituser'] = $user;
		}
		$this->db->update('formulaheader', $data, array('kdformula' => $no));
		$this->formulamodel->unlocktables();
		return $counter;
	}
    function insertDetail($flag,$no,$counter,$pcode,$qty,$satuan,$user,$pcodesave,$nilsatuan,$pcodeatas,$status,$qtyatas,$satuanatas)
	{
		$tgl = $this->session->userdata('Tanggal_Trans');
		$this->formulamodel->locktables('formuladetail');
		if($pcodesave==""){
			$data = array(
				'kdformula'	=> $no,
				'pcode' => $pcodeatas,
				'pcodedetail' => $pcode,
				'qty' => $qty,
				'satuan' => $satuan,
				'counter' => $counter,
			);
			$this->db->insert('formuladetail', $data);
		}
		else if($pcodesave!=$pcode)
		{
			$data = array(
				'pcodedetail' => $pcode,
				'qty' => $qty,
				'satuan' => $satuan
			);
			if($flag=="edit")
			{
				$data['editdate'] = $tgl;
				$data['edituser'] = $user;
			}
			$this->db->update('formuladetail', $data, array('kdformula' => $no,'pcode' => $pcodeatas,'pcodedetail'=>$pcodesave));
		}
		else if($pcodesave==$pcode)
		{
			$data = array(
				'qty' => $qty,
				'satuan' => $satuan
			);
			if($flag=="edit")
			{
				$data['editdate'] = $tgl;
				$data['edituser'] = $user;
			}
			$this->db->update('formuladetail', $data, array('kdformula' => $no,'pcode' => $pcodeatas,'pcodedetail'=>$pcodesave));
		}
		$this->formulamodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$pcodeatas = $this->input->post('pcodeatas');
		$pcode = $this->input->post('pcode');
		$this->formulamodel->locktables('formuladetail');
		$this->db->delete('formuladetail', array('kdformula' => $id,'pcode' => $pcodeatas,'pcodedetail'=>$pcode));
		$this->formulamodel->unlocktables();
	}

}
?>