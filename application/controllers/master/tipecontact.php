<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tipecontact extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/tipecontactmodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $sign = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/tipecontact/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/tipecontact/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/tipecontact/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']	= $this->tipecontactmodel->num_tipecontact_row($id,$with);
	        $data['tipecontactdata']	= $this->tipecontactmodel->gettipecontactList($config['per_page'],$page,$id,$with);
			$data['defcustomer']	= $this->tipecontactmodel->DefaultTipe();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/tipecontact/viewtipecontact', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  = "";
	     	$data['id']   = "";
	     	$data['nama'] = "";
	    	$this->load->view('master/tipecontact/addtipecontact',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function view_tipecontact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
     		$id 				= $this->uri->segment(4);
	    	$data['viewtipecontact'] = $this->tipecontactmodel->getDetail($id);
	    	$data['edit'] 		= false;
	    	$this->load->view('master/tipecontact/viewedittipecontact', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_tipe_contact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
			$defcustomer	= $this->tipecontactmodel->DefaultTipe();
			$id = $this->uri->segment(4);
			if($id!=$defcustomer->DefaultKodeTipeCustomer&&$id!=$defcustomer->DefaultKodeTipeSupplier){
				$data['viewtipecontact'] = $this->tipecontactmodel->getDetail($id);
				$data['cekAda'] = $this->tipecontactmodel->cekDelete($id);
				$this->load->view('master/tipecontact/deletetipecontact', $data);
			}
			else{
				$this->load->view('denied');
			}
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('tipe_contact', array('KdTipeContact' => $id));
		redirect('/master/tipecontact/');
	}
    
    function edit_tipecontact($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
			$defcustomer	= $this->tipecontactmodel->DefaultTipe();
			if($id!=$defcustomer->DefaultKodeTipeCustomer&&$id!=$defcustomer->DefaultKodeTipeSupplier){
				$data['viewtipecontact'] = $this->tipecontactmodel->getDetail($id);
				$data['edit'] 		= true;
				$this->load->view('master/tipecontact/viewedittipecontact', $data);
			}
			else{
				$this->load->view('denied');
			}
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_tipecontact(){
    	$id   = $this->input->post('kode');
    	$nama = strtoupper(trim(addslashes($this->input->post('nama'))));
    	$data = array(
    		  'NamaTipeContact'	=> $nama,
              'EditDate'	=> $this->session->userdata('Tanggal_Trans')
			);
		$this->db->update('tipe_contact', $data, array('KdTipeContact' => $id));
    	redirect('/master/tipecontact/');
    }
    function save_new_tipecontact(){
		$id   = strtoupper(trim(addslashes($this->input->post('kode'))));
    	$nama = strtoupper(trim(addslashes($this->input->post('nama'))));
    	$num  = $this->tipecontactmodel->get_id($id);
    	if($num!=0){
			$data['id']   = stripslashes($this->input->post('kode'));
			$data['nama'] = stripslashes($this->input->post('nama'));
			$data['msg']  = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/tipecontact/addtipecontact', $data);
		}
		else{
		 	$data = array(
               'KdTipeContact' => $id ,
               'NamaTipeContact' => $nama ,
               'AddDate'	=> $this->session->userdata('Tanggal_Trans')
            );
            $this->db->insert('tipe_contact', $data);
			redirect('/master/tipecontact/');
		}
	}
}
?>