<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class rekening_supplier extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/rekening_suppliermodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/rekening_supplier/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/rekening_supplier/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/rekening_supplier/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->rekening_suppliermodel->num_supplier_row($id,$with);
	        $data['supplierdata'] = $this->rekening_suppliermodel->getsupplierList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Supplier","Nama","No Rekening","Bank", "Nama Pemilik", "Status");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/rekening_supplier/list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  	 = "";
	     	$data['StatAktif']='Y';
			$data['label'] = 'Add Rekening Supplier';
			$data['track'] = $mylib->print_track();
			$data['supplier'] = $this->rekening_suppliermodel->getSupplier('');
	    	$this->load->view('master/rekening_supplier/add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['data'] = $this->rekening_suppliermodel->getDetail($id);
			$data['label'] = 'View Rekening Supplier';
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/rekening_supplier/viewedit', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function edit($kdsupplier, $norekening){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['data'] = $this->rekening_suppliermodel->getDetail($kdsupplier,$norekening);
	    	// print_r($data);
	    	$data['edit'] 		  = true;
			$data['track'] = $mylib->print_track();
			$data['label'] = 'Edit Rekening Supplier';
	    	$this->load->view('master/rekening_supplier/viewedit', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save(){
     	$mylib   = new globallib();
    	$kdsupplier	 = $this->input->post('KdSupplier');
    	$norekening	 = strtoupper(addslashes(trim($this->input->post('NoRekening'))));
		$namabank = strtoupper(addslashes(trim($this->input->post('NamaBank'))));
    	$namapemilik 	 = strtoupper(addslashes(trim($this->input->post('NamaPemilik'))));
		$stataktif  = $this->input->post('StatAktif');
		$date = date('Y-m-d');
		$sql = "Update rekening_supplier set NamaBank='$namabank', NamaPemilik='$namapemilik',StatAktif='$stataktif',EditDate='$date' 
				where KdSupplier='$kdsupplier' and NoRekening='$norekening' ";
		$this->db->query($sql);
    	redirect('/master/rekening_supplier/');
    }
    
    function save_new(){
     	$mylib = new globallib();
		$kdsupplier	 = $this->input->post('KdSupplier');
    	$norekening	 = strtoupper(addslashes(trim($this->input->post('NoRekening'))));
		$namabank = strtoupper(addslashes(trim($this->input->post('NamaBank'))));
    	$namapemilik 	 = strtoupper(addslashes(trim($this->input->post('NamaPemilik'))));
		$stataktif  = $this->input->post('StatAktif');
		$tgltrans = date('Y-m-d');
		$data = array(
			'KdSupplier'  => $kdsupplier,
			'NoRekening'  => $norekening,
			'NamaBank'	  => $namabank,
			'NamaPemilik' => $namapemilik,
			'StatAktif'	 => $stataktif,
			'AddDate'    => $tgltrans
		);
		$this->db->insert('rekening_supplier', $data);
		redirect('/master/rekening_supplier/');
	
	}
	
	function ajax_supplier(){
		$var = $this->input->post('id');
		$supplier = $this->rekening_suppliermodel->getSupplier($var);
		 
	 	echo "<option value=''> -- Pilih Supplier --</option>";
     	foreach ($supplier as $var) {
	 		echo "<option value=".$var['KdSupplier'].">".$var['Nama']."</option>";
	    }
    }

}
?>