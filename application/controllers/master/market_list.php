<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Market_list extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('master/market_listmodel','marketlist');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $user = $this->session->userdata('username');

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            $config['base_url'] = base_url() . 'index.php/master/market_list/index/';
            $config['uri_segment'] = 3;
            $page = $this->uri->segment(3);
           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->marketlist->num_tabel_row();
            $data['data'] = $this->marketlist->getTabelList($config['per_page'], $page);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('master/market_list/tabellist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            
			$data['userlevel'] = $this->marketlist->getUserLevel();

            $data['track'] = $mylib->print_track();
            $this->load->view('master/market_list/form_add', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function form_edit($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $userlevel = $this->session->userdata('userlevel');
            $data['header'] = $this->marketlist->getHeader($id);
            $data['detail'] = $this->marketlist->getDetail($id);
            $data['dPair'] = $this->marketlist->getPair($id);
            $data['userlevel'] = $this->marketlist->getUserLevel();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('master/market_list/form_edit', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        $v_market_list_name = $mylib->save_char($this->input->post('v_market_list_name'));
        $v_user_level = $this->input->post('v_user_level');
        
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');

        if ($flag == "add")
		{
			$v_no_dokumen = $mylib->get_counter_int($this->db->database,"market_list","market_list_id",100); 
			
            $this->insertNewHeader($v_no_dokumen, $v_market_list_name, $flag, $base_url, $user);
        } 
		else if ($flag == "edit") 
		{
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
            $this->updateHeader($v_no_dokumen, $v_market_list_name, $flag, $base_url, $user);

            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>Market List ' . $v_market_list_name . '</strong> berhasil', 'class' => 'success'));
        }
        
        $this->db->delete('market_list_pair', array('market_list_id' => $v_no_dokumen));
        
        for ($x = 0; $x < count($v_user_level); $x++) 
        {
        	$sid_pair = $mylib->get_counter_int($this->db->database,"market_list_pair","sid",100); 
        	
        	$this->insertPair($sid_pair,$v_no_dokumen,$v_user_level[$x]);	
        }

        for ($x = 0; $x < count($v_pcode1); $x++) 
        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);

            if ($pcode != "") 
            {
        		$detail = $this->marketlist->cekGetDetail($pcode,$v_no_dokumen);
        		
        		if($detail->pcode!=$pcode)
				{
					$this->insertDetail($v_no_dokumen,$pcode);
				}
            }
        }

        redirect('/master/market_list/form_edit/' . $v_no_dokumen . '');
    }

	function insertPair($sid,$no_dokumen,$v_level) 
	{
        $this->marketlist->locktables('market_list_pair');

        $data = array(
            'sid' => $sid,
            'market_list_id' => $no_dokumen,
            'UserLevelID' => $v_level
        );

        $this->db->insert('market_list_pair', $data);

        $this->marketlist->unlocktables();
	}
	
    function insertNewHeader($no_dokumen, $market_list_name, $flag, $base_url, $user) 
    {
        $this->marketlist->locktables('market_list');

        $data = array(
            'market_list_id' => $no_dokumen,
            'market_list_name' => $market_list_name
        );

        $this->db->insert('market_list', $data);

        $this->marketlist->unlocktables();
    }

    function updateHeader($no_dokumen, $market_list_name, $flag, $base_url, $user) 
    {
        $this->marketlist->locktables('market_list,market_list_detail');

        $data = array(
            'market_list_name' => $market_list_name
        );

        $this->db->update('market_list', $data, array('market_list_id' => $no_dokumen));
        
        $this->marketlist->unlocktables();
    }

    function insertDetail($no_dokumen, $pcode) 
    {
        $this->marketlist->locktables('market_list_detail');

        if ($pcode) {
            $data = array(
                'market_list_id' => $no_dokumen,
                'pcode' => $pcode
            );

            $this->db->insert('market_list_detail', $data);
        }

        $this->marketlist->unlocktables();
    }

    function delete_trans($id) 
    {
        $data["cekdata"] = $this->permintaan_barangmodel->cekNodok($id);

        if ($data["cekdata"]->NoDokumen != "") {
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus data gagal, karena sudah dibuat PR', 'class' => 'danger'));
        } else {
            $this->db->delete('permintaan_barang_header', array('NoDokumen' => $id));
            $this->db->delete('market_list_detail', array('NoDokumen' => $id));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        }

        redirect('/master/market_list/');
    }

    function delete_detail() 
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);

        $this->db->delete('market_list_detail', array('sid' => $sid, 'pcode' => $pcode, 'market_list_id' => $nodok));
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/master/market_list/form_edit/' . $nodok . '');
    }           

}

?>