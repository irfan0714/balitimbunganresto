<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class attribute extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/attributemodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
		 	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        
	     	$this->load->library('pagination');
	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/brand/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/brand/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/brand/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows'] = $this->attributemodel->num_attr_row($id,$with);
	        $data['attrdata']    = $this->attributemodel->getattrList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/attribute/viewattributelist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$data['msg'] = "";
	    	$this->load->view('master/attribute/addattribute',$data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function view_attr($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewattr']  = $this->attributemodel->getDetail($id);
	    	$data['edit'] 		= false;
	    	$this->load->view('master/attribute/editattribute', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    function edit_attr($id){
    /* 	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
	    	$data['viewattr']  = $this->attributemodel->getDetail($id);
	    	$data['edit']  		= true;
	    	$this->load->view('master/attribute/editattribute', $data);
	    }
		else{*/
			$this->load->view('denied');
	//	}
    }
    
    function save_attr(){
		$tgl = $this->session->userdata('Tanggal_Trans');
    	$id   = addslashes($this->input->post('kode'));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
		$tipe = $this->input->post('tipe');
		$panjang = trim($this->input->post('panjang'));
		$pk = $this->input->post('pk');
		$pilihan = $this->input->post('pilihan');
		$jenis = $this->input->post('jenis');
		if(empty($pk))
		{
			$pk = "T";
		}
		if($jenis=="I"){
			$data = array(
				'NamaAttribute'	=> $nama,
				'TipeAttr '	=> $tipe,
				'PanjangAttr '	=> $panjang,
				'Mandatory '	=> $pk,
				'EditDate'	=> $tgl
				);
		}
		else if($jenis=="P")
		{
			$part = explode("|",$pilihan);
			$data = array(
				'NamaAttribute'	=> $part[0],
				'TipeAttr '	=> "D",
				'PanjangAttr '	=> $part[1],
				'Mandatory '	=> $pk,
				'EditDate'	=> $tgl
				);
		}
		$this->db->update('attribute_barang', $data, array('KdAttribute' => $id));
    	redirect('/master/attribute/');
    }
    function save_new_attr(){
		$id   = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
		$tipe = $this->input->post('tipe');
		$panjang = trim($this->input->post('panjang'));
		$pk = $this->input->post('pk');
		$tgl = $this->session->userdata('Tanggal_Trans');
		$pilihan = $this->input->post('pilihan');
		$jenis = $this->input->post('jenis');
		$num  	 = $this->attributemodel->get_id($id);
		if($num==0){
			if(empty($pk))
			{
				$pk = "T";
			}
			if($jenis=="I"){
				$data = array(
					'KdAttribute' 	=> $id ,
					'NamaAttribute' 	=> $nama ,
					'TipeAttr '	=> $tipe,
					'PanjangAttr '	=> $panjang,
					'Mandatory '	=> $pk,
					'AddDate'	=> $tgl
				);
			}
			else if($jenis=="P")
			{
				$part = explode("|",$pilihan);
				$data = array(
					'KdAttribute' 	=> $id ,
					'NamaAttribute'	=> $part[0],
					'TipeAttr '	=> "D",
					'PanjangAttr '	=> $part[1],
					'Mandatory '	=> $pk,
					'AddDate'	=> $tgl
					);
			}
			$this->db->insert('attribute_barang', $data);
			redirect('/master/attribute/');
		}
		else
		{
			$data['msg']  	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/attribute/addattribute', $data);
		}
	}
}
?>