<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Group_harga extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/group_harga_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "group_harga", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            
	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/master/group_harga/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/master/group_harga/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            //$config['total_rows'] = $this->group_harga_model->num_group_harga_row($arr_search["search"]);
			$data['data'] = $this->group_harga_model->getGroupHargaList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('master/group_harga/group_harga_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'group_harga', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "group_harga",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/master/group_harga/index/' . $query_id . '');
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			//pertama hapus dahulu yang di temporary berdasarkan username tersebut
			$user = $this->session->userdata('username');
			//delete di temp
			$this->db->delete('groupharga_temp',array('adduser'=>$user));
			
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/group_harga/add_group_harga',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_data($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$data['id'] = $id;
			$data['header'] = $this->group_harga_model->getListHeader($id);
			$data['detail'] = $this->group_harga_model->getListDetail($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/group_harga/edit_group_harga',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function view_data($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
			$data['id'] = $id;
			$data['header'] = $this->group_harga_model->getListHeader($id);
			$data['detail'] = $this->group_harga_model->getListDetail($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/group_harga/view_group_harga',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_detail($id,$pcode){
		$this->db->delete('groupharga_detail',array('GroupHargaID'=>$id,'PCode'=>$pcode));
		echo json_encode(array("status" => TRUE));	
	}
    
    function getList()
    {
    		$user = $this->session->userdata('username');
            $data['data'] = $this->group_harga_model->getListTemp($user);
		
			$this->load->view('master/group_harga/content_group_harga', $data);
	}
	
	function save_data()
    {
    		//echo "<pre>";print_r($_POST);echo "</pre>";die;
    		$user = $this->session->userdata('username');
    		$namagroup = $this->input->post('v_namagroup');
    		$flag = $this->input->post('flag');
    		$excel = $this->input->post('excel');
    		//detail
			$pcode1 = $this->input->post('v_pcode');
			$harga1 = $this->input->post('v_harga');
			
			if($flag=="add"){
				
				//delete di temp
				$this->db->delete('groupharga_temp',array('adduser'=>$user));
				
				//header
				$data_header = array('GroupHargaName'=>$namagroup);
				$this->db->insert('groupharga_header',$data_header);
				
				//ambil group harga ID
				$id = $this->group_harga_model->getID($namagroup);
				$GroupHargaID = $id->GroupHargaID;
				
				//detail
				for ($x = 0; $x < count($pcode1); $x++)
				{
		            $pcode = $pcode1[$x];
		            $harga = $harga1[$x];
		            
		            	   $data_detail=array(
										  'GroupHargaID' => $GroupHargaID,
										  'PCode' => $pcode,
										  'Harga'=>$harga)						  
							            ;				
						
							$this->db->insert('groupharga_detail', $data_detail);
				}
				
				redirect('/master/group_harga/edit_data/'.$GroupHargaID);
				
			}else if($flag=="edit" AND $excel==""){
				$idGroupHarga = $this->input->post('v_id');
				
				//delete di temp
				$this->db->delete('groupharga_temp',array('adduser'=>$user));
				
				//header
				$data_header = array('GroupHargaName'=>$namagroup);
				$this->db->update('groupharga_header',$data_header,array('GroupHargaID'=>$idGroupHarga));
				
				//detail
				for ($x = 0; $x < count($pcode1); $x++)
				{
		            $pcode = $pcode1[$x];
		            $harga = $harga1[$x];
		            
		            	    $data_detail=array('GroupHargaID'=>$idGroupHarga,'PCode' => $pcode,'Harga'=>$harga);						  
											
						    //cek PCode Sudah ada atau belum
						    $cek = $this->group_harga_model->getCekPCode($idGroupHarga,$pcode);
						    if(!empty($cek)){
								$this->db->update('groupharga_detail', $data_detail,array('GroupHargaID'=>$idGroupHarga,'PCode' => $pcode));	
							}else{
								$this->db->insert('groupharga_detail', $data_detail);
							}
							
				}
				
				redirect('/master/group_harga/edit_data/'.$idGroupHarga);
			}
			
			if($excel=="")
			{
				$data['track'] = $mylib->print_track();
				redirect('/master/group_harga/edit_data/'.$idGroupHarga);
			}
			else
			{
				$mylib = new globallib();
				$id = $idGroupHarga = $this->input->post('v_id');
				$data['id'] = $id;
				$data['header'] = $this->group_harga_model->getListHeader($id);
				$data['detail'] = $this->group_harga_model->getListDetail($id);
				$data['track'] = $mylib->print_track();
		    	$this->load->view('master/group_harga/export_to_excel',$data);
			}
			
	}
	
	function aktif($id){
		$this->db->update('groupharga_header',array('Status'=>'A'),array('GroupHargaID'=>$id));
		redirect('/master/group_harga/index/');
	}
	
	function nonaktif($id){
		$this->db->update('groupharga_header',array('Status'=>'T'),array('GroupHargaID'=>$id));
		redirect('/master/group_harga/index/');
	}

}
?>