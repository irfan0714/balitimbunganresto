<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class kendaraan extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/kendaraanmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr  		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with		  = $this->input->post('searchby');
	     	$this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/kendaraan/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/kendaraan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/kendaraan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows'] = $this->kendaraanmodel->num_kendaraan_row($id,$with);
	        $data['kendaraan_data']   = $this->kendaraanmodel->get_kendaraan_List($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Kendaraan","Nama","No Polisi","Driver");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/kendaraan/kendaraan_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	= "";
	     	$data['id']		= "";
	     	$data['nama']	= "";
			$data['nopol']	= "";
	     	$data['mdriver']= $this->kendaraanmodel->getMaster();
	     	$data['driver']	= "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/kendaraan/add_kendaraan',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_kendaraan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['data_kendaraan'] = $this->kendaraanmodel->getDetail($id);
			$data['mdriver']  	 	= $this->kendaraanmodel->getMaster();
	    	$data['edit'] 		= false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/kendaraan/view_edit_kendaraan', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_kendaraan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['data_kendaraan'] = $this->kendaraanmodel->getDetail($id);
			$data['cekAda'] = $this->kendaraanmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/kendaraan/delete_kendaraan', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('kendaraan', array('KdKendaraan' => $id));
		redirect('/master/kendaraan/');
	}

    function edit_kendaraan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
	    	$data['data_kendaraan'] = $this->kendaraanmodel->getDetail($id);
	    	$data['mdriver']  	 	= $this->kendaraanmodel->getMaster();
	    	$data['edit'] 		= true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/kendaraan/view_edit_kendaraan', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_kendaraan(){
		$mylib = new globallib();
		$tgl = $this->session->userdata('Tanggal_Trans');
    	$id 	 	= strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 	= strtoupper(addslashes(trim($this->input->post('nama'))));
		$nopol 	 	= strtoupper(addslashes(trim($this->input->post('nopol'))));
    	$driver	 	= $this->input->post('driver');
		$data = array(
			'NamaKendaraan'	=> $nama ,
			'NoPolisi' 		=> $nopol ,
			'KdPersonal'	=> $driver,
			'AddDate'		=> $tgl
		);
		$this->db->update('kendaraan', $data, array('KdKendaraan' => $id));
    	redirect('/master/kendaraan/');
    }
    function save_new_kendaraan(){
		$tgl = $this->session->userdata('Tanggal_Trans');
		$id 	 	= strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 	= strtoupper(addslashes(trim($this->input->post('nama'))));
		$nopol 	 	= strtoupper(addslashes(trim($this->input->post('nopol'))));
    	$driver	 	= $this->input->post('driver');
    	$num 	    = $this->kendaraanmodel->get_id($id);
    	if($num == 0)
		{
		 	$data = array(
				'KdKendaraan'	=> $id ,
				'NamaKendaraan'	=> $nama ,
				'NoPolisi' 		=> $nopol ,
				'KdPersonal'	=> $driver,
				'AddDate'		=> $tgl
            );
            $this->db->insert('kendaraan', $data);
			redirect('master/kendaraan');
		}
		else
		{
			$data['id'] 	= $this->input->post('kode');
			$data['nama'] 	= $this->input->post('nama');
			$data['mdriver']= $this->kendaraanmodel->getMaster();
			$data['driver']	= $driver;
	     	$data['nopol'] = $this->input->post('nopol');
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/kendaraan/add_kendaraan', $data);
		}
	}
}
?>