<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class supplier extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/suppliermodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/supplier/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/supplier/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/supplier/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->suppliermodel->num_supplier_row($id,$with);
	        $data['supplierdata'] = $this->suppliermodel->getsupplierList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Supplier","Nama","Alamat","Kota","Telepon","Payment","TOP","PPn","Status");
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/supplier/viewsupplierlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  	 = "";
	     	$data['id']   	 = "";
	     	$data['nama']	 = "";
			$data['contact'] = "";
	     	$data['alm']	 = "";
	     	$data['kota']	 = "";
	     	$data['telp']	 = "";
			$data['npwp']	 = "";
			$data['Email']	 = "";
			$data['Notes']	 = "";
			$data['namapajak']	 = "";
	     	$data['almpajak']	 = "";
	     	$data['kotapajak']	 = "";
			$data['stataktif']	= "Y";
			$data['top'] = "0";
			$data['ppn'] = "0";
			$data['label'] = 'Add Supplier';
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supplier/addsupplier',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_supplier($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewsupplier'] = $this->suppliermodel->getDetail($id);
			$data['tipesupplier']	= $this->suppliermodel->getTipeSupplier();
			$data['groupsupplier']	= $this->suppliermodel->getGroupSupplier();
			$data['area']	= $this->suppliermodel->getArea();
			$data['label'] = 'View Supplier';
	    	$data['edit'] 		  = false;
			if($data['viewsupplier']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supplier/vieweditsupplier', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_supplier($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewsupplier'] = $this->suppliermodel->getDetail($id);
			$data['cekAda'] = $this->suppliermodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/supplier/deletesupplier', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('supplier', array('Kdsupplier' => $id));
		redirect('/master/supplier/');
	}

    function edit_supplier($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['viewsupplier'] = $this->suppliermodel->getDetail($id);
	    	$data['edit'] 		  = true;
			if($data['viewsupplier']->Payment=="C")
			{
			$data['checkedc']     = "checked='checked'";
			$data['checkedk']     = "";
			$data['topdisabled']  = "disabled";
			}
			else
			{
			$data['checkedk']     = "checked='checked'";
			$data['checkedc']     = "";
			$data['topdisabled']  = "";
			}
			$data['track'] = $mylib->print_track();
			$data['label'] = 'Edit Supplier';
	    	$this->load->view('master/supplier/vieweditsupplier', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_supplier(){
     	$mylib   = new globallib();
    	$id		 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$contact = strtoupper(addslashes(trim($this->input->post('contact'))));
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
    	$npwp 	 = $this->input->post('npwp');
    	$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('alamatpajak'))));
    	$kotapajak 	 = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	$notes 	 = addslashes(trim($this->input->post('notes')));
    	$email 	 = addslashes(trim($this->input->post('email')));
    	
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		$top     = $this->input->post('top');
		$stataktif  = $this->input->post('stataktif');
		$ppn     = $this->input->post('ppn');
    	$data = array(
			'Nama' 		 => $nama,
			'Contact'	 => $contact,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'NamaPajak'  => $namapajak,
			'AlamatPajak' => $almpajak,
			'KotaPajak'	 => $kotapajak,
			'NPWP'		 => $npwp,
			'Top'        => $top,
			'PPn'        => $ppn,
			'Notes'     => $notes,
			'Email' 	=> $email,
			'StatAktif' => $stataktif,
            'EditDate'	 => $tgltrans
			);
		$this->db->update('supplier', $data, array('KdSupplier' => $id));
    	redirect('/master/supplier/');
    }
    
    function save_new_supplier(){
     	$mylib = new globallib();
		$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
		$contact = strtoupper(addslashes(trim($this->input->post('contact'))));
		$idnama  = trim(substr($nama,0,2));
		$idnama = str_replace(' ', '', $idnama);
		$id      = $idnama.substr("00000".$this->suppliermodel->getidcounter($idnama)->Counter,-4);
    	$alm 	 = strtoupper(addslashes(trim($this->input->post('alm'))));
    	$telp 	 = strtoupper(trim($this->input->post('telp')));
    	$kota 	 = strtoupper(addslashes(trim($this->input->post('kota'))));
    	$notes 	 = addslashes(trim($this->input->post('notes')));
    	$email 	 = addslashes(trim($this->input->post('email')));
    	$npwp 	 = $this->input->post('npwp');
		$top     = $this->input->post('top');
		$ppn     = $this->input->post('ppn');
		$stataktif  = $this->input->post('stataktif');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		
		$namapajak 	 = strtoupper(addslashes(trim($this->input->post('namapajak'))));
    	$almpajak 	 = strtoupper(addslashes(trim($this->input->post('alamatpajak'))));
    	$kotapajak 	 = strtoupper(addslashes(trim($this->input->post('kotapajak'))));
    	
		$data = array(
			'KdSupplier' => $id,
			'Nama' 		 => $nama,
			'Contact'	 => $contact,
			'Alamat'	 => $alm,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'NamaPajak'  => $namapajak,
			'AlamatPajak' => $almpajak,
			'KotaPajak'	 => $kotapajak,
			'NPWP'		 => $npwp,
			'Top'        => $top,
			'PPn'        => $ppn,
			'StatAktif' => $stataktif,
			'Notes'     => $notes,
			'Email' => $email,
			'AddDate'    => $tgltrans
		);
		$this->db->insert('supplier', $data);
		redirect('/master/supplier/');
	
	}
}
?>