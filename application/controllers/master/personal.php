<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class personal extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/personalmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/personal/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/personal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/personal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->personalmodel->num_personal_row($id,$with);
	        $data['personal_data']  = $this->personalmodel->get_personal_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/personal/personal_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	$data['tipe']  = $this->personalmodel->getTipe();
	     	$data['niltipe'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/personal/add_personal',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_personal'] = $this->personalmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->personalmodel->getTipe();
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/personal/view_edit_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_personal'] = $this->personalmodel->getDetail($id);
			$data['cekAda'] = $this->personalmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/personal/delete_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
     	$this->db->delete('personal', array('KdPersonal' => $id));
		redirect('/master/personal/');
	}

    function edit_personal($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_personal'] = $this->personalmodel->getDetail($id);
	    	$data['tipe'] 	  = $this->personalmodel->getTipe();
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/personal/view_edit_personal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_personal(){
    	$id 	 = $this->input->post('kode');
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
    		  'NamaPersonal'	=> $nama,
    		  'KdTipePersonal' => $tipe ,
              'EditDate'		=> $tgltrans
			);
		$this->db->update('personal', $data, array('KdPersonal' => $id));
    	redirect('/master/personal/');
    }
    function save_new_personal(){
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
    	$num 	 = $this->personalmodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdPersonal'   => $id ,
               'NamaPersonal' => $nama ,
               'KdTipePersonal' => $tipe ,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('personal', $data);
			redirect('master/personal');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->personalmodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/personal/add_personal', $data);
		}
	}
}
?>