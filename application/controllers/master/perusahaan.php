<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class perusahaan extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/perusahaanmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/perusahaan/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/perusahaan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/perusahaan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}

	        $config['total_rows'] = $this->perusahaanmodel->num_perusahaan_row($id,$with);
	        $data['perusahaandata'] = $this->perusahaanmodel->getperusahaanList($config['per_page'],$page,$id,$with);
	        $data['header']		  = array("Kode Perusahaan","Nama");
			$data['default'] = $this->perusahaanmodel->DefaultKode();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/perusahaan/viewperusahaanlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  	 = "";
	     	$data['id']   	 = "";
	     	$data['nama']	 = "";
	     	$data['alm1']	 = "";
			$data['alm2']	 = "";
			$data['npwp']	 = "";
	     	$data['kota']	 = "";
	     	$data['telp']	 = "";
	    	$this->load->view('master/perusahaan/addperusahaan',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_perusahaan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['viewperusahaan'] = $this->perusahaanmodel->getDetail($id);
			$data['edit'] 		  = false;
	    	$this->load->view('master/perusahaan/vieweditperusahaan', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_perusahaan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
			$default = $this->perusahaanmodel->DefaultKode();
			if($default->DefaultKodePerusahaan!=$id){
				$data['viewperusahaan'] = $this->perusahaanmodel->getDetail($id);
				$data['cekAda'] = $this->perusahaanmodel->cekDelete($id);
				$this->load->view('master/perusahaan/deleteperusahaan', $data);
			}
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('perusahaan', array('Kdperusahaan' => $id));
		redirect('/master/perusahaan/');
	}

    function edit_perusahaan($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
			$default = $this->perusahaanmodel->DefaultKode();
			if($default->DefaultKodePerusahaan!=$id){
				$data['viewperusahaan'] = $this->perusahaanmodel->getDetail($id);
				$data['edit'] 		  = true;
				$this->load->view('master/perusahaan/vieweditperusahaan', $data);
			}
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_perusahaan(){
     	$mylib   = new globallib();
    	$id		 = $this->input->post('kode');
    	$nama 	 = addslashes(trim($this->input->post('nama')));
    	$alm1 	 = addslashes(trim($this->input->post('alm1')));
    	$alm2 	 = addslashes(trim($this->input->post('alm2')));
    	$telp 	 = trim($this->input->post('telp'));
    	$kota 	 = addslashes(trim($this->input->post('kota')));
    	$npwp 	 = addslashes(trim($this->input->post('npwp')));
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$data = array(
			'Nama' 		 => $nama,
			'Alamat1'	 => $alm1,
			'Alamat2'	 => $alm2,
			'Kota'		 => $kota,
			'Telepon'	 => $telp,
			'NPWP'	 => $npwp,
            'EditDate'	 => $tgltrans
			);
		$this->db->update('perusahaan', $data, array('KdPerusahaan' => $id));
    	redirect('/master/perusahaan/');
    }
    function save_new_perusahaan(){
     	$mylib = new globallib();
		$id		 = $this->input->post('kode');
    	$nama 	 = addslashes(trim($this->input->post('nama')));
    	$alm1 	 = addslashes(trim($this->input->post('alm1')));
    	$alm2 	 = addslashes(trim($this->input->post('alm2')));
    	$telp 	 = trim($this->input->post('telp'));
    	$kota 	 = addslashes(trim($this->input->post('kota')));
		$npwp 	 = addslashes(trim($this->input->post('npwp')));
    	$num  	 = $this->perusahaanmodel->get_id($id);
    	if($num!=0){
			$data['id']   	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['msg']  	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$data['alm1']	 = $this->input->post('alm1');
			$data['alm2']	 = $this->input->post('alm2');
	     	$data['kota']	 = $this->input->post('kota');
	     	$data['telp']	 = $this->input->post('telp');
	     	$data['npwp']	 = $this->input->post('npwp');
			$this->load->view('master/perusahaan/addperusahaan', $data);
		}
		else{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
				'Kdperusahaan' => $id,
				'Nama' 		 => $nama,
				'Alamat1'	 => $alm1,
				'Alamat2'	 => $alm2,
				'Kota'		 => $kota,
				'Telepon'	 => $telp,
				'NPWP'	 	=> $npwp,
   				'AddDate'    => $tgltrans
            );
            $this->db->insert('perusahaan', $data);
			redirect('/master/perusahaan/');
		}
	}
}
?>