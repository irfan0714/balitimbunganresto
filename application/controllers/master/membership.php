<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Membership extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/membership_model');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);	     	
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/membership/index/';
			$page					  = $this->uri->segment(4);		
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/membership/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/membership/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);

            $data['header'] = array('Kode','Nama','Contact','Phone','Fax','Alamat','NamaTypeMember','Email','Kota','Telepon','Status','AddDate','EditDate');
	        $config['total_rows']     = $this->membership_model->num_typetour_row($id,$with);
	        $data['detail']       = $this->membership_model->gettypetourList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/membership/viewlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){

     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
            $data=  array(
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'label'  => "Add Member",
	     	    'msg'    => "",
	     	    'kode'   => "",
	     	    'Kode'   => "",
                'Nama'   => "",
                'Contact'=> "",
                'Alamat' => "",
                'Kota'   => "",
                'Phone'  => "",
                'Fax'       => "",
                'Email'     => "",
                'AddDate'   => "",
                'EditDate'  => "",
                'Telepon'   => "",
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                'KdTipeMember'  => $this->membership_model->getType(),
                'track'     => $mylib->print_track()
            );
	    	$this->load->view('master/membership/add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete(){
     	$id = $this->input->post('kode');
		$this->db->delete('member', array('KdMember' => "$id"));
        echo "sukses";
//		redirect('/master/membership/');
	}
    
    function edit($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){

			$id 		    = $this->uri->segment(4);
	    	$detail         = $this->membership_model->getDetail($id);//print_r($detail);
	    	$data['detail'] = $this->membership_model->getDetail($id);
            $data =  array(
                'label'  => "Edit Member",
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'tr3'    => "save_edit",
                'detail' => $detail,
                'msg'    => "",
                'Kode'   => $detail->KdMember,
                'NamaMember'   => $detail->NamaMember,
                'JenisIdentitas'   => $detail->IDCard,
                'NoIdentitas'   => $detail->NoCard,
                'Alamat1' => $detail->Alamat1,
                'Alamat2' => $detail->Alamat2,
                'Telp1' => $detail->Telp1,
                'Telp2' => $detail->Telp2,
                'NoHP1' => $detail->NoHP1,
                'NoHP2' => $detail->NoHP2,
                'RecidenceMail'=> $detail->RecidenceMail,
                'OfficeMail'=> $detail->OfficeMail,
                'PhonePurposed1' => $detail->PhonePurposed1,
                'PhonePurposed2' => $detail->PhonePurposed2,
                'NamePurposed1' => $detail->NamePurposed1,
                'NamePurposed2' => $detail->NamePurposed2,
                'Kota'   => $detail->Kota,
                'hbd'   => $mylib->ubah_tanggal($detail->TglLahir),
                'Husband_Father'   => $detail->Husband_Father,
                'Nationality'   => $detail->Nationality,
                'Fax'       => '',
                'Email'     => $detail->Email,
                'Telepon'   => '',
                'NoRekening'   => '',
                'Bank'   => '',
                'Penerima'   =>'',
                'typenya'   => $detail->KdTipeMember,
                'statusnya'    => $detail->Status,
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                
            );
	    	$data['edit'] 	= true;
			$data['track']  = $mylib->print_track();
	    	$this->load->view('master/membership/edit', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_edit(){
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
    	$mylib = new globallib();
    	$kode = strtoupper(addslashes(trim($this->input->post('Kode'))));
    	$type_member = strtoupper(addslashes(trim($this->input->post('type_member'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('Nama'))));
    	$v_JenisIdentitas = strtoupper(addslashes(trim($this->input->post('Phone'))));
    	$v_NoIdentitas = strtoupper(addslashes(trim($this->input->post('v_NoIdentitas'))));
    	$hbd = $mylib->ubah_tanggal($this->input->post('hbd'));
    	$Nama_otherhbd = $this->input->post('Nama_other');
    	$nationality = $this->input->post('nationality');
    	$AlamatRumah = $this->input->post('AlamatRumah');
    	$TlpRumah = $this->input->post('TlpRumah');
    	$AlamatKantor = $this->input->post('AlamatKantor');
    	$TlpKantor = $this->input->post('TlpKantor');
    	$MobilePhone1 = $this->input->post('MobilePhone1');
    	$MobilePhone2 = $this->input->post('MobilePhone2');
        $Fax = $this->input->post('Fax');
        $Email = strtoupper(addslashes(trim($this->input->post('Email'))));
        $RecidenceMail = $this->input->post('RecidenceMail');
        $OfficeMail = $this->input->post('OfficeMail');
        
        $NamePurposed1 = $this->input->post('NamePurposed1');
        $PhonePurposed1 = $this->input->post('PhonePurposed1');
        
        $NamePurposed2 = $this->input->post('NamePurposed2');
        $PhonePurposed2 = $this->input->post('PhonePurposed2');
        $status = strtoupper(addslashes(trim($this->input->post('status'))));
       
        if(!empty($RecidenceMail)){
			$RecidenceMail = 1;
		}else{
			$RecidenceMail = 0;
		}
		
		if(!empty($OfficeMail)){
			$OfficeMail = 1;
		}else{
			$OfficeMail = 0;
		}
    	
    	  $data = array('NamaMember'	=> $nama,
				        'NickName' 		=> $nama,
				        'Nationality'	=> $nationality,
				        'Husband_Father'=> $Nama_otherhbd,
				        'IDCard'		=> $v_JenisIdentitas,
				        'NoCard'		=> $v_NoIdentitas,
				        'Alamat1'		=> $AlamatRumah,
				        'Alamat2'		=> $AlamatKantor,
				        'Kota'			=> '',
				        'KotaLahir'		=> '',
				        'TglLahir'		=> $hbd,
				        'Telp1'			=> $TlpRumah,
				        'Telp2'			=> $TlpKantor,
				        'Email'			=> $Email,
				        'RecidenceMail'	=> $RecidenceMail,
				        'OfficeMail'	=> $OfficeMail,
				        'Twitter'		=> '',
				        'KdTipeMember'  => $type_member,
				        'KdMember0'		=> '',
				        'NoHP1'			=> $MobilePhone1,
				        'NoHP2'			=> $MobilePhone2,
				        'JumlahPoint'	=> 0,
				        'NamePurposed1'	=> $NamePurposed1,
				        'PhonePurposed1'=>	$PhonePurposed1,
				        'NamePurposed2'	=> $NamePurposed2,
				        'PhonePurposed2'=>	$PhonePurposed2,
				        'Status'		=> $status,
				        'EditDate'		=> date('Y-m-d'));
        
            $this->db->update('member', $data,array('KdMember'=>$kode));
		redirect('/master/membership/');
    }
    function save(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die();
    	$mylib = new globallib();
    	$type_member = strtoupper(addslashes(trim($this->input->post('type_member'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('Nama'))));
    	$v_JenisIdentitas = strtoupper(addslashes(trim($this->input->post('Phone'))));
    	$v_NoIdentitas = strtoupper(addslashes(trim($this->input->post('v_NoIdentitas'))));
    	$hbd = $mylib->ubah_tanggal($this->input->post('hbd'));
    	$Nama_otherhbd = $this->input->post('Nama_other');
    	$nationality = $this->input->post('nationality');
    	$AlamatRumah = $this->input->post('AlamatRumah');
    	$TlpRumah = $this->input->post('TlpRumah');
    	$AlamatKantor = $this->input->post('AlamatKantor');
    	$TlpKantor = $this->input->post('TlpKantor');
    	$MobilePhone1 = $this->input->post('MobilePhone1');
    	$MobilePhone2 = $this->input->post('MobilePhone2');
        $Fax = $this->input->post('Fax');
        $Email = strtoupper(addslashes(trim($this->input->post('Email'))));
        $RecidenceMail = $this->input->post('RecidenceMail');
        $OfficeMail = $this->input->post('OfficeMail');
        
        $NamePurposed1 = $this->input->post('NamePurposed1');
        $PhonePurposed1 = $this->input->post('PhonePurposed1');
        
        $NamePurposed2 = $this->input->post('NamePurposed2');
        $PhonePurposed2 = $this->input->post('PhonePurposed2');
        $status = strtoupper(addslashes(trim($this->input->post('status'))));
        $password = md5("ilovesgv");
        $id = $this->get_code_counter();
    	
    	if(!empty($RecidenceMail)){
			$RecidenceMail = 1;
		}else{
			$RecidenceMail = 0;
		}
		
		if(!empty($OfficeMail)){
			$OfficeMail = 1;
		}else{
			$OfficeMail = 0;
		}
		
    	  $data = array('KdMember'		=> $id,
				        'NamaMember'	=> $nama,
				        'NickName' 		=> $nama,
				        'Nationality'	=> $nationality,
				        'Husband_Father'=> $Nama_otherhbd,
				        'IDCard'		=> $v_JenisIdentitas,
				        'NoCard'		=> $v_NoIdentitas,
				        'Alamat1'		=> $AlamatRumah,
				        'Alamat2'		=> $AlamatKantor,
				        'Kota'			=> '',
				        'KotaLahir'		=> '',
				        'TglLahir'		=> $hbd,
				        'Telp1'			=> $TlpRumah,
				        'Telp2'			=> $TlpKantor,
				        'Email'			=> $Email,
				        'RecidenceMail'	=> $RecidenceMail,
				        'OfficeMail'	=> $OfficeMail,
				        'Twitter'		=> '',
				        'KdTipeMember'  => $type_member,
				        'KdMember0'		=> '',
				        'NoHP1'			=> $MobilePhone1,
				        'NoHP2'			=> $MobilePhone2,
				        'Password'			=> $password,
				        'JumlahPoint'	=> 0,
				        'NamePurposed1'	=> $NamePurposed1,
				        'PhonePurposed1'=>	$PhonePurposed1,
				        'NamePurposed2'	=> $NamePurposed2,
				        'PhonePurposed2'=>	$PhonePurposed2,
				        'Status'		=> $status,
				        'AddDate'		=> date('Y-m-d'));
        
            $this->db->insert('member', $data);
		redirect('/master/membership/');

	}
	
	function get_code_counter()
	{
	    global $db;
	    
	    $q = "
	            SELECT
	                SUBSTRING(KdMember, 1, 7) AS last_counter
	            FROM
	                member
	            WHERE 1
	            ORDER BY
	                sid DESC
	            LIMIT
	                0,1
	    ";
	    
	    $qry = mysql_query($q);
	    $row = mysql_fetch_array($qry);
	    list($last_counter) = $row;
	    
	    $last_counter = ($last_counter*1)+1;
	    
	    $satu 	= (substr($last_counter,0,1)*1)*2;
	    $dua 	= (substr($last_counter,1,1)*1)*2;
	    $tiga 	= (substr($last_counter,2,1)*1)*2;
	    $empat 	= (substr($last_counter,3,1)*1)*2;
	    $lima 	= (substr($last_counter,4,1)*1)*2;
	    $enam 	= (substr($last_counter,5,1)*1)*2;
	    $tujuh 	= (substr($last_counter,6,1)*1)*2;
	    
	     $jml= $satu+$dua+$tiga+$empat+$lima+$enam+$tujuh;
	   
	    $hitung = $this->acak($jml);
	    
	    $counter = sprintf("%07s", $last_counter);
	    $id_member = $counter.$hitung;	    
	    return $id_member;
	}
	
	function acak($hitung){
		
		if($hitung<=10){
		   $hitung=10-$hitung;
		}else if($hitung<=20){
		   $hitung=20-$hitung;
		}else if($hitung<=30){
		   $hitung=30-$hitung;
		}else if($hitung<=40){
		   $hitung=40-$hitung;
		}else if($hitung<=50){
		   $hitung=50-$hitung;
		}else if($hitung<=60){
		   $hitung=60-$hitung;
		}else if($hitung<=70){
		   $hitung=70-$hitung;
		}else if($hitung<=80){
		   $hitung=80-$hitung;
		}else if($hitung<=90){
		   $hitung=90-$hitung;
		}else if($hitung<=100){
		   $hitung=100-$hitung;
		}else if($hitung<=110){
		   $hitung=110-$hitung;
		}else if($hitung<=120){
		   $hitung=120-$hitung;
		}
		
		return $hitung;
	}
}
?>