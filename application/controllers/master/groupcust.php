<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class groupcust extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/groupcustmodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);	     	
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/groupcust/index/';
			$page					  = $this->uri->segment(4);		
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/groupcust/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/groupcust/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			
	        $config['total_rows']     = $this->groupcustmodel->num_groupcust_row($id,$with);
	        $data['groupcustdata']       = $this->groupcustmodel->getGroupcustList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/groupcust/viewgroupcustlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
	     	$data['msg']  = "";
	     	$data['id']   = "";
	     	$data['nama'] = "";
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupcust/addgroupcust',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function view_groupcust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewgroupcust'] = $this->groupcustmodel->getDetail($id);
	    	$data['edit'] 		= false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupcust/vieweditgroupcust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_groupcust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				= $this->uri->segment(4);
	    	$data['viewgroupcust'] = $this->groupcustmodel->getDetail($id);
			$data['cekAda'] = $this->groupcustmodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupcust/deletegroupcust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete_This(){
     	$id = $this->input->post('kode');
		$this->db->delete('groupcust', array('KdGroupcust' => $id));
		redirect('/master/groupcust/');
	}
    
    function edit_groupcust($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				= $this->uri->segment(4);
	    	$data['viewgroupcust'] = $this->groupcustmodel->getDetail($id);
	    	$data['edit'] 		= true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/groupcust/vieweditgroupcust', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_groupcust(){
		$tgltrans = $this->session->userdata('Tanggal_Trans');
    	$id   = addslashes($this->input->post('kode'));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$data = array(
    		  'NamaGroupcust'	=> $nama,
              'EditDate'	=> $tgltrans
			);
		$this->db->update('groupcust', $data, array('KdGroupcust' => $id));
    	redirect('/master/groupcust/');
    }
    function save_new_groupcust(){
		$id   = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$num  = $this->groupcustmodel->get_id($id);
    	if($num!=0){
			$data['id']   = $this->input->post('kode');
			$data['nama'] = $this->input->post('nama');
			$data['msg']  = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/groupcust/addgroupcust', $data);
		}
		else{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'KdGroupcust'   => $id ,
               'NamaGroupcust' => $nama ,
               'AddDate'    => $tgltrans
            );
            $this->db->insert('groupcust', $data);
			redirect('/master/groupcust/');
		}
	}
}
?>