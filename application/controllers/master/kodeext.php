<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class kodeext extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/kodeextmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/master/kodeext/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/kodeext/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/master/kodeext/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("Kode External","Nama External","Tanggal","Status");
	        $config['total_rows']		= $this->kodeextmodel->num_kodeext_row($id,$with);
	        $data['data']	= $this->kodeextmodel->getkodeextList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/kodeext/kodeextlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->kodeextmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['tanggal'] = $aplikasi->TglTrans;
			$data['satuanatas'] = $this->kodeextmodel->getSatuanAtas();
			$data['track'] = $mylib->print_track();
	    	$this->load->view('master/kodeext/add_kodeext',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "kodeext.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_master/cetak_transaksi_printer', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->kodeextmodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->kodeextmodel->getDetail($id);
		$data['judul1'] = array("Kode Group","Nama Group");
		$data['niljudul1'] = array($header->kodegrp,$header->namagrp);
		$data['judul2'] = array("Status","Tanggal");
		$data['niljudul2'] = array($header->status,$header->tgl);
		$data['judullap'] = "Kode External";
		$data['url'] = "kodeext/printThis/".$id;
		$data['colspan_line'] = 4;
		$data['tipe_judul_detail'] = array("normal","normal","normal","normal");
		$data['judul_detail'] = array("Kode","Nama","Kode Ext","Nama Ext");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0);
		for($m=0;$m<count($detail);$m++)
		{			
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['PCode']);
			$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
			//$list_detail[] = $hasil;
			$list_detail[] = $detail[$m]['PCodeExt'];
			$list_detail[] = $detail[$m]['NamaExt'];
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array();//,"PPn Jual (".$header->PPn."%)","Total Order");
		$data['footer2']  = array();//,round($header->PPn/100*$header->Jumlah),round($header->Total));
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

    function delete_kodeext(){
     	$kode = $this->input->post('kode');
		$pcode = $this->input->post('pcode');
		$this->kodeextmodel->locktables('kodeextheader,kodeextdetail');
		$this->db->delete('kodeextheader', array( 'kodeext' => $kode ));
		$this->db->delete('kodeextdetail', array( 'kodeext' => $kode ));
		$this->kodeextmodel->unlocktables();
	}

    function edit_kodeext($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
			$pcode = $this->uri->segment(5);
			$aplikasi = $this->kodeextmodel->getDate();
			$data['header']	= $this->kodeextmodel->getHeader($id);
	    	$data['detail']	= $this->kodeextmodel->getDetail($id);	
			if($data['header']->status=="A")
			{
			$data['checkeda']     = "checked='checked'";
			$data['checkedt']     = "";
			}
			if($data['header']->status=="T")
			{
			$data['checkedt']     = "checked='checked'";
			$data['checkeda']     = "";
			}
			$data['track'] = $mylib->print_track();
			$this->load->view('master/kodeext/edit_kodeext', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	function getPCode()
	{
		$kode = $this->input->post('pcode');
		$detail = $this->kodeextmodel->getPCodeDet($kode);
		if(count($detail)!=0)
		{	
         	$nilai = '01*_*'.$detail->PCode.'*_*'.$detail->NamaLengkap."*_*". //012
			   $detail->SatuanSt.'*_*'.$detail->NamaSatuanSt.'*_*'.  //34
			   $detail->Harga0b.'*_*'.$detail->Konv0st.'*_*'.  //56
			   $detail->Harga1b.'*_*'.$detail->Konv1st.'*_*'.  //78
			   $detail->Harga2b.'*_*'.$detail->Konv2st.'*_*'.  //90
			   $detail->Harga3b.'*_*'.$detail->Konv3st.'*_*'.  //12
			   $detail->KdKategori.'*_*'.$detail->KdBrand.'*_*'.  //34
			   $detail->Satuan0.'*_*'.$detail->Nama0.'*_*'.   //56
			   $detail->Satuan1.'*_*'.$detail->Nama1.'*_*'.   //78
			   $detail->Satuan2.'*_*'.$detail->Nama2.'*_*'.   //90 
			   $detail->Satuan3.'*_*'.$detail->Nama3;   //12
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}

	function save_new_kodeext(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$namagrp = strtoupper($this->input->post('namagrp'));
		$tgl = $this->input->post('tgl');
		$status = $this->input->post('sumber');
		$pcode1 = $this->input->post('pcode');
		$pcodesave1 = $this->input->post('savepcode');
		$pcodeext1 = $this->input->post('pcodeext');
		$namaext1 = $this->input->post('namaext');
		if($no=="")
		{
			$counter = "1";
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$namagrp,$status,$user);
		}
		else
		{
		   	$counter = $this->updateHeader($flag,$mylib->ubah_tanggal($tgl),$no,$namagrp,$status,$user);
		}
	
		for($x=0;$x<count($pcode1)-1;$x++)
		{
			$pcode = strtoupper(addslashes(trim($pcode1[$x])));
			$pcodeext = trim($pcodeext1[$x]);
			$pcodesave = $pcodesave1[$x];
			$namaext = $namaext1[$x];
			if($pcode!=""){
				$this->insertDetail($flag,$no,$counter,$pcode,$pcodeext,$namaext,$user,$pcodesave);
				$counter++;
			}
		}
		redirect('/master/kodeext/');
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$namagrp = strtoupper($this->input->post('namagrp'));
		$status = $this->input->post('sumber');

		$pcode = strtoupper(addslashes(trim($this->input->post('pcode'))));
		$pcodesave = $this->input->post('pcodesave');
		$pcodeext = $this->input->post('pcodeext');
		$namaext = strtoupper($this->input->post('namaext'));
		if($no=="")
		{
			$counter = "1";
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$namagrp,$status,$user);
		}
		else
		{
			$counter = $this->updateHeader($flag,$mylib->ubah_tanggal($tgl),$no,$namagrp,$status,$user);
		}
		$this->insertDetail($flag,$no,$counter,$pcode,$pcodeext,$namaext,$user,$pcodesave);
		echo $no;
	}

	function insertNewHeader($flag,$tgl,$namagrp,$status,$user)
	{
		$this->kodeextmodel->locktables('kodeextlist,kodeextheader');
		$no   = "E".substr("000".$this->kodeextmodel->getidcounter()->grpext,-3);
		$data = array(
		    'kodegrp' => $no,
			'namagrp' => $namagrp,
			'tgl'	=> $tgl,
			'status'	=> $status,
			'adddate'    => $tgl,
			'adduser'    => $user
		);
		$this->db->insert('kodeextheader', $data);
		$this->kodeextmodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$tgl,$no,$namagrp,$status,$user)
	{
		$this->kodeextmodel->locktables('kodeextheader');
		$count = $this->kodeextmodel->getCounter($no);
		$counter = (int)$count->counter + 1;
		$data = array(
			'namagrp' => $namagrp,
			'status' => $status
		);
		if($flag=="edit")
		{
			$data['editdate'] = $tgl;
			$data['edituser'] = $user;
		}
		$this->db->update('kodeextheader', $data, array('kodegrp' => $no));
		$this->kodeextmodel->unlocktables();
		return $counter;
	}
    function insertDetail($flag,$no,$counter,$pcode,$pcodeext,$namaext,$user,$pcodesave)
	{
		$tgl = $this->session->userdata('Tanggal_Trans');
		$this->kodeextmodel->locktables('kodeextdetail');
		if($pcodesave==""){
			$data = array(
				'kodegrp' => $no,
				'pcode' => $pcode,
				'pcodeext' => $pcodeext,
				'namaext' => $namaext,
				'counter' => $counter
			);
			$this->db->insert('kodeextdetail', $data);
		}
		else if($pcodesave!=$pcode)
		{
			$data = array(
				'pcode' => $pcode,
				'pcodeext' => $pcodeext,
				'namaext' => $namaext
			);
			if($flag=="edit")
			{
				$data['editdate'] = $tgl;
				$data['edituser'] = $user;
			}
			$this->db->update('kodeextdetail', $data, array('kodegrp' => $no,'pcode'=>$pcodesave));
		}
		else if($pcodesave==$pcode)
		{
			$data = array(
				'pcodeext' => $pcodeext,
				'namaext' => $namaext
			);
			if($flag=="edit")
			{
				$data['editdate'] = $tgl;
				$data['edituser'] = $user;
			}
			$this->db->update('kodeextdetail', $data, array('kodegrp' => $no,'pcode'=>$pcodesave));
		}
		$this->kodeextmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$pcode = $this->input->post('pcode');
		$this->kodeextmodel->locktables('kodeextdetail');
		$this->db->delete('kodeextdetail', array('kodegrp' => $id,'pcode' => $pcode));
		$this->kodeextmodel->unlocktables();
	}

}
?>