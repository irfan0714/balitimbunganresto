<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Biro_Tour_Travel extends authcontroller {
    
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('master/biro_tourmodel');   
    }
    
    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);	     	
	     	$id 		  = $this->input->post('stSearchingKey');
	        $with 		  = $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;        
			$config['base_url']       = base_url().'index.php/master/biro_tour_travel/index/';
			$page					  = $this->uri->segment(4);		
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
			 	$id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/master/biro_tour_travel/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/master/biro_tour_travel/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);

            $data['header'] = array('Kode','Nama','Contact','Phone','Fax','Alamat','Email','Kota','Telepon','Status','AddDate','EditDate');
	        $config['total_rows']     = $this->biro_tourmodel->num_typetour_row($id,$with);
	        $data['detail']       = $this->biro_tourmodel->gettypetourList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('master/tourtravel/viewlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function add_new(){

     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
            $data=  array(
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'label'  => "Add Tour Travel",
	     	    'msg'    => "",
	     	    'kode'   => "",
	     	    'Kode'   => "",
                'Nama'   => "",
                'Contact'=> "",
                'Alamat' => "",
                'Kota'   => "",
                'Phone'  => "",
                'Fax'       => "",
                'Email'     => "",
                'AddDate'   => "",
                'EditDate'  => "",
                'Telepon'   => "",
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                'TypeTour'  => $this->biro_tourmodel->getType(),
                'track'     => $mylib->print_track()
            );
	    	$this->load->view('master/tourtravel/add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function delete(){
     	$id = $this->input->post('kode');
		$this->db->delete('tourtravel', array('KdTravel' => "$id"));
        echo "sukses";
//		redirect('/master/biro_tour_travel/');
	}
    
    function edit($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){

			$id 		    = $this->uri->segment(4);
	    	$detail         = $this->biro_tourmodel->getDetail($id);
	    	$data['detail'] = $this->biro_tourmodel->getDetail($id);
            $data =  array(
                'label'  => "Edit Tour Travel",
                'tr1'    => $this->uri->segment(1),
                'tr2'    => $this->uri->segment(2),
                'tr3'    => "save_edit",
                'detail' => $detail,
                'msg'    => "",
                'Kode'   => $detail->KdTravel,
                'Nama'   => $detail->Nama,
                'Contact'=> $detail->Contact,
                'Alamat' => $detail->Alamat,
                'Kota'   => $detail->Kota,
                'Phone'  => $detail->Phone,
                'Fax'       => $detail->Fax,
                'Email'     => $detail->Email,
                'Telepon'   => $detail->Telepon,
                'typenya'   => $detail->KdTypeTour,
                'statusnya'    => $detail->Aktif,
                'Aktif'     => Array ( Array ( 'Kd' => 'A', 'Ket' => 'Aktif' ),Array ( 'Kd' => 'T', 'Ket' => 'Tidak' )  ) ,
                'TypeTour'  => $this->biro_tourmodel->getType(),
            );
	    	$data['edit'] 	= true;
			$data['track']  = $mylib->print_track();
	    	$this->load->view('master/tourtravel/edit', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function save_edit(){
//        print_r($_POST);die();
        $nama = strtoupper(addslashes(trim($this->input->post('Nama'))));
        $id = strtoupper(addslashes(trim($this->input->post('Kode'))));
        $type = strtoupper(addslashes(trim($this->input->post('type'))));
        $Phone = strtoupper(addslashes(trim($this->input->post('Phone'))));
        $Fax = strtoupper(addslashes(trim($this->input->post('Fax'))));
        $contact = strtoupper(addslashes(trim($this->input->post('Contact'))));
        $alamat = strtoupper(addslashes(trim($this->input->post('Alamat'))));
        $kota = strtoupper(addslashes(trim($this->input->post('Kota'))));
        $telp = strtoupper(addslashes(trim($this->input->post('Telepon'))));
        $Email = strtoupper(addslashes(trim($this->input->post('Email'))));
        $status = strtoupper(addslashes(trim($this->input->post('status'))));

        $tgltrans = $this->session->userdata('Tanggal_Trans');
        $data = array(
            'Nama'      => $nama,
            'Contact'   => $contact,
            'Phone'     => $Phone,
            'Fax'       => $Fax,
            'Alamat'    => $alamat,
            'Email'     => $Email,
            'Kota'      => $kota,
            'Telepon'   => $telp,
            'Aktif'     => $status,
            'ADDDATE'   => $tgltrans,
            'KdTypeTour'=> $type
        );
		$this->db->update('tourtravel', $data, array('KdTravel' => $id));
    	redirect('/master/biro_tour_travel/');
    }
    function save(){
        $nama = strtoupper(addslashes(trim($this->input->post('Nama'))));
        $idnama  = substr($nama,0,2);
        $id      = $idnama.substr("00000".$this->biro_tourmodel->getidcounter($idnama)->Counter,-4);
    	$type = strtoupper(addslashes(trim($this->input->post('type'))));
    	$Phone = strtoupper(addslashes(trim($this->input->post('Phone'))));
    	$Fax = strtoupper(addslashes(trim($this->input->post('Fax'))));
        $contact = strtoupper(addslashes(trim($this->input->post('Contact'))));
        $alamat = strtoupper(addslashes(trim($this->input->post('Alamat'))));
        $kota = strtoupper(addslashes(trim($this->input->post('Kota'))));
        $telp = strtoupper(addslashes(trim($this->input->post('Telepon'))));
        $Email = strtoupper(addslashes(trim($this->input->post('Email'))));
        $status = strtoupper(addslashes(trim($this->input->post('status'))));

    	$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
                'KdTravel'  => $id,
                'Nama'      => $nama,
                'Contact'   => $contact,
                'Phone'     => $Phone,
                'Fax'       => $Fax,
                'Alamat'    => $alamat,
                'Email'     => $Email,
                'Kota'      => $kota,
                'Telepon'   => $telp,
                'Aktif'     => $status,
                'ADDDATE'   => $tgltrans,
                'KdTypeTour'=> $type
            );
            $this->db->insert('tourtravel', $data);
		redirect('/master/biro_tour_travel/');

	}
	
	function cek_double(){
		$phone		= $this->input->post('tlp');
		$cek     	= $this->biro_tourmodel->cekDetail($phone);
		
		if(empty($cek)){
			$data['cek']=FALSE;
		}else{
			$data['cek']=TRUE;
		}
		echo json_encode($data);
	}
}
?>