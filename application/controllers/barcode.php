<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Barcode extends authcontroller {
	
	function __construct(){
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('barcodemodel');
    }
	
	function index(){
		//$this->load->view('barcodeview');
		$this->load->view('create_barcode');
	}

	function gambar($kode){
		$height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '74';	
		$width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		$barcodeOPT = array(
			'text' => $kode, 
			'barHeight'=> $height, 
			'factor'=>$width,
		);
			
		$renderOPT = array();
		$render = Zend_Barcode::factory(
		'code39', 'image', $barcodeOPT, $renderOPT
		)->render();
	}
	
	function create(){
		$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
			$data['SubDivisi'] = $this->barcodemodel->getSubdivisi();
			$data['Product'] = $this->barcodemodel->getBarang();
			$data['action'] = "cetak";
            $this->load->view('create_barcode',$data);
        } else {
            $this->load->view('denied');
        }
	}
	
	function get_product($SubDivisi){
		$data['Product'] = $this->barcodemodel->getProduct($SubDivisi);
        $this->load->view('formproduct',$data);
	}
	
	function cetak(){
		$kdPro = $this->input->post('KdProduk');
		$qty = $this->input->post('jml');
		$date = date('Y-m-d H:i:s');
		$getBarcode = $this->barcodemodel->getBarcode($kdPro);
		if(!empty($getBarcode)){
			$barcode = $getBarcode[0]['Barcode1'];
			$nama = $getBarcode[0]['NamaInitial'];
			$harga = $getBarcode[0]['Harga1c'];
			//CETAK BARCODE
			if($barcode != 0){	
				$data['barcode'] = $barcode;
				$data['nama'] = $nama;
				$data['harga'] = $harga;
				if($barcode=='8997016379799'){
					$data['harga'] = 0;
				}
				$data['jml'] = $this->input->post('jml');
				$this->load->helper('print');
				$html = $this->load->view('cetak_barcode',$data, TRUE);
				
				$filename	='barcode';
				$ext		='bcode';
				header('Content-Type: application/bcode');
				header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
				header('Cache-Control: private, max-age=0, must-revalidate');
				header('Pragma: public');
				print $html;
			}
		}
		//INSERT LOG BARCODE
		$data = array(
			'PCode' => $kdPro,
			'Qty' => $qty,
			'Date' => $date
		);
		$this->db->insert('trans_barcode', $data);
		//redirect('barcode/create');
		//$this->load->view('cetak_barcode');
	}
}
?>