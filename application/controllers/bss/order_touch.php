<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class order_touch extends authcontroller {
	function __construct(){
		parent::__construct();
		//error_reporting(0);
		$this->load->library('globallib');
		$this->load->model('bss_model/order_touch_model');
	}

	function index(){
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");
		if($sign=="Y")
		{
			$segs 		  = $this->uri->segment_array();
			$arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
			$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
			$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
			$with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
			$this->load->library('pagination');

			$config['full_tag_open']  = '<div class="pagination">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open']   = '<span class="current">';
			$config['cur_tag_close']  = '</span>';
			$config['per_page']       = '12';
			$config['first_link'] 	  = 'First';
			$config['last_link'] 	  = 'Last';
			$config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/bss/order_touch/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
				if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/bss/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
				else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
					$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
					$config['base_url']     = base_url().'index.php/bss/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Kassa","No Transaksi","Tanggal","Waktu","Total Item","Table","Person", "Aksi");
			$config['total_rows']		= $this->order_touch_model->num_order_row($id,$with);
			$data['data']	= $this->order_touch_model->getList($config['per_page'],$page,$id,$with);
			$data['tanggal'] = $this->order_touch_model->getDate();
			$data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
			$this->load->view('bss/order_touch/order_touch_list', $data);
		}
		else{
			$this->load->view('denied');
		}
	}
	function add_new(){
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("add");
		if($sign=="Y"){
			$ipaddres                   = $this->session->userdata('ip_address');
			$nokassa                    = $this->order_touch_model->getnokassa($ipaddres);
			$datakassa                  = $this->order_touch_model->getkassa($ipaddres);
			$data['NoKassa'] 						= $nokassa;
			$data['msg'] 								= "";
			$data['datakassa'] 					= $datakassa;
			$user_id 										= $this->session->userdata('userid');
			$username 									= $this->session->userdata('username');
			$data['user_id'] 						= $user_id;
			$data['username'] 					= $username;
			$kddivisi                   = $datakassa[0]['KdKategori'];
			$data['userid']	 	        	= $this->order_touch_model->getUserId();
			$data['barang']	 		    		= $this->order_touch_model->getBarang();
			$data['note']	 		    			= $this->order_touch_model->getNote();
			$data['kategori']	 					= $this->order_touch_model->getKategori();
			$data['subkategori']	 			= $this->order_touch_model->getSubKategori();
			$data['userid']	 	        	= $this->order_touch_model->getUserId();
			$data['lokasi']	 	        	= $this->order_touch_model->getLokasi($kddivisi);
			$data['keterangan']	 	    	= $this->order_touch_model->getKeterangan();
			$data['store']							= $this->order_touch_model->aplikasi();
			$aplikasi 									= $this->order_touch_model->getDate();
			$data['tanggal'] 						= $aplikasi->TglTrans2;
			
			//----------------untuk keperluan lcd----
			$ipaddress			= $_SERVER['REMOTE_ADDR'];
			$data['iplokal']	= $ipaddress;
			//---------------eo untuk keper....------
			$this->load->view('bss/order_touch/add_order_touch',$data);
		}
		else{
			$this->load->view('denied');
		}
	}


	function save_trans()
	{
		$transaksi 		= $this->input->post('transaksi');
		if($transaksi=="yes")
		{
			$EditFlg    = $this->input->post('EditFlg');
			$nourut1    = $this->input->post('nourut');
			$pcode1     = $this->input->post('pcode');
			$qty1       = $this->input->post('qty');
			$berat1     = $this->input->post('berat');
			$keterangan1= $this->input->post('keterangan');
			$kassa   		= $this->input->post('kassa');
			$kasir   		= $this->input->post('kasir');
			$store	 		= $this->input->post('store');

			$kategorikassa	 = $this->input->post('kategorikassa');
			$tgltrans = $this->session->userdata('Tanggal_Trans');
			$userid = $this->session->userdata('userid');
			$username = $this->session->userdata('username');
			$tahun = substr($tgltrans,0,4);
			$bulan = substr($tgltrans,5,2);

			$idmeja	        = $this->input->post('idtable');
			$idguest        = $this->input->post('idguest');
			$idagent       = $this->input->post('idagent');
			if($idguest=="")$idguest=0;
			$personal	    = $this->input->post('idpersonal');
			$namapersonal = $this->input->post('namapersonal');
			$adddate        = date('y-m-d');
			$Jamsekarang = date("H:i:s");

			$data = array(
				'NoKassa'	   => $kassa,
				'NoStruk'	   => '',
				'Tanggal'	   => $adddate,
				'Waktu'	       => $Jamsekarang,
				'Kasir'		   => $kasir,
				'KdStore'	   => $store,
				'TotalItem'    => count($pcode1),
				'Status'	  => "0",
				'KdMeja'	   => $idmeja,
				'KdAgent'	   => $idagent,
				'KdPersonal' => $namapersonal,
				'TotalGuest'   => $idguest
			);
			$this->db->insert('trans_order_header', $data);
			$notrans = $this->db->insert_id();

			for($x0=0;$x0<count($pcode1);$x0++)
			{
				$pcode = strtoupper(addslashes(trim($pcode1[$x0])));
				$nourut = $nourut1[$x0];
				$qty = trim($qty1[$x0]);
				$berat = trim($berat1[$x0]);
				$keterangan = $keterangan1[$x0];

				if($pcode!="" && $qty>0){
					if(
						$pcode!='1234567890' OR
						$pcode!='1234567891' OR
						$pcode!='1234567892' OR
						$pcode!='1234567893' OR
						$pcode!='1234567894' OR
						$pcode!='1234567895' OR
						$pcode!='1234567896' OR
						$pcode!='1234567897' OR
						$pcode!='1234567898'
					){
					$data = array(
						'NoKassa'	   => $kassa,
						'NoTrans'	   => $notrans,
						'NoUrut'       => $nourut,
						'Tanggal'	   => $adddate,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Berat'        => round($berat,2),
						'Status'       => "0",
						'Keterangan'	=> $keterangan,
						'KdMeja' 		=> $idmeja
					);
					$this->db->insert('trans_order_detail', $data);
				  }


					//jika menu baru maka langsung saja di inserti di masterbarang
					if(
						$pcode=='1234567890' OR
						$pcode=='1234567891' OR
						$pcode=='1234567892' OR
						$pcode=='1234567893' OR
						$pcode=='1234567894' OR
						$pcode=='1234567895'
					){
						$v_PCode = $this->get_code_PCode('01', '3', '0317');
			   	  $v_Barcode1 = $v_PCode;

						$datax=array(
							'PCode' 				=> $v_PCode,
							'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'NamaStruk' 			=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'NamaInitial' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'SatuanSt' 			=> 'PCS',
							'harga1c' 			=> 0,
							'Barcode1' 			=> $v_Barcode1,
							'KdDivisi' 			=> '1',
							'KdSubDivisi' 		=> '01',
							'KdKategori' 			=> '3',
							'KdSubKategori' 		=> '0317',
							'KdGroupBarang'		=> '2',
							'Status' 				=> 'A',
							'AddDate' 			=> date('Y-m-d h:i:s'),
							'FlagStock' 			=> 'Y',
							'FlagPembelian' 		=> 'Y',
							'FlagPenjualanPOS' 	=> 'N',
							'FlagPenjualanPOSTouch'=> 'Y',
							'SubKategoriPOSTouch' => '0302',
							'PPN' 				=> '10',
							'komisi' 				=> '10',
							'DiscInternal' 		=> '20',
							'AddUser' 			=> 'System'
						);
						$this->db->insert('masterbarang',$datax);

						$datak = array(
							'PCode' 				=> $v_PCode,
							'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'harga1c' 			=> 0,
							'Barcode1' 			=> $v_Barcode1,
							'Service_charge' 		=> '5',
							'komisi' 				=> '10',
							'Jenis' 				=> '2',
							'PPN' 				=> '10',
							'KdKategori' 			=> '01',
							'KdSubKategori' 		=> '0302',
							'Satuan1' 			=> 'PCS',
							'FlagReady' 			=> 'Y',
							'FlagStock' 			=> 'Y',
							'Printer'				=> '01',
							'DiscInternal' 		=> '20'
						);

						$this->db->insert('masterbarang_touch',$datak);
						$this->db->delete('trans_order_detail',array('PCode'=>$pcode));
						$datad= array(
							'NoKassa'	   => $kassa,
							'NoTrans'	   => $notrans,
							'NoUrut'       => $nourut,
							'Tanggal'	   => $adddate,
							'Waktu'	       => $Jamsekarang,
							'Kasir'		   => $kasir,
							'KdStore'	   => $store,
							'PCode'        => $v_PCode,
							'Qty'          => round($qty,2),
							'Berat'        => round($berat,2),
							'Status'       => "0",
							'Keterangan'	=> $keterangan,
							'KdMeja' 		=> $idmeja,
							'MenuBaru'=>1
						);
						$this->db->insert('trans_order_detail', $datad);


					}

					if(
						$pcode=='1234567896' OR
						$pcode=='1234567897' OR
						$pcode=='1234567898'
					){
						$v_PCode = $this->get_code_PCode('01', '3', '0317');
						$v_Barcode1 = $v_PCode;

						$dataxx=array(
							'PCode' 				=> $v_PCode,
							'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'NamaStruk' 			=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'NamaInitial' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'SatuanSt' 			=> 'PCS',
							'harga1c' 			=> 0,
							'Barcode1' 			=> $v_Barcode1,
							'KdDivisi' 			=> '1',
							'KdSubDivisi' 		=> '25',
							'KdKategori' 			=> '3',
							'KdSubKategori' 		=> '0310',
							'KdGroupBarang'		=> '2',
							'Status' 				=> 'A',
							'AddDate' 			=> date('Y-m-d h:i:s'),
							'FlagStock' 			=> 'Y',
							'FlagPembelian' 		=> 'Y',
							'FlagPenjualanPOS' 	=> 'N',
							'FlagPenjualanPOSTouch'=> 'Y',
							'SubKategoriPOSTouch' => '0303',
							'PPN' 				=> '10',
							'komisi' 				=> '10',
							'DiscInternal' 		=> '20',
							'AddUser' 			=> 'System'
						);
						$this->db->insert('masterbarang',$dataxx);

						$datakk = array(
							'PCode' 				=> $v_PCode,
							'NamaLengkap' 		=> strtoupper(preg_replace("/[^a-zA-Z0-9]/", " ", $keterangan)),
							'harga1c' 			=> 0,
							'Barcode1' 			=> $v_Barcode1,
							'Service_charge' 		=> '5',
							'komisi' 				=> '10',
							'Jenis' 				=> '2',
							'PPN' 				=> '10',
							'KdKategori' 			=> '01',
							'KdSubKategori' 		=> '0303',
							'Satuan1' 			=> 'PCS',
							'FlagReady' 			=> 'Y',
							'FlagStock' 			=> 'Y',
							'Printer'				=> '01',
							'DiscInternal' 		=> '20'
						);

						$this->db->insert('masterbarang_touch',$datakk);
						$this->db->delete('trans_order_detail',array('PCode'=>$pcode));
						$datadd= array(
							'NoKassa'	   => $kassa,
							'NoTrans'	   => $notrans,
							'NoUrut'       => $nourut,
							'Tanggal'	   => $adddate,
							'Waktu'	       => $Jamsekarang,
							'Kasir'		   => $kasir,
							'KdStore'	   => $store,
							'PCode'        => $v_PCode,
							'Qty'          => round($qty,2),
							'Berat'        => round($berat,2),
							'Status'       => "0",
							'Keterangan'	=> $keterangan,
							'KdMeja' 		=> $idmeja,
							'MenuBaru'=>1
						);
						$this->db->insert('trans_order_detail', $datadd);
					}


				}
			}

			$corporate = $this->session->userdata('Alias');
			if($corporate=='BEBEK TIMBUNGAN SUNSET'){
				$this->cetakall2($notrans);
			}else{
				$this->cetakall($notrans);
			}
		}
		else
		$this->index();

	}


	// NPM
	function cetakmeja($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		//print_r($data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		redirect('bss/order_touch','refresh');
	}

	function cetakminum($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		redirect('bss/order_touch','refresh');
	}

	function cetakmakan($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);

		redirect('bss/order_touch','refresh');
	}

	function cetakgrill($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_grill'];
		$data['nm_printer'] = $printer[0]['nm_printer_grill'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,4);
		$data['jenis'] = 4;
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);

		redirect('bss/order_touch','refresh');
	}
	// NPM


	function get_code_PCode($v_KdSubDivisi, $v_KdKategori, $v_KdSubKategori)
	{
		//echo $v_KdSubDivisi." - ". $v_KdKategori." - ". $v_KdSubKategori;die;
		//2 sub divisi, 1 digit kategori, digit sub kategori, 4 angka counter

		if($v_KdSubDivisi==13 || $v_KdSubDivisi==19 || $v_KdSubDivisi==20)
		{
			$jml_string = strlen($v_KdSubDivisi.$v_KdSubKategori)*1;
			$q = "
			SELECT
			RIGHT(masterbarang.PCode,3) AS last_counter
			FROM
			masterbarang
			WHERE
			1
			AND LEFT(masterbarang.PCode, $jml_string) = '".$v_KdSubDivisi.$v_KdSubKategori."'
			ORDER BY
			masterbarang.PCode*1 DESC
			LIMIT
			0,1
			";
			$qry = mysql_query($q);
			$row = mysql_fetch_array($qry);
			list($last_counter) = $row;

			$counternya = ($last_counter*1) + 1;
			$PCode_oke  = $v_KdSubDivisi.$v_KdSubKategori.sprintf("%03s", $counternya);
		}
		else
		{
			$strlen_KdSubDivisi   = strlen($v_KdSubDivisi);
			$strlen_KdKategori    = strlen($v_KdKategori);
			$strlen_KdSubKategori = strlen($v_KdSubKategori);

			$total_digit = ($strlen_KdSubDivisi*1)+($strlen_KdKategori*1)+($strlen_KdSubKategori*1);
			$digit = $v_KdSubDivisi.$v_KdKategori.$v_KdSubKategori;

			$q = "
			SELECT
			RIGHT(masterbarang.PCode,4) AS last_counter
			FROM
			masterbarang
			WHERE
			1
			AND LEFT(masterbarang.PCode, ".$total_digit.") = '".$digit."'
			ORDER BY
			masterbarang.PCode*1 DESC
			LIMIT
			0,1
			";
			$qry = mysql_query($q);
			$row = mysql_fetch_array($qry);
			list($last_counter) = $row;
			//die();

			$counternya = ($last_counter*1) + 1;
			$PCode_oke  = $digit.sprintf("%04s", $counternya);
		}

		return $PCode_oke;
	}

	function cetakmeja2($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		//print_r($data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		redirect('bss/order_touch','refresh');
	}

	function cetakminum2($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		redirect('bss/order_touch','refresh');
	}

	function cetakmakan2($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);

		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
		$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);

		redirect('bss/order_touch','refresh');
	}

	
	
	function cetakall2($notrans){
		$username = $this->session->userdata('username');
		// Meja
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		if($username=='krisna337'){
			echo "meja";
			print_r($data);
		}else{
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);

		}
		// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		
		//Minuman
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		if($username=='krisna337'){
			echo "minuman";
			print_r($data);
		}else{
			if(!empty($data['detail'][0]['PCode'])){
			// echo "ISI";
			$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			}else{
				// echo "Kosong";
			}
		}
		
		

		
		//Makanan
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		if($username=='krisna337'){
			echo "makanan";
			print_r($data);
		}else{
			if(!empty($data['detail'][0]['PCode'])){
			
			// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			}else{
				// echo "Kosong";
			}
		}
		

		//LIVE COOKING 
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_cooking'];
		$data['nm_printer'] = $printer[0]['nm_printer_cooking'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,4);
		$data['jenis'] = 4;
		if($username=='krisna337'){
			echo "cooking";
			print_r($data);

			if(!empty($data['detail'][0]['PCode'])){
			$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			}else{
				// echo "Kosong";
			}
		}else{

			if(!empty($data['detail'][0]['PCode'])){
			// echo "ISI";
			$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			// $this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
			}else{
				// echo "Kosong";
			}

		}
		
		
		
		redirect('bss/order_touch/add_new/','refresh');
	}

	function cetakall($notrans){

		$corporate = $this->session->userdata('Alias');

		if($corporate=='BEBEK TIMBUNGAN SUNSET'){
				$this->cetakall2($notrans);
		}else{
				
			
				// Meja
				$store = $this->order_touch_model->aplikasi();
				$ipaddres   = $this->session->userdata('ip_address');
				$printer = $this->order_touch_model->NamaPrinter($ipaddres);

				$data['store'] = $store;
				$data['ip'] = $printer[0]['ipprinter_order'];
				$data['nm_printer'] = $printer[0]['nm_printer_order'];
				$data['kdkategori'] = $printer[0]['KdKategori'];
				$data['header'] = $this->order_touch_model->all_trans($notrans);
				$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
				$data['jenis'] = 1;
				$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);

				//Minuman
				$store = $this->order_touch_model->aplikasi();
				$ipaddres   = $this->session->userdata('ip_address');
				$printer = $this->order_touch_model->NamaPrinter($ipaddres);

				$data['store'] = $store;
				$data['ip'] = $printer[0]['ipprinter_bar'];
				$data['nm_printer'] = $printer[0]['nm_printer_bar'];
				$data['kdkategori'] = $printer[0]['KdKategori'];
				$data['header'] = $this->order_touch_model->all_trans($notrans);
				$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
				$data['jenis'] = 2;
				if(!empty($data['detail'])){
					$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
				}

				//Makanan
				$store = $this->order_touch_model->aplikasi();
				$ipaddres   = $this->session->userdata('ip_address');
				$printer = $this->order_touch_model->NamaPrinter($ipaddres);

				$data['store'] = $store;
				$data['ip'] = $printer[0]['ipprinter_kitchen'];
				$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
				$data['kdkategori'] = $printer[0]['KdKategori'];
				$data['header'] = $this->order_touch_model->all_trans($notrans);
				$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
				$data['jenis'] = 3;
				if(!empty($data['detail'])){
					$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
					$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
					$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);
				}

				//Grill
				$store = $this->order_touch_model->aplikasi();
				$ipaddres   = $this->session->userdata('ip_address');
				$printer = $this->order_touch_model->NamaPrinter($ipaddres);

				$data['store'] = $store;
				$data['ip'] = $printer[0]['ipprinter_grill'];
				$data['nm_printer'] = $printer[0]['nm_printer_grill'];
				$data['kdkategori'] = $printer[0]['KdKategori'];
				$data['header'] = $this->order_touch_model->all_trans($notrans);
				$data['detail'] = $this->order_touch_model->det_trans($notrans,4);
				$data['jenis'] = 4;
				if(!empty($data['detail'])){
					$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
					$this->load->view('bss/order_touch/cetak_transaksi_wireness_summary', $data);
					$this->load->view('bss/order_touch/cetak_transaksi_wireness', $data);
				}
				redirect('bss/order_touch/add_new/','refresh');

		}
	}


	function cancelall($notrans){
		$this->db->delete('trans_order_header',array('NoTrans'=>$notrans));
		$this->db->delete('trans_order_detail',array('NoTrans'=>$notrans));
		redirect('bss/order_touch/','refresh');
	}


	function clear_trans()
	{
		$notrans 		= $this->input->post('notrans');
		$nokassa 		= $this->input->post('kassa1');
		//echo $notrans;
		//echo $nokassa;
		$this->order_touch_model->clear_trans($notrans,$nokassa);
		$this->add_new();
	}

	function cektable(){
		$kdmeja = $this->input->post('data');
		$tgl = date('y-m-d');
		$OpenOrder = $this->order_touch_model->getOpenOrder($tgl, $kdmeja);
		echo json_encode($OpenOrder);
	}
}
