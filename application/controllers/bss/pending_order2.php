<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pending_order extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/pending_order_model');
    }

    function index_()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_meja"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "pending_order", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_meja= explode("=", $arrSearch[1]); // search meja
                    $arr_search["search"]["meja"] = $search_meja[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_meja"] = $search_meja[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '50';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pending_order/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pending_order/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mmeja'] = $this->pending_order_model->getMeja();
            $data['allmeja'] = $this->pending_order_model->getAllMeja();
            echo "<pre>";print_r($data['allmeja']);echo "</pre>";die();
			$data['data'] = $this->pending_order_model->getPendingOrderList($config['per_page'], $page, $arr_search["search"]);
			$config['total_rows'] = $this->pending_order_model->num_tabel_row($arr_search["search"]);

			//cek-data ini adalah itu mencocokan antara pesanan order dengan pesanan complete/cancel, jika sama muncul tombol done di view
			$data['cek_data'] = $this->pending_order_model->cekPendingOrderList($config['per_page'], $page, $arr_search["search"]);

            //echo "Data 1 = ".COUNT($data['data'])."<br>";
			//echo "Data 2 = ".COUNT($data['cek_data']);
			if(COUNT($data['data'])!=0 and COUNT($data['cek_data'])!=0){
			if(COUNT($data['data'])==COUNT($data['cek_data'])){
			$data['complete']=1;
			}
			}

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/pending_order/pending_order_list', $data);
        } else {
            $this->load->view('denied');
        }
    }


    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {

            $data['mmeja'] = $this->pending_order_model->getMeja();
            $data['allmeja'] = $this->pending_order_model->getAllMeja();
			$data['track'] = $mylib->print_track();

            $this->load->view('transaksi/pending_order/pending_order_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search()
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die();
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'sales_invoice', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_meja=".$this->input->post('search_meja');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "pending_order",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pending_order/index/' . $query_id . '');
    }


	function cancel($notrans,$pcode)
    {
		$data = $this->pending_order_model->get_by_id($notrans,$pcode);

		echo json_encode($data);
    }

	function done2($user,$pass,$notrans,$pcode)
    {
        $cek_otorisasi=$this->pending_order_model->cek_otorisasi($user);

		if(empty($cek_otorisasi)){
		$this->session->set_flashdata('msg', array('message' => '<h4><b>Anda Tidak Punya Otorisasi.</b></h4>', 'class' => 'danger'));
		}else{
		$validasi_user_pass=$this->pending_order_model->validasi_user($user,$pass);
		//print_r($validasi_user_pass);die;
				if(empty($validasi_user_pass)){
				$this->session->set_flashdata('msg', array('message' => '<h4><b>Password Anda Salah.</b></h4>', 'class' => 'danger'));
				}else{

				$Jamsekarang = date("H:i:s");
				$data_update= array(
				    'Waktu'=>$Jamsekarang,
					'Status' => '2'
				);
				$this->db->update('trans_order_detail', $data_update, array('NoTrans' => $notrans,'Tanggal' => date('Y-m-d'),'PCode'=>$pcode));

				//cek apakah semua orderan dicancel
				$cek = $this->pending_order_model->cekAllCancel($notrans);
				if(count($cek)==0){
					$this->db->update('trans_order_header',array('Status'=>'2'),array('NoTrans'=>$notrans));
				}

				$this->session->set_flashdata('msg', array('message' => '<h4><b>Sukses Cancel Order.</b></h4>', 'class' => 'info'));
				}
		}

	redirect('/transaksi/pending_order/index/'.$search);
    }

    function complete($notrans,$pcode,$src)
    {
        $this->pending_order_model->locktables('trans_order_detail');

			if ($notrans)
			{
				$Jamsekarang = date("H:i:s");
				$data = array(
				    'Waktu'=>$Jamsekarang,
					'Status' => '1'
				);

				$this->db->update('trans_order_detail', $data, array('NoTrans' => $notrans,'Tanggal' => date('Y-m-d'),'PCode'=>$pcode));
			}

        $this->pending_order_model->unlocktables();

        //echo json_encode(array('status'=>TRUE));

        $this->session->set_flashdata('msg', array('message' => 'Menu Berhasil Complete', 'class' => 'success'));

        redirect('/transaksi/pending_order/index/'.$src);
    }

    function completenew()
    {

    $notrans= $this->uri->segment(4);
	  $pcode = $this->uri->segment(5);
		$jml= $this->uri->segment(6);
		$berat= $this->uri->segment(7);
		$waktu= $this->uri->segment(8);
    $meja= $this->uri->segment(9);
		//echo $pcode."<br>";
		//echo $berat;die;
    	//echo $notrans."#".$pcode."#".$jml;die;
    	$pecah_pcode = explode('-',$pcode);
    	$pecah_berat = explode('-',$berat);
    	$pecah_waktu = explode('-',$waktu);
    	//-----
    	for($x=0;$x<$jml;$x++){
        $this->pending_order_model->locktables('trans_order_detail');

			if ($notrans)
			{
				$Jamsekarang = date("H:i:s");
				$data = array(
				    'Waktu'=>$Jamsekarang,
					  'Status' => '1',
				    'Berat'=>$pecah_berat[$x]
				);

				$where = array(
                       'KdMeja' => $meja,
				               // 'NoTrans' => $notrans,
                       'Status' => '0',
				               'Tanggal' => date('Y-m-d'),
				               'PCode'=>$pecah_pcode[$x],
				               'Waktu'=>$pecah_waktu[$x]
								);
				$this->db->update('trans_order_detail', $data, $where);
			}
		}
        $this->pending_order_model->unlocktables();
        //----
        echo json_encode(array('status'=>TRUE));

        //$this->session->set_flashdata('msg', array('message' => 'Menu Berhasil Complete', 'class' => 'success'));

        //redirect('/transaksi/pending_order/index/'.$src);
    }

    function buka_menu()
    {

        $notrans= $this->uri->segment(4);
	    $pcode = $this->uri->segment(5);
		$meja= $this->uri->segment(6);

				$Jamsekarang = date("H:i:s");
				$data = array(
				    'Waktu'=>$Jamsekarang,
					'Status' => '0'
				);

				$where = array(
				               'NoTrans' => $notrans,
				               'Tanggal' => date('Y-m-d'),
				               'PCode'=>$pcode,
				               'KdMeja'=>$meja
								);
				$this->db->update('trans_order_detail', $data, $where);

        $this->pending_order_model->unlocktables();

    }

    function cancel_order($user,$pass,$notrans,$pcode,$src)
    {

    	$cek_otorisasi=$this->pending_order_model->cek_otorisasi($user);

		if(empty($cek_otorisasi)){
		$this->session->set_flashdata('msg', array('message' => '<h4><b>Anda Tidak Punya Otorisasi.</b></h4>', 'class' => 'danger'));
		}else{
		$validasi_user_pass=$this->pending_order_model->validasi_user($user,$pass);
		//print_r($validasi_user_pass);die;
				if(empty($validasi_user_pass)){
				$this->session->set_flashdata('msg', array('message' => '<h4><b>Password Anda Salah.</b></h4>', 'class' => 'danger'));
				}else{

				$Jamsekarang = date("H:i:s");
				$data_update= array(
				    'Waktu'=>$Jamsekarang,
					'Status' => '2'
				);
				$this->db->update('trans_order_detail', $data_update, array('NoTrans' => $notrans,'Tanggal' => date('Y-m-d'),'PCode'=>$pcode));

				//cek apakah semua orderan dicancel
				$cek = $this->pending_order_model->cekAllCancel($notrans);
				if(count($cek)==0){
					$this->db->update('trans_order_header',array('Status'=>'2'),array('NoTrans'=>$notrans));
				}

				$this->session->set_flashdata('msg', array('message' => '<h4><b>Sukses Cancel Order.</b></h4>', 'class' => 'info'));
				}
		}

        redirect('/transaksi/pending_order/index/'.$src);
    }

    function cancel_ordernew()
    {
    	$user= $this->uri->segment(4);
	    $pass = $this->uri->segment(5);
		$notrans= $this->uri->segment(6);
		$pcode = $this->uri->segment(7);
		$jml= $this->uri->segment(8);

        $pecah_pcode = explode('-',$pcode);

    	$cek_otorisasi=$this->pending_order_model->cek_otorisasi($user);

		if(empty($cek_otorisasi)){
		//$this->session->set_flashdata('msg', array('message' => '<h4><b>Anda Tidak Punya Otorisasi.</b></h4>', 'class' => 'danger'));
		echo json_encode(array('status'=>'3'));
		}else{
		$validasi_user_pass=$this->pending_order_model->validasi_user($user,$pass);
		//print_r($validasi_user_pass);die;
				if(empty($validasi_user_pass)){
				//$this->session->set_flashdata('msg', array('message' => '<h4><b>Password Anda Salah.</b></h4>', 'class' => 'danger'));
			    echo json_encode(array('status'=>'2'));
				}else{
				for($x=0;$x<$jml;$x++){

				$Jamsekarang = date("H:i:s");
				$data_update= array(
				    'Waktu'=>$Jamsekarang,
					'Status' => '2'
				);

				$where = array(
				               'NoTrans' => $notrans,
				               'Tanggal' => date('Y-m-d'),
				               'PCode'=>$pecah_pcode[$x]
								);

				$this->db->update('trans_order_detail', $data_update,$where);
				}
				//cek apakah semua orderan dicancel
				$cek = $this->pending_order_model->cekAllCancel($notrans);
				if(count($cek)==0){
					$this->db->update('trans_order_header',array('Status'=>'2'),array('NoTrans'=>$notrans));
				}
		        echo json_encode(array('status'=>'1'));
				//$this->session->set_flashdata('msg', array('message' => '<h4><b>Sukses Cancel Order.</b></h4>', 'class' => 'info'));
				}


		}

        //redirect('/transaksi/pending_order/index/'.$src);
        //echo json_encode(array('status'=>TRUE));
    }

    function change_table()
    {
    	$meja_from= $this->uri->segment(4);
	    $meja_to = $this->uri->segment(5);

	    $sql = "
	    		INSERT INTO `trans_order_header`
	            (
	             `NoKassa`,
	             `Tanggal`,
	             `Waktu`,
	             `Kasir`,
	             `KdStore`,
	             `TotalItem`,
	             `Status`,
	             `KdPersonal`,
	             `KdMeja`,
	             `KdContact`,
	             `nokasst`,
	             `nostruk`,
	             `TotalGuest`,
	             `AddDate`,
	             `keterangan`,
	             `KdAgent`,
	             `IsCommit`)

	               SELECT
					  `NoKassa`,
					  `Tanggal`,
					  `Waktu`,
					  `Kasir`,
					  `KdStore`,
					  `TotalItem`,
					  '0',
					  `KdPersonal`,
					  '$meja_to',
					  `KdContact`,
					  `nokasst`,
					  `nostruk`,
					  `TotalGuest`,
					  `AddDate`,
					  `keterangan`,
					  `KdAgent`,
					  `IsCommit`
					FROM `trans_order_header`
					WHERE KdMeja='$meja_from' AND `Tanggal` = CURDATE() and Status=0 LIMIT 0,1
	    		";

        $this->db->query($sql);

        //cari no trans dari header
        $notrans_new = $this->globalmodel->getQuery(" NoTrans from trans_order_header where KdMeja='$meja_to' AND Status='0' AND `Tanggal` = CURDATE();");
        $notrans_detail_new = $this->globalmodel->getQuery(" Waktu,PCode from trans_order_detail where KdMeja='$meja_from' AND Status='0' AND `Tanggal` = CURDATE();");

        for($x=0;$x<count($notrans_detail_new);$x++){
        $sql_detail = "
					INSERT INTO `trans_order_detail`
		            (`NoTrans`,
		             `NoUrut`,
		             `NoKassa`,
		             `Tanggal`,
		             `Waktu`,
		             `Kasir`,
		             `KdStore`,
		             `PCode`,
		             `Qty`,
		             `Berat`,
		             `Satuan`,
		             `Keterangan`,
		             `Note_split`,
		             `Status`,
		             `KdPersonal`,
		             `KdMeja`,
		             `KdContact`)

		               SELECT
		                  '".$notrans_new[0]['NoTrans']."',
						  $x,
						  d.NoKassa,
						  d.Tanggal,
						  '".$notrans_detail_new[$x]['Waktu']."',
						  d.Kasir,
						  d.KdStore,
						  d.PCode,
						  d.Qty,
						  d.Berat,
						  d.Satuan,
						  d.Keterangan,
						  d.Note_split,
						  d.Status,
						  d.KdPersonal,
						  '$meja_to',
						  d.KdContact
						FROM `trans_order_detail` d inner join trans_order_header h on h.NoTrans=d.Notrans
						WHERE h.KdMeja='$meja_from' AND d.Waktu='".$notrans_detail_new[$x]['Waktu']."' AND d.PCode='".$notrans_detail_new[$x]['PCode']."' AND h.Tanggal = CURDATE() AND h.Status=0 LIMIT 0,1";
				  $this->db->query($sql_detail);

				  $data_updt =  array('change_table'=>'Y');
				  $where_updt = array('PCode'=>$notrans_detail_new[$x]['PCode'],);
		}

        $this->db->update('trans_order_header',array('Status'=>'2'),array('Tanggal'=>date('Y-m-d'),'KdMeja'=>$meja_from, 'Status'=>0));
        $sql = "update `trans_order_detail` d inner join trans_order_header h on h.NoTrans=d.Notrans
        		set d.Status=2
				where h.KdMeja='$meja_from' AND h.Tanggal = CURDATE() AND h.Status=0";
		$this->db->query($sql_detail);

        echo json_encode(array('status'=>TRUE));
    }

    function change_sticker()
    {
    	$sticker_lama= $this->uri->segment(4);
	    $sticker_baru = $this->uri->segment(5);
	    $meja = $this->uri->segment(6);

	    $this->db->update('trans_order_header',array('KdAgent'=>$sticker_baru),array('KdAgent'=>$sticker_lama,'Tanggal'=>date('Y-m-d'),'KdMeja'=>$meja));

        echo json_encode(array('status'=>TRUE));
    }

    function getMejaAktif()
    {
	      $query = $this->pending_order_model->getMejaAktif();

	      echo "<option value=''>All</option>";
	      foreach ($query as $cetak) {
		  echo "<option value=$cetak[KdMeja]>$cetak[KdMeja]</option>";
		  }
    }

	function done($kdmeja)
    {
        $this->pending_order_model->locktables('trans_order_header');

			if ($kdmeja)
			{
			    $cek_apakah_meja_tersebut_ada_nilai_nol = $this->pending_order_model->cek_meja($kdmeja);
				foreach($cek_apakah_meja_tersebut_ada_nilai_nol as $val){
					if($val['Status']!=1){
						$data = array(
							'Status' => '1'
						);
						$this->db->update('trans_order_header', $data, array('KdMeja' => $kdmeja,'Tanggal' => date('Y-m-d')));
						}
				}
			}

        $this->pending_order_model->unlocktables();

        $this->session->set_flashdata('msg', array('message' => 'Order Is Done', 'class' => 'success'));

        redirect('/transaksi/pending_order/');
    }

	function done_old($notrans)
    {
        $this->pending_order_model->locktables('trans_order_header');

			if ($notrans)
			{
				$data = array(
					'Status' => '1'
				);

				$this->db->update('trans_order_header', $data, array('NoTrans' => $notrans,'Tanggal' => date('Y-m-d')));
			}

        $this->pending_order_model->unlocktables();

        $this->session->set_flashdata('msg', array('message' => 'Order Is Done', 'class' => 'success'));

        redirect('/transaksi/pending_order/');
    }


    function getList()
    {

    	    $keyword = $this->uri->segment(4);//meja


            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '50';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

			$data['data'] = $this->pending_order_model->getPendingOrderList($keyword,$config['per_page']);

			//cek-data ini adalah itu mencocokan antara pesanan order dengan pesanan complete/cancel, jika sama muncul tombol done di view
			$data['cek_data'] = $this->pending_order_model->cekPendingOrderList($keyword,$config['per_page']);

			if(COUNT($data['data'])!=0 and COUNT($data['cek_data'])!=0){
				if(COUNT($data['data'])==COUNT($data['cek_data'])){
					$data['complete']=1;
					}
			}

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();


		$this->load->view('transaksi/pending_order/pending_order_list_content', $data);
	}

}

?>
