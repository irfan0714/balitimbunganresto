<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Reservasi extends authcontroller {

  function __construct() {
    parent::__construct();
    error_reporting(0);
    $this->load->library('globallib');
    $this->load->model('globalmodel');
    $this->load->model('bss_model/reservasi_model');
  }

  function index() {
    $mylib = new globallib();
    $sign = $mylib->getAllowList("all");
    if ($sign == "Y") {
      $id = $this->uri->segment(4);
      $user = $this->session->userdata('username');

      $data["search_keyword"] = "";
      $resSearch = "";
      $arr_search["search"] = array();
      $id_search = "";

      $data["search_bulan"] = date('m');

      $data["search_tgl_awal"] = "01-".date("m-Y");
      $data["search_tgl_akhir"] = date("t-m-Y");

      if ($id * 1 > 0) {
        $resSearch = $this->globalmodel->getSearch($id, "reservasi", $user);
        $arrSearch = explode("&", $resSearch->query_string);

        $id_search = $resSearch->id;

        if ($id_search) {
          $search_keyword = explode("=", $arrSearch[0]); // search keyword
          $arr_search["search"]["keyword"] = $search_keyword[1];
          $data["search_keyword"] = $search_keyword[1];

          $search_supervisor = explode("=", $arrSearch[1]);
          $arr_search["search"]["supervisor"] = $search_supervisor[1];
          $data["search_head_channel"] = $search_supervisor[1];

          $search_status = explode("=", $arrSearch[2]);
          $arr_search["search"]["status"] = $search_status[1];
          $data["search_status"] = $search_status[1];

          /*$search_bulan = explode("=", $arrSearch[3]);
          $arr_search["search"]["bulan"] = $search_bulan[1];
          $data["search_bulan"] = $search_bulan[1];

          $search_tahun = explode("=", $arrSearch[4]);
          $arr_search["search"]["tahun"] = $search_tahun[1];
          $data["search_tahun"] = $search_tahun[1];*/

          $search_tgl_awal= explode("=", $arrSearch[3]); // search tgl_awal
          $arr_search["search"]["tgl_awal"] = $search_tgl_awal[1];
          $data["search_tgl_awal"] = $mylib->ubah_tanggal($search_tgl_awal[1]);

          $search_tgl_akhir= explode("=", $arrSearch[4]); // search tgl_ahkir
          $arr_search["search"]["tgl_akhir"] = $search_tgl_akhir[1];
          $data["search_tgl_akhir"] = $mylib->ubah_tanggal($search_tgl_akhir[1]);

          $search_sort= explode("=", $arrSearch[5]); // search tgl_ahkir
          $arr_search["search"]["sort"] = $search_sort[1];
          $data["search_sort"] = $search_sort[1];

        }
      }

      // pagination
      $this->load->library('pagination');
      $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      $config['full_tag_close'] = '</ul>';
      $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
      $config['cur_tag_close'] = '</a></li>';
      $config['per_page'] = '100';
      $config['first_link'] = 'First';
      $config['last_link'] = 'Last';
      $config['num_links'] = 2;

      if ($id_search) {
        $config['base_url'] = base_url() . 'index.php/bss/reservasi/index/' . $id_search . '/';
        $config['uri_segment'] = 5;
        $page = $this->uri->segment(5);
      } else {
        $config['base_url'] = base_url() . 'index.php/bss/reservasi/index/';
        $config['uri_segment'] = 4;
        $page = $this->uri->segment(4);
      }

      $data['kasbank'] = $this->reservasi_model->getKasBank();
      $data['supervisor'] = $this->reservasi_model->getSupervisor();
      $data['status'] = array(array('KdStatus'=>'B', 'NamaStatus'=>'Booking'),
      array('KdStatus'=>'P', 'NamaStatus'=>'Pending'),
      array('KdStatus'=>'W', 'NamaStatus'=>'Waiting Approve'),
      array('KdStatus'=>'A', 'NamaStatus'=>'Approve'),
      array('KdStatus'=>'R', 'NamaStatus'=>'Reject'),
      array('KdStatus'=>'C', 'NamaStatus'=>'Cancel')
    );

    $data['bulan'] = $this->session->userdata('bulanaktif');
    $data['tahun'] = $this->session->userdata('tahunaktif');

    $thnbln = $data['tahun'] . $data['bulan'];

    $config['total_rows'] = $this->reservasi_model->num_reservasi_row($arr_search["search"]);
    $data['data'] = $this->reservasi_model->getReservasiList($config['per_page'], $page, $arr_search["search"]);
    $UserLevel = $this->session->userdata('userlevel');
    $data['booking'] = $this->reservasi_model->getJmlBooking($UserLevel);
    $data['beo'] = $this->reservasi_model->getJmlBeo($UserLevel);
    $data['pending'] = $this->reservasi_model->getJmlBeoPending($UserLevel);
    $data['waiting'] = $this->reservasi_model->getJmlBeoWaiting($UserLevel);
    $data['approved'] = $this->reservasi_model->getJmlBeoApprove($UserLevel);
    $data['rejected'] = $this->reservasi_model->getJmlBeoReject($UserLevel);
    $data['canceled'] = $this->reservasi_model->getJmlBeoCancel($UserLevel);


    $data['track'] = $mylib->print_track();

    $this->pagination->initialize($config);
    $data["pagination"] = $this->pagination->create_links();

    $this->load->view('bss/reservasi/listreservasi', $data);
  } else {
    $this->load->view('denied');
  }
}

function search() {
  $mylib = new globallib();

  $user = $this->session->userdata('username');

  // hapus dulu yah
  $this->db->delete('ci_query', array('module' => 'reservasi', 'AddUser' => $user));

  $search_value = "";
  $search_value .= "search_keyword=" . $mylib->save_char($this->input->post('search_keyword'));
  $search_value .= "&search_head_channel=".$this->input->post('search_head_channel');
  $search_value .= "&search_status=".$this->input->post('search_status');
  $search_value .= "&search_tgl_awal=".$mylib->ubah_tanggal($this->input->post('v_tgl_mulai'));
  $search_value .= "&search_tgl_akhir=".$mylib->ubah_tanggal($this->input->post('v_tgl_akhir'));
  $search_value .= "&search_sort=".$this->input->post('search_sort');
  //$search_value .= "&search_bulan=".$this->input->post('search_bulan');
  //$search_value .= "&search_tahun=".$this->input->post('search_tahun');

  $data = array(
    'query_string' => $search_value,
    'module' => "reservasi",
    'AddUser' => $user
  );

  $this->db->insert('ci_query', $data);

  $query_id = $this->db->insert_id();

  redirect('/bss/reservasi/index/' . $query_id . '');
}

function getDetailMenu()
{
  $mylib = new globallib();
  $tipe = $this->uri->segment(4);
  if($tipe=="BFM"){
    $thn = $this->uri->segment(7);
    $bln = $this->uri->segment(8);
    $no = $this->uri->segment(9);
    $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
    $booking = "T";
  }else{
    $nodok = $this->uri->segment(4);
    $booking = "Y";
  }


  $data['nodokumen'] = $nodok;
  $data['booking'] = $booking;

  $data['detailMenu'] = $this->globalmodel->getQuery(" * from trans_reservasi_detail where NoDokumen='".$nodok."'");

  $this->load->view('bss/reservasi/detail_menu', $data);
}

function cancel_reservasi() {
  $id=$this->input->post('id');
  $alasan=$this->input->post('v_alasan');
  $this->db->update('trans_reservasi',
  array('status'=>4,'ket_reject'=>$alasan),
  array('id'=>$id));

  //update status batal di voucher_beo
  $beo = $this->globalmodel->getQuery(" * from trans_reservasi where id='".$id."'");
  if($beo[0]['VoucherCode']!=''){
    $this->db->update('voucher_beo',array('status'=>2),array('BEO'=>$beo[0]['NoDokumen']));

    //update status batal di voucher
    $voucher_internal = $this->globalmodel->getQuery(" * FROM `voucher_beo` a WHERE a.`BEO`='".$beo[0]['NoDokumen']."' ");
    $this->db->update('voucher',array('status'=>2),array('novoucher'=>$voucher_internal[0]['no_voucher']));
  }

  // redirect('/bss/reservasi/');
  echo json_encode(array("status" => TRUE));
}

function noseri(){
  $mylib = new globallib();
  $tglaja = $this->input->post('tgl');
  $getTgl = $mylib->ubah_tanggal($tglaja);
  $no = "BFM/BEO/SM/";
  $tglApp = $getTgl;
  $bulan = substr($tglApp, 5, 2);
  $thn = substr($tglApp, 0, 4);
  $bln = $this->dateRomawi(substr($tglApp, 5, 2));

  $getNoDokumen = $this->globalmodel->getQuery(" NoDokumen from trans_reservasi WHERE MONTH(TglDokumen) = '".$bulan."' AND YEAR(TglDokumen)='".$thn."' order by NoDokumen desc limit 1");
  if (!empty($getNoDokumen)) {
    $nodokumen = $getNoDokumen[0]['NoDokumen'];
    $noLast = (substr($nodokumen, -4) *1) +1;
    if (strlen($noLast) == "1") {
      $noDokNew = "0000" . $noLast;
    } elseif (strlen($noLast) == "2") {
      $noDokNew = "000" . $noLast;
    } elseif (strlen($noLast) == "3") {
      $noDokNew = "00" . $noLast;
    } elseif (strlen($noLast) == "4") {
      $noDokNew = "0" . $noLast;
    } elseif (strlen($noLast) == "5") {
      $noDokNew = $noLast;
    } else {
      $noDokNew = "00001";
    }
  } else {
    $noDokNew = "00001";
  }

  $data['NoDokumen'] = $no . $thn . "/" . $bln . "/" . $noDokNew;

  echo json_encode($data);

}

function noseri_booking(){
  $mylib = new globallib();
  $tglaja =$this->input->post('tgl');
  $getTgl = $mylib->ubah_tanggal($tglaja);
  $no = "BOOKING";
  $tglApp = $getTgl;
  $bulan = substr($tglApp, 5, 2);
  $years = substr($tglApp, 0, 4);
  $thn = substr($tglApp, 2, 2);

  $getNoDokumen = $this->globalmodel->getQuery(" NoDokumen from trans_reservasi WHERE MONTH(TglDokumen) = '".$bulan."' AND YEAR(TglDokumen)='".$years."' AND SUBSTR(NoDokumen, 1,11) = '" .$no.$thn.$bulan."' order by NoDokumen desc limit 1");
  if (!empty($getNoDokumen)) {
    $nodokumen = $getNoDokumen[0]['NoDokumen'];
    $noLast = (substr($nodokumen, -4) *1) +1;
    if (strlen($noLast) == "1") {
      $noDokNew = "000" . $noLast;
    } elseif (strlen($noLast) == "2") {
      $noDokNew = "00" . $noLast;
    } elseif (strlen($noLast) == "3") {
      $noDokNew = "0" . $noLast;
    }  elseif (strlen($noLast) == "4") {
      $noDokNew = $noLast;
    } else {
      $noDokNew = "0001";
    }
  } else {
    $noDokNew = "0001";
  }

  $data['NoDokumen'] = $no . $thn  . $bulan . $noDokNew;
  echo json_encode($data);

}

function getDetailListMenu(){
  //data detail
  $mylib = new globallib();
  $tipe = $this->uri->segment(4);
  if($tipe=="BFM"){
    $thn = $this->uri->segment(7);
    $bln = $this->uri->segment(8);
    $no = $this->uri->segment(9);
    $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
    $booking = "T";
  }else{
    $nodok = $this->uri->segment(4);
    $booking = "Y";
  }


  $data['nodokumen'] = $nodok;
  $data['booking'] = $booking;

  $data['detailMenu'] = $this->globalmodel->getQuery(" * from trans_reservasi_detail where NoDokumen='".$nodok."'");
  $this->load->view('bss/reservasi/detail_menu_list', $data);
}

public function ajax_cancel_reservasi($id) {
  $nodok = str_replace("-", "/", $id);
  $data = $this->reservasi_model->getDataReservasiBeo($nodok);
  echo json_encode($data);
}

function hapus_trans_detail(){

  $tipe = $this->uri->segment(4);
  if($tipe=="BFM"){
    $thn = $this->uri->segment(7);
    $bln = $this->uri->segment(8);
    $no = $this->uri->segment(9);
    $pcode = $this->uri->segment(10);
    $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
    $booking = "T";
  }else{
    $nodok = $this->uri->segment(4);
    $pcode = $this->uri->segment(5);
    $booking = "Y";
  }

  $where = array("NoDokumen" => $nodok, 'PCode'=>$pcode);
  $this->db->delete("trans_reservasi_detail",$where);
  echo json_encode(array("status" => TRUE));
}

function edit_reservasi($id) {
  /*$thn = $this->uri->segment(7);
  $bln = $this->uri->segment(8);
  $no = $this->uri->segment(9);
  $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;*/

  //echo $nodok;

  $getDataReserv = $this->globalmodel->getQuery("
  *,a.`Nationality` AS asal_negara,b.Alamat,b.Telepon , a.`Contact` AS Contacts, a.`Phone` AS Phones
  from
  trans_reservasi a
  left join
  tourtravel b
  on
  a.KdTravel=b.KdTravel
  where
  1
  and id = '" . $id . "'");

  $no = "BFM/BEO/SM/";

  $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
  //$tglApp = $getTgl[0]['TglTrans'];
  $tglApp = $getDataReserv[0]['ReservasiDate'];
  $thn = substr($tglApp, 0, 4);
  $months = substr($tglApp, 5, 2);
  $bln = $this->dateRomawi(substr($tglApp, 5, 2));

  $queries = " NoDokumen from trans_reservasi WHERE YEAR(TglDokumen)='$thn' AND MONTH(TglDokumen)='$months' AND SUBSTR(NoDokumen, 1,3)<>'BOOKING' order by trans_reservasi.NoDokumen desc limit 1";
  //echo $queries;die;
  $getNoDokumen = $this->globalmodel->getQuery($queries);

  if (!empty($getNoDokumen)) {
    $nodokumen = $getNoDokumen[0]['NoDokumen'];
    $noLast = (substr($nodokumen, -4) *1) +1;
    if (strlen($noLast) == "1") {
      $noDokNew = "0000" . $noLast;
    } elseif (strlen($noLast) == "2") {
      $noDokNew = "000" . $noLast;
    } elseif (strlen($noLast) == "3") {
      $noDokNew = "00" . $noLast;
    } elseif (strlen($noLast) == "4") {
      $noDokNew = "0" . $noLast;
    } elseif (strlen($noLast) == "5") {
      $noDokNew = $noLast;
    } else {
      $noDokNew = "00001";
    }
  } else {
    $noDokNew = "00001";
  }



  $data['id'] = $getDataReserv[0]['id'];
  if(substr($getDataReserv[0]['NoDokumen'],0,3)=="BFM"){
    $data['NoDokumen'] = $getDataReserv[0]['NoDokumen'];
    $data['tampil_no_beo']="T";
  }else{
    $data['NoDokumen'] = $getDataReserv[0]['NoDokumen'];
    $data['NoDokumen_beo'] = $no . $thn . "/" . $bln . "/" . $noDokNew;
    $data['tampil_no_beo']="Y";
  }

  $data['booking']='T';

  $data['v_KdTravel'] = $getDataReserv[0]['KdTravel'];
  $data['v_nama_travel'] = $getDataReserv[0]['GroupName'];
  $data['v_group_code'] = $getDataReserv[0]['GroupCode'];
  $data['v_Company'] = $getDataReserv[0]['Nama'];
  $data['v_Alamat'] = $getDataReserv[0]['Alamat'];
  $data['v_Nationality'] = $getDataReserv[0]['asal_negara'];
  //echo $data['v_Alamat'].$data['v_Nationality'];die;
  $data['v_Telepon'] = $getDataReserv[0]['Telepon'];
  $data['v_adultParticipants'] = $getDataReserv[0]['adultParticipants'];
  $data['v_childParticipants'] = $getDataReserv[0]['childParticipants'];
  $data['v_kidsParticipants'] = $getDataReserv[0]['kidsParticipants'];
  $data['v_Jam_Kedatangan'] = $getDataReserv[0]['Jam_Kedatangan'];
  $data['v_Event1'] = $getDataReserv[0]['Event1'];
  $data['v_Event2'] = $getDataReserv[0]['Event2'];
  $data['v_Event3'] = $getDataReserv[0]['Event3'];
  $data['v_Event4'] = $getDataReserv[0]['Event4'];
  $data['v_Event5'] = $getDataReserv[0]['Event5'];
  $data['v_Contact'] = $getDataReserv[0]['Contacts'];
  $data['v_Phone'] = $getDataReserv[0]['Phones'];
  $data['v_Sales_In_Charge'] = $getDataReserv[0]['Sales_In_Charge'];
  $data['v_place_herborist'] = $getDataReserv[0]['place_herborist'];
  $data['v_place_the_luwus'] = $getDataReserv[0]['place_the_luwus'];
  $data['v_place_the_luwus_bawah'] = $getDataReserv[0]['place_the_luwus_bawah'];
  $data['v_place_black_eye_coffee'] = $getDataReserv[0]['place_black_eye_coffee'];
  $data['v_place_the_rice_view'] = $getDataReserv[0]['place_the_rice_view'];
  $data['v_place_chappel'] = $getDataReserv[0]['place_chappel'];

  $data['v_place_rice_field_deck'] = $getDataReserv[0]['place_rice_field_deck'];
  $data['v_place_garden_amphi_theater'] = $getDataReserv[0]['place_garden_amphi_theater'];
  $data['v_place_black_eye_sunset_road'] = $getDataReserv[0]['place_black_eye_sunset_road'];
  $data['v_place_bale_agung'] = $getDataReserv[0]['place_bale_agung'];
  $data['v_place_deck_view'] = $getDataReserv[0]['place_deck_view'];

  $data['v_func_type_wedding_party'] = $getDataReserv[0]['func_type_wedding_party'];
  $data['v_func_type_coffee_tea_break'] = $getDataReserv[0]['func_type_coffee_tea_break'];
  $data['v_func_type_cocktail_party'] = $getDataReserv[0]['func_type_cocktail_party'];
  $data['v_func_type_breakfast'] = $getDataReserv[0]['func_type_breakfast'];
  $data['v_func_type_birthday_party'] = $getDataReserv[0]['func_type_birthday_party'];
  $data['v_func_type_training'] = $getDataReserv[0]['func_type_training'];
  $data['v_func_type_lunch'] = $getDataReserv[0]['func_type_lunch'];
  $data['v_func_type_meeting'] = $getDataReserv[0]['func_type_meeting'];
  $data['v_func_type_dinner'] = $getDataReserv[0]['func_type_dinner'];
  $data['v_func_type_other'] = $getDataReserv[0]['func_type_other'];
  $data['v_func_type_other_content'] = $getDataReserv[0]['func_type_other_content'];
  $data['v_banquet_venue'] = $getDataReserv[0]['banquet_venue'];
  $data['v_banquet_event'] = $getDataReserv[0]['banquet_event'];
  $data['v_banquet_table_set_up'] = $getDataReserv[0]['banquet_table_set_up'];
  $data['v_banquet_persons'] = $getDataReserv[0]['banquet_persons'];
  $data['v_banquet_registration'] = $getDataReserv[0]['banquet_registration'];
  $data['v_banquet_coffee_break_1'] = $getDataReserv[0]['banquet_coffee_break_1'];
  $data['v_banquet_lunch'] = $getDataReserv[0]['banquet_lunch'];
  $data['v_banquet_coffee_break_2'] = $getDataReserv[0]['banquet_coffee_break_2'];
  $data['v_banquet_dinner'] = $getDataReserv[0]['banquet_dinner'];
  $data['v_equipment_flipchart'] = $getDataReserv[0]['equipment_flipchart'];
  $data['v_equipment_mineral_water'] = $getDataReserv[0]['equipment_mineral_water'];
  $data['v_equipment_lcd_projector'] = $getDataReserv[0]['equipment_lcd_projector'];
  $data['v_equipment_notepad_pencil'] = $getDataReserv[0]['equipment_notepad_pencil'];
  $data['v_equipment_mint'] = $getDataReserv[0]['equipment_mint'];
  $data['v_equipment_standard_set_up'] = $getDataReserv[0]['equipment_standard_set_up'];
  $data['v_equipment_screen'] = $getDataReserv[0]['equipment_screen'];
  $data['v_equipment_sound_system'] = $getDataReserv[0]['equipment_sound_system'];
  $data['v_billing_instruction'] = $getDataReserv[0]['billing_instruction'];
  $data['v_desc_front_office'] = $getDataReserv[0]['desc_front_office'];
  $data['v_desc_f_b_kitchen'] = $getDataReserv[0]['desc_f_b_kitchen'];
  $data['v_desc_black_aye'] = $getDataReserv[0]['desc_black_aye'];
  $data['v_desc_oemah_herborist'] = $getDataReserv[0]['desc_oemah_herborist'];
  $data['v_desc_engineering_edp'] = $getDataReserv[0]['desc_engineering_edp'];
  $data['v_desc_sport_recreation'] = $getDataReserv[0]['desc_sport_recreation'];
  $data['v_desc_rates'] = $getDataReserv[0]['desc_rates'];
  $data['v_desc_housekeeping'] = $getDataReserv[0]['desc_housekeeping'];
  $data['v_desc_accounting'] = $getDataReserv[0]['desc_accounting'];
  $data['v_desc_security'] = $getDataReserv[0]['desc_security'];
  $data['v_desc_graphis'] = $getDataReserv[0]['desc_graphis'];
  $data['v_desc_biaya_tambahan'] = $getDataReserv[0]['desc_biaya_tambahan'];
  $data['v_reservasi_date'] = $getDataReserv[0]['ReservasiDate'];

  $data['v_status'] = $getDataReserv[0]['status'];

  $data['v_dp_date'] = $getDataReserv[0]['DpDate'];
  $data['v_biaya_tambahan'] = $getDataReserv[0]['biaya_tambahan'];
  $data['v_VoucherCode'] = $getDataReserv[0]['VoucherCode'];
  $data['v_dp'] = $getDataReserv[0]['dp'];
  $data['v_Total'] = $getDataReserv[0]['Total'];
  $data['v_Remarks_Payment'] = $getDataReserv[0]['Remarks_Payment'];
  $data['v_edit_request'] = $getDataReserv[0]['edit_request'];

  $AddUser = $getDataReserv[0]['AddUser'];
  // $getName = $this->globalmodel->getQuery(" employee.employee_name,user.UserLevel from employee join user on employee.username=user.UserName where employee.username='".$AddUser."' ");
  // $data['v_add_user'] = $getName[0]['employee_name'];
  // $data['v_level_user'] = $getName[0]['UserLevel'];
  // $data['v_add_userid'] = $AddUser;
  $data['sales'] = $this->reservasi_model->getSales();
  $data['aktivitas'] = $this->reservasi_model->getAktivitas();
  $user = $this->session->userdata('username');
  $data['adm_sales'] = $this->reservasi_model->getAdmSales($user);

  $data['deposit'] = $this->reservasi_model->getDeposit();
  $data['voucher_beo'] = $this->reservasi_model->getVoucher();

  $nodok = $data['NoDokumen'];

  //data detail
  $data['detailMenu'] = $this->globalmodel->getQuery(" * from trans_reservasi_detail where NoDokumen='".$nodok."'");

  if(empty($data['adm_sales'])){
    $data['adm_sales_nama'] = "";
    $data['adm_sales_jabatan'] = "";
  }else{
    $data['adm_sales_nama'] = $data['adm_sales'][0]['Nama'];
    $data['adm_sales_jabatan'] = $data['adm_sales'][0]['Jabatan'];
  }
  $data['group_cc'] = $this->globalmodel->getQuery(" group_cc_id,group_cc_name from group_cc");
  $data['group_cc_value'] = $this->globalmodel->getQuery(" * from trans_reservasi_cc where NoDokumen='".$nodok."'");
  $data['type'] = "edit";
  $data['action'] = "save_data_edit";
  $this->load->view('bss/reservasi/tampil', $data);
  //print_r($getgetDataReserv);
}

function add_reservasi() {

  $mylib = new globallib();
  $sign = $mylib->getAllowList("add");
  $user = $this->session->userdata('username');
  if ($sign == "Y") {
    $no = "BFM/BEO/SM/";
    $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
    $tglApp = $getTgl[0]['TglTrans'];
    $thn = substr($tglApp, 0, 4);
    $bln = $this->dateRomawi(substr($tglApp, 5, 2));

    $getNoDokumen = $this->globalmodel->getQuery(" NoDokumen from trans_reservasi WHERE YEAR(TglDokumen)='".date('Y')."' AND MONTH(TglDokumen)='".date('m')."' AND SUBSTR(NoDokumen, 1,3)='BFM' order by trans_reservasi.NoDokumen desc limit 1");
    if (!empty($getNoDokumen)) {
      $nodokumen = $getNoDokumen[0]['NoDokumen'];
      $noLast = (substr($nodokumen, -4) *1) +1;
      if (strlen($noLast) == "1") {
        $noDokNew = "0000" . $noLast;
      } elseif (strlen($noLast) == "2") {
        $noDokNew = "000" . $noLast;
      } elseif (strlen($noLast) == "3") {
        $noDokNew = "00" . $noLast;
      } elseif (strlen($noLast) == "4") {
        $noDokNew = "0" . $noLast;
      } elseif (strlen($noLast) == "5") {
        $noDokNew = $noLast;
      } else {
        $noDokNew = "00001";
      }
    } else {
      $noDokNew = "00001";
    }

    $AddUser = $user;
    // $getName = $this->globalmodel->getQuery(" employee.employee_name,user.UserLevel from employee join user on employee.username=user.UserName where employee.username='".$AddUser."' ");
    // $data['v_add_user'] = $getName[0]['employee_name'];
    // $data['v_level_user'] = $getName[0]['UserLevel'];
    // $data['v_add_userid'] = $AddUser;
    $data['sales'] = $this->reservasi_model->getSales();
    $data['aktivitas'] = $this->reservasi_model->getAktivitas();
    $data['deposit'] = $this->reservasi_model->getDeposit();
    $data['voucher_beo'] = $this->reservasi_model->getVoucher();

    $data['adm_sales'] = $this->reservasi_model->getAdmSales($user);
    if(empty($data['adm_sales'])){
      $data['adm_sales_nama'] = "";
      $data['adm_sales_jabatan'] = "";
    }else{
      $data['adm_sales_nama'] = $data['adm_sales'][0]['Nama'];
      $data['adm_sales_jabatan'] = $data['adm_sales'][0]['Jabatan'];
    }

    $data['booking']='T';

    $data['group_cc'] = $this->globalmodel->getQuery(" group_cc_id,group_cc_name from group_cc");
    $data['type'] = "add";
    $data['action'] = "save_data";
    $data['NoDokumen'] = $no . $thn . "/" . $bln . "/" . $noDokNew;
    $this->load->view('bss/reservasi/tampil', $data);
  } else {
    $this->load->view('denied');
  }
}

function booking() {
  $mylib = new globallib();
  $sign = $mylib->getAllowList("add");
  $user = $this->session->userdata('username');
  if ($sign == "Y") {

    $tgl=date('Y-m-d');
    $bulan = substr($tgl, 5, 2);
    $tahun = substr($tgl, 2, 2);

    $kode = "BOOKING";
    $no = $this->get_no_counter_booking( $kode ,"trans_reservasi", "NoDokumen",$tahun,$bulan);

    $AddUser = $user;
    $data['sales']        = $this->reservasi_model->getSales();
    $data['aktivitas']    = $this->reservasi_model->getAktivitas();
    $data['deposit']      = $this->reservasi_model->getDeposit();
    $data['voucher_beo']  = $this->reservasi_model->getVoucher();

    $data['adm_sales'] = $this->reservasi_model->getAdmSales($user);
    if(empty($data['adm_sales'])){
      $data['adm_sales_nama']     = "";
      $data['adm_sales_jabatan']  = "";
    }else{
      $data['adm_sales_nama'] = $data['adm_sales'][0]['Nama'];
      $data['adm_sales_jabatan'] = $data['adm_sales'][0]['Jabatan'];
    }

    $data['booking']='Y';
    $data['group_cc'] = $this->globalmodel->getQuery(" group_cc_id,group_cc_name from group_cc");
    $data['type'] = "add";
    $data['action'] = "save_data";
    $data['NoDokumen'] = $no;
    $this->load->view('bss/reservasi/tampil', $data);
  } else {
    $this->load->view('denied');
  }
}

function save_detail_menu() {
  //echo "<pre>";print_r($_POST);echo "</pre>";die;
  $nodokumen = $this->input->post('nodokumen');
  $v_pcode1 = $this->input->post('pcode');
  $v_namabarang1 = $this->input->post('v_namabarang');
  $v_qty1 = $this->input->post('v_hidden_qty');
  $v_harga1 = $this->input->post('v_hidden_harga');
  $v_diskon1 = $this->input->post('v_hidden_diskon');

  for ($x = 0; $x < count($v_pcode1); $x++) {
    $data = array(
      "NoDokumen" => $nodokumen,
      "PCode" => $v_pcode1[$x],
      "NamaBarang" => $v_namabarang1[$x],
      "Qty" => $v_qty1[$x],
      "Harga" => $v_harga1[$x],
      "Diskon" => $v_diskon1[$x]
    );

    if ($v_namabarang1[$x] != "") {
      $this->globalmodel->queryInsert("trans_reservasi_detail", $data);
    }
  }

  $v_total_tambah = $this->input->post('v_hidden_grand_total');
  $v_total_lama = $this->input->post('gtotal');
  $v_total_akhir = ($v_total_tambah+$v_total_lama);

  $data = array("Total"=>$v_total_akhir);
  $where = array("NoDokumen"=>$nodokumen);
  $this->globalmodel->queryUpdate("trans_reservasi",$data,$where);

  redirect("pop/pop_up_detail_menu/index/".$nodokumen."");
  //echo "<script>self.close();</script>";
}

function save_data() {
  //echo "<pre>";print_r($_POST);echo "</pre>";die;
  $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
  $tglApp = $getTgl[0]['TglTrans'];
  $User = $this->session->userdata('username');
  $mylib = new globallib();


  $NoDokumen = $this->input->post('NoDokumen');
  $KdTravel = $this->input->post('KdTravel');
  $date_reservasi = $this->input->post('v_date_reservasi');
  $adultParticipants = $this->input->post('adultParticipants');
  $childParticipants = $this->input->post('childParticipants');
  $kidsParticipants = $this->input->post('kidsParticipants');
  $Jam_Kedatangan = $this->input->post('jam_kedatangan');

  $Event1 = $this->input->post('Event1');
  $Event2 = $this->input->post('Event2');
  $Event3 = $this->input->post('Event3');
  $Event4 = $this->input->post('Event4');
  $Event5 = $this->input->post('Event5');
  $GroupCode= $this->input->post('group_code');
  $GroupName = $this->input->post('nama_travel');
  $Nationality = $this->input->post('nationality');
  $Contact = $this->input->post('Contact');
  $Phone = $this->input->post('Phone');
  $Sales_In_Charge = $this->input->post('Sales_In_Charge');

  $place_herborist = $this->input->post('place_herborist');
  $place_the_luwus = $this->input->post('place_the_luwus');
  $place_the_luwus_bawah = $this->input->post('place_the_luwus_bawah');
  $place_black_eye_coffee = $this->input->post('place_black_eye_coffee');
  $place_the_rice_view = $this->input->post('place_the_rice_view');
  $place_chappel = $this->input->post('place_chappel');

  $place_rice_field_deck = $this->input->post('place_rice_field_deck');
  $place_garden_amphi_theater = $this->input->post('place_garden_amphi_theater');
  $place_black_eye_sunset_road= $this->input->post('place_black_eye_sunset_road');
  $place_bale_agung = $this->input->post('place_bale_agung');
  $place_deck_view = $this->input->post('place_deck_view');

  $func_type_wedding_party = $this->input->post('func_type_wedding_party');
  $func_type_coffee_tea_break = $this->input->post('func_type_coffee_tea_break');
  $func_type_cocktail_party = $this->input->post('func_type_cocktail_party');
  $func_type_breakfast = $this->input->post('func_type_breakfast');
  $func_type_birthday_party = $this->input->post('func_type_birthday_party');
  $func_type_training = $this->input->post('func_type_training');
  $func_type_lunch = $this->input->post('func_type_lunch');
  $func_type_meeting = $this->input->post('func_type_meeting');
  $func_type_dinner = $this->input->post('func_type_dinner');
  $func_type_other = $this->input->post('func_type_other');
  $func_type_other_content = $this->input->post('func_type_other_content');
  $banquet_venue = $this->input->post('banquet_venue');
  $banquet_event = $this->input->post('banquet_event');
  $banquet_table_set_up = $this->input->post('banquet_table_set_up');
  $banquet_persons = $this->input->post('banquet_persons');
  $banquet_registration = $this->input->post('banquet_registration');
  $banquet_coffee_break_1 = $this->input->post('banquet_coffee_break_1');
  $banquet_lunch = $this->input->post('banquet_lunch');
  $banquet_coffee_break_2 = $this->input->post('banquet_coffee_break_2');
  $banquet_dinner = $this->input->post('banquet_dinner');
  $equipment_flipchart = $this->input->post('equipment_flipchart');
  $equipment_mineral_water = $this->input->post('equipment_mineral_water');
  $equipment_lcd_projector = $this->input->post('equipment_lcd_projector');
  $equipment_notepad_pencil = $this->input->post('equipment_notepad_pencil');
  $equipment_mint = $this->input->post('equipment_mint');
  $equipment_standard_set_up = $this->input->post('equipment_standard_set_up');
  $equipment_screen = $this->input->post('equipment_screen');
  $equipment_sound_system = $this->input->post('equipment_sound_system');
  $billing_instruction = $this->input->post('billing_instruction');
  $desc_front_office = $this->input->post('desc_front_office');
  $desc_f_b_kitchen = $this->input->post('desc_f_b_kitchen');
  $desc_engineering_edp = $this->input->post('desc_engineering_edp');
  $desc_sport_recreation = $this->input->post('desc_sport_recreation');
  $desc_rates = $this->input->post('desc_rates');
  $desc_housekeeping = $this->input->post('desc_housekeeping');
  $desc_accounting = $this->input->post('desc_accounting');
  $desc_graphis = $this->input->post('desc_graphis');
  $desc_biaya_tambahan = $this->input->post('desc_biaya_tambahan');
  $VoucherCode= $this->input->post('voucher');
  $biaya_tambahan = $this->input->post('hidden_biaya_tambahan');
  $dp = $this->input->post('hidden_dp');
  $Total = $this->input->post('hidden_total');
  $Remarks_Payment = $this->input->post('Remarks_Payment');
  $DateReservasi = $this->input->post('v_date_reservasi');
  $Date = date("Y-m-d", strtotime($DateReservasi));
  $ReservasiDate = date($Date . " H:i:s");
  $AddDate = date($tglApp . " H:i:s");
  $AddUser = $User;
  $desc_black_aye = $this->input->post('desc_black_aye');
  $desc_oemah_herborist = $this->input->post('desc_oemah_herborist');
  $desc_security = $this->input->post('desc_security');
  $jenis = $this->input->post('jenis');

  $data = array(
    "NoDokumen" => $NoDokumen,
    "TglDokumen"=>$mylib->ubah_tanggal($date_reservasi),
    "KdTravel" => $KdTravel,
    "adultParticipants" => $adultParticipants,
    "childParticipants" => $childParticipants,
    "kidsParticipants" => $kidsParticipants,
    "Event1" => $Event1,
    "Event2" => $Event2,
    "Event3" => $Event3,
    "Event4" => $Event4,
    "Event5" => $Event5,
    "GroupCode"=> $GroupCode,
    "GroupName" => $GroupName,
    "Nationality" => $Nationality,
    "Jam_Kedatangan"=>$Jam_Kedatangan,
    "Contact" => $Contact,
    "Phone" => $Phone,
    "Sales_In_Charge" => $Sales_In_Charge,
    "place_herborist" => $place_herborist,
    "place_the_luwus" => $place_the_luwus,
    "place_the_luwus_bawah" => $place_the_luwus_bawah,
    "place_black_eye_coffee" => $place_black_eye_coffee,
    "place_the_rice_view" => $place_the_rice_view,
    "place_chappel" => $place_chappel,

    "place_rice_field_deck"=>$place_rice_field_deck,
    "place_garden_amphi_theater"=>$place_garden_amphi_theater,
    "place_black_eye_sunset_road"=>$place_black_eye_sunset_road,
    "place_bale_agung"=>$place_bale_agung,
    "place_deck_view"=>$place_deck_view,

    "func_type_wedding_party" => $func_type_wedding_party,
    "func_type_coffee_tea_break" => $func_type_coffee_tea_break,
    "func_type_cocktail_party" => $func_type_cocktail_party,
    "func_type_breakfast" => $func_type_breakfast,
    "func_type_birthday_party" => $func_type_birthday_party,
    "func_type_training" => $func_type_training,
    "func_type_lunch" => $func_type_lunch,
    "func_type_meeting" => $func_type_meeting,
    "func_type_dinner" => $func_type_dinner,
    "func_type_other" => $func_type_other,
    "func_type_other_content" => $func_type_other_content,
    "banquet_venue" => $banquet_venue,
    "banquet_event" => $banquet_event,
    "banquet_table_set_up" => $banquet_table_set_up,
    "banquet_persons" => $banquet_persons,
    "banquet_registration" => $banquet_registration,
    "banquet_coffee_break_1" => $banquet_coffee_break_1,
    "banquet_lunch" => $banquet_lunch,
    "banquet_coffee_break_2" => $banquet_coffee_break_2,
    "banquet_dinner" => $banquet_dinner,
    "equipment_flipchart" => $equipment_flipchart,
    "equipment_mineral_water" => $equipment_mineral_water,
    "equipment_lcd_projector" => $equipment_lcd_projector,
    "equipment_notepad_pencil" => $equipment_notepad_pencil,
    "equipment_mint" => $equipment_mint,
    "equipment_standard_set_up" => $equipment_standard_set_up,
    "equipment_screen" => $equipment_screen,
    "equipment_sound_system" => $equipment_sound_system,
    "billing_instruction" => $billing_instruction,
    "desc_front_office" => $desc_front_office,
    "desc_f_b_kitchen" => $desc_f_b_kitchen,
    "desc_black_aye" => $desc_black_aye,
    "desc_oemah_herborist" => $desc_oemah_herborist,
    "desc_engineering_edp" => $desc_engineering_edp,
    "desc_sport_recreation" => $desc_sport_recreation,
    "desc_rates" => $desc_rates,
    "desc_housekeeping" => $desc_housekeeping,
    "desc_accounting" => $desc_accounting,
    "desc_security" => $desc_security,
    "desc_graphis" => $desc_graphis,
    "desc_biaya_tambahan"=>$desc_biaya_tambahan,
    "ReservasiDate" => $ReservasiDate,
    "biaya_tambahan" => $biaya_tambahan,
    "VoucherCode" => $VoucherCode,
    "dp" => $dp,
    "Total" => $Total+$biaya_tambahan,
    "Remarks_Payment" => $Remarks_Payment,
    "AddDate" => $AddDate,
    "AddUser" => $AddUser
  );
  //echo "<pre>";print_r($data);echo "</pre>";die;
  $this->globalmodel->queryInsert("trans_reservasi", $data);

  $group_cc = $this->input->post('group_cc');
  for ($i = 0; $i < count($group_cc); $i++) {
    $data_group_cc = array(
      "NoDokumen" => $NoDokumen,
      "group_cc_id" => $group_cc[$i]
    );
    $this->globalmodel->queryInsert("trans_reservasi_cc", $data_group_cc);
  }


  //save detail Reservasi
  $v_pcode1 = $this->input->post('pcode');
  $v_namabarang1 = $this->input->post('v_namabarang');
  $v_qty1 = $this->input->post('v_hidden_qty');
  $v_harga1 = $this->input->post('v_hidden_harga');
  $v_diskon1 = $this->input->post('v_hidden_diskon');

  for ($x = 0; $x < count($v_pcode1); $x++) {
    $data = array(
      "NoDokumen" => $NoDokumen,
      "PCode" => $v_pcode1[$x],
      "NamaBarang" => $v_namabarang1[$x],
      "Qty" => $v_qty1[$x],
      "Harga" => $v_harga1[$x],
      "Diskon" => $v_diskon1[$x]
    );

    if ($v_namabarang1[$x] != "") {
      $this->globalmodel->queryInsert("trans_reservasi_detail", $data);
    }
  }

  $v_total_tambah = $this->input->post('v_hidden_grand_total');
  $v_total_lama = $this->input->post('gtotal');
  $v_total_akhir = ($v_total_tambah+$v_total_lama);

  $data = array("Total"=>$v_total_akhir);
  $where = array("NoDokumen"=>$NoDokumen);
  //$this->globalmodel->queryUpdate("trans_reservasi",$data,$where);
  //----------------------------------------------------------------------------------

  $getNoDokumen = $this->globalmodel->getQuery(" id from trans_reservasi WHERE 1 order by trans_reservasi.id desc limit 1");
  $tipe = substr($NoDokumen, 0, 3);
  if($tipe=="BFM"){
    redirect('/bss/reservasi/edit_reservasi/'.$getNoDokumen[0]['id'].'');
  }else{
    redirect('/bss/reservasi/');
  }

}



function save_data_edit() {
  //echo "<pre>";print_r($_POST);echo "</pre>";die;
  $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
  $tglApp = $getTgl[0]['TglTrans'];
  $User = $this->session->userdata('username');

  $mylib = new globallib();

  $id= $this->input->post('id');

  $NoDokumen = $this->input->post('NoDokumen');

  $date_reservasi = $this->input->post('v_date_reservasi');
  //$v_approve itu artinya kirim ke head
  $v_approve = $this->input->post('v_approve');
  $v_approve_head = $this->input->post('v_approve_head');
  $v_reject_head = $this->input->post('v_reject_head');
  $ket_reject = $this->input->post('ket_reject');
  $KdTravel = $this->input->post('KdTravel');
  $date_reservasi = $this->input->post('v_date_reservasi');
  $adultParticipants = $this->input->post('adultParticipants');
  $childParticipants = $this->input->post('childParticipants');
  $kidsParticipants = $this->input->post('kidsParticipants');
  $Jam_Kedatangan = $this->input->post('jam_kedatangan');

  $Event1 = $this->input->post('Event1');
  $Event2 = $this->input->post('Event2');
  $Event3 = $this->input->post('Event3');
  $Event4 = $this->input->post('Event4');
  $Event5 = $this->input->post('Event5');
  $GroupCode= $this->input->post('group_code');
  $GroupName = $this->input->post('nama_travel');
  $Nationality = $this->input->post('nationality');
  $Contact = $this->input->post('Contact');
  $Phone = $this->input->post('Phone');
  $Sales_In_Charge = $this->input->post('Sales_In_Charge');

  $place_herborist = $this->input->post('place_herborist');
  $place_the_luwus = $this->input->post('place_the_luwus');
  $place_the_luwus_bawah = $this->input->post('place_the_luwus_bawah');
  $place_black_eye_coffee = $this->input->post('place_black_eye_coffee');
  $place_the_rice_view = $this->input->post('place_the_rice_view');
  $place_chappel = $this->input->post('place_chappel');

  $place_rice_field_deck = $this->input->post('place_rice_field_deck');
  $place_garden_amphi_theater = $this->input->post('place_garden_amphi_theater');
  $place_black_eye_sunset_road = $this->input->post('place_black_eye_sunset_road');
  $place_bale_agung = $this->input->post('place_bale_agung');
  $place_deck_view = $this->input->post('place_deck_view');

  $func_type_wedding_party = $this->input->post('func_type_wedding_party');
  $func_type_coffee_tea_break = $this->input->post('func_type_coffee_tea_break');
  $func_type_cocktail_party = $this->input->post('func_type_cocktail_party');
  $func_type_breakfast = $this->input->post('func_type_breakfast');
  $func_type_birthday_party = $this->input->post('func_type_birthday_party');
  $func_type_training = $this->input->post('func_type_training');
  $func_type_lunch = $this->input->post('func_type_lunch');
  $func_type_meeting = $this->input->post('func_type_meeting');
  $func_type_dinner = $this->input->post('func_type_dinner');
  $func_type_other = $this->input->post('func_type_other');
  $func_type_other_content = $this->input->post('func_type_other_content');
  $banquet_venue = $this->input->post('banquet_venue');
  $banquet_event = $this->input->post('banquet_event');
  $banquet_table_set_up = $this->input->post('banquet_table_set_up');
  $banquet_persons = $this->input->post('banquet_persons');
  $banquet_registration = $this->input->post('banquet_registration');
  $banquet_coffee_break_1 = $this->input->post('banquet_coffee_break_1');
  $banquet_lunch = $this->input->post('banquet_lunch');
  $banquet_coffee_break_2 = $this->input->post('banquet_coffee_break_2');
  $banquet_dinner = $this->input->post('banquet_dinner');
  $equipment_flipchart = $this->input->post('equipment_flipchart');
  $equipment_mineral_water = $this->input->post('equipment_mineral_water');
  $equipment_lcd_projector = $this->input->post('equipment_lcd_projector');
  $equipment_notepad_pencil = $this->input->post('equipment_notepad_pencil');
  $equipment_mint = $this->input->post('equipment_mint');
  $equipment_standard_set_up = $this->input->post('equipment_standard_set_up');
  $equipment_screen = $this->input->post('equipment_screen');
  $equipment_sound_system = $this->input->post('equipment_sound_system');
  $billing_instruction = $this->input->post('billing_instruction');
  $desc_front_office = $this->input->post('desc_front_office');
  $desc_f_b_kitchen = $this->input->post('desc_f_b_kitchen');
  $desc_engineering_edp = $this->input->post('desc_engineering_edp');
  $desc_sport_recreation = $this->input->post('desc_sport_recreation');
  $desc_rates = $this->input->post('desc_rates');
  $desc_housekeeping = $this->input->post('desc_housekeeping');
  $desc_accounting = $this->input->post('desc_accounting');
  $desc_graphis = $this->input->post('desc_graphis');
  $desc_biaya_tambahan = $this->input->post('desc_biaya_tambahan');
  $VoucherCode= $this->input->post('voucher');
  $biaya_tambahan = $this->input->post('hidden_biaya_tambahan');
  $dp = $this->input->post('hidden_dp');
  $Total = $this->input->post('hidden_total');
  $Remarks_Payment = $this->input->post('Remarks_Payment');
  $DpDate = $this->input->post('v_date_dp');
  $DpDateFormat = date("Y-m-d", strtotime($DpDate));
  $DateReservasi = $this->input->post('v_date_reservasi');
  $Date = date("Y-m-d", strtotime($DateReservasi));
  $ReservasiDate = date($Date . " H:i:s");
  $AddDate = date($tglApp . " H:i:s");
  $AddUser = $User;
  $desc_black_aye = $this->input->post('desc_black_aye');
  $desc_oemah_herborist = $this->input->post('desc_oemah_herborist');
  $desc_security = $this->input->post('desc_security');

  $edit_request = $this->input->post('v_edit_request');
  $ket_edit_request = $this->input->post('ket_edit_request');

  $pilihdp = $this->input->post('pilihdp');
  $uang_muka = explode('#',$pilihdp);
  $id_uang_muka_beo = $uang_muka[0];

  $pilihvoucher = $this->input->post('pilihvoucher');
  $voucher_beo = explode('#',$pilihvoucher);
  $id_voucher_beo = $voucher_beo[0];

  $jenis = $this->input->post('jenis');

  $data = array(
    "NoDokumen" => $NoDokumen,
    "TglDokumen"=>$mylib->ubah_tanggal($date_reservasi),
    "KdTravel" => $KdTravel,
    "adultParticipants" => $adultParticipants,
    "childParticipants" => $childParticipants,
    "kidsParticipants" => $kidsParticipants,
    "Jam_Kedatangan"=>$Jam_Kedatangan,
    "Event1" => $Event1,
    "Event2" => $Event2,
    "Event3" => $Event3,
    "Event4" => $Event4,
    "Event5" => $Event5,
    "GroupCode"=> $GroupCode,
    "GroupName" => $GroupName,
    "Nationality" => $Nationality,
    "Contact" => $Contact,
    "Phone" => $Phone,
    "Sales_In_Charge" => $Sales_In_Charge,

    "place_herborist" => $place_herborist,
    "place_the_luwus" => $place_the_luwus,
    "place_the_luwus_bawah" => $place_the_luwus_bawah,
    "place_black_eye_coffee" => $place_black_eye_coffee,
    "place_the_rice_view" => $place_the_rice_view,
    "place_chappel" => $place_chappel,

    "place_rice_field_deck"=>$place_rice_field_deck,
    "place_garden_amphi_theater"=>$place_garden_amphi_theater,
    "place_black_eye_sunset_road"=>$place_black_eye_sunset_road,
    "place_bale_agung"=>$place_bale_agung,
    "place_deck_view"=>$place_deck_view,


    "func_type_wedding_party" => $func_type_wedding_party,
    "func_type_coffee_tea_break" => $func_type_coffee_tea_break,
    "func_type_cocktail_party" => $func_type_cocktail_party,
    "func_type_breakfast" => $func_type_breakfast,
    "func_type_birthday_party" => $func_type_birthday_party,
    "func_type_training" => $func_type_training,
    "func_type_lunch" => $func_type_lunch,
    "func_type_meeting" => $func_type_meeting,
    "func_type_dinner" => $func_type_dinner,
    "func_type_other" => $func_type_other,
    "func_type_other_content" => $func_type_other_content,
    "banquet_venue" => $banquet_venue,
    "banquet_event" => $banquet_event,
    "banquet_table_set_up" => $banquet_table_set_up,
    "banquet_persons" => $banquet_persons,
    "banquet_registration" => $banquet_registration,
    "banquet_coffee_break_1" => $banquet_coffee_break_1,
    "banquet_lunch" => $banquet_lunch,
    "banquet_coffee_break_2" => $banquet_coffee_break_2,
    "banquet_dinner" => $banquet_dinner,
    "equipment_flipchart" => $equipment_flipchart,
    "equipment_mineral_water" => $equipment_mineral_water,
    "equipment_lcd_projector" => $equipment_lcd_projector,
    "equipment_notepad_pencil" => $equipment_notepad_pencil,
    "equipment_mint" => $equipment_mint,
    "equipment_standard_set_up" => $equipment_standard_set_up,
    "equipment_screen" => $equipment_screen,
    "equipment_sound_system" => $equipment_sound_system,
    "billing_instruction" => $billing_instruction,
    "desc_front_office" => $desc_front_office,
    "desc_f_b_kitchen" => $desc_f_b_kitchen,
    "desc_black_aye" => $desc_black_aye,
    "desc_oemah_herborist" => $desc_oemah_herborist,
    "desc_engineering_edp" => $desc_engineering_edp,
    "desc_sport_recreation" => $desc_sport_recreation,
    "desc_rates" => $desc_rates,
    "desc_housekeeping" => $desc_housekeeping,
    "desc_accounting" => $desc_accounting,
    "desc_security" => $desc_security,
    "desc_graphis" => $desc_graphis,
    "desc_biaya_tambahan"=>$desc_biaya_tambahan,
    "ReservasiDate" => $ReservasiDate,
    "biaya_tambahan" => $biaya_tambahan,
    "VoucherCode" => $VoucherCode,
    "DpDate" => $DpDateFormat,
    "dp" => $dp,
    "Total" => $Total+$biaya_tambahan,
    "Remarks_Payment" => $Remarks_Payment,
    "EditDate" => $AddDate,
    "EditUser" => $AddUser
  );

  $where = array("id" => $id);
  $this->globalmodel->queryUpdate("trans_reservasi", $data, $where);


  //save detail Reservasi
  $v_pcode1 = $this->input->post('pcode');
  $v_namabarang1 = $this->input->post('v_namabarang');
  $v_qty1 = $this->input->post('v_hidden_qty');
  $v_harga1 = $this->input->post('v_hidden_harga');
  $v_diskon1 = $this->input->post('v_hidden_diskon');

  for ($x = 0; $x < count($v_pcode1); $x++) {
    $data = array(
      "NoDokumen" => $NoDokumen,
      "PCode" => $v_pcode1[$x],
      "NamaBarang" => $v_namabarang1[$x],
      "Qty" => $v_qty1[$x],
      "Harga" => $v_harga1[$x],
      "Diskon" => $v_diskon1[$x]
    );

    if ($v_namabarang1[$x] != "") {
      $this->globalmodel->queryInsert("trans_reservasi_detail", $data);
    }
  }

  $v_total_tambah = $this->input->post('v_hidden_grand_total');
  $v_total_lama = $this->input->post('gtotal');
  $v_total_akhir = ($v_total_tambah+$v_total_lama);

  $data = array("Total"=>$v_total_akhir);
  $where = array("NoDokumen"=>$NoDokumen);
  //$this->globalmodel->queryUpdate("trans_reservasi",$data,$where);
  //----------------------------------------------------------------------------


  //dikirim ke head
  if($v_approve=="1"){
    $tipe_nodok = substr($NoDokumen, 0, 7);
    if($tipe_nodok=="BOOKING"){
      $No_Dokumen = $this->input->post('NoDokumen_beo');
      //update detail
      $this->db->update("trans_reservasi_detail",array("NoDokumen" => $No_Dokumen),array("NoDokumen" => $NoDokumen));
    }else{
      $No_Dokumen = $this->input->post('NoDokumen');
    }
    //update
    $this->db->update("trans_reservasi",array("NoDokumen" => $No_Dokumen),array("id" => $id));

    $this->globalmodel->queryDelete("trans_reservasi_cc", array("NoDokumen" => $No_Dokumen));

    $group_cc = $this->input->post('group_cc');

    for ($i = 0; $i < count($group_cc); $i++) {

      if ($group_cc[$i] != "") {
        $data_group_cc = array(
          "NoDokumen" => $No_Dokumen,
          "group_cc_id" => $group_cc[$i]
        );
        $this->globalmodel->queryInsert("trans_reservasi_cc", $data_group_cc);
      }
    }


    $travel = $this->reservasi_model->getTravelMail($KdTravel);
    $salesman = $this->reservasi_model->getSalesMail($Sales_In_Charge);
    $aktivitas1 = $this->reservasi_model->getAktivitas1Mail($Event1);
    $aktivitas2 = $this->reservasi_model->getAktivitas2Mail($Event2);
    $aktivitas3 = $this->reservasi_model->getAktivitas3Mail($Event3);
    $aktivitas4 = $this->reservasi_model->getAktivitas4Mail($Event4);
    $aktivitas5 = $this->reservasi_model->getAktivitas5Mail($Event5);

    if(!empty($salesman)){
      $nm_sales = $salesman->NamaSalesman;
    }else{
      $nm_sales = "";
    }

    if($place_herborist=="1"){
      $vanue1="Oemah Herborist, ";
    }else{
      $vanue1="";
    }

    if($place_the_luwus=="1"){
      $vanue2="The Luwus Atas, ";
    }else{
      $vanue2="";
    }

    if($place_the_luwus_bawah=="1"){
      $vanue3="The Luwus Bawah, ";
    }else{
      $vanue3="";
    }

    if($place_black_eye_coffee=="1"){
      $vanue4="Black Eye Coffee, ";
    }else{
      $vanue4="";
    }

    if($place_the_rice_view=="1"){
      $vanue5="The Rice View, ";
    }else{
      $vanue5="";
    }

    if($place_chappel=="1"){
      $vanue6="The Chamber, ";
    }else{
      $vanue6="";
    }

    if($place_rice_field_deck=="1"){
      $vanue7="The Rice Field Deck, ";
    }else{
      $vanue7="";
    }

    if($place_garden_amphi_theater=="1"){
      $vanue8="Garden Amphi Theater, ";
    }else{
      $vanue8="";
    }

    if($place_bale_agung=="1"){
      $vanue9="Bale Agung, ";
    }else{
      $vanue9="";
    }

    if($place_deck_view=="1"){
      $vanue10="The Deck View.";
    }else{
      $vanue10="";
    }

    $all_vanue = $vanue1.$vanue2.$vanue3.$vanue4.$vanue5.$vanue6.$vanue7.$vanue8.$vanue9.$vanue10;

    //untuk status 1 itu dikirim ke Head Sales
    $mylib = new globallib();

    $to = "";
    $to_name = "";

    /*$sql = "
    a.`email_address` AS email,
    a.`email_name` AS employee_name
    FROM
    `function_email` a
    WHERE a.`func_name` = 'beo' ;
    ";*/

    $sql = "  c.`NamaSupervisor` AS employee_name,
    c.`email` AS email,
    c.`UserName`,
    d.`TelegramID`
    FROM
    `trans_reservasi` a
    INNER JOIN salesman b
    ON a.`Sales_In_Charge` = b.`KdSalesman`
    INNER JOIN supervisor c
    ON b.`KdSupervisor` = c.`KdSupervisor`
    INNER JOIN `user` d
    ON d.`UserName` = c.`UserName`
    WHERE a.`NoDokumen` = '".$No_Dokumen."';
    ";
    $arrData = $this->globalmodel->getQuery($sql);


    foreach($arrData as $val)
    {
      $to .= $val["email"].";";
      $to_name .= $val["employee_name"].";";
      $ChatID = $val["TelegramID"];
    }

    /*$sql_cc = "
    f.`employee_name` AS employee_name,
    f.`email` AS email
    FROM
    `trans_reservasi_cc` a
    INNER JOIN `group_cc` b
    ON a.`group_cc_id` = b.`group_cc_id`
    INNER JOIN `group_cc_detail` d
    ON b.`group_cc_id` = d.`group_cc_id`
    INNER JOIN employee f
    ON D.`employee_id` = f.`employee_id`
    WHERE a.`NoDokumen` = '".$NoDokumen."';
    ";

    $arrDataCc = $this->globalmodel->getQuery($sql_cc);*/

    $subject = "Notifikasi BEO ".$No_Dokumen;
    $author  = "Auto System";
    $url = 'http://sys.bebektimbungan.com/index.php/bss/reservasi/edit_reservasi/'.$No_Dokumen;

    $body  = '
    Dear Head Of Sales And Marketing BFM,<br><br>

    BEO <b>'.$No_Dokumen.'</b> telah selesai dibuat, mohon Approval BEO dengan <a href='.$url.' target="_blank">KLIK DISINI</a> atau Copy Paste Link ini di broswer anda : '.$url.'.<br><br>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 3px solid #00cc99; " class="border-table">
    <tbody>

    <tr style="background: #00cc99; height: 50;">
    <td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
    <p style="text-transform: uppercase;">
    Alert BANQUET EVENT ORDER No. '.$No_Dokumen.'
    </p>
    </td>
    </tr>

    <tr>
    <td colspan="3">&nbsp;</td>
    </tr>

    <tr height="25" class="tr">
    <td style="padding: 10 10 10 10px;">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr>

    <td width="20%" style=" font-size: 14px;">Reservasi Date</td>
    <td width="5%">:</td>
    <td style=" font-size: 14px;">'.$date_reservasi.'</td>

    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Activities 1</td>
    <td width="5%">:</td>
    <td style=" font-size: 14px;">'.$aktivitas1->nama_aktivitas_beo.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Company</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$travel->Nama.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Activities 2</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$aktivitas2->nama_aktivitas_beo.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Group Name</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$GroupName.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Activities 3</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$aktivitas3->nama_aktivitas_beo.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Telephone</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$travel->Telepon.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Activities 4</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$aktivitas4->nama_aktivitas_beo.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Nationality</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$Nationality.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Activities 5</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$aktivitas5->nama_aktivitas_beo.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Participants</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$adultParticipants.' adult '.$childParticipants.' child '.$kidsParticipants.' kids</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Contact</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$Contact.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Sales</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$nm_sales.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Phone</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$Phone.'</td>

    </tr>

    <tr>

    <td width="20%" style=" font-size: 14px;">Jam</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$Jam_Kedatangan.'</td>

    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td width="20%" style=" font-size: 14px;">Group Code</td>
    <td>:</td>
    <td style=" font-size: 14px;">'.$GroupCode.'</td>

    </tr>

    </table>
    </td>
    </tr>

    <tr height="25" class="tr">
    <td>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr bgcolor="#00cc99">
    <td colspan="100%" align="center" width="10%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>VANUE</strong>
    </td>
    </tr>

    <tr>
    <td colspan="100%">
    '.$vanue1.' '.$vanue2.' '.$vanue3.' '.$vanue4.' '.$vanue5.' '.$vanue6.' '.$vanue7.' '.$vanue8.' '.$vanue9.' '.$vanue10.'
    </td>
    </tr>
    </table>
    </td>
    </tr>



    <tr height="25" class="tr">
    <td>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

    <tr>
    <td colspan="100%">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr>
    <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Front Office</strong></td>
    <td width="1%">&nbsp;</td>
    <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>F & B kitchen</strong></td>
    </tr>
    <tr>
    <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_front_office.'
    </td>
    <td width="1%">&nbsp;</td>
    <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_f_b_kitchen.'
    </td>
    </tr>
    </table>

    </td>
    </tr>


    <tr height="25" class="tr">
    <td>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

    <tr>
    <td colspan="100%">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr>
    <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Black Eye Coffee</strong></td>
    <td width="1%">&nbsp;</td>
    <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Oemah Herborist</strong></td>
    </tr>
    <tr>
    <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_black_aye.'
    </td>
    <td width="1%">&nbsp;</td>
    <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_oemah_herborist.'
    </td>
    </tr>
    </table>

    </td>
    </tr>


    <tr height="25" class="tr">
    <td>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

    <tr>
    <td colspan="100%">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr>
    <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Acounting</strong></td>
    <td  width="1%">&nbsp;</td>
    <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Security & GA</strong></td>
    </tr>
    <tr>
    <td rowspan="3" align="justify" width="50%" style=" vertical-align: top; font-size: 14px;height: 30;">
    '.$desc_accounting.'
    </td>
    <td  width="1%">&nbsp;</td>
    <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_security.'
    </td>
    </tr>


    <tr>
    <td  width="1%">&nbsp;</td>
    <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Design Graphic</strong></td>
    </tr>
    <tr>
    <td  width="1%">&nbsp;</td>
    <td rowspan="3" align="justify" width="50%" style=" font-size: 14px;height: 30;">
    '.$desc_graphis.'
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    </td>
    </tr>


    </table>
    </td>
    </tr>



    </tbody>
    </table>
    <hr>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
    <tr>
    <td colspan="100%">
    <tr>
    <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Total</strong></td>
    <td width="1%">&nbsp;</td>
    <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total).'&nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
    </td>
    </tr>

    <tr>
    <td colspan="100%">
    <tr>
    <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Deposit</strong></td>
    <td width="1%">&nbsp;</td>
    <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($dp).'&nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
    </td>
    </tr>

    <tr>
    <td colspan="100%">
    <tr>
    <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Sisa</strong></td>
    <td width="1%">&nbsp;</td>
    <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total-$dp).'&nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
    </td>
    </tr>
    </table>
    <hr>
    <br><br><br>
    ';

    $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);

    $this->db->update('trans_reservasi',array('status'=>'1'),array("NoDokumen" => $No_Dokumen));

    //kirim ke telegram
    $this->load->library('telegramlib');
    $telegram = new telegramlib();



    $vtext = "
    <b>APPROVAL BEO (ID $id)</b>

    <b>BANQUET EVENT ORDER
    $No_Dokumen</b>

    <b>".$travel->Nama."</b>
    Group Name <b>$GroupName</b>
    Group Code <b>$GroupCode</b>
    Adult <b>$adultParticipants</b> Persons
    Child <b>$childParticipants</b> Persons
    Kids <b>$kidsParticipants</b> Persons
    Arrival Date <b>$date_reservasi</b>
    Arrival Time <b>$Jam_Kedatangan</b>
    SIC <b>$nm_sales</b>

    <b>Activities</b>
    1. ".$aktivitas1->nama_aktivitas_beo."
    2. ".$aktivitas2->nama_aktivitas_beo."
    3. ".$aktivitas3->nama_aktivitas_beo."

    <b>Vanue</b>
    $all_vanue

    Front Office :
    - $desc_front_office

    F & B Kitchen :
    - $desc_f_b_kitchen

    Omah Herborist :
    - $desc_oemah_herborist

    Black Eye :
    - $desc_black_eye

    Accounting :
    - $desc_accounting

    Design Graphic :
    - $desc_graphis

    Security/GA :
    - $desc_security

    Enginering :
    - $desc_engineering_edp

    Biaya Tambahan	 = ".number_format($biaya_tambahan)."
    Total			 = ".number_format($Total)."
    DP 				   = ".number_format($dp)."

    info lebih lengkap silahkan buka email corporate anda. terima kasih.

    ";

    $keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujubeo#'.$id);
    $keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakbeo#'.$id);

    $telegram->chat_id($ChatID);
    //$telegram->chat_id(300127392);
    $telegram->text($vtext);
    $telegram->keyboard($keyboard);
    $telegram->send_with_inline_keyboard();

    //ubah status reject
    $this->db->update('trans_reservasi',array('status_reject'=>'0','tgl_reject'=>'','ket_reject'=>''),array('NoDokumen'=>$No_Dokumen));

    //insert ke tabel voucher_beo
    if($jenis=="2"){

      $tglx=date('Y-m-d');
      $bulanx = substr($tglx, 5, 2);
      $tahunx = substr($tglx, 2, 2);

      $dayx = date('D', strtotime($tglx));
      $kodex = substr(strtoupper($dayx),2,1)."".substr(strtoupper($dayx),0,1);
      $no_voucher_generate = $this->get_no_counter_voucher_beo( $kodex ,"voucher_beo", "no_voucher",$tahunx,$bulanx,"id");

      $data_voucher_beo =  array ('expDate'=>$ReservasiDate,
      'no_voucher'=>$no_voucher_generate,
      'tourtravel'=>$KdTravel,
      'no_voucher_travel'=>$VoucherCode,
      'jenis'=>1,
      'nilai'=>$Total+$biaya_tambahan,
      'NoBukti'=>'',
      'no_receipt'=>$no_voucher_generate,
      'Keterangan'=>'Voucher Langsung Dari Form',
      'status'=>1,
      'KdBank'=>'',
      'BEO'=>$No_Dokumen,
      'uang_muka_beo'=>'T',
      'adduser'=>$AddUser,
      'adddate'=>$AddDate);

      //insert ke table Voucher_beo
      $this->db->insert('voucher_beo',$data_voucher_beo);


      //insert juga ke tabel Voucher
      $data_voucher = array('novoucher'=>$no_voucher_generate,
      'novoucher_travel'=>$VoucherCode,
      'nominal'=>$Total+$biaya_tambahan,
      'expDate'=>$ReservasiDate,
      'addDate'=>$AddDate,
      'userDate'=>$AddUser,
      'status'=>0,
      'notrans'=>'',
      'keterangan'=>'Voucher Terusan dari BEO '.$No_Dokumen,
      'terpakai'=>0,
      'jenis'=>5,
      'rupdisc'=>'',
      'ketentuan'=>'');
      $this->db->insert("voucher", $data_voucher);
    }

    /**
    *
    * @var

    //update no.beo ke uang_muka_beo
    if($id_uang_muka_beo!=""){
    //$this->db->update('uang_muka_beo',array('Status'=>1,'BEO'=>$No_Dokumen),array('id'=>$id_uang_muka_beo));

    //generate no.voucher
    $tgl=date('Y-m-d');
    $bulan = substr($tgl, 5, 2);
    $tahun = substr($tgl, 2, 2);

    $day = date('D', strtotime($tgl));
    $kode = substr(strtoupper($day),2,1)."".substr(strtoupper($day),0,1);
    $no_voucher = $this->get_no_counter_voucher( $kode ,"voucher_beo", "no_voucher",$tahun,$bulan);

    //cari detail uang muka beo
    $uang_beo =  $this->reservasi_model->getUangMukaBeos($id_uang_muka_beo);

    //insert ke voucher_beo
    //jenis 1 piutang sedangkan jenis 2 lunas
    $data_voucher_beo = array(
    'expDate'=>$mylib->ubah_tanggal($date_reservasi),
    'no_voucher'=>$no_voucher,
    'tourtravel'=>$uang_beo->KdTravel,
    'jenis'=>2,
    'KdBank'=>$uang_beo->KdBank,
    'nilai'=>$uang_beo->Nilai,
    'NoBukti'=>$uang_beo->NoBukti,
    'Keterangan'=>$uang_beo->Keterangan,
    'status'=>1,
    'no_receipt'=>$uang_beo->no_receipt,
    'BEO'=>$No_Dokumen,
    'uang_muka_beo'=>'Y',
    'AddUser'=>$user,
    'AddDate'=>date('Y-m-d')
  );

  //$this->db->insert("voucher_beo", $data_voucher_beo);
  $nominal = $uang_beo->Nilai;

}else if($id_voucher_beo!=""){
//$this->db->update('voucher_beo',array('status'=>1),array('id'=>$id_voucher_beo));
$date_voucher =  $this->reservasi_model->getVoucherBeos($id_voucher_beo);
$no_voucher = $date_voucher->no_voucher;
$nominal = $date_voucher->nilai;
}

//insert ke table voucher
/*$this->db->insert("voucher",
array(
'novoucher'=>$no_voucher,
'nominal'=>$nominal,
'expDate'=>$mylib->ubah_tanggal($date_reservasi),
'addDate'=>date('Y-m-d H:i:s'),
'userDate'=>$user,
'status'=>0,
'notrans'=>'',
'keterangan'=>'Travel '.$travel->Nama,
'terpakai'=>0,
'jenis'=>'5',
'rupdisc'=>'D',
'ketentuan'=>'')
);
* */

}

//dikirim ke head untuk edit request
if($edit_request=="1"){

  $NoDokumen = $this->input->post('NoDokumen');
  //pemberian no beo
  $this->db->update("trans_reservasi",array("NoDokumen" => $NoDokumen),array("id" => $id));

  //ubah status edit request
  $this->db->update("trans_reservasi",array("edit_request" => "1"),array("id" => $id));


  $this->globalmodel->queryDelete("trans_reservasi_cc", array("NoDokumen" => $NoDokumen));

  $group_cc = $this->input->post('group_cc');

  for ($i = 0; $i < count($group_cc); $i++) {

    if ($group_cc[$i] != "") {
      $data_group_cc = array(
        "NoDokumen" => $NoDokumen,
        "group_cc_id" => $group_cc[$i]
      );
      $this->globalmodel->queryInsert("trans_reservasi_cc", $data_group_cc);
    }
  }


  $travel = $this->reservasi_model->getTravelMail($KdTravel);
  $salesman = $this->reservasi_model->getSalesMail($Sales_In_Charge);
  $aktivitas1 = $this->reservasi_model->getAktivitas1Mail($Event1);
  $aktivitas2 = $this->reservasi_model->getAktivitas2Mail($Event2);
  $aktivitas3 = $this->reservasi_model->getAktivitas3Mail($Event3);
  $aktivitas4 = $this->reservasi_model->getAktivitas4Mail($Event4);
  $aktivitas5 = $this->reservasi_model->getAktivitas5Mail($Event5);

  if(!empty($salesman)){
    $nm_sales = $salesman->NamaSalesman;
  }else{
    $nm_sales = "";
  }

  if($place_herborist=="1"){
    $vanue1="Oemah Herborist, ";
  }else{
    $vanue1="";
  }

  if($place_the_luwus=="1"){
    $vanue2="The Luwus Atas, ";
  }else{
    $vanue2="";
  }

  if($place_the_luwus_bawah=="1"){
    $vanue3="The Luwus Bawah, ";
  }else{
    $vanue3="";
  }

  if($place_black_eye_coffee=="1"){
    $vanue4="Black Eye Coffee, ";
  }else{
    $vanue4="";
  }

  if($place_the_rice_view=="1"){
    $vanue5="The Rice View, ";
  }else{
    $vanue5="";
  }

  if($place_chappel=="1"){
    $vanue6="The Chamber, ";
  }else{
    $vanue6="";
  }

  if($place_rice_field_deck=="1"){
    $vanue7="The Rice Field Deck, ";
  }else{
    $vanue7="";
  }

  if($place_garden_amphi_theater=="1"){
    $vanue8="Garden Amphi Theater, ";
  }else{
    $vanue8="";
  }

  if($place_bale_agung=="1"){
    $vanue9="Bale Agung, ";
  }else{
    $vanue9="";
  }

  if($place_deck_view=="1"){
    $vanue10="The Deck View.";
  }else{
    $vanue10="";
  }

  $all_vanue = $vanue1.$vanue2.$vanue3.$vanue4.$vanue5.$vanue6.$vanue7.$vanue8.$vanue9.$vanue10;

  //untuk status 1 itu dikirim ke Head Sales
  $mylib = new globallib();

  $to = "";
  $to_name = "";

  /*$sql = "
  a.`email_address` AS email,
  a.`email_name` AS employee_name
  FROM
  `function_email` a
  WHERE a.`func_name` = 'beo' ;
  ";*/

  $sql = "  c.`NamaSupervisor` AS employee_name,
  c.`email` AS email,
  c.`UserName`,
  d.`TelegramID`
  FROM
  `trans_reservasi` a
  INNER JOIN salesman b
  ON a.`Sales_In_Charge` = b.`KdSalesman`
  INNER JOIN supervisor c
  ON b.`KdSupervisor` = c.`KdSupervisor`
  INNER JOIN `user` d
  ON d.`UserName` = c.`UserName`
  WHERE a.`NoDokumen` = '".$NoDokumen."';
  ";
  $arrData = $this->globalmodel->getQuery($sql);


  foreach($arrData as $val)
  {
    $to .= $val["email"].";";
    $to_name .= $val["employee_name"].";";
    $ChatID = $val["TelegramID"];
  }

  /*$sql_cc = "
  f.`employee_name` AS employee_name,
  f.`email` AS email
  FROM
  `trans_reservasi_cc` a
  INNER JOIN `group_cc` b
  ON a.`group_cc_id` = b.`group_cc_id`
  INNER JOIN `group_cc_detail` d
  ON b.`group_cc_id` = d.`group_cc_id`
  INNER JOIN employee f
  ON D.`employee_id` = f.`employee_id`
  WHERE a.`NoDokumen` = '".$NoDokumen."';
  ";

  $arrDataCc = $this->globalmodel->getQuery($sql_cc);*/

  $subject = "Notifikasi BEO ".$NoDokumen;
  $author  = "Auto System";
  $url = 'http://sys.bebektimbungan.com/index.php/bss/reservasi/edit_reservasi/'.$NoDokumen;

  $body  = '
  Dear Head Of Sales And Marketing BFM,<br><br>

  BEO <b>'.$NoDokumen.'</b> telah selesai diApprove namun ingin di edit, karena '.$ket_edit_request.' mohon Approval Edit Request BEO Tersebut dengan <a href='.$url.' target="_blank">KLIK DISINI</a> atau Copy Paste Link ini di broswer anda : '.$url.'.<br><br>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 3px solid #00cc99; " class="border-table">
  <tbody>

  <tr style="background: #00cc99; height: 50;">
  <td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
  <p style="text-transform: uppercase;">
  Alert BANQUET EVENT ORDER No. '.$NoDokumen.'
  </p>
  </td>
  </tr>

  <tr>
  <td colspan="3">&nbsp;</td>
  </tr>

  <tr height="25" class="tr">
  <td style="padding: 10 10 10 10px;">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>

  <td width="20%" style=" font-size: 14px;">Reservasi Date</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$date_reservasi.'</td>

  <td width="5%">&nbsp;</td>
  <td width="5%">&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 1</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$aktivitas1->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Company</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Nama.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 2</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas2->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Group Name</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupName.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 3</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas3->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Telephone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Telepon.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 4</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas4->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Nationality</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Nationality.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 5</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas5->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Participants</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$adultParticipants.' adult '.$childParticipants.' child '.$kidsParticipants.' kids</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Contact</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Contact.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Sales</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$nm_sales.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Phone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Phone.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Jam</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Jam_Kedatangan.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Group Code</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupCode.'</td>

  </tr>

  </table>
  </td>
  </tr>

  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr bgcolor="#00cc99">
  <td colspan="100%" align="center" width="10%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>VANUE</strong>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  '.$vanue1.' '.$vanue2.' '.$vanue3.' '.$vanue4.' '.$vanue5.' '.$vanue6.' '.$vanue7.' '.$vanue8.' '.$vanue9.' '.$vanue10.'
  </td>
  </tr>
  </table>
  </td>
  </tr>



  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Front Office</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>F & B kitchen</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_front_office.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_f_b_kitchen.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Black Eye Coffee</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Oemah Herborist</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_black_aye.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_oemah_herborist.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Acounting</strong></td>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Security & GA</strong></td>
  </tr>
  <tr>
  <td rowspan="3" align="justify" width="50%" style=" vertical-align: top; font-size: 14px;height: 30;">
  '.$desc_accounting.'
  </td>
  <td  width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_security.'
  </td>
  </tr>


  <tr>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Design Graphic</strong></td>
  </tr>
  <tr>
  <td  width="1%">&nbsp;</td>
  <td rowspan="3" align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_graphis.'
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  </td>
  </tr>


  </table>
  </td>
  </tr>



  </tbody>
  </table>
  <hr>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Total</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Deposit</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Sisa</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total-$dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>
  </table>
  <hr>
  <br><br><br>
  ';

  $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);

  $this->db->update('trans_reservasi',array('status'=>'1'),array("NoDokumen" => $NoDokumen));

  //kirim ke telegram
  $this->load->library('telegramlib');
  $telegram = new telegramlib();



  $vtext = "
  <b>REQUEST EDIT BEO (ID $id)</b>

  <b>BANQUET EVENT ORDER
  $NoDokumen</b>

  <b>".$travel->Nama."</b>
  Group Name <b>$GroupName</b>
  Group Code <b>$GroupCode</b>
  Adult <b>$adultParticipants</b> Persons
  Child <b>$childParticipants</b> Persons
  Kids <b>$kidsParticipants</b> Persons
  Arrival Date <b>$date_reservasi</b>
  Arrival Time <b>$Jam_Kedatangan</b>

  Alasan Edit BEO ini $ket_edit_request

  Total = ".number_format($Total)."
  DP    = ".number_format($dp)."

  info lebih lengkap silahkan buka email corporate anda. terima kasih.

  ";

  $keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujueditbeo#'.$id);
  $keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakeditbeo#'.$id);

  $telegram->chat_id($ChatID);
  //$telegram->chat_id(300127392);
  $telegram->text($vtext);
  $telegram->keyboard($keyboard);
  $telegram->send_with_inline_keyboard();

}

if($v_approve_head=="1"){

  $NoDokumen = $this->input->post('NoDokumen');

  //pemberian no beo
  $this->db->update("trans_reservasi",array("NoDokumen" => $NoDokumen),array("id" => $id));

  $travel = $this->reservasi_model->getTravelMail($KdTravel);
  $salesman = $this->reservasi_model->getSalesMail($Sales_In_Charge);
  $aktivitas1 = $this->reservasi_model->getAktivitas1Mail($Event1);
  $aktivitas2 = $this->reservasi_model->getAktivitas2Mail($Event2);
  $aktivitas3 = $this->reservasi_model->getAktivitas3Mail($Event3);
  $aktivitas4 = $this->reservasi_model->getAktivitas4Mail($Event4);
  $aktivitas5 = $this->reservasi_model->getAktivitas5Mail($Event5);

  if(!empty($salesman)){
    $nm_sales = $salesman->NamaSalesman;
  }else{
    $nm_sales = "";
  }

  if($place_herborist=="1"){
    $vanue1="Oemah Herborist, ";
  }else{
    $vanue1="";
  }

  if($place_the_luwus=="1"){
    $vanue2="The Luwus Atas, ";
  }else{
    $vanue2="";
  }

  if($place_the_luwus_bawah=="1"){
    $vanue3="The Luwus Bawah, ";
  }else{
    $vanue3="";
  }

  if($place_black_eye_coffee=="1"){
    $vanue4="Black Eye Coffee, ";
  }else{
    $vanue4="";
  }

  if($place_the_rice_view=="1"){
    $vanue5="The Rice View, ";
  }else{
    $vanue5="";
  }

  if($place_chappel=="1"){
    $vanue6="The Chamber, ";
  }else{
    $vanue6="";
  }

  if($place_rice_field_deck=="1"){
    $vanue7="The Rice Field Deck, ";
  }else{
    $vanue7="";
  }

  if($place_garden_amphi_theater=="1"){
    $vanue8="Garden Amphi Theater, ";
  }else{
    $vanue8="";
  }

  if($place_bale_agung=="1"){
    $vanue9="Bale Agung, ";
  }else{
    $vanue9="";
  }

  if($place_deck_view=="1"){
    $vanue10="The Deck View, ";
  }else{
    $vanue10="";
  }

  //untuk status 1 itu dikirim ke Head Sales
  $mylib = new globallib();

  $to = "";
  $to_name = "";

  $sql = "
  a.`email_address` AS email,
  a.`email_name` AS employee_name
  FROM
  `function_email` a
  WHERE a.`func_name` = 'beo' ;
  ";

  $arrData = $this->globalmodel->getQuery($sql);


  foreach($arrData as $val)
  {
    $to .= $val["email"].";";
    $to_name .= $val["employee_name"].";";
  }

  $sql_cc = "
  f.`employee_name` AS employee_name,
  f.`email` AS email
  FROM
  `trans_reservasi_cc` a
  INNER JOIN `group_cc` b
  ON a.`group_cc_id` = b.`group_cc_id`
  INNER JOIN `group_cc_detail` d
  ON b.`group_cc_id` = d.`group_cc_id`
  INNER JOIN employee f
  ON D.`employee_id` = f.`employee_id`
  WHERE a.`NoDokumen` = '".$NoDokumen."';
  ";

  $arrDataCc = $this->globalmodel->getQuery($sql_cc);

  $subject = "Notifikasi BEO ".$NoDokumen;
  $author  = "Auto System";
  $url = 'http://sys.bebektimbungan.com/index.php/bss/reservasi/edit_reservasi/'.$NoDokumen;

  $body  = '
  Dear All,<br><br>

  BEO <b>'.$NoDokumen.'</b> telah selesai dibuat dan di Approve. Silahkan <a href='.$url.' target="_blank">KLIK DISINI</a> atau Copy Paste Link ini di broswer anda : '.$url.'.<br><br>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 3px solid #00cc99; " class="border-table">
  <tbody>

  <tr style="background: #00cc99; height: 50;">
  <td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
  <p style="text-transform: uppercase;">
  Alert BANQUET EVENT ORDER No. '.$NoDokumen.'
  </p>
  </td>
  </tr>

  <tr>
  <td colspan="3">&nbsp;</td>
  </tr>

  <tr height="25" class="tr">
  <td style="padding: 10 10 10 10px;">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>

  <td width="20%" style=" font-size: 14px;">Reservasi Date</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$date_reservasi.'</td>

  <td width="5%">&nbsp;</td>
  <td width="5%">&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 1</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$aktivitas1->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Company</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Nama.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 2</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas2->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Group Name</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupName.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 3</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas3->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Telephone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Telepon.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 4</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas4->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Nationality</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Nationality.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 5</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas5->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Participants</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$adultParticipants.' adult '.$childParticipants.' child '.$kidsParticipants.' kids</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Contact</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Contact.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Sales</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$nm_sales.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Phone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Phone.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Jam</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Jam_Kedatangan.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Group Code</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupCode.'</td>

  </tr>


  </table>
  </td>
  </tr>





  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr bgcolor="#00cc99">
  <td colspan="100%" align="center" width="10%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>VANUE</strong>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  '.$vanue1.' '.$vanue2.' '.$vanue3.' '.$vanue4.' '.$vanue5.' '.$vanue6.' '.$vanue7.' '.$vanue8.' '.$vanue9.' '.$vanue10.'
  </td>
  </tr>
  </table>
  </td>
  </tr>



  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Front Office</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>F & B kitchen</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_front_office.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_f_b_kitchen.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Black Eye Coffee</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Oemah Herborist</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_black_aye.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_oemah_herborist.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Acounting</strong></td>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Security & GA</strong></td>
  </tr>
  <tr>
  <td rowspan="3" align="justify" width="50%" style=" vertical-align: top; font-size: 14px;height: 30;">
  '.$desc_accounting.'
  </td>
  <td  width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_security.'
  </td>
  </tr>


  <tr>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Design Graphic</strong></td>
  </tr>
  <tr>
  <td  width="1%">&nbsp;</td>
  <td rowspan="3" align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_graphis.'
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  </td>
  </tr>


  </table>
  </td>
  </tr>



  </tbody>
  </table>
  <hr>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Total</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Deposit</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Sisa</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total-$dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>
  </table>
  <hr>
  <br><br><br>
  ';

  $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);

  $this->db->update('trans_reservasi',array('status'=>'1','status_approve'=>'1','tgl_approve'=>date('Y-m-d')),array("NoDokumen" => $NoDokumen));
}

if($v_reject_head=="1"){

  $NoDokumen = $this->input->post('NoDokumen');

  //pemberian no beo
  $this->db->update("trans_reservasi",array("NoDokumen" => $NoDokumen),array("id" => $id));

  $travel = $this->reservasi_model->getTravelMail($KdTravel);
  $salesman = $this->reservasi_model->getSalesMail($Sales_In_Charge);
  $aktivitas1 = $this->reservasi_model->getAktivitas1Mail($Event1);
  $aktivitas2 = $this->reservasi_model->getAktivitas2Mail($Event2);
  $aktivitas3 = $this->reservasi_model->getAktivitas3Mail($Event3);
  $aktivitas4 = $this->reservasi_model->getAktivitas4Mail($Event4);
  $aktivitas5 = $this->reservasi_model->getAktivitas5Mail($Event5);

  if(!empty($salesman)){
    $nm_sales = $salesman->NamaSalesman;
  }else{
    $nm_sales = "";
  }

  if($place_herborist=="1"){
    $vanue1="Oemah Herborist, ";
  }else{
    $vanue1="";
  }

  if($place_the_luwus=="1"){
    $vanue2="The Luwus Atas, ";
  }else{
    $vanue2="";
  }

  if($place_the_luwus_bawah=="1"){
    $vanue3="The Luwus Bawah, ";
  }else{
    $vanue3="";
  }

  if($place_black_eye_coffee=="1"){
    $vanue4="Black Eye Coffee, ";
  }else{
    $vanue4="";
  }

  if($place_the_rice_view=="1"){
    $vanue5="The Rice View, ";
  }else{
    $vanue5="";
  }

  if($place_chappel=="1"){
    $vanue6="The Chamber, ";
  }else{
    $vanue6="";
  }

  if($place_rice_field_deck=="1"){
    $vanue7="The Rice Field Deck, ";
  }else{
    $vanue7="";
  }

  if($place_garden_amphi_theater=="1"){
    $vanue8="Garden Amphi Theater, ";
  }else{
    $vanue8="";
  }

  if($place_bale_agung=="1"){
    $vanue9="Bale Agung, ";
  }else{
    $vanue9="";
  }

  if($place_deck_view=="1"){
    $vanue10="The Deck View, ";
  }else{
    $vanue10="";
  }

  //untuk status 1 itu dikirim ke Head Sales
  $mylib = new globallib();

  $sql = "
  b.`employee_name`,
  b.`email`
  FROM
  `trans_reservasi` a
  INNER JOIN employee b
  ON a.`AddUser` = b.`username`
  WHERE a.`NoDokumen` = '".$NoDokumen."';
  ";

  $arrData = $this->globalmodel->getQuery($sql);


  foreach($arrData as $val)
  {
    $to = $val["email"];
    $to_name = $val["employee_name"];
  }


  $subject = "Notifikasi BEO ".$NoDokumen;
  $author  = "Auto System";
  $url = 'http://sys.bebektimbungan.com/index.php/bss/reservasi/edit_reservasi/'.$NoDokumen;

  $body  = '
  Dear '.$to_name.',<br><br>

  BEO <b>'.$NoDokumen.'</b> telah direject, karena <b>'.$ket_reject.'</b>. Silahkan <a href='.$url.' target="_blank">KLIK DISINI</a> untuk memperbaiki atau Copy Paste Link ini di broswer anda : '.$url.'.<br><br>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 3px solid #00cc99; " class="border-table">
  <tbody>

  <tr style="background: #00cc99; height: 50;">
  <td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
  <p style="text-transform: uppercase;">
  Alert BANQUET EVENT ORDER No. '.$NoDokumen.'
  </p>
  </td>
  </tr>

  <tr>
  <td colspan="3">&nbsp;</td>
  </tr>

  <tr height="25" class="tr">
  <td style="padding: 10 10 10 10px;">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>

  <td width="20%" style=" font-size: 14px;">Reservasi Date</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$date_reservasi.'</td>

  <td width="5%">&nbsp;</td>
  <td width="5%">&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 1</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$aktivitas1->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Company</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Nama.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 2</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas2->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Group Name</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupName.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 3</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas3->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Telephone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Telepon.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 4</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas4->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Nationality</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Nationality.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 5</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas5->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Participants</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$adultParticipants.' adult '.$childParticipants.' child '.$kidsParticipants.' kids</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Contact</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Contact.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Sales</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$nm_sales.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Phone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Phone.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Jam</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$Jam_Kedatangan.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Group Code</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$GroupCode.'</td>

  </tr>
  </table>
  </td>
  </tr>





  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr bgcolor="#00cc99">
  <td colspan="100%" align="center" width="10%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>VANUE</strong>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  '.$vanue1.' '.$vanue2.' '.$vanue3.' '.$vanue4.' '.$vanue5.' '.$vanue6.' '.$vanue7.' '.$vanue8.' '.$vanue9.' '.$vanue10.'
  </td>
  </tr>
  </table>
  </td>
  </tr>



  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Front Office</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>F & B kitchen</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_front_office.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_f_b_kitchen.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Black Eye Coffee</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Oemah Herborist</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_black_aye.'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_oemah_herborist.'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Acounting</strong></td>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Security & GA</strong></td>
  </tr>
  <tr>
  <td rowspan="3" align="justify" width="50%" style=" vertical-align: top; font-size: 14px;height: 30;">
  '.$desc_accounting.'
  </td>
  <td  width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_security.'
  </td>
  </tr>


  <tr>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Design Graphic</strong></td>
  </tr>
  <tr>
  <td  width="1%">&nbsp;</td>
  <td rowspan="3" align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$desc_graphis.'
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  </td>
  </tr>


  </table>
  </td>
  </tr>



  </tbody>
  </table>
  <hr>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Total</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Deposit</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Sisa</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($Total-$dp).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>
  </table>
  <hr>
  <br><br><br>
  ';

  $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);

  $this->db->update('trans_reservasi',array('status'=>'0','status_approve'=>'0','status_reject'=>'1','tgl_reject'=>date('Y-m-d'),'ket_reject'=>$ket_reject),array("NoDokumen" => $NoDokumen));
}

redirect('/bss/reservasi/');
}


function email_telebot($id){
  //kirim email

  $getDataReserv = $this->globalmodel->getQuery("
  *,a.`Nationality` AS asal_negara,b.Alamat,b.Telepon , a.`Contact` AS Contacts, a.`Phone` AS Phones,
  a.Sales_In_Charge AS Saless
  from
  trans_reservasi a
  left join
  tourtravel b
  on
  a.KdTravel=b.KdTravel
  INNER JOIN salesman c
  ON a.`Sales_In_Charge` = c.`KdSalesman`
  where
  1
  and id = '" . $id . "'");

  $travel = $this->reservasi_model->getTravelMail($getDataReserv[0]['KdTravel']);
  $salesman = $this->reservasi_model->getSalesMail($getDataReserv[0]['Saless']);
  $aktivitas1 = $this->reservasi_model->getAktivitas1Mail($getDataReserv[0]['Event1']);
  $aktivitas2 = $this->reservasi_model->getAktivitas2Mail($getDataReserv[0]['Event2']);
  $aktivitas3 = $this->reservasi_model->getAktivitas3Mail($getDataReserv[0]['Event3']);
  $aktivitas4 = $this->reservasi_model->getAktivitas4Mail($getDataReserv[0]['Event4']);
  $aktivitas5 = $this->reservasi_model->getAktivitas5Mail($getDataReserv[0]['Event5']);

  if(!empty($salesman)){
    $nm_sales = $salesman->NamaSalesman;
  }else{
    $nm_sales = "";
  }

  if($getDataReserv[0]['place_herborist']=="1"){
    $vanue1="Oemah Herborist, ";
  }else{
    $vanue1="";
  }

  if($getDataReserv[0]['place_the_luwus']=="1"){
    $vanue2="The Luwus Atas, ";
  }else{
    $vanue2="";
  }

  if($getDataReserv[0]['place_the_luwus_bawah']=="1"){
    $vanue3="The Luwus Bawah, ";
  }else{
    $vanue3="";
  }

  if($getDataReserv[0]['place_black_eye_coffee']=="1"){
    $vanue4="Black Eye Coffee, ";
  }else{
    $vanue4="";
  }

  if($getDataReserv[0]['place_the_rice_view']=="1"){
    $vanue5="The Rice View, ";
  }else{
    $vanue5="";
  }

  if($getDataReserv[0]['place_chappel']=="1"){
    $vanue6="The Chamber, ";
  }else{
    $vanue6="";
  }

  if($getDataReserv[0]['place_rice_field_deck']=="1"){
    $vanue7="The Rice Field Deck, ";
  }else{
    $vanue7="";
  }

  if($getDataReserv[0]['place_garden_amphi_theater']=="1"){
    $vanue8="Garden Amphi Theater, ";
  }else{
    $vanue8="";
  }

  if($getDataReserv[0]['place_bale_agung']=="1"){
    $vanue9="Bale Agung, ";
  }else{
    $vanue9="";
  }

  if($getDataReserv[0]['place_deck_view']=="1"){
    $vanue10="The Deck View, ";
  }else{
    $vanue10="";
  }

  $mylib = new globallib();

  $to = "";
  $to_name = "";

  $sql = "
  a.`email_address` AS email,
  a.`email_name` AS employee_name
  FROM
  `function_email` a
  WHERE a.`func_name` = 'beo' ;
  ";

  $arrData = $this->globalmodel->getQuery($sql);


  foreach($arrData as $val)
  {
    $to .= $val["email"].";";
    $to_name .= $val["employee_name"].";";
  }


  $subject = "Notifikasi BEO ".$getDataReserv[0]['NoDokumen'];
  $author  = "Auto System";
  $url = 'http://sys.bebektimbungan.com/index.php/bss/reservasi/edit_reservasi/'.$getDataReserv[0]['NoDokumen'];
  $body  = '
  Dear All,<br><br>

  BEO <b>'.$getDataReserv[0]['NoDokumen'].'</b> telah selesai dibuat dan di Approve. Silahkan <a href='.$url.' target="_blank">KLIK DISINI</a> atau Copy Paste Link ini di broswer anda : '.$url.'.<br><br>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 3px solid #00cc99; " class="border-table">
  <tbody>

  <tr style="background: #00cc99; height: 50;">
  <td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
  <p style="text-transform: uppercase;">
  Alert BANQUET EVENT ORDER No. '.$getDataReserv[0]['NoDokumen'].'
  </p>
  </td>
  </tr>

  <tr>
  <td colspan="3">&nbsp;</td>
  </tr>

  <tr height="25" class="tr">
  <td style="padding: 10 10 10 10px;">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>

  <td width="20%" style=" font-size: 14px;">Reservasi Date</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['TglDokumen'].'</td>

  <td width="5%">&nbsp;</td>
  <td width="5%">&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 1</td>
  <td width="5%">:</td>
  <td style=" font-size: 14px;">'.$aktivitas1->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Company</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Nama.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 2</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas2->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Group Name</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['GroupName'].'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 3</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas3->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Telephone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$travel->Telepon.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 4</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas4->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Nationality</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['Nationality'].'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Activities 5</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$aktivitas5->nama_aktivitas_beo.'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Participants</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['adultParticipants'].' adult, '.$getDataReserv[0]['childParticipants'].' child, '.$getDataReserv[0]['kidsParticipants'].' Kids</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Contact</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['Contact'].'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Sales</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$nm_sales.'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Phone</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['Phone'].'</td>

  </tr>

  <tr>

  <td width="20%" style=" font-size: 14px;">Jam</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['Jam_Kedatangan'].'</td>

  <td>&nbsp;</td>
  <td>&nbsp;</td>

  <td width="20%" style=" font-size: 14px;">Group Code</td>
  <td>:</td>
  <td style=" font-size: 14px;">'.$getDataReserv[0]['GroupCode'].'</td>

  </tr>


  </table>
  </td>
  </tr>





  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr bgcolor="#00cc99">
  <td colspan="100%" align="center" width="10%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>VANUE</strong>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  '.$vanue1.' '.$vanue2.' '.$vanue3.' '.$vanue4.' '.$vanue5.' '.$vanue6.' '.$vanue7.' '.$vanue8.' '.$vanue9.' '.$vanue10.'
  </td>
  </tr>
  </table>
  </td>
  </tr>



  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Front Office</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>F & B kitchen</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_front_office'].'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_f_b_kitchen'].'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Black Eye Coffee</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Oemah Herborist</strong></td>
  </tr>
  <tr>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_black_aye'].'
  </td>
  <td width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_oemah_herborist'].'
  </td>
  </tr>
  </table>

  </td>
  </tr>


  <tr height="25" class="tr">
  <td>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">

  <tr>
  <td colspan="100%">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Acounting</strong></td>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Security & GA</strong></td>
  </tr>
  <tr>
  <td rowspan="3" align="justify" width="50%" style=" vertical-align: top; font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_accounting'].'
  </td>
  <td  width="1%">&nbsp;</td>
  <td align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_security'].'
  </td>
  </tr>


  <tr>
  <td  width="1%">&nbsp;</td>
  <td  align="center" width="50%" style="background: #00cc99; font-size: 14px;height: 30;"><strong>Design Graphic</strong></td>
  </tr>
  <tr>
  <td  width="1%">&nbsp;</td>
  <td rowspan="3" align="justify" width="50%" style=" font-size: 14px;height: 30;">
  '.$getDataReserv[0]['desc_graphis'].'
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  </td>
  </tr>


  </table>
  </td>
  </tr>



  </tbody>
  </table>
  <hr>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #ffffff; ">
  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Total</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($getDataReserv[0]['Total']).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Deposit</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($getDataReserv[0]['dp']).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>

  <tr>
  <td colspan="100%">
  <tr>
  <td align="right" width="70%" style=" font-size: 14px;height: 30;"><strong>Sisa</strong></td>
  <td width="1%">&nbsp;</td>
  <td align="right" width="30%" style=" font-size: 14px;height: 30;"><strong>'.number_format($getDataReserv[0]['$Total']-$getDataReserv[0]['$dp']).'&nbsp;&nbsp;&nbsp;</strong></td>
  </tr>
  </td>
  </tr>
  </table>
  <hr>
  <br><br><br>
  ';

  $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);

  $this->db->update('trans_reservasi',array('status_approve'=>'1','tgl_approve'=>date('Y-m-d')),array('NoDokumen'=>$getDataReserv[0]['NoDokumen']));


  $sql = "  c.`NamaSupervisor` AS employee_name,
  c.`email` AS email,
  c.`UserName`,
  d.`TelegramID`
  FROM
  `trans_reservasi` a
  INNER JOIN salesman b
  ON a.`Sales_In_Charge` = b.`KdSalesman`
  INNER JOIN supervisor c
  ON b.`KdSupervisor` = c.`KdSupervisor`
  INNER JOIN `user` d
  ON d.`UserName` = c.`UserName`
  WHERE a.`NoDokumen` = '".$getDataReserv[0]['NoDokumen']."';
  ";
  $arrData = $this->globalmodel->getQuery($sql);


  foreach($arrData as $val)
  {
    $to .= $val["email"].";";
    $to_name .= $val["employee_name"].";";
    $ChatID = $val["TelegramID"];
  }

  //kirim ke telegram
  $this->load->library('telegramlib');
  $telegram = new telegramlib();
  $vtext = "
  Berhasil Approve.
  ";
  $telegram->chat_id($ChatID);
  //$telegram->chat_id(300127392); irfan
  $telegram->text($vtext);
  $telegram->keyboard($keyboard);
  $telegram->send_with_inline_keyboard();
}

function batal_reservasi(){
  $thn = $this->uri->segment(7);
  $bln = $this->uri->segment(8);
  $no = $this->uri->segment(9);
  $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
  $data = array("Status"=>"2");
  $where = array("NoDokumen"=>$nodok);
  $this->globalmodel->queryUpdate("trans_reservasi",$data,$where);
  redirect('/bss/reservasi/');
}

function dateRomawi($bulan) {
  switch ($bulan) {
    case 1:
    $blnRom = 'I';
    break;
    case 2:
    $blnRom = 'II';
    break;
    case 3:
    $blnRom = 'III';
    break;
    case 4:
    $blnRom = 'IV';
    break;
    case 5:
    $blnRom = 'V';
    break;
    case 6:
    $blnRom = 'VI';
    break;
    case 7:
    $blnRom = 'VII';
    break;
    case 8:
    $blnRom = 'VIII';
    break;
    case 9:
    $blnRom = 'IX';
    break;
    case 10:
    $blnRom = 'X';
    break;
    case 11:
    $blnRom = 'XI';
    break;
    case 12:
    $blnRom = 'XII';
    break;
  }
  return $blnRom;
}

function create_pdf() {
  //  $id = $this->uri->segment(4);
  $thn = $this->uri->segment(7);
  $bln = $this->uri->segment(8);
  $no = $this->uri->segment(9);
  $nodok = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
  $getHeader = $this->reservasi_model->getHeader($nodok);
  $data['reservasidate']=$getHeader[0]['tgl_reservasi'];
  $AddUser = $getHeader[0]['AddUser'];
  $data['aktivitas'] = $this->reservasi_model->getAktivitas();
  // $getName = $this->globalmodel->getQuery(" employee.employee_name,user.UserLevel from employee join user on employee.username=user.UserName where employee.username='".$AddUser."' ");
  // $data['v_add_user'] = $getName[0]['employee_name'];
  // $data['v_level_user'] = $getName[0]['UserLevel'];
  // $data['v_add_userid'] = $AddUser;
  $data['sales'] = $this->reservasi_model->getSales();

  $user = $this->session->userdata('username');
  $data['adm_sales'] = $this->reservasi_model->getAdmSales($user);
  if(empty($data['adm_sales'])){
    $data['adm_sales_nama'] = "";
    $data['adm_sales_jabatan'] = "";
  }else{
    $data['adm_sales_nama'] = $data['adm_sales'][0]['Nama'];
    $data['adm_sales_jabatan'] = $data['adm_sales'][0]['Jabatan'];
  }

  $data['group_cc'] = $this->globalmodel->getQuery(" group_cc_id,group_cc_name from group_cc");
  $data['group_cc_value'] = $this->globalmodel->getQuery(" * from trans_reservasi_cc where NoDokumen='".$nodok."'");

  $data['results'] = $getHeader;
  $html = $this->load->view('bss/reservasi/pdf_reservasi', $data, true);
  $this->load->library('m_pdf');

  $pdfFilePath = "the_pdf_reservasi.pdf";
  $pdf = $this->m_pdf->load();
  $pdf->WriteHTML($html);

  $pdf->Output();
  exit;
}

function cetak() {
  $data = $this->varCetak();
  $this->load->view('bss/cetak_bss/cetak_bss_reservasi', $data);
}

function versistruk() {
  $data = $this->varCetak();
  // $no = $this->uri->segment(4);
  $ip_address = $_SERVER['REMOTE_ADDR'];
  //$ip = "192.168.0.75";
  //  $printer = $this->reservasi_model->NamaPrinter($ip);
  //print_r($printer); die();
  //        print_r($_SERVER['REMOTE_ADDR']);
  //        die();
  //        $data['ip'] = $printer[0]['ip'];
  //        $data['nm_printer'] = $printer[0]['nm_printer'];
  // $data['store'] = $this->reservasi_model->aplikasi();
  //        $data['header'] = $this->reservasi_model->getHeader($no);
  //        $data['detail'] = $this->reservasi_model->getHeaderForPrint($no);

  $thn = $this->uri->segment(7);
  $bln = $this->uri->segment(8);
  $no = $this->uri->segment(9);
  $id = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
  $data['store'] = $this->pos_model->aplikasi();
  $data['header'] = $this->reservasi_model->getHeader($id);
  $data['detail'] = $this->reservasi_model->getHeaderForPrint($id);

  if (!empty($data['header'])) {
    //$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
    $this->load->view('bss/reservasi/cetak_strukreservasi', $data); // jika ada printernya
  }
}

function printThis() {
  $data = $this->varCetak();
  $id = $this->uri->segment(4);
  $data['fileName2'] = "reservasi.sss";
  $data['fontstyle'] = chr(27) . chr(80);
  $data['nfontstyle'] = "";
  $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
  //		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
  //		$data['string2'] = "(                     )         (                      )";
  $data['string1'] = "     Dibuat Oleh                Diperiksa Oleh                   Menyetujui";
  $data['string2'] = "(                     )    (                      )       (                      )";
  $this->load->view('bss/cetak_bss/cetak_bss_printer', $data);
}

function varCetak() {
  $this->load->library('printreportlib');
  $mylib = new printreportlib();
  $thn = $this->uri->segment(7);
  $bln = $this->uri->segment(8);
  $no = $this->uri->segment(9);
  $id = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
  $header = $this->reservasi_model->getHeaderForPrint($id);

  $data['header'] = $header;
  $detail = $this->reservasi_model->getDetailForPrint($id);
  //        print_r($detail);
  $data['judul1'] = array("No Dokumen", "Tanggal");
  $data['niljudul1'] = array($header->NoDokumen, $header->AddDate);
  $data['judul2'] = array("Group Name", "Event");
  $data['niljudul2'] = array($header->KdTravel, $header->Event);
  $data['judullap'] = "Reservasi";
  $data['url'] = "reservasi/printThis/" . $id;
  $data['url2'] = "reservasi/versistruk/" . $id;
  $data['colspan_line'] = 4;
  $data['lebar_detail'] = array(15, 30, 30, 15); // total 95  Rekening 	Nama 	Keterangan 	Jumlah
  $data['tipe_judul_detail'] = array("normal", "normal", "normal", "kanan");
  $data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
  $data['panjang_kertas'] = 30;
  $default_page_written = 19;
  $data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $default_page_written;
  if ($data['panjang_per_hal'] != 0) {
    $data['tot_hal'] = ceil((int) count($detail) / (int) $data['panjang_per_hal']);
  } else {
    $data['tot_hal'] = 1;
  }
  $list_detail = array();
  $detail_page = array();
  $counterRow = 0;
  $max_field_len = array(0, 0, 0, 0);
  for ($m = 0; $m < count($detail); $m++) {
    unset($list_detail);
    $counterRow++;
    $list_detail[] = stripslashes($detail[$m]['PCode']);
    $list_detail[] = stripslashes(substr($detail[$m]['NamaBarang'], 0, 20));
    $list_detail[] = stripslashes(substr($detail[$m]['Qty'], 0, 20)) . "          ";
    $list_detail[] = number_format($detail[$m]['Harga'], 2, ",", ".");
    $detail_page[] = $list_detail;
    $max_field_len = $mylib->get_max_field_len($max_field_len, $list_detail);
    if ($data['panjang_per_hal'] != 0) {
      if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
        $data['detail'][] = $detail_page;
        if ($m != count($detail) - 1) {
          unset($detail_page);
        }
      }
    }
  }
  $data['detail'][] = $detail_page;
  // $data['footer1'] = array("Jumlah Payment");
  // $data['footer2'] = array(number_format($header->JumlahPayment, 2, ",", "."));
  $data['brs_footer'] = array(20, 3, 15);
  $data['max_field_len'] = $max_field_len;
  $data['banyakBarang'] = $counterRow;
  $data['string1'] = "Dibuat Oleh";
  $data['string2'] = "Menyetujui";
  $data['string3'] = "(____________________)";
  $data['string4'] = "(____________________)";
  $data['judul_netto'] = array("Total");
  $data['isi_netto'] = "";
  return $data;
}

public function ajax_edit_bayar($id) {
  $nodok = str_replace("-", "/", $id);
  $data = $this->reservasi_model->get_by_id($nodok);
  echo json_encode($data);
}

public function ajax_update_bayar() {
  //$this->_validate();
  //echo "<pre>";print_r($_POST);echo "</pre>";die;
  $mylib = new globallib();
  $no = $this->input->post('NoDokumen');
  $NoDokumen = $this->input->post('NoDokumen');
  $Dp = $this->input->post('Dp');
  $Nama = $this->input->post('Nama');
  $KdKasBank = $this->input->post('v_KdKasBank');
  $no_bukti = $this->input->post('v_no_bukti');
  $tanggal = $this->input->post('v_date_reservasi');

  $data = array(
    'NoDokumen' => $no,
    'Jenis_Sisa_Bayar' => $this->input->post('rbtPembayaran'),
    'Kasir' => $this->input->post('Kasir'),
    'Sisa_Tunai' => $this->input->post('PaymentT'),
    'Sisa_Kredit' => $this->input->post('PaymentK'),
    'Sisa_Debit' => $this->input->post('PaymentD'),
    'status' => '3',
    'EditDate' => date('Y-m-d')
  );

  //Generate NoDokumen di trans_recepit_header
  $tgl=date('Y-m-d');
  $this->reservasi_model->locktables('counter,trans_receipt_header');
  $bulan = substr($tgl, 5, 2);
  $tahun = substr($tgl, 2, 2);
  if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
    $kd_no = $this->reservasi_model->getKodeBank($KdKasBank);
    $no = $this->get_no_counter( $kd_no->KdPenerimaan,"trans_receipt_header", "NoDokumen",$tahun,$bulan);
  }else{
    $new_no = $this->reservasi_model->getNewNo($tgl);
    $no = $new_no->NoReceipt;
  }



  $user = $this->session->userdata('username');
  $data_trans_receipt_header = array(
    'NoDokumen' => $no,
    'TglDokumen' => $mylib->ubah_tanggal($tanggal),
    'KdKasBank' => $KdKasBank,
    'Keterangan'=>'Uang Muka '.$NoDokumen,
    'JumlahReceipt' => $Dp,
    'TerimaDari' =>$Nama,
    'AddDate' => date('Y-m-d'),
    'AddUser'=> $user,
    'Jenis'=>1,
    'NoBukti'=>$no_bukti
  );

  $kd_no = $this->reservasi_model->getKodeBank($KdKasBank);
  $data_trans_receipt_detail = array(
    'NoDokumen' => $no,
    'TglDokumen' => date('Y-m-d'),
    'KdRekening' => $kd_no->KdRekening,
    'Jumlah'=>$Dp,
    'KdDepartemen'=>'00',
    'KdSubDivisi'=>$kd_no->KdSubDivisi,
    'Keterangan'=>'Uang Muka '.$NoDokumen,
    'Urutan'=>1,
    'AddDate' => date('Y-m-d'),
    'AddUser'=> $user,
    'NoBukti'=> $no_bukti,
    'Nama' =>$Nama,
    'Status' => 1
  );

  /*echo "<pre>";
  print_r($data_trans_receipt_header);
  echo "</pre>";

  echo "<pre>";
  print_r($data_trans_receipt_detail);
  echo "</pre>";die;*/

  $this->db->insert("trans_receipt_header",$data_trans_receipt_header);
  $this->db->insert("trans_receipt_detail",$data_trans_receipt_detail);
  $this->db->update('trans_reservasi', $data, array('NoDokumen' => $no));
  $this->cetakStruk($no);
  echo json_encode(array("status" => TRUE,"NoDokumen" =>$no));
}

private function _validate() {
  $data = array();
  $data['error_string'] = array();
  $data['inputerror'] = array();
  $data['status'] = TRUE;

  if ($this->input->post('rbtPembayaran') == '') {
    $data['inputerror'][] = 'Pembayaran';
    $data['error_string'][] = 'pembayran is required';
    $data['status'] = FALSE;
  }

  //        if ($this->input->post('lastName') == '') {
  //            $data['inputerror'][] = 'lastName';
  //            $data['error_string'][] = 'Last name is required';
  //            $data['status'] = FALSE;
  //        }

  if ($data['status'] === FALSE) {
    echo json_encode($data);
    exit();
  }
}

function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
{
  $query = "SELECT
  " . $table_name . "." . $col_primary . "
  FROM
  " . $table_name . "
  WHERE
  1
  AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

  ORDER BY
  " .$table_name . "." . $col_primary . " DESC
  LIMIT
  0,1
  ";
  //echo $query;
  $qry = mysql_query($query);
  $row = mysql_fetch_array($qry);
  $col_primary_ok = '';
  if(count($row)>0){
    list($col_primary_ok) = $row;
  }

  $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
  $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
  return $counter_fa;

}

function get_no_counter_voucher_beo( $kode,$table_name, $col_primary, $thn, $bln, $orderby )
{
  $query = "SELECT
  " . $table_name . "." . $col_primary . "
  FROM
  " . $table_name . "
  WHERE
  1
  AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
  ORDER BY
  " .$table_name . "." . $orderby . " DESC
  LIMIT
  0,1
  ";
  //echo $query;
  $qry = mysql_query($query);
  $row = mysql_fetch_array($qry);
  $col_primary_ok = '';
  if(count($row)>0){
    list($col_primary_ok) = $row;
  }
  $counter = (substr($col_primary_ok, 6, 4) * 1)+3;
  //$counter = ((substr($col_primary_ok, 6, 4) * 1)+1)+8;
  //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
  $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
  return $counter_fa;

}

function get_no_counter_voucher( $kode,$table_name, $col_primary, $thn,$bln )
{
  $query = "SELECT
  " . $table_name . "." . $col_primary . "
  FROM
  " . $table_name . "
  WHERE
  1
  AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
  ORDER BY
  " .$table_name . "." . $col_primary . " DESC
  LIMIT
  0,1
  ";
  //echo $query;
  $qry = mysql_query($query);
  $row = mysql_fetch_array($qry);
  $col_primary_ok = '';
  if(count($row)>0){
    list($col_primary_ok) = $row;
  }

  $counter = ((substr($col_primary_ok, 7, 4) * 1)+1)*3 + 2 * 3;
  //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
  $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
  return $counter_fa;

}

function cetakStruk($no) {
  //        $thn = $this->uri->segment(7);
  //        $bln = $this->uri->segment(8);
  //        $no = $this->uri->segment(9);
  //        $id = "BFM/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
  $data['store'] = $this->reservasi_model->aplikasi();
  $data['header'] = $this->reservasi_model->getHeader($no);
  $data['detail'] = $this->reservasi_model->getHeaderForPrint($no);

  if (!empty($data['header'])) {
    $this->load->view('bss/reservasi/cetak_strukreservasi', $data); // jika ada printernya
  }
}

function get_no_counter_booking( $kode,$table_name, $col_primary, $thn,$bln )
{
  $query = "SELECT
  " . $table_name . "." . $col_primary . "
  FROM
  " . $table_name . "
  WHERE
  1
  AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,11) = '" .$kode.$thn.$bln."'
  ORDER BY
  " .$table_name . "." . $col_primary . " DESC
  LIMIT
  0,1
  ";
  //echo $query;
  $qry = mysql_query($query);
  $row = mysql_fetch_array($qry);
  $col_primary_ok = '';
  if(count($row)>0){
    list($col_primary_ok) = $row;
  }

  $counter = (substr($col_primary_ok, 12, 4) * 1)+1;
  $counter_fa = $kode.sprintf($thn.$bln. sprintf("%04s", $counter));
  return $counter_fa;

}

function proses() {
  $this->load->library('telegramlib');
  $telegram = new telegramlib();

  $vtext = "
  <b>Test Aja</b>

  Hallo Muhammad Irfan";

  $keyboard[0][0] = array('text'=>'Setuju', 'callback_data' => 'Setujupos#'.$id);
  $keyboard[0][1] = array('text'=>'Tolak', 'callback_data' => 'Tolakpos#'.$id);

  $telegram->chat_id('300127392');
  $telegram->text($vtext);
  $telegram->keyboard($keyboard);
  $telegram->send_with_inline_keyboard();

}

}

?>
