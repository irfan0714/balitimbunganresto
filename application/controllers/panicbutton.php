<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class panicbutton extends authcontroller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model("globalmodel");
		// $this->load->model("profile_model", "profile");
	}


	//panicbutton
	function GetRevisi(){
	$result = $this->globalmodel->GetRevisi();
	echo json_encode($result);
	}

	function GetApproval(){
	$result = $this->globalmodel->GetApproval();
	echo json_encode($result);
	}

	function GetStatus(){
	$result = $this->globalmodel->GetStatus();
	echo json_encode($result);
	}

	function GetHistory(){
	$result = $this->globalmodel->GetHistory();
	echo json_encode($result);
	}

	function GetAll(){
	$result = $this->globalmodel->GetAll();
	echo json_encode($result);
	}

	function rejectreq(){
	$result = $this->globalmodel->rejectreq();
	echo json_encode($result);
	}

	function approvereq(){
	$result = $this->globalmodel->approvereq();
	echo json_encode($result);
	}

	function responreq(){
	$result = $this->globalmodel->responreq();
	echo json_encode($result);
	}

	function getDataRevisi(){
	$result = $this->globalmodel->getDataRevisi();
	echo json_encode($result);
	}

	function SaveRevisi(){
	$validator = array('success' => false, 'messages' => array());

	$req = $this->globalmodel->SaveRevisi();
	if($req == true){
		$validator['success'] = true;
		$validator['messages'] = 'Request Success! Check your Request Status. Thanks';
	}else {
		$validator['success'] = false;
		$validator['messages'] = 'Request Failed! Please Try Again';
	}

	echo json_encode($validator);
	}

	//panicbutton


}
?>
