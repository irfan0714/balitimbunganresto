<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
	$modul = "FA Ruangan";
    $list  = "npm_fa_ruangan.php";
    $htm   = "npm_fa_ruangan_form.php";
    $pk    = "ruangan_id";
    $title = "ruangan_name";
    
    if($ajax)
    {
        if($ajax=="ajax_cabang")
        {
            $v_cabang_id = $_GET["v_cabang_id"];
            
            $q = "
                    SELECT
                        depo.depo_id,    
                        depo.depo_name
                    FROM
                        depo
                    WHERE
                        1
                        AND depo.cabang_id = '".$v_cabang_id."' 
                    ORDER BY
                        depo.depo_name ASC
            ";
            $qry_depo = mysql_query($q);
            while($row_depo = mysql_fetch_array($qry_depo))
            {
                list($depo_id, $depo_name) = $row_depo;
                
                $arr_data["list_depo"][$depo_id] = $depo_id;
                $arr_data["depo_name"][$depo_id] = $depo_name;
            } 
            
            ?>
            <select class="form-control-new" name="v_depo_id" id="v_depo_id" style="width: 300px;">
                <option value="">-</option>
                <?php 
                    foreach($arr_data["list_depo"] as $depo_id=>$val)
                    {
                        $depo_name = $arr_data["depo_name"][$depo_id];
                        ?>
                        <option <?php if($data["depo_id"]==$depo_id) echo "selected='selected'"; ?> value="<?php echo $depo_id; ?>"><?php echo $depo_name; ?></option>
                        <?php
                    }    
                ?>
            </select>
            <?php
        }
      
        exit();
    }
    
	$q_cek = "
		SELECT
			uni.ruangan_id
		FROM
		(
			SELECT
				ruangan_id
			FROM
				fa_lokasi
			WHERE
				ruangan_id = '".$id."'
		) as uni
		LIMIT
			0,1
	";
	
	$qry_cek = mysql_query($q_cek);
	$jml_cek = mysql_num_rows($qry_cek);
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena ada relasi dengan FA LOKASI";
        }
        else
        {
	
			$q = "
				DELETE FROM
					fa_ruangan
				WHERE
					ruangan_id = '".$id."'
			";
			if(!mysql_query($q))
			{
				$msg = "Failed Delete ||".$q;
			}
			else
			{
				header("Location: ".$list.$link_back.$link_order_by);
			}  
		}
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
		$v_depo_id      = save_char($_POST["v_depo_id"]);
		$v_ruangan_name = save_char($_POST["v_ruangan_name"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        fa_ruangan
                    SET
                        depo_id = '".$v_depo_id."',
                        ruangan_name = '".$v_ruangan_name."'
                    WHERE
                        ruangan_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update ||".$q;
                }
                else
                {
                    $class_warning = "success";
                    $msg = "Berhasil menyimpan ".$v_ruangan_name."<br>";
                }
				
            }
            
        }
        else
        {   
            $id = get_counter_int("","fa_ruangan","ruangan_id",100);   
            
            $q = "
                    INSERT INTO
                        fa_ruangan
                    SET
                        ruangan_id = '".$id."',
                        depo_id = '".$v_depo_id."',
                        ruangan_name = '".$v_ruangan_name."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert ||".$q;
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            fa_ruangan.`ruangan_id`,
            depo.`depo_id`,
            depo.`depo_name`,
            hrd_cabang.`cabang_id`,
            hrd_cabang.`cabang_name`,
            fa_ruangan.`ruangan_name`
        FROM 
            fa_ruangan
            INNER JOIN depo ON
                fa_ruangan.depo_id = depo.depo_id
            INNER JOIN hrd_cabang ON
                depo.cabang_id = hrd_cabang.cabang_id
        WHERE
            1
            AND fa_ruangan.ruangan_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
        SELECT
            hrd_cabang.cabang_id,    
            hrd_cabang.cabang_name
        FROM
            hrd_cabang
        WHERE
            1
        ORDER BY
            hrd_cabang.cabang_name ASC
    ";
    $qry_cabang = mysql_query($q);
    while($row_cabang = mysql_fetch_array($qry_cabang))
    {
        list($cabang_id, $cabang_name) = $row_cabang;
        
        $arr_data["list_cabang"][$cabang_id] = $cabang_id;
        $arr_data["cabang_name"][$cabang_id] = $cabang_name;
    }
    
    $q = "
        SELECT
            depo.depo_id,    
            depo.depo_name
        FROM
            depo
        WHERE
            1
            AND depo.cabang_id = '".$data["cabang_id"]."'
        ORDER BY
            depo.depo_name ASC
    ";
    $qry_depo = mysql_query($q);
    while($row_depo = mysql_fetch_array($qry_depo))
    {
        list($depo_id, $depo_name) = $row_depo;
        
        $arr_data["list_depo"][$depo_id] = $depo_id;
        $arr_data["depo_name"][$depo_id] = $depo_name;
    }
     
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
	    function CallAjaxForm(vari,param1,param2,param3)
	    {
	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        if (param3 == undefined) param3 = '';
	        
	        document.getElementById('show_image_ajax_form').style.display = '';
	        
	        if (vari == 'ajax_cabang'){
	            var variabel;
	            
	            v_cabang_id = param1;
	            variabel = '&v_cabang_id='+v_cabang_id;
	            
	            xmlhttp.open('get', 'npm_fa_ruangan_form.php?ajax=ajax_cabang&'+variabel, true);
	            xmlhttp.onreadystatechange = function() {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)){
	                    document.getElementById('td_depo').innerHTML = xmlhttp.responseText;
	                    document.getElementById('show_image_ajax_form').style.display = 'none';
	                }
	                return false;
	            }
	            xmlhttp.send(null);
	        }
	    }

		function validasi()
		{
			if(document.getElementById("v_cabang_id").value=="")
			{
				alert("Cabang harus dipilih...");
				document.getElementById("v_cabang_id").focus();
				return false;
			}
	        else if(document.getElementById("v_depo_id").value*1==0)
	        {
	            alert("Depo harus dipilih...");
	            document.getElementById("v_depo_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_ruangan_name").value*1==0)
	        {
	            alert("Ruangan Name harus diisi...");
	            document.getElementById("v_ruangan_name").focus();
	            return false;
	        }
	        
		}
		
		function start_page()
		{
			document.getElementById("v_ruangan_name").focus();	
		}
	</script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    
			    	<tr>
                        <td class="title_table" width="150">Cabang</td>
                        <td> 
                            <select class="form-control-new" name="v_cabang_id" id="v_cabang_id" style="width: 300px;" onchange="CallAjaxForm('ajax_cabang', this.value)">
                                <option value="">-</option>
                                <?php 
                                    foreach($arr_data["list_cabang"] as $cabang_id=>$val)
                                    {
                                        $cabang_name = $arr_data["cabang_name"][$cabang_id];
                                        
                                        $cabang_name = str_replace("Cabang","",$cabang_name);
                                        ?>
                                        <option <?php if($data["cabang_id"]==$cabang_id) echo "selected='selected'"; ?> value="<?php echo $cabang_id; ?>"><?php echo $cabang_name; ?></option>
                                        <?php
                                    }    
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Depo</td>
                        <td id="td_depo"> 
                            <select class="form-control-new" name="v_depo_id" id="v_depo_id" style="width: 300px;">
                                <option value="">-</option>
                                <?php 
                                    foreach($arr_data["list_depo"] as $depo_id=>$val)
                                    {
                                        $depo_name = $arr_data["depo_name"][$depo_id];
                                        ?>
                                        <option <?php if($data["depo_id"]==$depo_id) echo "selected='selected'"; ?> value="<?php echo $depo_id; ?>"><?php echo $depo_name; ?></option>
                                        <?php
                                    }    
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Ruangan</td>
                        <td> 
                            <input type="text" class="form-control-new" name="v_ruangan_name" id="v_ruangan_name" value="<?php echo $data["ruangan_name"]; ?>" maxlength="100" style="width: 300px;" /> 
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
			    </table>			    
			    </form>		         
			</div>
		</div>
		
<?php include("footer.php"); ?>