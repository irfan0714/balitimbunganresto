<?php 
    include("header.php"); 
    
    if(!isset($_GET["sid"])){ $sid = isset($_GET["sid"]); } else { $sid = $_GET["sid"]; }
    if(!isset($_GET["table"])){ $table = isset($_GET["table"]); } else { $table = $_GET["table"]; }
    
    $sql    = "SELECT * FROM ".$db["master"].".".$table." WHERE sid = '".$sid."' LIMIT 0,1 ";
    $query  = mysql_query($sql) or die(mysql_error());      
    $result = mysql_fetch_array($query);
    
    if($result['photo_ext']=="jpg" || $result['photo_ext']=="jpeg")
    {
        $image_type = "image/jpeg";
    }
    else if($result['photo_ext']=="gif")
    {
        $image_type = "image/gif";    
    }
    else if($result['photo_ext']=="png")
    {
        $image_type = "image/png";    
    }

	// membuat image dari string database
	$img = imagecreatefromstring($result['photo_data']);

	$src_width = imageSX($img);
	$src_height = imageSY($img);
			
	if($src_width == $src_height)
	{
		$thumb_width = $src_width;
		$thumb_height= $src_height;
	}
	elseif($src_width < $src_height)
	{
		$thumb_width = $src_width;
		$thumb_height= $src_width;
	}
	elseif($src_width > $src_height)
	{
		$thumb_width = $src_height;
		$thumb_height= $src_height;
	}
	else
	{
		$thumb_width = 100;
		$thumb_height= 100;
	}
	
	$original_aspect = $src_width / $src_height;
	$thumb_aspect = $thumb_width / $thumb_height;

	if ( $original_aspect >= $thumb_aspect ) {

	 // If image is wider than thumbnail (in aspect ratio sense)
	 $new_height = $thumb_height;
	 $new_width = $src_width / ($src_height / $thumb_height);

	}
	else {
	 // If the thumbnail is wider than the image
	 $new_width = $thumb_width;
	 $new_height = $src_height / ($src_width / $thumb_width);
	}

	$im = imagecreatetruecolor( $thumb_width, $thumb_height );
	
	// Resize and crop
	imagecopyresampled($im,
	     $img,
	     0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
	     0 - ($new_height - $thumb_height) / 2, // Center the image vertically
	     0, 0,
	     $new_width, $new_height,
	     $src_width, $src_height);
	     
    
    header("Content-type:".$image_type);
    header('Content-Disposition: inline; filename="'.$result['remarks'].'"');
    //echo $result['photo_data'];
    
    if($result['photo_ext']=="jpg" || $result['photo_ext']=="jpeg")
    {
        $view_img = imagejpeg($im);
    }
    else if($result['photo_ext']=="gif")
    {
        $view_img = imagejpeg($im);
    }
    else if($result['photo_ext']=="png")
    {
        $view_img = imagejpeg($im);
    }
    
    echo $view_img;
    
?>