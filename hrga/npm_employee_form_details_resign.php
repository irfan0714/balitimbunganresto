<div class="col-md-12">
<table class="table table-bordered responsive">
	<thead>
		<tr>
		    <th width="30"><center>No</center></th>
		    <th><center>Tanggal</center></th>
		    <th><center>Keterangan</center></th>
		    <th><center>Simpan</center></th>
		    <th><center>Hapus</center></th>
		</tr>
	</thead>
	<tbody>
	<?php
    $no = 1; 
    for($i=1;$i<=1;$i++)
    {
        ?>
            <tr>
                <td>
                    <input type="hidden" name="no_resign[]" value="<?php echo $no; ?>">
                    <input type="hidden" name="v_sid_resign_<?php echo $no; ?>" id="v_sid_resign_<?php echo $no; ?>" value="0">
                    
                    <input type="hidden" name="v_resign_date_old_<?php echo $no; ?>" id="v_resign_date_old_<?php echo $no; ?>" value="">
                    <input type="hidden" name="v_remarks_resign_old_<?php echo $no; ?>" id="v_remarks_resign_old_<?php echo $no; ?>" value="">
                </td>
                <td align="center">
                    <input type="text" class="form-control-new datepicker" value="" name="v_resign_date_<?php echo $no; ?>" id="v_resign_date_<?php echo $no; ?>" maxlength="10" size="12">
                </td>
                <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_remarks_resign_<?php echo $no; ?>" id="v_remarks_resign_<?php echo $no; ?>"></td>
                <td align="center">
	            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_resign" id="btn_save_ajax_resign" value="Save" onClick="CallAjaxForm('save_ajax_resign','<?php echo $no; ?>')">
						<i class="entypo-floppy"></i>
					</button>
                </td>
                <td>&nbsp;</td>
            </tr>
        <?php
        $no++;
    }
 
    $q = "
        SELECT
            ".$db["master"].".employee_resign.*
        FROM
            ".$db["master"].".employee_resign
        WHERE
            ".$db["master"].".employee_resign.employee_id = '".$id."'
        ORDER BY
            ".$db["master"].".employee_resign.resign_date ASC
    ";
    $qry = mysql_query($q);
    $no_view = 1;
    while($r = mysql_fetch_object($qry))
    { 
		?>
		<tr>
		    <td>
		        <?php echo $no_view; ?>
		        <input type="hidden" name="no_resign[]" value="<?php echo $no; ?>">
		        <input type="hidden" name="v_sid_resign_<?php echo $no; ?>" id="v_sid_resign_<?php echo $no; ?>" value="<?php echo $r->sid; ?>">
		        
		        <input type="hidden" name="v_resign_date_old_<?php echo $no; ?>" id="v_resign_date_old_<?php echo $no; ?>" value="<?php echo format_show_date($r->resign_date); ?>">
		        <input type="hidden" name="v_remarks_resign_old_<?php echo $no; ?>" id="v_remarks_resign_old_<?php echo $no; ?>" value="<?php echo $r->remarks; ?>">
		    </td>
		    <td align="center">
		        <input type="text" class="form-control-new datepicker" value="<?php echo format_show_date($r->resign_date); ?>" name="v_resign_date_<?php echo $no; ?>" id="v_resign_date_<?php echo $no; ?>" maxlength="10" size="12">
		    </td>
		    <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_remarks_resign_<?php echo $no; ?>" id="v_remarks_resign_<?php echo $no; ?>" value="<?php echo $r->remarks; ?>"></td>
		    <td align="center">
            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_resign" id="btn_save_ajax_resign" value="Save" onClick="CallAjaxForm('save_ajax_resign','<?php echo $no; ?>')">
					<i class="entypo-floppy"></i>
				</button>
			</td>
		   	<td align="center"><input type="checkbox" name="del_resign[]" value="<?php echo $r->sid; ?>"></td>
		</tr>
		<?php 
		
        $no++;
        $no_view++;
    }
    
    if($id!="")
    {
	?>
		<tr>     
		   <td colspan="4">&nbsp;</td>
		   <td align="center">
		   	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_del_resign" id="btn_del_resign" value="Hapus">Hapus<i class="entypo-trash"></i></button>
		   </td>
		</tr>
	<?php 
	}
    ?>
    </tbody>
</table>
</div>