<?php 

    include("header.php");      
	         
    if(!isset($_GET["search_status"]))
	{ 
		$search_status = isset($_GET["search_status"]); 
	} 
	else 
	{ 
		$search_status = $_GET["search_status"]; 
	}    
    if(!isset($_GET["search_company_id"]))
	{ 
		$search_company_id = isset($_GET["search_company_id"]); 
	}
	else 
	{ 
		$search_company_id = $_GET["search_company_id"]; 
	}
    if(!isset($_GET["search_cabang_id"]))
	{ 
		$search_cabang_id = isset($_GET["search_cabang_id"]); 
	} 
	else 
	{ 
		$search_cabang_id = $_GET["search_cabang_id"]; 
	}
    
    if(!isset($_GET["v_keyword"]))
	{ 
		$v_keyword = isset($_GET["v_keyword"]); 
	} 
	else 
	{ 
		$v_keyword = $_GET["v_keyword"];
	}
    if(!isset($_GET["p"]))
	{ 
		$p = isset($_GET["p"]);
	} 
	else 
	{ 
	$p = $_GET["p"]; 
	}
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    //$v_keyword = save_char($v_keyword);
    
    $link_adjust    = "";
    $link_adjust   .= "?v_keyword=".$v_keyword;
    $link_adjust   .= "&p=".$p;
    $link_adjust   .= "&search_company_id=".$search_company_id;
    $link_adjust   .= "&search_cabang_id=".$search_cabang_id;
    $link_adjust   .= "&search_status=".$search_status;
    
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $icon_type_change = "entypo-up-dir";
    $order_type_change = "asc";
    if($order_type=="asc")
    {
        $order_type_change = "desc";
    	$icon_type_change = "entypo-down-dir";
    }
    
    $order_by_content = "";
    if($order_by!="")
    {
        $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
    }
    
	$modul = "Karyawan";
    $list  = "npm_employee.php";
    $htm   = "npm_employee_form.php";
    $pk    = "employee_id";
    $title = "employee_name";
    
    $v_currdate = date("d/m/Y");
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="get">
		<div class="row">
			<div class="col-md-10">
			
				<select class="form-control-new" name="search_status" id="search_status">
                    <option value="">Status</option>
                    <option <?php if($search_status=="join") echo "selected='selected'"; ?> value="join">Active</option>
                    <option <?php if($search_status=="resign") echo "selected='selected'"; ?> value="resign">Resign</option>
                 </select>
                 
                &nbsp;   
                <select class="form-control-new" name="search_company_id" id="search_company_id">
                    <option value="">Perusahaan</option>
                    <?php 
                        $q = "
                            SELECT
                                company.company_id,
                                company.company_name,
                                company.company_initial
                            FROM
                                company
                            WHERE
                                1
                            ORDER BY
                                company.company_initial ASC
                        ";
                        $qry_comp = mysql_query($q);
                        while($row_comp = mysql_fetch_array($qry_comp))
                        {
                            $selected = "";
                            if($search_company_id==$row_comp["company_id"])      
                            {
                                $selected = "selected='selected'";
                            }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_comp["company_id"]; ?>"><?php echo $row_comp["company_initial"]; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
                <select class="form-control-new" name="search_cabang_id" id="search_cabang_id">
                    <option value="">Cabang</option>
                    <?php 
                        $q = "
                                SELECT
                                    hrd_cabang.cabang_id,
                                    hrd_cabang.cabang_name
                                FROM
                                    hrd_cabang
                                WHERE
                                    1
                                ORDER BY
                                    hrd_cabang.cabang_name ASC
                        ";
                        $qry_cab = mysql_query($q);
                        while($row_cab = mysql_fetch_array($qry_cab))
                        {
                            $selected = "";
                            if($search_cabang_id==$row_cab["cabang_id"])      
                            {
                                $selected = "selected='selected'";
                            }
                            
                            $cabang_name = str_replace("Cabang","",$row_cab["cabang_name"])
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_cab["cabang_id"]; ?>"><?php echo $cabang_name; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
				Search&nbsp;
				<input type="text" size="30" maxlength="30" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>">
				&nbsp;
			</div>
			
			<div class="col-md-2" align="right">
				<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100),get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>')">Tambah<i class="entypo-plus"></i></button>
			</div>
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="10"><center>No</th>     
                        <th width="100"><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_nik&order_type=".$order_type_change; ?>" class="link_menu">No. Induk Karyawan<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th width="80"><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_code_hrd&order_type=".$order_type_change; ?>" class="link_menu">Kode HRD<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_name&order_type=".$order_type_change; ?>" class="link_menu">Nama<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        
                        <th>Perusahaan</th>
                        <th>Cabang</th>
                        <th><center>Depo</th>
                        <th><center>Divisi</th>
                        <th><center>Jabatan</th>
                        <th><center>Navigasi</th>
					</tr>
				</thead>
				<tbody style="color: black;">
					
					<?php    
                        $keyWord = trim($v_keyword);
                        
                        if($keyWord == '' && $search_company_id=="" && $search_cabang_id=="" && $search_status=="")
                        {    
                        	// cari data status dan posisi
                            {
                                $q = "
                                        SELECT 
                                            tbl_fixed.employee_id,
                                            tbl_fixed.v_status,
                                            tbl_fixed.v_date
                                        FROM
                                        (
                                            SELECT
                                                uni.employee_id,
                                                uni.v_status,
                                                uni.v_date
                                            FROM
                                            (
                                                SELECT
                                                    employee_join.employee_id,
                                                    'join' as v_status,
                                                    employee_join.join_date as v_date
                                                FROM
                                                    employee_join
                                                WHERE
                                                    1
                                                    
                                                UNION ALL
                                                
                                                SELECT
                                                    employee_resign.employee_id,
                                                    'resign' as v_status,
                                                    employee_resign.resign_date as v_date
                                                FROM
                                                    employee_resign
                                                WHERE
                                                    1
                                            ) as uni
                                            
                                            WHERE
                                                1
                                            GROUP BY
                                                uni.employee_id,
                                                uni.v_date
                                            ORDER BY
                                                uni.employee_id DESC,
                                                uni.v_date DESC 
                                        )AS tbl_fixed
                                        WHERE
                                            1
                                        GROUP BY 
                                            tbl_fixed.employee_id 
                                ";       
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list($employee_id, $v_status, $v_date) = $row;   
                                    
                                    $arr_data["status_employee"][$employee_id] = $v_status;
                                    $arr_data["status_date"][$employee_id] = $v_date;
                                }
                                 
                                // Selecet field perusahaan, cabang, depo, divisi, jabatan
                                $q = "
                                       SELECT
                                          uni.employee_id,
                                          uni.company_id,
                                          uni.company_name,
                                          uni.company_initial,
                                          uni.cabang_id,
                                          uni.cabang_name,
                                          uni.depo_id,
                                          uni.depo_name,
                                          uni.divisi_id,
                                          uni.divisi_name,
                                          uni.departemen_id,
                                          uni.departemen_name,
                                          uni.jabatan_id,
                                          uni.jabatan_name
                                        FROM
                                        (
                                            SELECT 
                                              employee_position.employee_id,
                                              company.company_id,
                                              company.company_name,
                                              company.company_initial,
                                              hrd_cabang.cabang_id,
                                              hrd_cabang.cabang_name,
                                              depo.depo_id,
                                              depo.depo_name,
                                              hrd_divisi.divisi_id,
                                              hrd_divisi.divisi_name,
                                              hrd_departemen.departemen_id,
                                              hrd_departemen.departemen_name,
                                              jabatan.jabatan_id,
                                              jabatan.jabatan_name 
                                            FROM
                                              employee_position 
                                              INNER JOIN company 
                                                ON employee_position.company_id = company.company_id 
                                              INNER JOIN hrd_divisi 
                                                ON employee_position.divisi_id = hrd_divisi.divisi_id 
                                              INNER JOIN hrd_departemen 
                                                ON employee_position.departemen_id = hrd_departemen.departemen_id 
                                              INNER JOIN depo 
                                                ON employee_position.depo_id = depo.depo_id 
                                              INNER JOIN hrd_cabang 
                                                ON depo.cabang_id = hrd_cabang.cabang_id
                                              INNER JOIN jabatan 
                                                ON employee_position.jabatan_id = jabatan.jabatan_id 
                                            WHERE 
                                              1
                                            ORDER BY
                                              employee_position.`start_date` DESC
                                        )AS uni
                                        WHERE
                                            1
                                        GROUP BY
                                            uni.employee_id 
                                ";              
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list(
                                        $employee_id,
                                        $company_id,
                                        $company_name,
                                        $company_initial,
                                        $cabang_id,
                                        $cabang_name,
                                        $depo_id,
                                        $depo_name,
                                        $divisi_id,
                                        $divisi_name,
                                        $departemen_id,
                                        $departemen_name,
                                        $jabatan_id,
                                        $jabatan_name
                                    ) = $row;
                                    
                                    $arr_data["list_employee_id"][$employee_id] = $employee_id;
                                    $arr_data["company_id"][$employee_id] = $company_id;
                                    $arr_data["company_name"][$employee_id] = $company_name;
                                    $arr_data["company_initial"][$employee_id] = $company_initial;
                                    $arr_data["cabang_id"][$employee_id] = $cabang_id;
                                    $arr_data["cabang_name"][$employee_id] = $cabang_name;
                                    $arr_data["depo_id"][$employee_id] = $depo_id;
                                    $arr_data["depo_name"][$employee_id] = $depo_name;
                                    $arr_data["divisi_id"][$employee_id] = $divisi_id;
                                    $arr_data["divisi_name"][$employee_id] = $divisi_name;
                                    $arr_data["departemen_id"][$employee_id] = $departemen_id;
                                    $arr_data["departemen_name"][$employee_id] = $departemen_name;
                                    $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
                                    $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
                                } 
                            }
                            
                            $sql = "
                                    SELECT 
                                        employee.employee_id,
                                        employee.employee_nik,
                                        employee.employee_code_hrd,
                                        employee.employee_name
                                    FROM 
                                        employee
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        employee.employee_name ASC,
                                        employee.employee_id ASC,
                                        employee.employee_nik ASC
                            ";                    
                            $query = mysql_query($sql);
                            $max = ceil(mysql_num_rows($query)/$jml_page);
                            $s = $jml_page * $p;
                            $sql = "               
                                        SELECT 
                                            employee.employee_id,
                                            employee.employee_nik,
                                            employee.employee_code_hrd,
                                            employee.employee_name
                                        FROM 
                                            employee
                                        WHERE
                                            1
                                        ORDER BY
                                            ".$order_by_content." 
                                            employee.employee_name ASC,
                                            employee.employee_id ASC,
                                            employee.employee_nik ASC
                                        LIMIT ".$s.", ".$jml_page." 
                                    ";         
                        }
                        else
                        {
                            $where_company_id = "";
                            if($search_company_id!="")
                            {
                                $where_company_id = " AND uni.company_id = '".$search_company_id."' ";    
                            }
                            
                            $where_cabang_id = "";
                            if($search_cabang_id!="")
                            {
                                $where_cabang_id = " AND uni.cabang_id = '".$search_cabang_id."' ";    
                            }
                                           ;
                            $where_status = "";
                            if($search_status!="")
                            {
                                $where_status = " AND uni.v_status = '".$search_status."'  ";
                            }
                            
                            unset($arr_keyword);
                            $arr_keyword[0] = "employee.employee_id";
							$arr_keyword[1] = "employee.employee_name";
							$arr_keyword[2] = "employee.employee_nik";
							$arr_keyword[3] = "employee.employee_code_hrd";
                            $arr_keyword[4] = "employee.email";
                            $arr_keyword[5] = "employee.no_ktp";
                            $arr_keyword[6] = "employee.username";
                            $arr_keyword[7] = "employee.no_kartu_keluarga";
                            $arr_keyword[8] = "employee.nama_ibu_kandung";
                            
							$search_keyword = search_keyword($v_keyword, $arr_keyword);
							$where = $search_keyword;
                                                                        
                            // cari data status dan posisi
                            {
                                $q = "
                                        SELECT
                                            uni.employee_id,
                                            uni.v_status,
                                            uni.v_date
                                        FROM
                                        (
                                            SELECT 
                                                tbl_fixed.employee_id,
                                                tbl_fixed.v_status,
                                                tbl_fixed.v_date
                                            FROM
                                            (
                                                SELECT
                                                    uni.employee_id,
                                                    uni.v_status,
                                                    uni.v_date
                                                FROM
                                                (
                                                    SELECT
                                                        employee_join.employee_id,
                                                        'join' as v_status,
                                                        employee_join.join_date as v_date
                                                    FROM
                                                        employee_join
                                                    WHERE
                                                        1
                                                        
                                                    UNION ALL
                                                    
                                                    SELECT
                                                        employee_resign.employee_id,
                                                        'resign' as v_status,
                                                        employee_resign.resign_date as v_date
                                                    FROM
                                                        employee_resign
                                                    WHERE
                                                        1
                                                ) as uni
                                                
                                                WHERE
                                                    1
                                                GROUP BY
                                                    uni.employee_id,
                                                    uni.v_date
                                                ORDER BY
                                                    uni.employee_id DESC,
                                                    uni.v_date DESC 
                                            )AS tbl_fixed
                                            WHERE
                                                1
                                            GROUP BY 
                                                tbl_fixed.employee_id 
                                        )AS uni
                                        WHERE
                                            1
                                            ".$where_status."
                                ";
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list($employee_id, $v_status, $v_date) = $row;
                                    
                                    $arr_data["status_employee"][$employee_id] = $v_status;
                                    $arr_data["status_date"][$employee_id] = $v_date;
                                    
                                    $arr_data["list_status"][$employee_id] = $employee_id;
                                }
                                
                                $where_status_emp = "";
                                foreach($arr_data["list_status"] as $employee_id=>$val)
                                {
                                    if($where_status_emp=="")    
                                    {
                                        $where_status_emp .= "AND ( employee.employee_id = '".$employee_id."' ";
                                    }
                                    else
                                    {
                                        $where_status_emp .= "OR employee.employee_id = '".$employee_id."' ";
                                    }
                                }
                                if($where_status_emp)
                                {
                                    $where_status_emp .= ")";
                                }
                                
                                
                                $q = "
                                        SELECT
                                          uni.employee_id,
                                          uni.company_id,
                                          uni.company_name,
                                          uni.company_initial,
                                          uni.cabang_id,
                                          uni.cabang_name,
                                          uni.depo_id,
                                          uni.depo_name,
                                          uni.divisi_id,
                                          uni.divisi_name,
                                          uni.departemen_id,
                                          uni.departemen_name,
                                          uni.jabatan_id,
                                          uni.jabatan_name
                                        FROM
                                        (
                                            SELECT 
                                              employee_position.employee_id,
                                              company.company_id,
                                              company.company_name,
                                              company.company_initial,
                                              hrd_cabang.cabang_id,
                                              hrd_cabang.cabang_name,
                                              depo.depo_id,
                                              depo.depo_name,
                                              hrd_divisi.divisi_id,
                                              hrd_divisi.divisi_name,
                                              hrd_departemen.departemen_id,
                                              hrd_departemen.departemen_name,
                                              jabatan.jabatan_id,
                                              jabatan.jabatan_name 
                                            FROM
                                              employee_position 
                                              INNER JOIN company 
                                                ON employee_position.company_id = company.company_id 
                                              INNER JOIN hrd_divisi 
                                                ON employee_position.divisi_id = hrd_divisi.divisi_id 
                                              INNER JOIN hrd_departemen 
                                                ON employee_position.departemen_id = hrd_departemen.departemen_id 
                                              INNER JOIN depo 
                                                ON employee_position.depo_id = depo.depo_id 
                                              INNER JOIN hrd_cabang 
                                                ON depo.cabang_id = hrd_cabang.cabang_id
                                              INNER JOIN jabatan 
                                                ON employee_position.jabatan_id = jabatan.jabatan_id 
                                            WHERE 
                                              1
                                            ORDER BY
                                              employee_position.`start_date` DESC
                                        )AS uni
                                        WHERE
                                            1
                                        GROUP BY
                                            uni.employee_id 
                                ";    
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list(
                                        $employee_id,
                                        $company_id,
                                        $company_name,
                                        $company_initial,
                                        $cabang_id,
                                        $cabang_name,
                                        $depo_id,
                                        $depo_name,
                                        $divisi_id,
                                        $divisi_name,
                                        $departemen_id,
                                        $departemen_name,
                                        $jabatan_id,
                                        $jabatan_name
                                    ) = $row;
                                    
                                    $arr_data["list_employee_id"][$employee_id] = $employee_id;
                                    $arr_data["company_id"][$employee_id] = $company_id;
                                    $arr_data["company_name"][$employee_id] = $company_name;
                                    $arr_data["company_initial"][$employee_id] = $company_initial;
                                    $arr_data["cabang_id"][$employee_id] = $cabang_id;
                                    $arr_data["cabang_name"][$employee_id] = $cabang_name;
                                    $arr_data["depo_id"][$employee_id] = $depo_id;
                                    $arr_data["depo_name"][$employee_id] = $depo_name;
                                    $arr_data["divisi_id"][$employee_id] = $divisi_id;
                                    $arr_data["divisi_name"][$employee_id] = $divisi_name;
                                    $arr_data["departemen_id"][$employee_id] = $departemen_id;
                                    $arr_data["departemen_name"][$employee_id] = $departemen_name;
                                    $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
                                    $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
                                    
                                    $arr_data["list_posisi"][$employee_id] = $employee_id;
                                } 
                            } 
                            
                            $where_useradmin_emp = where_array($arr_data["list_employee_id"], "employee.employee_id", "in");
							
                            $where_posisi_emp = "";
                            foreach($arr_data["list_posisi"] as $employee_id=>$val)
                            {
                                if($where_posisi_emp=="")    
                                {
                                    $where_posisi_emp .= "AND ( employee.employee_id = '".$employee_id."' ";
                                }
                                else
                                {
                                    $where_posisi_emp .= "OR employee.employee_id = '".$employee_id."' ";
                                }
                            }
                            if($where_posisi_emp)
                            {
                                $where_posisi_emp .= ")";
                            }
                            
                            if($search_company_id=="" && $search_cabang_id=="")
                            {
                                $where_posisi_emp = "";
                            }
                            
                            if($search_status=="")
                            {
                                $where_status_emp = "";
                            }
                            
                            $flag=0;
                            for($i=0; $i < strlen($keyWord); $i++)
                            {
                                if($keyWord[$i] == '\'') $flag++;
                                if($keyWord[$i] == '<') $flag++;
                                if($keyWord[$i] == '>') $flag++;
                            }
                            
                            if($flag==0)
                            {
                                $sql = "
                                            SELECT 
                                                employee.employee_id,
                                                employee.employee_nik,
                                                employee.employee_code_hrd,
                                                employee.employee_name
                                            FROM 
                                                employee
                                            WHERE
                                                1
                                                ".$where."
                                                ".$where_status_emp."
                                                ".$where_posisi_emp."
                                                ".$where_useradmin_emp."
                                            ORDER BY
                                                ".$order_by_content." 
                                                employee.employee_name ASC,
                                                employee.employee_id ASC,
                                                employee.employee_nik ASC
                                       ";
                                $query = mysql_query($sql);
                                $max = ceil(mysql_num_rows($query)/$jml_page);
                                $s = $jml_page * $p;
                                $sql = "
                                            SELECT 
                                                employee.employee_id,
                                                employee.employee_nik,
                                                employee.employee_code_hrd,
                                                employee.employee_name
                                            FROM 
                                                employee
                                            WHERE
                                                1
                                                ".$where."
                                                ".$where_status_emp."
                                                ".$where_posisi_emp."
                                                ".$where_useradmin_emp."
                                            ORDER BY
                                                ".$order_by_content." 
                                                employee.employee_name ASC,
                                                employee.employee_id ASC,
                                                employee.employee_nik ASC
                                            LIMIT ".$s.", ".$jml_page." 
                                      ";
                            }
                            else
                            {
                                $msg = "Your input are not allowed!";
                            }
                        }
                        $query = mysql_query($sql);
                                $sv = 0;
                                $i=0+($jml_page*$p);
                        if(!$row = mysql_num_rows($query))
                        {
                         echo "<tr>";
                         echo "<td align=\"center\" colspan=\"100%\">";
                         echo "No Data";
                         echo "</td>";
                         echo "</tr>";
                        }
                        else
                        {
                            while ($row = mysql_fetch_array($query))
                            {
                                list($employee_id, $employee_nik, $employee_code_hrd, $employee_name) = $row; 
							
                                $sv++;
                                $i++;
                                
                                $status_employee = $arr_data["status_employee"][$employee_id];
                                $status_date = $arr_data["status_date"][$employee_id];
                                
                                $company_id = $arr_data["company_id"][$employee_id];
                                $company_name = $arr_data["company_name"][$employee_id];
                                $company_initial = $arr_data["company_initial"][$employee_id];
                                $cabang_id = $arr_data["cabang_id"][$employee_id];
                                $cabang_name = $arr_data["cabang_name"][$employee_id];
                                $depo_id = $arr_data["depo_id"][$employee_id];
                                $depo_name = $arr_data["depo_name"][$employee_id];
                                $divisi_id = $arr_data["divisi_id"][$employee_id];
                                $divisi_name = $arr_data["divisi_name"][$employee_id];
                                $departemen_id = $arr_data["departemen_id"][$employee_id];
                                $departemen_name = $arr_data["departemen_name"][$employee_id];
                                $jabatan_id = $arr_data["jabatan_id"][$employee_id];
                                $jabatan_name = $arr_data["jabatan_name"][$employee_id];
                                
                                $employee_id_useradmin = $arr_data["list_employee_id"][$employee_id];
                                
                                
                                if($status_employee=="resign")
                                {
                                    $bgcolor_status = "background:#FFCCFF;";
                                }
                                else
                                {
                                    $bgcolor_status = "";    
                                }
								
								$bgcolor = "";
                                if($i%2==0)
                                {
                                    $bgcolor = "background:rgb(247, 247, 247) none repeat scroll 0% 0%;";
                                }
								
                     ?>
                            <tr title="<?php echo $row[$title]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $i; ?></td>
                                <td align="center" style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $employee_nik; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $employee_code_hrd; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $employee_name." (".$employee_id.")"; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $company_initial; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $cabang_name; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $depo_name; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $divisi_name; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $jabatan_name; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" align="center">
                                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')">
										<i class="entypo-pencil"></i>
									</button>
									
									<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=delete&id=<?php echo $row[$pk]; ?>')">
										<i class="entypo-trash"></i>
									</button>
                                </td>
                            </tr>
                     <?php
					 			  
                            }   
                        }
                     ?>
                     
				</tbody>
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>

<?php include("footer.php"); ?>                                   
                                     