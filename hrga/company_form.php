<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Company";
    $list  = "company.php";
    $htm   = "company_form.php";
    $pk    = "company_id";
    $title = "company_name";
    
    $q_cek = "
        SELECT
            uni.company_id
        FROM
        (
            SELECT
                company_id
            FROM
                employee_position
            WHERE
                company_id = '".$id."'
        ) as uni
        LIMIT
            0,1
    ";
    
    $qry_cek = mysql_query($q_cek);
    $jml_cek = mysql_num_rows($qry_cek);
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena ada relasi dengan Posisi Karyawan";
        }
        else
        {
    
            $q = "
                    DELETE FROM
                        company
                    WHERE
                        company_id = '".$id."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Delete";
            }
            else
            {
                header("Location: ".$list.$link_back.$link_order_by);
            }  
        }
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
		$v_company_name    = save_char($_POST["v_company_name"]);
		$v_company_initial = save_char($_POST["v_company_initial"]);
		$v_address         = save_char($_POST["v_address"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                        UPDATE
                            company
                        SET
                            company_name = '".$v_company_name."',
                            company_initial = '".$v_company_initial."',  
                            address = '".$v_address."',    
                            edited = '".ffclock()."'
                        WHERE
                            company_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "<strong>Failed Update!</strong> Gagal Query";
                }
                else
                {
                    $class_warning = "success";
                    $msg = "<strong>Berhasil menyimpan!</strong>";
                }
            }
        }
        else
        {   
            $id = get_counter_int("","company","company_id",100);   
            
            $q = "
                    INSERT INTO
                        company
                    SET
                        company_id = '".$id."',
                        company_name = '".$v_company_name."',
                        company_initial = '".$v_company_initial."',   
                        address = '".$v_address."',  
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert!";
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            company.*
        FROM 
            company
        WHERE
            1
            AND company.company_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function validasi()
    {
        if(document.getElementById("v_company_name").value=="")
        {
            alert("Nama harus diisi");
            document.getElementById("v_company_name").focus();
            return false;
        }
    }
    
    function start_page()
    {
        document.getElementById("v_company_name").focus();    
    }
    </script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    	
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
        
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
			    <form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			                            
			        <tr>
			            <td class="title_table" width="150">Nama</td>
			            <td> 
			                <input type="text" class="form-control-new" value="<?php echo $data["company_name"]; ?>" name="v_company_name" id="v_company_name" maxlength="255" size="50">
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table">Inisial</td>
			            <td> 
			                <input type="text" class="form-control-new" value="<?php echo $data["company_initial"]; ?>" name="v_company_initial" id="v_company_initial" maxlength="10" size="5">
			            </td>
			        </tr>
			            
			        <tr>
			            <td class="title_table">Address</td>
			            <td>
			                <textarea id="v_address" class="form-control-new" name="v_address" cols="80" ><?php echo $data["address"];?></textarea>
			            </td>
			        </tr>  
			        
			        <tr>
			            <td>&nbsp;</td>
			            <td>
			                <?php 
			                    if($show_btn=="Delete")
			                    {
				                ?>
				                	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
				                <?php 
				              	}
			                    else
			                    {
			                ?>
			                		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
			                <?php 
			                    }
			                ?>
			                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
			            </td>
			        </tr>
			        
			    </table>
			    </form> 
			    
		        <?php
		            if($id!="")
		            {
		         ?>
		         	<ol class="breadcrumb">
						<li><strong><i class="entypo-vcard"></i>Information data</strong></li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		         ?>
    			
    			
    		</div>
    	</div>

<?php include("footer.php"); ?>