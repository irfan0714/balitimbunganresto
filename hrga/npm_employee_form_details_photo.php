<div class="col-md-12">
<table class="table table-bordered responsive">
	<thead>
		<tr>
			<th width="30"><center>No</center></th>
		    <th><center>Dokumen</center></th>
		    <th><center>Tipe</center></th>               
		    <th><center>Keterangan</center></th>     
		    <th><center>Download</center></th>
		    <th><center>Simpan</center></th>
		    <th><center>Hapus</center></th>
		</tr>
	</thead>
	<tbody>

	<?php
    $no = 1; 
    for($i=1;$i<=1;$i++)
    {
        ?>
            <tr>
                <td>
                    <input type="hidden" name="no_photo[]" value="<?php echo $no; ?>">
                    <input type="hidden" name="v_sid_photo_<?php echo $no; ?>" id="v_sid_photo_<?php echo $no; ?>" value="0">
                </td>
                <td>
                	<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
							<img src="assets/images/200x150.png" alt="No picture">
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
						<div>
							<span class="btn btn-white btn-file btn-sm">
								<span class="fileinput-new">Select image</span>
								<span class="fileinput-exists">Change</span>
								<input type="file" name="v_photo_data_<?php echo $no; ?>" id="v_photo_data_<?php echo $no; ?>" accept="image/*">
							</span>
							<a href="#" class="btn btn-orange fileinput-exists btn-sm" data-dismiss="fileinput">Remove</a>
						</div>
					</div>
				</td>
                <td>
                    <select name="v_photo_type_<?php echo $no; ?>" id="v_photo_type_<?php echo $no; ?>" class="form-control-new" style="width:80px;">
                        <?php 
                            $q = "
                                    SELECT
                                        ".$db["master"].".kombo.isi
                                    FROM
                                        ".$db["master"].".kombo
                                    WHERE
                                        1
                                        AND ".$db["master"].".kombo.type = 'photo_type'
                                    ORDER BY
                                        ".$db["master"].".kombo.sort_by ASC
                            ";
                            $qry_photo_type = mysql_query($q);
                            while($row_photo_type = mysql_fetch_array($qry_photo_type))
                            {
                        ?>
                                <option value="<?php echo $row_photo_type["isi"]; ?>"><?php echo $row_photo_type["isi"]; ?></option>
                        <?php 
                            }
                        ?>
                    </select>
                </td>
                <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_remarks_photo_<?php echo $no; ?>" id="v_remarks_photo_<?php echo $no; ?>"></td>
                <td>&nbsp;</td>
                <td align="center">
                	<button type="submit" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_photo" id="btn_save_ajax_photo" value="Save">
						<i class="entypo-floppy"></i>
					</button>
				</td>
                <td>&nbsp;</td>
            </tr>
        <?php
        $no++;
         
    }

    $q = "
            SELECT
                ".$db["master"].".employee_photo.*
            FROM
                ".$db["master"].".employee_photo
            WHERE
                ".$db["master"].".employee_photo.employee_id = '".$id."'
            ORDER BY
                ".$db["master"].".employee_photo.sid ASC
    ";
    $qry = mysql_query($q);
    $no_view = 1;
    while($r = mysql_fetch_object($qry))
    {
		?>
		<tr>
		    <td>
		        <?php echo $no_view; ?>
		        <input type="hidden" name="no_photo[]" value="<?php echo $no; ?>">
		        <input type="hidden" name="v_sid_photo_<?php echo $no; ?>" id="v_sid_photo_<?php echo $no; ?>" value="<?php echo $r->sid; ?>">
		    </td>
		    <td align="center"><img src="npm_show_images.php?sid=<?php echo $r->sid; ?>&table=employee_photo" width="140" class="img_css"></td>
		    <td>
		         <select name="v_photo_type_<?php echo $no; ?>" id="v_photo_type_<?php echo $no; ?>" class="form-control-new" style="width:80px;">
		            <?php 
		                $q = "
		                        SELECT
		                            ".$db["master"].".kombo.isi
		                        FROM
		                            ".$db["master"].".kombo
		                        WHERE
		                            1
		                            AND ".$db["master"].".kombo.type = 'photo_type'
		                        ORDER BY
		                            ".$db["master"].".kombo.sort_by ASC
		                ";
		                $qry_photo_type = mysql_query($q);
		                while($row_photo_type = mysql_fetch_array($qry_photo_type))
		                {
		                    $selected = "";
		                    if($row_photo_type["isi"]==$r->photo_type)
		                    {
		                        $selected = "selected='selected'";
		                    }
		            ?>
		                    <option <?php echo $selected; ?> value="<?php echo $row_photo_type["isi"]; ?>"><?php echo $row_photo_type["isi"]; ?></option>
		            <?php 
		                }
		            ?>
		        </select> 
		    </td>
		    <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_remarks_photo_<?php echo $no; ?>" id="v_remarks_photo_<?php echo $no; ?>" value="<?php echo $r->remarks; ?>"></td>
		    <td align="center">
		    	<i class="entypo-download"></i>
		    	<a class="link_menu tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Download" title="" href="npm_employee_download_photo.php?sid=<?php echo $r->sid; ?>&table=employee_photo">Download</a>
			</td>
		    <td align="center">
            	<button type="submit" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_photo" id="btn_save_ajax_photo" value="Save">
					<i class="entypo-floppy"></i>
				</button>
		    </td>
		    <td align="center"><input type="checkbox" name="del_photo[]" value="<?php echo $r->sid; ?>"></td>
		</tr>
		<?php 
        $no++;
        $no_view++;
    }
    
    if($id!="")
    {
	?>
		<tr>     
		   <td colspan="6">&nbsp;</td>
		   <td align="center">
		   	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_del_photo" id="btn_del_photo" value="Hapus">Hapus<i class="entypo-trash"></i></button>
		   </td>
		</tr>
	<?php 
	}
    ?>
    </tbody>
</table>
</div>