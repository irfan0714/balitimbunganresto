<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "FA Sub Type";
    $list  = "npm_fa_sub_type.php";
    $htm   = "npm_fa_sub_type_form.php";
    $pk    = "sub_type_id";
    $title = "sub_type_name";
    
    if($ajax)
    {
        if($ajax=="ajax_kategory")
        {
            $v_kategory_id = $_GET["v_kategory_id"];
            
            $q = "
                    SELECT
                        fa_type.type_id,    
                        fa_type.type_name
                    FROM
                        fa_type
                    WHERE
                        1
                        AND fa_type.kategory_id = '".$v_kategory_id."' 
                    ORDER BY
                        fa_type.type_name ASC
            ";
            $qry_type = mysql_query($q);
            while($row_type = mysql_fetch_array($qry_type))
            {
                list($type_id, $type_name) = $row_type;
                
                $arr_data["list_type"][$type_id] = $type_id;
                $arr_data["type_name"][$type_id] = $type_name;
            } 
            
            ?>
            <select class="form-control-new" name="v_type_id" id="v_type_id" style="width: 300px;" onchange="CallAjaxForm('ajax_type', this.value)">
                <option value="">-</option>
                <?php 
                    foreach($arr_data["list_type"] as $type_id=>$val)
                    {
                        $type_name = $arr_data["type_name"][$type_id];
                        ?>
                        <option <?php if($data["type_id"]==$type_id) echo "selected='selected'"; ?> value="<?php echo $type_id; ?>"><?php echo $type_name; ?></option>
                        <?php
                    }    
                ?>
            </select>
            <?php
        }
        else if($ajax=="ajax_type")
        {
            $v_type_id = $_GET["v_type_id"];
            
            $q = "
                    SELECT
                        fa_group_asset.group_asset_name
                    FROM
                        fa_type
                        INNER JOIN fa_group_asset ON
                                fa_type.group_asset_id = fa_group_asset.group_asset_id
                    WHERE
                        1
                        AND fa_type.type_id = '".$v_type_id."'
                    LIMIT
                        0,1 
            "; 
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($group_asset_name) = $row;   
            
            echo $group_asset_name;
        }
        exit();
    }
    
	$q_cek = "
		SELECT
			uni.sub_type_id
		FROM
		(
			SELECT
				sub_type_id
			FROM
				fa_proposal_detail
			WHERE
				sub_type_id = '".$id."'
            
            UNION 
            
            SELECT
                type_id
            FROM
                fa_sixed_asset
            WHERE
                sub_type_id = '".$id."'
		) as uni
		LIMIT
			0,1
	";
	
	$qry_cek = mysql_query($q_cek);
	$jml_cek = mysql_num_rows($qry_cek);
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena ada relasi dengan FA PROPOSAL DETAIL / FIXED ASSET";
        }
        else
        {
			$q = "
				DELETE FROM
					fa_sub_type
				WHERE
					sub_type_id = '".$id."'
			";
			if(!mysql_query($q))
			{
				$msg = "Failed Delete";
			}
			else
			{
				header("Location: ".$list.$link_back.$link_order_by);
			}  
		}
    }
    
    
    if(isset($_REQUEST["btn_save"])!="")
    {
        $v_type_id          = save_char($_POST["v_type_id"]);
        $v_sub_type_name    = save_char($_POST["v_sub_type_name"]);
        $v_approval_mutasi  = save_char($_POST["v_approval_mutasi"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        fa_sub_type
                    SET
                        type_id = '".$v_type_id."',
                        sub_type_name = '".$v_sub_type_name."',
                        approval_mutasi = '".$v_approval_mutasi."',
                        edited_user = '".$ses_login."',
                        edited_date = NOW()
                    WHERE
                        sub_type_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update";
                }
                else
                {
                    $class_warning = "success";
                    $msg = "Berhasil menyimpan";
                }
            }
        }
        else
        {   
            $id = get_counter_int("","fa_sub_type","sub_type_id",100);   
            
            $q = "
                INSERT INTO
                    fa_sub_type
                SET
                    sub_type_id = '".$id."',
                    type_id = '".$v_type_id."',
                    sub_type_name = '".$v_sub_type_name."',
                    approval_mutasi = '".$v_approval_mutasi."',
                    author_user = '".$ses_login."',
                    author_date = NOW(),
                    edited_user = '".$ses_login."',
                    edited_date = NOW()
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert";
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            fa_sub_type.`sub_type_id`,
            fa_sub_type.`type_id`,
            fa_sub_type.`sub_type_name`,
            fa_sub_type.`approval_mutasi`,
            fa_sub_type.`author_user`,
            fa_sub_type.`author_date`,
            fa_sub_type.`edited_user`,
            fa_sub_type.`edited_date`,
            fa_type.`kategory_id`
        FROM 
            fa_sub_type
            INNER JOIN fa_type ON
                fa_sub_type.type_id = fa_type.type_id
        WHERE
            1
            AND fa_sub_type.sub_type_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
        SELECT
            fa_kategory.kategory_id,    
            fa_kategory.kategory_name
        FROM
            fa_kategory
        WHERE
            1
        ORDER BY
            fa_kategory.kategory_name ASC
    ";
    $qry_kat = mysql_query($q);
    while($row_kat = mysql_fetch_array($qry_kat))
    {
        list($kategory_id, $kategory_name) = $row_kat;
        
        $arr_data["list_kategory"][$kategory_id] = $kategory_id;
        $arr_data["kategory_name"][$kategory_id] = $kategory_name;
    }
    
    $q = "
        SELECT
            fa_type.type_id,    
            fa_type.type_name
        FROM
            fa_type
        WHERE
            1
            AND fa_type.kategory_id = '".$data["kategory_id"]."' 
        ORDER BY
            fa_type.type_name ASC
    ";
    $qry_type = mysql_query($q);
    while($row_type = mysql_fetch_array($qry_type))
    {
        list($type_id, $type_name) = $row_type;
        
        $arr_data["list_type"][$type_id] = $type_id;
        $arr_data["type_name"][$type_id] = $type_name;
    }
    
    $q = "
        SELECT
            fa_group_asset.group_asset_name
        FROM
            fa_type
            INNER JOIN fa_group_asset ON
                    fa_type.group_asset_id = fa_group_asset.group_asset_id
        WHERE
            1
            AND fa_type.type_id = '".$data["type_id"]."'
        LIMIT
            0,1 
    "; 
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($group_asset_name) = $row;   
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

	<script type="text/javascript">
	    function CallAjaxForm(vari,param1,param2,param3)
	    {
	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        if (param3 == undefined) param3 = '';
	        
	        document.getElementById('show_image_ajax_form').style.display = '';
	        
	        if (vari == 'ajax_kategory'){
	            var variabel;
	            
	            v_kategory_id = param1;
	            variabel = '&v_kategory_id='+v_kategory_id;
	            
	            xmlhttp.open('get', 'npm_fa_sub_type_form.php?ajax=ajax_kategory&'+variabel, true);
	            xmlhttp.onreadystatechange = function() {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)){
	                    document.getElementById('td_type').innerHTML = xmlhttp.responseText;
	        			document.getElementById('show_image_ajax_form').style.display = 'none';
	                }
	                return false;
	            }
	            xmlhttp.send(null);
	        }
	        else if (vari == 'ajax_type'){
	            var variabel;
	            
	            v_type_id = param1;
	            variabel = '&v_type_id='+v_type_id;
	            
	            xmlhttp.open('get', 'npm_fa_sub_type_form.php?ajax=ajax_type&'+variabel, true);
	            xmlhttp.onreadystatechange = function() {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)){
	                    document.getElementById('td_group_asset').innerHTML = xmlhttp.responseText;
	                    document.getElementById('v_sub_type_name').focus();
	        			document.getElementById('show_image_ajax_form').style.display = 'none';
	                }
	                return false;
	            }
	            xmlhttp.send(null);
	        }
	        
	    }

		function validasi()
		{
			if(document.getElementById("v_kategory_id").value=="")
			{
				alert("Kategori harus dipilih...");
				document.getElementById("v_kategory_id").focus();
				return false;
			}
	        else if(document.getElementById("v_type_id").value*1==0)
	        {
	            alert("Type harus dipilih...");
	            document.getElementById("v_type_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_sub_type_name").value*1==0)
	        {
	            alert("Sub Type Name harus diisi...");
	            document.getElementById("v_sub_type_name").focus();
	            return false;
	        }
		}
		
		function start_page()
		{
			document.getElementById("v_sub_type_name").focus();	
		}
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
				<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    	
			    	<tr>
                        <td class="title_table" width="150">Kategory</td>
                        <td> 
                            <select class="form-control-new" name="v_kategory_id" id="v_kategory_id" style="width: 300px;" onchange="CallAjaxForm('ajax_kategory', this.value)">
                                <option value="">-</option>
                                <?php 
                                    foreach($arr_data["list_kategory"] as $kategory_id=>$val)
                                    {
                                        $kategory_name = $arr_data["kategory_name"][$kategory_id];
                                        ?>
                                        <option <?php if($data["kategory_id"]==$kategory_id) echo "selected='selected'"; ?> value="<?php echo $kategory_id; ?>"><?php echo $kategory_name; ?></option>
                                        <?php
                                    }    
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Type</td>
                        <td id="td_type"> 
                            <select class="form-control-new" name="v_type_id" id="v_type_id" style="width: 300px;" onchange="CallAjaxForm('ajax_type', this.value)">
                                <option value="">-</option>
                                <?php 
                                    foreach($arr_data["list_type"] as $type_id=>$val)
                                    {
                                        $type_name = $arr_data["type_name"][$type_id];
                                        ?>
                                        <option <?php if($data["type_id"]==$type_id) echo "selected='selected'"; ?> value="<?php echo $type_id; ?>"><?php echo $type_name; ?></option>
                                        <?php
                                    }    
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Group Asset</td>
                        <td id="td_group_asset"> 
                            <?php echo $group_asset_name; ?>&nbsp;
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Sub Type</td>
                        <td> 
                            <input type="text" class="form-control-new" name="v_sub_type_name" id="v_sub_type_name" value="<?php echo $data["sub_type_name"]; ?>" maxlength="100" style="width: 300px;" /> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Approval Mutasi</td>
                        <td> 
                            <label><input <?php if($data["approval_mutasi"]*1==0 || $data["approval_mutasi"]*1==1) echo "checked='checked'"; ?> type="radio" name="v_approval_mutasi" id="v_approval_mutasi" value="1" /> <span style="color: blue;">&nbsp;Yes</span></label>
                            &nbsp;&nbsp;
                            <label><input <?php if($data["approval_mutasi"]*1==2) echo "checked='checked'"; ?> type="radio" name="v_approval_mutasi" id="v_approval_mutasi" value="2" /> <span style="color: red;">&nbsp;No</span></label>
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
			    </table>			    
			    </form>
		         
			</div>
		</div>
		
<?php include("footer.php"); ?>