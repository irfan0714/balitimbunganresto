<?php
include("header.php");

$modul   = "REPORT DAILY - PEMAKAIAN VOUCHER";

if(!isset($_GET["v_date"])){ $v_date = isset($_GET["v_date"]); } else { $v_date = $_GET["v_date"]; }

unset($arr_data);
             
$q = "
        SELECT
            transaksi_detail_voucher.NomorVoucher,    
            SUM(transaksi_detail_voucher.NilaiVoucher) AS pemakaian
        FROM
            transaksi_detail_voucher
        WHERE
            1
            AND transaksi_detail_voucher.Tanggal = '".format_save_date($v_date)."'
            AND transaksi_detail_voucher.Jenis IN ('1','2')
        GROUP BY
            transaksi_detail_voucher.NomorVoucher
        ORDER BY
            transaksi_detail_voucher.NomorVoucher ASC
"; 
$counter = 0;
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list(
        $NomorVoucher,    
        $pemakaian 
    ) = $row;
    
    $arr_data["list_data"][$NomorVoucher] = $NomorVoucher;
    $arr_data["data_pemakaian"][$NomorVoucher] = $pemakaian;
} 

$q = "
        SELECT
            ticket.noticket,    
            ticket.harga
        FROM
            ticket
        WHERE
            1
            AND ticket.add_date = '".format_save_date($v_date)."'
            AND ticket.noidentitas != '1234'
        ORDER BY
            ticket.noticket ASC
"; 
$counter = 0;
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list(
        $noticket,    
        $harga 
    ) = $row;
    
    $arr_data["list_data"][$noticket] = $noticket;
    $arr_data["data_ticket"][$noticket] = $harga;
} 

	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
		
	</script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black" onload="start_page()">
<form method="get">
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
    
		
		<div class="row">
              <table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30">No</td>
                    <td>No Ticket</td>
                    <td>Harga Ticket</td>
                    <td>Pemakaian</td>
                </tr>
                
                <tbody style="color: black;">
                
                <?php           
                    $no = 1;
                    foreach($arr_data["list_data"] as $noticket=>$val)
                    {
                        $harga_ticket = $arr_data["data_ticket"][$noticket];
                        $data_pemakaian = $arr_data["data_pemakaian"][$noticket];
                        
                        $bgcolor = "";
                        if($harga_ticket!=$data_pemakaian)
                        {
                            $bgcolor = "yellow";
                        }
                        
                        ?>
                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td bgcolor="<?php echo $bgcolor; ?>"><?php echo $no; ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>"><?php echo $noticket; ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>" style="text-align: right"><?php echo format_number($harga_ticket, 0, ",", ".", "ind"); ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>" style="text-align: right"><?php echo format_number($data_pemakaian, 0, ",", ".", "ind"); ?></td>
                        </tr>
                        <?php
                        
                        $arr_total["harga_ticket"] += $harga_ticket;
                        $arr_total["data_pemakaian"] += $data_pemakaian;
                        $no++;
                    }
                ?>
                </tbody>
                
                <tr style="text-align: right;">
                    <td colspan="2">TOTAL</td>
                    <td style="text-align: right"><?php echo format_number($arr_total["harga_ticket"], 0, ",", ".", "ind"); ?></td>
                    <td style="text-align: right"><?php echo format_number($arr_total["data_pemakaian"], 0, ",", ".", "ind"); ?></td>
                </tr>
                
                
              </table>
       	
       	</div>
</form>		
		
    
<?php include("footer.php"); ?>