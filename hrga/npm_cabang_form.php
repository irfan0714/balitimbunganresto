<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";

    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
	
	if($ajax)
	{
		if($ajax=="save_ajax")
		{
			$pk_id  			= $_GET["pk_id"];
            $id                 = $pk_id;
			$v_depo_id			= $_GET["v_depo_id"];
            $v_employee_id      = $_GET["v_employee_id"];
			$v_depo_name        = save_char($_GET["v_depo_name"]);
			
			$q = "
				SELECT 
					hrd_cabang.*
				FROM 
					hrd_cabang
				WHERE
					1
					AND hrd_cabang.cabang_id = '".$id."'
				LIMIT 0,1
			";
			$qry["data"] = mysql_query($q);
			$data = mysql_fetch_array($qry["data"]);
			
			if($v_depo_id*1==0)
			{
				// insert
				$counter_details = get_counter_int("","depo","depo_id",100);
                                
				$q = "
						INSERT INTO
							depo
						SET
							depo_id = '".$counter_details."',
							cabang_id = '".$id."',
							depo_name = '".$v_depo_name."',
                            employee_id = '".$v_employee_id."',
                            author = '".$ffclock."',
                            edited = '".$ffclock."'
				";
				if(!mysql_query($q))
				{
					$msg = "Gagal Exists<br>";
				}
			}
			else
			{
				// update
				
				$q = "
						UPDATE
							depo
						SET
							depo_name = '".$v_depo_name."',
                            employee_id = '".$v_employee_id."',
                            edited = '".$ffclock."'
						WHERE
							depo_id = '".$v_depo_id."'
				";
				if(!mysql_query($q))
				{
					$msg = "Failed update details ||".$q."<br>";
				}    
			}
			
			include("npm_cabang_form_details.php");
		}
		exit();
	}
	
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Cabang";
    $list  = "npm_cabang.php";
    $htm   = "npm_cabang_form.php";
    $pk    = "cabang_id";
    $title = "cabang_name";
	
	$q_cek = "
		SELECT
			uni.cabang_id
		FROM
		(
			SELECT
				hrd_cabang.cabang_id
			FROM
				hrd_employee_position
                INNER JOIN depo ON
                    hrd_employee_position.depo_id = depo.depo_id
			WHERE
				hrd_cabang.cabang_id = '".$id."'
		) as uni
		LIMIT
			0,1
	";
	
	$qry_cek = mysql_query($q_cek);
	$jml_cek = mysql_num_rows($qry_cek);
		
	$class_warning = "error";
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena memiliki relasi dengan Posisi Karyawan";
        }
        else
        {
	
			$q = "
				DELETE FROM
					hrd_cabang
				WHERE
					cabang_id = '".$id."'
			";
			if(!mysql_query($q))
			{
				$msg = "Failed Delete";
			}
			else
			{
				$q = "
					DELETE FROM
						depo
					WHERE
						cabang_id = '".$id."'
				";
				if(!mysql_query($q))
				{
					$msg = "Failed Delete 2";
				}
				else
				{
					header("Location: ".$list.$link_back.$link_order_by);
				}
			}  
		}
    }
    
    if(isset($_REQUEST["btn_del_details"])!="")
    {   
		while(list($key, $val)=@each($del))
		{
            // cek ke employee_position
            $q = "
                SELECT
                    hrd_employee_position.depo_id
                FROM
                    hrd_employee_position
                WHERE
                    hrd_employee_position.depo_id = '".$val."'
                LIMIT
                    0,1
            ";
            $qry_cek = mysql_query($q);
            $row_cek = mysql_fetch_array($qry_cek);
            
            if($row_cek["depo_id"]=="")
            {
			    $q = "
				    DELETE FROM
					    depo
				    WHERE
					    depo_id = '".$val."'    
			    ";
			    mysql_query($q);
            }
            else
            {
                $msg = "Tidak Bisa dihapus karena ada relasi dengan Posisi Karyawan<br>";
            }
		}
        
       	$class_warning = "success";
       	$msg = "<strong>Berhasil menghapus!</strong>";
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
		$v_cabang_name    = save_char($_POST["v_cabang_name"]);
		$v_cabang_initial = save_char($_POST["v_cabang_initial"]);
		$v_cabang_no      = save_char($_POST["v_cabang_no"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        hrd_cabang
                    SET
                        cabang_name = '".$v_cabang_name."',
                        cabang_initial = '".$v_cabang_initial."',
                        cabang_no = '".$v_cabang_no."',
                        edited = '".ffclock()."'
                    WHERE
                        cabang_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Delete";
                }
                else
                {
                    $no = $_POST["no"];
                    
                    while(list($key, $val)=@each($no))
                    {
						$v_depo_id     = $_POST['v_depo_id_'.$val];
						$v_depo_name   = save_char($_POST['v_depo_name_'.$val]);
						$v_employee_id = save_char($_POST['v_employee_id_'.$val]);
                        
                        if($v_depo_name)
                        {
                            if($v_depo_id*1==0)
                            {
                                // insert
                                $counter_details = get_counter_int("","depo","depo_id",100);
                                
                                $q = "
                                        INSERT INTO
                                            depo
                                        SET
                                            depo_id = '".$counter_details."',
                                            cabang_id = '".$id."',
                                            depo_name = '".$v_depo_name."',
                                            employee_id = '".$v_employee_id."',
                                            author = '".$ffclock."',
                                            edited = '".$ffclock."'
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "<strong>Gagal menyimpan!</strong>";
                                }
                            }
                            else
                            {
                                $q = "
                                        UPDATE
                                            depo
                                        SET
                                            depo_name = '".$v_depo_name."',
                                            employee_id = '".$v_employee_id."',
                                            edited = '".$ffclock."'
                                        WHERE
                                            depo_id = '".$v_depo_id."'
                                ";
                                //echo $q."<hr>";
                                if(!mysql_query($q))
                                {
                                    $msg = "<strong>Gagal menyimpan 2!</strong>";
                                }    
                            }
                        }
                    }
                }
                
				$class_warning = "success";
                $msg = "<strong>Berhasil menyimpan!</strong>";
            }
            
        }
        else
        {   
            $id = get_counter_int("","hrd_cabang","cabang_id",100);   
            
            $q = "
                    INSERT INTO
                        hrd_cabang
                    SET
                        cabang_id = '".$id."',
                        cabang_name = '".$v_cabang_name."',
                        cabang_initial = '".$v_cabang_initial."',
                        cabang_no = '".$v_cabang_no."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg = "<strong>Failed Update!</strong> Gagal Query";
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
			hrd_cabang.cabang_id,
			hrd_cabang.cabang_name,
			hrd_cabang.cabang_initial,
			hrd_cabang.cabang_no,
			hrd_cabang.author,
			hrd_cabang.edited
		FROM 
			hrd_cabang
        WHERE
            1
            AND hrd_cabang.cabang_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
	function validasi()
	{
		if(document.getElementById("v_cabang_name").value=="")
		{
			alert("Nama harus diisi");
			document.getElementById("v_cabang_name").focus();
			return false;
		}
	}
	
	function start_page()
	{
		document.getElementById("v_cabang_name").focus();	
	}
    
    function pop_up_employee(nilai)
    {
        //alert(nilai);
        windowOpener(600, 800, 'Search Employee', 'npm_cabang_pop_up_employee.php?nilai='+nilai, 'Search Employee')
    }
    
	function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
	{
		try
		{
			if (!tipenya) return false;
			document.getElementById("show_image_ajax_form").style.display='';

			if (param1 == undefined) param1 = '';
			if (param2 == undefined) param2 = '';
			if (param3 == undefined) param3 = '';
			if (param4 == undefined) param4 = '';
			if (param5 == undefined) param5 = '';

			var variabel;
			var arr_data;
			variabel = "";

			if(tipenya=='save_ajax')
			{   
				pk_id = '<?php echo $id; ?>';
				
		        v_depo_id    = document.getElementById("v_depo_id_"+param1).value;
				v_depo_name  = document.getElementById("v_depo_name_"+param1).value;
		        v_employee_id= document.getElementById("v_employee_id_"+param1).value;
                
                variabel += "&pk_id="+pk_id;
                variabel += "&v_depo_id="+v_depo_id;
                variabel += "&v_depo_name="+v_depo_name;
                variabel += "&v_employee_id="+v_employee_id;

				//alert(variabel);
				
				if(v_depo_name=='')
				{
					alert('Nama Depo harus diisi...');
					return false;
				}
				else
				{
					xmlhttp.open('get', 'npm_cabang_form.php?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById('table_details').innerHTML = xmlhttp.responseText;
							document.getElementById("show_image_ajax_form").style.display='none';
	                        alert("Berhasil menyimpan");
						}

						return false;
					}
				}
				
				xmlhttp.send(null);
			}
		}
		catch(err)
		{
			txt  = "There was an error on this page.\n\n";
			txt += "Error description : "+ err.message +"\n\n";
			txt += "Click OK to continue\n\n";
			alert(txt);
		}
	}
    
</script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        	
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    	<tr>
                        <td class="title_table" width="150">Nama</td>
                        <td> 
                            <input type="text" class="form-control-new " value="<?php echo $data["cabang_name"]; ?>" name="v_cabang_name" id="v_cabang_name" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Inisial</td>
                        <td> 
                            <input type="text" class="form-control-new " value="<?php echo $data["cabang_initial"]; ?>" name="v_cabang_initial" id="v_cabang_initial" maxlength="10" size="5">
                        	&nbsp;
                        	<b>KC No</b>
                        	&nbsp;&nbsp;
                        	<input type="text" class="form-control-new " value="<?php echo $data["cabang_no"]; ?>" name="v_cabang_no" id="v_cabang_no" maxlength="10" size="5">
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
					
					<?php
					if($id!="")
					{
					?>
					<tr>
	                    <td colspan="100%" id="table_details">
	                        <?php 
	                            include("npm_cabang_form_details.php");
	                        ?>    
	                    </td>
	                </tr>
					<?php
					}
					?>
			    
			    </table>			    
			    </form>     
        	
        		<?php 
				if($id!="")
				{
				?>
					<ol class="breadcrumb">
						<li><strong><i class="entypo-vcard"></i>Information data</strong></li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		         ?>
		         
        	</div>
    	</div>
    

<?php include("footer.php"); ?>
