<div class="col-md-12">
<table class="table table-bordered responsive">
	<thead>
		<tr>
			<th width="30"><center>No</center></th>
		    <th><center>Mulai</center></th>
		    <th><center>Akhir</center></th>
		    <th><center>Perusahaan</center></th>
		    <th><center>Cabang</center></th>
		    <th><center>Depo</center></th>
		    <th><center>Divisi</center></th>
		    <th><center>Jabatan</center></th>
		    <th><center>Status Acting</center></th>
		    <th><center>Keterangan</center></th>
		</tr>
	</thead>
	<tbody>

		<?php
	    $no = 1;
	    $q = "
	        SELECT
	            ".$db["master"].".employee_position.*,
	            
	            ".$db["master"].".company.company_id,
	            ".$db["master"].".company.company_name,
	            ".$db["master"].".company.company_initial,
	            
	            ".$db["master"].".hrd_divisi.divisi_id,
	            ".$db["master"].".hrd_divisi.divisi_name,
	            
	            ".$db["master"].".hrd_departemen.departemen_id,
	            ".$db["master"].".hrd_departemen.departemen_name,
	            
	            ".$db["master"].".hrd_cabang.cabang_id,
	            ".$db["master"].".hrd_cabang.cabang_name,
	            
	            ".$db["master"].".depo.depo_id,
	            ".$db["master"].".depo.depo_name,
	            
	            ".$db["master"].".jabatan.jabatan_id,
	            ".$db["master"].".jabatan.jabatan_name
	            
	        FROM
	            ".$db["master"].".employee_position
	            INNER JOIN ".$db["master"].".employee ON
	                ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
	                
	            INNER JOIN ".$db["master"].".company ON
	                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
	            
	            INNER JOIN ".$db["master"].".hrd_divisi ON
	                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
	            
	            INNER JOIN ".$db["master"].".hrd_departemen ON
	                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
	            
	            INNER JOIN ".$db["master"].".depo ON
	                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
	            INNER JOIN ".$db["master"].".hrd_cabang ON
	                ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
	            
	            INNER JOIN ".$db["master"].".jabatan ON
	                ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
	        WHERE
	            ".$db["master"].".employee_position.employee_id = '".$id."'
	        ORDER BY
	            ".$db["master"].".employee_position.start_date ASC
	    ";
	    $qry = mysql_query($q);
	    $no_view = 1;
	    while($r = mysql_fetch_object($qry))
	    {
	    	$echo_acting = "";
	    	if($r->status_acting==1)
	    	{
				$echo_acting = format_show_date($r->start_acting)." s/d ".format_show_date($r->end_acting)." :: ".$r->remarks_acting;	
			}
	        
			?>
			<tr height="25">
			    <td>
			        <?php echo $no_view; ?>
			        <input type="hidden" name="no_position[]" value="<?php echo $no; ?>">
			        <input type="hidden" name="v_sid_position_<?php echo $no; ?>" id="v_sid_position_<?php echo $no; ?>" value="<?php echo $r->sid; ?>">
			    </td>
			    <td align="center"><?php echo format_show_date($r->start_date); ?></td>
			    <td align="center"><?php if($r->end_date!="0000-00-00" ) echo format_show_date($r->end_date); ?></td>
			    <td><?php echo $r->company_initial; ?></td>
			    <td><?php echo $r->cabang_name; ?></td>
			    <td><?php echo $r->depo_name; ?></td>
			    <td><?php echo $r->divisi_name; ?></td>
			    <td><?php echo $r->jabatan_name; ?></td>
			    <td><?php echo $echo_acting; ?></td>
			    <td><?php echo $r->remarks; ?></td>
			</tr>
			<?php 
	        $no++;
	        $no_view++;
	    }
		?>

		<tr>
		    <td colspan="100%" align="center">
		    	<a href="npm_employee_position.php?v_employee_id=<?php echo $id; ?>" class="link_menu tooltip-primary" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Input Data Posisi" title="">
		    		<i class="entypo-pencil"></i>Posisi Karyawan
		    	</a>
		    </td>
		</tr>
	</tbody>
</table>
</div>