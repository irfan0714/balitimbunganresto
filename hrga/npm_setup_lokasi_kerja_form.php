<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    if(!isset($_POST["btn_save_username"])){ $btn_save_username = isset($_POST["btn_save_username"]); } else { $btn_save_username = $_POST["btn_save_username"]; }
    
	if($ajax)
	{
		if($ajax=="save_ajax")
		{
			$pk_id  			= $_GET["pk_id"];
            $id                 = $pk_id;
			$v_sid			    = $_GET["v_sid"];
			$v_employee_id      = save_char($_GET["v_employee_id"]);
			$v_username      	= save_char($_GET["v_username"]);
            
			if($v_sid*1==0)
			{
				// insert
				$counter_details = get_counter_int($db["master"],"setup_lokasi_kerja","sid",100);
                                
				$q = "
					INSERT INTO
						".$db["master"].".setup_lokasi_kerja
					SET
						sid = '".$counter_details."',
						absensi_cabang_id = '".$id."',
                        employee_id = '".$v_employee_id."'
				";
				if(!mysql_query($q))
				{
					$msg = "Gagal Insert";
				}
			}
			else
			{
				$q = "
					UPDATE
						".$db["master"].".setup_lokasi_kerja
					SET
						employee_id = '".$v_employee_id."'
					WHERE
						sid = '".$v_sid."'
				";
				if(!mysql_query($q))
				{
					$msg .= "Failed update details ||".$q."<br>";
				}				
			}
				
			if($v_username)
			{
				update_username($v_employee_id, $v_username, ""); 	
			}
			
			include("npm_setup_lokasi_kerja_form_details.php");
		}
        else if($ajax=="generate_username")
        {
            $v_employee_name = save_char($_GET["v_employee_name"]);
            $v_birthday      = save_char($_GET["v_birthday"]);
            //$v_employee_id = 771;
            
            $generate_username = generate_username($v_employee_name, $v_birthday);
            
            echo $generate_username[1];
        }
        else if($ajax=="reset_password")
        {
			$v_employee_id = save_char($_GET["v_employee_id"]);
			$v_username    = save_char($_GET["v_username"]);
			
			$generate_password = generate_password($v_employee_id, 6);
			
			$q = "
                UPDATE
                    ".$db["master"].".user 
                SET
                    password = '".md5($generate_password)."',
                    history_password = '".md5($generate_password)."',
		    		pass_awal = '".$generate_password."'
                WHERE
                    username = '".$v_username."'
	        ";
	        mysql_query($q);
	        
	        $q = "
                UPDATE
                    ".$db["master"].".employee 
                SET
                    password = '".md5($generate_password)."'
                WHERE
                    employee_id = '".$v_employee_id."'
	        ";
	        mysql_query($q);
	        
	        update_global_user($v_employee_id);
            call_log($v_username,"Reset Password","Update","");
            
            echo $generate_password;
        }
		exit();
	}
	
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Setup Lokasi Kerja";
    $list  = "npm_setup_lokasi_kerja.php";
    $htm   = "npm_setup_lokasi_kerja_form.php";
    $pk    = "absensi_cabang_id";
	
    $jml_cek = 0;
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if(isset($_REQUEST["btn_del_details"])!="")
    {   
		while(list($key, $val)=@each($del))
		{
            // cek ke employee_position
		    $q = "
			    DELETE FROM
				    ".$db["master"].".setup_lokasi_kerja
			    WHERE
				    sid = '".$val."'    
		    ";
		    mysql_query($q);
		}
        
        $class_warning = "success";
        $msg = "Berhasil Menghapus<br>";
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
        if($id!="")
        {
            if($action=="edit")
            {
                $no = $_POST["no"];
                $v_data = $_POST["v_data"];;
                $v_data_all = $_POST["v_data_all"];
                
                while(list($key, $val)=@each($no))
                {
                    $v_sid                = $_POST['v_sid_'.$val];
                    $v_employee_id        = $_POST['v_employee_id_'.$val];
                    
                    if($v_employee_id)
                    {
                        if($v_sid*1==0)
                        {
                            // insert
                            $counter_details = get_counter_int($db["master"],"setup_lokasi_kerja","sid",100);
                            
                            $q = "
                                INSERT INTO
                                    ".$db["master"].".setup_lokasi_kerja
                                SET
                                    sid = '".$counter_details."',
                                    absensi_cabang_id = '".$id."',
                                    employee_id = '".$v_employee_id."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg .= "Gagal<br>";
                            }
                        }
                        else
                        {
                            // update
                            $q = "
                                    UPDATE
                                        ".$db["master"].".setup_lokasi_kerja
                                    SET
                                        employee_id = '".$v_employee_id."',
                                        remarks = '".$v_remarks."'
                                    WHERE
                                        sid = '".$v_sid."'
                            ";
                            
                            if(!mysql_query($q))
                            {
                                $msg .= "Gagal ||".$q."<br>";
                            }    
                        }
                    }
                }

                foreach($v_data_all as $key=>$val)
                {
                    $q = "
                        UPDATE
                            ".$db["master"].".setup_lokasi_kerja
                        SET
                            absensi = '0'
                        WHERE
                            employee_id = '".$val."'
                    ";
                    
                    if(!mysql_query($q))
                    {
                        $msg .= "Gagal ||".$q."<br>";
                    }        
                }
                
                foreach($v_data as $key=>$val)
                {
                    $q = "
                        UPDATE
                            ".$db["master"].".setup_lokasi_kerja
                        SET
                            absensi = '1'
                        WHERE
                            employee_id = '".$val."'
                    ";
                    
                    if(!mysql_query($q))
                    {
                        $msg .= "Gagal ||".$q."<br>";
                    }        
                }
               
				$class_warning = "success";
                $msg .= "Berhasil menyimpan";
            }
        }
    }
    
    $q = "
        SELECT 
            ".$db["master"].".absensi_cabang.*
        FROM 
            ".$db["master"].".absensi_cabang
        WHERE
            1
            AND ".$db["master"].".absensi_cabang.absensi_cabang_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
		function validasi()
		{
			
		}
		
		function start_page()
		{
			
		}
	    
	    function pop_up_search_employee_details(nilai)
	    {
	        var pk_id;
	        pk_id  = "<?php echo $id; ?>";
	        //alert(v_date); 
	       
	        windowOpener('600', '800', 'Cari Karyawan', 'npm_setup_lokasi_kerja_pop_up_employee.php?id='+pk_id+'&v_nilai='+nilai, 'Cari Karyawan');
	    }
	    
		
		function CallAjaxForm(vari,param1){
			if (param1 == undefined) param1 = '';
			
			document.getElementById('show_image_ajax_form').style.display = '';
			
			if (vari == 'save_ajax'){
				var variabel;
				
				var pk_id;
				var v_employee_id;
				
				pk_id = '<?php echo $id; ?>';
				no = param1;
				
				v_username   = document.getElementById("v_username_"+no).value;
				v_employee_id   = document.getElementById("v_employee_id_"+no).value;
				
				variabel  = '';
				variabel += 'pk_id='+pk_id;
				variabel += '&v_employee_id='+v_employee_id;
				variabel += '&v_username='+v_username;
				
				//alert(variabel);
				
				if(v_employee_id*1==0)
				{
					alert('Karyawan harus dipilih...');
				}
				else
				{
					
					xmlhttp.open('get', 'npm_setup_lokasi_kerja_form.php?ajax=save_ajax&'+variabel, true);
					xmlhttp.onreadystatechange = function() {
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById('table_details').innerHTML = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display = 'none';
							alert("Berhasil menyimpan");
						}
						return false;
					}
					xmlhttp.send(null);
				}
			}
	        else if(vari == 'generate_username')
	        {
	            var variable;
	            var v_employee_name;
	            var v_birthday;
	            
	            no = param1; 
	            
	            v_employee_name = document.getElementById("v_employee_name_"+no).value;
	            v_birthday      = document.getElementById("v_birthday_"+no).value;
	            
	            variable   = "&v_employee_name="+v_employee_name;
	            variable  += "&v_birthday="+v_birthday;
	            //alert(variable);
	            
	            if(v_employee_name=="")
	            {
	                alert("Nama harus diisi");
	                document.getElementById('v_username_'+no).focus();
	                document.getElementById('show_image_ajax_form').style.display = 'none';
	                return false;
	            }
	            else if(v_birthday=="" || v_birthday=="00/00/0000")
	            {
	                alert("Birthday lahir harus diisi");                                  
	                document.getElementById('v_username_'+no).focus();
	                document.getElementById('show_image_ajax_form').style.display = 'none';
	                return false;   
	            }
	            else
	            {
	                xmlhttp.open('get', 'npm_setup_lokasi_kerja_form.php?ajax=generate_username&'+variable, true);
	                xmlhttp.onreadystatechange = function() {
	                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
	                    {
	                        document.getElementById('v_username_'+no).value = xmlhttp.responseText;
	                        document.getElementById('show_image_ajax_form').style.display = 'none';
	                    }
	                    return false;
	                }
	                xmlhttp.send(null);
	            } 
	            
	        } 
			else if(vari == 'reset_password')
	        {
	            var variable;
	            var v_employee_id;
	            var v_username;
	            
	            no = param1; 
	            
	            v_employee_id = document.getElementById("v_employee_id_"+no).value;
	            v_username      = document.getElementById("v_username_"+no).value;
	            
	            variable   = "&v_employee_id="+v_employee_id;
	            variable  += "&v_username="+v_username;
	            //alert(variable);
	           	
	            xmlhttp.open('get', 'npm_setup_lokasi_kerja_form.php?ajax=reset_password&'+variable, true);
	            xmlhttp.onreadystatechange = function() {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)){
						 document.getElementById("show_password_"+no).innerHTML     = xmlhttp.responseText;
						 document.getElementById('show_image_ajax_form').style.display = 'none';
	                }
	                return false;
	            }
	            xmlhttp.send(null);
	        }
		}
	    
	    function CheckAll(param, target){
	        var field = document.getElementsByName(target);
	        var chkall = document.getElementById(param);
	        if (chkall.checked == true){
	            for (i = 0; i < field.length; i++)
	                field[i].checked = true ;
	        }else{
	            for (i = 0; i < field.length; i++)
	                field[i].checked = false ;
	        }        
	    }
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    	<tr>
                        <td class="title_table" width="150">Nama</td>
                        <td> 
                            <?php echo $data["absensi_cabang_name"]; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Alamat</td>
                        <td> 
                            <?php echo $data["address_1"]; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">&nbsp;</td>
                        <td> 
                            <?php echo $data["address_2"]; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Kota</td>
                        <td> 
                            <?php echo $data["city"]; ?>
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>
							<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						 	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
			    
				    <tr>
		                <td colspan="100%" id="table_details">
		                    <?php 
		                        include("npm_setup_lokasi_kerja_form_details.php");
		                    ?>    
		                </td>
		            </tr>
		            
			    </table>			    
			    </form>
			
				<?php
		            if($id!="")
		            {
		         ?>
		         	
		         	<ol class="breadcrumb">
						<li><strong><i class="entypo-vcard"></i>Information data</strong></li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		         ?>
		         
			</div>
		</div>
		
<?php include("footer.php"); ?>