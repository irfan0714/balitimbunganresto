<?php
include("header.php");
                               
$modul            = "Master Fixed Asset";
$file_current     = "npm_fa_master_fixed_asset.php";
$file_name        = "npm_fa_master_fixed_asset_data.php";

$sess_employee_id = $config_employee["employee_id"] ;
$add_edit_user    = ffclock();

function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}

function select_fa_receipt($receipt_no)
{
	global $db;  
	
	$qreceipt = "
		SELECT 
		  ".$db["master"].".fa_receipt.receipt_no,
		  ".$db["master"].".fa_receipt.receipt_date,
		  ".$db["master"].".fa_receipt.pono
		FROM
		  ".$db["master"].".fa_receipt 
		WHERE 1 
		  AND ".$db["master"].".fa_receipt.receipt_no = '".$receipt_no."' 
		LIMIT 0, 01
	";
	$qry_receipt = mysql_query($qreceipt);
	$row_receipt = mysql_fetch_array($qry_receipt);

	db_connect_type("192.168.0.8","root","");
	
	$qpono ="
		SELECT
		  '".$row_receipt["receipt_no"]."' AS receipt_no,
		  '".$row_receipt["receipt_date"]."' AS receipt_date,
		  vci.fa_purchase_order.pono,
		  vci.fa_purchase_order.podate,
		  vci.fa_purchase_order.NoProposal
		FROM
		  vci.fa_purchase_order 
		WHERE 1 
		  AND vci.fa_purchase_order.pono = '".$row_receipt["pono"]."' 
		LIMIT 0, 01 
	";
	$qry_pono = mysql_query($qpono);
	$row = mysql_fetch_array($qry_pono);
	
	return $row;
}

function fixed_asset_code_counter($db_name,$table_name,$col_primary,$depo,$type)
{
	global $db;
	
	// pengkodean fixed assert
	$tahun       = date("y");
	$bulan       = date("m");
	$lokasi      = sprintf("%02s", $depo);
	$type_barang = sprintf("%03s", $type);
	
	 $query = "
        SELECT
            ".$db_name.".".$table_name.".".$col_primary."
        FROM
            ".$db_name.".".$table_name."
        WHERE
            1
            AND SUBSTR(".$db_name.".".$table_name.".".$col_primary.", 1, 2) = '".$tahun."'
            AND SUBSTR(".$db_name.".".$table_name.".".$col_primary.", 3, 2) = '".$bulan."'
            AND SUBSTR(".$db_name.".".$table_name.".".$col_primary.", 6, 2) = '".$lokasi."'
            AND SUBSTR(".$db_name.".".$table_name.".".$col_primary.", 9, 3) = '".$type_barang."'
        ORDER BY
            ".$db_name.".".$table_name.".".$col_primary." DESC
        LIMIT
            0,1
    ";        
    $qry = mysql_query($query);
    $row = mysql_fetch_array($qry);   
    
    $counter = (substr($row["fixed_asset_code"],13,4)*1)+1;
               
    $fixed_code = $tahun.$bulan." ".$lokasi." ".$type_barang." ".sprintf("%04s", $counter);
    
    return $fixed_code;
}

function send_master_fixed_asset()
{
	$body = '
		<table align="center" cellpadding="0" cellspacing="0" width="80%" style="border: 3px solid #C1CDD8; ">
			<tbody>

				<tr style="background: #bad0ee; height: 50;">
					<td colspan="6" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
						<p style="text-transform: uppercase;">
							Master Fixed Asset
						</p>
					</td>
				</tr>
				
				<tr height="25">
					<td colspan="6" style="padding: 0 0 0 10px;">Berikut informasi Master Fixed Asset</td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>

				<tr >
					<td width="150" style="padding: 0 0 0 10px;"><b>No PB</b></td>
					<td width="9">:</td>
					<td>'.$data["NoTransaksi"].'</td>
					
					<td width="150" style="padding: 0 0 0 10px;"><b>Lokasi</b></td>
					<td width="9">:</td>
					<td>Cabang Semarang :: Umum </td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Proposal</b></td>
					<td width="9">:</td>
					<td>'.$data["fixed_asset_code"].'</td>
					
					<td width="150" style="padding: 0 0 0 10px;"><b>Ruangan</b></td>
					<td width="9">:</td>
					<td>Ruang Finance </td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Kategori</b></td>
					<td width="9">:</td>
					<td>'.format_show_date($data["status_date"]).'</td>
					
					<td width="150" style="padding: 0 0 0 10px;"><b>Penanggung Jawab</b></td>
					<td width="9">:</td>
					<td>personal</td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Type</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">'.$data["status"].'</font></td>
					
					<td width="150" style="padding: 0 0 0 10px;"><b>Nama</b></td>
					<td width="9">:</td>
					<td>Hendri Trestiawan</td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Sub Type</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">'.$data["status"].'</font></td>
					
					<td width="150" style="padding: 0 0 0 10px;"><b>Jabatan</b></td>
					<td width="9">:</td>
					<td>IT Supervisor</td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Deskripsi</b></td>
					<td width="9">:</td>
					<td>
						<table>
							<tr>
								<td>Kendaraan Merek</td>
								<td>:</td>
								<td>Yamaha</td>
							</tr>
							<tr>
								<td>Kendaraan Jenis</td>
								<td>:</td>
								<td>Xvion</td>
							</tr>
							<tr>
								<td>Kendaraan Warna</td>
								<td>:</td>
								<td>Merah</td>
							</tr>
							<tr>
								<td>Kendaraan CC</td>
								<td>:</td>
								<td>150</td>
							</tr>
							<tr>
								<td>No Polisi</td>
								<td>:</td>
								<td>BJM895BMT</td>
							</tr>
							<tr>
								<td>No Rangka</td>
								<td>:</td>
								<td>DASFVDS2512600DDF</td>
							</tr>
							<tr>
								<td>No Mesin</td>
								<td>:</td>
								<td>KLDSF9940380LJGD</td>
							</tr>
							<tr>
								<td>Tgl STNK</td>
								<td>:</td>
								<td>18/08/2015</td>
							</tr>
							<tr>
								<td>Tgl Pajak</td>
								<td>:</td>
								<td>11/08/2015</td>
							</tr>
							<tr>
								<td>No BPKB</td>
								<td>:</td>
								<td>SVSS26548DAS</td>
							</tr>
						</table>
						
					</td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Tanggal Beli</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">29/07/2015</font></td>
					
					<td style="padding: 0 0 0 10px;"><b>Nominal (Rp)</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">-</font></td>
				</tr>

				<tr height="25">
					<td style="padding: 0 0 0 10px;"><b>Keterangan</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">Xivion</font></td>
					
					<td style="padding: 0 0 0 10px;"><b>Metode Penyusutan</b></td>
					<td width="9">:</td>
					<td><font style="text-transform:capitalize;">3 Bulan</font></td>
				</tr>
				
				<tr height="25">
					<td colspan="6"><hr style="border: 1px solid #C1CDD8;"></hr></td>
				</tr>
				
				<tr>
					<td colspan="6" style="padding: 0 0 10px 10px;">
					Yang Membuat
					<br>
					<br>
					<br>
					('.$data_employee["employee_name"].')
					</td>
				</tr>
			</tbody>
		</table>
	';
}

$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
		$search_by             = trim($_GET["search_by"]);
		$search_keyword        = trim($_GET["search_keyword"]);
		$search_fa_code        = trim($_GET["search_fa_code"]);
		$search_no_proposal    = trim($_GET["search_no_proposal"]);
		$search_no_receipt     = trim($_GET["search_no_receipt"]);
		$search_cabang		   = trim($_GET["search_cabang"]);
		$search_kategory       = trim($_GET["search_kategory"]);
		$search_period         = trim($_GET["search_period"]);
		$v_mm                  = trim($_GET["v_mm"]);
		$v_yyyy                = trim($_GET["v_yyyy"]);
		$v_date_from           = trim($_GET["v_date_from"]);
		$v_date_to             = trim($_GET["v_date_to"]);
		$v_fixed_asset_id      = trim($_GET["v_fixed_asset_id"]);

		$v_fixed_asset_id_curr = $_GET["v_fixed_asset_id_curr"];
		
		$where = "";        
        if($search_by=="data")
        {    
            if($search_keyword)
            {
                //echo $search_keyword;
                unset($arr_keyword);
                $arr_keyword[0] = "fa_fixed_asset.fixed_asset_code";    
                $arr_keyword[1] = "fa_fixed_asset.NoProposal";      
                $arr_keyword[2] = "fa_fixed_asset.receipt_no";     
                $arr_keyword[3] = "fa_kategory.kategory_name";        
                $arr_keyword[4] = "fa_type.type_name";       
                $arr_keyword[5] = "fa_sub_type.sub_type_name";        
                $arr_keyword[6] = "fa_fixed_asset.desc_kendaraan_merek";
                $arr_keyword[7] = "fa_fixed_asset.desc_kendaraan_jenis";
                $arr_keyword[8] = "fa_fixed_asset.desc_kendaraan_warna";
                $arr_keyword[9] = "fa_fixed_asset.desc_kendaraan_cc";
                $arr_keyword[10] = "fa_fixed_asset.desc_kendaraan_nopol";
                $arr_keyword[11] = "fa_fixed_asset.desc_kendaraan_no_rangka";
                $arr_keyword[12] = "fa_fixed_asset.desc_kendaraan_no_mesin";
                $arr_keyword[13] = "fa_fixed_asset.desc_kendaraan_tgl_stnk";
                $arr_keyword[14] = "fa_fixed_asset.desc_kendaraan_tgl_pajak";
                $arr_keyword[15] = "fa_fixed_asset.desc_kendaraan_no_bpkb";
                $arr_keyword[16] = "fa_fixed_asset.desc_gedung_nama_pemilik";
                $arr_keyword[17] = "fa_fixed_asset.desc_gedung_luas_bangunan";
                $arr_keyword[18] = "fa_fixed_asset.desc_gedung_luas_tanah";
                $arr_keyword[19] = "fa_fixed_asset.desc_gedung_tinggi_bangunan";
                $arr_keyword[20] = "fa_fixed_asset.desc_gedung_asuransi_par";
                $arr_keyword[21] = "fa_fixed_asset.desc_gedung_no_asuransi_par";
                $arr_keyword[22] = "fa_fixed_asset.desc_gedung_asuransi_start_date_par";
                $arr_keyword[23] = "fa_fixed_asset.desc_gedung_asuransi_end_date_par";
                $arr_keyword[24] = "fa_fixed_asset.desc_gedung_asuransi_earthquake";
                $arr_keyword[25] = "fa_fixed_asset.desc_gedung_no_asuransi_earthquake";
                $arr_keyword[26] = "fa_fixed_asset.desc_gedung_asuransi_start_date_earthquake";
                $arr_keyword[27] = "fa_fixed_asset.desc_gedung_asuransi_end_date_earthquake";
                $arr_keyword[28] = "fa_fixed_asset.desc_gedung_fungsi";
                $arr_keyword[29] = "fa_fixed_asset.desc_gedung_desa";
                $arr_keyword[30] = "fa_fixed_asset.desc_gedung_kecamatan";
                $arr_keyword[31] = "fa_fixed_asset.desc_gedung_kota";
                $arr_keyword[32] = "fa_fixed_asset.desc_gedung_kabupaten";
                $arr_keyword[33] = "fa_fixed_asset.desc_gedung_provinsi";
                $arr_keyword[35] = "fa_fixed_asset.desc_gudang_luas_bangunan";
                $arr_keyword[36] = "fa_fixed_asset.desc_gudang_luas_tanah";
                $arr_keyword[37] = "fa_fixed_asset.desc_gudang_no_asuransi_par";
                $arr_keyword[38] = "fa_fixed_asset.desc_gudang_asuransi_start_date_par";
                $arr_keyword[39] = "fa_fixed_asset.desc_gudang_asuransi_end_date_par";
                $arr_keyword[40] = "fa_fixed_asset.desc_gudang_asuransi_earthquake";
                $arr_keyword[41] = "fa_fixed_asset.desc_gudang_no_asuransi_earthquake";
                $arr_keyword[42] = "fa_fixed_asset.desc_gudang_asuransi_start_date_earthquake";
                $arr_keyword[43] = "fa_fixed_asset.desc_gudang_asuransi_end_date_earthquake";
                $arr_keyword[44] = "fa_fixed_asset.desc_gudang_desa";
                $arr_keyword[45] = "fa_fixed_asset.desc_gudang_kecamatan";
                $arr_keyword[46] = "fa_fixed_asset.desc_gudang_kota";
                $arr_keyword[47] = "fa_fixed_asset.desc_gudang_kabupaten";
                $arr_keyword[48] = "fa_fixed_asset.desc_gudang_provinsi";
                
                $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
                $where .= $where_search_keyword;
            } 
        }        
        else if($search_by=="fa_code")
        {    
        	if($search_fa_code)
        	{
        		$where .="AND ".$db["master"].".fa_fixed_asset.fixed_asset_code LIKE '%".$search_fa_code."%'";   
			}
        }       
        else if($search_by=="no_proposal")
        {    
        	if($search_no_proposal)
        	{
        		$where .="AND ".$db["master"].".fa_fixed_asset.NoProposal  LIKE '%".$search_no_proposal."%'";   
        	}  
        }       
        else if($search_by=="no_receipt")
        {    
        	if($search_no_receipt)
        	{
        		$where .="AND ".$db["master"].".fa_fixed_asset.receipt_no  LIKE '%".$search_no_receipt."%'";   
        	}  
        }
        
        if($search_cabang)
        {
			$where .="AND ".$db["master"].".hrd_cabang.cabang_id='".$search_cabang."'";	
		}
		else
		{
		    if(count($arr_data["list_useradmin"])>0)
		    {
		    	$where .= where_array($arr_data["list_useradmin"], "cabang.cabang_id", "in");
			} 
		}
		
        if($search_kategory)
        {
        	$where .="AND ".$db["master"].".fa_kategory.kategory_id='".$search_kategory."'";   
		}
		
		if($search_period=="monthly")
		{
			$where .="AND SUBSTR(".$db["master"].".fa_fixed_asset.tanggal_beli,1,7) = '".$v_yyyy."-".$v_mm."'";
		}
		else if($search_period=="range")
		{
			$where .="AND ".$db["master"].".fa_fixed_asset.tanggal_beli BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'";	
		}
		
		if($v_fixed_asset_id)
		{
			$where = "AND ".$db["master"].".fa_fixed_asset.fixed_asset_id='".$v_fixed_asset_id."'";
		}
            
        // select query all
        {
			$q="
				SELECT 
				  ".$db["master"].".fa_fixed_asset.fixed_asset_id,
				  ".$db["master"].".fa_fixed_asset.fixed_asset_code,
				  ".$db["master"].".fa_fixed_asset.NoProposal,
				  ".$db["master"].".fa_fixed_asset.receipt_no,
				  ".$db["master"].".fa_fixed_asset.sub_type_id,
				  ".$db["master"].".fa_fixed_asset.tanggal_beli,
				  ".$db["master"].".fa_fixed_asset.photo,
				  ".$db["master"].".fa_kategory.kategory_id,
				  ".$db["master"].".fa_kategory.kategory_name,
				  ".$db["master"].".fa_type.type_name,
				  ".$db["master"].".fa_sub_type.sub_type_name,
				  ".$db["master"].".hrd_cabang.cabang_name,
				  ".$db["master"].".depo.depo_name,
				  tb_lokasi.ruangan_id,
				  tb_pj.type_pj,
				  tb_pj.employee_id
				FROM
				  ".$db["master"].".fa_fixed_asset 
				  INNER JOIN ".$db["master"].".fa_sub_type 
				    ON ".$db["master"].".fa_fixed_asset.sub_type_id = ".$db["master"].".fa_sub_type.sub_type_id 
				  INNER JOIN ".$db["master"].".fa_type 
				    ON ".$db["master"].".fa_sub_type.type_id = ".$db["master"].".fa_type.type_id 
				  INNER JOIN ".$db["master"].".fa_kategory 
				    ON ".$db["master"].".fa_type.kategory_id = ".$db["master"].".fa_kategory.kategory_id 
				    
				  INNER JOIN 
				  (
				  	SELECT 
					  unu.* 
					FROM
					  (SELECT 
					    ".$db["master"].".fa_lokasi.sid AS sid_lokasi,
					    ".$db["master"].".fa_lokasi.fixed_asset_id,
					    ".$db["master"].".fa_lokasi.lokasi_date,
					    ".$db["master"].".fa_lokasi.depo_id,
					    ".$db["master"].".fa_lokasi.ruangan_id 
					  FROM
					    ".$db["master"].".fa_lokasi 
					  WHERE 1 
					    AND ".$db["master"].".fa_lokasi.mutasi_no = '' 
					  ORDER BY ".$db["master"].".fa_lokasi.lokasi_date DESC,
					    ".$db["master"].".fa_lokasi.sid DESC) AS unu 
					WHERE 1 
					GROUP BY unu.fixed_asset_id
				  ) AS tb_lokasi 
				  	ON fa_fixed_asset.fixed_asset_id = tb_lokasi.fixed_asset_id
				   
				  INNER JOIN ".$db["master"].".depo 
				    ON ".$db["master"].".tb_lokasi.depo_id = ".$db["master"].".depo.depo_id 
				  INNER JOIN ".$db["master"].".hrd_cabang 
				    ON ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id 
				    
				   INNER JOIN 
				    (SELECT 
				      uni.* 
				    FROM
				      (SELECT 
				        ".$db["master"].".fa_penanggung_jawab.sid AS sid_pj,
				        ".$db["master"].".fa_penanggung_jawab.fixed_asset_id,
				        ".$db["master"].".fa_penanggung_jawab.pj_date,
				        ".$db["master"].".fa_penanggung_jawab.type_pj,
				        ".$db["master"].".fa_penanggung_jawab.employee_id
				      FROM
				        ".$db["master"].".fa_penanggung_jawab 
				      WHERE 1 
				        AND ".$db["master"].".fa_penanggung_jawab.mutasi_no = '' 
				      ORDER BY ".$db["master"].".fa_penanggung_jawab.pj_date DESC,
				        ".$db["master"].".fa_penanggung_jawab.sid DESC) AS uni 
				    WHERE 1 
				    GROUP BY uni.fixed_asset_id) AS tb_pj 
				    ON fa_fixed_asset.fixed_asset_id = tb_pj.fixed_asset_id 
				    
				WHERE 1 
					".$where."
				ORDER BY 
					".$db["master"].".fa_fixed_asset.fixed_asset_id DESC,
					".$db["master"].".fa_fixed_asset.fixed_asset_code ASC
			";
			$sql=mysql_query($q);
			while($row=mysql_fetch_array($sql))
			{ 
				list(
					$fixed_asset_id,
					$fixed_asset_code,
					$NoProposal,
					$receipt_no,
					$sub_type_id,
					$tanggal_beli,
					$photo,
					$kategory_id,
					$kategory_name,
					$type_name,
					$sub_type_name,
					$cabang_name,
					$depo_name,
					$ruangan_id,
					$type_pj,
					$employee_id)=$row;
				
				$arr_data["list_fixed_asset"][$fixed_asset_id]=$fixed_asset_id;
				$arr_data["fixed_asset_code"][$fixed_asset_id]=$fixed_asset_code;
				$arr_data["NoProposal"][$fixed_asset_id]=$NoProposal;
				$arr_data["receipt_no"][$fixed_asset_id]=$receipt_no;
				$arr_data["sub_type_id"][$fixed_asset_id]=$sub_type_id;
				$arr_data["tanggal_beli"][$fixed_asset_id]=$tanggal_beli;
				$arr_data["photo"][$fixed_asset_id]=$photo;
				$arr_data["kategory_id"][$fixed_asset_id]=$kategory_id;
				$arr_data["kategory_name"][$fixed_asset_id]=$kategory_name;
				$arr_data["type_name"][$fixed_asset_id]=$type_name;
				$arr_data["sub_type_name"][$fixed_asset_id]=$sub_type_name;
				$arr_data["cabang_name"][$fixed_asset_id]=$cabang_name;
				$arr_data["depo_name"][$fixed_asset_id]=$depo_name;
				$arr_data["ruangan_id"][$fixed_asset_id]=$ruangan_id;
				$arr_data["type_pj"][$fixed_asset_id]=$type_pj;
				$arr_data["employee_id"][$fixed_asset_id]=$employee_id;
				
				$arr_data["list_emp"][$employee_id]=$employee_id;
			}
		}  
		
		// select ruangan
		{
			$q="
				SELECT 
				  fa_ruangan.ruangan_id,
				  fa_ruangan.ruangan_name 
				FROM
				  ".$db["master"].".fa_ruangan 
				ORDER BY 
				  fa_ruangan.ruangan_id ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($ruangan_id,$ruangan_name)=$row;
				
				$arr_data["list_receipt"][$ruangan_id]=$ruangan_id;
				$arr_data["ruangan_name"][$ruangan_id]=$ruangan_name;
			}	
		}
		
		// select employee
        {
        	$where_emp = where_array($arr_data["list_emp"], "employee.employee_id", "in");
        	
        	$q="
				SELECT 
				  employee.employee_id,
				  employee.employee_name 
				FROM
				  ".$db["master"].".employee 
				WHERE 1 
				  ".$where_emp."
				ORDER BY 
				  employee.employee_id ASC 
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($employee_id,$employee_name)=$row;
				
				$arr_data["list_employee"][$employee_id]=$employee_id;
				$arr_data["employee_name"][$employee_id]=$employee_name;
			}	
		} 
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_matreq_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
    	<thead>
			<tr>
	            <th width="30" rowspan="2"><strong><center>No</center></strong></th>
	            <th rowspan="2"><strong><center>FA Code</center></strong></th>
	            <th rowspan="2"><strong><center>No Proposal</center></strong></th>
	            <th rowspan="2"><strong><center>No Receipt</center></strong></th>
	            <th colspan="3"><strong><center>Kategori</center></strong></th>     
	            <th rowspan="2"><strong><center>Tgl Beli</center></strong></th>      
	            <th colspan="2"><strong><center>Penanggung Jawab</center></strong></th>  
	            <th colspan="3"><strong><center>Lokasi Asset</center></strong></th>    
	            <th rowspan="2"><strong><center>Photo</center></strong></th>    
			</tr>
			
	        <tr>
	        	<th><strong><center>Group</center></strong></th>
	        	<th><strong><center>Type</center></strong></th>
	        	<th><strong><center>Sub Type</center></strong></th>
	        	
	        	<th><strong><center>Type</center></strong></th>
	        	<th><strong><center>Name</center></strong></th>
	        	
	        	<th><strong><center>Cabang</center></strong></th>
	        	<th><strong><center>Depo</center></strong></th>
	        	<th><strong><center>Ruangan</center></strong></th>
	        </tr>
	        
		</thead>
		<tbody>
		
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_fixed_asset"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_fixed_asset"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        $photo_echo="";    
        foreach($arr_data["list_fixed_asset"] as $fixed_asset_id => $val)
        {    
        	$nomor++;
        	
			$sub_type_id = $arr_data["sub_type_id"][$fixed_asset_id];
			$kategory_id = $arr_data["kategory_id"][$fixed_asset_id];
			
			$fixed_asset_code = $arr_data["fixed_asset_code"][$fixed_asset_id];
			$NoProposal = $arr_data["NoProposal"][$fixed_asset_id];
			$receipt_no = $arr_data["receipt_no"][$fixed_asset_id];
			$kategory_name = $arr_data["kategory_name"][$fixed_asset_id];
			$type_name = $arr_data["type_name"][$fixed_asset_id];
			$sub_type_name = $arr_data["sub_type_name"][$fixed_asset_id];
			$tanggal_beli = $arr_data["tanggal_beli"][$fixed_asset_id];
			$photo = $arr_data["photo"][$fixed_asset_id];
			
			$cabang_name = str_replace("Cabang","",$arr_data["cabang_name"][$fixed_asset_id]);
			$depo_name = $arr_data["depo_name"][$fixed_asset_id];
			$ruangan_id = $arr_data["ruangan_id"][$fixed_asset_id];
			$type_pj = $arr_data["type_pj"][$fixed_asset_id];
			$employee_id = $arr_data["employee_id"][$fixed_asset_id];
			
			$employee_name = $arr_data["employee_name"][$employee_id];
				
			$ruangan_name = $arr_data["ruangan_name"][$ruangan_id];
           
            if($photo)
            {
				$photo_echo ="<td onclick=\"view_photo('".$photo."')\"><center><img src=\"images/view.png\" alt=\"Photo Asset\" title=\"View Photo\"/></center></td>";
			}
			else
			{
				$photo_echo ="<td onclick=\"CallAjaxForm('edit_data','".$fixed_asset_id."')\"><center>-</center></td>";	
			} 
			
			$style_color = "";
            if($v_fixed_asset_id_curr==$fixed_asset_id)
            {
                $style_color = "background:#cafdb5;";    
            } 
            
            $style_blm="";
            if($employee_id==0 && $type_pj!="umum")
            {
				$style_blm = "background:#FCF;";    	
			}
            		
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
             
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" align="center" style="<?php echo $style_blm; ?>"><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" align="center" style="<?php echo $style_blm; ?>"><nobr><?php echo $fixed_asset_code; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" align="center" style="<?php echo $style_blm; ?>"><?php echo $NoProposal; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" align="center" style="<?php echo $style_blm; ?>"><?php echo $receipt_no; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $kategory_name; ?></td>   
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $type_name; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $sub_type_name; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" align="center" style="<?php echo $style_blm; ?>"><?php echo format_show_date($tanggal_beli); ?></td>  
                
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="text-align: center; text-transform:capitalize;" style=""><?php echo $type_pj; ?></td>   
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $employee_name; ?></td>  
                
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $cabang_name; ?></td>   
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $depo_name; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $fixed_asset_id; ?>')" style="<?php echo $style_blm; ?>"><?php echo $ruangan_name; ?></td>  
                <?php echo $photo_echo; ?>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
	</table> 
	</form>
	</div>  
   	<?php  
    }

	else if($ajax=="add_data")
	{
		// select receipt
		{
			$q="
				SELECT 
				  fa_receipt.receipt_no,
				  fa_receipt.receipt_date,
				  fa_receipt.pono 
				FROM
				  fa_receipt 
				ORDER BY 
				  fa_receipt.receipt_date DESC,
				  fa_receipt.receipt_no ASC,
				  fa_receipt.pono ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($receipt_no,$receipt_date,$pono)=$row;
				
				$arr_data["list_receipt"][$receipt_no]=$receipt_no;
				$arr_data["receipt_date"][$receipt_no]=$receipt_date;
				$arr_data["pono"][$receipt_no]=$pono;
			}	
		}
		
		// select kategori
		{
			$q="
				SELECT 
				  * 
				FROM
				  fa_kategory 
				ORDER BY 
				  fa_kategory.kategory_name ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($kategory_id,$kategory_name)=$row;
				
				$arr_data["list_kategori"][$kategory_id]=$kategory_id;
				$arr_data["kategory_name"][$kategory_id]=$kategory_name;
			}	
		}

		// select cabang
		{
			$q="
				SELECT 
				  hrd_cabang.cabang_id,
				  hrd_cabang.cabang_name 
				FROM
				  hrd_cabang 
				ORDER BY 
				  hrd_cabang.cabang_name ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($cabang_id,$cabang_name)=$row;
				
				$arr_data["list_cabang"][$cabang_id]=$cabang_id;
				$arr_data["cabang_name"][$cabang_id]=$cabang_name;
			}	
		}
		
		// select metode penyusutan
		{
			$q="
				SELECT 
				  * 
				FROM
				  fa_metode_penyusutan 
				ORDER BY 
				  fa_metode_penyusutan.metode_penyusutan_name ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($metode_penyusutan_id,$metode_penyusutan_name,$total_bulan)=$row;
				
				$arr_data["list_penyusutan"][$metode_penyusutan_id]=$metode_penyusutan_id;
				$arr_data["metode_penyusutan_name"][$metode_penyusutan_id]=$metode_penyusutan_name;
				$arr_data["total_bulan"][$metode_penyusutan_id]=$total_bulan;
			}
		}
		
		// select asset
		{
			$q="
				SELECT 
				  fa_fixed_asset.receipt_no 
				FROM
				  fa_fixed_asset 
				WHERE fa_fixed_asset.receipt_no IN 
				  (SELECT 
				    fa_receipt.receipt_no 
				  FROM
				    fa_receipt 
				  GROUP BY 
				    fa_receipt.receipt_no)
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($receipt_no)=$row;
				
				$arr_data["list_asset_receipt"][$receipt_no]=$receipt_no;
			}
		}
		
        ?>
        <div class="col-md-12" align="left">
        
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
        	<input type="hidden" name="v_fixed_asset_id" id="v_fixed_asset_id" value="">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">No PB</td>
                <td width="40%">
                    <select name="v_receipt_no" id="v_receipt_no" class="form-control-new" onchange="CallAjaxForm('ajax_proposal', this.value);">  
                        <option value="">-</option>
                        <?php 
                        	foreach($arr_data["list_receipt"] as $receipt_no => $val)
                        	{
                        		$asset_receipt = $arr_data["list_asset_receipt"][$receipt_no];
								
								if($asset_receipt!=$receipt_no)
								{
									?>
	                                <option value="<?php echo $receipt_no; ?>"><?php echo $receipt_no; ?></option>
	                                <?php	
								}
							}
                        ?>
                    </select>
                </td>
                
                <td class="title_table" width="200">Lokasi</td>
                <td>
                    <select name="v_cabang_id" id="v_cabang_id" class="form-control-new" onchange="CallAjaxForm('ajax_depo', this.value);">  
                        <option value="">-</option>
                        <?php 
                        	foreach($arr_data["list_cabang"] as $cabang_id => $val)
                        	{
								$cabang_name = $arr_data["cabang_name"][$cabang_id];	
								$cabang_name = str_replace("Cabang","",$cabang_name);
								
								?>
                                <option value="<?php echo $cabang_id; ?>"><?php echo $cabang_name; ?></option>
                                <?php
							}
                        ?>
                    </select>
                    
                    <span id="span_depo"><select class="form-control-new" style="width: 70px;"><option value="">-</option></select></span>
                </td> 
                
            </tr>  

            <tr>
                <td class="title_table">Proposal</td>
                <td id="td_no_proposal"></td> 
                
                <td class="title_table">Ruangan</td>
                <td id="td_ruangan_asset">
                    <select class="form-control-new" style="width: 70px;"><option value="">-</option></select>
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">Kategori</td>
                <td>
                    <select name="v_kategory_id" id="v_kategory_id" class="form-control-new" onchange="CallAjaxForm('ajax_type', this.value);">  
                        <option value="">-</option>
                        <?php 
                        	foreach($arr_data["list_kategori"] as $kategory_id => $val)
                        	{
                        		$kategory_name = $arr_data["kategory_name"][$kategory_id];
                        		
								?>
                                <option value="<?php echo $kategory_id; ?>"><?php echo $kategory_name; ?></option>
                                <?php	
                        	}
                        ?>
                    </select>
                </td>
                
                <td class="title_table">Penanggung Jawab</td>
                <td>
                	<label><input type="radio" name="v_penanggung_jawab" id="v_penanggung_jawab" value="personal" onclick="change_pj(this.value)"/>&nbsp;Personal</label>&nbsp;
                	<label><input type="radio" name="v_penanggung_jawab" id="v_penanggung_jawab" value="umum" onclick="change_pj(this.value)"/>&nbsp;Umum</label>
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">Type</td>
                <td id="td_type" colspan="3">
                    <select class="form-control-new" style="width: 70px;"><option value="">-</option></select>
                </td>
	            
                <td class="title_table" width="150" id="td_empl_name_1" style="display: none;">Nama</td>
                <td id="td_empl_name_2" style="display: none;">
                	<input type="hidden" name="v_emp_id" id="v_emp_id" value="">
                   	<input type="text" class="form-control-new" name="v_emp_name" id="v_emp_name" size="30" value="" onclick="pop_up_employee()">
                   	
                   	<button type="button" class="btn btn-info btn-sm sm-new"title="Search Employee" onclick="pop_up_employee()">
						<i class="entypo-Search"></i>
					</button>
      			</td> 
            </tr>
                
            <tr>
                <td class="title_table">Sub Type</td>
                <td id="td_sub_type" colspan="3">
                    <select class="form-control-new" style="width: 70px;"><option value="">-</option></select>
                </td>
	            
                <td class="title_table" width="150" id="td_jabatan_1" style="display: none;">Jabatan</td>
                <td id="td_jabatan_2" style="display: none;"></td> 
            </tr>
            
            <tr>
            	<td class="title_table">Deskripsi</td>
	            <td id="td_deskripsi" colspan="3"></td>
            </tr>
            
            <tr>
                <td class="title_table" width="150">Tanggal Beli</td>
                <td id="td_tanggal_beli" colspan="3"></td> 
            </tr>
            
            <tr>
                <td class="title_table">Qty</td>
                <td>
                	<label><input type="checkbox" name="v_cek_qty" id="v_cek_qty" value="1" onclick="cek_qty();"/> Cheklis jika qty lebih dari satu </label>
                	<span id="span_qty" style="display: none;"><input type="text" name="v_qty" id="v_qty" value="" class="form-control-new" style="text-align: right; width:50px;"/></span>
                </td> 
	            
	            <td colspan="2"><b>Nilai Asset</b>&nbsp;<i>*) Diinput oleh Acct</i></td>
            </tr>
            
            <tr>
            	<td class="title_table">Photo</td>
	            <td>
	            	<div class="fileinput fileinput-new" data-provides="fileinput">
						<span class="btn btn-info btn-file sm-new">
							<span class="fileinput-new">Select file</span>
							<span class="fileinput-exists">Change</span>
							<input type="file" name='fupload' id='fupload' accept=".pdf, .jpeg, .jpg, .png">
						</span>
						<span class="fileinput-filename"></span>
						<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
					</div>       
	            </td>
	            
                <td class="title_table">Nominal (Rp)</td>
                <td>
                   <input type="text" class="form-control-new" name="v_nominal" id="v_nominal" onblur="toFormat2('v_nominal')" size="30" style="text-align: right;">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_keterangan" id="v_keterangan" maxlength="100" style="width: 80%;">
                </td> 
                
                <td class="title_table">Metode Penyusutan</td>
                <td>
                    <select name="v_metode_penyusutan_id" id="v_metode_penyusutan_id" class="form-control-new">  
                        <option value="">-</option>
                        <?php 
                        	foreach($arr_data["list_penyusutan"] as $metode_penyusutan_id => $val)
                        	{
								$metode_penyusutan_name = $arr_data["metode_penyusutan_name"][$metode_penyusutan_id];
								$total_bulan = $arr_data["total_bulan"][$metode_penyusutan_id];
								
								?>
                                <option value="<?php echo $metode_penyusutan_id; ?>"><?php echo $metode_penyusutan_name; ?></option>
                                <?php	
							}
                        ?>
                    </select>
                </td>
                
            </tr>
               
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
        	</table>  
        	</form>
        </div>
        <?php
		
	}
    
    else if($ajax=="edit_data")
    {                                      
		$v_fixed_asset_id = $_GET["v_fixed_asset_id"];       
        
        // select detail proposal
        {
            $q = "
                SELECT 
				  ".$db["master"].".fa_fixed_asset.*,
				  ".$db["master"].".fa_kategory.kategory_name,
				  ".$db["master"].".fa_type.type_id,
				  ".$db["master"].".fa_type.type_name,
				  ".$db["master"].".fa_sub_type.sub_type_name,
				  ".$db["master"].".hrd_cabang.cabang_id,
				  ".$db["master"].".hrd_cabang.cabang_name,
				  ".$db["master"].".depo.depo_id,
				  ".$db["master"].".depo.depo_name,
				  tb_lokasi.sid_lokasi,
				  tb_lokasi.ruangan_id,
				  tb_pj.sid_pj,
				  tb_pj.type_pj,
				  tb_pj.employee_id
				FROM
				  ".$db["master"].".fa_fixed_asset 
				  INNER JOIN ".$db["master"].".fa_sub_type 
				    ON ".$db["master"].".fa_fixed_asset.sub_type_id = ".$db["master"].".fa_sub_type.sub_type_id 
				  INNER JOIN ".$db["master"].".fa_type 
				    ON ".$db["master"].".fa_sub_type.type_id = ".$db["master"].".fa_type.type_id 
				  INNER JOIN ".$db["master"].".fa_kategory 
				    ON ".$db["master"].".fa_type.kategory_id = ".$db["master"].".fa_kategory.kategory_id 
				  INNER JOIN 
				  (
				  	SELECT 
					  unu.* 
					FROM
					  (SELECT 
					    ".$db["master"].".fa_lokasi.sid AS sid_lokasi,
					    ".$db["master"].".fa_lokasi.fixed_asset_id,
					    ".$db["master"].".fa_lokasi.lokasi_date,
					    ".$db["master"].".fa_lokasi.depo_id,
					    ".$db["master"].".fa_lokasi.ruangan_id,
					    ".$db["master"].".fa_lokasi.mutasi_no 
					  FROM
					    ".$db["master"].".fa_lokasi 
					  WHERE 1 
					    AND ".$db["master"].".fa_lokasi.mutasi_no = '' 
					  ORDER BY ".$db["master"].".fa_lokasi.lokasi_date DESC,
					    ".$db["master"].".fa_lokasi.sid DESC) AS unu 
					WHERE 1 
					GROUP BY unu.fixed_asset_id
				  ) AS tb_lokasi 
				  	ON fa_fixed_asset.fixed_asset_id = tb_lokasi.fixed_asset_id
				  	
				  INNER JOIN 
				    (SELECT 
				      uni.* 
				    FROM
				      (SELECT 
				        ".$db["master"].".fa_penanggung_jawab.sid AS sid_pj,
				        ".$db["master"].".fa_penanggung_jawab.fixed_asset_id,
				        ".$db["master"].".fa_penanggung_jawab.pj_date,
				        ".$db["master"].".fa_penanggung_jawab.type_pj,
				        ".$db["master"].".fa_penanggung_jawab.employee_id,
				        ".$db["master"].".fa_penanggung_jawab.mutasi_no 
				      FROM
				        ".$db["master"].".fa_penanggung_jawab 
				      WHERE 1 
				        AND ".$db["master"].".fa_penanggung_jawab.mutasi_no = '' 
				      ORDER BY ".$db["master"].".fa_penanggung_jawab.pj_date DESC,
				        ".$db["master"].".fa_penanggung_jawab.sid DESC) AS uni 
				    WHERE 1 
				    GROUP BY uni.fixed_asset_id) AS tb_pj 
				    ON fa_fixed_asset.fixed_asset_id = tb_pj.fixed_asset_id 
				    	
				  INNER JOIN ".$db["master"].".depo 
				    ON ".$db["master"].".tb_lokasi.depo_id = ".$db["master"].".depo.depo_id 
				  INNER JOIN ".$db["master"].".hrd_cabang 
				    ON ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id 
				WHERE 1
					AND ".$db["master"].".fa_fixed_asset.fixed_asset_id='".$v_fixed_asset_id."'
				LIMIT 0,01
            "; 
            $qry_curr = mysql_query($q);
            $data = mysql_fetch_array($qry_curr);  
        }     
		
		// cabang
		{
			$q="
				SELECT 
					cabang_id,
					cabang_name 
				FROM 
					".$db["master"].".hrd_cabang 
				ORDER BY 
					cabang_name ASC
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($cabang_id,$cabang_name)=$row;
				
				$arr_data["list_cabang"][$cabang_id]=$cabang_id;
				$arr_data["cabang_name"][$cabang_id]=$cabang_name;
			}
		}
		
		// depo
		{
			$q="
				SELECT 
					depo_id,
					depo_name 
				FROM 
					".$db["master"].".depo 
				WHERE 
					1
					AND depo.cabang_id='".$data["cabang_id"]."' 
				ORDER BY 
					depo_name ASC 
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($depo_id,$depo_name)=$row;
				
				$arr_data["list_depo"][$depo_id]=$depo_id;
				$arr_data["depo_name"][$depo_id]=$depo_name;
			}
		}
		
		// ruangan
		{
			$q="
				SELECT 
					depo_id,
					ruangan_id,
					ruangan_name 
				FROM 
					".$db["master"].".fa_ruangan 
				WHERE 1 
					AND fa_ruangan.depo_id='".$data["depo_id"]."' 
				ORDER BY 
					ruangan_name ASC 
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($depo_id,$ruangan_id,$ruangan_name)=$row;
				
				$arr_data["list_ruangan"][$ruangan_id]=$ruangan_id;
				$arr_data["ruangan_name"][$ruangan_id]=$ruangan_name;
			}
		}
		
		// metode penyusutan
		{
			$q="SELECT * FROM ".$db["master"].".fa_metode_penyusutan ORDER BY metode_penyusutan_name ASC";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($metode_penyusutan_id,$metode_penyusutan_name,$total_bulan)=$row;
				
				$arr_data["list_metode_penyusutan"][$metode_penyusutan_id]=$metode_penyusutan_id;
				$arr_data["metode_penyusutan_name"][$metode_penyusutan_id]=$metode_penyusutan_name;
				$arr_data["total_bulan"][$metode_penyusutan_id]=$total_bulan;
			}
		}
		
		// penanggung jawab
		{
			$q="SELECT fixed_asset_id FROM ".$db["master"].".fa_penanggung_jawab WHERE fixed_asset_id='".$data["fixed_asset_id"]."'";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($fixed_asset_id)=$row;
				
				$arr_data["total_fixed_asset_pj"][$fixed_asset_id]=$fixed_asset_id;
			}
		}
		
		// lokasi
		{
			$q="
				SELECT 
				  fixed_asset_id 
				FROM
				  ".$db["master"].".fa_lokasi 
				WHERE fixed_asset_id = '".$data["fixed_asset_id"]."'
			";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($fixed_asset_id)=$row;
				
				$arr_data["total_fixed_asset_lokasi"][$fixed_asset_id]=$fixed_asset_id;
			}
		}
		
		$emp_pj = select_employee($data["employee_id"],date("d/m/Y"));
		
		if($data["type_pj"]=="personal")
		{
			$chked_pj_personal = "checked=\"checked\"";

			$td_empl_name .= "display: ;";
			$colspan_desc .= "0";
			$td_jabatan .= "display: ;";
		}
		
		if($data["type_pj"]=="umum")
		{
			$chked_pj_umum = "checked=\"checked\"";

			$td_empl_name .= "display:none;";
			$colspan_desc .= "3";
			$td_jabatan .= "display:none;";
		}

		if(count($arr_data["total_fixed_asset_pj"])*1>1)
		{
			$where_pj = "
			<input type='hidden' name='v_penanggung_jawab' id='v_penanggung_jawab' value='".$data["type_pj"]."' />
			<font style='text-transform: capitalize'>".$data["type_pj"]."</font>";
			
			$where_pj_name = 
			"
				<input type=\"hidden\" class=\"text\" name=\"v_emp_id\" id=\"v_emp_id\" value=\"".$data["employee_id"]."\">
				<input type='hidden' name='v_emp_name' id='v_emp_name' value='".$emp_pj["employee_name"]."' />".$emp_pj["employee_name"];
		}
		else
		{
			$where_pj = "
				<label><input type=\"radio\" name=\"v_penanggung_jawab\" id=\"v_penanggung_jawab\" value=\"personal\" onclick=\"change_pj(this.value)\" ".$chked_pj_personal."/>&nbsp;Personal</label>&nbsp;
                <label><input type=\"radio\" name=\"v_penanggung_jawab\" id=\"v_penanggung_jawab\" value=\"umum\" onclick=\"change_pj(this.value)\" ".$chked_pj_umum."/>&nbsp;Umum</label>
            ";
			
			$where_pj_name = "
				<input type=\"hidden\" name=\"v_emp_id\" id=\"v_emp_id\" value=\"".$data["employee_id"]."\">
           		<input type=\"text\" class=\"form-control-new\" name=\"v_emp_name\" id=\"v_emp_name\" size=\"30\" onclick=\"pop_up_employee()\" value=\"".$emp_pj["employee_name"]."\">
            	
            	<button type=\"button\" class=\"btn btn-info btn-sm sm-new\" title=\"Search Employee\" onclick=\"pop_up_employee()\">
					<i class=\"entypo-Search\"></i>
				</button>
            ";
		}
		
        if($data["type_id"]*1==1) // type = kendaraan
        {
        	$where_desc .="
				<table class='table table-bordered responsive'>
	            	<tr>
		                <td class='title_table' width='150'>Kendaraan Merek</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_kendaraan_merek' id='v_kendaraan_merek' maxlength='30' size='30' value='".$data["desc_kendaraan_merek"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Kendaraan Jenis</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_kendaraan_jenis' id='v_kendaraan_jenis' maxlength='30' size='30' value='".$data["desc_kendaraan_jenis"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Kendaraan Warna</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_kendaraan_warna' id='v_kendaraan_warna' maxlength='30' size='30' value='".$data["desc_kendaraan_warna"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Kendaraan CC</td>
		                <td>
		                    <input style='text-align: right;' type='text' class='form-control-new' name='v_kendaraan_cc' id='v_kendaraan_cc' maxlength='30' size='30' onblur='toFormat(\"v_kendaraan_cc\")' value='".$data["desc_kendaraan_cc"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>No Polisi</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_no_polisi' id='v_no_polisi' maxlength='30' size='30' value='".$data["desc_kendaraan_nopol"]."'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>No Rangka</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_no_rangka' id='v_no_rangka' maxlength='50' size='30' value='".$data["desc_kendaraan_no_rangka"]."'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>No Mesin</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_no_mesin' id='v_no_mesin' maxlength='50' size='30' value='".$data["desc_kendaraan_no_mesin"]."'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Tgl STNK</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_tgl_stnk' id='v_tgl_stnk' maxlength='10' size='15' value='".format_show_date($data["desc_kendaraan_tgl_stnk"])."'>
		                    <img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_tgl_stnk, \"dd/mm/yyyy\");'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Tgl Pajak</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_tgl_pajak' id='v_tgl_pajak' maxlength='10' size='15' value='".format_show_date($data["desc_kendaraan_tgl_pajak"])."'>
		                    <img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_tgl_pajak, \"dd/mm/yyyy\");'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>No BPKB</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_no_bpkb' id='v_no_bpkb' maxlength='50' size='30' value='".$data["desc_kendaraan_no_bpkb"]."'>
		                </td> 
		            </tr>
	        	</table>
	        ";
			
		}
		else if($data["type_id"]*1==7) // type = Tanah/Gedung
		{
			$echo_asuransi_par="";
			$style_asuransi_par = "style='display:none;'";	
			if($data["desc_gedung_asuransi_par"]==1)
			{
				$echo_asuransi_par = 'checked="checked"';
				$style_asuransi_par = "style='display: ;'";	
			}
			
			$echo_asuransi_earthquake="";
			$style_asuransi_earthquake = "style='display:none;'";	
			if($data["desc_gedung_asuransi_earthquake"]==1)
			{
				$echo_asuransi_earthquake = 'checked="checked"';
				$style_asuransi_earthquake = "style='display: ;'";		
			}
			
        	$where_desc .="
				<table class='table table-bordered responsive'>
		            <tr>
		                <td class='title_table' width='110'>Nama Pemilik</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_nama_pemilik' id='v_gedung_nama_pemilik' maxlength='50' size='30' value='".$data["desc_gedung_nama_pemilik"]."'>
		                </td> 
		                
		                <td class='title_table' >Desa</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_desa' id='v_gedung_desa' maxlength='50' size='30' value='".$data["desc_gedung_desa"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Luas Bangunan</td>
		                <td>
		                    <input style='text-align: right;' type='text' class='form-control-new' name='v_gedung_luas_bangunan' id='v_luas_bangunan' maxlength='30' size='30' onblur='toFormat('v_luas_bangunan')' value='".$data["desc_gedung_luas_bangunan"]."'>
		                </td>  
		                
		                <td class='title_table'>Kecamatan</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_kecamatan' id='v_gedung_kecamatan' maxlength='50' size='30' value='".$data["desc_gedung_kecamatan"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Luas Tanah</td>
		                <td>
		                    <input style='text-align: right;' type='text' class='form-control-new' name='v_gedung_luas_tanah' id='v_gedung_luas_tanah' maxlength='30' size='30' onblur='toFormat('v_gedung_luas_tanah')' value='".$data["desc_gedung_luas_tanah"]."'>
		                </td> 
		                
		                <td class='title_table'>Kota</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_kota' id='v_gedung_kota' maxlength='50' size='30' value='".$data["desc_gedung_kota"]."'>
		                </td>
		            </tr>
		            <tr>
		                <td class='title_table'>Tinggi Bangunan</td>
		                <td>
		                    <input style='text-align: right;' type='text' class='form-control-new' name='v_gedung_tinggi_bangunan' id='v_gedung_tinggi_bangunan' size='30' onblur='toFormat('v_gedung_tinggi_bangunan')' value='".$data["desc_gedung_tinggi_bangunan"]."'>
		                </td> 
		                
		                <td class='title_table'>Kabupaten</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_kabupaten' id='v_gedung_kabupaten' maxlength='50' size='30' value='".$data["desc_gedung_kabupaten"]."'>
		                </td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Type Asuransi</td>
		                <td>
		                    <label><input type='checkbox' name='chk_gedung_par' value='gedung_par' onclick='change_par()' id='chk_gedung_par' ".$echo_asuransi_par."/>&nbsp;PAR</label>&nbsp;
		                    <label><input type='checkbox' name='chk_gedung_earthquake' value='gedung_earthquake' onclick='change_earthquake()' id='chk_gedung_earthquake' ".$echo_asuransi_earthquake."/>&nbsp;Earthquake</label>
		                </td>
		                
		                <td class='title_table'>provinsi</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gedung_provinsi' id='v_gedung_provinsi' maxlength='50' size='30' value='".$data["desc_gedung_provinsi"]."'>
		                </td>  
		            </tr>
            
		            <tr id='tr_no_asuransi_par' ".$style_asuransi_par.">
		                <td class='title_table' >No Asuransi PAR</td>
		                <td colspan='3'>
		                    <input type='text' class='form-control-new ' name='v_gedung_no_asuransi_par' id='v_gedung_no_asuransi_par' maxlength='30' size='30' value='".$data["desc_gedung_no_asuransi_par"]."'>
		                </td>  
		            </tr>
		            
		            <tr id='tr_periode_par' ".$style_asuransi_par.">
		                <td class='title_table'>Periode PAR</td>
		                <td colspan='3'>
		                	<input type='text' class='textform-control-new' name='v_gedung_start_date_par' id='v_gedung_start_date_par' maxlength='10' size='10' value='".format_show_date($data["desc_gedung_asuransi_start_date_par"])."'>
		                	<img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_gedung_start_date_par, 'dd/mm/yyyy');'>
		            		&nbsp;s/d&nbsp;
		                	<input type='text' class='form-control-new' name='v_gedung_end_date_par' id='v_gedung_end_date_par' maxlength='10' size='10' value='".format_show_date($data["desc_gedung_asuransi_end_date_par"])."'>
		                	<img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_gedung_end_date_par, 'dd/mm/yyyy');'>
		            	</td> 
		            </tr>
		            
		            <tr id='tr_no_asuransi_earthquake' ".$style_asuransi_earthquake.">
		                <td class='title_table' >No Asuransi Earthquake</td>
		                <td colspan='3'>
		                    <input type='text' class='form-control-new' name='v_gedung_no_asuransi_earthquake' id='v_gedung_no_asuransi_earthquake' maxlength='30' size='30' value='".$data["desc_gedung_no_asuransi_earthquake"]."'>
		                </td> 
		            </tr>
		            
		            <tr id='tr_periode_earthquake' ".$style_asuransi_earthquake.">
		                <td class='title_table'>Periode Earthquake</td>
		                <td colspan='3'>
		                	<input type='text' class='form-control-new' name='v_gedung_start_date_earthquake' id='v_gedung_start_date_earthquake' maxlength='10' size='10' value='".format_show_date($data["desc_gedung_asuransi_start_date_earthquake"])."'>
		                	<img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_gedung_start_date_earthquake, 'dd/mm/yyyy');'>
		            		&nbsp;s/d&nbsp;
		                	<input type='text' class='form-control-new' name='v_gedung_end_date_earthquake' id='v_gedung_end_date_earthquake' maxlength='10' size='10' value='".format_show_date($data["desc_gedung_asuransi_end_date_earthquake"])."'>
		                	<img src='images/calendar.gif' style='cursor: pointer;' onClick='popUpCalendar(this, theform.v_gedung_end_date_earthquake, 'dd/mm/yyyy');'>
		            	</td> 
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Fungsi</td>
		                <td colspan='3'>
		                    <input type='text' class='form-control-new' name='v_gedung_fungsi' id='v_gedung_fungsi' maxlength='50' size='30' value='".$data["desc_gedung_fungsi"]."'>
		                </td> 
		            </tr>
		        </table>
	        ";
			
		}
        else if($data["type_id"]*1==4) // type = Gudang
		{
			$echo_asuransi_par="";
			$style_asuransi_par = "style='display:none;'";	
			$td_no_asuransi_par = "
				<td id='td_no_asuransi_par_1'>&nbsp;</td>
		    	<td id='td_no_asuransi_par_2'>&nbsp;</td>
			";
			$td_periode_asuransi_par = "
				<td id='td_periode_asuransi_par_1'>&nbsp;</td>
	            <td id='td_periode_asuransi_par_2'>&nbsp;</td> 
			";
			if($data["desc_gudang_asuransi_par"]==1)
			{
				$echo_asuransi_par = 'checked="checked"';
				$style_asuransi_par = "style='display: ;'";	
				
				$td_no_asuransi_par = "
					<td id='td_no_asuransi_par_1' class='title_table'>No Asuransi PAR</td>
		            <td id='td_no_asuransi_par_2'><input type='text' class='form-control-new ' name='v_gudang_no_asuransi_par' id='v_gudang_no_asuransi_par' maxlength='30' size='30' value='".$data["desc_gudang_no_asuransi_par"]."'></td>
				";
				
				$td_periode_asuransi_par = "
					<td id='td_periode_asuransi_par_1' class='title_table'>Periode PAR</td>
		            <td id='td_periode_asuransi_par_2'>
		            	<input type='text' class='form-control-new' name='v_gudang_start_date_par' id='v_gudang_start_date_par' maxlength='10' size='10' value='".format_show_date($data["desc_gudang_asuransi_start_date_par"])."'>
				    	<img src='images/calendar.gif' style='cursor: pointer;' onClick=\"popUpCalendar(this, theform.v_gudang_start_date_par, 'dd/mm/yyyy');\">
						&nbsp;s/d&nbsp;
				    	<input type='text' class='form-control-new' name='v_gudang_end_date_par' id='v_gudang_end_date_par' maxlength='10' size='10' value='".format_show_date($data["desc_gudang_asuransi_end_date_par"])."'>
				    	<img src='images/calendar.gif' style='cursor: pointer;' onClick=\"popUpCalendar(this, theform.v_gudang_end_date_par, 'dd/mm/yyyy');\">
		            </td> 
				";
			}
			
			$echo_asuransi_earthquake="";
			$style_asuransi_earthquake = "style='display:none;'";
			$td_no_asuransi_earthquake ="";	
			if($data["desc_gudang_asuransi_earthquake"]==1)
			{
				$echo_asuransi_earthquake = 'checked="checked"';
				$style_asuransi_earthquake = "style='display: ;'";
				
				$td_no_asuransi_earthquake="
				
				";		
			}
			
        	$where_desc .="
				<table class='table table-bordered responsive'>
		            <tr>
		                <td class='title_table'>Luas Bangunan</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gudang_luas_bangunan' id='v_gudang_luas_bangunan' maxlength='30' size='30' onblur='toFormat('v_gudang_luas_bangunan')' value='".$data["desc_gudang_luas_bangunan"]."'>
		                </td>  
		                
		                <td class='title_table'>Desa</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gudang_desa' id='v_gudang_desa' maxlength='50' size='30' value='".$data["desc_gudang_desa"]."'>
		                </td> 
		            </tr>
		            <tr>
		                <td class='title_table'>Luas Tanah</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gudang_luas_tanah' id='v_gudang_luas_tanah' maxlength='30' size='30' onblur='toFormat('v_gudang_luas_tanah')' value='".$data["desc_gudang_luas_tanah"]."'>
		                </td>  
		                
		                <td class='title_table'>Kecamatan</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_gudang_kecamatan' id='v_gudang_kecamatan' maxlength='50' size='30' value='".$data["desc_gudang_kecamatan"]."'>
		                </td>
		            </tr>
            
		            <tr>
		                <td class='title_table'>Type Asuransi</td>
		                <td>
		                    <label><input type='checkbox' name='chk_gudang_par' value='gudang_par' onclick=\"CallAjaxForm('ajax_chk_asuransi', this.value);\" id='chk_gudang_par' ".$echo_asuransi_par."/>&nbsp;PAR</label>&nbsp;
		                    <label><input type='checkbox' name='chk_gudang_earthquaks' value='gudang_earthquaks' onclick=\"CallAjaxForm('ajax_chk_asuransi', this.value);\" id='chk_gudang_earthquaks' ".$echo_asuransi_earthquake."/>&nbsp;Earthquake</label>
		                </td>   
		                
		                <td class='title_table'>Kota</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_gudang_kota' id='v_gudang_kota' maxlength='50' size='30' value='".$data["desc_gudang_kota"]."'>
		                </td>
		            </tr>
            
		            <tr>
		                ".$td_no_asuransi_par."   
		                
		                <td class='title_table'>Kabupaten</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_gudang_kabupaten' id='v_gudang_kabupaten' maxlength='50' size='30' value='".$data["desc_gudang_kabupaten"]."'>
		                </td>
		            </tr>
            
		            <tr>
		                ".$td_periode_asuransi_par."     
		                
		                <td class='title_table'>Provinsi</td>
		                <td>
		                    <input type='text' class='form-control-new ' name='v_gudang_provinsi' id='v_gudang_provinsi' maxlength='50' size='30' value='".$data["desc_gudang_provinsi"]."'>
		                </td>
		            </tr>
            
		            <tr id='tr_no_asuransi_earthquake' ".$style_asuransi_earthquake.">
		                <td id='td_no_asuransi_earthquake_1' class='title_table'>No Asuransi Earthquake</td>
		                <td id='td_no_asuransi_earthquake_2' colspan='3'><input type='text' class='text ' name='v_gudang_no_asuransi_earthquake' id='v_gudang_no_asuransi_earthquake' maxlength='30' size='30' value='".$data["desc_gudang_no_asuransi_earthquake"]."'></td>   
		            </tr>
		            
		            <tr id='tr_periode_earthquake' ".$style_asuransi_earthquake.">
		                <td id='td_periode_earthquake_1' class='title_table'>Periode Earthquake</td>
		                <td id='td_periode_earthquake_2' colspan='3'>
		                	<input type='text' class='form-control-new' name='v_gudang_start_date_earthquake' id='v_gudang_start_date_earthquake' maxlength='10' size='10' value='".format_show_date($data["desc_gudang_asuransi_start_date_earthquake"])."'>
					    	<img src='images/calendar.gif' style='cursor: pointer;' onClick=\"popUpCalendar(this, theform.v_gudang_start_date_earthquake, 'dd/mm/yyyy');\">
							&nbsp;s/d&nbsp;
					    	<input type='text' class='form-control-new' name='v_gudang_end_date_earthquake' id='v_gudang_end_date_earthquake' maxlength='10' size='10' value='".format_show_date($data["desc_gudang_asuransi_end_date_earthquake"])."'>
					    	<img src='images/calendar.gif' style='cursor: pointer;' onClick=\"popUpCalendar(this, theform.v_gudang_end_date_earthquake, 'dd/mm/yyyy');\">
		                </td>  
		            </tr>
		        </table>
	        ";
			
		}              
        else if($data["type_id"]*1==2 || $data["type_id"]*1==3 || $data["type_id"]*1==6) // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)
        {
        	$where_desc .= "
        		<table class='table table-bordered responsive'>
		            <tr>
		                <td class='title_table' width='100'>Merk</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_desc_merek' id='v_desc_merek' size='50' value='".$data["desc_merek"]."'>
		                </td>  
		            </tr>
		            <tr>
		                <td class='title_table'>Tipe</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_desc_tipe' id='v_desc_tipe' size='50' value='".$data["desc_tipe"]."'>
		                </td>  
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Jenis</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_desc_jenis' id='v_desc_jenis' size='50' value='".$data["desc_jenis"]."'>
		                </td>  
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Warna</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_desc_warna' id='v_desc_warna' size='50' value='".$data["desc_warna"]."'>
		                </td>  
		            </tr>
		            
		            <tr>
		                <td class='title_table'>Tahun</td>
		                <td>
		                    <input type='text' class='form-control-new' name='v_desc_tahun' id='v_desc_tahun' size='50' value='".$data["desc_tahun"]."'>
		                </td>  
		            </tr>
		        </table>
        	";
			
		}
		
        ?> 
        <div class="col-md-12" align="left">
        	
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			                                           
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_fixed_asset_id" id="v_fixed_asset_id" value="<?php echo $data["fixed_asset_id"];?>">   
        <input type="hidden" name="v_photo_old" id="v_photo_old" value="<?php echo $data["photo"];?>">    
        <input type="hidden" name="v_sid_pj" id="v_sid_pj" value="<?php echo $data["sid_pj"];?>">        
        <input type="hidden" name="v_sid_lokasi" id="v_sid_lokasi" value="<?php echo $data["sid_lokasi"];?>">    
        <input type="hidden" name="v_jumlah_pj" id="v_jumlah_pj" value="<?php echo count($arr_data["total_fixed_asset_pj"]);?>">          
        
        <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">FA Code</td>
                <td width="40%"><b><?php echo $data["fixed_asset_code"]; ?></b></td>
                
                <td class="title_table" width="200">Lokasi</td>
                <td>
                <?php
                if(count($arr_data["total_fixed_asset_lokasi"])*1>1)
                {
					?>
					<input type="hidden" name="v_cabang_id" id="v_cabang_id" value="<?php echo $data["cabang_id"];?>" />
					<input type="hidden" name="v_depo_id" id="v_depo_id" value="<?php echo $data["depo_id"];?>" />
					<?php	
					echo $data["cabang_name"]." :: ".$data["depo_name"];
				}
				else
				{
                ?>
                	<select name="v_cabang_id" id="v_cabang_id" class="form-control-new" onchange="CallAjaxForm('ajax_depo', this.value);">  
                        <option value="">-</option>
                        <?php 
                        	foreach($arr_data["list_cabang"] as $cabang_id => $val)
							{
								$cabang_name = $arr_data["cabang_name"][$cabang_id];
								$cabang_name = str_replace("Cabang","",$cabang_name);
								
								$selected="";
								if($data["cabang_id"] == $cabang_id){
							   		$selected = "selected='selected'";
							   	}
							   	
								?>
                                <option <?php echo $selected; ?> value="<?php echo $cabang_id; ?>"><?php echo $cabang_name; ?></option>
                                <?php	
							}
                        ?>
                    </select>
                    
                    <span id="span_depo">
                    	<select name="v_depo_id" id="v_depo_id" class="form-control-new" onchange="CallAjaxForm('ajax_ruangan', this.value);">  
                    		<option value="">-</option>
					        <?php 
				                foreach($arr_data["list_depo"] AS $depo_id => $val)
				                {
									$depo_name = $arr_data["depo_name"][$depo_id];
									
				                	$selected="";
									if($data["depo_id"] == $depo_id){
								   		$selected = "selected='selected'";
								   	}
								   	
				                    ?>
				                    <option <?php echo $selected; ?> value="<?php echo $depo_id; ?>"><?php echo $depo_name; ?></option>
				                    <?php
				                }
					        ?>
					    </select>
                    </span>
                    <?php
				}
                ?>
                </td> 
            </tr>  

            <tr>
                <td class="title_table">Proposal</td>
                <td><b><?php echo $data["NoProposal"];?></b></td> 
                
                <td class="title_table">Ruangan</td>
                <td id="td_ruangan_asset">
                <?php
                if(count($arr_data["total_fixed_asset_lokasi"])*1>1)
                {
                	$q_ru = "
                		SELECT 
                			ruangan_name 
                		FROM 
                			".$data["ruangan_id"].".fa_ruangan 
                		WHERE 1
                			AND ruangan_id='".$data["ruangan_id"]."'
                	";
                	$qry_ru = mysql_query($q_ru);
                	$tb_ru = mysql_fetch_array($qry_ru);
                	
					?>
					<input type="hidden" name="v_ruangan_id" id="v_ruangan_id" value="<?php echo $data["ruangan_id"];?>" />
					<?php	
					echo $tb_ru["ruangan_name"];
				}
				else
				{
                ?>
                	<select name="v_ruangan_id" id="v_ruangan_id" class="form-control-new">  
			            <?php 
			            if(count($arr_data["list_ruangan"])==0)
		            	{
							echo "<option value=\"\">-</option>";	
						}
						else
						{
							echo "<option value=\"\">-</option>";
			                foreach($arr_data["list_ruangan"] AS $ruangan_id => $val)
			                {
								$ruangan_name = $arr_data["ruangan_name"][$ruangan_id];
								
			                	$selected="";
			                	if($data["ruangan_id"] == $ruangan_id)
								{
									$selected = "selected='selected'";	
							   	}
			                    ?>
			                    <option <?php echo $selected; ?> value="<?php echo $ruangan_id; ?>"><?php echo $ruangan_name; ?></option>
			                    <?php
			                }
			            }
			            ?>
			        </select>
			        <?php
			        }
			    ?>
                </td>
            </tr>
                
            <tr>
                <td class="title_table">Kategori</td>
                <td><?php echo $data["kategory_name"];?></td>
                
                <td class="title_table">Penanggung Jawab</td>
                <td><?php echo $where_pj; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Type</td>
                <td id="td_type" colspan="<?php echo $colspan_desc; ?>">
        			<input type="hidden" name="v_type_id" id="v_type_id" value="<?php echo $data["type_id"];?>">    
                	<?php echo $data["type_name"];?>
                </td>
                
                <td class="title_table" width="150" id="td_empl_name_1" style="<?php echo $td_empl_name; ?>">Nama</td>
                <td id="td_empl_name_2" style="<?php echo $td_empl_name;?>"><?php echo $where_pj_name;?></td>
            </tr>
                
            <tr>
                <td class="title_table">Sub Type</td>
                <td id="td_sub_type" colspan="<?php echo $colspan_desc; ?>"><?php echo $data["sub_type_name"];?></td>
	            
                <td class="title_table" width="150" id="td_jabatan_1" style="<?php echo $td_jabatan; ?>">Jabatan</td>
                <td id="td_jabatan_2" style="<?php echo $td_jabatan; ?>"><?php echo $emp_pj["jabatan_name"];?></td> 
            </tr>
            
            <tr>
            	<td class="title_table">Deskripsi</td>
	            <td id="td_deskripsi" colspan="3"><?php echo $where_desc; ?></td>
            </tr>
            
            <tr>
                <td class="title_table" width="150">Tanggal Beli</td>
                <td id="td_tanggal_beli"><?php echo format_show_date($data["tanggal_beli"]);?></td> 
            	
            	<td colspan="2"><b>Nilai Asset</b>&nbsp;<i>*) Diinput oleh Acct</i></td>
            </tr>
            
            <tr>
            	<td class="title_table">Photo</td>
	            <td>
	            	
	            	<div class="fileinput fileinput-new" data-provides="fileinput">
						<span class="btn btn-info btn-file sm-new">
							<span class="fileinput-new">Select file</span>
							<span class="fileinput-exists">Change</span>
							<input type="file" name='fupload' id='fupload' accept=".pdf, .jpeg, .jpg, .png">
						</span>
						<span class="fileinput-filename"></span>
						<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
					</div> 
	            	
	            	<?php
	            	if($data["photo"])
	            	{
						?>
						<label style="cursor: pointer;">
							<img src="images/view.png" alt="Photo Asset" onclick="view_photo('<?php echo $data["photo"];?>')" title="View Photo"/>
						</label>
						<?php
					}
	            	?>  
	            </td>
	            
                <td class="title_table">Nominal (Rp)</td>
                <td>
                   <input type="text" class="form-control-new" name="v_nominal" id="v_nominal" onblur="toFormat2('v_nominal')" size="30" style="text-align: right;" value="<?php echo format_number($data["nominal"],2);?>">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_keterangan" id="v_keterangan" maxlength="100" style="width: 80%;" value="<?php echo $data["keterangan"];?>">
                </td> 
                
                <td class="title_table">Metode Penyusutan</td>
                <td>
                    <select name="v_metode_penyusutan_id" id="v_metode_penyusutan_id" class="form-control-new">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_metode_penyusutan"] as $metode_penyusutan_id => $val)
                            {
								$metode_penyusutan_name = $arr_data["metode_penyusutan_name"][$metode_penyusutan_id];
								
                                $selected = "";
								if($data["metode_penyusutan_id"] == $metode_penyusutan_id){
							   		$selected = "selected='selected'";
							   	}	
							   		   	
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $metode_penyusutan_id; ?>"><?php echo $metode_penyusutan_name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
               
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                	<button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button>
                	<?php
                	if(count($arr_data["total_fixed_asset_pj"])*1==1)
                	{
                		?>
                		<button type="submit" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $data["fixed_asset_code"]; ?>');" value="Delete">Delete<i class="entypo-trash"></i></button>
                		<?php
					}
                	?>
               </td>
            </tr> 
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    else if($ajax=="ajax_proposal")
    {
    	$v_receipt_no = $_GET["v_receipt_no"];
    	
    	$data_receipt = select_fa_receipt($v_receipt_no);
		
		echo "<input type=\"hidden\" name=\"v_no_proposal\" id=\"v_no_proposal\" value=\"".$data_receipt["pono"]."\"/>";
		echo $data_receipt["pono"];
		?>
		||
		<?php
		echo "<input type=\"hidden\" name=\"v_receipt_date\" id=\"v_receipt_date\" value=\"".format_show_date($data_receipt["receipt_date"])."\"/>";
		echo format_show_date($data_receipt["receipt_date"]);
	}
    
    else if($ajax=="ajax_type")
    {
    	$v_kategory_id = $_GET["v_kategory_id"];
    	
    	$q="
    		SELECT 
			  fa_type.type_id,
			  fa_type.kategory_id,
			  fa_type.group_asset_id,
			  fa_type.type_name 
			FROM
			  fa_type 
			WHERE 1 
			  AND fa_type.kategory_id = '".$v_kategory_id."' 
			ORDER BY fa_type.type_name ASC
    	";
    	$qry = mysql_query($q);
    	while($row = mysql_fetch_array($qry))
    	{
			list($type_id,$kategory_id,$group_asset_id,$type_name)=$row;
			
			$arr_data["list_type"][$type_id]=$type_id;
			$arr_data["kategory_id"][$type_id]=$kategory_id;
			$arr_data["group_asset_id"][$type_id]=$group_asset_id;
			$arr_data["type_name"][$type_id]=$type_name;
		}
    	
    	//$tabel_type = $selecttb->selectTable("fa_type","type_id,kategory_id,group_asset_id,type_name","fa_type.kategory_id='".$v_kategory_id."'","type_name ASC");
    	
		if(count($arr_data["list_type"])==0)
        {
			$style_type = "style=\"width: 70px;\"";	
		}
		
		?>
        <select name="v_type_id" id="v_type_id" class="form-control-new" onchange="CallAjaxForm('ajax_sub_type', this.value);" <?php echo $style_type; ?>>  
            <?php 
            	if(count($arr_data["list_type"])==0)
            	{
					echo "<option value=\"\">-</option>";	
				}
				else
				{
					echo "<option value=\"\">-</option>";	
	                foreach($arr_data["list_type"] as $type_id => $val)
	                {
						$type_name = $arr_data["type_name"][$type_id];
						
	                    ?>
	                    <option value="<?php echo $type_id; ?>"><?php echo $type_name; ?></option>
	                    <?php
	                }
				}
            ?>
        </select>
		<?php
	}
    
    else if($ajax=="ajax_sub_type")
    {
    	$v_no_proposal = $_GET["v_no_proposal"];
    	$v_type_id = $_GET["v_type_id"];
    	
    	if($v_no_proposal)
    	{
    		$data_receipt = select_fa_receipt($v_no_proposal);
		}
		
		$q="
			SELECT 
			  fa_sub_type.sub_type_id,
			  fa_sub_type.type_id,
			  fa_sub_type.sub_type_name 
			FROM
			  fa_sub_type 
			WHERE 1 
			  AND fa_sub_type.type_id = '".$v_type_id."' 
			ORDER BY 
			  fa_sub_type.sub_type_name ASC 
    	";
    	$qry = mysql_query($q);
    	while($row = mysql_fetch_array($qry))
    	{
			list($sub_type_id,$type_id,$sub_type_name)=$row;
			
			$arr_data["list_sub_type"][$sub_type_id]=$type_id;
			$arr_data["type_id"][$sub_type_id]=$type_id;
			$arr_data["sub_type_name"][$sub_type_id]=$sub_type_name;
		}
    	
    	//$tabel_sub_type = $selecttb->selectTable("fa_sub_type","sub_type_id,type_id,sub_type_name","fa_sub_type.type_id='".$v_type_id."'","sub_type_name ASC");
    	
		if(count($arr_data["list_sub_type"])==0)
        {
			$style_sub_type = "style=\"width: 70px;\"";	
		}
		
		?>
        <select name="v_sub_type_id" id="v_sub_type_id" class="form-control-new" <?php echo $style_sub_type; ?>>  
            <?php 
            	if(count($arr_data["list_sub_type"])==0)
            	{
					echo "<option value=\"\">-</option>";	
				}
				else
				{
					echo "<option value=\"\">-</option>";	
	                foreach($arr_data["list_sub_type"] as $sub_type_id => $val)
	                {
						$sub_type_name = $arr_data["sub_type_name"][$sub_type_id];
			
	                    ?>
	                    <option value="<?php echo $sub_type_id; ?>"><?php echo $sub_type_name; ?></option>
	                    <?php
	                }
				}
            ?>
        </select>
		<?php
		?>
		||
		<?php
		if($v_type_id=="1") // type = kendaraan
		{               
		?>
    	<table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="150">Kendaraan Merek</td>
                <td>
                    <input type="text" class="form-control-new " name="v_kendaraan_merek" id="v_kendaraan_merek" maxlength="30" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table" >Kendaraan Jenis</td>
                <td>
                    <input type="text" class="form-control-new " name="v_kendaraan_jenis" id="v_kendaraan_jenis" maxlength="30" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table" >Kendaraan Warna</td>
                <td>
                    <input type="text" class="form-control-new " name="v_kendaraan_warna" id="v_kendaraan_warna" maxlength="30" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table" >Kendaraan CC</td>
                <td>
                    <input style="text-align: right;" type="text" class="form-control-new" name="v_kendaraan_cc" id="v_kendaraan_cc" maxlength="30" size="30" onblur="toFormat('v_kendaraan_cc')">
                </td> 
            </tr>
            <tr>
                <td class="title_table" >No Polisi</td>
                <td>
                    <input type="text" class="form-control-new " name="v_no_polisi" id="v_no_polisi" maxlength="30" size="30">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table" >No Rangka</td>
                <td>
                    <input type="text" class="form-control-new " name="v_no_rangka" id="v_no_rangka" maxlength="50" size="30">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table" >No Mesin</td>
                <td>
                    <input type="text" class="form-control-new " name="v_no_mesin" id="v_no_mesin" maxlength="50" size="30">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table" >Tgl STNK</td>
                <td>
                    <input type="text" class="form-control-new " name="v_tgl_stnk" id="v_tgl_stnk" maxlength="10" size="15">
                    <img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_tgl_stnk, 'dd/mm/yyyy');">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table" >Tgl Pajak</td>
                <td>
                    <input type="text" class="form-control-new " name="v_tgl_pajak" id="v_tgl_pajak" maxlength="10" size="15">
                    <img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_tgl_pajak, 'dd/mm/yyyy');">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table" >No BPKB</td>
                <td>
                    <input type="text" class="form-control-new " name="v_no_bpkb" id="v_no_bpkb" maxlength="50" size="30">
                </td> 
            </tr>
        </table>
        <?php
        }
        else if($v_type_id=="7") // type = Gedung/Tanah
        {               
		?>
    	<table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="110">Nama Pemilik</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gedung_nama_pemilik" id="v_gedung_nama_pemilik" maxlength="50" size="30">
                </td> 
                
                <td class="title_table">Desa</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gedung_desa" id="v_gedung_desa" maxlength="50" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table">Luas Bangunan</td>
                <td>
                    <input style="text-align: right;"  type="text" class="form-control-new " name="v_gedung_luas_bangunan" id="v_luas_bangunan" maxlength="30" size="30" onblur="toFormat('v_luas_bangunan')">
                </td> 
                
                <td class="title_table">Kecamatan</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gedung_kecamatan" id="v_gedung_kecamatan" maxlength="50" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table">Luas Tanah</td>
                <td>
                    <input style="text-align: right;" type="text" class="form-control-new " name="v_gedung_luas_tanah" id="v_gedung_luas_tanah" maxlength="30" size="30" onblur="toFormat('v_gedung_luas_tanah')">
                </td> 
                
                <td class="title_table">Kota</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gedung_kota" id="v_gedung_kota" maxlength="50" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table">Tinggi Bangunan</td>
                <td>
                    <input style="text-align: right;" type="text" class="form-control-new " name="v_gedung_tinggi_bangunan" id="v_gedung_tinggi_bangunan" size="30" onblur="toFormat('v_gedung_tinggi_bangunan')">
                </td> 
                
                <td class="title_table">Kabupaten</td>
                <td>
                    <input type="text" class="form-control-new" name="v_gedung_kabupaten" id="v_gedung_kabupaten" maxlength="50" size="30">
                </td> 
            </tr>
            
            <tr>
                <td class="title_table">Type Asuransi</td>
                <td>
                    <label><input type="checkbox" name="chk_gedung_par" value="gedung_par" onclick="change_par()" id="chk_gedung_par"/>&nbsp;PAR</label>&nbsp;
                    <label><input type="checkbox" name="chk_gedung_earthquake" value="gedung_earthquake" onclick="change_earthquake()" id="chk_gedung_earthquake"/>&nbsp;Earthquake</label>
                </td>
                
                <td class="title_table">provinsi</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gedung_provinsi" id="v_gedung_provinsi" maxlength="50" size="30">
                </td>  
            </tr>
            
            <tr class="title_table" id="tr_no_asuransi_par" style="display: none;">
                <td>No Asuransi PAR</td>
                <td colspan="3">
                    <input type="text" class="form-control-new " name="v_gedung_no_asuransi_par" id="v_gedung_no_asuransi_par" maxlength="50" size="30">
                </td>  
            </tr>
            
            <tr class="title_table" id="tr_periode_par" style="display: none;">
                <td>Periode PAR</td>
                <td colspan="3">
                	<input type="text" class="form-control-new " name="v_gedung_start_date_par" id="v_gedung_start_date_par" maxlength="10" size="10">
                	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gedung_start_date_par, 'dd/mm/yyyy');">
            		&nbsp;s/d&nbsp;
                	<input type="text" class="form-control-new " name="v_gedung_end_date_par" id="v_gedung_end_date_par" maxlength="10" size="10">
                	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gedung_end_date_par, 'dd/mm/yyyy');">
            	</td> 
            </tr>
            
            <tr class="title_table" id="tr_no_asuransi_earthquake" style="display: none;">
                <td>No Asuransi Earthquake</td>
                <td colspan="3">
                    <input type="text" class="form-control-new " name="v_gedung_no_asuransi_earthquake" id="v_gedung_no_asuransi_earthquake" maxlength="50" size="30">
                </td> 
            </tr>
            
            <tr class="title_table" id="tr_periode_earthquake" style="display: none;">
                <td>Periode Earthquake</td>
                <td colspan="3">
                	<input type="text" class="form-control-new " name="v_gedung_start_date_earthquake" id="v_gedung_start_date_earthquake" maxlength="10" size="10">
                	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gedung_start_date_earthquake, 'dd/mm/yyyy');">
            		&nbsp;s/d&nbsp;
                	<input type="text" class="form-control-new " name="v_gedung_end_date_earthquake" id="v_gedung_end_date_earthquake" maxlength="10" size="10">
                	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gedung_end_date_earthquake, 'dd/mm/yyyy');">
            	</td> 
            </tr>
            
            <tr class="title_table">
                <td>Fungsi</td>
                <td colspan="3">
                    <input type="text" class="form-control-new " name="v_gedung_fungsi" id="v_gedung_fungsi" maxlength="50" size="30">
                </td> 
            </tr>
        </table>
        <?php
        }
        else if($v_type_id=="4") // type = Gudang
        {               
		?>
    	<table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="180">Luas Bangunan</td>
                <td width="300">
                    <input type="text" class="form-control-new " name="v_gudang_luas_bangunan" id="v_gudang_luas_bangunan" maxlength="30" size="30" onblur="toFormat('v_gudang_luas_bangunan')">
                </td>  
                
                <td class="title_table">Desa</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_desa" id="v_gudang_desa" maxlength="50" size="30">
                </td> 
            </tr>
            <tr>
                <td class="title_table">Luas Tanah</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_luas_tanah" id="v_gudang_luas_tanah" maxlength="30" size="30" onblur="toFormat('v_gudang_luas_tanah')">
                </td>  
                
                <td class="title_table">Kecamatan</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_kecamatan" id="v_gudang_kecamatan" maxlength="50" size="30">
                </td>
            </tr>
            
            <tr>
                <td class="title_table">Type Asuransi</td>
                <td>
                    <label><input type="checkbox" name="chk_gudang_par" value="gudang_par" onclick="CallAjaxForm('ajax_chk_asuransi', this.value);" id="chk_gudang_par"/>&nbsp;PAR</label>&nbsp;
                    <label><input type="checkbox" name="chk_gudang_earthquaks" value="gudang_earthquaks" onclick="CallAjaxForm('ajax_chk_asuransi', this.value);" id="chk_gudang_earthquaks"/>&nbsp;Earthquake</label>
                </td>   
                
                <td class="title_table">Kota</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_kota" id="v_gudang_kota" maxlength="50" size="30">
                </td>
            </tr>
            
            <tr>
                <td id="td_no_asuransi_par_1">&nbsp;</td>
                <td id="td_no_asuransi_par_2">&nbsp;</td>   
                
                <td class="title_table">Kabupaten</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_kabupaten" id="v_gudang_kabupaten" maxlength="50" size="30">
                </td>
            </tr>
            
            <tr>
                <td id="td_periode_asuransi_par_1">&nbsp;</td>
                <td id="td_periode_asuransi_par_2">&nbsp;</td>  
                
                <td class="title_table">Provinsi</td>
                <td>
                    <input type="text" class="form-control-new " name="v_gudang_provinsi" id="v_gudang_provinsi" maxlength="50" size="30">
                </td>
            </tr>
            
            <tr id="tr_no_asuransi_earthquake" style="display: none;">
                <td id="td_no_asuransi_earthquake_1">&nbsp;</td>
                <td id="td_no_asuransi_earthquake_2" colspan="3">&nbsp;</td>   
            </tr>
            
            <tr id="tr_periode_earthquake" style="display: none;">
                <td id="td_periode_earthquake_1">&nbsp;</td>
                <td id="td_periode_earthquake_2" colspan="3">&nbsp;</td>  
            </tr>
        </table>
        <?php
        }
        else if($v_type_id=="2" || $v_type_id=="3" || $v_type_id=="6") // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)
        {            
		?>
    	<table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="100">Merk</td>
                <td>
                    <input type="text" class="form-control-new" name="v_desc_merek" id="v_desc_merek" size="50">
                </td>  
            </tr>
            <tr>
                <td class="title_table">Tipe</td>
                <td>
                    <input type="text" class="form-control-new" name="v_desc_tipe" id="v_desc_tipe" size="50">
                </td>  
            </tr>
            
            <tr>
                <td class="title_table">Jenis</td>
                <td>
                    <input type="text" class="form-control-new" name="v_desc_jenis" id="v_desc_jenis" size="50">
                </td>  
            </tr>
            
            <tr>
                <td class="title_table">Warna</td>
                <td>
                    <input type="text" class="form-control-new" name="v_desc_warna" id="v_desc_warna" size="50">
                </td>  
            </tr>
            
            <tr>
                <td class="title_table">Tahun</td>
                <td>
                    <input type="text" class="form-control-new" name="v_desc_tahun" id="v_desc_tahun" size="50">
                </td>  
            </tr>
        </table>
        <?php
		}
        ?>
        ||
        <?php
        if($v_no_proposal)
        {
        	echo "<input type=\"hidden\" name=\"v_receipt_date\" id=\"v_receipt_date\" value=\"".format_show_date($data_receipt["receipt_date"])."\"/>";
			echo format_show_date($data_receipt["receipt_date"]);
		}
		else
		{
			?>
			<input type="text" class="form-control-new" name="v_receipt_date" id="v_receipt_date" maxlength="10" size="10" value="" >
        	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_receipt_date, 'dd/mm/yyyy');">
			<?php
		}
        ?>   
		<?php
	}
    
    else if($ajax=="ajax_depo")
    {
    	$v_cabang_id = $_GET["v_cabang_id"];
    	
    	$q="
			SELECT 
			  depo.depo_id,
			  depo.cabang_id,
			  depo.depo_name 
			FROM
			  depo 
			WHERE 1 
			  AND depo.cabang_id = '".$v_cabang_id."' 
			ORDER BY 
			  depo.depo_name ASC
    	";
    	$qry = mysql_query($q);
    	while($row = mysql_fetch_array($qry))
    	{
			list($depo_id,$cabang_id,$depo_name)=$row;
			
			$arr_data["list_depo"][$depo_id]=$depo_id;
			$arr_data["cabang_id"][$depo_id]=$cabang_id;
			$arr_data["depo_name"][$depo_id]=$depo_name;
		}
		
    	//$tabel_depo = $selecttb->selectTable("depo","depo_id,cabang_id,depo_name","depo.cabang_id='".$v_cabang_id."'","depo_name ASC");
    	
		if(count($arr_data["list_depo"])==0)
        {
			$style_type = "style=\"width: 70px;\"";	
		}
		
		?>
        <select name="v_depo_id" id="v_depo_id" class="form-control-new" <?php echo $style_type; ?> onchange="CallAjaxForm('ajax_ruangan', this.value);">  
            <?php 
            	if(count($arr_data["list_depo"])==0)
            	{
					echo "<option value=\"\">-</option>";	
				}
				else
				{
					echo "<option value=\"\">-</option>";	
	                foreach($arr_data["list_depo"] AS $depo_id => $val)
	                {
						$depo_name = $arr_data["depo_name"][$depo_id];
			
	                    ?>
	                    <option value="<?php echo $depo_id; ?>"><?php echo $depo_name; ?></option>
	                    <?php
	                }
				}
            ?>
        </select>
		<?php
	}
    
    else if($ajax=="ajax_ruangan")
    {
    	$v_depo_id = $_GET["v_depo_id"];
    	
    	$q="
			SELECT 
			  fa_ruangan.ruangan_id,
			  fa_ruangan.depo_id,
			  fa_ruangan.ruangan_name 
			FROM
			  fa_ruangan 
			WHERE 1 
			  AND fa_ruangan.depo_id = '".$v_depo_id."' 
			ORDER BY 
			  fa_ruangan.ruangan_name ASC
    	";
    	$qry = mysql_query($q);
    	while($row = mysql_fetch_array($qry))
    	{
			list($ruangan_id,$depo_id,$ruangan_name)=$row;
			
			$arr_data["list_ruangan"][$ruangan_id]=$ruangan_id;
			$arr_data["depo_id"][$ruangan_id]=$depo_id;
			$arr_data["ruangan_name"][$ruangan_id]=$ruangan_name;
		}
    	
    	//$tabel_ruangan = $selecttb->selectTable("fa_ruangan","ruangan_id,depo_id,ruangan_name","fa_ruangan.depo_id='".$v_depo_id."'","ruangan_name ASC");
    	
		if(count($arr_data["list_ruangan"])==0)
        {
			$style_type = "style=\"width: 70px;\"";	
		}
		
		?>
        <select name="v_ruangan_id" id="v_ruangan_id" class="form-control-new" <?php echo $style_type; ?>>  
            <?php 
            	if(count($arr_data["list_ruangan"])==0)
            	{
					echo "<option value=\"\">-</option>";	
				}
				else
				{
					echo "<option value=\"\">-</option>";	
	                foreach($arr_data["list_ruangan"] AS $ruangan_id => $val)
	                {
						$ruangan_name = $arr_data["ruangan_name"][$ruangan_id];
						
	                    ?>
	                    <option value="<?php echo $ruangan_id; ?>"><?php echo $ruangan_name; ?></option>
	                    <?php
	                }
				}
            ?>
        </select>
		<?php
	}
    
    else if($ajax=="ajax_chk_asuransi")
    {
    	$v_fixed_asset_id = $_GET["v_fixed_asset_id"];
    	$chk_gudang_par = $_GET["chk_gudang_par"];
    	$chk_gudang_earthquaks = $_GET["chk_gudang_earthquaks"];
    	
    	if($v_fixed_asset_id)
    	{
			// select detail proposal
	        {
	            $q = "
	                SELECT 
					  ".$db["master"].".fa_fixed_asset.*,
					  ".$db["master"].".fa_kategory.kategory_name,
					  ".$db["master"].".fa_type.type_id,
					  ".$db["master"].".fa_type.type_name,
					  ".$db["master"].".fa_sub_type.sub_type_name,
					  ".$db["master"].".cabang.cabang_id,
					  ".$db["master"].".cabang.cabang_name,
					  ".$db["master"].".depo.depo_id,
					  ".$db["master"].".depo.depo_name,
					  tb_lokasi.sid_lokasi,
					  tb_lokasi.ruangan_id,
					  tb_pj.sid_pj,
					  tb_pj.type_pj,
					  tb_pj.employee_id
					FROM
					  ".$db["master"].".fa_fixed_asset 
					  INNER JOIN ".$db["master"].".fa_sub_type 
					    ON ".$db["master"].".fa_fixed_asset.sub_type_id = ".$db["master"].".fa_sub_type.sub_type_id 
					  INNER JOIN ".$db["master"].".fa_type 
					    ON ".$db["master"].".fa_sub_type.type_id = ".$db["master"].".fa_type.type_id 
					  INNER JOIN ".$db["master"].".fa_kategory 
					    ON ".$db["master"].".fa_type.kategory_id = ".$db["master"].".fa_kategory.kategory_id 
					  INNER JOIN 
					  (
					  	SELECT 
						  unu.* 
						FROM
						  (SELECT 
						    ".$db["master"].".fa_lokasi.sid AS sid_lokasi,
						    ".$db["master"].".fa_lokasi.fixed_asset_id,
						    ".$db["master"].".fa_lokasi.lokasi_date,
						    ".$db["master"].".fa_lokasi.depo_id,
						    ".$db["master"].".fa_lokasi.ruangan_id,
						    ".$db["master"].".fa_lokasi.mutasi_no 
						  FROM
						    ".$db["master"].".fa_lokasi 
						  WHERE 1 
						    AND ".$db["master"].".fa_lokasi.mutasi_no = '' 
						  ORDER BY ".$db["master"].".fa_lokasi.lokasi_date DESC,
						    ".$db["master"].".fa_lokasi.sid DESC) AS unu 
						WHERE 1 
						GROUP BY unu.fixed_asset_id
					  ) AS tb_lokasi 
					  	ON fa_fixed_asset.fixed_asset_id = tb_lokasi.fixed_asset_id
					  	
					  INNER JOIN 
					    (SELECT 
					      uni.* 
					    FROM
					      (SELECT 
					        ".$db["master"].".fa_penanggung_jawab.sid AS sid_pj,
					        ".$db["master"].".fa_penanggung_jawab.fixed_asset_id,
					        ".$db["master"].".fa_penanggung_jawab.pj_date,
					        ".$db["master"].".fa_penanggung_jawab.type_pj,
					        ".$db["master"].".fa_penanggung_jawab.employee_id,
					        ".$db["master"].".fa_penanggung_jawab.mutasi_no 
					      FROM
					        ".$db["master"].".fa_penanggung_jawab 
					      WHERE 1 
					        AND ".$db["master"].".fa_penanggung_jawab.mutasi_no = '' 
					      ORDER BY ".$db["master"].".fa_penanggung_jawab.pj_date DESC,
					        ".$db["master"].".fa_penanggung_jawab.sid DESC) AS uni 
					    WHERE 1 
					    GROUP BY uni.fixed_asset_id) AS tb_pj 
					    ON fa_fixed_asset.fixed_asset_id = tb_pj.fixed_asset_id 
					    	
					  INNER JOIN ".$db["master"].".depo 
					    ON ".$db["master"].".tb_lokasi.depo_id = ".$db["master"].".depo.depo_id 
					  INNER JOIN ".$db["master"].".cabang 
					    ON ".$db["master"].".depo.cabang_id = ".$db["master"].".cabang.cabang_id 
					WHERE 1
						AND ".$db["master"].".fa_fixed_asset.fixed_asset_id='".$v_fixed_asset_id."'
					LIMIT 0,01
	            "; 
	            //echo $q;    
	            $qry_curr = mysql_query($q);
	            $data = mysql_fetch_array($qry_curr);  
	        } 
			
			if($data["type_id"]=="7") // type = Tanah/Gedung
			{
				$cho_asuransi_par_no = $data["desc_gedung_no_asuransi_par"];
				$cho_asuransi_par_start_date = $data["desc_gedung_asuransi_start_date_par"];
				$cho_asuransi_par_end_date = $data["desc_gedung_asuransi_end_date_par"];
				
				$cho_asuransi_earthquake_no = $data["desc_gedung_no_asuransi_earthquake"];
				$cho_asuransi_earthquake_start_date = $data["desc_gedung_asuransi_start_date_earthquake"];
				$cho_asuransi_earthquake_end_date = $data["desc_gedung_asuransi_end_date_earthquake"];
			}
			else if($data["type_id"]=="4") //type = Gudang
			{
				$cho_asuransi_par_no = $data["desc_gudang_no_asuransi_par"];
				$cho_asuransi_par_start_date = $data["desc_gudang_asuransi_start_date_par"];
				$cho_asuransi_par_end_date = $data["desc_gudang_asuransi_end_date_par"];
				
				$cho_asuransi_earthquake_no = $data["desc_gudang_no_asuransi_earthquake"];
				$cho_asuransi_earthquake_start_date = $data["desc_gudang_asuransi_start_date_earthquake"];
				$cho_asuransi_earthquake_end_date = $data["desc_gudang_asuransi_end_date_earthquake"];
			}
		}
    	
    	?>
    	No Asuransi PAR
    	||
    	<input type="text" class="form-control-new " name="v_gudang_no_asuransi_par" id="v_gudang_no_asuransi_par" maxlength="30" size="30" value="<?php echo $cho_asuransi_par_no; ?>">
    	||
        Periode PAR
        ||
    	<input type="text" class="form-control-new" name="v_gudang_start_date_par" id="v_gudang_start_date_par" maxlength="10" size="10" value="<?php echo format_show_date($cho_asuransi_par_start_date); ?>">
    	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gudang_start_date_par, 'dd/mm/yyyy');">
		&nbsp;s/d&nbsp;
    	<input type="text" class="form-control-new" name="v_gudang_end_date_par" id="v_gudang_end_date_par" maxlength="10" size="10" value="<?php echo format_show_date($cho_asuransi_par_end_date); ?>">
    	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gudang_end_date_par, 'dd/mm/yyyy');">
    	||
    	No Asuransi Earthquake
    	||
    	<input type="text" class="form-control-new" name="v_gudang_no_asuransi_earthquake" id="v_gudang_no_asuransi_earthquake" maxlength="30" size="30" value="<?php echo $cho_asuransi_earthquake_no; ?>">
    	||
        Periode Earthquake
        ||
    	<input type="text" class="form-control-new" name="v_gudang_start_date_earthquake" id="v_gudang_start_date_earthquake" maxlength="10" size="10" value="<?php echo format_show_date($cho_asuransi_earthquake_start_date); ?>">
    	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gudang_start_date_earthquake, 'dd/mm/yyyy');">
		&nbsp;s/d&nbsp;
    	<input type="text" class="form-control-new " name="v_gudang_end_date_earthquake" id="v_gudang_end_date_earthquake" maxlength="10" size="10" value="<?php echo format_show_date($cho_asuransi_earthquake_end_date); ?>">
    	<img src="images/calendar.gif" style="cursor: pointer;" onClick="popUpCalendar(this, theform.v_gudang_end_date_earthquake, 'dd/mm/yyyy');">
    	
    	<?php			
		
	}
	
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                                                                                                                                                   
                if(!isset($_POST["v_receipt_no"])){ $v_receipt_no = isset($_POST["v_receipt_no"]); } else { $v_receipt_no = $_POST["v_receipt_no"]; }     
                if(!isset($_POST["v_no_proposal"])){ $v_no_proposal = isset($_POST["v_no_proposal"]); } else { $v_no_proposal = $_POST["v_no_proposal"]; } 
                if(!isset($_POST["v_kategory_id"])){ $v_kategory_id = isset($_POST["v_kategory_id"]); } else { $v_kategory_id = $_POST["v_kategory_id"]; }          
                if(!isset($_POST["v_type_id"])){ $v_type_id = isset($_POST["v_type_id"]); } else { $v_type_id = $_POST["v_type_id"]; }            
                if(!isset($_POST["v_sub_type_id"])){ $v_sub_type_id = isset($_POST["v_sub_type_id"]); } else { $v_sub_type_id = $_POST["v_sub_type_id"]; } 
                if(!isset($_POST["v_receipt_date"])){ $v_receipt_date = isset($_POST["v_receipt_date"]); } else { $v_receipt_date = $_POST["v_receipt_date"]; }   
                if(!isset($_POST["v_keterangan"])){ $v_keterangan = isset($_POST["v_keterangan"]); } else { $v_keterangan = $_POST["v_keterangan"]; } 
                if(!isset($_POST["v_cabang_id"])){ $v_cabang_id = isset($_POST["v_cabang_id"]); } else { $v_cabang_id = $_POST["v_cabang_id"]; } 
                if(!isset($_POST["v_depo_id"])){ $v_depo_id = isset($_POST["v_depo_id"]); } else { $v_depo_id = $_POST["v_depo_id"]; } 
                if(!isset($_POST["v_ruangan_id"])){ $v_ruangan_id = isset($_POST["v_ruangan_id"]); } else { $v_ruangan_id = $_POST["v_ruangan_id"]; } 
                if(!isset($_POST["v_penanggung_jawab"])){ $v_penanggung_jawab = isset($_POST["v_penanggung_jawab"]); } else { $v_penanggung_jawab = $_POST["v_penanggung_jawab"]; } 
                if(!isset($_POST["v_emp_id"])){ $v_emp_id = isset($_POST["v_emp_id"]); } else { $v_emp_id = $_POST["v_emp_id"]; }
                if(!isset($_POST["v_emp_name"])){ $v_emp_name = isset($_POST["v_emp_name"]); } else { $v_emp_name = $_POST["v_emp_name"]; } 
                if(!isset($_POST["v_nominal"])){ $v_nominal = isset($_POST["v_nominal"]); } else { $v_nominal = $_POST["v_nominal"]; } 
                if(!isset($_POST["v_metode_penyusutan_id"])){ $v_metode_penyusutan_id = isset($_POST["v_metode_penyusutan_id"]); } else { $v_metode_penyusutan_id = $_POST["v_metode_penyusutan_id"]; } 
                
                // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)    
                if(!isset($_POST["v_desc_merek"])){ $v_desc_merek = isset($_POST["v_desc_merek"]); } else { $v_desc_merek = $_POST["v_desc_merek"]; }   
                if(!isset($_POST["v_desc_tipe"])){ $v_desc_tipe = isset($_POST["v_desc_tipe"]); } else { $v_desc_tipe = $_POST["v_desc_tipe"]; }   
                if(!isset($_POST["v_desc_jenis"])){ $v_desc_jenis = isset($_POST["v_desc_jenis"]); } else { $v_desc_jenis = $_POST["v_desc_jenis"]; }    
                if(!isset($_POST["v_desc_warna"])){ $v_desc_warna = isset($_POST["v_desc_warna"]); } else { $v_desc_warna = $_POST["v_desc_warna"]; }   
                if(!isset($_POST["v_desc_tahun"])){ $v_desc_tahun = isset($_POST["v_desc_tahun"]); } else { $v_desc_tahun = $_POST["v_desc_tahun"]; }
                
                // type = kendaraan               
                if(!isset($_POST["v_kendaraan_merek"])){ $v_kendaraan_merek = isset($_POST["v_kendaraan_merek"]); } else { $v_kendaraan_merek = $_POST["v_kendaraan_merek"]; }   
                if(!isset($_POST["v_kendaraan_jenis"])){ $v_kendaraan_jenis = isset($_POST["v_kendaraan_jenis"]); } else { $v_kendaraan_jenis = $_POST["v_kendaraan_jenis"]; }   
                if(!isset($_POST["v_kendaraan_warna"])){ $v_kendaraan_warna = isset($_POST["v_kendaraan_warna"]); } else { $v_kendaraan_warna = $_POST["v_kendaraan_warna"]; }    
                if(!isset($_POST["v_kendaraan_cc"])){ $v_kendaraan_cc = isset($_POST["v_kendaraan_cc"]); } else { $v_kendaraan_cc = $_POST["v_kendaraan_cc"]; }   
                if(!isset($_POST["v_no_polisi"])){ $v_no_polisi = isset($_POST["v_no_polisi"]); } else { $v_no_polisi = $_POST["v_no_polisi"]; }    
                if(!isset($_POST["v_no_rangka"])){ $v_no_rangka = isset($_POST["v_no_rangka"]); } else { $v_no_rangka = $_POST["v_no_rangka"]; }   
                if(!isset($_POST["v_no_mesin"])){ $v_no_mesin = isset($_POST["v_no_mesin"]); } else { $v_no_mesin = $_POST["v_no_mesin"]; }   
                if(!isset($_POST["v_tgl_stnk"])){ $v_tgl_stnk = isset($_POST["v_tgl_stnk"]); } else { $v_tgl_stnk = $_POST["v_tgl_stnk"]; }      
                if(!isset($_POST["v_tgl_pajak"])){ $v_tgl_pajak = isset($_POST["v_tgl_pajak"]); } else { $v_tgl_pajak = $_POST["v_tgl_pajak"]; }     
                if(!isset($_POST["v_no_bpkb"])){ $v_no_bpkb = isset($_POST["v_no_bpkb"]); } else { $v_no_bpkb = $_POST["v_no_bpkb"]; } 
                
                // type = Gedung/Tanah
                if(!isset($_POST["v_gedung_nama_pemilik"])){ $v_gedung_nama_pemilik = isset($_POST["v_gedung_nama_pemilik"]); } else { $v_gedung_nama_pemilik = $_POST["v_gedung_nama_pemilik"]; }   
                if(!isset($_POST["v_gedung_luas_bangunan"])){ $v_gedung_luas_bangunan = isset($_POST["v_gedung_luas_bangunan"]); } else { $v_gedung_luas_bangunan = $_POST["v_gedung_luas_bangunan"]; }   
                if(!isset($_POST["v_gedung_luas_tanah"])){ $v_gedung_luas_tanah = isset($_POST["v_gedung_luas_tanah"]); } else { $v_gedung_luas_tanah = $_POST["v_gedung_luas_tanah"]; }    
                if(!isset($_POST["v_gedung_tinggi_bangunan"])){ $v_gedung_tinggi_bangunan = isset($_POST["v_gedung_tinggi_bangunan"]); } else { $v_gedung_tinggi_bangunan = $_POST["v_gedung_tinggi_bangunan"]; } 
                
                if(!isset($_POST["chk_gedung_par"])){ $chk_gedung_par = isset($_POST["chk_gedung_par"]); } else { $chk_gedung_par = $_POST["chk_gedung_par"]; }
                if(!isset($_POST["chk_gedung_earthquake"])){ $chk_gedung_earthquake = isset($_POST["chk_gedung_earthquake"]); } else { $chk_gedung_earthquake = $_POST["chk_gedung_earthquake"]; }
                
                if(!isset($_POST["v_gedung_no_asuransi_par"])){ $v_gedung_no_asuransi_par = isset($_POST["v_gedung_no_asuransi_par"]); } else { $v_gedung_no_asuransi_par = $_POST["v_gedung_no_asuransi_par"]; }    
                if(!isset($_POST["v_gedung_start_date_par"])){ $v_gedung_start_date_par = isset($_POST["v_gedung_start_date_par"]); } else { $v_gedung_start_date_par = $_POST["v_gedung_start_date_par"]; }   
                if(!isset($_POST["v_gedung_end_date_par"])){ $v_gedung_end_date_par = isset($_POST["v_gedung_end_date_par"]); } else { $v_gedung_end_date_par = $_POST["v_gedung_end_date_par"]; }  
                
                if(!isset($_POST["v_gedung_no_asuransi_earthquake"])){ $v_gedung_no_asuransi_earthquake = isset($_POST["v_gedung_no_asuransi_earthquake"]); } else { $v_gedung_no_asuransi_earthquake = $_POST["v_gedung_no_asuransi_earthquake"]; }    
                if(!isset($_POST["v_gedung_start_date_earthquake"])){ $v_gedung_start_date_earthquake = isset($_POST["v_gedung_start_date_earthquake"]); } else { $v_gedung_start_date_earthquake = $_POST["v_gedung_start_date_earthquake"]; }   
                if(!isset($_POST["v_gedung_end_date_earthquake"])){ $v_gedung_end_date_earthquake = isset($_POST["v_gedung_end_date_earthquake"]); } else { $v_gedung_end_date_earthquake = $_POST["v_gedung_end_date_earthquake"]; }  
                     
                if(!isset($_POST["v_gedung_fungsi"])){ $v_gedung_fungsi = isset($_POST["v_gedung_fungsi"]); } else { $v_gedung_fungsi = $_POST["v_gedung_fungsi"]; }       
                if(!isset($_POST["v_gedung_desa"])){ $v_gedung_desa = isset($_POST["v_gedung_desa"]); } else { $v_gedung_desa = $_POST["v_gedung_desa"]; }       
                if(!isset($_POST["v_gedung_kecamatan"])){ $v_gedung_kecamatan = isset($_POST["v_gedung_kecamatan"]); } else { $v_gedung_kecamatan = $_POST["v_gedung_kecamatan"]; } 
                if(!isset($_POST["v_gedung_kota"])){ $v_gedung_kota = isset($_POST["v_gedung_kota"]); } else { $v_gedung_kota = $_POST["v_gedung_kota"]; }       
                if(!isset($_POST["v_gedung_kabupaten"])){ $v_gedung_kabupaten = isset($_POST["v_gedung_kabupaten"]); } else { $v_gedung_kabupaten = $_POST["v_gedung_kabupaten"]; } 
                if(!isset($_POST["v_gedung_provinsi"])){ $v_gedung_provinsi = isset($_POST["v_gedung_provinsi"]); } else { $v_gedung_provinsi = $_POST["v_gedung_provinsi"]; }
               	   
                // type = Gudang
                if(!isset($_POST["v_gudang_luas_bangunan"])){ $v_gudang_luas_bangunan = isset($_POST["v_gudang_luas_bangunan"]); } else { $v_gudang_luas_bangunan = $_POST["v_gudang_luas_bangunan"]; }   
                if(!isset($_POST["v_gudang_luas_tanah"])){ $v_gudang_luas_tanah = isset($_POST["v_gudang_luas_tanah"]); } else { $v_gudang_luas_tanah = $_POST["v_gudang_luas_tanah"]; }   
                
                if(!isset($_POST["chk_gudang_par"])){ $chk_gudang_par = isset($_POST["chk_gudang_par"]); } else { $chk_gudang_par = $_POST["chk_gudang_par"]; }
                if(!isset($_POST["chk_gudang_earthquaks"])){ $chk_gudang_earthquaks = isset($_POST["chk_gudang_earthquaks"]); } else { $chk_gudang_earthquaks = $_POST["chk_gudang_earthquaks"]; }
                
                if(!isset($_POST["v_gudang_no_asuransi_par"])){ $v_gudang_no_asuransi_par = isset($_POST["v_gudang_no_asuransi_par"]); } else { $v_gudang_no_asuransi_par = $_POST["v_gudang_no_asuransi_par"]; }    
               	if(!isset($_POST["v_gudang_start_date_par"])){ $v_gudang_start_date_par = isset($_POST["v_gudang_start_date_par"]); } else { $v_gudang_start_date_par = $_POST["v_gudang_start_date_par"]; }   
                if(!isset($_POST["v_gudang_end_date_par"])){ $v_gudang_end_date_par = isset($_POST["v_gudang_end_date_par"]); } else { $v_gudang_end_date_par = $_POST["v_gudang_end_date_par"]; }   
                
                if(!isset($_POST["v_gudang_no_asuransi_earthquake"])){ $v_gudang_no_asuransi_earthquake = isset($_POST["v_gudang_no_asuransi_earthquake"]); } else { $v_gudang_no_asuransi_earthquake = $_POST["v_gudang_no_asuransi_earthquake"]; }    
               	if(!isset($_POST["v_gudang_start_date_earthquake"])){ $v_gudang_start_date_earthquake = isset($_POST["v_gudang_start_date_earthquake"]); } else { $v_gudang_start_date_earthquake = $_POST["v_gudang_start_date_earthquake"]; }   
                if(!isset($_POST["v_gudang_end_date_earthquake"])){ $v_gudang_end_date_earthquake = isset($_POST["v_gudang_end_date_earthquake"]); } else { $v_gudang_end_date_earthquake = $_POST["v_gudang_end_date_earthquake"]; } 
               	
               	if(!isset($_POST["v_gudang_desa"])){ $v_gudang_desa = isset($_POST["v_gudang_desa"]); } else { $v_gudang_desa = $_POST["v_gudang_desa"]; }   
                if(!isset($_POST["v_gudang_kecamatan"])){ $v_gudang_kecamatan = isset($_POST["v_gudang_kecamatan"]); } else { $v_gudang_kecamatan = $_POST["v_gudang_kecamatan"]; }        
                if(!isset($_POST["v_gudang_kota"])){ $v_gudang_kota = isset($_POST["v_gudang_kota"]); } else { $v_gudang_kota = $_POST["v_gudang_kota"]; }       
                if(!isset($_POST["v_gudang_kabupaten"])){ $v_gudang_kabupaten = isset($_POST["v_gudang_kabupaten"]); } else { $v_gudang_kabupaten = $_POST["v_gudang_kabupaten"]; }       
                if(!isset($_POST["v_gudang_provinsi"])){ $v_gudang_provinsi = isset($_POST["v_gudang_provinsi"]); } else { $v_gudang_provinsi = $_POST["v_gudang_provinsi"]; } 
                
                if(!isset($_POST["v_cek_qty"])){ $v_cek_qty = isset($_POST["v_cek_qty"]); } else { $v_cek_qty = $_POST["v_cek_qty"]; } 
                if(!isset($_POST["v_qty"])){ $v_qty = isset($_POST["v_qty"]); } else { $v_qty = $_POST["v_qty"]; } 
               	   
				$lokasi_file    = $_FILES['fupload']['tmp_name'];
				$nama_file      = $_FILES['fupload']['name'];
				$tipe_file      = $_FILES['fupload']['type'];
				$acak           = rand(000000,999999);
				$name_paksa     = preg_replace("/[^A-Z0-9._-]/i", "_", $nama_file);
				$nama_file_unik = $acak.$name_paksa;

				$v_receipt_date = format_save_date($v_receipt_date);
				$v_tgl_stnk     = format_save_date($v_tgl_stnk);
				$v_tgl_pajak    = format_save_date($v_tgl_pajak);
				$v_nominal      = save_int($v_nominal);
				$v_kendaraan_cc = save_int($v_kendaraan_cc);
				$v_qty          = save_int($v_qty);
                    
                /*if($v_receipt_no=="")
                {    
                	$msg = "No Penerimaan Barang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();                                                                           
                }
                else if($v_no_proposal=="")
                {       
                	$msg = "No Proposal harus diisi";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else */
                if($v_kategory_id=="")
                {      
                	$msg = "Kategori harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_type_id=="")
                {   
                	$msg = "Type harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_sub_type_id=="")
                {   
                	$msg = "Sub Type harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }    
                else if($v_receipt_date=="")
                {   
                	$msg = "Tanggal Beli harus diisi";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }     
                /*else if($nama_file=="")
                {   
                	$msg = "Photo harus diisi";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                } */ 
                else if($v_keterangan=="")
                {   
                	$msg = "Keterangan harus diisi";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }        
                else if($v_cabang_id=="")
                {   
                	$msg = "Lokasi Asset harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }     
                else if($v_depo_id=="")
                {   
                	$msg = "Lokasi Asset harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }     
                /*else if($v_ruangan_id=="")
                {   
                	$msg = "Ruangan Asset harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                } */    
                else if($v_penanggung_jawab=="")
                {   
                	$msg = "Penanggung Jawab harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }  
                else
                {
					$where_fa_fixed_asset ="";
					$where_fa_fixed_asset_photo ="";
					$where_fa_pj ="";
					
	                if($v_cek_qty)
	                {
						if($v_qty*1==0)
						{
		                	$msg = "Qty harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
						}
						else if($v_qty*1==1)
						{
		                	$msg = "Qty harus lebih dari 1";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
						}
					}
				
                	if($v_penanggung_jawab=="personal")
                	{
                		if($v_emp_name=="")
                		{
		                	$msg = "Nama Penanggung Jawab harus dipilih";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
						}
						
						$where_fa_pj .= "employee_id = '".$v_emp_id."',";
					}
					else if($v_penanggung_jawab=="umum")
                	{
                		$q="
                			SELECT 
							  depo.employee_id 
							FROM
							  ".$db["master"].".depo 
							WHERE 1 
							  AND depo.depo_id = '".$v_depo_id."' 
							ORDER BY 
							  depo.depo_id ASC 
                		";
                		$qry = mysql_query($q);
                		$r = mysql_fetch_array($qry);
                		
                		$where_fa_pj .= "employee_id = '".$r["employee_id"]."',";
					}
					
					if($v_cek_qty=="" || empty($v_cek_qty))
					{
						if($nama_file)
	                    {
	                        if($tipe_file=="image/jpeg" || $tipe_file=="image/jpg" || $tipe_file=="image/gif" || $tipe_file=="image/png")
	                        {
	                        	UploadBukti($nama_file_unik,"photo_fixed_asset");  
	                                                        
	                            $where_fa_fixed_asset_photo = "photo='".$nama_file_unik."',"; 
	                        }
	                        else
	                        {
	                            $msg = "Dokumen lampiran harus .jpg .gif .png .pdf";
	                            echo "<script>alert('".$msg."');</script>";
	                            die();
	                        } 
	                    }
					}

					if($v_nominal)
					{
						$where_fa_fixed_asset .= "nominal='".$v_nominal."',";
					}
					
					if($v_metode_penyusutan_id)
					{
						$where_fa_fixed_asset .= "metode_penyusutan_id='".$v_metode_penyusutan_id."',";
					}

					if($v_type_id=="1") // type = kendaraan
					{    
		                if($v_kendaraan_merek=="")
		                {   
		                	$msg = "Kendaraan Merek harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_kendaraan_jenis=="")
		                {   
		                	$msg = "Kendaraan Jenis harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_kendaraan_warna=="")
		                {   
		                	$msg = "Kendaraan Warna harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_kendaraan_cc=="")
		                {   
		                	$msg = "Kendaraan CC harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_no_polisi=="")
		                {   
		                	$msg = "No Polisi harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_no_rangka=="")
		                {   
		                	$msg = "No Rangka harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_no_mesin=="")
		                {   
		                	$msg = "No Mesin harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_tgl_stnk=="")
		                {   
		                	$msg = "Tanggal STNK harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_tgl_pajak=="")
		                {   
		                	$msg = "Tanggal Pajak harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                else if($v_no_bpkb=="")
		                {   
		                	$msg = "No BPKB harus diisi";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
		                } 
		                
						$where_fa_fixed_asset .="
							desc_kendaraan_merek = '".$v_kendaraan_merek."',
	                        desc_kendaraan_jenis = '".$v_kendaraan_jenis."',
	                        desc_kendaraan_warna = '".$v_kendaraan_warna."',
	                        desc_kendaraan_cc = '".$v_kendaraan_cc."',
	                        desc_kendaraan_nopol = '".$v_no_polisi."',
	                        desc_kendaraan_no_rangka = '".$v_no_rangka."',
	                        desc_kendaraan_no_mesin = '".$v_no_mesin."',
	                        desc_kendaraan_tgl_stnk = '".$v_tgl_stnk."',
	                        desc_kendaraan_tgl_pajak = '".$v_tgl_pajak."',
	                        desc_kendaraan_no_bpkb = '".$v_no_bpkb."',
						";	
					}
                    else if($v_type_id=="7") // type = Tanah/Gedung
                    {
                    	if($chk_gedung_par)
                    	{	
                    		$v_gedung_no_asuransi_par = save_char($v_gedung_no_asuransi_par);
                    		$v_gedung_start_date_par = format_save_date($v_gedung_start_date_par);
                    		$v_gedung_end_date_par = format_save_date($v_gedung_end_date_par);
                    		
							if($v_gedung_no_asuransi_par=="")
				            {   
				            	$msg = "No Asuransi Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gedung_start_date_par=="")
				            {   
				            	$msg = "Periode Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gedung_end_date_par=="")
				            {   
				            	$msg = "Periode Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            
							$where_fa_fixed_asset .= "desc_gedung_asuransi_par = '1',";	 
							$where_fa_fixed_asset .= "desc_gedung_no_asuransi_par = '".$v_gedung_no_asuransi_par."',";	
							$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_par = '".$v_gedung_start_date_par."',";	
							$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_par = '".$v_gedung_end_date_par."',";	
						}
						
						if($chk_gedung_earthquake)
                    	{
                    		$v_gedung_no_asuransi_earthquake = save_char($v_gedung_no_asuransi_earthquake);
                    		$v_gedung_start_date_earthquake = format_save_date($v_gedung_start_date_earthquake);
                    		$v_gedung_end_date_earthquake = format_save_date($v_gedung_end_date_earthquake);
                    		
                    		if($v_gedung_no_asuransi_earthquake=="")
				            {   
				            	$msg = "No Asuransi Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gedung_start_date_earthquake=="")
				            {   
				            	$msg = "Periode Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gedung_end_date_earthquake=="")
				            {   
				            	$msg = "Periode Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            
							$where_fa_fixed_asset .= "desc_gedung_asuransi_earthquake = '1',";	 
							$where_fa_fixed_asset .= "desc_gedung_no_asuransi_earthquake = '".$v_gedung_no_asuransi_par."',";	
							$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_earthquake = '".$v_gedung_start_date_earthquake."',";	
							$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_earthquake = '".$v_gedung_end_date_earthquake."',";
						}
						
                    	$where_fa_fixed_asset .="
							desc_gedung_nama_pemilik = '".$v_gedung_nama_pemilik."',
	                        desc_gedung_luas_bangunan = '".save_int($v_gedung_luas_bangunan)."',
	                        desc_gedung_luas_tanah = '".save_int($v_gedung_luas_tanah)."',
	                        desc_gedung_tinggi_bangunan = '".save_int($v_gedung_tinggi_bangunan)."',
	                        desc_gedung_fungsi = '".$v_gedung_fungsi."',
	                        desc_gedung_desa = '".$v_gedung_desa."',
	                        desc_gedung_kecamatan = '".$v_gedung_kecamatan."',
	                        desc_gedung_kota = '".$v_gedung_kota."',
	                        desc_gedung_kabupaten = '".$v_gedung_kabupaten."',
	                        desc_gedung_provinsi = '".$v_gedung_provinsi."',
						";
					}
					else if($v_type_id=="4") // type = Gudang
					{
						if($chk_gedung_par)
                    	{	
                    		$v_gudang_no_asuransi_par = save_char($v_gudang_no_asuransi_par);
                    		$v_gudang_start_date_par = format_save_date($v_gudang_start_date_par);
                    		$v_gudang_end_date_par = format_save_date($v_gudang_end_date_par);
                    		
							if($v_gudang_no_asuransi_par=="")
				            {   
				            	$msg = "No Asuransi Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gudang_start_date_par=="")
				            {   
				            	$msg = "Periode Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gudang_end_date_par=="")
				            {   
				            	$msg = "Periode Par harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            
							$where_fa_fixed_asset .= "desc_gudang_asuransi_par = '1',";	 
							$where_fa_fixed_asset .= "desc_gudang_no_asuransi_par = '".$v_gudang_no_asuransi_par."',";	
							$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_par = '".$v_gudang_start_date_par."',";	
							$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_par = '".$v_gudang_end_date_par."',";	
						}
						
						if($chk_gedung_earthquake)
                    	{
                    		$v_gudang_no_asuransi_earthquake = save_char($v_gudang_no_asuransi_earthquake);
                    		$v_gudang_start_date_earthquake = format_save_date($v_gudang_start_date_earthquake);
                    		$v_gudang_end_date_earthquake = format_save_date($v_gudang_end_date_earthquake);
                    		
                    		if($v_gudang_no_asuransi_earthquake=="")
				            {   
				            	$msg = "No Asuransi Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gudang_start_date_earthquake=="")
				            {   
				            	$msg = "Periode Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_gudang_end_date_earthquake=="")
				            {   
				            	$msg = "Periode Earthquake harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            
							$where_fa_fixed_asset .= "desc_gudang_asuransi_earthquake = '1',";	 
							$where_fa_fixed_asset .= "desc_gudang_no_asuransi_earthquake = '".$v_gudang_no_asuransi_earthquake."',";	
							$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_earthquake = '".$v_gudang_start_date_earthquake."',";	
							$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_earthquake = '".$v_gudang_end_date_earthquake."',";
						}
                
                    	$where_fa_fixed_asset .="
	                        desc_gudang_luas_bangunan = '".save_int($v_gudang_luas_bangunan)."',
	                        desc_gudang_luas_tanah = '".save_int($v_gudang_luas_tanah)."',
	                        desc_gudang_desa = '".$v_gudang_desa."',
	                        desc_gudang_kecamatan = '".$v_gudang_kecamatan."',
	                        desc_gudang_kota = '".$v_gudang_kota."',
	                        desc_gedung_kabupaten = '".$v_gudang_kabupaten."',
	                        desc_gedung_provinsi = '".$v_gudang_provinsi."',
						";
					}
					else if($v_type_id=="2" || $v_type_id=="3" || $v_type_id=="6") // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)
					{
						if($v_desc_merek=="")
			            {   
			            	$msg = "Merek harus diisi";
			                echo "<script>alert('".$msg."');</script>";
			                die();
			            } 
			            else if($v_desc_tipe=="")
			            {   
			            	$msg = "Tipe harus diisi";
			                echo "<script>alert('".$msg."');</script>";
			                die();
			            }
			            else if($v_desc_jenis=="")
			            {   
			            	$msg = "Jenis harus diisi";
			                echo "<script>alert('".$msg."');</script>";
			                die();
			            }
			            else if($v_desc_warna=="")
			            {   
			            	$msg = "Warna harus diisi";
			                echo "<script>alert('".$msg."');</script>";
			                die();
			            }
			            else if($v_desc_tahun=="")
			            {   
			            	$msg = "Tahun harus diisi";
			                echo "<script>alert('".$msg."');</script>";
			                die();
			            }
			            else
			            {
							$where_fa_fixed_asset .= "desc_merek = '".$v_desc_merek."',";	 
							$where_fa_fixed_asset .= "desc_tipe = '".$v_desc_tipe."',";	
							$where_fa_fixed_asset .= "desc_jenis = '".$v_desc_jenis."',";
							$where_fa_fixed_asset .= "desc_warna = '".$v_desc_warna."',";
							$where_fa_fixed_asset .= "desc_tahun = '".$v_desc_tahun."',";
						}
					}
					
					if($v_cek_qty)
					{
						if($v_qty*1>1)
						{
							for($i=1;$i<=$v_qty;$i++)
							{
								$lokasi_file    = $_FILES['fupload']['tmp_name'];
								$nama_file      = $_FILES['fupload']['name'];
								$tipe_file      = $_FILES['fupload']['type'];
								$acak           = rand(000000,999999);
								$name_paksa     = preg_replace("/[^A-Z0-9._-]/i", "_", $nama_file);
								$nama_file_unik = $acak.$name_paksa;
								
								if($nama_file)
			                    {
			                        if($tipe_file=="image/jpeg" || $tipe_file=="image/jpg" || $tipe_file=="image/gif" || $tipe_file=="image/png")
			                        {
			                        	UploadBukti($nama_file_unik,"photo_fixed_asset");  
			                        	
			                            $where_fa_fixed_asset_photo = "photo='".$nama_file_unik."',"; 
			                        }
			                        else
			                        {
			                            $msg = "Dokumen lampiran harus .jpg .gif .png .pdf";
			                            echo "<script>alert('".$msg."');</script>";
			                            die();
			                        } 
			                    }
								
								$fixed_asset_code = fixed_asset_code_counter($db["master"], "fa_fixed_asset","fixed_asset_code",$v_depo_id,$v_type_id);
			                    $counter_fixed_asset = get_counter_int($db["master"],"fa_fixed_asset","fixed_asset_id",100);
			                    $counter_penanggung_jawab = get_counter_int($db["master"],"fa_penanggung_jawab","sid",100);
			                    $counter_lokasi = get_counter_int($db["master"],"fa_lokasi","sid",100);
			                    
			                    // insert master fix asset
			                    {
				                    $q = "
					                    INSERT INTO
					                        ".$db["master"].".fa_fixed_asset
					                    SET
					                        fixed_asset_id = '".$counter_fixed_asset."',
					                        fixed_asset_code = '".$fixed_asset_code."',
					                        NoProposal = '".$v_no_proposal."',
					                        receipt_no = '".$v_receipt_no."',
					                        sub_type_id = '".$v_sub_type_id."',
					                        tanggal_beli = '".$v_receipt_date."',
					                        keterangan = '".$v_keterangan."',
					                        ".$where_fa_fixed_asset."
					                        ".$where_fa_fixed_asset_photo."
					                        author_user = '".$ses_login."',
					                        author_date = NOW(),
					                        edited_user = '".$ses_login."',
					                        edited_date = NOW()
					                        
					                ";   
					                if(!mysql_query($q))
					                {
					                    $msg = "Gagal menyimpan Fixed Asset";
					                    echo "<script>alert('".$msg."');</script>";
					                    die(); 
					                }
								}
								
								$fixed_asset_id = mysql_insert_id();
								
								// insert penanggung jawab
								{
									$q = "
					                    INSERT INTO
					                        ".$db["master"].".fa_penanggung_jawab
					                    SET
					                        sid = '".$counter_penanggung_jawab."',
					                        fixed_asset_id = '".$fixed_asset_id."',
					                        pj_date = NOW(),
					                        type_pj = '".$v_penanggung_jawab."',
					                        ".$where_fa_pj."
					                        mutasi_no = ''
					                ";              
					                if(!mysql_query($q))
					                {
					                    $msg = "Gagal menyimpan Penanggung Jawab";
					                    echo "<script>alert('".$msg."');</script>";
					                    die(); 
					                }
								}
								
								// insert lokasi
								{
									$q = "
					                    INSERT INTO
					                        ".$db["master"].".fa_lokasi
					                    SET
					                        sid = '".$counter_lokasi."',
					                        fixed_asset_id = '".$fixed_asset_id."',
					                        lokasi_date = NOW(),
					                        depo_id = '".$v_depo_id."',
					                        ruangan_id = '".$v_ruangan_id."',
					                        mutasi_no = ''
					                ";             
					                if(!mysql_query($q))
					                {
					                    $msg = "Gagal menyimpan Lokasi";
					                    echo "<script>alert('".$msg."');</script>";
					                    die(); 
					                }
								}
								
								$arr_data["fixed_id_baru"][$fixed_asset_id]=$fixed_asset_id;
							}	
						}
					}
					else
					{
	                	$fixed_asset_code = fixed_asset_code_counter($db["master"], "fa_fixed_asset","fixed_asset_code",$v_depo_id,$v_type_id);
	                    $counter_fixed_asset = get_counter_int($db["master"],"fa_fixed_asset","fixed_asset_id",100);
	                    $counter_penanggung_jawab = get_counter_int($db["master"],"fa_penanggung_jawab","sid",100);
	                    $counter_lokasi = get_counter_int($db["master"],"fa_lokasi","sid",100);
	                    
	                    // insert master fix asset
	                    {
		                    $q = "
			                    INSERT INTO
			                        ".$db["master"].".fa_fixed_asset
			                    SET
			                        fixed_asset_id = '".$counter_fixed_asset."',
			                        fixed_asset_code = '".$fixed_asset_code."',
			                        NoProposal = '".$v_no_proposal."',
			                        receipt_no = '".$v_receipt_no."',
			                        sub_type_id = '".$v_sub_type_id."',
			                        tanggal_beli = '".$v_receipt_date."',
			                        keterangan = '".$v_keterangan."',
			                        ".$where_fa_fixed_asset."
			                        ".$where_fa_fixed_asset_photo."
			                        author_user = '".$ses_login."',
			                        author_date = NOW(),
			                        edited_user = '".$ses_login."',
			                        edited_date = NOW()
			                        
			                ";   
			                if(!mysql_query($q))
			                {
			                    $msg = "Gagal menyimpan Fixed Asset";
			                    echo "<script>alert('".$msg."');</script>";
			                    die(); 
			                }	
						}
						
						$fixed_asset_id = mysql_insert_id();
						
						// insert penanggung jawab
						{
							$q = "
			                    INSERT INTO
			                        ".$db["master"].".fa_penanggung_jawab
			                    SET
			                        sid = '".$counter_penanggung_jawab."',
			                        fixed_asset_id = '".$fixed_asset_id."',
			                        pj_date = NOW(),
			                        type_pj = '".$v_penanggung_jawab."',
			                        ".$where_fa_pj."
			                        mutasi_no = ''
			                ";              
			                if(!mysql_query($q))
			                {
			                    $msg = "Gagal menyimpan Penanggung Jawab";
			                    echo "<script>alert('".$msg."');</script>";
			                    die(); 
			                }
						}
						
						// insert lokasi
						{
							$q = "
			                    INSERT INTO
			                        ".$db["master"].".fa_lokasi
			                    SET
			                        sid = '".$counter_lokasi."',
			                        fixed_asset_id = '".$fixed_asset_id."',
			                        lokasi_date = NOW(),
			                        depo_id = '".$v_depo_id."',
			                        ruangan_id = '".$v_ruangan_id."',
			                        mutasi_no = ''
			                ";             
			                if(!mysql_query($q))
			                {
			                    $msg = "Gagal menyimpan Lokasi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die(); 
			                }
						}
	                }
	                
	                // copy images jika qty lebih dari satu
	                if($v_cek_qty)
					{
						if($v_qty*1>1)
						{
							$lokasi_file    = $_FILES['fupload']['tmp_name'];
							$nama_file      = $_FILES['fupload']['name'];
							
							if($nama_file)
		                    {
		                    	$fixed_id_baru = where_array($arr_data["fixed_id_baru"], "fa_fixed_asset.fixed_asset_id", "in");
							
								$q="
							    	SELECT 
									  fa_fixed_asset.photo 
									FROM
									  ".$db["master"].".fa_fixed_asset 
									WHERE 1 
									  ".$fixed_id_baru."
									ORDER BY fa_fixed_asset.fixed_asset_id ASC
									LIMIT 0,01 
							    ";
							    $qry = mysql_query($q);
							   	$data = mysql_fetch_array($qry);
							   	
							   	$q="
							    	SELECT 
									  fa_fixed_asset.photo 
									FROM
									  ".$db["master"].".fa_fixed_asset 
									WHERE 1 
									  ".$fixed_id_baru."
									ORDER BY fa_fixed_asset.fixed_asset_id ASC
							    ";
							    $qry = mysql_query($q);
							   	while($row = mysql_fetch_array($qry))
							   	{
									list($photo)=$row;
									
									copy('photo_fixed_asset/'.$data["photo"].'', 'photo_fixed_asset/'.$photo.'');	
									copy('photo_fixed_asset/medium_'.$data["photo"].'', 'photo_fixed_asset/medium_'.$photo.'');
									copy('photo_fixed_asset/thumb_'.$data["photo"].'', 'photo_fixed_asset/thumb_'.$photo.'');
								}
		                    	
		                    }
						}
					}
					
					$msg = "Berhasil menyimpan";
	                echo "<script>alert('".$msg."');</script>"; 
	                
	                if($v_cek_qty)
	                {
						echo "<script>parent.CallAjaxForm('search');</script>";         
					}
					else
					{
						echo "<script>parent.CallAjaxForm('search','".$fixed_asset_id."');</script>";         	
					}
	                
	                die();
				}   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["v_fixed_asset_id"])){ $v_fixed_asset_id = isset($_POST["v_fixed_asset_id"]); } else { $v_fixed_asset_id = $_POST["v_fixed_asset_id"]; }  
                if(!isset($_POST["v_type_id"])){ $v_type_id = isset($_POST["v_type_id"]); } else { $v_type_id = $_POST["v_type_id"]; }   
                if(!isset($_POST["v_photo_old"])){ $v_photo_old = isset($_POST["v_photo_old"]); } else { $v_photo_old = $_POST["v_photo_old"]; }   
                if(!isset($_POST["v_keterangan"])){ $v_keterangan = isset($_POST["v_keterangan"]); } else { $v_keterangan = $_POST["v_keterangan"]; } 
                
                if(!isset($_POST["v_jumlah_pj"])){ $v_jumlah_pj = isset($_POST["v_jumlah_pj"]); } else { $v_jumlah_pj = $_POST["v_jumlah_pj"]; } 
                if(!isset($_POST["v_sid_pj"])){ $v_sid_pj = isset($_POST["v_sid_pj"]); } else { $v_sid_pj = $_POST["v_sid_pj"]; } 
                if(!isset($_POST["v_penanggung_jawab"])){ $v_penanggung_jawab = isset($_POST["v_penanggung_jawab"]); } else { $v_penanggung_jawab = $_POST["v_penanggung_jawab"]; } 
                if(!isset($_POST["v_emp_id"])){ $v_emp_id = isset($_POST["v_emp_id"]); } else { $v_emp_id = $_POST["v_emp_id"]; }
                if(!isset($_POST["v_emp_name"])){ $v_emp_name = isset($_POST["v_emp_name"]); } else { $v_emp_name = $_POST["v_emp_name"]; } 
                if(!isset($_POST["v_nominal"])){ $v_nominal = isset($_POST["v_nominal"]); } else { $v_nominal = $_POST["v_nominal"]; } 
                if(!isset($_POST["v_metode_penyusutan_id"])){ $v_metode_penyusutan_id = isset($_POST["v_metode_penyusutan_id"]); } else { $v_metode_penyusutan_id = $_POST["v_metode_penyusutan_id"]; } 
                
                if(!isset($_POST["v_sid_lokasi"])){ $v_sid_lokasi = isset($_POST["v_sid_lokasi"]); } else { $v_sid_lokasi = $_POST["v_sid_lokasi"]; } 
                if(!isset($_POST["v_cabang_id"])){ $v_cabang_id = isset($_POST["v_cabang_id"]); } else { $v_cabang_id = $_POST["v_cabang_id"]; } 
                if(!isset($_POST["v_depo_id"])){ $v_depo_id = isset($_POST["v_depo_id"]); } else { $v_depo_id = $_POST["v_depo_id"]; } 
                if(!isset($_POST["v_ruangan_id"])){ $v_ruangan_id = isset($_POST["v_ruangan_id"]); } else { $v_ruangan_id = $_POST["v_ruangan_id"]; } 
                
                // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)    
                if(!isset($_POST["v_desc_merek"])){ $v_desc_merek = isset($_POST["v_desc_merek"]); } else { $v_desc_merek = $_POST["v_desc_merek"]; }   
                if(!isset($_POST["v_desc_tipe"])){ $v_desc_tipe = isset($_POST["v_desc_tipe"]); } else { $v_desc_tipe = $_POST["v_desc_tipe"]; }   
                if(!isset($_POST["v_desc_jenis"])){ $v_desc_jenis = isset($_POST["v_desc_jenis"]); } else { $v_desc_jenis = $_POST["v_desc_jenis"]; }    
                if(!isset($_POST["v_desc_warna"])){ $v_desc_warna = isset($_POST["v_desc_warna"]); } else { $v_desc_warna = $_POST["v_desc_warna"]; }   
                if(!isset($_POST["v_desc_tahun"])){ $v_desc_tahun = isset($_POST["v_desc_tahun"]); } else { $v_desc_tahun = $_POST["v_desc_tahun"]; }
                
                // type = Kendaraan
                if(!isset($_POST["v_kendaraan_merek"])){ $v_kendaraan_merek = isset($_POST["v_kendaraan_merek"]); } else { $v_kendaraan_merek = $_POST["v_kendaraan_merek"]; }   
                if(!isset($_POST["v_kendaraan_jenis"])){ $v_kendaraan_jenis = isset($_POST["v_kendaraan_jenis"]); } else { $v_kendaraan_jenis = $_POST["v_kendaraan_jenis"]; }   
                if(!isset($_POST["v_kendaraan_warna"])){ $v_kendaraan_warna = isset($_POST["v_kendaraan_warna"]); } else { $v_kendaraan_warna = $_POST["v_kendaraan_warna"]; }    
                if(!isset($_POST["v_kendaraan_cc"])){ $v_kendaraan_cc = isset($_POST["v_kendaraan_cc"]); } else { $v_kendaraan_cc = $_POST["v_kendaraan_cc"]; }   
                if(!isset($_POST["v_no_polisi"])){ $v_no_polisi = isset($_POST["v_no_polisi"]); } else { $v_no_polisi = $_POST["v_no_polisi"]; }    
                if(!isset($_POST["v_no_rangka"])){ $v_no_rangka = isset($_POST["v_no_rangka"]); } else { $v_no_rangka = $_POST["v_no_rangka"]; }   
                if(!isset($_POST["v_no_mesin"])){ $v_no_mesin = isset($_POST["v_no_mesin"]); } else { $v_no_mesin = $_POST["v_no_mesin"]; }   
                if(!isset($_POST["v_tgl_stnk"])){ $v_tgl_stnk = isset($_POST["v_tgl_stnk"]); } else { $v_tgl_stnk = $_POST["v_tgl_stnk"]; }      
                if(!isset($_POST["v_tgl_pajak"])){ $v_tgl_pajak = isset($_POST["v_tgl_pajak"]); } else { $v_tgl_pajak = $_POST["v_tgl_pajak"]; }     
                if(!isset($_POST["v_no_bpkb"])){ $v_no_bpkb = isset($_POST["v_no_bpkb"]); } else { $v_no_bpkb = $_POST["v_no_bpkb"]; } 
                
                // type = Gedung/Tanah
                if(!isset($_POST["v_gedung_nama_pemilik"])){ $v_gedung_nama_pemilik = isset($_POST["v_gedung_nama_pemilik"]); } else { $v_gedung_nama_pemilik = $_POST["v_gedung_nama_pemilik"]; }   
                if(!isset($_POST["v_gedung_luas_bangunan"])){ $v_gedung_luas_bangunan = isset($_POST["v_gedung_luas_bangunan"]); } else { $v_gedung_luas_bangunan = $_POST["v_gedung_luas_bangunan"]; }   
                if(!isset($_POST["v_gedung_luas_tanah"])){ $v_gedung_luas_tanah = isset($_POST["v_gedung_luas_tanah"]); } else { $v_gedung_luas_tanah = $_POST["v_gedung_luas_tanah"]; }    
                if(!isset($_POST["v_gedung_tinggi_bangunan"])){ $v_gedung_tinggi_bangunan = isset($_POST["v_gedung_tinggi_bangunan"]); } else { $v_gedung_tinggi_bangunan = $_POST["v_gedung_tinggi_bangunan"]; }   
                
                if(!isset($_POST["chk_gedung_par"])){ $chk_gedung_par = isset($_POST["chk_gedung_par"]); } else { $chk_gedung_par = $_POST["chk_gedung_par"]; }
                if(!isset($_POST["chk_gedung_earthquake"])){ $chk_gedung_earthquake = isset($_POST["chk_gedung_earthquake"]); } else { $chk_gedung_earthquake = $_POST["chk_gedung_earthquake"]; }
                
                if(!isset($_POST["v_gedung_no_asuransi_par"])){ $v_gedung_no_asuransi_par = isset($_POST["v_gedung_no_asuransi_par"]); } else { $v_gedung_no_asuransi_par = $_POST["v_gedung_no_asuransi_par"]; }    
                if(!isset($_POST["v_gedung_start_date_par"])){ $v_gedung_start_date_par = isset($_POST["v_gedung_start_date_par"]); } else { $v_gedung_start_date_par = $_POST["v_gedung_start_date_par"]; }   
                if(!isset($_POST["v_gedung_end_date_par"])){ $v_gedung_end_date_par = isset($_POST["v_gedung_end_date_par"]); } else { $v_gedung_end_date_par = $_POST["v_gedung_end_date_par"]; }  
                
                if(!isset($_POST["v_gedung_no_asuransi_earthquake"])){ $v_gedung_no_asuransi_earthquake = isset($_POST["v_gedung_no_asuransi_earthquake"]); } else { $v_gedung_no_asuransi_earthquake = $_POST["v_gedung_no_asuransi_earthquake"]; }    
                if(!isset($_POST["v_gedung_start_date_earthquake"])){ $v_gedung_start_date_earthquake = isset($_POST["v_gedung_start_date_earthquake"]); } else { $v_gedung_start_date_earthquake = $_POST["v_gedung_start_date_earthquake"]; }   
                if(!isset($_POST["v_gedung_end_date_earthquake"])){ $v_gedung_end_date_earthquake = isset($_POST["v_gedung_end_date_earthquake"]); } else { $v_gedung_end_date_earthquake = $_POST["v_gedung_end_date_earthquake"]; }  
                
                if(!isset($_POST["v_gedung_fungsi"])){ $v_gedung_fungsi = isset($_POST["v_gedung_fungsi"]); } else { $v_gedung_fungsi = $_POST["v_gedung_fungsi"]; }       
                if(!isset($_POST["v_gedung_desa"])){ $v_gedung_desa = isset($_POST["v_gedung_desa"]); } else { $v_gedung_desa = $_POST["v_gedung_desa"]; }       
                if(!isset($_POST["v_gedung_kecamatan"])){ $v_gedung_kecamatan = isset($_POST["v_gedung_kecamatan"]); } else { $v_gedung_kecamatan = $_POST["v_gedung_kecamatan"]; } 
                if(!isset($_POST["v_gedung_kota"])){ $v_gedung_kota = isset($_POST["v_gedung_kota"]); } else { $v_gedung_kota = $_POST["v_gedung_kota"]; }       
                if(!isset($_POST["v_gedung_kabupaten"])){ $v_gedung_kabupaten = isset($_POST["v_gedung_kabupaten"]); } else { $v_gedung_kabupaten = $_POST["v_gedung_kabupaten"]; } 
                if(!isset($_POST["v_gedung_provinsi"])){ $v_gedung_provinsi = isset($_POST["v_gedung_provinsi"]); } else { $v_gedung_provinsi = $_POST["v_gedung_provinsi"]; }
                
                // type = Gudang
                if(!isset($_POST["v_gudang_luas_bangunan"])){ $v_gudang_luas_bangunan = isset($_POST["v_gudang_luas_bangunan"]); } else { $v_gudang_luas_bangunan = $_POST["v_gudang_luas_bangunan"]; }   
                if(!isset($_POST["v_gudang_luas_tanah"])){ $v_gudang_luas_tanah = isset($_POST["v_gudang_luas_tanah"]); } else { $v_gudang_luas_tanah = $_POST["v_gudang_luas_tanah"]; }   
                
                if(!isset($_POST["chk_gudang_par"])){ $chk_gudang_par = isset($_POST["chk_gudang_par"]); } else { $chk_gudang_par = $_POST["chk_gudang_par"]; }
                if(!isset($_POST["chk_gudang_earthquaks"])){ $chk_gudang_earthquaks = isset($_POST["chk_gudang_earthquaks"]); } else { $chk_gudang_earthquaks = $_POST["chk_gudang_earthquaks"]; }
                
                if(!isset($_POST["v_gudang_no_asuransi_par"])){ $v_gudang_no_asuransi_par = isset($_POST["v_gudang_no_asuransi_par"]); } else { $v_gudang_no_asuransi_par = $_POST["v_gudang_no_asuransi_par"]; }    
               	if(!isset($_POST["v_gudang_start_date_par"])){ $v_gudang_start_date_par = isset($_POST["v_gudang_start_date_par"]); } else { $v_gudang_start_date_par = $_POST["v_gudang_start_date_par"]; }   
                if(!isset($_POST["v_gudang_end_date_par"])){ $v_gudang_end_date_par = isset($_POST["v_gudang_end_date_par"]); } else { $v_gudang_end_date_par = $_POST["v_gudang_end_date_par"]; }   
                
                if(!isset($_POST["v_gudang_no_asuransi_earthquake"])){ $v_gudang_no_asuransi_earthquake = isset($_POST["v_gudang_no_asuransi_earthquake"]); } else { $v_gudang_no_asuransi_earthquake = $_POST["v_gudang_no_asuransi_earthquake"]; }    
               	if(!isset($_POST["v_gudang_start_date_earthquake"])){ $v_gudang_start_date_earthquake = isset($_POST["v_gudang_start_date_earthquake"]); } else { $v_gudang_start_date_earthquake = $_POST["v_gudang_start_date_earthquake"]; }   
                if(!isset($_POST["v_gudang_end_date_earthquake"])){ $v_gudang_end_date_earthquake = isset($_POST["v_gudang_end_date_earthquake"]); } else { $v_gudang_end_date_earthquake = $_POST["v_gudang_end_date_earthquake"]; } 
               	
               	if(!isset($_POST["v_gudang_desa"])){ $v_gudang_desa = isset($_POST["v_gudang_desa"]); } else { $v_gudang_desa = $_POST["v_gudang_desa"]; }   
                if(!isset($_POST["v_gudang_kecamatan"])){ $v_gudang_kecamatan = isset($_POST["v_gudang_kecamatan"]); } else { $v_gudang_kecamatan = $_POST["v_gudang_kecamatan"]; }        
                if(!isset($_POST["v_gudang_kota"])){ $v_gudang_kota = isset($_POST["v_gudang_kota"]); } else { $v_gudang_kota = $_POST["v_gudang_kota"]; }       
                if(!isset($_POST["v_gudang_kabupaten"])){ $v_gudang_kabupaten = isset($_POST["v_gudang_kabupaten"]); } else { $v_gudang_kabupaten = $_POST["v_gudang_kabupaten"]; }       
                if(!isset($_POST["v_gudang_provinsi"])){ $v_gudang_provinsi = isset($_POST["v_gudang_provinsi"]); } else { $v_gudang_provinsi = $_POST["v_gudang_provinsi"]; } 
               	
				$lokasi_file    = $_FILES['fupload']['tmp_name'];
				$nama_file      = $_FILES['fupload']['name'];
				$tipe_file      = $_FILES['fupload']['type'];
				$acak           = rand(000000,999999);
				$name_paksa     = preg_replace("/[^A-Z0-9._-]/i", "_", $nama_file);
				$nama_file_unik = $acak.$name_paksa;

				$v_tgl_stnk     = format_save_date($v_tgl_stnk);
				$v_tgl_pajak    = format_save_date($v_tgl_pajak);
				$v_nominal      = save_int($v_nominal);
				$v_kendaraan_cc = save_int($v_kendaraan_cc);
				
                if($v_del==1)   
                {
                	if($btn_delete=="Delete")
                	{
                		if($v_jumlah_pj==1)
                		{	
                		    unlink("photo_fixed_asset/".$v_photo_old."");   
                            unlink("photo_fixed_asset/medium_".$v_photo_old.""); 
                            unlink("photo_fixed_asset/thumb_".$v_photo_old.""); 
	                            
                			// hapus fa_fixed_asset
	                		{
		                		$q = "
		                            DELETE FROM
		                                ".$db["master"].".fa_fixed_asset
		                            WHERE
		                                ".$db["master"].".fa_fixed_asset.fixed_asset_id = '".$v_fixed_asset_id."'    
		                        ";
		                        if(!mysql_query($q))
		                        {
		                            $msg = "Gagal menghapus";
		                            echo "<script>alert('".$msg."');</script>";
		                            die();    
		                        }
							}
							
							// hapus lokasi
							{
		                		$q = "
		                            DELETE FROM
		                                ".$db["master"].".fa_lokasi
		                            WHERE
		                                ".$db["master"].".fa_lokasi.sid = '".$v_sid_lokasi."'    
		                        ";
		                        if(!mysql_query($q))
		                        {
		                            $msg = "Gagal menghapus detail";
		                            echo "<script>alert('".$msg."');</script>";
		                            die();    
		                        }
							}
							
							// hapus penanggung jawab
							{
		                		$q = "
		                            DELETE FROM
		                                ".$db["master"].".fa_penanggung_jawab
		                            WHERE
		                                ".$db["master"].".fa_penanggung_jawab.sid = '".$v_sid_pj."'    
		                        ";
		                        if(!mysql_query($q))
		                        {
		                            $msg = "Gagal menghapus detail";
		                            echo "<script>alert('".$msg."');</script>";
		                            die();    
		                        }
							}
	                                     
	                        $msg = "Berhasil menghapus";
	                        echo "<script>alert('".$msg."');</script>"; 
	                        echo "<script>parent.CallAjaxForm('search');</script>";         
	                        die();
						}
						else
						{          
	                        $msg = "Asset tidak bisa dihapus, \r\n Karena asset sudah pernah dimutasi";
	                        echo "<script>alert('".$msg."');</script>"; 
	                        echo "<script>parent.CallAjaxForm('search','".$v_fixed_asset_id."');</script>";         
	                        die();
						}
					}
				}
                else
                {
                    if($btn_save=="Save")
                    {   
                    	$where_fa_fixed_asset = "";
                    	$where_fa_pj="";
                    	
	                    if($v_type_id=="1") // type = Kendaraan
						{    
			                if($v_kendaraan_merek=="")
			                {   
			                	$msg = "Kendaraan Merek harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_kendaraan_jenis=="")
			                {   
			                	$msg = "Kendaraan Jenis harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_kendaraan_warna=="")
			                {   
			                	$msg = "Kendaraan Warna harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_kendaraan_cc=="")
			                {   
			                	$msg = "Kendaraan CC harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_no_polisi=="")
			                {   
			                	$msg = "No Polisi harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_no_rangka=="")
			                {   
			                	$msg = "No Rangka harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_no_mesin=="")
			                {   
			                	$msg = "No Mesin harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                /*else if($v_tgl_stnk=="")
			                {   
			                	$msg = "Tanggal STNK harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_tgl_pajak=="")
			                {   
			                	$msg = "Tanggal Pajak harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } 
			                else if($v_no_bpkb=="")
			                {   
			                	$msg = "No BPKB harus diisi";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
			                } */
			                
							$where_fa_fixed_asset .="
								desc_kendaraan_merek = '".$v_kendaraan_merek."',
		                        desc_kendaraan_jenis = '".$v_kendaraan_jenis."',
		                        desc_kendaraan_warna = '".$v_kendaraan_warna."',
		                        desc_kendaraan_cc = '".$v_kendaraan_cc."',
		                        desc_kendaraan_nopol = '".$v_no_polisi."',
		                        desc_kendaraan_no_rangka = '".$v_no_rangka."',
		                        desc_kendaraan_no_mesin = '".$v_no_mesin."',
		                        desc_kendaraan_tgl_stnk = '".$v_tgl_stnk."',
		                        desc_kendaraan_tgl_pajak = '".$v_tgl_pajak."',
		                        desc_kendaraan_no_bpkb = '".$v_no_bpkb."',
							";	
						}
						else if($v_type_id=="7") // type = Tanah/Gedung
	                    {
	                    	if($chk_gedung_par)
	                    	{	
	                    		$v_gedung_no_asuransi_par = save_char($v_gedung_no_asuransi_par);
	                    		$v_gedung_start_date_par = format_save_date($v_gedung_start_date_par);
	                    		$v_gedung_end_date_par = format_save_date($v_gedung_end_date_par);
	                    		
								if($v_gedung_no_asuransi_par=="")
					            {   
					            	$msg = "No Asuransi Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gedung_start_date_par=="")
					            {   
					            	$msg = "Periode Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gedung_end_date_par=="")
					            {   
					            	$msg = "Periode Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            
								$where_fa_fixed_asset .= "desc_gedung_asuransi_par = '1',";	 
								$where_fa_fixed_asset .= "desc_gedung_no_asuransi_par = '".$v_gedung_no_asuransi_par."',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_par = '".$v_gedung_start_date_par."',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_par = '".$v_gedung_end_date_par."',";	
							}
							else
							{
								$where_fa_fixed_asset .= "desc_gedung_asuransi_par = '0',";	 
								$where_fa_fixed_asset .= "desc_gedung_no_asuransi_par = '',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_par = '0000-00-00',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_par = '0000-00-00',";
							}
							
							if($chk_gedung_earthquake)
	                    	{
	                    		$v_gedung_no_asuransi_earthquake = save_char($v_gedung_no_asuransi_earthquake);
	                    		$v_gedung_start_date_earthquake = format_save_date($v_gedung_start_date_earthquake);
	                    		$v_gedung_end_date_earthquake = format_save_date($v_gedung_end_date_earthquake);
	                    		
	                    		if($v_gedung_no_asuransi_earthquake=="")
					            {   
					            	$msg = "No Asuransi Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gedung_start_date_earthquake=="")
					            {   
					            	$msg = "Periode Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gedung_end_date_earthquake=="")
					            {   
					            	$msg = "Periode Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            }
					            
								$where_fa_fixed_asset .= "desc_gedung_asuransi_earthquake = '1',";	 
								$where_fa_fixed_asset .= "desc_gedung_no_asuransi_earthquake = '".$v_gedung_no_asuransi_par."',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_earthquake = '".$v_gedung_start_date_earthquake."',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_earthquake = '".$v_gedung_end_date_earthquake."',";
							}
							else
							{
								$where_fa_fixed_asset .= "desc_gedung_asuransi_earthquake = '0',";	 
								$where_fa_fixed_asset .= "desc_gedung_no_asuransi_earthquake = '',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_start_date_earthquake = '0000-00-00',";	
								$where_fa_fixed_asset .= "desc_gedung_asuransi_end_date_earthquake = '0000-00-00',";
							}
							
	                    	$where_fa_fixed_asset .="
								desc_gedung_nama_pemilik = '".$v_gedung_nama_pemilik."',
		                        desc_gedung_luas_bangunan = '".save_int($v_gedung_luas_bangunan)."',
		                        desc_gedung_luas_tanah = '".save_int($v_gedung_luas_tanah)."',
		                        desc_gedung_tinggi_bangunan = '".save_int($v_gedung_tinggi_bangunan)."',
		                        desc_gedung_fungsi = '".$v_gedung_fungsi."',
		                        desc_gedung_desa = '".$v_gedung_desa."',
		                        desc_gedung_kecamatan = '".$v_gedung_kecamatan."',
		                        desc_gedung_kota = '".$v_gedung_kota."',
		                        desc_gedung_kabupaten = '".$v_gedung_kabupaten."',
		                        desc_gedung_provinsi = '".$v_gedung_provinsi."',
							";
						}
						else if($v_type_id=="4") // type = Gudang
						{
							if($chk_gudang_par)
	                    	{	
	                    		$v_gudang_no_asuransi_par = save_char($v_gudang_no_asuransi_par);
	                    		$v_gudang_start_date_par = format_save_date($v_gudang_start_date_par);
	                    		$v_gudang_end_date_par = format_save_date($v_gudang_end_date_par);
	                    		
								if($v_gudang_no_asuransi_par=="")
					            {   
					            	$msg = "No Asuransi Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gudang_start_date_par=="")
					            {   
					            	$msg = "Periode Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gudang_end_date_par=="")
					            {   
					            	$msg = "Periode Par harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            
								$where_fa_fixed_asset .= "desc_gudang_asuransi_par = '1',";	 
								$where_fa_fixed_asset .= "desc_gudang_no_asuransi_par = '".$v_gudang_no_asuransi_par."',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_par = '".$v_gudang_start_date_par."',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_par = '".$v_gudang_end_date_par."',";	
							}
							else
							{
								$where_fa_fixed_asset .= "desc_gudang_asuransi_par = '0',";	 
								$where_fa_fixed_asset .= "desc_gudang_no_asuransi_par = '',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_par = '0000-00-00',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_par = '0000-00-00',";
							}
							
							if($chk_gudang_earthquaks)
	                    	{
	                    		$v_gudang_no_asuransi_earthquake = save_char($v_gudang_no_asuransi_earthquake);
	                    		$v_gudang_start_date_earthquake = format_save_date($v_gudang_start_date_earthquake);
	                    		$v_gudang_end_date_earthquake = format_save_date($v_gudang_end_date_earthquake);
	                    		
	                    		if($v_gudang_no_asuransi_earthquake=="")
					            {   
					            	$msg = "No Asuransi Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gudang_start_date_earthquake=="")
					            {   
					            	$msg = "Periode Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            } 
					            else if($v_gudang_end_date_earthquake=="")
					            {   
					            	$msg = "Periode Earthquake harus diisi";
					                echo "<script>alert('".$msg."');</script>";
					                die();
					            }
					            
								$where_fa_fixed_asset .= "desc_gudang_asuransi_earthquake = '1',";	 
								$where_fa_fixed_asset .= "desc_gudang_no_asuransi_earthquake = '".$v_gudang_no_asuransi_earthquake."',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_earthquake = '".$v_gudang_start_date_earthquake."',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_earthquake = '".$v_gudang_end_date_earthquake."',";
							}
							else
							{
								$where_fa_fixed_asset .= "desc_gudang_asuransi_earthquake = '0',";	 
								$where_fa_fixed_asset .= "desc_gudang_no_asuransi_earthquake = '',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_start_date_earthquake = '0000-00-00',";	
								$where_fa_fixed_asset .= "desc_gudang_asuransi_end_date_earthquake = '0000-00-00',";
							}
							
	                    	$where_fa_fixed_asset .="
		                        desc_gudang_luas_bangunan = '".save_int($v_gudang_luas_bangunan)."',
		                        desc_gudang_luas_tanah = '".save_int($v_gudang_luas_tanah)."',
		                        desc_gudang_desa = '".$v_gudang_desa."',
		                        desc_gudang_kecamatan = '".$v_gudang_kecamatan."',
		                        desc_gudang_kota = '".$v_gudang_kota."',
		                        desc_gudang_kabupaten = '".$v_gudang_kabupaten."',
		                        desc_gudang_provinsi = '".$v_gudang_provinsi."',
							";
						}
						else if($v_type_id=="2" || $v_type_id=="3" || $v_type_id=="6") // type = Perlengkapan Kantor // Perlengkapan Gedung // Umum (Dapur)
						{
							if($v_desc_merek=="")
				            {   
				            	$msg = "Merek harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            } 
				            else if($v_desc_tipe=="")
				            {   
				            	$msg = "Tipe harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            else if($v_desc_jenis=="")
				            {   
				            	$msg = "Jenis harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            else if($v_desc_warna=="")
				            {   
				            	$msg = "Warna harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            else if($v_desc_tahun=="")
				            {   
				            	$msg = "Tahun harus diisi";
				                echo "<script>alert('".$msg."');</script>";
				                die();
				            }
				            else
				            {
								$where_fa_fixed_asset .= "desc_merek = '".$v_desc_merek."',";	 
								$where_fa_fixed_asset .= "desc_tipe = '".$v_desc_tipe."',";	
								$where_fa_fixed_asset .= "desc_jenis = '".$v_desc_jenis."',";
								$where_fa_fixed_asset .= "desc_warna = '".$v_desc_warna."',";
								$where_fa_fixed_asset .= "desc_tahun = '".$v_desc_tahun."',";
							}
						}
						
						$search_empl = "";
						if($v_penanggung_jawab=="personal")
	                	{
	                		/*if($v_emp_id=="")
	                		{
			                	$msg = "Nama Penanggung Jawab harus dipilih";
			                    echo "<script>alert('".$msg."');</script>";
			                    die();
							}*/
							
							$search_empl = $v_emp_id;
						}
						else if($v_penanggung_jawab=="umum")
	                	{
	                		$q="
	                			SELECT 
								  depo.employee_id 
								FROM
								  depo 
								WHERE 1 
								  AND depo.depo_id = '".$v_depo_id."' 
								ORDER BY 
								  depo.depo_id ASC
	                		";
	                		$qry = mysql_query($q);
	                		$depo_emp = mysql_fetch_array($qry);
	                		
	                		$search_empl = $depo_emp["employee_id"];
						}
					
						if($nama_file)
	                    {  
	                        if($tipe_file=="image/jpeg" || $tipe_file=="image/jpg" || $tipe_file=="image/gif" || $tipe_file=="image/png")
	                        {
		                    	unlink("photo_fixed_asset/".$v_photo_old."");   
	                            unlink("photo_fixed_asset/medium_".$v_photo_old.""); 
	                            unlink("photo_fixed_asset/thumb_".$v_photo_old.""); 
                            
	                        	UploadBukti($nama_file_unik,"photo_fixed_asset");  
	                                                        
	                            $where_fa_fixed_asset .= "photo='".$nama_file_unik."',"; 
	                        }
	                        else
	                        {
	                            $msg = "Dokumen lampiran harus .jpg .gif .png .pdf";
	                            echo "<script>alert('".$msg."');</script>";
	                            die();
	                        } 
	                    }
						
						if($v_nominal)
						{
							$where_fa_fixed_asset .= "nominal='".$v_nominal."',";
						}
						
						if($v_metode_penyusutan_id)
						{
							$where_fa_fixed_asset .= "metode_penyusutan_id='".$v_metode_penyusutan_id."',";
						}

						// update fa fixed asset
						{
							$q = "
	                            UPDATE
	                            	".$db["master"].".fa_fixed_asset
	                            SET     
	                            	".$where_fa_fixed_asset."
			                        keterangan = '".$v_keterangan."',
			                        edited_user = '".$ses_login."',
			                        edited_date = NOW()
	                             WHERE 1
	                                AND ".$db["master"].".fa_fixed_asset.fixed_asset_id = '".$v_fixed_asset_id."'              
	                        ";  
	                       	//echo $q."<hr/>";             
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan 1";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
						}
						
						// update penanggung jawab
						{
							$q = "
	                            UPDATE
	                            	".$db["master"].".fa_penanggung_jawab
	                            SET  
									type_pj = '".$v_penanggung_jawab."',
									employee_id = '".$search_empl."'
	                             WHERE 1
	                                AND ".$db["master"].".fa_penanggung_jawab.sid = '".$v_sid_pj."'   
	                                AND ".$db["master"].".fa_penanggung_jawab.fixed_asset_id = '".$v_fixed_asset_id."'              
	                        ";
	                        //echo $q."<hr/>";                         
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan 2";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
						}
						
						// update lokasi
						{
							$q = "
	                            UPDATE
	                            	".$db["master"].".fa_lokasi
	                            SET     
	                            	depo_id = '".$v_depo_id."',
									ruangan_id = '".$v_ruangan_id."'
	                             WHERE 1
	                                AND ".$db["master"].".fa_lokasi.sid = '".$v_sid_lokasi."'   
	                                AND ".$db["master"].".fa_lokasi.fixed_asset_id = '".$v_fixed_asset_id."'              
	                        ";
	                        //echo $q."<hr/>";                  
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan 2";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
						}

						$msg = "Berhasil menyimpan";
		                echo "<script>alert('".$msg."');</script>"; 
		                echo "<script>parent.CallAjaxForm('search','".$v_fixed_asset_id."');</script>";         
		                die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  