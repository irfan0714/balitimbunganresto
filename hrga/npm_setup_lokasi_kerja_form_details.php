<table class="table table-bordered responsive">
	<thead>
		<tr class="title_blue">
		    <th width="10"><center>No</center></center></th>
		    <th width="80"><center>NIK</center></th>
		    <th width="80"><center>Kode HRD</center></th>
		    <th><center>Nama</center></th>    
		    <th width="100"><center>Departemen</center></th>
		    <th width="100"><center>Jabatan</center></th>
		    <th width="100"><center>Atasan</center></th>
		    <th width="80"><center>Username</center></th>
		    <th width="80"><center>Birthday</center></th>
		    <th width="50"><center>
		        Absensi<br>
		        <input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')">
		    </center></th>
		    <th width="80"><center>Masa Kerja</center></th>
		    <th width="50"><center>Save</center></th>
		    <th width="50"><center>Delete</center></th>
		</tr>
	</thead>
	<tbody>

 	<?php
    $no = 1; 
    for($i=1;$i<=1;$i++)
    {
        ?>
            <tr>
                <td>
                    <input type="hidden" name="no[]" value="<?php echo $no; ?>">
                    <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="0">
                    <input type="hidden" name="v_employee_id_<?php echo $no; ?>" id="v_employee_id_<?php echo $no; ?>" value="0">
                    <input type="hidden" name="v_username_<?php echo $no; ?>" id="v_username_<?php echo $no; ?>" value="">
                </td>
                <td><nobr><span id="show_employee_nik_<?php echo $no; ?>"></span>
                	
                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" name="btn_emp" id="btn_emp" value="..." onclick="pop_up_search_employee_details('<?php echo $no; ?>')">
						<i class="entypo-dot-3"></i>
					</button></nobr>
				</td>
                <td id="show_employee_code_hrd_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_employee_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_departemen_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_jabatan_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_atasan_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_username_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_birthday_<?php echo $no; ?>">&nbsp;</td>
                
                <td align="center">
                    &nbsp;
                </td>
                
                <td id="show_masa_kerja_<?php echo $no; ?>">&nbsp;</td>
                <td align="center">
                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Simpan" title="" name="btn_save_ajax" id="btn_save_ajax" value="Save" onClick="CallAjaxForm('save_ajax','<?php echo $no; ?>')">
						<i class="entypo-floppy"></i>
					</button>
				</td>
				<td>&nbsp;</td>
            </tr>
        <?php
        $no++;
         
    }
    
	?>

	<?php 
    $q = "
            SELECT 
                ".$db["master"].".setup_lokasi_kerja.sid,
                ".$db["master"].".setup_lokasi_kerja.remarks,
                ".$db["master"].".setup_lokasi_kerja.absensi,
                ".$db["master"].".employee.employee_id,
                ".$db["master"].".employee.employee_nik,
                ".$db["master"].".employee.employee_code_hrd,
                ".$db["master"].".employee.employee_name,
                ".$db["master"].".employee.username,
                ".$db["master"].".employee.birthday,
                
                tbl_position.sid_position,
                tbl_position.start_date,
                tbl_position.end_date,
                tbl_position.company_id,
                tbl_position.company_name,
                tbl_position.company_initial,
                tbl_position.cabang_id,
                tbl_position.cabang_name,
                tbl_position.depo_id,
                tbl_position.depo_name,
                tbl_position.divisi_id,
                tbl_position.divisi_name,
                tbl_position.departemen_id,
                tbl_position.departemen_name,
                tbl_position.jabatan_id,
                tbl_position.jabatan_name,
                
                tb_status.v_status,
                tb_status.v_date
                
            FROM 
                ".$db["master"].".setup_lokasi_kerja
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".setup_lokasi_kerja.employee_id
                INNER JOIN
                (
                    SELECT
                        uni.employee_id,
                        uni.sid_position,
                        uni.start_date,
                        uni.end_date,
                        uni.company_id,
                        uni.company_name,
                        uni.company_initial,
                        uni.cabang_id,
                        uni.cabang_name,
                        uni.depo_id,
                        uni.depo_name,
                        uni.divisi_id,
                        uni.divisi_name,
                        uni.departemen_id,
                        uni.departemen_name,
                        uni.jabatan_id,
                        uni.jabatan_name
                    FROM
                    (
                        SELECT
                          uni2.employee_id,
                          uni2.sid_position,
                          uni2.start_date,
                          uni2.end_date,
                          uni2.company_id,
                          uni2.company_name,
                          uni2.company_initial,
                          uni2.cabang_id,
                          uni2.cabang_name,
                          uni2.depo_id,
                          uni2.depo_name,
                          uni2.divisi_id,
                          uni2.divisi_name,
                          uni2.departemen_id,
                          uni2.departemen_name,
                          uni2.jabatan_id,
                          uni2.jabatan_name
                        FROM
                        (
                            SELECT 
                              employee_position.employee_id,
                              employee_position.`sid` AS sid_position,
                              employee_position.`start_date`,
                              employee_position.`end_date`,
                              company.company_id,
                              company.company_name,
                              company.company_initial,
                              hrd_cabang.cabang_id,
                              hrd_cabang.cabang_name,
                              depo.depo_id,
                              depo.depo_name,
                              hrd_divisi.divisi_id,
                              hrd_divisi.divisi_name,
                              hrd_departemen.departemen_id,
                              hrd_departemen.departemen_name,
                              jabatan.jabatan_id,
                              jabatan.jabatan_name 
                            FROM
                              employee_position 
                              INNER JOIN company 
                                ON employee_position.company_id = company.company_id 
                              INNER JOIN hrd_divisi 
                                ON employee_position.divisi_id = hrd_divisi.divisi_id 
                              INNER JOIN hrd_departemen 
                                ON employee_position.departemen_id = hrd_departemen.departemen_id 
                              INNER JOIN depo 
                                ON employee_position.depo_id = depo.depo_id 
                              INNER JOIN hrd_cabang 
                                ON depo.cabang_id = hrd_cabang.cabang_id 
                              INNER JOIN jabatan 
                                ON employee_position.jabatan_id = jabatan.jabatan_id 
                            WHERE 
                              1
                            ORDER BY
                              employee_position.`start_date` DESC
                        )AS uni2
                        WHERE
                            1
                        GROUP BY
                            uni2.employee_id
                    )AS uni
                    WHERE
                        1 
                )AS tbl_position ON
                    tbl_position.employee_id = ".$db["master"].".employee.employee_id
               	INNER JOIN 
			    (
				    SELECT 
				      tbl_fixed.employee_id,
				      tbl_fixed.v_status,
				      tbl_fixed.v_date 
				    FROM
				      (SELECT 
				        uni.employee_id,
				        uni.v_status,
				        uni.v_date 
				      FROM
				        (SELECT 
				          employee_join.employee_id,
				          'join' AS v_status,
				          employee_join.join_date AS v_date 
				        FROM
				          employee_join 
				        WHERE 1 
				        UNION
				        ALL 
				        SELECT 
				          employee_resign.employee_id,
				          'resign' AS v_status,
				          employee_resign.resign_date AS v_date 
				        FROM
				          employee_resign 
				        WHERE 1) AS uni 
				      WHERE 1 
				      GROUP BY uni.employee_id,
				        uni.v_date 
				      ORDER BY uni.employee_id DESC,
				        uni.v_date DESC) AS tbl_fixed 
				    WHERE 1 
				    GROUP BY tbl_fixed.employee_id
				) AS tb_status 
			    ON tb_status.employee_id = employee.employee_id 
            WHERE
                1
                AND ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id = '".$id."'
            ORDER BY
                ".$db["master"].".employee.employee_name ASC
            
    ";
    $no_view=1;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {   
		list(
			$sid,
			$remarks,
			$absensi,
			$employee_id,
			$employee_nik,
			$employee_code_hrd,
			$employee_name,
			$username,
			$birthday,
			$sid_position,
			$start_date,
			$end_date,
			$company_id,
			$company_name,
			$company_initial,
			$cabang_id,
			$cabang_name,
			$depo_id,
			$depo_name,
			$divisi_id,
			$divisi_name,
			$departemen_id,
			$departemen_name,
			$jabatan_id,
			$jabatan_name,
			$v_status,
			$v_date
		)=$row;
		
		if($absensi)
        {
            $checkbox_absensi = "checked='checked'";
        }
        else
        {
            $checkbox_absensi = "";
        }
        
         $q = "
            SELECT
                ".$db["master"].".employee.employee_id,
                ".$db["master"].".employee.employee_nik,
                ".$db["master"].".employee.employee_code_hrd,
                ".$db["master"].".employee.employee_name
            FROM
                ".$db["master"].".setup_atasan_details 
                INNER JOIN ".$db["master"].".setup_atasan ON
                    ".$db["master"].".setup_atasan_details.setup_atasan_id =  ".$db["master"].".setup_atasan.setup_atasan_id
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".setup_atasan.employee_id = ".$db["master"].".employee.employee_id
            WHERE
                1
                AND ".$db["master"].".setup_atasan_details.employee_id = '".$employee_id."'
        ";
        $qry_atasan = mysql_query($q);
        $row_atasan = mysql_fetch_array($qry_atasan);
        list($atasan_id, $atasan_nik, $atasan_code_hrd, $atasan_name) = $row_atasan;
        
        $atasan = $atasan_name;
        
        $bg_color_birthday = "";
        if($username=="")
        {
            if($birthday=="" || $birthday=="0000-00-00")
            {
                $bg_color_birthday = "background: yellow";     
            }
        }
		
		if($v_status=="resign")
        {
            $bgcolor_status = "background:#FFCCFF;";
        }
        else
        {
            $bgcolor_status = "";    
        }
        
		?>
		<tr style="<?php echo $bgcolor_status; ?>">
		    <td>
		        <?php echo $no_view; ?>
		        <input type="hidden" name="no[]" value="<?php echo $no; ?>">
		        <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="<?php echo $sid; ?>">
		        <input type="hidden" name="v_employee_id_<?php echo $no; ?>" id="v_employee_id_<?php echo $no; ?>" value="<?php echo $employee_id; ?>">
		        <input type="hidden" name="v_data_all[]" value="<?php echo $employee_id; ?>">
		    </td>
		    <td><nobr><span id="show_employee_nik_<?php echo $no; ?>"><?php echo $employee_nik; ?></span>
		    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" name="btn_emp" id="btn_emp" value="..." onclick="pop_up_search_employee_details('<?php echo $no; ?>')">
					<i class="entypo-dot-3"></i>
				</button></nobr>
			</td>
		    <td id="show_employee_code_hrd_<?php echo $no; ?>"><?php echo $remployee_code_hrd; ?>&nbsp;</td>
		    <td id="show_employee_name_<?php echo $no; ?>">
		        <?php echo $employee_name; ?>&nbsp;
		        <input type="hidden" name="v_employee_name_<?php echo $no; ?>" id="v_employee_name_<?php echo $no; ?>" value="<?php echo $employee_name; ?>">
		    </td>
		    <td id="show_departemen_name_<?php echo $no; ?>">
		        <?php echo $departemen_name; ?>&nbsp;
		    </td>
		    <td id="show_jabatan_name_<?php echo $no; ?>"><?php echo $jabatan_name; ?>&nbsp;</td>
		    <td id="show_atasan_<?php echo $no; ?>">&nbsp;</td>
		    <td id="show_username_<?php echo $no; ?>">
		        <?php 
		            if($username=="")
		            {
		                ?>
		                    <nobr>
		                    <input type="text" class="form-control-new" name="v_username_<?php echo $no; ?>" id="v_username_<?php echo $no; ?>" value="<?php echo $username; ?>" size="10" maxlength="255" />        
		                    
		                    <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Generate Username" title="" value="Gen" onclick="CallAjaxForm('generate_username','<?php echo $no; ?>')">
								<i class="entypo-arrows-ccw"></i>
							</button>
		                    </nobr>
		                <?php
		            }
		            else
		            {
		            	?><input type="hidden" name="v_username_<?php echo $no; ?>" id="v_username_<?php echo $no; ?>" value="<?php echo $username; ?>" /><?php
		                echo $username;   
		            }
		        ?>
		    </td>
		    <td id="show_birthday_<?php echo $no; ?>" align="center" style="<?php echo $bg_color_birthday; ?>">
		        <?php echo format_show_date($birthday); ?>&nbsp;
		        <input type="hidden" name="v_birthday_<?php echo $no; ?>" id="v_birthday_<?php echo $no; ?>" value="<?php echo format_show_date($birthday); ?>">
		    </td>
		    
		    <td align="center">
		        <input type="checkbox" <?php echo $checkbox_absensi; ?> name="v_data[]" id="v_data_<?php echo $no; ?>" value="<?php echo $employee_id; ?>">
		    </td>
		    <td style="text-align: center;"><nobr><?php echo masa_kerja($employee_id, date("Y-m-d")); ?></nobr></td>
		    <td align="center">
            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Simpan" title="" name="btn_save_ajax" id="btn_save_ajax" value="Save" onClick="CallAjaxForm('save_ajax','<?php echo $no; ?>')">
					<i class="entypo-floppy"></i>
				</button>
			</td>
		    <td align="center"><input type="checkbox" name="del[]" value="<?php echo $sid; ?>"></td>
		</tr>
		<?php
		
		$no++;
		$no_view++;
		
		$arr_data["list_data"][$sid]=$sid;
		
	}
    
	if($id!="")
	{
		if(count($arr_data["list_data"])*1>0)
		{
		?>
			<tr>     
			<td colspan="12">&nbsp;</td>
			<td align="center">
				
            	<button type="submit" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" name="btn_del_details" id="btn_del_details" value="<?php echo $show_btn; ?>" >
					<i class="entypo-trash"></i>
				</button>
			</td>
			</tr>
		<?php 
		}
	}
	?>
    
	</tbody>
</table>