<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    
    $modul = "FA Type";
    $list  = "npm_fa_type.php";
    $htm   = "npm_fa_type_form.php";
    $pk    = "type_id";
    $title = "type_name";
	
	$q_cek = "
		SELECT
			uni.type_id
		FROM
		(
			SELECT
				type_id
			FROM
				fa_sub_type
			WHERE
				type_id = '".$id."'
            
            UNION 
            
            SELECT
                type_id
            FROM
                fa_proposal
            WHERE
                type_id = '".$id."'
		) as uni
		LIMIT
			0,1
	";
	$qry_cek = mysql_query($q_cek);
	$jml_cek = mysql_num_rows($qry_cek);
	
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena ada relasi dengan FA SUB TYPE / PROPOSAL";
        }
        else
        {
	
			$q = "
					DELETE FROM
						fa_type
					WHERE
						type_id = '".$id."'
			";
			if(!mysql_query($q))
			{
				$msg = "Failed Delete";
			}
			else
			{
				header("Location: ".$list.$link_back.$link_order_by);
			}  
		}
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
        $v_kategory_id          = save_char($_POST["v_kategory_id"]);
        $v_group_asset_id       = save_char($_POST["v_group_asset_id"]);
        $v_type_name            = save_char($_POST["v_type_name"]);
		$v_metode_penyusutan_id = save_char($_POST["v_metode_penyusutan_id"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        fa_type
                    SET
                        kategory_id = '".$v_kategory_id."',
                        group_asset_id = '".$v_group_asset_id."',
                        type_name = '".$v_type_name."',
                        metode_penyusutan_id = '".$v_metode_penyusutan_id."'
                    WHERE
                        type_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update";
                }
                else
                {
                    $class_warning = "success";
                    $msg = "Berhasil menyimpan";
                }
            }
        }
        else
        {   
            $id = get_counter_int("","fa_type","type_id",100);   
            
            $q = "
                INSERT INTO
                    fa_type
                SET
                    type_id = '".$id."',
                    kategory_id = '".$v_kategory_id."',
                    group_asset_id = '".$v_group_asset_id."',
                    type_name = '".$v_type_name."',
                    metode_penyusutan_id = '".$v_metode_penyusutan_id."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert";
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            fa_type.*
        FROM 
            fa_type
        WHERE
            1
            AND fa_type.type_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
        SELECT
            fa_kategory.kategory_id,    
            fa_kategory.kategory_name
        FROM
            fa_kategory
        WHERE
            1
        ORDER BY
            fa_kategory.kategory_name ASC
    ";
    $qry_kat = mysql_query($q);
    while($row_kat = mysql_fetch_array($qry_kat))
    {
        list($kategory_id, $kategory_name) = $row_kat;
        
        $arr_data["list_kategory"][$kategory_id] = $kategory_id;
        $arr_data["kategory_name"][$kategory_id] = $kategory_name;
    }
    
    $q = "
        SELECT
            fa_group_asset.group_asset_id,    
            fa_group_asset.group_asset_name
        FROM
            fa_group_asset
        WHERE
            1
        ORDER BY
            fa_group_asset.group_asset_name ASC
    ";
    $qry_g_ass = mysql_query($q);
    while($row_g_ass = mysql_fetch_array($qry_g_ass))
    {
        list($group_asset_id, $group_asset_name) = $row_g_ass;
        
        $arr_data["list_group_asset"][$group_asset_id] = $group_asset_id;
        $arr_data["group_asset_name"][$group_asset_id] = $group_asset_name;
    }
    
    $q = "
        SELECT
            fa_metode_penyusutan.metode_penyusutan_id,    
            fa_metode_penyusutan.metode_penyusutan_name
        FROM
            fa_metode_penyusutan
        WHERE
            1
        ORDER BY
            fa_metode_penyusutan.metode_penyusutan_name ASC
    ";
    $qry_met = mysql_query($q);
    while($row_met = mysql_fetch_array($qry_met))
    {
        list($metode_penyusutan_id, $metode_penyusutan_name) = $row_met;
        
        $arr_data["list_metode_penyusutan"][$metode_penyusutan_id] = $metode_penyusutan_id;
        $arr_data["metode_penyusutan_name"][$metode_penyusutan_id] = $metode_penyusutan_name;
    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
		function validasi()
		{
			if(document.getElementById("v_kategory_id").value=="")
			{
				alert("Kategori harus dipilih...");
				document.getElementById("v_kategory_id").focus();
				return false;
			}
	        else if(document.getElementById("v_group_asset_id").value*1==0)
	        {
	            alert("Group Asset harus dipilih...");
	            document.getElementById("v_group_asset_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_type_name").value*1==0)
	        {
	            alert("Type Name harus diisi...");
	            document.getElementById("v_type_name").focus();
	            return false;
	        }
	        
		}
		
		function start_page()
		{
			document.getElementById("v_type_name").focus();	
		}
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    
			    	<tr>
	                    <td class="title_table" width="150">Kategory</td>
	                    <td> 
	                        <select class="form-control-new" name="v_kategory_id" id="v_kategory_id" style="width: 300px;">
	                            <option value="">-</option>
	                            <?php 
	                                foreach($arr_data["list_kategory"] as $kategory_id=>$val)
	                                {
	                                    $kategory_name = $arr_data["kategory_name"][$kategory_id];
	                                    ?>
	                                    <option <?php if($data["kategory_id"]==$kategory_id) echo "selected='selected'"; ?> value="<?php echo $kategory_id; ?>"><?php echo $kategory_name; ?></option>
	                                    <?php
	                                }    
	                            ?>
	                        </select>
	                    </td>
	                </tr>
	                
	                <tr>
	                    <td class="title_table">Group Asset</td>
	                    <td> 
	                        <select class="form-control-new" name="v_group_asset_id" id="v_group_asset_id" style="width: 300px;">
	                            <option value="">-</option>
	                            <?php 
	                                foreach($arr_data["list_group_asset"] as $group_asset_id=>$val)
	                                {
	                                    $group_asset_name = $arr_data["group_asset_name"][$group_asset_id];
	                                    ?>
	                                    <option <?php if($data["group_asset_id"]==$group_asset_id) echo "selected='selected'"; ?> value="<?php echo $group_asset_id; ?>"><?php echo $group_asset_name; ?></option>
	                                    <?php
	                                }    
	                            ?>
	                        </select>
	                    </td>
	                </tr>
	                
	                <tr>
	                    <td class="title_table">Metode Penyusutan</td>
	                    <td> 
	                        <select class="form-control-new" name="v_metode_penyusutan_id" id="v_metode_penyusutan_id" style="width: 300px;">
	                            <option value="">-</option>
	                            <?php 
	                                foreach($arr_data["list_metode_penyusutan"] as $metode_penyusutan_id=>$val)
	                                {
	                                    $metode_penyusutan_name = $arr_data["metode_penyusutan_name"][$metode_penyusutan_id];
	                                    ?>
	                                    <option <?php if($data["metode_penyusutan_id"]==$metode_penyusutan_id) echo "selected='selected'"; ?> value="<?php echo $metode_penyusutan_id; ?>"><?php echo $metode_penyusutan_name; ?></option>
	                                    <?php
	                                }    
	                            ?>
	                        </select>
	                    </td>
	                </tr>
	                
	                <tr>
	                    <td class="title_table">Type Name</td>
	                    <td> 
	                        <input type="text" class="form-control-new" name="v_type_name" id="v_type_name" value="<?php echo $data["type_name"]; ?>" style="width: 300px;" maxlength="100" />
	                    </td>
	                </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
			    </table>			    
			    </form>
			</div>
		</div>
		
<?php include("footer.php"); ?>