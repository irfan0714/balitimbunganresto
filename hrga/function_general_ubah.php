<?php

function update_nik($v_employee_id, $v_employee_nik, $v_employee_nik_old)
{
    global $db;
    
    $return = "";
    if($v_employee_nik!=$v_employee_nik_old)
    {
        $q = "
                SELECT
                    ".$db["master"].".employee.employee_nik
                FROM
                    ".$db["master"].".employee
                WHERE
                    ".$db["master"].".employee.employee_nik = '".$v_employee_nik."' 
                LIMIT
                    0,1
             ";
       $qry_cek = mysql_query($q);
       
       if(mysql_num_rows($qry_cek)*1==0)
       {
           $upd = "
                    UPDATE
                        ".$db["master"].".employee
                    SET
                        employee_nik = '".$v_employee_nik."'
                    WHERE
                        employee_id = '".$v_employee_id."'
           ";
           mysql_query($upd);
           
           $return = "Berhasil Update NIK<br>";
       }    
       else
       {
            $return = "Gagal Update, NIK ".$v_employee_nik." sudah digunakan<br>";    
       }
    }
    
    return $return;
}

function update_username($v_employee_id, $v_username, $v_username_old)
{
    global $db;
    global $default;
    
    $ins_group_user = false;
    
    $return = "";
    if($v_username!=$v_username_old || true)
    {
        
         $q = "
                SELECT
                    ".$db["master"].".user.user_id,
                    ".$db["master"].".user.username
                FROM
                    ".$db["master"].".user
                WHERE
                    ".$db["master"].".user.username = '".$v_username_old."' 
                LIMIT
                    0,1
             "; 
         $qry_cek2 = mysql_query($q);
         $row_cek2 = mysql_fetch_array($qry_cek2);
       
        
        $q = "
                SELECT
                    ".$db["master"].".employee.username,
                    ".$db["master"].".employee.password
                FROM
                    ".$db["master"].".employee
                WHERE
                    ".$db["master"].".employee.username = '".$v_username."' 
                LIMIT
                    0,1
             "; 
       $qry_cek = mysql_query($q);
       $row_cek = mysql_fetch_array($qry_cek);
       
       if(mysql_num_rows($qry_cek)*1==0 || true)
       {
           $upd = "
                    UPDATE
                        ".$db["master"].".employee
                    SET
                        username = '".$v_username."'
                    WHERE
                        employee_id = '".$v_employee_id."'
           ";
           mysql_query($upd);
           
           if($v_username)
           {
               if($row_cek2["username"])
               {
                    $q = "
                            UPDATE
                                ".$db["master"].".user
                            SET
                                username = '".$v_username."'
                            WHERE
                                user_id = '".$row_cek2["user_id"]."'
                   ";    
               }
               else
               {
                    $q = "
                            INSERT INTO
                                ".$db["master"].".user
                            SET
                                username = '".$v_username."',
                                otorisasi = 'Reguler'
                   ";
                   
                   $ins_group_user = true;
               }
               mysql_query($q);
               
               if($ins_group_user)
               {
                   $q = "
                            SELECT 
                                ".$db["master"].".user.user_id
                            FROM
                                ".$db["master"].".user
                            WHERE
                                ".$db["master"].".user.username = '".$v_username."'
                            LIMIT
                                0,1
                   ";
                   $qry_user_id = mysql_query($q);
                   $row_user_id = mysql_fetch_array($qry_user_id);
                   
                   $del = "DELETE FROM ".$db["master"].".group_user_details WHERE user_id = '".$row_user_id["user_id"]."'";
                   mysql_query($del);
                   
                   $ins = "
                            INSERT INTO
                                ".$db["master"].".group_user_details
                            SET
                                group_user_id = '3',
                                user_id = '".$row_user_id["user_id"]."'
                   ";
                   mysql_query($ins);
                   
               }
           }
                   
           $return = "Berhasil Update Username<br>";
       }    
       else
       {
            $return = "Gagal Update, Username ".$v_username." sudah digunakan<br>";    
       }
       
       //$q = "UPDATE ".$db["master"].".user SET password = '".md5($default["password"])."' WHERE username = '".$v_username."' ";
       //mysql_query($q);
    }
    
    return $return;
}


function call_save_query($report_name, $q_data, $user)
{
	global $db;
	
    $q = "
            SELECT
                ".$db["master"].".where_report.content
            FROM
                ".$db["master"].".where_report
            WHERE
				1
                AND ".$db["master"].".where_report.report_name = '".$report_name."'
                AND ".$db["master"].".where_report.username = '".$user."'
    ";
    $qry_report = mysql_query($q);
    $jml_report = mysql_num_rows($qry_report);

    if($jml_report*1==0)
    {
        $q = "
                INSERT INTO
                    ".$db["master"].".where_report
                SET
                    report_name = '".$report_name."',
                    username = '".$user."',
                    content = '".$q_data."'
        ";
        mysql_query($q);
    }
    else
    {
        $q = "
                UPDATE
                    ".$db["master"].".where_report
                SET
                    content = '".$q_data."'
                WHERE
                    1
                    AND report_name = '".$report_name."'
                    AND username = '".$user."'
        ";
        if(!mysql_query($q))
        {
            $msg = "Failed Update Report<br>".$q;
        }
    }
}

/*
function search_keyword($v_keyword, $arr_keyword)
{
	$exp_keyword = explode(" ",$v_keyword);
	$jml_keyword = count($exp_keyword)-1;
	
	$where_keyword = " AND
					   (
					 ";
	for($key=0;$key<=$jml_keyword;$key++)
	{
		foreach($arr_keyword as $val)
		{
			$where_keyword .= $val." LIKE '%".$exp_keyword[$key]."%' OR ";
		}
	}
	$where_keyword  = substr($where_keyword,0,-4); 
	$where_keyword .= ")";
	
	return $where_keyword;
}
 */
 
function search_keyword($v_keyword, $arr_keyword)
{
    $exp_keyword = explode(" ",$v_keyword);
    $jml_keyword = count($exp_keyword)-1;
    $jml_kolom   = count($arr_keyword);
    
    $where_keyword = "";
    for($key=0;$key<=$jml_keyword;$key++)
    {
        $i = 0;
        $where_keyword .= " AND ( ";
        foreach($arr_keyword as $val)
        {
            $where_keyword .= $val." LIKE '%".$exp_keyword[$key]."%' OR ";
            $i++;
            
            if($i==$jml_kolom)
            {
                $where_keyword = substr($where_keyword,0,-3);
            }
        }
        $where_keyword .= " ) ";
    }
    $where_keyword  = substr($where_keyword,0,-4);                                              
    

    
    $where_keyword .= ")";

    //echo $where_keyword;
    return $where_keyword;
}

function search_keyword_or($v_keyword, $arr_keyword)
{
    $exp_keyword = explode(" ",$v_keyword);
    $jml_keyword = count($exp_keyword)-1;
    $jml_kolom   = count($arr_keyword);
    
    $where_keyword = "";
    $where_keyword .= " AND ( ";
    for($key=0;$key<=$jml_keyword;$key++)
    {
        $i = 0;
        
        foreach($arr_keyword as $val)
        {
            $where_keyword .= $val." LIKE '%".$exp_keyword[$key]."%' OR ";
            $i++;
        }
        
    }
    $where_keyword = substr($where_keyword,0,-3);
    $where_keyword .= " ) ";
    //$where_keyword  = substr($where_keyword,0,-4);                                              

    //$where_keyword .= ")";

    //echo $where_keyword;
    return $where_keyword;
}

 
function get_group_user($user_id)
{
	global $db;
	
	$q = "
            SELECT 
                ".$db["master"].".group_user.group_user_name
            FROM 
                ".$db["master"].".group_user
				INNER JOIN ".$db["master"].".group_user_details ON
					".$db["master"].".group_user.group_user_id = ".$db["master"].".group_user_details.group_user_id
            WHERE
                1
                AND ".$db["master"].".group_user_details.user_id = '".$user_id."'
            LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
	
	return $data["group_user_name"];
}

function grandtotal_pembelian($id)
{
	global $db;
	
	$q = "
            SELECT 
                ".$db["master"].".pembelian.*
            FROM 
                ".$db["master"].".pembelian
            WHERE
                1
                AND ".$db["master"].".pembelian.pembelian_no = '".$id."'
            LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
	
	$q = "
			SELECT
				".$db["master"].".pembelian_details.qty,
				".$db["master"].".pembelian_details.harga
			FROM
				".$db["master"].".pembelian_details
				INNER JOIN ".$db["master"].".pembelian ON
					".$db["master"].".pembelian_details.pembelian_no = ".$db["master"].".pembelian.pembelian_no
			WHERE
				".$db["master"].".pembelian_details.pembelian_no = '".$id."'
			ORDER BY
				".$db["master"].".pembelian_details.sid ASC
	";
	$qry = mysql_query($q);
	$no = 1;
	$subtotal = 0;
	$total_1 = 0;
	while($r = mysql_fetch_object($qry))
	{
		$subtotal = ($r->harga * $r->qty); 
		$total_1 += $subtotal;                                    
	}
	
	$disc_header = $data["disc_amount"];
	if($data["disc_type"]=="Persen")
	{
		$disc_header = $total_1 * ($data["disc_amount"]/100);
	}
	
	$total2 = ($total_1 + $data["rounding"]) - ($disc_header + $data["dp"]);
	
	$ppn_value = 0;
	if($data["ppn"]=="Ya")
	{
		$ppn_value = $total2*0.1;
	}
	
	$total3 = $total2 + $ppn_value;
											
	$datax["total"] = $total3-$data["dp"];
	$datax["ppn"]   = $ppn_value;
	$datax["dp"]    = $data["dp"];
	
	$datax["jual"] = $total3-$ppn_value;
	
	return $datax;										
}

function format_show_date_bulan($date)
{
    if($date)
    {
        $exp_date = explode("-",$date);
        
        $bulan = $exp_date[1];
        
        if($bulan*1=="1")
        {
            $bulan_show = "Januari";
        }
        else if($bulan*1=="2")
        {
            $bulan_show = "Febuari";
        }
        else if($bulan*1=="3")
        {
            $bulan_show = "Maret";
        }
        else if($bulan*1=="4")
        {
            $bulan_show = "April";
        }
        else if($bulan*1=="5")
        {
            $bulan_show = "Mei";
        }
        else if($bulan*1=="6")
        {
            $bulan_show = "Juni";
        }
        else if($bulan*1=="7")
        {
            $bulan_show = "Juli";
        }
        else if($bulan*1=="8")
        {
            $bulan_show = "Agustus";
        }
        else if($bulan*1=="9")
        {
            $bulan_show = "September";
        }
        else if($bulan*1=="10")
        {
            $bulan_show = "Oktober";
        }
        else if($bulan*1=="11")
        {
            $bulan_show = "November";
        }
        else if($bulan*1=="12")
        {
            $bulan_show = "Desember";
        }
        
        $return = $exp_date[2]." ".$bulan_show." ".$exp_date[0];
        
    }
    else
    {
        $return = "";
    }
    
    return $return;
}


function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}


function get_total_group_user($group_user_id)
{
	global $db;

	$q = "
			SELECT
				COUNT(*) as jml
			FROM
				".$db["master"].".group_user_details
			WHERE
				".$db["master"].".group_user_details.group_user_id = '".$group_user_id."'
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["jml"];
}

function get_stok($barang_id,$gudang_id,$v_date_from,$v_date_to)
{
	global $db;
	unset($arr_data);
	
	$where_gudang_id = "";
	$where_gudang_id_from = "";
	$where_gudang_id_to = "";
	if($gudang_id!="Semua")
	{
		$where_gudang_id = " AND ".$db["master"].".gudang.gudang_id = '".$gudang_id."' ";
		$where_gudang_id_from = " AND ".$db["master"].".kirim_internal.gudang_id_from = '".$gudang_id."' ";
		$where_gudang_id_to = " AND ".$db["master"].".kirim_internal.gudang_id_to = '".$gudang_id."' ";
	}
	$row_kirim_internal_in_prev = 0;
	$row_kirim_internal_out_prev = 0;
	$row_kirim_internal_in_prev = 0;
	$row_kirim_internal_in = 0;
	$row_kirim_internal_out_prev = 0;
	$row_kirim_internal_out = 0;
	$row_kirim_internal_in = 0;
	$row_kirim_internal_out = 0;
	
	## IN STOCK
	// pembelian
	$q= "
            SELECT
                SUM(".$db["master"].".pembelian_details.qty) as qty
            FROM
                ".$db["master"].".pembelian_details
                INNER JOIN ".$db["master"].".pembelian ON
                    ".$db["master"].".pembelian_details.pembelian_no = ".$db["master"].".pembelian.pembelian_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pembelian.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pembelian_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pembelian.pembelian_date < '".format_save_date($v_date_from)."'
				AND ".$db["master"].".pembelian.status_pembelian = 'Selesai'
				".$where_gudang_id."
				
    ";
    $qry_pembelian_prev = mysql_query($q);
    $row_pembelian_prev = mysql_fetch_array($qry_pembelian_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".pembelian_details.qty) as qty
            FROM
                ".$db["master"].".pembelian_details
                INNER JOIN ".$db["master"].".pembelian ON
                    ".$db["master"].".pembelian_details.pembelian_no = ".$db["master"].".pembelian.pembelian_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pembelian.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pembelian_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pembelian.pembelian_date BETWEEN '".format_save_date($v_date_from)."' AND  '".format_save_date($v_date_to)."'
				AND ".$db["master"].".pembelian.status_pembelian = 'Selesai'
				".$where_gudang_id."
    ";
    $qry_pembelian = mysql_query($q);
    $row_pembelian = mysql_fetch_array($qry_pembelian);
	
	// adjustment_barang_no 	
	$q= "
            SELECT
                SUM(".$db["master"].".adjustment_barang_details.qty) as qty
            FROM
                ".$db["master"].".adjustment_barang_details
                INNER JOIN ".$db["master"].".adjustment_barang ON
                    ".$db["master"].".adjustment_barang_details.adjustment_barang_no = ".$db["master"].".adjustment_barang.adjustment_barang_no 	
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".adjustment_barang.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".adjustment_barang_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".adjustment_barang.adjustment_barang_date < '".format_save_date($v_date_from)."'
				AND ".$db["master"].".adjustment_barang.status_adjustment_barang = 'Disetujui'
				".$where_gudang_id."
				
    ";
    $qry_adjustment_prev = mysql_query($q);
    $row_adjustment_prev = mysql_fetch_array($qry_adjustment_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".adjustment_barang_details.qty) as qty
            FROM
                ".$db["master"].".adjustment_barang_details
                INNER JOIN ".$db["master"].".adjustment_barang ON
                    ".$db["master"].".adjustment_barang_details.adjustment_barang_no = ".$db["master"].".adjustment_barang.adjustment_barang_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".adjustment_barang.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".adjustment_barang_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".adjustment_barang.adjustment_barang_date BETWEEN '".format_save_date($v_date_from)."' AND  '".format_save_date($v_date_to)."'
				AND ".$db["master"].".adjustment_barang.status_adjustment_barang = 'Disetujui'
				".$where_gudang_id."
    ";
    $qry_adjustment = mysql_query($q);
    $row_adjustment = mysql_fetch_array($qry_adjustment);
    
	if($gudang_id!="Semua")
	{
		//kirim_internal IN
		$q= "
				SELECT
					SUM(".$db["master"].".kirim_internal_details.qty) as qty
				FROM
					".$db["master"].".kirim_internal_details
					INNER JOIN ".$db["master"].".kirim_internal ON
						".$db["master"].".kirim_internal_details.kirim_internal_no = ".$db["master"].".kirim_internal.kirim_internal_no
				WHERE
					".$db["master"].".kirim_internal_details.barang_id = '".$barang_id."'
					AND ".$db["master"].".kirim_internal.kirim_internal_date < '".format_save_date($v_date_from)."' 
					AND ".$db["master"].".kirim_internal.status_kirim = 'Disetujui'
					".$where_gudang_id_to." 
		";
		$qry_kirim_internal_in_prev = mysql_query($q);
		$row_kirim_internal_in_prev = mysql_fetch_array($qry_kirim_internal_in_prev);
		
		$q= "
				SELECT
					SUM(".$db["master"].".kirim_internal_details.qty) as qty
				FROM
					".$db["master"].".kirim_internal_details
					INNER JOIN ".$db["master"].".kirim_internal ON
						".$db["master"].".kirim_internal_details.kirim_internal_no = ".$db["master"].".kirim_internal.kirim_internal_no
				WHERE
					".$db["master"].".kirim_internal_details.barang_id = '".$barang_id."'
					AND ".$db["master"].".kirim_internal.kirim_internal_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
					AND ".$db["master"].".kirim_internal.status_kirim = 'Disetujui'
					".$where_gudang_id_to." 
		";
		$qry_kirim_internal_in = mysql_query($q);
		$row_kirim_internal_in = mysql_fetch_array($qry_kirim_internal_in);
	}
    ## IN STOCK
    
    ## OUT STOCK
	if($gudang_id!="Semua")
	{
		//kirim_internal OUT
		$q= "
				SELECT
					SUM(".$db["master"].".kirim_internal_details.qty) as qty
				FROM
					".$db["master"].".kirim_internal_details
					INNER JOIN ".$db["master"].".kirim_internal ON
						".$db["master"].".kirim_internal_details.kirim_internal_no = ".$db["master"].".kirim_internal.kirim_internal_no
				WHERE
					".$db["master"].".kirim_internal_details.barang_id = '".$barang_id."'
					AND ".$db["master"].".kirim_internal.kirim_internal_date < '".format_save_date($v_date_from)."' 
					AND ".$db["master"].".kirim_internal.status_kirim = 'Disetujui'
					".$where_gudang_id_from." 
		";
		$qry_kirim_internal_out_prev = mysql_query($q);
		$row_kirim_internal_out_prev = mysql_fetch_array($qry_kirim_internal_out_prev);
		
		$q= "
				SELECT
					SUM(".$db["master"].".kirim_internal_details.qty) as qty
				FROM
					".$db["master"].".kirim_internal_details
					INNER JOIN ".$db["master"].".kirim_internal ON
						".$db["master"].".kirim_internal_details.kirim_internal_no = ".$db["master"].".kirim_internal.kirim_internal_no
				WHERE
					".$db["master"].".kirim_internal_details.barang_id = '".$barang_id."'
					AND ".$db["master"].".kirim_internal.kirim_internal_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
					AND ".$db["master"].".kirim_internal.status_kirim = 'Disetujui'
					".$where_gudang_id_from." 
		";
		$qry_kirim_internal_out = mysql_query($q);
		$row_kirim_internal_out = mysql_fetch_array($qry_kirim_internal_out);
	}	
	// kirim eksternal
	$q= "
            SELECT
                SUM(".$db["master"].".kirim_eksternal_details.qty) as qty
            FROM
                ".$db["master"].".kirim_eksternal_details
                INNER JOIN ".$db["master"].".kirim_eksternal ON
                    ".$db["master"].".kirim_eksternal_details.kirim_eksternal_no = ".$db["master"].".kirim_eksternal.kirim_eksternal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".kirim_eksternal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".kirim_eksternal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".kirim_eksternal.kirim_eksternal_date < '".format_save_date($v_date_from)."'
				AND ".$db["master"].".kirim_eksternal.status_kirim = 'Disetujui'
				".$where_gudang_id."
    ";
    $qry_kirim_eksternal_prev = mysql_query($q);
    $row_kirim_eksternal_prev = mysql_fetch_array($qry_kirim_eksternal_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".kirim_eksternal_details.qty) as qty
            FROM
                ".$db["master"].".kirim_eksternal_details
                INNER JOIN ".$db["master"].".kirim_eksternal ON
                    ".$db["master"].".kirim_eksternal_details.kirim_eksternal_no = ".$db["master"].".kirim_eksternal.kirim_eksternal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".kirim_eksternal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".kirim_eksternal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".kirim_eksternal.kirim_eksternal_date BETWEEN '".format_save_date($v_date_from)."' AND  '".format_save_date($v_date_to)."'
				AND ".$db["master"].".kirim_eksternal.status_kirim = 'Disetujui'
				".$where_gudang_id."
    ";
    $qry_kirim_eksternal = mysql_query($q);
    $row_kirim_eksternal = mysql_fetch_array($qry_kirim_eksternal);
	
	
	// pengeluaran_barang_internal
	$q= "
            SELECT
                SUM(".$db["master"].".pengeluaran_barang_internal_details.qty) as qty
            FROM
                ".$db["master"].".pengeluaran_barang_internal_details
                INNER JOIN ".$db["master"].".pengeluaran_barang_internal ON
                    ".$db["master"].".pengeluaran_barang_internal_details.pengeluaran_barang_internal_no = ".$db["master"].".pengeluaran_barang_internal.pengeluaran_barang_internal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pengeluaran_barang_internal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pengeluaran_barang_internal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pengeluaran_barang_internal.pengeluaran_barang_internal_date < '".format_save_date($v_date_from)."'
				AND ".$db["master"].".pengeluaran_barang_internal.status_pengeluaran = 'Disetujui'
				".$where_gudang_id."
				
    ";
    $qry_pengeluaran_barang_internal_prev = mysql_query($q);
    $row_pengeluaran_barang_internal_prev = mysql_fetch_array($qry_pengeluaran_barang_internal_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".pengeluaran_barang_internal_details.qty) as qty
            FROM
                ".$db["master"].".pengeluaran_barang_internal_details
                INNER JOIN ".$db["master"].".pengeluaran_barang_internal ON
                    ".$db["master"].".pengeluaran_barang_internal_details.pengeluaran_barang_internal_no = ".$db["master"].".pengeluaran_barang_internal.pengeluaran_barang_internal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pengeluaran_barang_internal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pengeluaran_barang_internal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pengeluaran_barang_internal.pengeluaran_barang_internal_date BETWEEN '".format_save_date($v_date_from)."' AND  '".format_save_date($v_date_to)."'
				AND ".$db["master"].".pengeluaran_barang_internal.status_pengeluaran = 'Disetujui'
				".$where_gudang_id."
    ";
    $qry_pengeluaran_barang_internal = mysql_query($q);
    $row_pengeluaran_barang_internal = mysql_fetch_array($qry_pengeluaran_barang_internal);
	
	// pengeluaran_barang_eksternal
	$q= "
            SELECT
                SUM(".$db["master"].".pengeluaran_barang_eksternal_details.qty) as qty
            FROM
                ".$db["master"].".pengeluaran_barang_eksternal_details
                INNER JOIN ".$db["master"].".pengeluaran_barang_eksternal ON
                    ".$db["master"].".pengeluaran_barang_eksternal_details.pengeluaran_barang_eksternal_no = ".$db["master"].".pengeluaran_barang_eksternal.pengeluaran_barang_eksternal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pengeluaran_barang_eksternal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pengeluaran_barang_eksternal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pengeluaran_barang_eksternal.pengeluaran_barang_eksternal_date < '".format_save_date($v_date_from)."'
				AND ".$db["master"].".pengeluaran_barang_eksternal.status_pengeluaran = 'Disetujui'
				".$where_gudang_id."
				
    ";
    $qry_pengeluaran_barang_eksternal_prev = mysql_query($q);
    $row_pengeluaran_barang_eksternal_prev = mysql_fetch_array($qry_pengeluaran_barang_eksternal_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".pengeluaran_barang_eksternal_details.qty) as qty
            FROM
                ".$db["master"].".pengeluaran_barang_eksternal_details
                INNER JOIN ".$db["master"].".pengeluaran_barang_eksternal ON
                    ".$db["master"].".pengeluaran_barang_eksternal_details.pengeluaran_barang_eksternal_no = ".$db["master"].".pengeluaran_barang_eksternal.pengeluaran_barang_eksternal_no
				INNER JOIN ".$db["master"].".gudang ON
					".$db["master"].".pengeluaran_barang_eksternal.gudang_id = ".$db["master"].".gudang.gudang_id
            WHERE
                ".$db["master"].".pengeluaran_barang_eksternal_details.barang_id = '".$barang_id."'
                AND ".$db["master"].".pengeluaran_barang_eksternal.pengeluaran_barang_eksternal_date BETWEEN '".format_save_date($v_date_from)."' AND  '".format_save_date($v_date_to)."'
				AND ".$db["master"].".pengeluaran_barang_eksternal.status_pengeluaran = 'Disetujui'
				".$where_gudang_id."
    ";
    $qry_pengeluaran_barang_eksternal = mysql_query($q);
    $row_pengeluaran_barang_eksternal = mysql_fetch_array($qry_pengeluaran_barang_eksternal);
	
	## OUT STOCK
    
    $begining = ( 
					$row_pembelian_prev["qty"] + 
					$row_kirim_internal_in_prev["qty"] + 
					$row_adjustment_prev["qty"] 
				) 
				- 
				( 
					$row_kirim_internal_out_prev["qty"] + 
					$row_kirim_eksternal_prev["qty"] + 
					$row_pengeluaran_barang_internal_prev["qty"] + 
					$row_pengeluaran_barang_eksternal_prev["qty"] 
				);
				
    $ending   =  ( 
					$row_pembelian_prev["qty"] + $row_pembelian["qty"] + 
					$row_kirim_internal_in_prev["qty"] + $row_kirim_internal_in["qty"] +
					$row_adjustment_prev["qty"] + $row_adjustment["qty"] 
				) 
				- 
				( 
					$row_kirim_internal_out_prev["qty"] + $row_kirim_internal_out["qty"] + 
					$row_kirim_eksternal_prev["qty"] + $row_kirim_eksternal["qty"] + 
					$row_pengeluaran_barang_internal_prev["qty"] + $row_pengeluaran_barang_internal["qty"] + 
					$row_pengeluaran_barang_eksternal_prev["qty"] + $row_pengeluaran_barang_eksternal["qty"] 
				);
	
	$masuk = (
				$row_pembelian["qty"] + 
				$row_kirim_internal_in["qty"] +
				$row_adjustment["qty"]
			);
	
	$keluar = (
				$row_kirim_internal_out["qty"] + 
				$row_kirim_eksternal["qty"] + 
				$row_pengeluaran_barang_internal["qty"] + 
				$row_pengeluaran_barang_eksternal["qty"] 
			);
	
			
	
	$arr_data["awal"] = $begining;
	$arr_data["masuk"] = $masuk;
	$arr_data["keluar"] = $keluar;
	$arr_data["akhir"] = $ending;
	
	return $arr_data;
}

function call_log($username,$modul,$action,$no_reff)
{
	global $db;
	
	$q = "
			INSERT INTO
				".$db["master"].".log
			SET
				username = '".$username."',
				modul = '".$modul."',
				action = '".$action."',
				no_reff = '".$no_reff."',
				post_date = '".date_now("Y-m-d H:i:s")."'
	";
	
	$msg = "";
	if(!mysql_query($q))
	{
		$msg = "Failed Insert Log";
	}
	
	
	return $msg;
}

function get_mmyyyy($date)
{
	$exp_date = explode("-",$date);
	
	$return   = $exp_date[1]."-".$exp_date[0];
	return $return;
}

function status_day($v_mmyyyy_from,$v_mmyyyy_to)
{
	$exp_from = explode("-",$v_mmyyyy_from);
	$mm_from  = $exp_from[0];
	$yyyy_from  = $exp_from[1];
	
	$exp_to = explode("-",$v_mmyyyy_to);
	$mm_to  = $exp_to[0];
	$yyyy_to  = $exp_to[1];
	
	$mktime_from = mktime(0,0,0,$mm_from,1,$yyyy_from);
	$mktime_to   = mktime(0,0,0,$mm_to,1,$yyyy_to);
	
	unset($arr_data);
	
	$arr_data["from"] = $yyyy_from."-".$mm_from."-01";
	$arr_data["to"] = $yyyy_to."-".$mm_to."-".date('t',$mktime_to);
	
	return $arr_data;
}

function loop_mmyyyy($v_mmyyyy_from,$v_mmyyyy_to)
{
	$exp_from = explode("-",$v_mmyyyy_from);
	$mm_from  = $exp_from[0];
	$yyyy_from  = $exp_from[1];
	
	$exp_to = explode("-",$v_mmyyyy_to);
	$mm_to  = $exp_to[0];
	$yyyy_to  = $exp_to[1];
	
	$mktime_from = mktime(0,0,0,$mm_from,1,$yyyy_from);
	$mktime_to   = mktime(0,0,0,$mm_to,1,$yyyy_to);
	$i_mm   = $mm_from;
	$i_yyyy = $yyyy_from;
	
	$i = $mktime_from;
	unset($arr_data);
	while($i<$mktime_to)
	{
		$i = mktime(0,0,0,$i_mm,1,$i_yyyy);
		
		$i_mmyyyy = date('m',$i)."-".date('Y',$i);
		
		$arr_data[$i_mmyyyy] = $i_mmyyyy;
		$i_mm++;
		
	}
	
	return $arr_data;
}


function yesterday($v_date)
{
	$return = date('d/m/Y',parsedate($v_date)-86400);	

	return $return;
}

function get_saldo_awal_kas($v_kas_date_from, $v_kas_date_to, $v_kas_type)
{
	global $db;
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".kas
			WHERE
				1
				AND kas_type = '".$v_kas_type."'
				AND kas_date < '".format_save_date($v_kas_date_from)."'
				AND operator = 'Plus'
			ORDER BY
				kas_date ASC
	";
	$qry_plus = mysql_query($q);
	$row_plus = mysql_fetch_array($qry_plus);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".kas
			WHERE
				1
				AND kas_type = '".$v_kas_type."'
				AND kas_date < '".format_save_date($v_kas_date_from)."'
				AND operator = 'Minus'
			ORDER BY
				kas_date ASC
	";
	$qry_minus = mysql_query($q);
	$row_minus = mysql_fetch_array($qry_minus);
	
	$return = $row_plus["amount"] - $row_minus["amount"];
	
	return $return;
}

function get_debit($v_account_no,$v_date_from,$v_date_to)
{
	global $db;
	$saldo_awal = get_saldo_awal($v_date_from, $v_date_to, $v_account_no);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_to)."'
				AND acc_post = 'Debit'
	";
	$qry_plus = mysql_query($q);
	$row_plus = mysql_fetch_array($qry_plus);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_to)."'
				AND acc_post = 'Credit'
	";
	$qry_minus = mysql_query($q);
	$row_minus = mysql_fetch_array($qry_minus);
	
	$return = ($saldo_awal + $row_plus["amount"]) - $row_minus["amount"];
	
	return $return;
}

function get_debit_is($v_account_no,$v_date_from,$v_date_to)
{
	global $db;
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
				AND acc_post = 'Debit'
	";
	$qry_plus = mysql_query($q);
	$row_plus = mysql_fetch_array($qry_plus);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
				AND acc_post = 'Credit'
	";
	$qry_minus = mysql_query($q);
	$row_minus = mysql_fetch_array($qry_minus);
	
	$return = $row_plus["amount"] - $row_minus["amount"];
	
	return $return;
}

function get_credit($v_account_no,$v_date_from,$v_date_to)
{
	global $db;
	$saldo_awal = get_saldo_awal($v_date_from, $v_date_to, $v_account_no);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_to)."'
				AND acc_post = 'Debit'
	";
	$qry_plus = mysql_query($q);
	$row_plus = mysql_fetch_array($qry_plus);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_to)."'
				AND acc_post = 'Credit'
	";
	$qry_minus = mysql_query($q);
	$row_minus = mysql_fetch_array($qry_minus);
	
	$return = ($saldo_awal + $row_plus["amount"]) - $row_minus["amount"];
	
	$return = $return * (-1);
	return $return;
}

function get_saldo_awal($v_date_from, $v_date_to, $v_account_no)
{
	global $db;
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_from)."'
				AND acc_post = 'Debit'
	";
	$qry_plus = mysql_query($q);
	$row_plus = mysql_fetch_array($qry_plus);
	
	$q = "
			SELECT
				SUM(amount) as amount
			FROM
				".$db["master"].".journal
			WHERE
				1
				AND account_no = '".$v_account_no."'
				AND journal_date < '".format_save_date($v_date_from)."'
				AND acc_post = 'Credit'
	";
	$qry_minus = mysql_query($q);
	$row_minus = mysql_fetch_array($qry_minus);
	
	$return = $row_plus["amount"] - $row_minus["amount"];
	
	return $return;
}

function get_lock_acc($date)
{
    global $db;
    
    //format_date dd/mm/yyyy
    $exp_date = explode("/",$date);
    $mmyyyy = $exp_date[1]."-".$exp_date[2];
    
    $q = "
            SELECT
                lock_acc_name
            FROM
                ".$db["master"].".lock_acc
            WHERE
                1
                AND lock_acc_name = '".$mmyyyy."'
            LIMIT
                0,1 
    ";
    $qry = mysql_query($q);
    
    if(!mysql_num_rows($qry))
    {
        return false;
    }
    else
    {
        return true;
    }
}

function format_save_date($date)
{
    if($date!="")
    {
        $exp_date = explode("/",$date);
        $return = $exp_date[2]."-".$exp_date[1]."-".$exp_date[0];
    }
    else
    {
        $return = "";    
    }
    return $return;
}

function get_approval()
{
	global $db;
	
	$q = "
			SELECT
				employee_name
			FROM
				".$db["master"].".employee
			WHERE
				employee_type = 'Director'
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["employee_name"];
}

function get_created()
{
	global $db;
	
	$q = "
			SELECT
				employee_name
			FROM
				".$db["master"].".employee
			WHERE
				1
				AND employee_type LIKE '%Accounting%'
				AND resign_date = '0000-00-00'
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["employee_name"];
}



function file_img($ext)
{
    if($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif")
    {
        $return = '<img src="images/photo.png" />';
    }
    else if($ext=="zip")
    {
        $return = '<img src="images/zip.png" />';
    }
    else if($ext=="rar")
    {
        $return = '<img src="images/rar.png" />';
    }
    else if($ext=="doc" || $ext=="docx")
    {
        $return = '<img src="images/doc.png" />';
    }
    else if($ext=="xls" || $ext=="xlsx")
    {
        $return = '<img src="images/xls.png" />';
    }
    else if($ext=="ppt" || $ext=="pptx")
    {
        $return = '<img src="images/ppt.png" />';
    }
    else if($ext=="ogg" || $ext=="mp3")
    {
        $return = '<img src="images/music.png" />';
    }
    else if($ext=="pdf")
    {
        $return = '<img src="images/pdf.png" />';
    }
    else
    {
        $return = '<img src="images/file.png" />';
    }
    
    return $return;
    
}

function format_show_datetime($date)
{
	if($date)
	{
    	$exp_date = explode(" ",$date);
    	$return = format_show_date($exp_date[0])." ".$exp_date[1];
	}
	else
	{
		$return = "";
	}
    
    return $return;
}

function tanggal_01($v_stok_from,$v_stok_to)
{
	$from = parsedate($v_stok_from);
	$to = parsedate($v_stok_to);
	
	$status = false;
	for($i=$from;$i<=$to;$i=$i+86400)
	{
		if(date('d',$i)=="01")
		{
			$status = true;
			break;
		}
	}
	
	return $status;
}

    
function parsedate($str) 
{
    $return = "";
    
    if($str!="")
    {
        $exp_str = explode("/",$str);
    
        $return = mktime(0,0,0,$exp_str[1],$exp_str[0],$exp_str[2]);
        return $return;
    }
    
    return $return;
}   

function parsedate_time($str) 
{
    $return = "";
    
    if($str!="")
    {
        $exp_awal  = explode(" ",$str);
        
        $exp_tgl   = explode("/",$exp_awal[0]);
        $exp_time  = explode(":",$exp_awal[1]);
    
        $return = mktime($exp_time[0],$exp_time[1],$exp_time[2],$exp_str[1],$exp_str[0],$exp_str[2]);
        
        return $return;
    }
    
    return $return;
}
   
function get_counter_int($db_name,$tbl_name,$pk,$limit=100)
{
  $q = "
            SELECT
                ".$db_name.".".$tbl_name.".".$pk."
            FROM
                ".$db_name.".".$tbl_name."
            ORDER BY
                ".$db_name.".".$tbl_name.".".$pk."*1 DESC
            LIMIT 0,".$limit."
    ";
    $qry_id = mysql_query($q);
    $jml_data = mysql_num_rows($qry_id);
    
    $no = true;
    while($r_id = mysql_fetch_object($qry_id))
    {
        if($no)
        {
            $id = $r_id->$pk;
            $no = false;
        }
        
        $arr_data["list_id"][$r_id->$pk] = $r_id->$pk;
    }
    
    $prev_id = 0;
    
    if($jml_data!=0)
    {
        foreach($arr_data["list_id"] as $list_id=>$val)
        {
            if($prev_id*1!=0)
            {
                if( ($prev_id-1) != $val)
                {
                    $id = $val;
                    break;
                }
            }
            $prev_id = $val;
        }
    }
    else
    {
        $id = 0;
    }
    $id = ($id*1) + 1;
    
    return $id;
} 

function get_counter_pk($db_name,$tbl_name,$pk,$kd_pk)
{
   $q = "
            SELECT
                SUBSTR(".$db_name.".".$tbl_name.".".$pk.",3,5)*1 AS counter
            FROM
                ".$db_name.".".$tbl_name."
            ORDER BY
                counter DESC
            LIMIT
                0,1
    ";
    
    $qry_id = mysql_query($q);
    $row_id = mysql_fetch_array($qry_id);
    $counter = $row_id["counter"]+1;
    
    //echo $counter;
    //die();
    
    $return = $kd_pk.sprintf("%05s",$counter);
    
    return $return;
} 


function get_counter_tr($db_name, $table_name, $col_primary , $yyyy, $mm, $kode="KD"){
    //$table_name = cibadak.tablename
    //$col_primary = colomn primary key
    //$tanggal = parsedate('01-02-2010');
    
    $prefix = $kode."-".$yyyy."-".$mm."-";
    $query = "
            SELECT 
                MAX(REPLACE(".$col_primary.", '".$prefix."', '') * 1) AS counter
            FROM
                ".$db_name.".".$table_name."
            WHERE 
                ".$col_primary." LIKE '".$prefix."%'
    ";
    $qry = mysql_query($query);
    $row = mysql_fetch_array($qry);
    
    $counter = $row["counter"];
    
    $counter++;
    
    //$show_counter =  sprintf("%03s",$counter);
    $show_counter = $counter;
    
    $receipt_id = $prefix.$show_counter;
    return $receipt_id;
    
}


function get_counter_fixed_asset($db_name, $table_name, $col_primary , $prefix){
    //$table_name = cibadak.tablename
    //$col_primary = colomn primary key
    //$tanggal = parsedate('01-02-2010');
    
    $query = "
            SELECT 
                MAX(REPLACE(".$col_primary.", '".$prefix."', '') * 1) AS counter
            FROM
                ".$db_name.".".$table_name."
            WHERE 
                ".$col_primary." LIKE '".$prefix."%'
    ";
    $qry = mysql_query($query);
    $row = mysql_fetch_array($qry);
    
    $counter = $row["counter"];
    
    $counter++;
    
    $show_counter =  sprintf("%03s",$counter);
    //$show_counter = $counter;
    
    $receipt_id = $prefix.$show_counter;
    return $receipt_id;
    
}

function get_counter_tr_herly($db_name, $table_name, $col_primary , $prefix, $digit, $sufix){
    //$table_name = cibadak.tablename
    //$col_primary = colomn primary key
    //$tanggal = parsedate('01-02-2010');
    
    //$prefix = $kode."-".$yyyy."-".$mm."-";
    $query = "
            SELECT 
                MAX(REPLACE(".$col_primary.", '".$prefix."', '') * 1) AS counter
            FROM
                ".$db_name.".".$table_name."
            WHERE 
                ".$col_primary." LIKE '".$prefix."%'
    ";
    $qry = mysql_query($query);
    $row = mysql_fetch_array($qry);
    
    $counter = $row["counter"];
    
    $counter++;
    
    $show_counter =  sprintf("%0".$digit."s",$counter);
    //$show_counter = $counter;
    
    $receipt_id = $prefix.$show_counter.$sufix;
    return $receipt_id;
    
}
    
	
function get_counter_faktur_pajak_sales($db_name, $table_name, $col_primary , $yyyy){
    //$table_name = cibadak.tablename
    //$col_primary = colomn primary key
    //$tanggal = parsedate('01-02-2010');
    //010.000-11.00000049
	$yyyy = substr($yyyy,2,2);
	
    $prefix = "010.000-".$yyyy.".";;
    $query = "
            SELECT 
                MAX(REPLACE(".$col_primary.", '".$prefix."', '') * 1) AS counter
            FROM
                ".$db_name.".".$table_name."
            WHERE 
                ".$col_primary." LIKE '".$prefix."%'
    ";
    $qry = mysql_query($query);
    $row = mysql_fetch_array($qry);
    
    $counter = $row["counter"];
    
    $counter++;
    
    //$show_counter =  sprintf("%03s",$counter);
    $show_counter = sprintf("%08d",$counter);
    
    $receipt_id = $prefix.$show_counter;
    return $receipt_id;
    
}


function combo($query, $name, $value, $column, $title='', $selected='', $addition='', $validasi='') 
{
    $judul = "";
    $data = "";
    $rs = mysql_query($query) or die("Error Creating Combo!<br>$query<br>".mysql_error());
    
    if ($title != "") {
        $data .= "<option value=''>".$title."</option>";    
    }
    
    
    while ($r = mysql_fetch_object($rs)) 
    {
        
        if (is_array($column)) 
        {
            $judul = "";
            foreach($column as $column_id=>$val)
            {
                $judul .= $r->$val." :: ";
            }    
            $judul = substr($judul,0,-4);
        }
        else
        {
            $judul = $r->$column;    
        }
        
        $lock_selected = "";
        if($r->$value==$selected)
        {
            $lock_selected = "selected='selected'";
        }
        
        $data .= "<option ".$lock_selected." value='".$r->$value."'>".$judul."</option>";
    }
    $hasil = "<select name='$name' class='text_area $validasi' $addition>$data</select>";
    return $hasil;

}

            
function alertnfocus($id,$msg){
    echo "<script>alert(\"".$msg."\");parent.document.getElementById('".$id."').focus();</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe error di BETUL kan!
}

function alertnback($msg){
    echo "<script>alert(\"".$msg."\");history.go(-1);</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe error di BETUL kan!
}

function alertnclose($msg){
    echo "<script>alert(\"".$msg."\");window.close();</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe error di BETUL kan!
}

function alertRedirect($msg,$url){
    echo "<script>alert(\"".$msg."\");window.location = '".$url."';</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe!
}

function simpleAlert($msg){
    echo "<script>alert(\"".$msg."\");</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe error di BETUL kan!
}

function simpleRedirect($url){
    echo "<script>window.location = '".$url."';</script>";
    exit();//kalau masuk ke function ini berarti proses untuk selanjutnya akan di stop sampe error di BETUL kan!
}

function confirmInsert($msg){
    echo "<script>
    var answer = confirm('".$msg."');
    if (!answer){
        alert('Tambah data dibatalkan!')
        window.close();
    }
    </script>";
}

function h2error($msg){
    echo "<h2><font color=red>".$msg."</font></h2>";
    exit();
}

function ga_span_red($message){
    $str = "<span style='color:#F00'>".$message."</span>";
    return $str;
}

function ga_span_blue($message){
    $str = "<span style='color:#3366FF'>".$message."</span>";
    return $str;
}


function save_date($date)
{
    $return = $date + diff_time();
    
    return $return;
}

function replace($string,$karakter,$replace)
{
    $return = preg_replace("/".$karakter."/", $replace, $string);
    
    return $return;
}

function save_char($char)
{
    $char1 = '"';
    $return = trim(mysql_real_escape_string($char));
    
    return $return;
}

function save_pop($char)
{
    $char1 = '"';
    $return = trim($char);
    $return2 = str_replace('"','',$return);
    
    return $return2;
}

function save_int($int)
{
    $first  = trim(str_replace("`", "", $int));
    $second = str_replace(",", "", $first);
    
    return $second;
}

function format_number($nilai,$decimal=0,$point=".",$thousands=",")
{
	if($nilai*1!=0)
	{
    	$return = number_format($nilai, $decimal, $point, $thousands);
	}
	else
	{
		$return = "&nbsp;";
	}
    
    return $return;
}

function format_date($format="d-m-Y")
{
    return $format;
}

function format_bulan($date)
{
	$exp_date = explode("-",$date);
	
	if($exp_date[1]=="01")
	{
		$mm = "Januari";
	}
	else if($exp_date[1]=="02")
	{
		$mm = "Februari";
	}
	else if($exp_date[1]=="03")
	{
		$mm = "Maret";
	}
	else if($exp_date[1]=="04")
	{
		$mm = "April";
	}
	else if($exp_date[1]=="05")
	{
		$mm = "Mei";
	}
	else if($exp_date[1]=="06")
	{
		$mm = "Juni";
	}
	else if($exp_date[1]=="07")
	{
		$mm = "Juli";
	}
	else if($exp_date[1]=="08")
	{
		$mm = "Agustus";
	}
	else if($exp_date[1]=="09")
	{
		$mm = "September";
	}
	else if($exp_date[1]=="10")
	{
		$mm = "Oktober";
	}
	else if($exp_date[1]=="11")
	{
		$mm = "November";
	}
	else if($exp_date[1]=="12")
	{
		$mm = "Desember";
	}
	
	return $exp_date[2]." ".$mm." ".$exp_date[0];
}

	function format_show_date2($date){
		$return = "";
		if($date){
			$exp_date = explode("-",$date);
			$return = $exp_date[2]."/".$exp_date[1]."/".$exp_date[0];
		}
		
		return $return;
	}

	function format_show_date($mysqldate){
		$return = "";
		if($mysqldate){
			$datepiece = explode("-", $mysqldate);
			if($datepiece[1]=='01'){
				$namabulan = 'Jan';
			}elseif($datepiece[1]=='02'){
				$namabulan = 'Feb';
			}elseif($datepiece[1]=='03'){
				$namabulan = 'Mar';
			}elseif($datepiece[1]=='04'){
				$namabulan = 'Apr';
			}elseif($datepiece[1]=='05'){
				$namabulan = 'Mei';
			}elseif($datepiece[1]=='06'){
				$namabulan = 'Jun';
			}elseif($datepiece[1]=='07'){
				$namabulan = 'Jul';
			}elseif($datepiece[1]=='08'){
				$namabulan = 'Ags';
			}elseif($datepiece[1]=='09'){
				$namabulan = 'Sep';
			}elseif($datepiece[1]=='10'){
				$namabulan = 'Okt';
			}elseif($datepiece[1]=='11'){
				$namabulan = 'Nov';
			}elseif($datepiece[1]=='12'){
				$namabulan = 'Des';
			}
			$return	= $datepiece[2]." ".$namabulan." ".$datepiece[0];
		}
		return $return;
	}    
    
?>