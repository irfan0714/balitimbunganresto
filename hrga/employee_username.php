<?php 
    include("header.php");      
	         
    if(!isset($_GET["search_status"])){ $search_status = isset($_GET["search_status"]); } else { $search_status = $_GET["search_status"]; }    
    if(!isset($_GET["search_company_id"])){ $search_company_id = isset($_GET["search_company_id"]); } else { $search_company_id = $_GET["search_company_id"]; }
    if(!isset($_GET["search_cabang_id"])){ $search_cabang_id = isset($_GET["search_cabang_id"]); } else { $search_cabang_id = $_GET["search_cabang_id"]; }
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    $v_keyword = save_char($v_keyword);
    
    $link_adjust    = "";
    $link_adjust   .= "?v_keyword=".$v_keyword;
    $link_adjust   .= "&p=".$p;
    
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $icon_type_change = "entypo-up-dir";
    $order_type_change = "asc";
    if($order_type=="asc")
    {
        $order_type_change = "desc";
    	$icon_type_change = "entypo-down-dir";
    }
    
    $order_by_content = "";
    if($order_by!="")
    {
        $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
    }
    
	$modul = "Generate Username";
    $list  = "employee_username.php";
    $htm   = "employee_username_form.php";
    $pk    = "employee_id";
    $title = "employee_name";
    
   	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    
    function generate_username_new($v_employee_name, $v_birthday)
	{
	    global $db;
	    
	    $exp_employee_name  = explode(" ", $v_employee_name);
	    $jml_spasi          = count($exp_employee_name);
	    
	    $exp_birthday       = explode("/", $v_birthday);
	    $hari               = $exp_birthday[0];
	    $bulan              = $exp_birthday[1];
	    $tahun              = $exp_birthday[2]; 
	    
	    $arr_data[1] = $exp_employee_name[0].$hari.$bulan;
	    
	    if($jml_spasi>=2)
	    {
	        $arr_data[2] = $exp_employee_name[1].$hari.$bulan;    
	    }
	    
	    if($jml_spasi>=2)
	    {
	        $arr_data[3] = "";
	        for($i=1;$i<=$jml_spasi;$i++)
	        {
	            $curr = $i-1;
	            $arr_data[3] .= substr($exp_employee_name[$curr],0,1);    
	        }
	        $arr_data[3] .= $hari.$bulan;
	    }
	    
	    $jml_arr_data = count($arr_data);
	    
	    // cek ke database
	    $j = 1;
	    for($i=1;$i<=$jml_arr_data;$i++)
	    {
	        $username = strtolower($arr_data[$i]);
	        
	        $q = "
	            SELECT
	                username
	            FROM
	                ".$db["master"].".employee
	            WHERE
	                ".$db["master"].".employee.username = '".$username."'
	            LIMIT
	                0,1
	        ";
	        $qry = mysql_query($q);
	        $ada = mysql_num_rows($qry);
	        
	        if(!$ada)
	        {
	           $arr_username[$j] = $username; 
	           $j++; 
	        }
	    }
	    
	    return $arr_username;
	}

	if($ajax)
	{
        if($ajax=="generate_username")
        {
            $v_employee_id = save_char($_GET["v_employee_id"]);
            $v_employee_name = save_char($_GET["v_employee_name"]);
            $v_birthday      = save_char($_GET["v_birthday"]);
            
            echo "masuk";
            
            //$generate_username = generate_username_new($v_employee_name, $v_birthday);
            
            echo $generate_username[1];
        }
		exit();
	}
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
		
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
        <form method="get">
		<div class="row">
			<div class="col-md-10">
			
                &nbsp;
				Search&nbsp;
				<input type="text" size="30" maxlength="30" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>">
				&nbsp;
			</div>
			
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="10"><center>No</th>    
						<th width="10"><center>Employee ID</th>   
                        <th width="100"><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_nik&order_type=".$order_type_change; ?>" class="link_menu">No. Induk Karyawan<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th width="80"><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_code_hrd&order_type=".$order_type_change; ?>" class="link_menu">Kode HRD<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_name&order_type=".$order_type_change; ?>" class="link_menu">Nama<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.email&order_type=".$order_type_change; ?>" class="link_menu">Email<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.birthday&order_type=".$order_type_change; ?>" class="link_menu">Tanggal  Lahir<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.username&order_type=".$order_type_change; ?>" class="link_menu">Username<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center>Navigasi</th>
					</tr>
				</thead>
				<tbody>
					
					<?php    
                        $keyWord = trim($v_keyword);
                        
                        if($keyWord == '')
                        {    
                            $sql = "
                                    SELECT 
                                        employee.employee_id,
                                        employee.employee_nik,
                                        employee.employee_code_hrd,
                                        employee.employee_name,
                                        employee.email,
                                        employee.birthday,
                                        employee.username
                                    FROM 
                                        employee
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        employee.employee_name ASC,
                                        employee.employee_id ASC,
                                        employee.employee_nik ASC
                            ";                    
                            $query = mysql_query($sql);
                            $max = ceil(mysql_num_rows($query)/$jml_page);
                            $s = $jml_page * $p;
                            $sql = "               
                                        SELECT 
                                            employee.employee_id,
                                            employee.employee_nik,
                                            employee.employee_code_hrd,
                                            employee.employee_name,
                                        	employee.email,
	                                        employee.birthday,
	                                        employee.username
                                        FROM 
                                            employee
                                        WHERE
                                            1
                                        ORDER BY
                                            ".$order_by_content." 
                                            employee.employee_name ASC,
                                            employee.employee_id ASC,
                                            employee.employee_nik ASC
                                        LIMIT ".$s.", ".$jml_page." 
                                    ";         
                        }
                        else
                        {
                            unset($arr_keyword);
                            $arr_keyword[0] = "employee.employee_id";
							$arr_keyword[1] = "employee.employee_name";
							$arr_keyword[2] = "employee.employee_nik";
							$arr_keyword[3] = "employee.employee_code_hrd";
                            $arr_keyword[4] = "employee.email";
                            $arr_keyword[6] = "employee.username";
                            
							$search_keyword = search_keyword($v_keyword, $arr_keyword);
							$where = $search_keyword;
                               
                            $flag=0;
                            for($i=0; $i < strlen($keyWord); $i++)
                            {
                                if($keyWord[$i] == '\'') $flag++;
                                if($keyWord[$i] == '<') $flag++;
                                if($keyWord[$i] == '>') $flag++;
                            }
                            
                            if($flag==0)
                            {
                                $sql = "
                                            SELECT 
                                                employee.employee_id,
                                                employee.employee_nik,
                                                employee.employee_code_hrd,
                                                employee.employee_name,
	                                        	employee.email,
		                                        employee.birthday,
		                                        employee.username
                                            FROM 
                                                employee
                                            WHERE
                                                1
                                                ".$where."
                                                ".$where_status_emp."
                                                ".$where_posisi_emp."
                                                ".$where_useradmin_emp."
                                            ORDER BY
                                                ".$order_by_content." 
                                                employee.employee_name ASC,
                                                employee.employee_id ASC,
                                                employee.employee_nik ASC
                                       ";
                                $query = mysql_query($sql);
                                $max = ceil(mysql_num_rows($query)/$jml_page);
                                $s = $jml_page * $p;
                                $sql = "
                                            SELECT 
                                                employee.employee_id,
                                                employee.employee_nik,
                                                employee.employee_code_hrd,
                                                employee.employee_name,
	                                        	employee.email,
		                                        employee.birthday,
		                                        employee.username
                                            FROM 
                                                employee
                                            WHERE
                                                1
                                                ".$where."
                                            ORDER BY
                                                ".$order_by_content." 
                                                employee.employee_name ASC,
                                                employee.employee_id ASC,
                                                employee.employee_nik ASC
                                            LIMIT ".$s.", ".$jml_page." 
                                      ";
                            }
                            else
                            {
                                $msg = "Your input are not allowed!";
                            }
                        }
                        $query = mysql_query($sql);
                                $sv = 0;
                                $i=0+($jml_page*$p);
                        if(!$row = mysql_num_rows($query))
                        {
                         echo "<tr>";
                         echo "<td align=\"center\" colspan=\"100%\">";
                         echo "No Data";
                         echo "</td>";
                         echo "</tr>";
                        }
                        else
                        {
                            while ($row = mysql_fetch_array($query))
                            {
                                list($employee_id, $employee_nik, $employee_code_hrd, $employee_name, $email, $birthday, $username) = $row; 
							
                                $sv++;
                                $i++;
                                
								$bgcolor = "";
                                if($i%2==0)
                                {
                                    $bgcolor = "background:rgb(247, 247, 247) none repeat scroll 0% 0%;";
                                }
								
                     ?>
                            <tr title="<?php echo $row[$title]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
                                <td style="<?php echo $bgcolor_status; ?>" ><?php echo $i; ?></td>
                                <td align="center" style="<?php echo $bgcolor_status; ?>" ><?php echo $employee_id; ?></td>
                                <td align="center" style="<?php echo $bgcolor_status; ?>" ><?php echo $employee_nik; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" ><?php echo $employee_code_hrd; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" ><?php echo $employee_name; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" ><?php echo $email; ?></td>
                                <td align="center" style="<?php echo $bgcolor_status; ?>" ><?php echo format_show_date($birthday); ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" id="v_username_<?php echo $employee_id; ?>" ><?php echo $username; ?></td>
                                <td style="<?php echo $bgcolor_status; ?>" align="center">
                                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')">
										<i class="entypo-pencil"></i>
									</button>
								</td>
                            </tr>
                     <?php
					 			  
                            }   
                        }
                     ?>
                     
				</tbody>
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>
		
<?php include("footer.php"); ?>                                   
                                     