
<?
$config["paging"] = $_SERVER['PHP_SELF'];
$exp_paging = explode("/",$config["paging"]);
$config["paging"] = $exp_paging[count($exp_paging) - 1];

$pp      = $p + 1;
$current = $p + 1;
$prev    = ($pp - 1) - 1;
$next    = ($pp - 1) + 1;
$url     = $config["paging"].$link_adjust.$link_order_by;

$halaman = $max;

$mundur  = $halaman - 4;

if($max * 1 != 0){
	
	?>
	<div class="row">
		<div class="col-xs-6 col-left">
			<div id="table-2_info" class="dataTables_info">&nbsp;</div>
		</div>
		<div class="col-xs-6 col-right">
			<div class="dataTables_paginate paging_bootstrap">
	<?php

	if($max != 1)
	{
		if($halaman <= 6)
		{
			if($current == 1)
			{
				if($max == 1)
				{
					echo "<ul class='pagination pagination-sm'>";
					echo "<li class='prev disabled'>";
					echo "<a href='javascript:void(0);'>";
					echo "<i class='entypo-left-open'></i>";
					echo "</a>";
					echo "</li>";
					
					for($i = 1;$i <= $halaman;$i++){
						$asli = $i - 1;
						
						echo "<li class='active'>";
						echo "<a href='$url&p=$asli' title='$i'>";
						echo $i;
						echo "</a>";
						echo "</li>";
					}
					
					echo "<li class='next disabled'>";
					echo "<a href='javascript:void(0);'>";
					echo "<i class='entypo-right-open'></i>";
					echo "</a>";
					echo "</li>";
					echo "</ul>";
				}
				else
				{
					echo "<ul class='pagination pagination-sm'>";
					echo "<li class='prev disabled'>";
					echo "<a href='javascript:void(0);'>";
					echo "<i class='entypo-left-open'></i>";
					echo "</a>";
					echo "</li>";
					
					for($i = 1;$i <= $halaman;$i++){
						if($current == $i){
							$css_current = "active";
						}
						else
						{
							$css_current = "";
						}
						
						$asli = $i - 1;
						echo "<li class='$css_current'>";
						echo "<a href='$url&p=$asli' title='$i'>";
						echo $i;
						echo "</a>";
						echo "</li>";
					}
					
					echo "<li class='next'>";
					echo "<a href='$url&p=$next' title='Next'>";
					echo "<i class='entypo-right-open'></i>";
					echo "</a>";
					echo "</li>";
					echo "</ul>";
				}
			}
			else
			{
				if($current == $max)
				{
					echo "<ul class='pagination pagination-sm'>";
					echo "<li class='prev'>";
					echo "<a href='$url&p=$prev' title='Prev'>";
					echo "<i class='entypo-left-open'></i>";
					echo "</a>";
					echo "</li>";
					
					for($i = 1;$i <= $halaman;$i++){
						if($current == $i){
							$css_current = "active";
						}
						else
						{
							$css_current = "";
						}
						$asli = $i - 1;
						echo "<li class='$css_current'>";
						echo "<a href='$url&p=$asli' title='$i'>";
						echo $i;
						echo "</a>";
						echo "</li>";
					}
					
					echo "<li class='next disabled'>";
					echo "<a href='javascript:void(0);'>";
					echo "<i class='entypo-right-open'></i>";
					echo "</a>";
					echo "</li>";
					echo "</ul>";
				}
				else
				{
					echo "<ul class='pagination pagination-sm'>";
					echo "<li class='prev'>";
					echo "<a href='$url&p=$prev' title='Prev'>";
					echo "<i class='entypo-left-open'></i>";
					echo "</a>";
					echo "</li>";
					
					for($i = 1;$i <= $halaman;$i++){
						if($current == $i){
							$css_current = "active";
						}
						else
						{
							$css_current = "";
						}
						$asli = $i - 1;
						echo "<li class='$css_current'>";
						echo "<a href='$url&p=$asli' title='$i'>";
						echo $i;
						echo "</a>";
						echo "</li>";
					}
					
					echo "<li class='next'>";
					echo "<a href='$url&p=$next' title='Next'>";
					echo "<i class='entypo-right-open'></i>";
					echo "</a>";
					echo "</li>";
					echo "</ul>";
				}
			}
		}
		else
		{
			if($halaman >= 6){
				if($current <= 2){
					if($current == 1)
					{
						
						echo "<ul class='pagination pagination-sm'>";
						echo "<li class='prev disabled'>";
						echo "<a href='javascript:void(0);'>";
						echo "<i class='entypo-left-open'></i>";
						echo "</a>";
						echo "</li>";
						
						for($i = 1;$i <= 5;$i++){
							if($current == $i){
								$css_current = "active";
							}
							else
							{
								$css_current = "";
							}
							$asli = $i - 1;
							
							echo "<li class='$css_current'>";
							echo "<a href='$url&p=$asli' title='$i'>";
							echo $i;
							echo "</a>";
							echo "</li>";
						}

						$max_asli = $max - 1;
						
						echo "<li>";
						echo "<a href='javascript:void(0);'>";
						echo "<i class='entypo-dot-2'></i>";
						echo "</a>";
						echo "</li>";
						
						echo "<li>";
						echo "<a href='paging_text' href='$url&p=$max_asli' title='$max'>";
						echo $max;
						echo "</a>";
						echo "</li>";
						
						echo "<li class='next'>";
						echo "<a href='$url&p=$next' title='Next'>";
						echo "<i class='entypo-right-open'></i>";
						echo "</a>";
						echo "</li>";
						
						echo "</ul>";
					}
					else
					{
						echo "<ul class='pagination pagination-sm'>";
						echo "<li class='prev'>";
						echo "<a href='$url&p=$prev' title='Prev'>";
						echo "<i class='entypo-left-open'></i>";
						echo "</a>";
						echo "</li>";
						
						for($i = 1;$i <= 5;$i++){
							if($current == $i){
								$css_current = "active";
							}
							else
							{
								$css_current = "";
							}
							$asli = $i - 1;
							
							echo "<li class='$css_current'>";
							echo "<a href='$url&p=$asli' title='$i'>";
							echo $i;
							echo "</a>";
							echo "</li>";
						}
						
						echo "<li>";
						echo "<a href='javascript:void(0);'>";
						echo "<i class='entypo-dot-2'></i>";
						echo "</a>";
						echo "</li>";
						
						echo "<li>";
						echo "<a href='$url&p=$max' title='$max'>";
						echo $max;
						echo "</a>";
						echo "</li>";
						
						echo "<li class='next'>";
						echo "<a class='paging_text' href='$url&p=$next' title='Next'>";
						echo "<i class='entypo-right-open'></i>";
						echo "</a>";
						echo "</li>";
						
						echo "</ul>";
					}
				}
				else
				{
					if($current == $max || $current == $max - 1 || $current == $max - 2){
						if($current == $max)
						{
							echo "<ul class='pagination pagination-sm'>";
							
							echo "<li class='prev'>";
							echo "<a href='$url&p=$prev' title='Prev'>";
							echo "<i class='entypo-left-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=0' title='1'>";
							echo "1";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-dot-2'></i>";
							echo "</a>";
							echo "</li>";

							for($i = $mundur;$i <= $max;$i++)
							{
								if($current == $i)
								{
									$css_current = "active";
								}
								else
								{
									$css_current = "";
								}
								$asli = $i - 1;
							
								echo "<li class='$css_current'>";
								echo "<a href='$url&p=$asli' title='$i'>";
								echo $i;
								echo "</a>";
								echo "</li>";
							}

							echo "<li class='next disabled'>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-right-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "</ul>";
						}
						else
						{
							echo "<ul class='pagination pagination-sm'>";
							
							echo "<li class='prev'>";
							echo "<a href='$url&p=$prev' title='Prev'>";
							echo "<i class='entypo-left-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=0' title='1'>";
							echo "1";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-dot-2'></i>";
							echo "</a>";
							echo "</li>";
							
							for($i = $mundur;$i <= $max;$i++){
								if($current == $i){
									$css_current = "active";
								}
								else
								{
									$css_current = "";
								}
								$asli = $i - 1;
								
								echo "<li class='$css_current'>";
								echo "<a href='$url&p=$asli' title='$i'>";
								echo $i;
								echo "</a>";
								echo "</li>";
							}
							
							echo "<li class='next'>";
							echo "<a href='$url&p=$next' title='Next'>";
							echo "<i class='entypo-right-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "</ul>";
						}
					}
					else
					{
						if($current == 3)
						{
							echo "<ul class='pagination pagination-sm'>";
							
							echo "<li class='prev'>";
							echo "<a href='$url&p=$prev' title='Prev'>";
							echo "<i class='entypo-left-open'></i>";
							echo "</a>";
							echo "</li>";

							$one        = $current - 2;
							$one_asli   = $current - 3;

							$two        = $current - 1;
							$two_asli   = $current - 2;

							$currentnya = $current - 1;

							$three      = $current + 1;
							$three_asli = $current;

							$four       = $current + 2;
							$four_asli  = $current + 1;

							$max_asli   = $max - 1;
							
							echo "<li>";
							echo "<a href='$url&p=$one_asli' title='$one'>";
							echo $current - 2;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=$two_asli' title='$two'>";
							echo $current - 1;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=$currentnya' title='$current'>";
							echo $current;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=$three_asli' title='$three'>";
							echo $current + 1;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=$four_asli' title='$four'>";
							echo $current + 2;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-dot-2'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a href='$url&p=$max_asli' title='$max'>";
							echo $max;
							echo "</a>";
							echo "</li>";
						
							echo "<li class='next'>";
							echo "<a class='paging_text' href='$url&p=$next' title='Next'>";
							echo "<i class='entypo-right-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "</ul>";
						}
						else
						{
							echo "<ul class='pagination pagination-sm'>";
							echo "<li class='prev'>";
							echo "<a href='$url&p=$prev' title='Prev'>";
							echo "<i class='entypo-left-open'></i>";
							echo "</a>";
							echo "</li>";

							$one        = $current - 2;
							$one_asli   = $current - 3;

							$two        = $current - 1;
							$two_asli   = $current - 2;

							$currentnya = $current - 1;

							$three      = $current + 1;
							$three_asli = $current;

							$four       = $current + 2;
							$four_asli  = $current + 1;

							$max_asli   = $max - 1;

							echo "<li>";
							echo "<a href='$url&p=0' title='1'>";
							echo "1";
							echo "</a>";
							echo "</li>";
						
							echo "<li>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-dot-2'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=$one_asli' title='$one'>";
							echo $current - 2;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=$two_asli' title='$two'>";
							echo $current - 1;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text_current' href='$url&p=$currentnya' title='$current'>";
							echo $current;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=$three_asli' title='$three'>";
							echo $current + 1;
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=$four_asli' title='$four'>";
							echo $current + 2;
							echo "</a>";
							echo "</li>";
						
							echo "<li>";
							echo "<a href='javascript:void(0);'>";
							echo "<i class='entypo-dot-2'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "<li>";
							echo "<a class='paging_text' href='$url&p=$max_asli' title='$max'>";
							echo $max;
							echo "</a>";
							echo "</li>";
						
							echo "<li class='next'>";
							echo "<a class='paging_text' href='$url&p=$next' title='Next'>";
							echo "<i class='entypo-right-open'></i>";
							echo "</a>";
							echo "</li>";
							
							echo "</ul>";
						}
					}
				}
			}
		}
	}

	?>
			</div>
		</div>
	</div>	
	<?php
}
?>

