<div class="col-md-12">
<table class="table table-bordered responsive">
	<thead>
		<tr>
		    <th width="30"><center>No</center></th>
		    <th><center>Hubungan</center></th>
		    <th><center>Nama</center></th>               
		    <th><center>Tanggal Lahir</center></th>
		    <th><center>Jenis Kelamin</center></th>
		    <th><center>Simpan</center></th>
		    <th><center>Hapus</center></th>
		</tr>
	</thead>
	<tbody>
		
		<?php
	    $no = 1; 
	    for($i=1;$i<=1;$i++)
	    {
	        ?>
	            <tr>
	                <td>
	                    <input type="hidden" name="no_family[]" value="<?php echo $no; ?>">
	                    <input type="hidden" name="v_sid_family_<?php echo $no; ?>" id="v_sid_family_<?php echo $no; ?>" value="0">
	                </td>
	                <td>
	                	<nobr>
	                    <select name="v_relation_id_<?php echo $no; ?>" id="v_relation_id_<?php echo $no; ?>" class="form-control-new" onchange="change_relation('<?php echo $no; ?>', this.value)">
	                        <option value="">Pilih Hubungan</option>
	                        <?php 
	                            $q = "
	                                    SELECT
	                                        ".$db["master"].".relation.relation_id,
	                                        ".$db["master"].".relation.relation_name
	                                    FROM
	                                        ".$db["master"].".relation
	                                    WHERE
	                                        1
	                                    ORDER BY
	                                        ".$db["master"].".relation.sort_by ASC
	                            ";
	                            $qry_rela = mysql_query($q);
	                            while($row_rela = mysql_fetch_array($qry_rela))
	                            {
	                        ?>
	                                <option value="<?php echo $row_rela["relation_id"]; ?>"><?php echo $row_rela["relation_name"]; ?></option>
	                        <?php 
	                            }
	                        ?>
	                    </select>
	                    
	                    <span id="show_cerai_<?php echo $no; ?>" style="font-weight:bold; display:none;">
	                        <select name="v_sts_cerai_<?php echo $no; ?>" id="v_sts_cerai_<?php echo $no; ?>" class="form-control-new">
	                            <option value="">Menikah</option>
	                            <option value="cerai">Cerai</option>
	                        </select>
	                    </span>
	                	</nobr>
	                </td>
	                <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_family_name_<?php echo $no; ?>" id="v_family_name_<?php echo $no; ?>"></td>
	               	<td align="center"><input type="text" class="form-control-new datepicker" size="12" maxlength="10" name="v_birthday_<?php echo $no; ?>" id="v_birthday_<?php echo $no; ?>" value=""></td>
	                <td align="center">
	                    <select name="v_gender_<?php echo $no; ?>" id="v_gender_<?php echo $no; ?>" style="width:100px;" class="form-control-new">
	                        <option value="Laki - Laki">Laki - Laki</option>
	                        <option value="Perempuan">Perempuan</option>
	                    </select>
	                </td>
	                <td align="center">
	                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_family" id="btn_save_ajax_family" value="Save" onClick="CallAjaxForm('save_ajax_family','<?php echo $no; ?>')">
							<i class="entypo-floppy"></i>
						</button>
	                </td>
	                <td>&nbsp;</td>
	            </tr>
	        <?php
	        $no++;
	    }
	    
	    
	    $q = "
            SELECT
                ".$db["master"].".employee_family.*
            FROM
                ".$db["master"].".employee_family
                INNER JOIN ".$db["master"].".relation ON
                    ".$db["master"].".employee_family.relation_id = ".$db["master"].".relation.relation_id
            WHERE
                ".$db["master"].".employee_family.employee_id = '".$id."'
            ORDER BY
                ".$db["master"].".relation.sort_by ASC,
                ".$db["master"].".employee_family.sid ASC
	    ";
	    $qry = mysql_query($q);
	    $no_view = 1;
	    while($r = mysql_fetch_object($qry))
	    {
		?>
			<tr>
			    <td>
			        <?php echo $no_view; ?>
			        <input type="hidden" name="no_family[]" value="<?php echo $no; ?>">
			        <input type="hidden" name="v_sid_family_<?php echo $no; ?>" id="v_sid_family_<?php echo $no; ?>" value="<?php echo $r->sid; ?>">
			    </td>
			    <td>
			    <nobr> 
			        <select name="v_relation_id_<?php echo $no; ?>" id="v_relation_id_<?php echo $no; ?>" class="form-control-new" onchange="change_relation('<?php echo $no; ?>', this.value)">
			            <option value="">Pilih Hubungan</option>
			            <?php 
			                $q = "
			                        SELECT
			                            ".$db["master"].".relation.relation_id,
			                            ".$db["master"].".relation.relation_name
			                        FROM
			                            ".$db["master"].".relation
			                        WHERE
			                            1
			                        ORDER BY
			                            ".$db["master"].".relation.sort_by ASC
			                ";
			                $qry_rela = mysql_query($q);
			                while($row_rela = mysql_fetch_array($qry_rela))
			                {
			                    $selected = "";
			                    if($row_rela["relation_id"]==$r->relation_id)
			                    {
			                        $selected = "selected='selected'";
			                    }
			                    
			            ?>
			                    <option <?php echo $selected; ?> value="<?php echo $row_rela["relation_id"]; ?>"><?php echo $row_rela["relation_name"]; ?></option>
			            <?php 
			                }
			            ?>
			        </select>
			        
			        <?php 
			            $display_cerai = "none";
			            if($r->relation_id==3)
			            {
			                $display_cerai = "";
			            }
			      
			        ?>
			        
			        <span id="show_cerai_<?php echo $no; ?>" style="font-weight:bold; display:<?php echo $display_cerai; ?>;">
			            <select name="v_sts_cerai_<?php echo $no; ?>" id="v_sts_cerai_<?php echo $no; ?>" class="form-control-new">
			                <option value="" <?php if($r->sts_cerai=="") echo "selected='selected'"; ?>>Menikah</option>
			                <option value="cerai" <?php if($r->sts_cerai=="cerai") echo "selected='selected'"; ?>>Cerai</option>
			            </select>
			        </span>
			    </nobr>
			    <?php //echo $r->sts_cerai; ?>
			    </td>
			    <td><input type="text" class="form-control-new" size="50" maxlength="255" name="v_family_name_<?php echo $no; ?>" id="v_family_name_<?php echo $no; ?>" value="<?php echo $r->family_name; ?>"></td>
			   	<td align="center"><input type="text" class="form-control-new datepicker" size="12" maxlength="10" name="v_birthday_<?php echo $no; ?>" id="v_birthday_<?php echo $no; ?>" value="<?php echo format_show_date($r->birthday); ?>"></td>
			    <td align="center">
			        <select name="v_gender_<?php echo $no; ?>" id="v_gender_<?php echo $no; ?>" style="width:100px;" class="form-control-new">
			            <option value="Laki - Laki" <?php if($r->gender=="Laki - Laki") echo "selected='selected'"; ?> >Laki - Laki</option>
			            <option value="Perempuan" <?php if($r->gender=="Perempuan") echo "selected='selected'"; ?>>Perempuan</option>
			        </select>
			    </td>
			    <td align="center">
                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" name="btn_save_ajax_family" id="btn_save_ajax_family" value="Save" onClick="CallAjaxForm('save_ajax_family','<?php echo $no; ?>')">
						<i class="entypo-floppy"></i>
					</button>
			    </td>
			    <td align="center"><input type="checkbox" name="del_family[]" value="<?php echo $r->sid; ?>"></td>
			</tr>
					
		<?php 
	        $no++;
	        $no_view++;
    	}
		
        if($id!="")
        {
    		?>
			<tr>     
			   <td colspan="6">&nbsp;</td>
			   <td align="center">
			   	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_del_family" id="btn_del_family" value="Hapus">Hapus<i class="entypo-trash"></i></button>
			   </td>
			</tr>
    		<?php 
        }
    	?>
	
	</tbody>
</table>
</div>