<?php 
    include("header.php"); 
    
    if(!isset($_GET["search_cabang_id"])){ $search_cabang_id = isset($_GET["search_cabang_id"]); } else { $search_cabang_id = $_GET["search_cabang_id"]; }
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    if(!isset($_GET["v_from"])){ $v_from = isset($_GET["v_from"]); } else { $v_from = $_GET["v_from"]; }
    if(!isset($_GET["v_to"])){ $v_to = isset($_GET["v_to"]); } else { $v_to = $_GET["v_to"]; }
    
    if(!isset($_GET["v_employee_id"])){ $v_employee_id = isset($_GET["v_employee_id"]); } else { $v_employee_id = $_GET["v_employee_id"]; }
    
    $link_adjust    = "?v_keyword=".$v_keyword."&v_from=".$v_from."&v_to=".$v_to."&p=".$p;
    $link_adjust   .= "&v_employee_id=".$v_employee_id;
    $link_adjust   .= "&search_cabang_id=".$search_cabang_id;
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $icon_type_change = "entypo-up-dir";
    $order_type_change = "asc";
    if($order_type=="asc")
    {
        $order_type_change = "desc";
    	$icon_type_change = "entypo-down-dir";
    }
    
    $order_by_content = "";
    if($order_by!="")
    {
        $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
    }
    
	$modul = "Posisi Karyawan";
    $list  = "npm_employee_position.php";
    $htm   = "npm_employee_position_form.php";
    $pk    = "sid";
    $title = "employee_name";
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
	</script>
</head>

<body class="page-body skin-black" onload="start_page();">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="get">
		<div class="row">
			<div class="col-md-10">
				<select class="form-control-new" name="search_cabang_id" id="search_cabang_id">
                    <option value="">Cabang</option>
                    <?php 
                        $q = "
                            SELECT
                                ".$db["master"].".hrd_cabang.cabang_id,
                                ".$db["master"].".hrd_cabang.cabang_name
                            FROM
                                ".$db["master"].".hrd_cabang
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".hrd_cabang.cabang_name ASC
                        ";
                        $qry_cab = mysql_query($q);
                        while($row_cab = mysql_fetch_array($qry_cab))
                        {
                            $selected = "";
                            if($search_cabang_id==$row_cab["cabang_id"])      
                            {
                                $selected = "selected='selected'";
                            }
                            
                            $cabang_name = str_replace("Cabang","",$row_cab["cabang_name"]);
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_cab["cabang_id"]; ?>"><?php echo $cabang_name; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
                Dari
	            &nbsp;
	            <input type="text" name="v_from" id="v_from" class="form-control-new" value="<?php echo $v_from; ?>" size="10" maxlength="10">
	            Sampai
	            &nbsp;
	            <input type="text" name="v_to" id="v_to" class="form-control-new" value="<?php echo $v_to; ?>" size="10" maxlength="10">
	            &nbsp;
	            <b>Pencarian</b>&nbsp;&nbsp;<input type="text" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>" size="30">
	     	</div>
			
			<div class="col-md-2" align="right">
				<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit" value="Cari">Search<i class="entypo-search"></i></button>
				<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100),get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>')">Tambah<i class="entypo-plus"></i></button>
			</div>
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="10">No</center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee_position.start_date&order_type=".$order_type_change; ?>" class="link_menu">Mulai<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee_position.end_date&order_type=".$order_type_change; ?>" class="link_menu">Akhir<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_nik&order_type=".$order_type_change; ?>" class="link_menu">NIK<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_name&order_type=".$order_type_change; ?>" class="link_menu">Nama<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=company.company_initial&order_type=".$order_type_change; ?>" class="link_menu">Perusahaan<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=hrd_cabang.cabang_name&order_type=".$order_type_change; ?>" class="link_menu">Cabang<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=hrd_divisi.divisi_name&order_type=".$order_type_change; ?>" class="link_menu">Divisi<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=jabatan.jabatan_name&order_type=".$order_type_change; ?>" class="link_menu">Jabatan<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        
                        <th><center>Navigasi</center></th>
					</tr>
				</thead>
				<tbody>
				
					<?php
                    $keyWord = trim($v_keyword);
                  	
                    if($keyWord == '' && $v_from=='' && $v_to=='' && $v_employee_id=="" && $search_cabang_id=="")
                    {
                        $sql = "
                                SELECT 
                                    ".$db["master"].".employee_position.sid,
                                    ".$db["master"].".employee_position.start_date,
                                    ".$db["master"].".employee_position.end_date,
                                    ".$db["master"].".employee.employee_id,
                                    ".$db["master"].".employee.employee_nik,
                                    ".$db["master"].".employee.employee_code_hrd,
                                    ".$db["master"].".employee.employee_name,
                                    ".$db["master"].".employee_position.remarks,
                                    ".$db["master"].".employee_position.author,
                                    ".$db["master"].".employee_position.edited,
                                    
                                    ".$db["master"].".company.company_id,
                                    ".$db["master"].".company.company_name,
                                    ".$db["master"].".company.company_initial,
                                    
                                    ".$db["master"].".hrd_divisi.divisi_id,
                                    ".$db["master"].".hrd_divisi.divisi_name,
                                    
                                    ".$db["master"].".hrd_departemen.departemen_id,
                                    ".$db["master"].".hrd_departemen.departemen_name,
                                    
                                    ".$db["master"].".hrd_cabang.cabang_id,
                                    ".$db["master"].".hrd_cabang.cabang_name,
                                    
                                    ".$db["master"].".depo.depo_id,
                                    ".$db["master"].".depo.depo_name,
                                    
                                    ".$db["master"].".jabatan.jabatan_id,
                                    ".$db["master"].".jabatan.jabatan_name
									
                                FROM 
                                    ".$db["master"].".employee_position
                                    INNER JOIN ".$db["master"].".employee ON
                                        ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                                    INNER JOIN ".$db["master"].".company ON
                                        ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                    INNER JOIN ".$db["master"].".hrd_divisi ON
                                        ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                    INNER JOIN ".$db["master"].".hrd_departemen ON
                                        ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                    INNER JOIN ".$db["master"].".depo ON
                                        ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                    INNER JOIN ".$db["master"].".hrd_cabang ON
                                        ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                    INNER JOIN ".$db["master"].".jabatan ON
                                        ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                WHERE
                                    1
                                ORDER BY
                                    ".$order_by_content." 
                                    ".$db["master"].".employee_position.start_date DESC,
                                    ".$db["master"].".employee_position.end_date DESC
                        ";
                        $query = mysql_query($sql);
                        $max = ceil(mysql_num_rows($query)/$jml_page);
                        $s = $jml_page * $p;
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".employee_position.sid,
                                        ".$db["master"].".employee_position.start_date,
                                        ".$db["master"].".employee_position.end_date,
                                        ".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name,
                                        ".$db["master"].".employee_position.remarks,
                                        ".$db["master"].".employee_position.author,
                                        ".$db["master"].".employee_position.edited,
                                        
                                        ".$db["master"].".company.company_id,
                                        ".$db["master"].".company.company_name,
                                        ".$db["master"].".company.company_initial,
                                        
                                        ".$db["master"].".hrd_divisi.divisi_id,
                                        ".$db["master"].".hrd_divisi.divisi_name,
                                        
                                        ".$db["master"].".hrd_departemen.departemen_id,
                                        ".$db["master"].".hrd_departemen.departemen_name,
                                        
                                        ".$db["master"].".hrd_cabang.cabang_id,
                                        ".$db["master"].".hrd_cabang.cabang_name,
                                        
                                        ".$db["master"].".depo.depo_id,
                                        ".$db["master"].".depo.depo_name,
                                        
                                        ".$db["master"].".jabatan.jabatan_id,
                                        ".$db["master"].".jabatan.jabatan_name
                                        
                                    FROM 
                                        ".$db["master"].".employee_position
                                        INNER JOIN ".$db["master"].".employee ON
                                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                                        INNER JOIN ".$db["master"].".company ON
                                            ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                        INNER JOIN ".$db["master"].".hrd_divisi ON
                                            ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                        INNER JOIN ".$db["master"].".hrd_departemen ON
                                            ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                        INNER JOIN ".$db["master"].".depo ON
                                            ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                        INNER JOIN ".$db["master"].".hrd_cabang ON
                                            ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                        INNER JOIN ".$db["master"].".jabatan ON
                                            ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                    WHERE
                                        1
                                        ".$echo_ses_group."
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".employee_position.start_date DESC,
                                        ".$db["master"].".employee_position.end_date DESC
                                    LIMIT ".$s.", ".$jml_page." 
                                ";
                    }
                    else
                    {
                    	$where_cabang_id = "";
                        if($search_cabang_id!="")
                        {
                            $where_cabang_id = " AND hrd.hrd_cabang.cabang_id = '".$search_cabang_id."' ";    
                        }
                        else
                        {
							$where_cabang_id = $echo_ses_group;
						}
                        
                        $where_date = "";
                        if($v_from!="" && $v_to!="")
                        {
                            $where_date = " 
                            	AND 
                                (
                                    ".$db["master"].".employee_position.start_date BETWEEN '".format_save_date($v_from)."' AND '".format_save_date($v_to)."' 
                                    OR
                                    ".$db["master"].".employee_position.end_date BETWEEN '".format_save_date($v_from)."' AND '".format_save_date($v_to)."' 
                                )
                            ";
                        }
                        
                        $where_employee_id = "";
                        if($v_employee_id!="")
                        {
                            $where_employee_id = " AND employee.employee_id = '".$v_employee_id."' ";    
                        }
                        
                        unset($arr_keyword);
						$arr_keyword[0] = "employee.employee_name";
						$arr_keyword[1] = "employee.employee_nik";
						$arr_keyword[2] = "employee.employee_code_hrd";
						$arr_keyword[3] = "company.company_name";
                        $arr_keyword[4] = "company.company_initial";
						$arr_keyword[5] = "hrd_cabang.cabang_name";
                        $arr_keyword[6] = "depo.depo_name";
                        $arr_keyword[7] = "hrd_divisi.divisi_name";
                        $arr_keyword[8] = "hrd_departemen.departemen_name";
                        $arr_keyword[9] = "jabatan.jabatan_name";
						
						$search_keyword = search_keyword($v_keyword, $arr_keyword);
						$where = $search_keyword.$where_date;
                        
                        $flag=0;
                        for($i=0; $i < strlen($keyWord); $i++)
                        {
                            if($keyWord[$i] == '\'') $flag++;
                            if($keyWord[$i] == '<') $flag++;
                            if($keyWord[$i] == '>') $flag++;
                        }
                        
                        if($flag==0)
                        {
                            $sql = "
                                    SELECT 
                                        ".$db["master"].".employee_position.sid,
                                        ".$db["master"].".employee_position.start_date,
                                        ".$db["master"].".employee_position.end_date,
                                        ".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name,
                                        ".$db["master"].".employee_position.remarks,
                                        ".$db["master"].".employee_position.author,
                                        ".$db["master"].".employee_position.edited,
                                        
                                        ".$db["master"].".company.company_id,
                                        ".$db["master"].".company.company_name,
                                        ".$db["master"].".company.company_initial,
                                        
                                        ".$db["master"].".hrd_divisi.divisi_id,
                                        ".$db["master"].".hrd_divisi.divisi_name,
                                        
                                        ".$db["master"].".hrd_departemen.departemen_id,
                                        ".$db["master"].".hrd_departemen.departemen_name,
                                        
                                        ".$db["master"].".hrd_cabang.cabang_id,
                                        ".$db["master"].".hrd_cabang.cabang_name,
                                        
                                        ".$db["master"].".depo.depo_id,
                                        ".$db["master"].".depo.depo_name,
                                        
                                        ".$db["master"].".jabatan.jabatan_id,
                                        ".$db["master"].".jabatan.jabatan_name
                                        
                                    FROM 
                                        ".$db["master"].".employee_position
                                        INNER JOIN ".$db["master"].".employee ON
                                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                                        INNER JOIN ".$db["master"].".company ON
                                            ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                        INNER JOIN ".$db["master"].".hrd_divisi ON
                                            ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                        INNER JOIN ".$db["master"].".hrd_departemen ON
                                            ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                        INNER JOIN ".$db["master"].".depo ON
                                            ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                        INNER JOIN ".$db["master"].".hrd_cabang ON
                                            ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                        INNER JOIN ".$db["master"].".jabatan ON
                                            ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                    WHERE
                                        1
                                        ".$where."
                                        ".$where_date."
                                        ".$where_employee_id."
                                        ".$where_cabang_id."
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".employee_position.start_date DESC,
                                        ".$db["master"].".employee_position.end_date DESC
                                   ";
                            $query = mysql_query($sql);
                            $max = ceil(mysql_num_rows($query)/$jml_page);
                            $s = $jml_page * $p;
                            $sql = "
                                        SELECT 
                                            ".$db["master"].".employee_position.sid,
                                            ".$db["master"].".employee_position.start_date,
                                            ".$db["master"].".employee_position.end_date,
                                            ".$db["master"].".employee.employee_id,
                                            ".$db["master"].".employee.employee_nik,
                                            ".$db["master"].".employee.employee_code_hrd,
                                            ".$db["master"].".employee.employee_name,
                                            ".$db["master"].".employee_position.remarks,
                                            ".$db["master"].".employee_position.author,
                                            ".$db["master"].".employee_position.edited,
                                            
                                            ".$db["master"].".company.company_id,
                                            ".$db["master"].".company.company_name,
                                            ".$db["master"].".company.company_initial,
                                            
                                            ".$db["master"].".hrd_divisi.divisi_id,
                                            ".$db["master"].".hrd_divisi.divisi_name,
                                            
                                            ".$db["master"].".hrd_departemen.departemen_id,
                                            ".$db["master"].".hrd_departemen.departemen_name,
                                            
                                            ".$db["master"].".hrd_cabang.cabang_id,
                                            ".$db["master"].".hrd_cabang.cabang_name,
                                            
                                            ".$db["master"].".depo.depo_id,
                                            ".$db["master"].".depo.depo_name,
                                            
                                            ".$db["master"].".jabatan.jabatan_id,
                                            ".$db["master"].".jabatan.jabatan_name
                                            
                                        FROM 
                                            ".$db["master"].".employee_position
                                            INNER JOIN ".$db["master"].".employee ON
                                                ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                                            INNER JOIN ".$db["master"].".company ON
                                                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                            INNER JOIN ".$db["master"].".hrd_divisi ON
                                                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                            INNER JOIN ".$db["master"].".hrd_departemen ON
                                                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                            INNER JOIN ".$db["master"].".depo ON
                                                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                            INNER JOIN ".$db["master"].".hrd_cabang ON
                                                ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                            INNER JOIN ".$db["master"].".jabatan ON
                                                ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                        WHERE
                                            1
                                            ".$where."
                                            ".$where_date."
                                            ".$where_employee_id."
                                            ".$where_cabang_id."
                                        ORDER BY
                                            ".$order_by_content." 
                                            ".$db["master"].".employee_position.start_date DESC,
                                            ".$db["master"].".employee_position.end_date DESC
                                        LIMIT ".$s.", ".$jml_page." 
                                  ";
                        }else
                        {
                            $msg = "Your input are not allowed!";
                        }
                    }
                    
                    $query = mysql_query($sql);
                            $sv = 0;
                            $i=1+($jml_page*$p);
                    if(!$row = mysql_num_rows($query))
                    {
                     echo "<tr>";
                     echo "<td align=\"center\" colspan=\"100%\">";
                     echo "No Data";
                     echo "</td>";
                     echo "</tr>";
                    }
                    else
                    {
                        while ($row = mysql_fetch_array($query))
                        {
                            $sv++;
                            
                            $bgcolor = "";
                            if($i%2==0)
                            {
                                $bgcolor = "background:#E7E7E7;";
                            }
                            
                            $status_join = get_employee_status_join($row["employee_id"], date_now('d/m/Y'));
                            
                            $bgcolor_status = "";
                            if($status_join["v_status"]=="resign")
                            {
                                $bgcolor_status = "background:#FFCCFF;";
                            }
                 ?>
                        <tr title="<?php echo $row[$title]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $i; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')" align="center"><?php echo format_show_date($row["start_date"]); ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')" align="center"><?php if($row["end_date"]!="0000-00-00") echo format_show_date($row["end_date"]); ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["employee_nik"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["employee_name"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["company_initial"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["cabang_name"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["divisi_name"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["jabatan_name"]; ?></td>
                            <td style="<?php echo $bgcolor_status; ?>" align="center">
                            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')">
									<i class="entypo-pencil"></i>
								</button>
								
								<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=delete&id=<?php echo $row[$pk]; ?>')">
									<i class="entypo-trash"></i>
								</button>
                            </td>
                        </tr>
                 <?php
                            $i = $i+1; 
                        }
                    }
                 ?>
                 
				</tbody>
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>
		
<?php include("footer.php"); ?>                           
	                           