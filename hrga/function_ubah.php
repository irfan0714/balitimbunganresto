<?php     
include 'function_general.php';

function base_url()
{
    if($_SERVER["SERVER_NAME"]=="localhost")
    {
        $base_url = "http://localhost/office/hrd-vci/";    
    }
    else
    {
        $base_url = "http://192.168.0.8/hrd/";
    }
    
    return $base_url;
}

function log_cron($subject, $content)
{
    global $db;
    
    $q = "
            INSERT INTO
                ".$db["master"].".log_cron 
            SET , 
                log_date = '".date_now("Y-m-d H:i:s")."' , 
                SUBJECT = '".$subject."' , 
                content = '".$content."'
    ";    
    
    mysql_query($q);
        
}

function log_login($v_username, $msg)
{
    global $db;
    
    $ip = getenv("REMOTE_ADDR");
    
    $q = "
            SELECT
                ".$db["master"].".user.username,
                ".$db["master"].".user.password
            FROM
                ".$db["master"].".user
            WHERE
                1
                AND ".$db["master"].".user.username = '".$v_username."'
            LIMIT
                0,1
    "; 
    
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $ins = "
                INSERT INTO 
                    ".$db["master"].".log_login 
                SET
                    login_date = '".date_now("Y-m-d H:i:s")."' , 
                    username = '".$v_username."' , 
                    password = '".$row["password"]."' , 
                    ip_address = '".$ip."' , 
                    msg = '".$msg."'
    ";
    
    mysql_query($ins);
}

function status_pajak($v_employee_id)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".employee_family.family_name,
                ".$db["master"].".employee_family.sts_cerai
            FROM
                ".$db["master"].".employee_family
            WHERE
                1 
                AND ".$db["master"].".employee_family.employee_id = '".$v_employee_id."'
                AND ".$db["master"].".employee_family.relation_id = '3'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $sts_kawin = "TK";
    
    if($row["family_name"]!="" && $row["sts_cerai"]!="cerai")
    {
        $sts_kawin = "K";
    }
    
    // cek anak
    $q = "
            SELECT
                COUNT(".$db["master"].".employee_family.family_name) as jml_anak
            FROM
                ".$db["master"].".employee_family
            WHERE
                1 
                AND ".$db["master"].".employee_family.employee_id = '".$v_employee_id."'
                AND ".$db["master"].".employee_family.relation_id = '4'
            LIMIT
                0,1
    ";
    $qry_anak = mysql_query($q);
    $row_anak = mysql_fetch_array($qry_anak);
    
    if($row_anak["jml_anak"]*1>0)
    {
        $sts_kawin .= "/".$row_anak["jml_anak"];   
    }
    
    return $sts_kawin;
}

function enkripsi($input, $type)
{
    if($type=="enkripsi")
    {
        $return = base64_encode(base64_encode(base64_encode(base64_encode(base64_encode($input))))); 
    }
    else if($type=="dekripsi")
    {
        $return = base64_decode(base64_decode(base64_decode(base64_decode(base64_decode($input)))));
    }
    
    return $return;
} 

function validasi_password($new_password, $old_password)
{
    $return["msg"]    = "";
    $return["status"] = 0;      
    
    $string  = "";
    $string .= "abcdefghijklmnopqrstuvwxyz";
    $string .= strtoupper("abcdefghijklmnopqrstuvwxyz");
     
    $jml_karakter = strlen($new_password);
    
    $sts_angka = 0;
    for($i=0;$i<$jml_karakter;$i++)
    {
        $curr = substr($new_password,$i,1);
        
        if(is_numeric($curr))
        {
            $sts_angka = 1;
            break;    
        }
    }
    
    $sts_string = 0;
    for($i=0;$i<$jml_karakter;$i++)
    {
        $curr = substr($new_password,$i,1);
        
        if(strpos($string,$curr))
        {
            $sts_string = 1;
            break;        
        }
    }
    
    if($new_password==$old_password)
    {
        $return["msg"]    = "Password tidak boleh sama dengan Password lama";
    }
    else if($jml_karakter < 6)
    {
        $return["msg"]    = "Password minimal 6 karakter";
    }
    else if($sts_angka==0)
    {
        $return["msg"] =  "Password harus mengandung angka<br>(Password harus terdiri dari kombinasi huruf dan angka)";    
    } 
    else if($sts_string==0)
    {
        $return["msg"] =  "Password harus mengandung huruf<br>(Password harus terdiri dari kombinasi huruf dan angka)";
    } 
    else
    {
        $return["status"] = 1;
        $return["msg"] = "Berhasil";
    }
    
    return $return;
}

function send_reset_password($v_username)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".employee.email,
                ".$db["master"].".employee.employee_name
            FROM
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.username = '".$v_username."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $q = "
            SELECT
                ".$db["master"].".user.user_id
            FROM
                ".$db["master"].".user
            WHERE
                1
                AND ".$db["master"].".user.username = '".$v_username."'
            LIMIT
                0,1
    ";
    $qry_user = mysql_query($q);
    $row_user = mysql_fetch_array($qry_user);
    
    if($row["email"])
    {
        
        $mktime       = mktime(0,0,0,date_now("m"),date_now("d")+3,date_now("Y"));
        $expired_date = date("Y-m-d H:i:s", $mktime);
        
        $idp = $row_user["user_id"]."||".date_now("Y-m-d H:i:s")."||".$expired_date;
        $idp = $row_user["user_id"]."||".date_now("Y-m-d H:i:s");
        
        $enkripsi_idp = enkripsi($idp, "enkripsi");
        
        $q = "
                INSERT INTO
                    ".$db["master"].".reset_password 
                SET
                    submit_date = '".date_now("Y-m-d H:i:s")."' , 
                    expired_date = '".$expired_date."' , 
                    user_id = '".$row_user["user_id"]."' , 
                    idp = '".$enkripsi_idp."' , 
                    STATUS = '0'
        ";
        mysql_query($q);
        
        $link_idp = "http://192.168.0.100/vci/resetpassword.php?idp=".$enkripsi_idp;
        
        $isi = '
                <table width="600" align="center" cellspacing="2" cellpadding="2">
                      <tr>
                          <td colspan="3">Dear '.$row["employee_name"].', <br><br>Untuk Reset Password anda, harap Klik Link Berikut :</td>
                      </tr>
                      
                      <tr>
                          <td colspan="3"><a href="'.$link_idp.'" target="_blank">'.$link_idp.'</a></td>
                      </tr>
                      
                    
                       <tr>
                          <td colspan="3"><br><br>Bila mengalami kendala, mohon hubungi administrator. <br>Terima Kasih</td>
                      </tr>
                </table>
                ';
        
        send_email("Reset Password Kode idp", $isi, $row["email"], $row["employee_name"], "Auto Email");
        
        $msg = "Reset Password Berhasil<br>Password dikirim ke email";
    }
    else
    {
        $msg = "User tidak memiliki email";    
    }
    
    
    return $msg;
}


function reset_password($v_username)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
            WHERE
                ".$db["master"].".employee.username = '".$v_username."'
            LIMIT
                0,1
    ";
    $qry_emp = mysql_query($q);
    $row_emp = mysql_fetch_array($qry_emp);
    $id = $row_emp["employee_id"];
    
    $generate_password = generate_password($id, 6);
        
//    $upd = "
//            UPDATE
//                ".$db["master"].".employee
//            SET
//                password = '".md5($generate_password)."'
//            WHERE
//                ".$db["master"].".employee.employee_id = '".$id."'
//    ";
//    mysql_query($upd);
    
    $q = "SELECT username, employee_name, email FROM ".$db["master"].".employee WHERE employee_id = '".$id."' ";
    $qry_username = mysql_query($q);
    $row_username = mysql_fetch_array($qry_username);
    
    $q = "UPDATE ".$db["master"].".user SET password = '".md5($generate_password)."' WHERE username = '".$row_username["username"]."' ";
    mysql_query($q);
    
    // kalau ada email, kirim ke email tentang perubahan password
    if($row_username["email"])
    {
        $isi = '
                <table width="600" align="center" cellspacing="2" cellpadding="2">
                    <tr>
                          <td colspan="3">Dear '.$row_username["employee_name"].', <br><br>Berikut Account user yang baru :</td>
                      </tr>
                      
                      <tr>
                          <td colspan="3">&nbsp;</td>
                      </tr>
                      
                     <tr height="25">
                          <td width="100"><b>Username</b></td>
                          <td width="9">:</td>
                          <td>'.$row_username["username"].'</td>
                      </tr>
                    
                    <tr height="25">
                        <td><b>Password</b></td>
                        <td>:</td>
                        <td>'.$generate_password.'</td>
                    </tr>
                    
                    <tr>
                      <td colspan="3"><br><br>Anda memilik otorisasi di : </td>
                   </tr>
                   
                   <tr>
                          <td colspan="3">
                                <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                    <tr style="font-weight:bold;">
                                        <td>Url Applikasi</td>
                                        <td>Nama Applikasi</td>
                                        <td>&nbsp;</td>
                                    </tr>
                   
                   ';
                   
               
               $q = "
                        SELECT
                            uni.sort_by,
                            uni.application_url,
                            uni.application_name,
                            uni.level_name
                        FROM
                        (
                            SELECT
                                '1' as sort_by,
                                'http://192.168.0.100/vci' AS application_url,
                                'System Kepegawaian' AS application_name,
                                ".$db["master"].".group_user.group_user_name AS level_name
                            FROM
                                ".$db["master"].".user
                            INNER JOIN ".$db["master"].".group_user_details ON
                                ".$db["master"].".group_user_details.user_id = ".$db["master"].".user.user_id    
                            INNER JOIN ".$db["master"].".group_user ON
                                ".$db["master"].".group_user_details.group_user_id = ".$db["master"].".group_user.group_user_id    
                            WHERE
                                1
                                AND ".$db["master"].".user.username     = '".$row_username["username"]."'
                            UNION ALL
                            SELECT
                                '2' as sort_by,
                                ".$db["master"].".application.application_url,
                                ".$db["master"].".application.application_name,
                                ".$db["master"].".application_level.level_name
                            FROM
                                ".$db["master"].".application_setup
                                INNER JOIN ".$db["master"].".application ON
                                    ".$db["master"].".application_setup.application_id = ".$db["master"].".application.application_id
                                INNER JOIN ".$db["master"].".application_level ON
                                     ".$db["master"].".application_level.sid = ".$db["master"].".application_setup.application_level_id
                            WHERE
                                ".$db["master"].".application_setup.employee_id = '".$id."'
                        )as uni 
                        WHERE
                            1
                        ORDER BY
                            uni.sort_by ASC,
                            uni.application_name ASC
               ";    
               $qry_apps = mysql_query($q);
               while($row_apps = mysql_fetch_array($qry_apps))
               {
                    $isi .='    
                            <tr>
                                <td>'.$row_apps["application_url"].'</td>
                                <td>'.$row_apps["application_name"].'</td>
                                <td>&nbsp;</td>
                            </tr>                 
                       ';    
               }    
                   
                $isi .='
                        </table>
                          </td>
                      </tr>
                    
                       <tr>
                          <td colspan="3"><br><br>Segeralah mengganti password anda, pastikan password hanya anda yang mengetahui nya. <br>Terima Kasih</td>
                      </tr>
                </table>
                ';
        
        
        send_email("Reset Password", $isi, $row_username["email"], $row_username["employee_name"], "Auto Email");
    }
    
    update_global_user($id);
        
}

function update_global_user($employee_id)
{
    global $db;
    
//    $upd = "
//                UPDATE
//                    ".$db["master"].".employee
//                    INNER JOIN ".$db["master"].".user ON
//                        ".$db["master"].".employee.username = ".$db["master"].".user.username
//                SET
//                    ".$db["master"].".employee.password = ".$db["master"].".user.password
//                WHERE
//                    ".$db["master"].".employee.employee_id = '".$employee_id."'
//    ";
//    mysql_query($upd);
    
    $q = "
            SELECT
                ".$db["master"].".employee.employee_name,
                ".$db["master"].".employee.username,
                ".$db["master"].".user.password,
                ".$db["master"].".application.db_host,
                ".$db["master"].".application.db_user,
                ".$db["master"].".application.db_password,
                ".$db["master"].".application.db_name,
                ".$db["master"].".application.table_level_name,
                ".$db["master"].".application.kolom_level_name,
                ".$db["master"].".application.kolom_user_name,
                ".$db["master"].".application.kolom_nama_user,
                ".$db["master"].".application.kolom_password,
                ".$db["master"].".application.kolom_active,
                ".$db["master"].".application_level.level_code,
                ".$db["master"].".application_level.level_name
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".application_setup ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".application_setup.employee_id
                    AND ".$db["master"].".employee.employee_id = '".$employee_id."'
                    AND ".$db["master"].".application_setup.employee_id = '".$employee_id."'
                INNER JOIN ".$db["master"].".application ON
                    ".$db["master"].".application.application_id = ".$db["master"].".application_setup.application_id
                INNER JOIN ".$db["master"].".application_level ON
                    ".$db["master"].".application_setup.application_level_id = ".$db["master"].".application_level.sid
                INNER JOIN ".$db["master"].".user ON
                    ".$db["master"].".user.username = ".$db["master"].".employee.username
            WHERE   
                1
            ORDER BY
                 ".$db["master"].".application_setup.sid ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        $db_connect =  mysql_connect($row["db_host"], $row["db_user"], $row["db_password"]) or die ("Error Connection Database ".$row["db_name"]);
        
        $q = "
                SELECT
                    ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_level_name"]." as level_id 
                FROM           
                    ".$row["db_name"].".".$row["table_level_name"]."
                WHERE
                    ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_user_name"]." = '".$row["username"]."'
                LIMIT
                    0,1
        ";
        
        //die($q);
        
        
        $qry_cek = mysql_query($q);
        $row_cek = mysql_fetch_array($qry_cek);
        $oi = "";
        if($row_cek["level_id"]=="")
        {
            ## insert
            if($row["kolom_active"]=="non")
            {
                $q = "
                        INSERT INTO
                            ".$row["db_name"].".".$row["table_level_name"]."
                        SET 
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_level_name"]." = '".$row["level_code"]."' ,
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_password"]." = '".$row["password"]."',
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_user_name"]." = '".$row["username"]."',
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_nama_user"]." = '".$row["employee_name"]."'
                ";
                mysql_query($q);
            }
            else
            {
                $q = "
                        INSERT INTO
                            ".$row["db_name"].".".$row["table_level_name"]."
                        SET 
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_level_name"]." = '".$row["level_code"]."' ,
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_password"]." = '".$row["password"]."',
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_user_name"]." = '".$row["username"]."',
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_nama_user"]." = '".$row["employee_name"]."',
                            ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_active"]." = '1' 
                ";
                mysql_query($q); 
                //$oi .= $q."<hr>";    
            }
        }
        else
        {
            ## update
            $q = "
                    UPDATE
                        ".$row["db_name"].".".$row["table_level_name"]."
                    SET 
                        ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_level_name"]." = '".$row["level_code"]."' ,
                        ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_password"]." = '".$row["password"]."' 
                    WHERE
                        ".$row["db_name"].".".$row["table_level_name"].".".$row["kolom_user_name"]." = '".$row["username"]."'
            ";
            //die($q);
            $oi .= $q."<hr>";
            mysql_query($q);
        }    
    }
    
    //die($oi); 
}

function cron_log($subject, $status, $message)
{
    global $db; 
    
    $q = "
            INSERT 
                ".$db["master"].".cron_log 
            SET
                cron_datetime = '".date("Y-m-d H:i:s")."' , 
                SUBJECT = '".$subject."' , 
                STATUS = '".$status."' , 
                message = '".$message."'
            ";
    mysql_query($q);
}

function generate_username($v_employee_name, $v_birthday)
{
    global $db;
    
    $exp_employee_name  = explode(" ", $v_employee_name);
    $jml_spasi          = count($exp_employee_name);
    
    $exp_birthday       = explode("/", $v_birthday);
    $hari               = $exp_birthday[0];
    $bulan              = $exp_birthday[1];
    $tahun              = $exp_birthday[2]; 
    
    $arr_data[1] = $exp_employee_name[0].$hari.$bulan;
    
    if($jml_spasi>=2)
    {
        $arr_data[2] = $exp_employee_name[1].$hari.$bulan;    
    }
    
    if($jml_spasi>=2)
    {
        $arr_data[3] = "";
        for($i=1;$i<=$jml_spasi;$i++)
        {
            $curr = $i-1;
            $arr_data[3] .= substr($exp_employee_name[$curr],0,1);    
        }
        $arr_data[3] .= $hari.$bulan;
    }
    
    $jml_arr_data = count($arr_data);
    
    // cek ke database
    $j = 1;
    for($i=1;$i<=$jml_arr_data;$i++)
    {
        $username = strtolower($arr_data[$i]);
        
        $q = "
                SELECT
                    username
                FROM
                    ".$db["master"].".user
                WHERE
                    ".$db["master"].".user.username = '".$username."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $ada = mysql_num_rows($qry);
        
        if(!$ada)
        {
           $arr_username[$j] = $username; 
           $j++; 
        }
    }
    
    return $arr_username;
}
    

function generate_password($employee_id, $jml_char)
{
    $bahan = $employee_id.date("d-m-Y H:i:s");
    
    $return = substr(md5($bahan),0,$jml_char);
    
    return $return;    
}

function status_absen($absen_date, $jam_absensi, $employee_id)
{
    global $db;
    
    $data["masuk"]  = 0;    
    $data["pot_25"] = 0;
    $data["pot_50"] = 0;
    
    if($jam_absensi)
    {
        $data["masuk"] = 1;    
    
        $q = "
                SELECT
                    ".$db["master"].".work_hour.work_hour_name,
                    ".$db["master"].".work_day.work_day_name
                FROM
                    ".$db["master"].".employee
                    INNER JOIN ".$db["master"].".work_hour ON
                        ".$db["master"].".employee.work_hour_id = ".$db["master"].".work_hour.work_hour_id
                    INNER JOIN ".$db["master"].".work_day ON
                        ".$db["master"].".employee.work_day_id = ".$db["master"].".work_day.work_day_id
                WHERE
                    ".$db["master"].".employee.employee_id = '".$employee_id."'
                LIMIT
                    0,1 
        ";
        
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        
        //if($employee_id=="11")
        //{
        //    echo $absen_date." || ".$jam_absensi."<hr>";
        //}
        
        if($row["work_hour_name"])
        {
            $exp_work_hour = explode("-",$row["work_hour_name"]);
            $jam_masuk = $exp_work_hour[0];
            
            $jadwal_masuk = parsedate_time(format_show_date($absen_date)." ".$exp_work_hour[0]); 
            $absen_masuk  = parsedate_time(format_show_date($absen_date)." ".$jam_absensi); 
            $selisih      = $absen_masuk - $jadwal_masuk;
            
            //if($employee_id=="11")
            //{
            //    echo $absen_date." || ".$jam_absensi." || ".$exp_work_hour[0]."++".$absen_masuk." - ".$jadwal_masuk."<hr>";
            //}
            
            if($selisih >= 1 && $selisih <= 1800)
            {
                $data["pot_25"] = 1;    
            }
            else if($selisih >= 1801)
            {
                $data["pot_50"] = 1;    
            }
            
            
        }
    
    } 
    
    
    
    
    
    
    return $data;
}

function jml_hari_absen($employee_id, $periode_start, $periode_end)
{
    global $db;
    
    $date_awal  = parsedate(format_show_date($periode_start));
    $date_akhir = parsedate(format_show_date($periode_end));

    $tot_masuk  = 0;
    $tot_pot_25 = 0;
    $tot_pot_50 = 0;
    
    for($j=$date_awal;$j<=$date_akhir;$j=$j+86400)
    {
        $hari = get_day(date("d/m/Y",$j));
        
        $jam_absensi     = jam_absensi(date("Y-m-d",$j), $employee_id);
        
        $status_absen    = status_absen(date("Y-m-d",$j), $jam_absensi["masuk"], $employee_id);
        
        $tot_masuk    += $status_absen["masuk"];
        $tot_pot_25   += $status_absen["pot_25"];
        $tot_pot_50   += $status_absen["pot_50"];
        
    }
                                    
    $data["total"]  = $tot_masuk;
    $data["pot_25"] = $tot_pot_25;
    $data["pot_50"] = $tot_pot_50;
    
    return $data;
}

function cek_relation($v_employee_id, $v_relation_id, $v_family_name)
{
    global $db;
    
    unset($arr_keyword, $data);
    $arr_keyword[0] = "employee_family.family_name";
    
    $search_keyword = search_keyword_or($v_family_name, $arr_keyword);
    $where = $search_keyword;
    
    $q = "
            SELECT
                ".$db["master"].".relation.relation_name,
                ".$db["master"].".employee_family.family_name,
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee_family
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee_family.employee_id = ".$db["master"].".employee.employee_id
                    AND ".$db["master"].".employee_family.employee_id NOT IN('".$v_employee_id."')
                INNER JOIN ".$db["master"].".relation ON
                    ".$db["master"].".relation.relation_id = ".$db["master"].".employee_family.relation_id
            WHERE
                1
                ".$where."
            ORDER BY
                ".$db["master"].".employee.employee_name ASC
    ";
    $i = 0;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        $data[$i]["relation_name"] = $row["relation_name"];
        $data[$i]["family_name"]   = $row["family_name"];
        $data[$i]["employee_id"]   = $row["employee_id"];
        
        $i++;
    }
    
    return $data;
    
}

function send_email_employee($employee_id, $subject, $isi)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".employee.email,
                ".$db["master"].".employee.employee_name
            FROM
                ".$db["master"].".employee
            WHERE
                ".$db["master"].".employee.employee_id = '".$employee_id."'
            LIMIT
                0,1    
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($row["email"])
    {
        send_email($subject, $isi, $row["email"], $row["employee_name"], "Auto Email");
    }
    
}
            
function send_notif($tanggal, $notif_type, $notif_modul, $employee_id, $subject, $isi)
{
    global $db; 
    
    $q = "
            INSERT INTO
                ".$db["master"].".notif
            SET    
                notif_date = '".$tanggal."' , 
                notif_type = '".$notif_type."' , 
                notif_modul = '".$notif_modul."' , 
                employee_id = '".$employee_id."' , 
                SUBJECT = '".$subject."' , 
                message = '".$isi."'
    ";   
    mysql_query($q);
}
            //send_notif(date("Y-m-d H:i:s"), "personal", $row_hen["approval_level_user_1"], "Pengajuan Cuti", $isi);

function date_diff_hen($date1, $date2)
{
    $diff = abs(strtotime($date2) - strtotime($date1));

    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $return["tahun"] = $years;
    $return["bulan"] = $months;
    $return["hari"] = $days;
    
    return $return;
}

function masa_kerja($employee_id, $tanggal)
{
    global $db;
    
    // kalo pernah resign, ambil posisi terakhir. kalo gak tetep posisi pertama
    
    $q = "
        SELECT
            ".$db["master"].".employee_resign.resign_date
        FROM
            ".$db["master"].".employee_resign
        WHERE                                                   
            ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
        ORDER BY
            ".$db["master"].".employee_resign.resign_date DESC
        LIMIT
            0,1
    ";
    $qry_cek = mysql_query($q);
    $row_cek = mysql_fetch_array($qry_cek);
    
    if($row_cek["resign_date"])
    {
        $q = "
            SELECT
                ".$db["master"].".employee_join.join_date
            FROM
                ".$db["master"].".employee_join
            WHERE                                                   
                ".$db["master"].".employee_join.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_join.join_date DESC
            LIMIT
                0,1
        ";
    }
    else
    {
        $q = "
            SELECT
                ".$db["master"].".employee_join.join_date
            FROM
                ".$db["master"].".employee_join
            WHERE                                                   
                ".$db["master"].".employee_join.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_join.join_date ASC
            LIMIT
                0,1
        ";    
    }
        //echo $q."<hr>";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $start  = $row["join_date"];
    $end    = $tanggal;
    
    //echo $start." - ".$end;
    
    $masa_kerja = date_diff_hen($start, $end);
    
    $return = $masa_kerja["tahun"]." Tahun ".$masa_kerja["bulan"]." Bulan ".$masa_kerja["hari"]." Hari";
    
    return $return;
}

function sisa_cuti($employee_id)
{
    global $db;
    
    // kalo pernah resign, ambil posisi terakhir. kalo gak tetep posisi pertama
    
    $q = "
        SELECT
            ".$db["master"].".employee_resign.resign_date
        FROM
            ".$db["master"].".employee_resign
        WHERE                                                   
            ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
        ORDER BY
            ".$db["master"].".employee_resign.resign_date DESC
        LIMIT
            0,1
    ";
    $qry_cek = mysql_query($q);
    $row_cek = mysql_fetch_array($qry_cek);
    
    if($row_cek["resign_date"])
    {
        $q = "
                SELECT
                    ".$db["master"].".employee_join.join_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                ORDER BY
                    ".$db["master"].".employee_join.join_date DESC
                LIMIT
                    0,1
            ";
    }
    else
    {
        $q = "
                SELECT
                    ".$db["master"].".employee_join.join_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                ORDER BY
                    ".$db["master"].".employee_join.join_date ASC
                LIMIT
                    0,1
            ";    
    }
 
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $start  = $row["join_date"];
    $end    = date("Y-m-d"); 
    $masa_kerja = date_diff_hen($start, $end);
    
    $tahun = date("Y");
    $tahun = "2013";
    
    $q = "
            SELECT
                ".$db["master"].".leave_saldo.amount
            FROM
                ".$db["master"].".leave_saldo
            WHERE
                1
                AND ".$db["master"].".leave_saldo.employee_id = '".$employee_id."'
                AND ".$db["master"].".leave_saldo.saldo_year = '".$tahun."'
            LIMIT
                0,1
    ";
    $qry_sa = mysql_query($q);
    $row_sa = mysql_fetch_array($qry_sa);
    
    $sa = 12;
    if($row_sa["amount"]*1<>0)
    {
        $sa = $row_sa["amount"];
    }
    
    $q = "
            SELECT
                COUNT(".$db["master"].".leave_together_details.leave_date) as jml
            FROM
                ".$db["master"].".leave_together 
                INNER JOIN ".$db["master"].".leave_together_details ON
                    ".$db["master"].".leave_together.leave_together_id = ".$db["master"].".leave_together_details.leave_together_id
                    -- AND ".$db["master"].".leave_together.leave_year = '".$tahun."'
            WHERE
                1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $cuti_bersama = $row["jml"];
    
    
    // cuti personal
    $q = "
            SELECT
                COUNT(".$db["master"].".leave_details.leave_date) as jml
            FROM
                ".$db["master"].".leave
                INNER JOIN ".$db["master"].".leave_details ON
                    ".$db["master"].".leave.leave_id = ".$db["master"].".leave_details.leave_id
                    AND ".$db["master"].".leave.approval_level_1 = '1'
                    AND ".$db["master"].".leave.status_potong = '1'
                    AND ".$db["master"].".leave.employee_id = '".$employee_id."' 
                    -- AND YEAR(".$db["master"].".leave_details.leave_date) = '".$tahun."'
            WHERE
                1
    ";
    $qry_personal = mysql_query($q);
    $row_personal = mysql_fetch_array($qry_personal);
    
    $cuti_personal = $row_personal["jml"]*1;
    
    $sisa = $sa - ($cuti_bersama + $cuti_personal);
    
    if($masa_kerja["tahun"] < 1)
    {
        $sisa_show = "Anda belum mendapatkan Jatah Cuti";
    }
    else
    {
        // tambah cuti 2014
        $sisa += 12;
        
        $sisa_show = $sisa." Hari";
    }
    
    return $sisa_show;
}

function permit_cuti($employee_id, $tanggal)
{
    global $db;
    
    $q = "
            SELECT
                uni.tipe,
                uni.keterangan
            FROM
                (
                    SELECT
                        ".$db["master"].".permit.permit_type as tipe,
                        ".$db["master"].".permit.remarks as keterangan
                    FROM
                        ".$db["master"].".permit
                        INNER JOIN ".$db["master"].".permit_details ON
                            ".$db["master"].".permit.permit_id = ".$db["master"].".permit_details.permit_id
                            AND ".$db["master"].".permit_details.permit_date = '".$tanggal."'
                            AND ".$db["master"].".permit.employee_id = '".$employee_id."'
                    WHERE
                        1
                    
                    UNION ALL
                    
                    SELECT
                        ".$db["master"].".leave.leave_type as tipe,
                        ".$db["master"].".leave.remarks as keterangan
                    FROM
                        ".$db["master"].".leave
                        INNER JOIN ".$db["master"].".leave_details ON
                            ".$db["master"].".leave.leave_id = ".$db["master"].".leave_details.leave_id
                            AND ".$db["master"].".leave_details.leave_date = '".$tanggal."'
                            AND ".$db["master"].".leave.employee_id = '".$employee_id."'
                            AND ".$db["master"].".leave.approval_level_1 = '1'
                            AND ".$db["master"].".leave.approval_level_2 = '1'
                            AND ".$db["master"].".leave.approval_level_3 = '1'
                    WHERE
                        1
                    
                )as uni
            WHERE
                1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
    
}

function absen_bg_masuk($tanggal, $jam_masuk, $employee_id)
{
    global $db;
    $return = "";
    
    $q = "
            SELECT
                ".$db["master"].".work_hour.work_hour_name,
                ".$db["master"].".work_day.work_day_name
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".work_hour ON
                    ".$db["master"].".employee.work_hour_id = ".$db["master"].".work_hour.work_hour_id
                INNER JOIN ".$db["master"].".work_day ON
                    ".$db["master"].".employee.work_day_id = ".$db["master"].".work_day.work_day_id
            WHERE
                ".$db["master"].".employee.employee_id = '".$employee_id."'
            LIMIT
                0,1 
    ";
    //echo $q."<hr>";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($row["work_hour_name"])
    {
        $exp_work_hour = explode("-",$row["work_hour_name"]);
        
        
        if($jam_masuk > $exp_work_hour[0])
        {
            //echo "masuk ".$exp_work_hour[0]." Jam Masuk".$jam_masuk."<br>";
            $return = "background:#FFFF00;";    
        }
    }
    
    
    return $return;
}

function absen_bg_pulang($tanggal, $jam_pulang, $employee_id)
{
    global $db;
    $return = "";
    
    $q = "
            SELECT
                ".$db["master"].".work_hour.work_hour_name,
                ".$db["master"].".work_day.work_day_name
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".work_hour ON
                    ".$db["master"].".employee.work_hour_id = ".$db["master"].".work_hour.work_hour_id
                INNER JOIN ".$db["master"].".work_day ON
                    ".$db["master"].".employee.work_day_id = ".$db["master"].".work_day.work_day_id
            WHERE
                ".$db["master"].".employee.employee_id = '".$employee_id."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($row["work_hour_name"])
    {
        $exp_work_hour = explode("-",$row["work_hour_name"]);
        
        if($jam_pulang < $exp_work_hour[1])
        {
            $return = "background:#FFFF00;";    
        }
    }
    
    $hari = date("N", parsedate(format_show_date($tanggal)));
    //echo $hari;
    
    if($hari=="6")
    {
        $return = "";
        
        if($exp_work_hour[1] < "13:00:00")
        {
            $return = "background:#FFFF00;";    
        }
    }
    
    if($hari=="7")
    {
        $return = "";
    }
    
    if($jam_pulang=="")
    {
        $return = "";
    }
    
    return $return;
    
}

function jam_absensi($tanggal, $employee_id)
{
    global $db;
    
    $q = "
            SELECT
                DATE_FORMAT(".$db["master"].".absensi.absensi_date,'%H:%i:%s') AS absensi_date
            FROM
                ".$db["master"].".absensi
            WHERE
                1
                AND ".$db["master"].".absensi.absensi_date BETWEEN '".$tanggal." 00:00:00' AND '".$tanggal." 23:59:59'
                AND ".$db["master"].".absensi.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".absensi.absensi_date ASC
    ";
    $qry_jml   = mysql_query($q);
    $jml_absen = mysql_num_rows($qry_jml);
    
    $q = "
            SELECT
                DATE_FORMAT(".$db["master"].".absensi.absensi_date,'%H:%i:%s') AS absensi_date
            FROM
                ".$db["master"].".absensi
            WHERE
                1
                AND ".$db["master"].".absensi.absensi_date BETWEEN '".$tanggal." 00:00:00' AND '".$tanggal." 23:59:59'
                -- AND ".$db["master"].".absensi.absensi_type = 'in'
                AND ".$db["master"].".absensi.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".absensi.absensi_date ASC
            LIMIT
                0,1
    ";
    //echo $q."<hr>";
    $qry_in = mysql_query($q);
    $row_in = mysql_fetch_array($qry_in);
    
    $q = "
            SELECT
                DATE_FORMAT(".$db["master"].".absensi.absensi_date,'%H:%i:%s') AS absensi_date
            FROM
                ".$db["master"].".absensi
            WHERE
                1
                AND ".$db["master"].".absensi.absensi_date BETWEEN '".$tanggal." 00:00:00' AND '".$tanggal." 23:59:59'
                -- AND ".$db["master"].".absensi.absensi_type = 'out'
                AND ".$db["master"].".absensi.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".absensi.absensi_date DESC
            LIMIT
                0,1
    ";
    $qry_out = mysql_query($q);
    $row_out = mysql_fetch_array($qry_out);
    
    if($jml_absen*1==0)
    {
        $row_in["absensi_date"]  = "";
        $row_out["absensi_date"] = "";
    }
    
    if($jml_absen*1==1)
    {
        $row_out["absensi_date"] = "";
    }
    
    $return["masuk"]  = $row_in["absensi_date"];
    $return["pulang"] = $row_out["absensi_date"];
    
    return $return;
    
}

function saldo_awal_cuti($v_saldo_year, $employee_id)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".leave_saldo.saldo_id,
                ".$db["master"].".leave_saldo.amount
            FROM
                ".$db["master"].".leave_saldo
            WHERE
                1
                AND ".$db["master"].".leave_saldo.saldo_year = '".$v_saldo_year."'
                AND ".$db["master"].".leave_saldo.employee_id = '".$employee_id."'
            LIMIT
                0,1
    ";
    //echo $q."<hr>";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
} 

function data_menu($menu_id)
{
    global $db;
    
    $q= "
            SELECT
                *
            FROM
                ".$db["master"].".menu
            WHERE
                ".$db["master"].".menu.menu_id = '".$menu_id."'
            LIMIT
                0,1
    ";
    
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
}

function setup_leave_approval($leave_id)
{    
    global $db;
    global $config_employee;
    
    $v_employee_id  = $config_employee["employee_id"];
    
    $q = "
            SELECT
                ".$db["master"].".leave.submit_date,
                ".$db["master"].".leave.employee_id,
                ".$db["master"].".leave.approval_level_user_1,
                ".$db["master"].".leave.approval_level_user_2,
                ".$db["master"].".leave.approval_level_user_3
            FROM
                ".$db["master"].".leave
            WHERE
                1
                AND ".$db["master"].".leave.leave_id = '".$leave_id."'
                AND ".$db["master"].".leave.employee_id = '".$v_employee_id."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $v_employee_id = $row["employee_id"];
    
    $approval_level_user_1 = $row["approval_level_user_1"];
    $approval_level_user_2 = $row["approval_level_user_2"];
    $approval_level_user_3 = $row["approval_level_user_3"];
    
    $level_1 = search_atasan($v_employee_id);
    $level_2 = search_atasan($level_1);
    $level_3 = "903";
    
    // kalo belum ada dipilih - dipilih
    if($approval_level_user_1*1==0)
    {
        $upd_level_1 = "UPDATE ".$db["master"].".leave SET approval_level_user_1 = '".$level_1."' WHERE leave_id = '".$leave_id."' ";
        mysql_query($upd_level_1);
    }
    
    if($approval_level_user_2*1==0)
    {
        $upd_level_2 = "UPDATE ".$db["master"].".leave SET approval_level_user_2 = '".$level_2."' WHERE leave_id = '".$leave_id."' ";
        mysql_query($upd_level_2);
    }
    
    if($approval_level_user_3*1==0)
    {
        $upd_level_3 = "UPDATE ".$db["master"].".leave SET approval_level_user_3 = '".$level_3."' WHERE leave_id = '".$leave_id."' ";
        mysql_query($upd_level_3);
    }
    
}

function search_atasan($v_employee_id)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".setup_atasan.employee_id
            FROM
                ".$db["master"].".setup_atasan
                INNER JOIN ".$db["master"].".setup_atasan_details ON
                    ".$db["master"].".setup_atasan.setup_atasan_id = ".$db["master"].".setup_atasan_details.setup_atasan_id
                    AND ".$db["master"].".setup_atasan_details.employee_id = '".$v_employee_id."'
            WHERE
                1
            LIMIT
                0,1
    ";
    $qry_pos_1 = mysql_query($q);
    $row_pos_1 = mysql_fetch_array($qry_pos_1);
    
    return $row_pos_1["employee_id"];
}

function get_images_approval($nilai, $employee_name)
{
    if($nilai==0)
    {
        $return = "<img title='Proses ".$employee_name."' src='images/cal_forward.gif'>";
    }
    else if($nilai==1)
    {
        $return = "<img title='Disetujui ".$employee_name."' src='images/accept.png'>";    
    }
    else if($nilai==2)
    {
        $return = "<img title='Ditolak ".$employee_name."' src='images/cross.gif'>";
    }
    
    return $return;
}

function show_tanggal_cuti($nilai)
{
    $exp_nilai   = explode(",",$nilai);
    $jml_tanggal = count($exp_nilai);
    
    $show = "";
    for($i=1;$i<=$jml_tanggal;$i++)
    {
        $curr  = $i-1;
        
        $show .= format_show_date($exp_nilai[$curr])." (".get_day(format_show_date($exp_nilai[$curr])).")<br>";
    }   
    $show = substr($show,0,-4);
    
    return $show;
    
}

function get_status_cuti($nilai)
{
    if($nilai=="0")
    {
        $return = "Proses";
    }
    else if($nilai=="1")
    {
        $return = "Diterima";
    }
    else if($nilai=="2")
    {
        $return = "Ditolak";
    }
    
    return $return;
}

function employee_data($v_employee_id, $v_date)
{
    global $db;
    
    $q = "SELECT 
            ".$db["master"].".employee.employee_id,
            ".$db["master"].".employee.employee_nik,
            ".$db["master"].".employee.employee_code_hrd,
            ".$db["master"].".employee.employee_name,
            
            tbl_position.company_id,
            tbl_position.company_name,
            tbl_position.company_initial,
            tbl_position.cabang_id,
            tbl_position.cabang_name,
            tbl_position.depo_id,
            tbl_position.depo_name,                                                            
            tbl_position.divisi_id,
            tbl_position.divisi_name,
            tbl_position.departemen_id,
            tbl_position.departemen_name,
            tbl_position.jabatan_id,
            tbl_position.jabatan_name
        FROM 
            ".$db["master"].".employee
            INNER JOIN
            (
                SELECT
                    ".$db["master"].".employee_position.employee_id,
                    
                    ".$db["master"].".company.company_id,
                    ".$db["master"].".company.company_name,
                    ".$db["master"].".company.company_initial,
                    
                    ".$db["master"].".cabang.cabang_id,
                    ".$db["master"].".cabang.cabang_name,
                    
                    ".$db["master"].".depo.depo_id,
                    ".$db["master"].".depo.depo_name,
                    
                    ".$db["master"].".divisi.divisi_id,
                    ".$db["master"].".divisi.divisi_name,
                    
                    ".$db["master"].".departemen.departemen_id,
                    ".$db["master"].".departemen.departemen_name,
                    
                    ".$db["master"].".jabatan.jabatan_id,
                    ".$db["master"].".jabatan.jabatan_name
                FROM
                    ".$db["master"].".employee_position
                    INNER JOIN ".$db["master"].".company ON
                        ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                    INNER JOIN ".$db["master"].".divisi ON
                        ".$db["master"].".employee_position.divisi_id = ".$db["master"].".divisi.divisi_id
                    INNER JOIN ".$db["master"].".departemen ON
                        ".$db["master"].".employee_position.departemen_id = ".$db["master"].".departemen.departemen_id
                    INNER JOIN ".$db["master"].".depo ON
                        ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                    INNER JOIN ".$db["master"].".cabang ON
                        ".$db["master"].".depo.cabang_id = ".$db["master"].".cabang.cabang_id
                    INNER JOIN ".$db["master"].".jabatan ON
                        ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                WHERE
                    1
            )as tbl_position ON
                tbl_position.employee_id = ".$db["master"].".employee.employee_id
            
            INNER JOIN
            (
                SELECT 
                    tbl_fixed.employee_id,
                    tbl_fixed.v_status,
                    tbl_fixed.v_date
                FROM
                (
                    SELECT
                        uni.employee_id,
                        uni.v_status,
                        uni.v_date
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_join.employee_id,
                            'join' as v_status,
                            ".$db["master"].".employee_join.join_date as v_date
                        FROM
                            ".$db["master"].".employee_join
                        WHERE
                            1
                            AND ".$db["master"].".employee_join.join_date <= '".format_save_date($v_date)."'
                            
                        UNION ALL
                        
                        SELECT
                            ".$db["master"].".employee_resign.employee_id,
                            'resign' as v_status,
                            ".$db["master"].".employee_resign.resign_date as v_date
                        FROM
                            ".$db["master"].".employee_resign
                        WHERE
                            1
                            AND ".$db["master"].".employee_resign.resign_date <= '".format_save_date($v_date)."'
                    ) as uni
                    
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id,
                        uni.v_date
                    ORDER BY
                        uni.employee_id DESC,
                        uni.v_date DESC 
                )AS tbl_fixed
                WHERE
                    1
                GROUP BY 
                    tbl_fixed.employee_id
            )as tbl_status ON
                tbl_status.employee_id = ".$db["master"].".employee.employee_id
                
        WHERE
            1
            AND tbl_status.v_status = 'join'
            AND ".$db["master"].".employee.employee_id = '".$v_employee_id."'
        ORDER BY
            ".$db["master"].".employee.employee_name ASC,
            ".$db["master"].".employee.employee_id ASC,
            ".$db["master"].".employee.employee_nik ASC,
            ".$db["master"].".employee.employee_code_hrd ASC
        LIMIT 0, 1 ";
     $qry = mysql_query($q);
     $row = mysql_fetch_array($qry);
     
     return $row;   
        
}

function employee_position($v_position_id, $v_date)
{
    global $db;
    
    $q = "SELECT 
            ".$db["master"].".employee.employee_id,
            ".$db["master"].".employee.employee_nik,
            ".$db["master"].".employee.employee_code_hrd,
            ".$db["master"].".employee.employee_name,
            tbl_position.company_name,
            tbl_position.cost_center_name,
            tbl_position.department_id,
            tbl_position.department_name,
            tbl_position.position_id,
            tbl_position.position_name
        FROM 
            ".$db["master"].".employee
            INNER JOIN
            (
                SELECT
                    ".$db["master"].".employee_position.employee_id,
                    ".$db["master"].".employee_position.position_id,
                    ".$db["master"].".position.position_name,
                    ".$db["master"].".department.cost_center_id,
                    ".$db["master"].".cost_center.cost_center_name,
                    ".$db["master"].".department.department_id,
                    ".$db["master"].".department.department_name,
                    ".$db["master"].".company.company_id,
                    ".$db["master"].".company.company_name
                FROM
                    ".$db["master"].".employee_position
                    INNER JOIN ".$db["master"].".position ON
                        ".$db["master"].".employee_position.position_id = ".$db["master"].".position.sid
                    INNER JOIN ".$db["master"].".department ON
                        ".$db["master"].".position.department_id = ".$db["master"].".department.department_id
                    INNER JOIN ".$db["master"].".cost_center ON
                        ".$db["master"].".department.cost_center_id = ".$db["master"].".cost_center.sid
                    INNER JOIN ".$db["master"].".company ON
                        ".$db["master"].".cost_center.company_id = ".$db["master"].".company.company_id
                WHERE
                    1
            )as tbl_position ON
                tbl_position.employee_id = ".$db["master"].".employee.employee_id
                AND tbl_position.position_id = '".$v_position_id."'
            
            INNER JOIN
            (
                SELECT 
                    tbl_fixed.employee_id,
                    tbl_fixed.v_status,
                    tbl_fixed.v_date
                FROM
                (
                    SELECT
                        uni.employee_id,
                        uni.v_status,
                        uni.v_date
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_join.employee_id,
                            'join' as v_status,
                            ".$db["master"].".employee_join.join_date as v_date
                        FROM
                            ".$db["master"].".employee_join
                        WHERE
                            1
                            AND ".$db["master"].".employee_join.join_date <= '".format_save_date($v_date)."'
                            
                        UNION ALL
                        
                        SELECT
                            ".$db["master"].".employee_resign.employee_id,
                            'resign' as v_status,
                            ".$db["master"].".employee_resign.resign_date as v_date
                        FROM
                            ".$db["master"].".employee_resign
                        WHERE
                            1
                            AND ".$db["master"].".employee_resign.resign_date <= '".format_save_date($v_date)."'
                    ) as uni
                    
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id,
                        uni.v_date
                    ORDER BY
                        uni.employee_id DESC,
                        uni.v_date DESC 
                )AS tbl_fixed
                WHERE
                    1
                GROUP BY 
                    tbl_fixed.employee_id
            )as tbl_status ON
                tbl_status.employee_id = ".$db["master"].".employee.employee_id
                
        WHERE
            1
            AND tbl_status.v_status = 'join'
        ORDER BY
            ".$db["master"].".employee.employee_name ASC,
            ".$db["master"].".employee.employee_id ASC,
            ".$db["master"].".employee.employee_nik ASC,
            ".$db["master"].".employee.employee_code_hrd ASC
        LIMIT 0, 1 ";
     $qry = mysql_query($q);
     $row = mysql_fetch_array($qry);
     
     return $row;   
}


function get_day($date)
{
    $parse = parsedate($date); 
    $day   = date("N", $parse);
    
    if($day=="1"){ $return = "Senin"; }
    else if($day=="2"){ $return = "Selasa"; }
    else if($day=="3"){ $return = "Rabu"; }
    else if($day=="4"){ $return = "Kamis"; }
    else if($day=="5"){ $return = "Jumat"; }
    else if($day=="6"){ $return = "Sabtu"; }
    else if($day=="7"){ $return = "Minggu"; }
    
    return $return;
}

function turn_over_karyawan($v_date_from, $v_date_to, $v_company_id, $cabang_id, $divisi_id)
{
    global $db;
    
    $emp_resign = "";
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_resign ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_resign.employee_id
                    AND ".$db["master"].".employee_resign.resign_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_resign.employee_id
    ";
    //echo $q."<hr>";
    $qry_resign = mysql_query($q);
    while($row_resign = mysql_fetch_array($qry_resign))
    {
        $emp_id      = $row_resign["employee_id"];
        $emp_resign .= "'".$emp_id."',";    
    }
    $emp_resign = substr($emp_resign,0,-1);
    
    $where_resign = "";
    $where_resign = " AND ".$db["master"].".employee.employee_id IN (".$emp_resign.") ";    
    
    
    
    // company
    $where_company_id = "";
    if($v_company_id)
    { 
        $exp_comp = explode("||", $v_company_id);
        $jml_comp = count($exp_comp)-1;
        
        $where_company_id = " AND tbl_position.company_id IN (";
        
        $isi_comp = "";
        for($i=0;$i<$jml_comp;$i++)
        {
            $isi_comp .= "'".$exp_comp[$i]."',";
        }
        
        $isi_comp = substr($isi_comp,0,-1);
        
        $where_company_id .= $isi_comp;
        $where_company_id .= ")";
    }
    
    if($cabang_id){ $where_cabang_id = " AND tbl_position.cabang_id = '".$cabang_id."'"; }
    if($divisi_id){ $where_divisi_id = " AND tbl_position.divisi_id = '".$divisi_id."'"; }
    
    
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN
                (
                    SELECT
                        uni.employee_id,
                        uni.company_id,
                        uni.company_initial,
                        uni.company_name,
                        uni.cabang_id,
                        uni.cabang_name,  
                        uni.depo_id,  
                        uni.depo_name,  
                        uni.divisi_id,  
                        uni.divisi_name,
                        uni.departemen_id,
                        uni.departemen_name,
                        uni.jabatan_id,
                        uni.jabatan_name
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_position.employee_id,
                            ".$db["master"].".company.company_id,
                            ".$db["master"].".company.company_initial,
                            ".$db["master"].".company.company_name,
                            ".$db["master"].".cabang.cabang_id,  
                            ".$db["master"].".cabang.cabang_name,
                            ".$db["master"].".depo.depo_id,
                            ".$db["master"].".depo.depo_name,
                            ".$db["master"].".divisi.divisi_id,
                            ".$db["master"].".divisi.divisi_name,
                            ".$db["master"].".departemen.departemen_id,
                            ".$db["master"].".departemen.departemen_name,
                            ".$db["master"].".jabatan.jabatan_id,
                            ".$db["master"].".jabatan.jabatan_name
                        FROM
                            ".$db["master"].".employee_position
                            INNER JOIN ".$db["master"].".company ON
                                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                            INNER JOIN ".$db["master"].".divisi ON
                                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".divisi.divisi_id
                            INNER JOIN ".$db["master"].".departemen ON
                                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".departemen.departemen_id
                            INNER JOIN ".$db["master"].".depo ON
                                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                            INNER JOIN ".$db["master"].".cabang ON
                                ".$db["master"].".depo.cabang_id = ".$db["master"].".cabang.cabang_id
                            INNER JOIN ".$db["master"].".jabatan ON
                                ".$db["master"].".jabatan.jabatan_id = ".$db["master"].".employee_position.jabatan_id
                        WHERE
                            1
                        ORDER BY 
                            ".$db["master"].".employee_position.sid DESC   
                    )AS uni
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id 
                )as tbl_position ON
                    tbl_position.employee_id = ".$db["master"].".employee.employee_id 
                    ".$where_resign."
            WHERE
                1
                ".$where_divisi_id."
                ".$where_cabang_id."
                ".$where_company_id."
            GROUP BY
                ".$db["master"].".employee.employee_id
                
    ";
    
    //echo $q;
    //die();
    $qry = mysql_query($q);
    $jml = mysql_num_rows($qry)*1;
    
    //$row["jml"] = 2;
    
    return $jml;
}  

function employee_status_cabang($cabang_id, $employee_status_id, $v_company_id, $v_date_from)
{
    global $db;      
    
    $emp_ok = "";
    $q = "
            SELECT
                tbl_join.employee_id AS employee_id_join,
                tbl_resign.employee_id AS employee_id_resign
            FROM
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_join ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                            AND ".$db["master"].".employee_join.join_date < '".format_save_date($v_date_from)."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_join.employee_id
                )AS tbl_join 
                LEFT JOIN 
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_resign ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_resign.employee_id
                            AND ".$db["master"].".employee_resign.resign_date < '".format_save_date($v_date_from)."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_resign.employee_id
                )AS tbl_resign ON
                    tbl_join.employee_id = tbl_resign.employee_id
            WHERE
                1
                AND tbl_resign.employee_id IS NULL 
    ";
    //echo "<pre>".$q."</pre>";
    //die();
    $qry_ok = mysql_query($q);
    while($row_ok = mysql_fetch_array($qry_ok))
    {
        $emp_id      = $row_ok["employee_id_join"];
        $emp_ok     .= "'".$emp_id."',";    
    }
    $emp_ok = substr($emp_ok,0,-1);
    
    $where_company_id = "";
    if($v_company_id)
    {
        $where_company_id = " AND tbl_position.company_id = '".$v_company_id."' "; 
    }
    
    $where_cabang_id = "";
    if($cabang_id)
    {
        $where_cabang_id = " AND tbl_position.cabang_id = '".$cabang_id."' "; 
    }
    
    $where_employee_status_id = "";
    $where_employee_status_id = " AND ".$db["master"].".employee.employee_status_id = '".$employee_status_id."' "; 
    
    $q = "
            SELECT
                COUNT(".$db["master"].".employee.employee_id) as jml
            FROM
                ".$db["master"].".employee
                INNER JOIN
                (
                    SELECT
                        uni.employee_id,
                        uni.company_id,
                        uni.company_initial,
                        uni.company_name,
                        uni.cabang_id,
                        uni.cabang_name,  
                        uni.depo_id,  
                        uni.depo_name,  
                        uni.divisi_id,  
                        uni.divisi_name,
                        uni.departemen_id,
                        uni.departemen_name,
                        uni.jabatan_id,
                        uni.jabatan_name
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_position.employee_id,
                            ".$db["master"].".company.company_id,
                            ".$db["master"].".company.company_initial,
                            ".$db["master"].".company.company_name,
                            ".$db["master"].".cabang.cabang_id,  
                            ".$db["master"].".cabang.cabang_name,
                            ".$db["master"].".depo.depo_id,
                            ".$db["master"].".depo.depo_name,
                            ".$db["master"].".divisi.divisi_id,
                            ".$db["master"].".divisi.divisi_name,
                            ".$db["master"].".departemen.departemen_id,
                            ".$db["master"].".departemen.departemen_name,
                            ".$db["master"].".jabatan.jabatan_id,
                            ".$db["master"].".jabatan.jabatan_name
                        FROM
                            ".$db["master"].".employee_position
                            INNER JOIN ".$db["master"].".company ON
                                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                            INNER JOIN ".$db["master"].".divisi ON
                                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".divisi.divisi_id
                            INNER JOIN ".$db["master"].".departemen ON
                                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".departemen.departemen_id
                            INNER JOIN ".$db["master"].".depo ON
                                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                            INNER JOIN ".$db["master"].".cabang ON
                                ".$db["master"].".depo.cabang_id = ".$db["master"].".cabang.cabang_id
                            INNER JOIN ".$db["master"].".jabatan ON
                                ".$db["master"].".jabatan.jabatan_id = ".$db["master"].".employee_position.jabatan_id
                        WHERE
                            1
                        ORDER BY 
                            ".$db["master"].".employee_position.sid DESC   
                    )AS uni
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id 
                )as tbl_position ON
                    tbl_position.employee_id = ".$db["master"].".employee.employee_id 
                    AND ".$db["master"].".employee.employee_id IN (".$emp_ok.")
                    ".$where_employee_status_id."
                    ".$where_cabang_id."
                    ".$where_company_id."
            WHERE
                1
    ";
    
    //echo "<pre>".$q."</pre>";
    //die();
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["jml"];
}

function department_cabang($cabang_id, $divisi_id, $v_company_id, $v_date_from)
{
    global $db;
 
    $where_company_id = "";
    if($v_company_id!="")
    {
        $where_company_id = " AND tbl_position.company_id = '".$v_company_id."' "; 
    }
    
    $where_divisi_id = "";
    if($divisi_id!="")
    {
        $where_divisi_id = " AND tbl_position.divisi_id = '".$divisi_id."' ";     
    }
    
    $where_cabang_id = "";
    if($cabang_id!="")
    {
        $where_cabang_id = " AND tbl_position.cabang_id = '".$cabang_id."' ";     
    }
    
    
    $emp_ok = "";
    $q = "
            SELECT
                tbl_join.employee_id AS employee_id_join,
                tbl_resign.employee_id AS employee_id_resign
            FROM
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_join ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                            AND ".$db["master"].".employee_join.join_date <= '".format_save_date($v_date_from)."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_join.employee_id
                )AS tbl_join 
                LEFT JOIN 
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_resign ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_resign.employee_id
                            AND ".$db["master"].".employee_resign.resign_date <= '".format_save_date($v_date_from)."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_resign.employee_id
                )AS tbl_resign ON
                    tbl_join.employee_id = tbl_resign.employee_id
            WHERE
                1
                AND tbl_resign.employee_id IS NULL 
    ";
    
    //echo $q;
    //die();
    $qry_ok = mysql_query($q);
    while($row_ok = mysql_fetch_array($qry_ok))
    {
        $emp_id      = $row_ok["employee_id_join"];
        $emp_ok     .= "'".$emp_id."',";    
    }
    $emp_ok = substr($emp_ok,0,-1);
    
    $q = "
            SELECT
                COUNT(".$db["master"].".employee.employee_id) as jml
            FROM
                ".$db["master"].".employee
                INNER JOIN
                (
                    SELECT
                        uni.employee_id,
                        uni.cabang_id,
                        uni.cabang_name,
                        uni.divisi_id,
                        uni.divisi_name,
                        uni.departemen_id,
                        uni.departemen_name,
                        uni.company_id,
                        uni.company_name,
                        uni.jabatan_name
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_position.employee_id,
                            ".$db["master"].".divisi.divisi_id,
                            ".$db["master"].".divisi.divisi_name,
                            ".$db["master"].".cabang.cabang_id,
                            ".$db["master"].".cabang.cabang_name,
                            ".$db["master"].".departemen.departemen_id,
                            ".$db["master"].".departemen.departemen_name,
                            ".$db["master"].".company.company_id,
                            ".$db["master"].".company.company_name,
                            ".$db["master"].".jabatan.jabatan_name
                        FROM
                            ".$db["master"].".employee_position
                            INNER JOIN ".$db["master"].".company ON
                                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                
                            INNER JOIN ".$db["master"].".divisi ON
                                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".divisi.divisi_id
                                
                            INNER JOIN ".$db["master"].".departemen ON
                                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".departemen.departemen_id
                            INNER JOIN ".$db["master"].".depo ON
                                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                            INNER JOIN ".$db["master"].".cabang ON
                                ".$db["master"].".depo.cabang_id = ".$db["master"].".cabang.cabang_id
                                
                            INNER JOIN ".$db["master"].".jabatan ON
                                ".$db["master"].".jabatan.jabatan_id = ".$db["master"].".employee_position.jabatan_id
                        WHERE
                            1
                        ORDER BY 
                            ".$db["master"].".employee_position.sid DESC   
                    )AS uni
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id 
                )as tbl_position ON
                    tbl_position.employee_id = ".$db["master"].".employee.employee_id 
                    AND ".$db["master"].".employee.employee_id IN(".$emp_ok.") 
                INNER JOIN
                (
                    SELECT 
                        tbl_fixed.employee_id,
                        tbl_fixed.v_status,
                        tbl_fixed.v_date
                    FROM
                    (
                        SELECT
                            uni.employee_id,
                            uni.v_status,
                            uni.v_date
                        FROM
                        (
                            SELECT
                                ".$db["master"].".employee_join.employee_id,
                                'join' as v_status,
                                ".$db["master"].".employee_join.join_date as v_date
                            FROM
                                ".$db["master"].".employee_join
                            WHERE
                                1
                                AND ".$db["master"].".employee_join.join_date <= '".format_save_date($v_date_from)."'
                                
                            UNION ALL
                            
                            SELECT
                                ".$db["master"].".employee_resign.employee_id,
                                'resign' as v_status,
                                ".$db["master"].".employee_resign.resign_date as v_date
                            FROM
                                ".$db["master"].".employee_resign
                            WHERE
                                1
                                AND ".$db["master"].".employee_resign.resign_date <= '".format_save_date($v_date_from)."'
                        ) as uni
                        
                        WHERE
                            1
                        GROUP BY
                            uni.employee_id,
                            uni.v_date
                        ORDER BY
                            uni.employee_id DESC,
                            uni.v_date DESC 
                    )AS tbl_fixed
                    WHERE
                        1
                    GROUP BY 
                        tbl_fixed.employee_id
                )as tbl_status ON
                    tbl_status.employee_id = ".$db["master"].".employee.employee_id
            WHERE
                1
                ".$where_company_id."
                ".$where_divisi_id."
                ".$where_cabang_id."
    ";
    //echo $q."<hr>";
    //die();
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["jml"];
}

function department_cabang_26072013($cost_center_name, $department_name, $v_company_id, $v_date_from)
{
    global $db;
 
    $where_company = "";
    if($v_company_id)
    {
        $where_company = " AND ".$db["master"].".company.company_id = '".$v_company_id."' "; 
    }
    
    $q = "
            SELECT
                COUNT(".$db["master"].".employee_position.employee_id) as jml
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_position ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                INNER JOIN ".$db["master"].".position ON
                    ".$db["master"].".position.sid = ".$db["master"].".employee_position.position_id
                INNER JOIN ".$db["master"].".department ON
                    ".$db["master"].".position.department_id = ".$db["master"].".department.department_id
                    AND ".$db["master"].".department.department_name = '".$department_name."'
                INNER JOIN ".$db["master"].".cost_center ON
                    ".$db["master"].".department.cost_center_id = ".$db["master"].".cost_center.sid
                    AND ".$db["master"].".cost_center.cost_center_name = '".$cost_center_name."'
                INNER JOIN ".$db["master"].".company ON
                    ".$db["master"].".cost_center.company_id = ".$db["master"].".company.company_id
                    ".$where_company."
                INNER JOIN
                (
                    SELECT 
                        tbl_fixed.employee_id,
                        tbl_fixed.v_status,
                        tbl_fixed.v_date
                    FROM
                    (
                        SELECT
                            uni.employee_id,
                            uni.v_status,
                            uni.v_date
                        FROM
                        (
                            SELECT
                                ".$db["master"].".employee_join.employee_id,
                                'join' as v_status,
                                ".$db["master"].".employee_join.join_date as v_date
                            FROM
                                ".$db["master"].".employee_join
                            WHERE
                                1
                                AND ".$db["master"].".employee_join.join_date <= '".format_save_date($v_date_from)."'
                                
                            UNION ALL
                            
                            SELECT
                                ".$db["master"].".employee_resign.employee_id,
                                'resign' as v_status,
                                ".$db["master"].".employee_resign.resign_date as v_date
                            FROM
                                ".$db["master"].".employee_resign
                            WHERE
                                1
                                AND ".$db["master"].".employee_resign.resign_date <= '".format_save_date($v_date_from)."'
                        ) as uni
                        
                        WHERE
                            1
                        GROUP BY
                            uni.employee_id,
                            uni.v_date
                        ORDER BY
                            uni.employee_id DESC,
                            uni.v_date DESC 
                    )AS tbl_fixed
                    WHERE
                        1
                    GROUP BY 
                        tbl_fixed.employee_id
                )as tbl_status ON
                    tbl_status.employee_id = ".$db["master"].".employee.employee_id
                    
                
            WHERE
                1
                AND tbl_status.v_status = 'join'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["jml"];
}


function bulan_3_karakter()
{
    unset($arr_data);
    
    $arr_data["1"] = "Jan";
    $arr_data["2"] = "Feb";
    $arr_data["3"] = "Mar";
    $arr_data["4"] = "Apr";
    $arr_data["5"] = "Mei";
    $arr_data["6"] = "Jun";
    $arr_data["7"] = "Jul";
    $arr_data["8"] = "Ags";
    $arr_data["9"] = "Sep";
    $arr_data["10"] = "Okt";
    $arr_data["11"] = "Nov";
    $arr_data["12"] = "Des";
    
    return $arr_data;
}

function total_employee($month, $year)
{
    $total = (active_employee($month, $year) + new_employee($month, $year)) - resign_employee($month, $year);
    
    $total_show = (active_employee($month, $year)." + ".new_employee($month, $year))." - ".resign_employee($month, $year);
    
    return $total;
}

function active_employee($month, $year)
{
    global $db; 
    
    $date_now = date_now("Y-m-d");
    
    $month = sprintf("%02s",$month);
    
    $date_awal  = $year."-".sprintf("%02s",$month)."-01";
    $date_akhir = $year."-".sprintf("%02s",$month)."-".date('t',mktime(0,0,0,sprintf("%02s",$month),1,$year));
    
     /*
     $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_join ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                    AND ".$db["master"].".employee_join.join_date < '".$date_awal."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_join.employee_id
    ";

    $qry = mysql_query($q);
    $jml_employee_prev = mysql_num_rows($qry);
    
    if($month*1==1)
    {
        if($date_now < $date_awal)
        {
            $active = 0;    
        }
        else
        {
            $resign = 0;
            $resign = resign_employee($month, $year);
            $new = new_employee($month, $year);
            $active = ($jml_employee_prev + $resign) - $new;
            $active_show = $jml_employee_prev." + ".$resign." = ".$active;
            
        }
    }
    else
    {
        if($date_now < $date_awal)
        {
            $active = 0;    
        }
        else
        {
            $active = total_employee($month-1, $year);
        }
    }
    
    
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_join ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                    AND ".$db["master"].".employee_join.join_date < '".$date_awal."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_join.employee_id
    ";

    $qry = mysql_query($q);
    $jml_employee_join = mysql_num_rows($qry);
    
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_resign ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_resign.employee_id
                    AND ".$db["master"].".employee_resign.resign_date < '".$date_awal."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_resign.employee_id
    ";

    $qry = mysql_query($q);
    $jml_employee_resign = mysql_num_rows($qry);
    
    $active = $jml_employee_join - $jml_employee_resign;
    */ 
    
    $emp_ok = 0;
    $q = "
            SELECT
                tbl_join.employee_id AS employee_id_join,
                tbl_resign.employee_id AS employee_id_resign
            FROM
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_join ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                            AND ".$db["master"].".employee_join.join_date < '".$date_awal."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_join.employee_id
                )AS tbl_join 
                LEFT JOIN 
                (
                    SELECT
                        ".$db["master"].".employee.employee_id
                    FROM
                        ".$db["master"].".employee
                        INNER JOIN ".$db["master"].".employee_resign ON
                            ".$db["master"].".employee.employee_id = ".$db["master"].".employee_resign.employee_id
                            AND ".$db["master"].".employee_resign.resign_date < '".$date_awal."'
                    WHERE
                        1
                    GROUP BY
                        ".$db["master"].".employee_resign.employee_id
                )AS tbl_resign ON
                    tbl_join.employee_id = tbl_resign.employee_id
            WHERE
                1
                AND tbl_resign.employee_id IS NULL 
    ";
    $qry_ok = mysql_query($q);
    while($row_ok = mysql_fetch_array($qry_ok))
    {
        $emp_ok++;    
    }
    
    return $emp_ok;
}

function active_employee_31122013($month, $year)
{
    global $db; 
    
    $date_now = date_now("Y-m-d");
    
    $month = sprintf("%02s",$month);
    
    $date_awal  = $year."-".sprintf("%02s",$month)."-01";
    $date_akhir = $year."-".sprintf("%02s",$month)."-".date('t',mktime(0,0,0,sprintf("%02s",$month),1,$year));
    
    $q = "
            SELECT
                ".$db["master"].".employee_join.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_join ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                    AND ".$db["master"].".employee_join.join_date < '".$date_awal."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_join.employee_id
    ";
    $qry = mysql_query($q);
    $jml_employee_prev = mysql_num_rows($qry);
    
    if($month*1==1)
    {
        if($date_now < $date_awal)
        {
            $active = 0;    
        }
        else
        {
            $resign = 0;
            $resign = resign_employee($month, $year);
            $new = new_employee($month, $year);
            $active = ($jml_employee_prev + $resign) - $new;
            $active_show = $jml_employee_prev." + ".$resign." = ".$active;
        }
    }
    else
    {
        if($date_now < $date_awal)
        {
            $active = 0;    
        }
        else
        {
            $active = total_employee($month-1, $year);
        }
    }
    
    return $active;
}

function new_employee($month, $year)
{
    global $db; 
    
    $month = sprintf("%02s",$month);
    
    $date_awal  = $year."-".sprintf("%02s",$month)."-01";
    $date_akhir = $year."-".sprintf("%02s",$month)."-".date('t',mktime(0,0,0,sprintf("%02s",$month),1,$year));
    
    
    // karywan aktive
    $emp_active = "";
    $q = "
            SELECT
                ".$db["master"].".employee.employee_id
            FROM
                ".$db["master"].".employee
                INNER JOIN ".$db["master"].".employee_join ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".employee_join.employee_id
                    AND ".$db["master"].".employee_join.join_date < '".$date_awal."'
            WHERE
                1
            GROUP BY
                ".$db["master"].".employee_join.employee_id
    ";
    $qry_act = mysql_query($q);
    while($row_act = mysql_fetch_array($qry_act))
    {
        $emp_id      = $row_act["employee_id"];
        $emp_active .= "'".$emp_id."',";    
    }
    $emp_active = substr($emp_active,0,-1);
    
    $q = "
            SELECT
                ".$db["master"].".employee_join.employee_id
            FROM
                ".$db["master"].".employee_join
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee_join.employee_id = ".$db["master"].".employee.employee_id
                    AND ".$db["master"].".employee_join.employee_id NOT IN (".$emp_active.")
            WHERE
                1
                AND MONTH(".$db["master"].".employee_join.join_date) = '".$month."'
                AND YEAR(".$db["master"].".employee_join.join_date) = '".$year."'
            GROUP BY
                ".$db["master"].".employee_join.employee_id 
    ";
    
    //if($month==10){echo $q."<hr>";}
    
    $qry = mysql_query($q);
    $row = mysql_num_rows($qry);
    
    return $row;
}

function resign_employee($month, $year)
{
    global $db; 
    
    $month = sprintf("%02s",$month);
    
    $q = "
            SELECT
                ".$db["master"].".employee_resign.employee_id as jml
            FROM
                ".$db["master"].".employee_resign
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee_resign.employee_id = ".$db["master"].".employee.employee_id
            WHERE
                1
                AND MONTH(".$db["master"].".employee_resign.resign_date) = '".$month."'
                AND YEAR(".$db["master"].".employee_resign.resign_date) = '".$year."'
            GROUP BY
                ".$db["master"].".employee_resign.employee_id 
    ";
    $qry = mysql_query($q);
    $row = mysql_num_rows($qry);
    
    return $row;
}

function mutasi_employee($month, $year)
{
    global $db; 
    
    $month = sprintf("%02s",$month);
    
    $q = "
            SELECT
                ".$db["master"].".employee_position.employee_id as jml
            FROM
                ".$db["master"].".employee_position
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee_position.employee_id = ".$db["master"].".employee.employee_id
            WHERE
                1
                AND MONTH(".$db["master"].".employee_position.start_date) = '".$month."'
                AND YEAR(".$db["master"].".employee_position.start_date) = '".$year."'
                AND ".$db["master"].".employee_position.v_status IN('Prmosi','Demosi','Mutasi')
            GROUP BY
                ".$db["master"].".employee_position.employee_id 
    ";
    $qry = mysql_query($q);
    $row = mysql_num_rows($qry);
    
    return $row;
}

function update_resign_position($sid, $action)
{
    global $db;
    
    $q = "
            SELECT
                ".$db["master"].".employee_resign.employee_id,
                ".$db["master"].".employee_resign.resign_date
            FROM
                ".$db["master"].".employee_resign
            WHERE
                1
                AND ".$db["master"].".employee_resign.sid = '".$sid."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($action=="add")
    {
        // cari posisi nya buat di update end date nya
        $q = "
                SELECT 
                    *
                FROM
                    ".$db["master"].".employee_position
                WHERE
                    1
                    AND ".$db["master"].".employee_position.employee_id = '".$row["employee_id"]."'
                    AND ".$db["master"].".employee_position.end_date = '0000-00-00'
                ORDER BY
                    ".$db["master"].".employee_position.sid ASC
        ";
        $qry_loop = mysql_query($q);
        while($row_loop = mysql_fetch_array($qry_loop))
        {
            $upd = "
                    UPDATE
                        ".$db["master"].".employee_position
                    SET
                        end_date = '".$row["resign_date"]."'
                    WHERE
                        ".$db["master"].".employee_position.sid = '".$row_loop["sid"]."'
            ";
            mysql_query($upd);
        }
        
        
        
    }
    else if($action=="delete")
    {
        // cari posisi nya buat di update end date nya
        $q = "
                SELECT 
                    *
                FROM
                    ".$db["master"].".employee_position
                WHERE
                    1
                    AND ".$db["master"].".employee_position.employee_id = '".$row["employee_id"]."'
                    AND ".$db["master"].".employee_position.end_date = '".$row["resign_date"]."'
                ORDER BY
                    ".$db["master"].".employee_position.sid ASC
        ";
        $qry_loop = mysql_query($q);
        while($row_loop = mysql_fetch_array($qry_loop))
        {
            $upd = "
                    UPDATE
                        ".$db["master"].".employee_position
                    SET
                        end_date = '0000-00-00'
                    WHERE
                        ".$db["master"].".employee_position.sid = '".$row_loop["sid"]."'
            ";
            mysql_query($upd);
        }
    }
    
}

function update_employee_join_resign($employee_id)
{
     global $db;
     
     $q = "
            SELECT 
                *
            FROM 
                ".$db["master"].".employee_position
            WHERE
                1
                AND ".$db["master"].".employee_position.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_position.start_date ASC
     ";
     $qry_position = mysql_query($q);
     $temp_start_date = "";
     $temp_end_date   = "";
     while($row_position = mysql_fetch_array($qry_position))
     {
        $start_date = $row_position["start_date"];
        $end_date   = $row_position["end_date"];
         
        if($start_date!="0000-00-00")
        {    
            $temp_start_date .= "'".$start_date."',";
        }
        
        if($end_date!="0000-00-00")
        {    
            $temp_end_date .= "'".$end_date."',";
        }
     } 
     $temp_start_date  = substr($temp_start_date,0,-1);
     $temp_end_date    = substr($temp_end_date,0,-1);
     
     $del_start = "DELETE FROM ".$db["master"].".employee_join WHERE join_date NOT IN (".$temp_start_date.") AND employee_id = '".$employee_id."' ";
     
     /*
     if($temp_end_date=="")
     {
         $del_end   = "DELETE FROM ".$db["master"].".employee_resign WHERE employee_id = '".$employee_id."' ";
     }
     else
     {
        $del_end   = "DELETE FROM ".$db["master"].".employee_resign WHERE resign_date NOT IN (".$temp_end_date.") AND employee_id = '".$employee_id."' ";    
     }
     */
     
     
     mysql_query($del_start);
     //mysql_query($del_end);
     
     $q = "
            SELECT 
                *
            FROM 
                ".$db["master"].".employee_position
            WHERE
                1
                AND ".$db["master"].".employee_position.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_position.start_date ASC
     ";
     $qry_position = mysql_query($q);
     while($row_position = mysql_fetch_array($qry_position))
     {
         $start_date = $row_position["start_date"];
         $end_date   = $row_position["end_date"];
         
         if($start_date!="0000-00-00")
         {
             $q = "
                    SELECT
                        *
                    FROM
                        ".$db["master"].".employee_join 
                    WHERE
                        1
                        AND ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                        AND ".$db["master"].".employee_join.join_date = '".$start_date."'
                    LIMIT
                        0,1
             ";
             $qry_cek_start = mysql_query($q);
             $jml_cek_start = mysql_num_rows($qry_cek_start);
             
             if($jml_cek_start*1==0)
             {
                 $counter_details = get_counter_int($db["master"],"employee_join","sid",100);
                 $ins = "
                            INSERT INTO 
                                ".$db["master"].".employee_join 
                            SET
                                sid = '".$counter_details."',
                                employee_id = '".$employee_id."',
                                join_date = '".$start_date."',
                                remarks = '',
                                author = '".ffclock()."',
                                edited = '".ffclock()."'
                 ";
                 mysql_query($ins);
             }
         }
         
         /*
         if($end_date!="0000-00-00")
         {
             $q = "
                    SELECT
                        *
                    FROM
                        ".$db["master"].".employee_resign
                    WHERE
                        1
                        AND ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
                        AND ".$db["master"].".employee_resign.resign_date = '".$end_date."'
                    LIMIT
                        0,1
             ";
             $qry_cek_end = mysql_query($q);
             $jml_cek_end = mysql_num_rows($qry_cek_end);
             
             if($jml_cek_end*1==0)
             {
                 $counter_details = get_counter_int($db["master"],"employee_resign","sid",100);
                 $ins = "
                            INSERT INTO 
                                ".$db["master"].".employee_resign
                            SET
                                sid = '".$counter_details."',
                                employee_id = '".$employee_id."',
                                resign_date = '".$end_date."',
                                remarks = '',
                                author = '".ffclock()."',
                                edited = '".ffclock()."'
                 ";
                 mysql_query($ins);
             }
         }
         */
         
     }
     
    
}

function get_employee_status_join($employee_id, $date)
{
    global $db;
    
    $q = "
            SELECT
                uni.v_status,
                uni.v_date
            FROM
            (
                SELECT
                    'join' as v_status,
                    ".$db["master"].".employee_join.join_date as v_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    1
                    AND ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                
                UNION ALL
                
                SELECT
                    'resign' as v_status,
                    ".$db["master"].".employee_resign.resign_date as v_date
                FROM
                    ".$db["master"].".employee_resign
                WHERE
                    1
                    AND ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
            ) as uni
            
            WHERE
                uni.v_date <= '".format_save_date($date)."'
            ORDER BY
                uni.v_date DESC
            LIMIT
                0,1
    ";
    
    //echo $q."<hr>";
    
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
}       

function get_currency($kurs_date,$kurs_code)
{
	global $db;
	
	$q = "
			SELECT
				".$db["master"].".kurs_harian.rate
			FROM
				".$db["master"].".kurs_harian
			WHERE
				".$db["master"].".kurs_harian.kurs_date = '".$kurs_date."'
				AND ".$db["master"].".kurs_harian.kurs_code = '".$kurs_code."'
			LIMIT
				0,1
	";
	//echo $q."<hr>";
	$qry = mysql_query($q);
	if(!mysql_num_rows($qry))
	{
		$return = 0;
	}
	else
	{
		$row = mysql_fetch_array($qry);
		$return = $row["rate"];
	}
	
	return $return;
}

function img_document($ext)
{
	if($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif")
	{
		$return = '<img src="images/photo.png" />';
	}
	else if($ext=="zip")
	{
		$return = '<img src="images/zip.png" />';
	}
	else if($ext=="rar")
	{
		$return = '<img src="images/rar.png" />';
	}
	else if($ext=="doc" || $ext=="docx")
	{
		$return = '<img src="images/doc.png" />';
	}
	else if($ext=="xls" || $ext=="xlsx")
	{
		$return = '<img src="images/xls.png" />';
	}
	else if($ext=="ppt" || $ext=="pptx")
	{
		$return = '<img src="images/ppt.png" />';
	}
	else if($ext=="ogg" || $ext=="mp3")
	{
		$return = '<img src="images/music.png" />';
	}
	else if($ext=="pdf")
	{
		$return = '<img src="images/pdf.png" />';
	}
	else
	{
		$return = '<img src="images/file.png" />';
	}
	
	return $return;
}

function pm_last_update($nilai)
{
	global $db;
	$q = "
			SELECT
				".$db["master"].".project_management_details.post_date
			FROM
				".$db["master"].".project_management_details
			WHERE
				1
				AND ".$db["master"].".project_management_details.enkripsi = '".$nilai."'
			ORDER BY
				".$db["master"].".project_management_details.post_date DESC
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["post_date"];
}


function pm_where_share($ses_login)
{
	global $db;
	
	$q = "
            SELECT 
                DISTINCT
                uni.show_doc
            FROM
            (
                SELECT
                    ".$db["master"].".project_management_share.project_management_no as show_doc
                FROM
                    ".$db["master"].".project_management_share
                WHERE
                    1
                    AND ".$db["master"].".project_management_share.username = '".$ses_login."'
                
                UNION ALL
                
                SELECT
                    ".$db["master"].".project_management.project_management_no as show_doc
                FROM
                    ".$db["master"].".project_management
                WHERE
                    1
                    AND ".$db["master"].".project_management.upload_by = '".$ses_login."'
            )as uni
            WHERE
                1
            ORDER BY
                uni.show_doc
    ";
    
    $where_share = " AND ".$db["master"].".project_management.project_management_no IN(";  
    $qry_share = mysql_query($q);
    while($row_share = mysql_fetch_array($qry_share))
    {
        $where_share .= "'".$row_share["show_doc"]."',";
    }
    if(mysql_num_rows($qry_share))
    {
        $where_share  = substr($where_share,0,-1);
    }
    $where_share .= ")";
    
    if(!mysql_num_rows($qry_share))
    {
        $where_share = " AND ".$db["master"].".project_management.project_management_no IN('')";    
    }
	
	return $where_share;
}

function ticket_internal_last_update($nilai)
{
	global $db;
	$q = "
			SELECT
				".$db["master"].".ticket_internal_details.username,
				".$db["master"].".ticket_internal_details.post_date
			FROM
				".$db["master"].".ticket_internal_details
			WHERE
				1
				AND ".$db["master"].".ticket_internal_details.ticket_internal_no = '".$nilai."'
			ORDER BY
				".$db["master"].".ticket_internal_details.sid DESC
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["username"]." ".format_show_datetime($row["post_date"]);
}

function ticket_internal_where_share($ses_login)
{
	global $db;
	
	$q = "
            SELECT 
                DISTINCT
                uni.show_doc
            FROM
            (
                SELECT
                    ".$db["master"].".ticket_internal.ticket_internal_no as show_doc
                FROM
                    ".$db["master"].".ticket_internal
					INNER JOIN ".$db["master"].".group_user_details ON
						".$db["master"].".ticket_internal.group_user_id = ".$db["master"].".group_user_details.group_user_id
                WHERE
                    1
                    AND ".$db["master"].".group_user_details.username = '".$ses_login."'
                
                UNION ALL
                
                SELECT
                    ".$db["master"].".ticket_internal.ticket_internal_no as show_doc
                FROM
                    ".$db["master"].".ticket_internal
                WHERE
                    1
                    AND ".$db["master"].".ticket_internal.created_by = '".$ses_login."'
				
				UNION ALL
				
				SELECT
                    ".$db["master"].".ticket_internal.ticket_internal_no as show_doc
                FROM
                    ".$db["master"].".ticket_internal
                WHERE
                    1
                    AND ".$db["master"].".ticket_internal.group_user_id = '0'
				
            )as uni
            WHERE
                1
            ORDER BY
                uni.show_doc
    ";
    
    $where_share = " AND ".$db["master"].".ticket_internal.ticket_internal_no IN(";  
    $qry_share = mysql_query($q);
    while($row_share = mysql_fetch_array($qry_share))
    {
        $where_share .= "'".$row_share["show_doc"]."',";
    }
    if(mysql_num_rows($qry_share))
    {
        $where_share  = substr($where_share,0,-1);
    }
    $where_share .= ")";
    
    if(!mysql_num_rows($qry_share))
    {
        $where_share = " AND ".$db["master"].".ticket_internal.ticket_internal_no IN('')";    
    }
	
	return $where_share;
}


function pm_total($nilai)
{
	global $db;
	$q = "
			SELECT
				COUNT(*) as jml
			FROM
				".$db["master"].".project_management_details
			WHERE
				1
				AND ".$db["master"].".project_management_details.enkripsi = '".$nilai."'
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["jml"];
}

function get_link($link)
{
	$return = save_pop(save_char(str_replace(' ','-',$link)));
	
	return $return;
}

function replace_karakter($source, $from, $to)
{
    $return = save_pop(save_char(str_replace($from, $to ,$source)));
    
    return $return;
}

function get_captcha($captcha_id)
{
    $format = md5("Hendri".$captcha_id);
    
    return $format;    
}

function get_data_budget($employee_type,$budget_mmyyyy)
{
	global $db;
	
	$q = "
			SELECT
				".$db["master"].".budget_details.sid,	
				".$db["master"].".budget_details.amount	
			FROM
				".$db["master"].".budget 
				INNER JOIN ".$db["master"].".budget_details ON
					".$db["master"].".budget.budget_id = ".$db["master"].".budget_details.budget_id
			WHERE
				1
				AND ".$db["master"].".budget.budget_mmyyyy = '".$budget_mmyyyy."'
				AND ".$db["master"].".budget_details.employee_type = '".$employee_type."'
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row;
}

function get_total_budget($budget_mmyyyy)
{
	global $db;
	
	$q = "
			SELECT
				SUM(".$db["master"].".budget_details.amount) as amount	
			FROM
				".$db["master"].".budget 
				INNER JOIN ".$db["master"].".budget_details ON
					".$db["master"].".budget.budget_id = ".$db["master"].".budget_details.budget_id
			WHERE
				1
				AND ".$db["master"].".budget.budget_mmyyyy = '".$budget_mmyyyy."'
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
	
	return $row["amount"];
}

function get_now_account($v_account_no)
{
	global $db;
	
	$saldo_awal = get_saldo_awal(date_now("d/m/Y"), date_now("d/m/Y"), $v_account_no);
	
	$q = "
			 SELECT 
				".$db["master"].".journal.*,
				".$db["master"].".account.account_name
			FROM 
				".$db["master"].".journal
				INNER JOIN ".$db["master"].".account ON
					".$db["master"].".account.account_no = ".$db["master"].".journal.account_no
			WHERE
				1
				AND ".$db["master"].".journal.account_no = '".$v_account_no."'
			ORDER BY
				".$db["master"].".journal.journal_date ASC,
				".$db["master"].".journal.journal_no ASC,
				".$db["master"].".account.account_name ASC
	";
	$qry = mysql_query($q);
	$balance = $saldo_awal;
	while($row = mysql_fetch_array($qry))
	{
		if($row["acc_post"]=="Debit")
		{
			$balance += $row["amount"];
		}
		else
		{
			$balance -= $row["amount"];
		}
	}
	
	return $balance;
}

function get_penjualan($v_date_from,$v_date_to)
{
	global $db;
	
	$q = "
			SELECT
				".$db["master"].".sales_invoice.sales_invoice_no,
				".$db["master"].".sales_invoice.delivery_order_no
			FROM
				".$db["master"].".sales_invoice
			WHERE
				1
				AND ".$db["master"].".sales_invoice.sales_invoice_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' 
			ORDER BY
				".$db["master"].".sales_invoice.sales_invoice_date ASC
	";
	$qry = mysql_query($q);
	$subtotal = 0;
	while($row = mysql_fetch_array($qry))
	{
		$subtotal += grandtotal_sales_wo_dp($row["sales_invoice_no"],$row["delivery_order_no"]);	
	}
	
	return $subtotal;
}	

function get_hpp($v_date_from,$v_date_to)
{
	global $db;
	
	$q = "
			SELECT
				".$db["master"].".sales_invoice.sales_invoice_no,
				".$db["master"].".sales_invoice.delivery_order_no
			FROM
				".$db["master"].".sales_invoice
			WHERE
				1
				AND ".$db["master"].".sales_invoice.sales_invoice_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' 
			ORDER BY
				".$db["master"].".sales_invoice.sales_invoice_date ASC
	";
	$qry = mysql_query($q);
	$grand_total = 0;
	while($row = mysql_fetch_array($qry))
	{
		$sales_invoice_no  = $row["sales_invoice_no"];
		$delivery_order_no = $row["delivery_order_no"];
		
		$q = "
				SELECT
					".$db["master"].".delivery_order_details.qty,
					".$db["master"].".sales_order_details.price,
					".$db["master"].".sales_order_details.disc_details,
					".$db["master"].".finish_goods.finish_goods_name,
					".$db["master"].".finish_goods.colour,
					".$db["master"].".finish_goods.type,
					".$db["master"].".finish_goods.material,
					".$db["master"].".sales_order_details.cogs
				FROM
					".$db["master"].".delivery_order_details
					INNER JOIN ".$db["master"].".delivery_order ON
						".$db["master"].".delivery_order_details.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
					INNER JOIN ".$db["master"].".sales_order_details ON
						".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
						AND ".$db["master"].".sales_order_details.finish_goods_id = ".$db["master"].".delivery_order_details.finish_goods_id
					INNER JOIN ".$db["master"].".finish_goods ON
						".$db["master"].".delivery_order_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
				WHERE
					".$db["master"].".delivery_order_details.delivery_order_no = '".$delivery_order_no."'
				ORDER BY
					".$db["master"].".delivery_order_details.sid ASC
		";
		$qry = mysql_query($q);
		$no = 1;
		$subtotal = 0;
		while($r = mysql_fetch_object($qry))
		{
			$subtotal = $r->cogs * $r->qty;
			$grand_total += $subtotal;
		}

	}
	
	return $grand_total;
}


function grandtotal_sales_wo_dp($id,$do_no)
{
    global $db; 
    
    $q = "
            SELECT 
                ".$db["master"].".sales_invoice.*,
                ".$db["master"].".sales_order.sales_order_no,
                ".$db["master"].".sales_order.dp_so,
                ".$db["master"].".customer.customer_name,
                dp_use
            FROM 
                ".$db["master"].".sales_invoice
                INNER JOIN ".$db["master"].".delivery_order ON
                    ".$db["master"].".delivery_order.delivery_order_no = ".$db["master"].".sales_invoice.delivery_order_no
                INNER JOIN ".$db["master"].".sales_order ON
                    ".$db["master"].".delivery_order.sales_order_no = ".$db["master"].".sales_order.sales_order_no
                INNER JOIN ".$db["master"].".customer ON
                    ".$db["master"].".customer.customer_id = ".$db["master"].".sales_order.customer_id
                LEFT JOIN
                (
                    SELECT
                        ".$db["master"].".delivery_order.sales_order_no,
                        SUM(".$db["master"].".sales_invoice.dp) as dp_use
                    FROM
                        ".$db["master"].".sales_invoice
                        INNER JOIN ".$db["master"].".delivery_order ON
                            ".$db["master"].".sales_invoice.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
                    GROUP BY
                        ".$db["master"].".delivery_order.sales_order_no
                ) as tbl_dp_use ON
                    tbl_dp_use.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
            WHERE
                1
                AND ".$db["master"].".sales_invoice.sales_invoice_no = '".$id."'
            LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
            SELECT
                ".$db["master"].".delivery_order_details.qty,
                ".$db["master"].".sales_order_details.price,
                ".$db["master"].".sales_order_details.disc_details,
                ".$db["master"].".finish_goods.finish_goods_name,
                ".$db["master"].".finish_goods.colour,
                ".$db["master"].".finish_goods.type,
                ".$db["master"].".finish_goods.material
            FROM
                ".$db["master"].".delivery_order_details
                INNER JOIN ".$db["master"].".delivery_order ON
                    ".$db["master"].".delivery_order_details.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
                INNER JOIN ".$db["master"].".sales_order_details ON
                    ".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
                    AND ".$db["master"].".sales_order_details.finish_goods_id = ".$db["master"].".delivery_order_details.finish_goods_id
                INNER JOIN ".$db["master"].".finish_goods ON
                    ".$db["master"].".delivery_order_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
            WHERE
                ".$db["master"].".delivery_order_details.delivery_order_no = '".$do_no."'
            ORDER BY
                ".$db["master"].".delivery_order_details.sid ASC
    ";
    $qry = mysql_query($q);
    $no = 1;
    $subtotal = 0;
    $grand_total = 0;
    while($r = mysql_fetch_object($qry))
    {
        $subtotal = ($r->price-$r->disc_details) * $r->qty;
        
        $grand_total += $subtotal;
    }
    

    $ppn_value = 0;
    
    if($data["ppn"]=="Yes")
    {
        $ppn_value = $grand_total*0.1;
    }
    
    $grand_total = ($grand_total + $data["rounding"]+$ppn_value) - ($data["disc"]) ;
    
    return $grand_total;
}



function grandtotal_sales($id,$do_no)
{
    global $db; 
    
    $q = "
            SELECT 
                ".$db["master"].".sales_invoice.*,
                ".$db["master"].".sales_order.sales_order_no,
                ".$db["master"].".sales_order.dp_so,
                ".$db["master"].".customer.customer_name,
                dp_use
            FROM 
                ".$db["master"].".sales_invoice
                INNER JOIN ".$db["master"].".delivery_order ON
                    ".$db["master"].".delivery_order.delivery_order_no = ".$db["master"].".sales_invoice.delivery_order_no
                INNER JOIN ".$db["master"].".sales_order ON
                    ".$db["master"].".delivery_order.sales_order_no = ".$db["master"].".sales_order.sales_order_no
                INNER JOIN ".$db["master"].".customer ON
                    ".$db["master"].".customer.customer_id = ".$db["master"].".sales_order.customer_id
                LEFT JOIN
                (
                    SELECT
                        ".$db["master"].".delivery_order.sales_order_no,
                        SUM(".$db["master"].".sales_invoice.dp) as dp_use
                    FROM
                        ".$db["master"].".sales_invoice
                        INNER JOIN ".$db["master"].".delivery_order ON
                            ".$db["master"].".sales_invoice.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
                    GROUP BY
                        ".$db["master"].".delivery_order.sales_order_no
                ) as tbl_dp_use ON
                    tbl_dp_use.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
            WHERE
                1
                AND ".$db["master"].".sales_invoice.sales_invoice_no = '".$id."'
            LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
            SELECT
                ".$db["master"].".delivery_order_details.qty,
                ".$db["master"].".sales_order_details.price,
                ".$db["master"].".sales_order_details.disc_details,
                ".$db["master"].".finish_goods.finish_goods_name,
                ".$db["master"].".finish_goods.colour,
                ".$db["master"].".finish_goods.type,
                ".$db["master"].".finish_goods.material
            FROM
                ".$db["master"].".delivery_order_details
                INNER JOIN ".$db["master"].".delivery_order ON
                    ".$db["master"].".delivery_order_details.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
                INNER JOIN ".$db["master"].".sales_order_details ON
                    ".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
                    AND ".$db["master"].".sales_order_details.finish_goods_id = ".$db["master"].".delivery_order_details.finish_goods_id
                INNER JOIN ".$db["master"].".finish_goods ON
                    ".$db["master"].".delivery_order_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
            WHERE
                ".$db["master"].".delivery_order_details.delivery_order_no = '".$do_no."'
            ORDER BY
                ".$db["master"].".delivery_order_details.sid ASC
    ";
    $qry = mysql_query($q);
    $no = 1;
    $subtotal = 0;
    $grand_total = 0;
    while($r = mysql_fetch_object($qry))
    {
        $subtotal = ($r->price-$r->disc_details) * $r->qty;
        
        $grand_total += $subtotal;
    }
    

    $ppn_value = 0;
    
    if($data["ppn"]=="Yes")
    {
        $ppn_value = $grand_total*0.1;
    }
    
    $grand_total = ($grand_total + $data["rounding"]+$ppn_value) - ($data["disc"]+$data["dp"]) ;
    
    return $grand_total;
}

function grandtotal_purchase($id,$receive_no)
{
    global $db; 
    
    $q = "
            SELECT 
                ".$db["master"].".purchase_invoice.*,
                ".$db["master"].".purchase_order.purchase_order_no,
                ".$db["master"].".purchase_order.dp_po,
                ".$db["master"].".supplier.supplier_name,
                dp_use
            FROM 
                ".$db["master"].".purchase_invoice
                INNER JOIN ".$db["master"].".receive ON
                    ".$db["master"].".receive.receive_no = ".$db["master"].".purchase_invoice.receive_no
                INNER JOIN ".$db["master"].".purchase_order ON
                    ".$db["master"].".receive.purchase_order_no = ".$db["master"].".purchase_order.purchase_order_no
                INNER JOIN ".$db["master"].".supplier ON
                    ".$db["master"].".supplier.supplier_id = ".$db["master"].".purchase_order.supplier_id
                LEFT JOIN
                (
                    SELECT
                        ".$db["master"].".receive.purchase_order_no,
                        SUM(".$db["master"].".purchase_invoice.dp) as dp_use
                    FROM
                        ".$db["master"].".purchase_invoice
                        INNER JOIN ".$db["master"].".receive ON
                            ".$db["master"].".purchase_invoice.receive_no = ".$db["master"].".receive.receive_no
                    GROUP BY
                        ".$db["master"].".receive.purchase_order_no
                ) as tbl_dp_use ON
                    tbl_dp_use.purchase_order_no = ".$db["master"].".receive.purchase_order_no
            WHERE
                1
                AND ".$db["master"].".purchase_invoice.purchase_invoice_no = '".$id."'
            LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
            SELECT
                ".$db["master"].".receive_details.qty,
                ".$db["master"].".purchase_order_details.price,
                ".$db["master"].".purchase_order_details.disc_details,
                ".$db["master"].".material_goods.material_goods_name,
                ".$db["master"].".material_goods.colour,
                ".$db["master"].".material_goods.type,
                ".$db["master"].".material_goods.material
            FROM
                ".$db["master"].".receive_details
                INNER JOIN ".$db["master"].".receive ON
                    ".$db["master"].".receive_details.receive_no = ".$db["master"].".receive.receive_no
                INNER JOIN ".$db["master"].".purchase_order_details ON
                    ".$db["master"].".purchase_order_details.purchase_order_no = ".$db["master"].".receive.purchase_order_no
                    AND ".$db["master"].".purchase_order_details.material_goods_id = ".$db["master"].".receive_details.material_goods_id
                INNER JOIN ".$db["master"].".material_goods ON
                    ".$db["master"].".receive_details.material_goods_id = ".$db["master"].".material_goods.material_goods_id
            WHERE
                ".$db["master"].".receive_details.receive_no = '".$receive_no."'
            ORDER BY
                ".$db["master"].".receive_details.sid ASC
    ";
    $qry = mysql_query($q);
    $no = 1;
    $subtotal = 0;
    $grand_total = 0;
    while($r = mysql_fetch_object($qry))
    {
        $subtotal = ($r->price-$r->disc_details) * $r->qty;
        
        $grand_total += $subtotal;
    }
    

    $ppn_value = 0;
    
    if($data["ppn"]=="Yes")
    {
        $ppn_value = $grand_total*0.1;
    }
    
    $grand_total = ($grand_total + $data["rounding"]+$ppn_value) - ($data["disc"]+$data["dp"]) ;
    
    return $grand_total;
}

function grandtotal_pr($pr_no)
{
    global $db;
    
    $q = "
            SELECT
                SUM(".$db["master"].".pr_details.qty * ".$db["master"].".material_goods.price) as amount
            FROM
                ".$db["master"].".pr_details
                INNER JOIN ".$db["master"].".material_goods ON
                    ".$db["master"].".pr_details.material_goods_id = ".$db["master"].".material_goods.material_goods_id
            WHERE
                1
                AND ".$db["master"].".pr_details.pr_no = '".$pr_no."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["amount"];
}

function grandtotal_production($pr_no,$reff_type,$reff_no)
{
    global $db;
    
	
	if($reff_type=="Sales Order")
	{
		$q = "
				SELECT
					SUM(".$db["master"].".sales_order_details.qty * ".$db["master"].".finish_goods.price) as amount
				FROM
					".$db["master"].".sales_order_details
					INNER JOIN ".$db["master"].".finish_goods ON
						".$db["master"].".sales_order_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
				WHERE
					1
					AND ".$db["master"].".sales_order_details.sales_order_no = '".$reff_no."'
		";
		$qry = mysql_query($q);
		$row = mysql_fetch_array($qry);
	}
	else if($reff_type=="Planning")
	{
		$q = "
				SELECT
					SUM(".$db["master"].".planning_details.qty * ".$db["master"].".finish_goods.price) as amount
				FROM
					".$db["master"].".planning_details
					INNER JOIN ".$db["master"].".finish_goods ON
						".$db["master"].".planning_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
				WHERE
					1
					AND ".$db["master"].".planning_details.planning_no = '".$reff_no."'
		";
		$qry = mysql_query($q);
		$row = mysql_fetch_array($qry);
	}
	
	
    
    
    return $row["amount"];
}

function grandtotal_adj_pr($adj_pr_no)
{
    global $db;
    
    $q = "
            SELECT
                SUM(".$db["master"].".adj_pr_details.qty * ".$db["master"].".material_goods.price) as amount
            FROM
                ".$db["master"].".adj_pr_details
                INNER JOIN ".$db["master"].".material_goods ON
                    ".$db["master"].".adj_pr_details.material_goods_id = ".$db["master"].".material_goods.material_goods_id
            WHERE
                1
                AND ".$db["master"].".adj_pr_details.adj_pr_no = '".$adj_pr_no."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["amount"];
}

function grandtotal_stock_adj_material($stock_adj_material_no)
{
    global $db;
    
    $q = "
            SELECT
                SUM(".$db["master"].".stock_adj_material_details.qty * ".$db["master"].".material_goods.price) as amount
            FROM
                ".$db["master"].".stock_adj_material_details
                INNER JOIN ".$db["master"].".material_goods ON
                    ".$db["master"].".stock_adj_material_details.material_goods_id = ".$db["master"].".material_goods.material_goods_id
            WHERE
                1
                AND ".$db["master"].".stock_adj_material_details.stock_adj_material_no = '".$stock_adj_material_no."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["amount"];
}

function grandtotal_stock_adj_finish($stock_adj_finish_no)
{
    global $db;
    
    $q = "
            SELECT
                SUM(".$db["master"].".stock_adj_finish_details.qty * ".$db["master"].".finish_goods.price) as amount
            FROM
                ".$db["master"].".stock_adj_finish_details
                INNER JOIN ".$db["master"].".finish_goods ON
                    ".$db["master"].".stock_adj_finish_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
            WHERE
                1
                AND ".$db["master"].".stock_adj_finish_details.stock_adj_finish_no = '".$stock_adj_finish_no."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row["amount"];
}


function diff_time()
{
    $diff_time = 0;
    
    return $diff_time;
}

function ip_address()
{
    $return = $_SERVER['REMOTE_ADDR'];
    
    return $return;
}

function get_counter_klik($pages)
{
    global $db;
    
    if(isset($_SESSION["ses_login"])!="")
    {
        $pengunjung = $_SESSION["ses_login"];
    }
    else
    {
        $pengunjung = "guest";    
    }
    
    $q = "
            INSERT INTO
                ".$db["master"].".counter_klik
            SET
                pages = '".base_url().$pages."',
                ip_address = '".ip_address()."',
                post_date = '".date_now("Y-m-d H:i:s")."',
                visitor = '".$pengunjung."'
    "; 
    mysql_query($q);
}

function url_self()
{
    $return = $_SERVER['REQUEST_URI'];
    return $return;
}

function hak_akses($username,$menu_id)
{
    global $db;
    
    $q = "
            SELECT
                COUNT(".$db["master"].".menu_group.menu_group_id) as jml_valid
            FROM
                ".$db["master"].".menu_group
				INNER JOIN ".$db["master"].".group_user_details ON
					".$db["master"].".menu_group.group_user_id = ".$db["master"].".group_user_details.group_user_id
            WHERE
				1
                AND ".$db["master"].".menu_group.menu_id = '".$menu_id."'
                AND ".$db["master"].".group_user_details.username = '".$username."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($username!="admin")
    {
        if($row["jml_valid"]*1==0)
        {
            h2error("You do not have authorization");
            die();
        }
    }
}

function date_now($format)
{
    $now = time()+diff_time();
    
    $date_now = date($format,$now);
    
    return $date_now;
}

function ffclock()
{
    $return = $_SESSION['ses_login']." ".date_now("d/m/Y H:i:s");
    
    return $return;
}

function get_data_customer($customer_id)
{
    global $db;
    
    $q = "
            SELECT
                *
            FROM
                ".$db["master"].".customer
            WHERE
                customer_id = '".$customer_id."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
}

function get_data_supplier($supplier_id)
{
    global $db;
    
    $q = "
            SELECT
                *
            FROM
                ".$db["master"].".supplier
            WHERE
                supplier_id = '".$supplier_id."'
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
}

function get_stock($product_id,$v_date_from,$v_date_to)
{
    global $db; 
    //echo $product_id;
    
    $q= "
            SELECT
                SUM(".$db["master"].".purchase_order_details.qty_big) as qty_big,
                SUM(".$db["master"].".purchase_order_details.qty_small) as qty_small
            FROM
                ".$db["master"].".purchase_order_details
                INNER JOIN ".$db["master"].".purchase_order ON
                    ".$db["master"].".purchase_order_details.purchase_order_no = ".$db["master"].".purchase_order.purchase_order_no
            WHERE
                ".$db["master"].".purchase_order_details.product_id = '".$product_id."'
                AND ".$db["master"].".purchase_order.delivery_date < '".format_save_date($v_date_from)."'
    ";
    $qry_purchase_prev = mysql_query($q);
    $row_purchase_prev = mysql_fetch_array($qry_purchase_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".purchase_order_details.qty_big) as qty_big,
                SUM(".$db["master"].".purchase_order_details.qty_small) as qty_small
            FROM
                ".$db["master"].".purchase_order_details
                INNER JOIN ".$db["master"].".purchase_order ON
                    ".$db["master"].".purchase_order_details.purchase_order_no = ".$db["master"].".purchase_order.purchase_order_no
            WHERE
                ".$db["master"].".purchase_order_details.product_id = '".$product_id."'
                AND ".$db["master"].".purchase_order.delivery_date >= '".format_save_date($v_date_from)."' AND ".$db["master"].".purchase_order.delivery_date <= '".format_save_date($v_date_to)."'
    ";
    $qry_purchase = mysql_query($q);
    $row_purchase = mysql_fetch_array($qry_purchase);
    
    $q= "
            SELECT
                SUM(".$db["master"].".sales_order_details.qty_big) as qty_big,
                SUM(".$db["master"].".sales_order_details.qty_small) as qty_small
            FROM
                ".$db["master"].".sales_order_details
                INNER JOIN ".$db["master"].".sales_order ON
                    ".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".sales_order.sales_order_no
            WHERE
                ".$db["master"].".sales_order_details.product_id = '".$product_id."'
                AND ".$db["master"].".sales_order.delivery_date >= '".format_save_date($v_date_from)."' AND ".$db["master"].".sales_order.delivery_date <= '".format_save_date($v_date_to)."'
    ";
    //echo "<hr>";
    $qry_sales = mysql_query($q);
    $row_sales = mysql_fetch_array($qry_sales);
    
    $q= "
            SELECT
                SUM(".$db["master"].".sales_order_details.qty_big) as qty_big,
                SUM(".$db["master"].".sales_order_details.qty_small) as qty_small
            FROM
                ".$db["master"].".sales_order_details
                INNER JOIN ".$db["master"].".sales_order ON
                    ".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".sales_order.sales_order_no
            WHERE
                ".$db["master"].".sales_order_details.product_id = '".$product_id."'
                AND ".$db["master"].".sales_order.delivery_date < '".format_save_date($v_date_from)."'
    ";
    $qry_sales_prev = mysql_query($q);
    $row_sales_prev = mysql_fetch_array($qry_sales_prev);
    
    $q= "
            SELECT
                SUM(".$db["master"].".stock_adj_details.qty_big) as qty_big,
                SUM(".$db["master"].".stock_adj_details.qty_small) as qty_small
            FROM
                ".$db["master"].".stock_adj_details
                INNER JOIN ".$db["master"].".stock_adj ON
                    ".$db["master"].".stock_adj_details.stock_adj_no = ".$db["master"].".stock_adj.stock_adj_no
            WHERE
                ".$db["master"].".stock_adj_details.product_id = '".$product_id."'
    ";
    $qry_stock_adj = mysql_query($q);
    $row_stock_adj = mysql_fetch_array($qry_stock_adj);
    
    $begining_big = ($row_stock_adj["qty_big"] + $row_purchase_prev["qty_big"]) - $row_sales_prev["qty_big"];
    $begining_small = ($row_stock_adj["qty_small"] + $row_purchase_prev["qty_small"]) - $row_sales_prev["qty_small"];
    
    $purchase_big = $row_purchase["qty_big"];
    $purchase_small = $row_purchase["qty_small"];
    
    $sales_big = $row_sales["qty_big"];
    $sales_small = $row_sales["qty_small"];
    
    $ending_big =  ($row_stock_adj["qty_big"] + $row_purchase_prev["qty_big"] + $purchase_big) - ($row_sales_prev["qty_big"] + $sales_big);
    $ending_small =  ($row_stock_adj["qty_small"] + $row_purchase_prev["qty_small"] + $purchase_small) - ($row_sales_prev["qty_small"] + $sales_small);
    
    $return["begining_big"] = $begining_big;
    $return["begining_small"] = $begining_small;
    
    $return["purchase_big"] = $purchase_big;
    $return["purchase_small"] = $purchase_small;
    
    $return["sales_big"] = $sales_big;
    $return["sales_small"] = $sales_small;
    
    $return["ending_big"] = $ending_big;
    $return["ending_small"] = $ending_small;
    
    return $return; 
}

function grandtotal_ar($id,$do_no)
{
	global $db; 
	
	$q = "
			SELECT 
				".$db["master"].".sales_invoice.*,
				".$db["master"].".sales_order.sales_order_no,
				".$db["master"].".sales_order.dp_so,
				".$db["master"].".customer.customer_name,
				dp_use
			FROM 
				".$db["master"].".sales_invoice
				INNER JOIN ".$db["master"].".delivery_order ON
					".$db["master"].".delivery_order.delivery_order_no = ".$db["master"].".sales_invoice.delivery_order_no
				INNER JOIN ".$db["master"].".sales_order ON
					".$db["master"].".delivery_order.sales_order_no = ".$db["master"].".sales_order.sales_order_no
				INNER JOIN ".$db["master"].".customer ON
					".$db["master"].".customer.customer_id = ".$db["master"].".sales_order.customer_id
				LEFT JOIN
				(
					SELECT
						".$db["master"].".delivery_order.sales_order_no,
						SUM(".$db["master"].".sales_invoice.dp) as dp_use
					FROM
						".$db["master"].".sales_invoice
						INNER JOIN ".$db["master"].".delivery_order ON
							".$db["master"].".sales_invoice.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
					GROUP BY
						".$db["master"].".delivery_order.sales_order_no
				) as tbl_dp_use ON
					tbl_dp_use.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
			WHERE
				1
				AND ".$db["master"].".sales_invoice.sales_invoice_no = '".$id."'
			LIMIT 0,1
	";
	$qry["data"] = mysql_query($q);
	$data = mysql_fetch_array($qry["data"]);
	
	$q = "
			SELECT
				".$db["master"].".delivery_order_details.qty,
				".$db["master"].".sales_order_details.price,
				".$db["master"].".sales_order_details.disc_details,
				".$db["master"].".finish_goods.finish_goods_name,
				".$db["master"].".finish_goods.colour,
				".$db["master"].".finish_goods.type,
				".$db["master"].".finish_goods.material
			FROM
				".$db["master"].".delivery_order_details
				INNER JOIN ".$db["master"].".delivery_order ON
					".$db["master"].".delivery_order_details.delivery_order_no = ".$db["master"].".delivery_order.delivery_order_no
				INNER JOIN ".$db["master"].".sales_order_details ON
					".$db["master"].".sales_order_details.sales_order_no = ".$db["master"].".delivery_order.sales_order_no
					AND ".$db["master"].".sales_order_details.finish_goods_id = ".$db["master"].".delivery_order_details.finish_goods_id
				INNER JOIN ".$db["master"].".finish_goods ON
					".$db["master"].".delivery_order_details.finish_goods_id = ".$db["master"].".finish_goods.finish_goods_id
			WHERE
				".$db["master"].".delivery_order_details.delivery_order_no = '".$do_no."'
			ORDER BY
				".$db["master"].".delivery_order_details.sid ASC
	";
	$qry = mysql_query($q);
	$no = 1;
	$subtotal = 0;
	$grand_total = 0;
	while($r = mysql_fetch_object($qry))
	{
		$subtotal = ($r->price-$r->disc_details) * $r->qty;
		
		$grand_total += $subtotal;
	}
	

	$ppn_value = 0;
	
	if($data["ppn"]=="Yes")
	{
		$ppn_value = $grand_total*0.1;
	}
	
	$grand_total = ($grand_total + $data["rounding"]+$ppn_value) - ($data["disc"]+$data["dp"]) ;
	
	return $grand_total;
}

function grandtotal_ap($id,$receive_no)
{
	global $db; 
	
	$q = "
			SELECT 
				".$db["master"].".purchase_invoice.*,
				".$db["master"].".purchase_order.purchase_order_no,
				".$db["master"].".purchase_order.dp_po,
				".$db["master"].".supplier.supplier_name,
				dp_use
			FROM 
				".$db["master"].".purchase_invoice
				INNER JOIN ".$db["master"].".receive ON
					".$db["master"].".receive.receive_no = ".$db["master"].".purchase_invoice.receive_no
				INNER JOIN ".$db["master"].".purchase_order ON
					".$db["master"].".receive.purchase_order_no = ".$db["master"].".purchase_order.purchase_order_no
				INNER JOIN ".$db["master"].".supplier ON
					".$db["master"].".supplier.supplier_id = ".$db["master"].".purchase_order.supplier_id
				LEFT JOIN
				(
					SELECT
						".$db["master"].".receive.purchase_order_no,
						SUM(".$db["master"].".purchase_invoice.dp) as dp_use
					FROM
						".$db["master"].".purchase_invoice
						INNER JOIN ".$db["master"].".receive ON
							".$db["master"].".purchase_invoice.receive_no = ".$db["master"].".receive.receive_no
					GROUP BY
						".$db["master"].".receive.purchase_order_no
				) as tbl_dp_use ON
					tbl_dp_use.purchase_order_no = ".$db["master"].".receive.purchase_order_no
			WHERE
				1
				AND ".$db["master"].".purchase_invoice.purchase_invoice_no = '".$id."'
			LIMIT 0,1
	";
	$qry["data"] = mysql_query($q);
	$data = mysql_fetch_array($qry["data"]);
	
	$q = "
			SELECT
				".$db["master"].".receive_details.qty,
				".$db["master"].".purchase_order_details.price,
				".$db["master"].".purchase_order_details.disc_details,
				".$db["master"].".material_goods.material_goods_name,
				".$db["master"].".material_goods.colour,
				".$db["master"].".material_goods.type,
				".$db["master"].".material_goods.material
			FROM
				".$db["master"].".receive_details
				INNER JOIN ".$db["master"].".receive ON
					".$db["master"].".receive_details.receive_no = ".$db["master"].".receive.receive_no
				INNER JOIN ".$db["master"].".purchase_order_details ON
					".$db["master"].".purchase_order_details.purchase_order_no = ".$db["master"].".receive.purchase_order_no
					AND ".$db["master"].".purchase_order_details.material_goods_id = ".$db["master"].".receive_details.material_goods_id
				INNER JOIN ".$db["master"].".material_goods ON
					".$db["master"].".receive_details.material_goods_id = ".$db["master"].".material_goods.material_goods_id
			WHERE
				".$db["master"].".receive_details.receive_no = '".$receive_no."'
			ORDER BY
				".$db["master"].".receive_details.sid ASC
	";
	$qry = mysql_query($q);
	$no = 1;
	$subtotal = 0;
	$grand_total = 0;
	while($r = mysql_fetch_object($qry))
	{
		$subtotal = ($r->price-$r->disc_details) * $r->qty;
		
		$grand_total += $subtotal;
	}
	

	$ppn_value = 0;
	
	if($data["ppn"]=="Yes")
	{
		$ppn_value = $grand_total*0.1;
	}
	
	$grand_total = ($grand_total + $data["rounding"]+$ppn_value) - ($data["disc"]+$data["dp"]) ;
	
	return $grand_total;
}



    
    
?>