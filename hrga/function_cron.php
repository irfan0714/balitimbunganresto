<?php
function cron_alert_perjanjian($type, $day)
{
    // 1st reminder, 2nd reminder, final reminder
    // H-45, H-30, H-14
    $h_45 = date("Y-m-d", parsedate($day)+(86400*45));
    $h_30 = date("Y-m-d", parsedate($day)+(86400*30));
    $h_14  = date("Y-m-d", parsedate($day)+(86400*14));
    
    if($type=="1")
    {
        $day_exec = $h_45;
        $judul = "1st reminder";
    }
    else if($type=="2")
    {
        $day_exec = $h_30;
        $judul = "2nd reminder";
    }
    else if($type=="3")
    {
        $day_exec = $h_14;
        $judul = "Final reminder";
    }
    
    unset($arr_data);

    $q = "
            SELECT
                uni.type_data,
                uni.employee_id,
                uni.start_date,
                uni.end_date,
                uni.subject,
                uni.remarks
            FROM(  
                SELECT 
                    'Perjanjian' AS type_data,
                    hrd.`employee_agreement`.employee_id,
                    hrd.`employee_agreement`.start_date,
                    hrd.`employee_agreement`.end_date,
                    hrd.`employee_agreement`.subject,
                    hrd.`employee_agreement`.`remarks`
                FROM
                    hrd.`employee_agreement`
                WHERE
                    1
                    AND hrd.`employee_agreement`.end_date = '".$day_exec."'

                UNION ALL

                SELECT 
                    'Kontrak' AS type_data,
                    hrd.`employee_contract`.employee_id,
                    hrd.`employee_contract`.start_date,
                    hrd.`employee_contract`.end_date,
                    hrd.`employee_contract`.subject,
                    hrd.`employee_contract`.`remarks`
                FROM
                    hrd.`employee_contract`
                WHERE
                    1
                    AND hrd.`employee_contract`.end_date = '".$day_exec."'
            )AS uni
            WHERE
                1
            ORDER BY
                uni.end_date ASC
             
    "; 
    $ada_data = false;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($type_data, $employee_id, $start_date, $end_date, $subject, $remarks) = $row;
        
        $arr_data["list_employee"][$employee_id] = $employee_id;
        $arr_data["list_data"][$end_date] = $end_date;
        
        $arr_data["type_data"][$end_date][$employee_id] = $type_data;
        $arr_data["start_date"][$end_date][$employee_id] = $start_date;
        $arr_data["end_date"][$end_date][$employee_id] = $end_date;
        $arr_data["subject"][$end_date][$employee_id] = $subject;
        $arr_data["remarks"][$end_date][$employee_id] = $remarks;
        $ada_data = true;
    }
    
    // Status join dan resign
    {
        $q="
            SELECT 
              tbl_fixed.employee_id,
              tbl_fixed.v_status,
              tbl_fixed.v_date 
            FROM
              (SELECT 
                uni.employee_id,
                uni.v_status,
                uni.v_date 
              FROM
                (SELECT 
                  hrd.employee_join.employee_id,
                  'join' AS v_status,
                  hrd.employee_join.join_date AS v_date 
                FROM
                  hrd.employee_join 
                WHERE 1 
                UNION
                ALL 
                SELECT 
                  hrd.employee_resign.employee_id,
                  'resign' AS v_status,
                  hrd.employee_resign.resign_date AS v_date 
                FROM
                  hrd.employee_resign 
                WHERE 1) AS uni 
              WHERE 1 
              GROUP BY uni.employee_id,
                uni.v_date 
              ORDER BY uni.employee_id DESC,
                uni.v_date DESC) AS tbl_fixed 
            WHERE 1 
            GROUP BY tbl_fixed.employee_id
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($employee_id, $v_status, $v_date) = $row;
            
            $arr_data["list_fixed"][$employee_id] = $employee_id;
            $arr_data["fixed_status"][$employee_id] = $v_status;
            $arr_data["fixed_date"][$employee_id] = $v_date;
        }
    }
    
    $where_employee_id = "";
    $where_employee_id_name = "";
    foreach($arr_data["list_employee"] as $employee_id=>$val)
    {
        if($arr_data["list_fixed"][$employee_id]==$employee_id)
        {
            $v_status = $arr_data["fixed_status"][$employee_id];
            if($v_status=="join")
            {
                if($where_employee_id == "")
                {
                    $where_employee_id .= " AND (hrd.employee_position.employee_id = '".$val."' ";    
                    $where_employee_id_name .= " AND (hrd.employee.employee_id = '".$val."' ";    
                }    
                else
                {
                    $where_employee_id .= " OR hrd.employee_position.employee_id = '".$val."' ";    
                    $where_employee_id_name .= " OR hrd.employee.employee_id = '".$val."' ";    
                }
            }
        }
    }
    
    if($where_employee_id)
    {
        $where_employee_id .= ")";
        $where_employee_id_name .= ")";
        
        $q = "
                SELECT
                  uni.employee_id,
                  uni.company_id,
                  uni.company_name,
                  uni.company_initial,
                  uni.cabang_id,
                  uni.cabang_name,
                  uni.depo_id,
                  uni.depo_name,
                  uni.divisi_id,
                  uni.divisi_name,
                  uni.departemen_id,
                  uni.departemen_name,
                  uni.jabatan_id,
                  uni.jabatan_name
                FROM
                (
                    SELECT 
                      hrd.employee_position.employee_id,
                      hrd.company.company_id,
                      hrd.company.company_name,
                      hrd.company.company_initial,
                      hrd.cabang.cabang_id,
                      hrd.cabang.cabang_name,
                      hrd.depo.depo_id,
                      hrd.depo.depo_name,
                      hrd.divisi.divisi_id,
                      hrd.divisi.divisi_name,
                      hrd.departemen.departemen_id,
                      hrd.departemen.departemen_name,
                      hrd.jabatan.jabatan_id,
                      hrd.jabatan.jabatan_name 
                    FROM
                      hrd.employee_position 
                      INNER JOIN hrd.company 
                        ON hrd.employee_position.company_id = hrd.company.company_id 
                      INNER JOIN hrd.divisi 
                        ON hrd.employee_position.divisi_id = hrd.divisi.divisi_id 
                      INNER JOIN hrd.departemen 
                        ON hrd.employee_position.departemen_id = hrd.departemen.departemen_id 
                      INNER JOIN hrd.depo 
                        ON hrd.employee_position.depo_id = hrd.depo.depo_id 
                      INNER JOIN hrd.cabang 
                        ON hrd.depo.cabang_id = hrd.cabang.cabang_id 
                      INNER JOIN hrd.jabatan 
                        ON hrd.employee_position.jabatan_id = hrd.jabatan.jabatan_id 
                    WHERE 
                      1
                      ".$where_employee_id."
                    ORDER BY
                      hrd.employee_position.`start_date` DESC
                )AS uni
                WHERE
                    1
                GROUP BY
                    uni.employee_id 
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list(
              $employee_id,
              $company_id,
              $company_name,
              $company_initial,
              $cabang_id,
              $cabang_name,
              $depo_id,
              $depo_name,
              $divisi_id,
              $divisi_name,
              $departemen_id,
              $departemen_name,
              $jabatan_id,
              $jabatan_name
            ) = $row;    
            
            $arr_data["company_id"][$employee_id] = $company_id;
            $arr_data["company_name"][$employee_id] = $company_name;
            $arr_data["company_initial"][$employee_id] = $company_initial;
            $arr_data["cabang_id"][$employee_id] = $cabang_id;
            $arr_data["cabang_name"][$employee_id] = $cabang_name;
            $arr_data["depo_id"][$employee_id] = $depo_id;
            $arr_data["depo_name"][$employee_id] = $depo_name;
            $arr_data["divisi_id"][$employee_id] = $divisi_id;
            $arr_data["divisi_name"][$employee_id] = $divisi_name;
            $arr_data["departemen_id"][$employee_id] = $departemen_id;
            $arr_data["departemen_name"][$employee_id] = $departemen_name;
            $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
            $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
        }
        
        $q = "
                SELECT
                    hrd.employee.employee_id,
                    hrd.employee.employee_nik,
                    hrd.employee.employee_name
                FROM
                    hrd.employee
                WHERE
                    1
                    ".$where_employee_id_name."
                ORDER BY
                    hrd.employee.employee_id
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($employee_id, $employee_nik, $employee_name) = $row;
            
            $arr_data["employee_nik"][$employee_id] = $employee_nik;
            $arr_data["employee_name"][$employee_id] = $employee_name;
        }
    }
    
    
    $body = '
        <table align="center" class="in_table_data">
            '; 
                foreach($arr_data["list_data"] as $end_date=>$val)
                {
                    
            $body .= ' 
            <tr class="title_blue">
                <td colspan="100%">'.format_show_date($end_date).'</td>
            </tr>
            <tr class="title_blue" align="center">
                <td>Type</td>
                <td>Mulai</td>
                <td>Subjek</td>
                <td>Keterangan</td>
                <td>NIK</td>
                <td>Nama</td>
                <td>Perusahaan</td>
                <td>Cabang</td>
                <td>Divisi</td>
                <td>Jabatan</td>
            </tr>
            '; 
                foreach($arr_data["type_data"][$end_date] as $employee_id=>$val)
                {
                    
                    $type_data = $arr_data["type_data"][$end_date][$employee_id];
                    $start_date = $arr_data["start_date"][$end_date][$employee_id];
                    $subject = $arr_data["subject"][$end_date][$employee_id];
                    $remarks = $arr_data["remarks"][$end_date][$employee_id];
                    
                    $employee_nik = $arr_data["employee_nik"][$employee_id];
                    $employee_name = $arr_data["employee_name"][$employee_id];
                    
                    $company_id = $arr_data["company_id"][$employee_id];
                    $company_name = $arr_data["company_name"][$employee_id];
                    $company_initial = $arr_data["company_initial"][$employee_id];
                    $cabang_id = $arr_data["cabang_id"][$employee_id];
                    $cabang_name = $arr_data["cabang_name"][$employee_id];
                    $depo_id = $arr_data["depo_id"][$employee_id];
                    $depo_name = $arr_data["depo_name"][$employee_id];
                    $divisi_id = $arr_data["divisi_id"][$employee_id];
                    $divisi_name = $arr_data["divisi_name"][$employee_id];
                    $departemen_id = $arr_data["departemen_id"][$employee_id];
                    $departemen_name = $arr_data["departemen_name"][$employee_id];
                    $jabatan_id = $arr_data["jabatan_id"][$employee_id];
                    $jabatan_name = $arr_data["jabatan_name"][$employee_id];
                
                $body .= '
            
                    <tr>
                        <td>'.$type_data.'</td>
                        <td>'.format_show_date($start_date).'</td>
                        <td>'.$subject.'&nbsp;</td>
                        <td>'.$remarks.'&nbsp;</td>
                        <td>'.$employee_nik.'&nbsp;</td>
                        <td>'.$employee_name.'&nbsp;</td>
                        <td>'.$company_name.'</td>
                        <td>'.$cabang_name.'</td>
                        <td>'.$divisi_name.'</td>
                        <td>'.$jabatan_name.'</td>
                    </tr>
                    ';
                }
             $body .= '
             <tr>
                <td colspan="100%">&nbsp;</td>
             </tr>
            ';
                }
            $body .= '
        </table>
        <style>
            table{
                font-family:tahoma,sans-serif; 
                font-size:11px;
                color:#000000;    
            }

            .in_table {
                font-family:tahoma,sans-serif; 
                font-size:11px;  
            }

            .in_table_data {
                font-family:tahoma,sans-serif; 
                font-size:12px;  
            }

            .in_table_data td {
                border-bottom:1px solid #C1CDD8;
                border-right:1px solid #C1CDD8;
                
            }

            .title_blue
            {
                /* background:url(images/title_blue.png) repeat-x; */
                font-family:tahoma,sans-serif; 
                height: 20px;
                font-weight: bold;
                color: #15428b;
                padding-left: 5px;
                padding-right: 5px;
            } 
            
            .title_blue td
            {
                border-top:1px solid #C1CDD8;
                height:25px;
                background:#bad0ee;
                border: solid 1px #99bbe8;
            }
        </style> 
    ';
    
    $body = '
        <table align="center" border="1">
            '; 
                foreach($arr_data["list_data"] as $end_date=>$val)
                {
                    
            $body .= ' 
            <tr style="font-weight:bold;">
                <td colspan="10">'.format_show_date($end_date).'</td>
            </tr>
            <tr align="center" style="font-weight:bold;">
                <td>Type</td>
                <td>Mulai</td>
                <td>Subjek</td>
                <td>Keterangan</td>
                <td>NIK</td>
                <td>Nama</td>
                <td>Perusahaan</td>
                <td>Cabang</td>
                <td>Divisi</td>
                <td>Jabatan</td>
            </tr>
            '; 
                foreach($arr_data["type_data"][$end_date] as $employee_id=>$val)
                {
                    
                    $type_data = $arr_data["type_data"][$end_date][$employee_id];
                    $start_date = $arr_data["start_date"][$end_date][$employee_id];
                    $subject = $arr_data["subject"][$end_date][$employee_id];
                    $remarks = $arr_data["remarks"][$end_date][$employee_id];
                    
                    $employee_nik = $arr_data["employee_nik"][$employee_id];
                    $employee_name = $arr_data["employee_name"][$employee_id];
                    
                    $company_id = $arr_data["company_id"][$employee_id];
                    $company_name = $arr_data["company_name"][$employee_id];
                    $company_initial = $arr_data["company_initial"][$employee_id];
                    $cabang_id = $arr_data["cabang_id"][$employee_id];
                    $cabang_name = $arr_data["cabang_name"][$employee_id];
                    $depo_id = $arr_data["depo_id"][$employee_id];
                    $depo_name = $arr_data["depo_name"][$employee_id];
                    $divisi_id = $arr_data["divisi_id"][$employee_id];
                    $divisi_name = $arr_data["divisi_name"][$employee_id];
                    $departemen_id = $arr_data["departemen_id"][$employee_id];
                    $departemen_name = $arr_data["departemen_name"][$employee_id];
                    $jabatan_id = $arr_data["jabatan_id"][$employee_id];
                    $jabatan_name = $arr_data["jabatan_name"][$employee_id];
                
                $body .= '
            
                    <tr>
                        <td>'.$type_data.'</td>
                        <td>'.format_show_date($start_date).'</td>
                        <td>'.$subject.'&nbsp;</td>
                        <td>'.$remarks.'&nbsp;</td>
                        <td>'.$employee_nik.'&nbsp;</td>
                        <td>'.$employee_name.'&nbsp;</td>
                        <td>'.$company_name.'</td>
                        <td>'.$cabang_name.'</td>
                        <td>'.$divisi_name.'</td>
                        <td>'.$jabatan_name.'</td>
                    </tr>
                    ';
                }
             $body .= '
             <tr>
                <td colspan="10">&nbsp;</td>
             </tr>
            ';
                }
            $body .= '
        </table>
    ';
    
    
    if($ada_data)
    {    
        $q = "
                SELECT
                    hrd.function_email.email_address,
                    hrd.function_email.email_name
                FROM
                    hrd.function_email
                WHERE
                    1
                    AND hrd.function_email.func_name = 'alert_perjanjian'
                ORDER BY
                    hrd.function_email.email_address ASC
        ";
        $qry = mysql_query($q);
        
        $email_address_multi = "";
        $email_name_multi = "";
        
        while($row = mysql_fetch_array($qry))
        {
            list($email_address, $email_name) = $row;
            
            $email_address_multi .= $email_address.";";
            $email_name_multi .= $email_name.";";
        }
        
        //$email_address_multi = "";
        //$email_name_multi = "";
        
        //$email_address_multi .= "fky@vci.co.id;";
        //$email_name_multi .= "Frangky Kucrut;";
        
        //$email_address_multi .= "hnd@vci.co.id;";
        //$email_name_multi .= "Hendri;";
        
        send_email_multiple("Alert Perjanjian & Kontrak ".$judul." (".$day.")", $body, $email_address_multi, $email_name_multi, "Cron"); 
    }
}

function cron_alert_perjanjian_cabang($type, $day)
{
    $email_address_multi = "";
    $email_name_multi    = "";
    
    unset($arr_data);
    
    // 1st reminder, 2nd reminder, final reminder
    // H-45, H-30, H-14
    $h_45 = date("Y-m-d", parsedate($day)+(86400*45));
    $h_30 = date("Y-m-d", parsedate($day)+(86400*30));
    $h_14  = date("Y-m-d", parsedate($day)+(86400*14));
    
    if($type=="1")
    {
        $day_exec = $h_45;
        $judul = "1st reminder";
    }
    else if($type=="2")
    {
        $day_exec = $h_30;
        $judul = "2nd reminder";
    }
    else if($type=="3")
    {
        $day_exec = $h_14;
        $judul = "Final reminder";
    }
    
    unset($arr_data);

    $q = "
            SELECT
                uni.type_data,
                uni.employee_id,
                uni.start_date,
                uni.end_date,
                uni.subject,
                uni.remarks
            FROM(
                
                SELECT 
                    'Perjanjian' AS type_data,
                    hrd.`employee_agreement`.employee_id,
                    hrd.`employee_agreement`.start_date,
                    hrd.`employee_agreement`.end_date,
                    hrd.`employee_agreement`.subject,
                    hrd.`employee_agreement`.`remarks`
                FROM
                    hrd.`employee_agreement`
                WHERE
                    1
                    AND hrd.`employee_agreement`.end_date = '".$day_exec."'

                UNION ALL

                SELECT 
                    'Kontrak' AS type_data,
                    hrd.`employee_contract`.employee_id,
                    hrd.`employee_contract`.start_date,
                    hrd.`employee_contract`.end_date,
                    hrd.`employee_contract`.subject,
                    hrd.`employee_contract`.`remarks`
                FROM
                    hrd.`employee_contract`
                WHERE
                    1
                    AND hrd.`employee_contract`.end_date = '".$day_exec."'
            )AS uni
            WHERE
                1
            ORDER BY
                uni.end_date ASC
             
    ";
    $ada_data = false;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($type_data, $employee_id, $start_date, $end_date, $subject, $remarks) = $row;
        
        $arr_data["list_employee"][$employee_id] = $employee_id;
        $arr_data["list_data"][$end_date] = $end_date;
        
        $arr_data["type_data"][$end_date][$employee_id] = $type_data;
        $arr_data["start_date"][$end_date][$employee_id] = $start_date;
        $arr_data["end_date"][$end_date][$employee_id] = $end_date;
        $arr_data["subject"][$end_date][$employee_id] = $subject;
        $arr_data["remarks"][$end_date][$employee_id] = $remarks;
        $ada_data = true;
    }
    
    $where_employee_id = "";
    $where_employee_id_name = "";
    foreach($arr_data["list_employee"] as $key=>$val)
    {
        if($where_employee_id == "")
        {
            $where_employee_id .= " AND (hrd.employee_position.employee_id = '".$val."' ";    
            $where_employee_id_name .= " AND (hrd.employee.employee_id = '".$val."' ";    
        }    
        else
        {
            $where_employee_id .= " OR hrd.employee_position.employee_id = '".$val."' ";    
            $where_employee_id_name .= " OR hrd.employee.employee_id = '".$val."' ";    
        }
    }
    
    if($where_employee_id)
    {
        $where_employee_id .= ")";
        $where_employee_id_name .= ")";
        
        $q = "
                SELECT
                  uni.employee_id,
                  uni.company_id,
                  uni.company_name,
                  uni.company_initial,
                  uni.cabang_id,
                  uni.cabang_name,
                  uni.depo_id,
                  uni.depo_name,
                  uni.divisi_id,
                  uni.divisi_name,
                  uni.departemen_id,
                  uni.departemen_name,
                  uni.jabatan_id,
                  uni.jabatan_name
                FROM
                (
                    SELECT 
                      hrd.employee_position.employee_id,
                      hrd.company.company_id,
                      hrd.company.company_name,
                      hrd.company.company_initial,
                      hrd.cabang.cabang_id,
                      hrd.cabang.cabang_name,
                      hrd.depo.depo_id,
                      hrd.depo.depo_name,
                      hrd.divisi.divisi_id,
                      hrd.divisi.divisi_name,
                      hrd.departemen.departemen_id,
                      hrd.departemen.departemen_name,
                      hrd.jabatan.jabatan_id,
                      hrd.jabatan.jabatan_name 
                    FROM
                      hrd.employee_position 
                      INNER JOIN hrd.company 
                        ON hrd.employee_position.company_id = hrd.company.company_id 
                      INNER JOIN hrd.divisi 
                        ON hrd.employee_position.divisi_id = hrd.divisi.divisi_id 
                      INNER JOIN hrd.departemen 
                        ON hrd.employee_position.departemen_id = hrd.departemen.departemen_id 
                      INNER JOIN hrd.depo 
                        ON hrd.employee_position.depo_id = hrd.depo.depo_id 
                      INNER JOIN hrd.cabang 
                        ON hrd.depo.cabang_id = hrd.cabang.cabang_id 
                      INNER JOIN hrd.jabatan 
                        ON hrd.employee_position.jabatan_id = hrd.jabatan.jabatan_id 
                    WHERE 
                      1
                      ".$where_employee_id."
                    ORDER BY
                      hrd.employee_position.`start_date` DESC
                )AS uni
                WHERE
                    1
                GROUP BY
                    uni.employee_id 
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list(
              $employee_id,
              $company_id,
              $company_name,
              $company_initial,
              $cabang_id,
              $cabang_name,
              $depo_id,
              $depo_name,
              $divisi_id,
              $divisi_name,
              $departemen_id,
              $departemen_name,
              $jabatan_id,
              $jabatan_name
            ) = $row;    
            
            $arr_data["company_id"][$employee_id] = $company_id;
            $arr_data["company_name"][$employee_id] = $company_name;
            $arr_data["company_initial"][$employee_id] = $company_initial;
            $arr_data["cabang_id"][$employee_id] = $cabang_id;
            $arr_data["cabang_name"][$employee_id] = $cabang_name;
            $arr_data["depo_id"][$employee_id] = $depo_id;
            $arr_data["depo_name"][$employee_id] = $depo_name;
            $arr_data["divisi_id"][$employee_id] = $divisi_id;
            $arr_data["divisi_name"][$employee_id] = $divisi_name;
            $arr_data["departemen_id"][$employee_id] = $departemen_id;
            $arr_data["departemen_name"][$employee_id] = $departemen_name;
            $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
            $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
        }
        
        $q = "
                SELECT
                    hrd.employee.employee_id,
                    hrd.employee.employee_nik,
                    hrd.employee.employee_name
                FROM
                    hrd.employee
                WHERE
                    1
                    ".$where_employee_id_name."
                ORDER BY
                    hrd.employee.employee_id
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($employee_id, $employee_nik, $employee_name) = $row;
            
            $arr_data["employee_nik"][$employee_id] = $employee_nik;
            $arr_data["employee_name"][$employee_id] = $employee_name;
        }
    }
    
    foreach($arr_data["list_data"] as $end_date=>$val)
    {
        foreach($arr_data["type_data"][$end_date] as $employee_id=>$val)
        {    
            $type_data = $arr_data["type_data"][$end_date][$employee_id];
            $start_date = $arr_data["start_date"][$end_date][$employee_id];
            $subject = $arr_data["subject"][$end_date][$employee_id];
            $remarks = $arr_data["remarks"][$end_date][$employee_id];
            
            $employee_nik = $arr_data["employee_nik"][$employee_id];
            $employee_name = $arr_data["employee_name"][$employee_id];
            
            $company_id = $arr_data["company_id"][$employee_id];
            $company_name = $arr_data["company_name"][$employee_id];
            $company_initial = $arr_data["company_initial"][$employee_id];
            $cabang_id = $arr_data["cabang_id"][$employee_id];
            $cabang_name = $arr_data["cabang_name"][$employee_id];
            $depo_id = $arr_data["depo_id"][$employee_id];
            $depo_name = $arr_data["depo_name"][$employee_id];
            $divisi_id = $arr_data["divisi_id"][$employee_id];
            $divisi_name = $arr_data["divisi_name"][$employee_id];
            $departemen_id = $arr_data["departemen_id"][$employee_id];
            $departemen_name = $arr_data["departemen_name"][$employee_id];
            $jabatan_id = $arr_data["jabatan_id"][$employee_id];
            $jabatan_name = $arr_data["jabatan_name"][$employee_id];    
            
            $arr_data["data_cabang"][$cabang_id] = $cabang_id;
            $arr_data["per_cabang"][$cabang_id][$employee_id] = $employee_id;
            $arr_data["end_date"][$cabang_id][$employee_id]   = $end_date;
            
        }
    }
    
    if($ada_data)
    {
        $q = "
                SELECT
                    hrd.hrd_cabang.employee_id,
                    hrd.hrd_cabang.cabang_id,
                    hrd.employee.email,
                    hrd.employee.employee_name
                FROM
                    hrd.hrd_cabang
                    INNER JOIN hrd.employee ON
                        hrd.employee.employee_id = hrd.hrd_cabang.employee_id
                WHERE
                    1
                ORDER BY
                    hrd.hrd_cabang.cabang_id ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($employee_id, $cabang_id, $email, $employee_name) = $row;
            
            $arr_data["list_cabang"][$cabang_id] = $cabang_id;
            $arr_data["email_address"][$cabang_id] = $email;
            $arr_data["email_name"][$cabang_id] = $employee_name;
        }
           
        // Status join dan resign
        {
            $q="
                SELECT 
                  tbl_fixed.employee_id,
                  tbl_fixed.v_status,
                  tbl_fixed.v_date 
                FROM
                  (SELECT 
                    uni.employee_id,
                    uni.v_status,
                    uni.v_date 
                  FROM
                    (SELECT 
                      hrd.employee_join.employee_id,
                      'join' AS v_status,
                      hrd.employee_join.join_date AS v_date 
                    FROM
                      hrd.employee_join 
                    WHERE 1 
                    UNION
                    ALL 
                    SELECT 
                      hrd.employee_resign.employee_id,
                      'resign' AS v_status,
                      hrd.employee_resign.resign_date AS v_date 
                    FROM
                      hrd.employee_resign 
                    WHERE 1) AS uni 
                  WHERE 1 
                  GROUP BY uni.employee_id,
                    uni.v_date 
                  ORDER BY uni.employee_id DESC,
                    uni.v_date DESC) AS tbl_fixed 
                WHERE 1 
                GROUP BY tbl_fixed.employee_id
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($employee_id, $v_status, $v_date) = $row;
                
                $arr_data["list_fixed"][$employee_id] = $employee_id;
                $arr_data["fixed_status"][$employee_id] = $v_status;
                $arr_data["fixed_date"][$employee_id] = $v_date;
            }
        }
           
        foreach($arr_data["list_cabang"] as $cabang_id=>$val)
        {
            $email_address_cabang = $arr_data["email_address"][$cabang_id];
            $email_name_cabang    = $arr_data["email_name"][$cabang_id];
            
            // yang hanya ada isi
            if($cabang_id==$arr_data["data_cabang"][$cabang_id])
            {
                foreach($arr_data["per_cabang"][$cabang_id] as $employee_id=>$val)
                {
                    if($employee_id==$arr_data["list_fixed"][$employee_id])
                    {
                        $v_status = $arr_data["fixed_status"][$employee_id];
                        
                        if($v_status=="join")
                        {
                            $end_date = $arr_data["end_date"][$cabang_id][$employee_id];
                            
                            $type_data = $arr_data["type_data"][$end_date][$employee_id];
                            $start_date = $arr_data["start_date"][$end_date][$employee_id];
                            $subject = $arr_data["subject"][$end_date][$employee_id];
                            $remarks = $arr_data["remarks"][$end_date][$employee_id];
                            
                            $employee_nik = $arr_data["employee_nik"][$employee_id];
                            $employee_name = $arr_data["employee_name"][$employee_id];
                            
                            $company_id = $arr_data["company_id"][$employee_id];
                            $company_name = $arr_data["company_name"][$employee_id];
                            $company_initial = $arr_data["company_initial"][$employee_id];
                            $cabang_id = $arr_data["cabang_id"][$employee_id];
                            $cabang_name = $arr_data["cabang_name"][$employee_id];
                            $depo_id = $arr_data["depo_id"][$employee_id];
                            $depo_name = $arr_data["depo_name"][$employee_id];
                            $divisi_id = $arr_data["divisi_id"][$employee_id];
                            $divisi_name = $arr_data["divisi_name"][$employee_id];
                            $departemen_id = $arr_data["departemen_id"][$employee_id];
                            $departemen_name = $arr_data["departemen_name"][$employee_id];
                            $jabatan_id = $arr_data["jabatan_id"][$employee_id];
                            $jabatan_name = $arr_data["jabatan_name"][$employee_id];    

                            
                            $body = '
                                <table align="center" border="1">
                                    '; 
                                        
                                    $body .= ' 
                                    <tr style="font-weight:bold;">
                                        <td colspan="10">'.format_show_date($end_date).'</td>
                                    </tr>
                                    <tr style="font-weight:bold;" align="center">
                                        <td>Type</td>
                                        <td>Mulai</td>
                                        <td>Subjek</td>
                                        <td>Keterangan</td>
                                        <td>NIK</td>
                                        <td>Nama</td>
                                        <td>Perusahaan</td>
                                        <td>Cabang</td>
                                        <td>Divisi</td>
                                        <td>Jabatan</td>
                                    </tr>
                                    '; 

                                        $body .= '
                                    
                                            <tr>
                                                <td>'.$type_data.'</td>
                                                <td>'.format_show_date($start_date).'</td>
                                                <td>'.$subject.'&nbsp;</td>
                                                <td>'.$remarks.'&nbsp;</td>
                                                <td>'.$employee_nik.'&nbsp;</td>
                                                <td>'.$employee_name.'&nbsp;</td>
                                                <td>'.$company_name.'</td>
                                                <td>'.$cabang_name.'</td>
                                                <td>'.$divisi_name.'</td>
                                                <td>'.$jabatan_name.'</td>
                                            </tr>
                                            ';
                                       
                                     $body .= '
                                     <tr>
                                        <td colspan="10">&nbsp;</td>
                                     </tr>
                                    ';
                                       
                                    $body .= '
                                </table>
                                
                            ';
                            
                            //$email_address_multi .= "hndtmp@vci.co.id;";
                            //$email_name_multi .= "Hendri Temp;";
                            
                            //$email_address_multi .= "wsc@vci.co.id;";
                            //$email_name_multi .= "Wieok;";
                            
                            $q = "
                                    SELECT
                                        hrd.function_email.email_address,
                                        hrd.function_email.email_name
                                    FROM
                                        hrd.function_email
                                    WHERE
                                        1
                                        AND hrd.function_email.func_name = 'alert_perjanjian'
                                    ORDER BY
                                        hrd.function_email.email_address ASC
                            ";
                            $qry = mysql_query($q);
                            
                            $email_address_multi = "";
                            $email_name_multi = "";
                            
                            $email_address_multi .= $email_address_cabang.";";
                            $email_name_multi .= $email_name_cabang.";";
                            
                            while($row = mysql_fetch_array($qry))
                            {
                                list($email_address, $email_name) = $row;
                                
                                $email_address_multi .= $email_address.";";
                                $email_name_multi .= $email_name.";";
                            }
                            
                            send_email_multiple("Alert Perjanjian & Kontrak Cabang ".$judul." (".$day.")", $body, $email_address_multi, $email_name_multi, "Cron");     
                        }     
                    }  
                }    
            }
        }
        
    } // ada data
          
}

function reminder_daily($date_start, $date_end)
{
    $hour_save   = date("H");
    $minute      = date("i");

    if(
        $minute*1==1 || $minute*1==2 || $minute*1==3 || $minute*1==4 || $minute*1==5 
    )
    {
        $minute_save="05";
    }
    else if(
        $minute*1==6 || $minute*1==7 || $minute*1==8 || $minute*1==9 || $minute*1==10 
    )
    {
        $minute_save="10";
    }
    else if(
        $minute*1==11 || $minute*1==12 || $minute*1==13 || $minute*1==14 || $minute*1==15 
    )
    {
        $minute_save="15";
    }
    else if(
        $minute*1==16 || $minute*1==17 || $minute*1==18 || $minute*1==19 || $minute*1==20
    )
    {
        $minute_save="20";
    }
    else if(
        $minute*1==21 || $minute*1==22 || $minute*1==23 || $minute*1==24 || $minute*1==25
    )
    {
        $minute_save="25";
    }
    else if(
        $minute*1==26 || $minute*1==27 || $minute*1==28 || $minute*1==29 || $minute*1==30
    )
    {
        $minute_save="30";
    }
    else if(
        $minute*1==31 || $minute*1==32 || $minute*1==33 || $minute*1==34 || $minute*1==35
    )
    {
        $minute_save="35";
    }
    else if(
        $minute*1==36 || $minute*1==37 || $minute*1==38 || $minute*1==39 || $minute*1==40
    )
    {
        $minute_save=="40";
    }
    else if(
        $minute*1==41 || $minute*1==42 || $minute*1==43 || $minute*1==44 || $minute*1==45
    )
    {
        $minute_save="45";
    }
    else if(
        $minute*1==46 || $minute*1==47 || $minute*1==48 || $minute*1==49 || $minute*1==50
    )
    {
        $minute_save="50";
    }
    else if(
        $minute*1==51 || $minute*1==52 || $minute*1==53 || $minute*1==54 || $minute*1==55
    )
    {
        $minute_save="55";
    }
    else if(
        $minute*1==56 || $minute*1==57 || $minute*1==58 || $minute*1==59 || $minute*1==0
    )
    {
        $minute_save="00";
    }
    
    $time_save = $hour_save.":".$minute_save.":00"; 
    
    //echo $time_save."<hr>";
    
    $ada_data = false;
    
    for($i=parsedate($date_start);$i<=parsedate($date_end);$i=$i+86400)
    {  
        $hari = date("N", $i); // 1 (for Monday) through 7 (for Sunday)
     
        $q = "
        SELECT
            uni_global.repeatition_name,
            uni_global.reminder_name,
            uni_global.sid,
            uni_global.subject,
            uni_global.reminder_date,
            uni_global.date_alert,
            uni_global.reminder_time,
            uni_global.repeatition_id
        FROM
        (
                SELECT
                    'Sekali' AS repeatition_name,
                    uni.reminder_name,
                    uni.sid,
                    uni.subject,
                    uni.reminder_date,
                    uni.date_alert,
                    uni.reminder_time,
                    uni.repeatition_id
                FROM
                (
                    SELECT
                        '0 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        hrd.fixed_asset_reminder.reminder_date AS date_alert,    
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '10'
                    
                    UNION
                    
                    SELECT
                        '1 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '1'
                    
                    UNION
                    
                    SELECT
                        '2 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '2'
                    
                    UNION
                    
                    SELECT
                        '3 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '3'
                    
                    UNION
                    
                    SELECT
                        '7 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 7 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '4'
                    
                    UNION
                    
                    SELECT
                        '14 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 14 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '11'
                    
                    UNION
                    
                    SELECT
                        '1 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '5'
                    
                    UNION
                    
                    SELECT
                        '2 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '6'
                    
                    UNION
                    
                    SELECT
                        '3 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '7'
                ) AS uni
                WHERE
                    1
                    AND uni.repeatition_id = '1'
                    AND YEAR(uni.date_alert) = '".date("Y", $i)."'
                    AND MONTH(uni.date_alert) = '".date("m", $i)."'
                    AND DAY(uni.date_alert) = '".date("d", $i)."'
                
                UNION
                
                
                SELECT
                    'Setiap Minggu' AS repeatition_name,
                    uni.reminder_name,
                    uni.sid,
                    uni.subject,
                    uni.reminder_date,
                    uni.date_alert,
                    uni.reminder_time,
                    uni.repeatition_id
                FROM
                (
                    SELECT
                        '0 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        hrd.fixed_asset_reminder.reminder_date AS date_alert,    
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '10'
                    
                    UNION
                    
                    SELECT
                        '1 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '1'
                    
                    UNION
                    
                    SELECT
                        '2 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '2'
                    
                    UNION
                    
                    SELECT
                        '3 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '3'
                    
                    UNION
                    
                    SELECT
                        '7 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 7 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '4'
                    
                    UNION
                    
                    SELECT
                        '14 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 14 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '11'
                    
                    UNION
                    
                    SELECT
                        '1 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '5'
                    
                    UNION
                    
                    SELECT
                        '2 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '6'
                    
                    UNION
                    
                    SELECT
                        '3 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '7'
                ) AS uni
                WHERE
                    1
                    AND uni.repeatition_id = '3'
                    AND ((DATE_FORMAT(uni.date_alert, 'w')*1)+1) = '".$hari."'
                
                UNION    
                
                
                SELECT
                    'Setiap Bulan' AS repeatition_name,
                    uni.reminder_name,
                    uni.sid,
                    uni.subject,
                    uni.reminder_date,
                    uni.date_alert,
                    uni.reminder_time,
                    uni.repeatition_id
                FROM
                (
                    SELECT
                        '0 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        hrd.fixed_asset_reminder.reminder_date AS date_alert,    
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '10'
                    
                    UNION
                    
                    SELECT
                        '1 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '1'
                    
                    UNION
                    
                    SELECT
                        '2 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '2'
                    
                    UNION
                    
                    SELECT
                        '3 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '3'
                    
                    UNION
                    
                    SELECT
                        '7 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 7 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '4'
                    
                    UNION
                    
                    SELECT
                        '14 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 14 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '11'
                    
                    UNION
                    
                    
                    SELECT
                        '1 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '5'
                    
                    UNION
                    
                    SELECT
                        '2 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '6'
                    
                    UNION
                    
                    SELECT
                        '3 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '7'
                ) AS uni
                WHERE
                    1
                    AND uni.repeatition_id = '4'
                    AND DAY(uni.date_alert) = '".date("d", $i)."'
                
                UNION
                
                SELECT
                    'Setiap Tahun' AS repeatition_name,
                    uni.reminder_name,
                    uni.sid,
                    uni.subject,
                    uni.reminder_date,
                    uni.date_alert,
                    uni.reminder_time,
                    uni.repeatition_id
                FROM
                (
                    SELECT
                        '0 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        hrd.fixed_asset_reminder.reminder_date AS date_alert,    
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '10'
                    
                    UNION
                    
                    SELECT
                        '1 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '1'
                    
                    UNION
                    
                    SELECT
                        '2 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '2'
                    
                    UNION
                    
                    SELECT
                        '3 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '3'
                    
                    UNION
                    
                    SELECT
                        '7 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 7 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '4'
                    
                    UNION
                    
                    SELECT
                        '14 Hari' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 14 DAY) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '11'
                    
                    UNION
                    
                    SELECT
                        '1 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 1 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '5'
                    
                    UNION
                    
                    SELECT
                        '2 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 2 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '6'
                    
                    UNION
                    
                    SELECT
                        '3 Bulan' as reminder_name,
                        hrd.fixed_asset_reminder.sid,
                        hrd.fixed_asset_reminder.subject,
                        hrd.fixed_asset_reminder.reminder_date,    
                        DATE_SUB(hrd.fixed_asset_reminder.reminder_date, INTERVAL 3 MONTH) AS date_alert,
                        hrd.fixed_asset_reminder.reminder_time,
                        hrd.fixed_asset_reminder.repeatition_id
                    FROM
                        hrd.fixed_asset_reminder
                    WHERE
                        1
                        AND hrd.fixed_asset_reminder.reminder_id = '7'
                ) AS uni
                WHERE
                    1
                    AND uni.repeatition_id = '5'
                    AND DAY(uni.date_alert) = '".date("d", $i)."'
                    AND MONTH(uni.date_alert) = '".date("m", $i)."'
        )AS uni_global
        WHERE
            1
            AND uni_global.reminder_time = '".$time_save."'
        ORDER BY
            uni_global.date_alert ASC,
            uni_global.reminder_date ASC
        ";
       
        $counter = 0;
/*        echo "<pre>";
        echo $q;
        echo "</pre>";
        die();*/
        $data_echo = "";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($repeatition_name, $reminder_name, $sid, $subject, $reminder_date, $date_alert, $reminder_time, $repeatition_id) = $row;
            
            $ada_data = true;
            
            //echo date("d-m-Y", $i)."(".date("N", $i).")";
            //echo "<br>";
            //echo $repeatition_name.", ".$reminder_name.", ".$sid.", ".$subject.", ".$reminder_date.", ".$date_alert.", ".$reminder_time.", ".$repeatition_id;
            //echo "<br>";
            
            $arr_data["list_sid"][$sid] = $sid;
            
            $arr_data["list_data"][date("Y-m-d", $i)][$counter] = $counter;
            
            $arr_data["repeatition_name"][date("Y-m-d", $i)][$counter] = $repeatition_name;
            $arr_data["reminder_name"][date("Y-m-d", $i)][$counter] = $reminder_name;
            $arr_data["sid"][date("Y-m-d", $i)][$counter] = $sid;
            $arr_data["subject"][date("Y-m-d", $i)][$counter] = $subject;
            $arr_data["reminder_date"][date("Y-m-d", $i)][$counter] = $reminder_date;
            $arr_data["date_alert"][date("Y-m-d", $i)][$counter] = $date_alert;
            $arr_data["reminder_time"][date("Y-m-d", $i)][$counter] = $reminder_time;
            $arr_data["repeatition_id"][date("Y-m-d", $i)][$counter] = $repeatition_id;
            $counter++;
        }
    }
    
    $where_sid = where_array($arr_data["list_sid"], "hrd.fixed_asset_reminder.sid", $type="in");
    
    $q = "
    	SELECT 
		  fixed_asset_reminder.sid,
		  cabang.cabang_name,
		  depo.depo_name,
		  fa_fixed_asset.fixed_asset_id,
		  fa_fixed_asset.fixed_asset_code,
		  fa_fixed_asset.desc_kendaraan_nopol,
		  fa_fixed_asset.desc_kendaraan_merek,
		  fa_fixed_asset.desc_kendaraan_jenis,
		  fa_fixed_asset.desc_kendaraan_cc,
		  fa_fixed_asset.desc_kendaraan_warna,
		  fa_fixed_asset.desc_kendaraan_no_rangka,
		  fa_fixed_asset.desc_kendaraan_no_mesin,
		  tb_pj.employee_id,
		  employee.employee_name,
		  employee.gender,
		  employee.email,
		  IF(tabel_risk.ket_risk = 'Y', 'Y', 'N') AS ket_risk 
		FROM
		  hrd.fa_fixed_asset 
		  INNER JOIN hrd.fixed_asset_reminder 
		    ON hrd.fa_fixed_asset.fixed_asset_id = hrd.fixed_asset_reminder.fixed_asset_id
		  ".$where_sid."  
		  INNER JOIN 
		    (SELECT 
		      unu.* 
		    FROM
		      (SELECT 
		        hrd.fa_lokasi.sid AS sid_lokasi,
		        hrd.fa_lokasi.fixed_asset_id,
		        hrd.fa_lokasi.lokasi_date,
		        hrd.fa_lokasi.depo_id,
		        hrd.fa_lokasi.ruangan_id 
		      FROM
		        hrd.fa_lokasi 
		      WHERE 1 
		        AND hrd.fa_lokasi.mutasi_no = '' 
		      ORDER BY hrd.fa_lokasi.lokasi_date DESC,
		        hrd.fa_lokasi.sid DESC) AS unu 
		    WHERE 1 
		    GROUP BY unu.fixed_asset_id) AS tb_lokasi 
		    ON fa_fixed_asset.fixed_asset_id = tb_lokasi.fixed_asset_id 
		  INNER JOIN hrd.depo 
		    ON tb_lokasi.depo_id = hrd.depo.depo_id 
		  INNER JOIN hrd.cabang 
		    ON hrd.depo.cabang_id = hrd.cabang.cabang_id 
		  INNER JOIN 
		    (SELECT 
		      uni.* 
		    FROM
		      (SELECT 
		        hrd.fa_penanggung_jawab.sid AS sid_pj,
		        hrd.fa_penanggung_jawab.fixed_asset_id,
		        hrd.fa_penanggung_jawab.pj_date,
		        hrd.fa_penanggung_jawab.type_pj,
		        hrd.fa_penanggung_jawab.employee_id 
		      FROM
		        hrd.fa_penanggung_jawab 
		      WHERE 1 
		        AND hrd.fa_penanggung_jawab.mutasi_no = '' 
		      ORDER BY hrd.fa_penanggung_jawab.pj_date DESC,
		        hrd.fa_penanggung_jawab.sid DESC) AS uni 
		    WHERE 1 
		    GROUP BY uni.fixed_asset_id) AS tb_pj 
		    ON hrd.fa_fixed_asset.fixed_asset_id = tb_pj.fixed_asset_id 
		  LEFT JOIN hrd.employee 
		    ON tb_pj.employee_id = hrd.employee.employee_id 
		  LEFT JOIN 
		    (SELECT 
		      fa_fixed_asset.fixed_asset_id,
		      tb_risk.ket_risk 
		    FROM
		      hrd.fa_fixed_asset 
		      LEFT JOIN 
		        (SELECT 
		          * 
		        FROM
		          (SELECT 
		            fixed_asset_id,
		            'Y' AS ket_risk 
		          FROM
		            hrd.fa_pemusnahan_detail 
		          WHERE 1 
		          GROUP BY hrd.fa_pemusnahan_detail.fixed_asset_id 
		          UNION
		          SELECT 
		            fixed_asset_id,
		            'Y' AS ket_risk 
		          FROM
		            hrd.fa_penjualan 
		          WHERE 1 
		          GROUP BY hrd.fa_penjualan.fixed_asset_id 
		          UNION
		          SELECT 
		            fixed_asset_id,
		            'Y' AS ket_risk 
		          FROM
		            hrd.fa_perubahan_status 
		          WHERE 1 
		          GROUP BY hrd.fa_perubahan_status.fixed_asset_id) AS uni 
		        ORDER BY fixed_asset_id ASC,
		          ket_risk DESC) AS tb_risk 
		        ON hrd.fa_fixed_asset.fixed_asset_id = tb_risk.fixed_asset_id 
		    WHERE 1 
		    GROUP BY hrd.fa_fixed_asset.fixed_asset_id 
		    ORDER BY hrd.fa_fixed_asset.fixed_asset_id ASC) AS tabel_risk 
		    ON hrd.fa_fixed_asset.fixed_asset_id = tabel_risk.fixed_asset_id 
		WHERE 1 
		ORDER BY hrd.fixed_asset_reminder.sid ASC 
    ";
		  
    $qry = mysql_query($q);    
    while($row = mysql_fetch_array($qry))
    {
        list(
        	$sid,
        	$cabang_name,
        	$depo_name,	
		  	$fixed_asset_id,
		  	$fixed_asset_code,
        	$desc_kendaraan_nopol,
        	$desc_kendaraan_merek,
        	$desc_kendaraan_jenis,
        	$desc_kendaraan_cc, 
        	$desc_kendaraan_warna,
        	$desc_kendaraan_no_rangka,
        	$desc_kendaraan_no_mesin,
        	$employee_id,
        	$employee_name,
        	$gender,
        	$email,
        	$ket_risk) = $row;
        
        $arr_data["cabang_name"][$sid] = $cabang_name;
        $arr_data["depo_name"][$sid] = $depo_name;
        $arr_data["fixed_asset_id"][$sid] = $fixed_asset_id;
        $arr_data["fixed_asset_code"][$sid] = $fixed_asset_code;
        $arr_data["desc_kendaraan_nopol"][$sid] = $desc_kendaraan_nopol;
        $arr_data["desc_kendaraan_merek"][$sid] = $desc_kendaraan_merek;
        $arr_data["desc_kendaraan_jenis"][$sid] = $desc_kendaraan_jenis;
        $arr_data["desc_kendaraan_cc"][$sid] = $desc_kendaraan_cc;
        $arr_data["desc_kendaraan_warna"][$sid] = $desc_kendaraan_warna;
        $arr_data["desc_kendaraan_no_rangka"][$sid] = $desc_kendaraan_no_rangka;
        $arr_data["desc_kendaraan_no_mesin"][$sid] = $desc_kendaraan_no_mesin;
        $arr_data["employee_id"][$sid] = $employee_id;
        $arr_data["employee_name"][$sid] = $employee_name;
        $arr_data["gender"][$sid] = $gender;
        $arr_data["email"][$sid] = $email;
        $arr_data["ket_risk"][$sid] = $ket_risk;
    }
    
    //echo "<pre>";
   // print_r($arr_data);
    //echo "</pre>";
    
    $body = '
    		<table align="center" cellpadding="0" cellspacing="0" width="80%" style="border: 3px solid #C1CDD8; ">
				<tbody>

					<tr style="background: #bad0ee; height: 50;">
						<td colspan="6" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
							<p style="text-transform: uppercase;">
								Alert Reminder Vehicle
							</p>
						</td>
					</tr>

					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>

					<tr>
						<td colspan="3" align="center">
							<table border="1" cellpadding="0" cellspacing="0" width="98%">
								<tr style="background: #bad0ee;" height="25">
									<td rowspan="2" style="padding: 0 0 0 3px;">Pengingat</td>
				                    <td rowspan="2" style="padding: 0 0 0 3px;">Subjek</td>
				                    <td rowspan="2" style="padding: 0 0 0 3px;">Tanggal</td>
				                    <td rowspan="2" style="padding: 0 0 0 3px;">Merk/Type</td>
				                    <td rowspan="2" style="padding: 0 0 0 3px;">Jenis/Model</td>
				                    <td rowspan="2" style="padding: 0 0 0 3px;">Pengguna</td>
				                    <td colspan="2" style="padding: 0 0 0 3px; text-align:center;">Lokasi</td>
								</tr>
								
								<tr style="background: #bad0ee;" height="25">
						        	<td align="center">Cabang</td>
						        	<td align="center">Depo</td>
						        </tr>
                '; 
				
                for($i=parsedate($date_start);$i<=parsedate($date_end);$i=$i+86400)
                {
                    foreach($arr_data["list_data"][date("Y-m-d", $i)] as $counter => $val)
                    {
                        $repeatition_name = $arr_data["repeatition_name"][date("Y-m-d", $i)][$counter];
                        $reminder_name = $arr_data["reminder_name"][date("Y-m-d", $i)][$counter];
                        $sid = $arr_data["sid"][date("Y-m-d", $i)][$counter];
                        $subject = $arr_data["subject"][date("Y-m-d", $i)][$counter];
                        $reminder_date = $arr_data["reminder_date"][date("Y-m-d", $i)][$counter];
                        $date_alert = $arr_data["date_alert"][date("Y-m-d", $i)][$counter];
                        $reminder_time = $arr_data["reminder_time"][date("Y-m-d", $i)][$counter];
                        $repeatition_id = $arr_data["repeatition_id"][date("Y-m-d", $i)][$counter];
                        
                        $cabang_name = $arr_data["cabang_name"][$sid];
                        $depo_name = $arr_data["depo_name"][$sid];
				        $desc_kendaraan_nopol = $arr_data["desc_kendaraan_nopol"][$sid];
				        $desc_kendaraan_merek = $arr_data["desc_kendaraan_merek"][$sid];
				        $desc_kendaraan_jenis = $arr_data["desc_kendaraan_jenis"][$sid];
				        $desc_kendaraan_cc = $arr_data["desc_kendaraan_cc"][$sid];
				        $desc_kendaraan_warna = $arr_data["desc_kendaraan_warna"][$sid];
				        $desc_kendaraan_no_rangka = $arr_data["desc_kendaraan_no_rangka"][$sid];
				        $desc_kendaraan_no_mesin = $arr_data["desc_kendaraan_no_mesin"][$sid];
				        $employee_id = $arr_data["employee_id"][$sid];
				        $employee_name = $arr_data["employee_name"][$sid];
        				$gender = $arr_data["gender"][$sid];
				        $email = $arr_data["email"][$sid];
        				$ket_risk = $arr_data["ket_risk"][$sid];
        				
        				if($ket_risk=="N")
        				{
	                        if($email)
	                        {
	                            $arr_data["list_email"][$email] = $email;
	                            $arr_data["personal_pengguna"][$email] = $employee_name; 
	                            $arr_data["personal_gender"][$email] = $gender;
	                            $arr_data["personal_no_polisi"][$email] = $desc_kendaraan_nopol;
	                            $arr_data["personal_merk_type"][$email] = $desc_kendaraan_merek;
	                            $arr_data["personal_reminder_name"][$email] = $reminder_name;
	                            $arr_data["personal_reminder_date"][$email] = $reminder_date;                        
	                        }	
						
                        $body .= '
                    		
                    		<tr>
								<td style="padding: 0 0 0 3px;" align="center">'.$reminder_name.'</td>
								<td style="padding: 0 0 0 3px;">'.$subject.'</td>
								<td style="padding: 0 0 0 3px;" align="center">'.format_show_date($reminder_date).'</td>
								<td style="padding: 0 0 0 3px;">'.$desc_kendaraan_merek.'</td>
								<td style="padding: 0 0 0 3px;">'.$desc_kendaraan_jenis.'</td>
								<td style="padding: 0 0 0 3px;">'.$employee_name.'</td>
								<td style="padding: 0 0 0 3px;">'.$cabang_name.'</td>
								<td style="padding: 0 0 0 3px;">'.$depo_name.'</td>
							</tr>  
                            
                            ';
						}
                    }
                }  
                 $body .= '
							</table>
						</td>
					</tr>

					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					
				</tbody>
			</table>
                ';
                
        
        //echo $body;
        //echo "<pre>";
        //print_r($arr_data["list_data"][date("Y-m-d", $i)]);
        //echo "</pre>";
        
     
     if($ada_data)
     {
         $q = "
                SELECT
                    hrd.function_email.email_address,
                    hrd.function_email.email_name
                FROM
                    hrd.function_email
                WHERE
                    1
                    AND hrd.function_email.func_name = 'alert_fixed_asset_reminder'
                ORDER BY
                    hrd.function_email.email_address ASC
        ";
        $qry = mysql_query($q);
        
        $email_address_multi = "";
        $email_name_multi = "";
        
        while($row = mysql_fetch_array($qry))
        {
            list($email_address, $email_name) = $row;
            
            $email_address_multi .= $email_address.";";
            $email_name_multi .= $email_name.";";
        }
        
        send_email_multiple("Alert Reminder Vehicle (".date("d/m/Y", $i).")", $body, $email_address_multi, $email_name_multi, "Cron");  
     }
     
     
     
     // kirim email personal ke pengguna
     foreach($arr_data["list_email"] as $email=>$val)
     {
        $employee_name = $arr_data["personal_pengguna"][$email]; 
        $gender = $arr_data["personal_gender"][$email];
        $desc_kendaraan_nopol = $arr_data["personal_no_polisi"][$email] ;
        $desc_kendaraan_merek = $arr_data["personal_merk_type"][$email];
        $reminder_name = $arr_data["personal_reminder_name"][$email];
        $reminder_date = $arr_data["personal_reminder_date"][$email];  
        
        $echo_gender = "";
        if($gender=="Perempuan")
        {
			$echo_gender = "Ibu";	
		}
		else
		{
			$echo_gender = "Bapak";	
		}
		
		$body_personal ='
			<table align="center" cellpadding="0" cellspacing="0" width="60%" style="border: 3px solid #C1CDD8; ">
				<tbody>

					<tr style="background: #bad0ee; height: 50;">
						<td style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
							<p style="text-transform: uppercase;">
								Alert Reminder Vehicle
							</p>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td style="padding: 0 0 0 6px;">
							Dear '.$echo_gender.' '.$employee_name.',<br><br>
							Kendaraan dengan No Polisi '.$desc_kendaraan_nopol.' ('.$desc_kendaraan_merek.') akan dilakukan perpanjangan STNK Pada '.format_show_date($reminder_date).'<br><br>
							Mohon segera hubungi Divisi GA agar tidak terjadi keterlambatan perpanjangan STNK<br><br>
							Terima Kasih
						</td>
					</tr>

					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					
				</tbody>
			</table>
		';
        
        $email_address_multi = $email.";"; 
        $email_name_multi = $employee_name.";";
        
        $email_address_multi .= "hndtmp@vci.co.id;"; 
        $email_name_multi .= "hndtmp@vci.co.id;";
        
        send_email_multiple("Alert Reminder Vehicle (".$reminder_name." - ".$desc_kendaraan_nopol.")", $body_personal, $email_address_multi, $email_name_multi, "Cron");  
     }
     
} 

function cron_alert_presensi($date_oke, $status="", $v_absensi_cabang="")
{
    global $db;
    
    $oke = $date_oke;
    //$oke = "07/09/2015";
    $date_now = parsedate($oke);
    
    if($v_absensi_cabang=="")
    {
        $where_absensi_cabang = "AND ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id IN ('1') ";
    }
    else
    {
        $where_absensi_cabang = "AND ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id IN (".$v_absensi_cabang.") ";
    }
    
    $end_period_parse   = $date_now-432000;
    $start_period_parse = $end_period_parse-604800;
    
    $end_period   = date("Y-m-d", $end_period_parse)." 23:59:59";
    $start_period = date("Y-m-d", $start_period_parse)." 00:00:00";
    
    $date_awal  = $start_period_parse;
    $date_akhir = $end_period_parse;
    
    $q = "
            SELECT
                ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id,
                ".$db["master"].".employee.employee_id,
                ".$db["master"].".employee.employee_name,
                ".$db["master"].".employee.email,
                ".$db["master"].".employee.gender
            FROM
                ".$db["master"].".setup_lokasi_kerja
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".setup_lokasi_kerja.employee_id
            WHERE
                1
                ".$where_absensi_cabang."
                AND ".$db["master"].".setup_lokasi_kerja.absensi = '1'
                -- AND ".$db["master"].".employee.employee_id = '289'
            ORDER BY
                ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id ASC,
                ".$db["master"].".employee.employee_name ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($absensi_cabang_id, $employee_id, $employee_name, $email, $gender) = $row;
        
        $arr_data["list_employee"][$employee_id] = $employee_id;   
        
        $arr_data["absensi_cabang_id"][$employee_id] = $absensi_cabang_id;
        $arr_data["employee_name"][$employee_id] = $employee_name;
        $arr_data["email"][$employee_id] = $email;
        $arr_data["gender"][$employee_id] = $gender;
    }
    
    
    if(count($arr_data["list_employee"])*1>0)
    {
        $where_employee = where_array($arr_data["list_employee"], "employee_id", "in");
        // dapatkan data absensi
        
        // select record
        {   
            $q = "
                    SELECT
                        ".$db["master"].".periode.tanggal,
                        ".$db["master"].".periode.remarks
                    FROM
                        ".$db["master"].".periode
                    WHERE
                        1
                        AND ".$db["master"].".periode.tanggal BETWEEN '".date("Y-m-d", $date_awal)."' AND '".date("Y-m-d", $date_akhir)."'
                        AND ".$db["master"].".periode.kerja = '0'
                    ORDER BY
                        ".$db["master"].".periode.tanggal ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($tanggal, $remarks) = $row;
                
                $arr_data["libur_kalender"][$tanggal] = $tanggal;
                $arr_data["libur_kalender_remarks"][$tanggal] = $remarks;
            } 
            
            
            $q = "
                SELECT 
                  ".$db["master"].".absensi_record.employee_id,
                  ".$db["master"].".absensi_record.absensi_date,
                  IF(MIN(hrd.absensi_record.absensi_time)>='16:00:00','',MIN(hrd.absensi_record.absensi_time)) AS datang,
                      MAX(hrd.absensi_record.absensi_time) AS pulang 
                FROM
                  ".$db["master"].".absensi_record 
                WHERE 1 
                  ".$where_employee."
                  AND ".$db["master"].".absensi_record.absensi_date BETWEEN '".date("Y-m-d", $date_awal)."' AND '".date("Y-m-d", $date_akhir)."' 
                GROUP BY 
                    ".$db["master"].".absensi_record.absensi_date,
                      ".$db["master"].".absensi_record.employee_id 
                ORDER BY 
                    ".$db["master"].".absensi_record.absensi_date ASC
            ";    
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($employee_id, $absensi_date, $datang, $pulang) = $row;

                $arr_data["record_datang"][$employee_id][$absensi_date] = $datang;
                $arr_data["record_pulang"][$employee_id][$absensi_date] = $pulang;
            }
            
            $q = "
                    SELECT 
                      ".$db["master"].".absensi_record.employee_id,
                      ".$db["master"].".absensi_record.absensi_date,
                      COUNT(".$db["master"].".absensi_record.sid) AS total 
                    FROM
                      ".$db["master"].".absensi_record 
                    WHERE 1
                        ".$where_employee."
                        AND ".$db["master"].".absensi_record.absensi_date BETWEEN '".date("Y-m-d", $date_awal)."' AND '".date("Y-m-d", $date_akhir)."'
                    GROUP BY 
                        ".$db["master"].".absensi_record.absensi_cabang_id, 
                        ".$db["master"].".absensi_record.employee_id, 
                        ".$db["master"].".absensi_record.absensi_date 
                ";     
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($employee_id, $absensi_date, $total) = $row;
                    
                    $arr_data["list_emp_count"][$employee_id] = $employee_id;
                    $arr_data["absensi_date"][$employee_id] = $absensi_date;
                    $arr_data["total"][$employee_id][$absensi_date] = $total;
                }                            
                
                // ambil data absensi_daily
                $q = "
                         SELECT 
                                hrd.absensi_rekap.employee_id,
                                hrd.absensi_rekap.absensi_rekap_date,
                                hrd.absensi_rekap_type.type_id,
                                hrd.absensi_rekap_type.type_name,
                                hrd.absensi_rekap.remarks
                         FROM
                            hrd.absensi_rekap
                            INNER JOIN hrd.absensi_rekap_type ON
                                hrd.absensi_rekap_type.`type_id` = hrd.absensi_rekap.`type_id`
                         WHERE
                            1
                            ".$where_employee."
                            AND ".$db["master"].".absensi_rekap.absensi_rekap_date BETWEEN '".date("Y-m-d", $date_awal)."' AND '".date("Y-m-d", $date_akhir)."'
                         ORDER BY
                            hrd.absensi_rekap.absensi_rekap_date DESC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($employee_id, $absensi_rekap_date, $type_id, $type_name, $remarks) = $row;
                    
                    $arr_data["absensi_rekap_remarks"][$employee_id][$absensi_rekap_date] .= $remarks.",";
                    $arr_data["absensi_rekap_type"][$employee_id][$absensi_rekap_date] .= $type_name.",";
                }
                
                // ambil data absensi_personal untuk ngecek status emnunggu approval
                $q = "
                        SELECT
                            hrd.absensi_personal.employee_id,
                            hrd.absensi_personal.type_id,     
                            hrd.absensi_personal.date_from,
                            hrd.absensi_personal.date_to
                        FROM
                            hrd.absensi_personal
                        WHERE
                            1
                            AND hrd.absensi_personal.status_pengajuan = '1'
                            AND 
                                (
                                    hrd.absensi_personal.approval_1_status = '0'
                                    OR
                                    hrd.absensi_personal.approval_2_status = '0'
                                )
                            AND hrd.absensi_personal.date_from >= '".date("Y-m-d", $date_awal)."'
                            AND hrd.absensi_personal.date_from <= '".date("Y-m-d", $date_akhir)."'
                        ORDER BY
                            hrd.absensi_personal.employee_id ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($employee_id, $type_id, $date_from, $date_to) = $row;
                    
                    $date_pending_start = parsedate(format_show_date($date_from));
                    $date_pending_end = parsedate(format_show_date($date_to));
                    
                    for($i=$date_pending_start;$i<=$date_pending_end;$i=$i+86400)
                    {
                        $arr_data["approval_pending"][$employee_id][$i]["Menunggu Approval"] = "Menunggu Approval";
                    }
                } 
        }
        
        
        
        foreach($arr_data["list_employee"] as $employee_id=>$val)
        {
            for($j=$date_awal;$j<=$date_akhir;$j=$j+86400)
            {                          
                $hari = get_day(date("d/m/Y",$j));
                $remarks = "";
                
                
                $record_datang = $arr_data["record_datang"][$employee_id][date("Y-m-d",$j)];
                $record_pulang = $arr_data["record_pulang"][$employee_id][date("Y-m-d",$j)];
                
                $jam_absensi = jam_absensi(date("Y-m-d",$j), $employee_id);
                
                $arr_jam_datang = explode(":",$jam_absensi["datang"]);
                $arr_jam_pulang = explode(":",$jam_absensi["pulang"]);
                $arr_record_datang = explode(":",$record_datang);
                $arr_record_pulang = explode(":",$record_pulang);
                
                $bg_color_tr = "";
                $bg_color = "";
                
                if($hari=="Sabtu" || $hari=="Minggu")
                {
                    $bg_color_tr = "background:#FFCCFF;";    
                }
                
                $libur_kalender = $arr_data["libur_kalender"][date("Y-m-d",$j)];
                $libur_kalender_remarks = $arr_data["libur_kalender_remarks"][date("Y-m-d",$j)];
                
                
                $remarks = $arr_data["absensi_rekap_remarks"][$employee_id][date("Y-m-d", $j)];
                $type_name = $arr_data["absensi_rekap_type"][$employee_id][date("Y-m-d", $j)];
                
                $remarks = substr($remarks, 0, -1);
                $type_name = substr($type_name, 0, -1);
                
                if($hari!="Minggu")
                {
                    if($libur_kalender)
                    {
                        if($type_name)
                        {
                            $remarks .= ", ".$libur_kalender_remarks;
                            $type_name .= ", Libur Kalender";        
                        }
                        else
                        {
                            $remarks = $libur_kalender_remarks;
                            $type_name = "Libur Kalender";    
                        }
                        
                    }
                }
                
                $total = $arr_data["total"][$employee_id][date("Y-m-d",$j)];
                
                if($total)
                {
                    $echo_total = $total;
                }
                else
                {      
                    $echo_total = "...";
                }
                
                if($jam_absensi["masuk"]==$record_datang)
                {
                    $jam_masuk = $jam_absensi["masuk"]; 
                }
                else
                {
                	if($jam_absensi["masuk"])
					{
						$jam_masuk =$jam_absensi["masuk"];		
					}
					else
					{
						$jam_masuk =$record_datang;	
					}   
                }
                
                if($jam_absensi["pulang"]=="")
                {
                	if($echo_total>0)
                	{
                		if($arr_record_datang[0]==$arr_record_pulang[0])
                		{
                			$jam_pulang = "";		
                		}
                		else
                		{
							$jam_pulang = $record_pulang;	
						}
					}
					else
					{
						$jam_pulang ="";	
					}
                }
                else
                {
					if($arr_jam_pulang[0]>$arr_record_pulang[0])
					{
						$jam_pulang = $jam_absensi["pulang"];
					}
					else
					{
						$jam_pulang = $record_pulang;
					}
                }
                
                $bg_masuk  = absen_bg_masuk(date("Y-m-d",$j), $jam_masuk, $employee_id);
                $bg_pulang = absen_bg_pulang(date("Y-m-d",$j), $jam_pulang, $employee_id);
                
                $absensi_cabang_id = $arr_data["absensi_cabang_id"][$employee_id];
                
                // karyawan HO
                if($absensi_cabang_id*1==1)
                {
                
                    if($hari!="Minggu" && $hari!="Sabtu")
                    {
                        if($employee_id=="12")
                        {
                            //echo date("Y-m-d", $j)."::".$bg_masuk."||".$remarks."<hr>";
                        }
                        
                        
                        if(
                            ($jam_masuk=="" && $remarks == "")
                            ||
                            ($jam_pulang=="" && $remarks == "")
                            ||
                            ($bg_masuk!="" && $remarks == "")
                            ||
                            ($bg_pulang!="" && $remarks == "")
                        )
                        {
                            // masuk ke dalam array u/ alert
                            //echo "Alert ".date("Y-m-d", $j);
                            //echo "<hr>";
                            $absensi_cabang_id = $arr_data["absensi_cabang_id"][$employee_id];
                            
                            //$keterangan = "-";
                            if($jam_masuk=="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Lupa Absen"] = "Lupa Absen";    
                            }
                            
                            if($jam_pulang=="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Lupa Absen"] = "Lupa Absen";
                            }
                            
                            if($bg_masuk!="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Datang Terlambat"] = "Datang Terlambat";
                            }
                            
                            if($bg_pulang!="" && $remarks == "" && $bg_masuk=="")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Ijin Meninggalkan Tempat Kerja"] = "Ijin Meninggalkan Tempat Kerja";
                            }
                            else if($bg_pulang!="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Datang Terlambat"] = "Datang Terlambat";
                            }
                            
                            
                            $keterangan_detail_echo = "";
                            foreach($arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)] as $keterangan_detail=>$val)
                            {
                                $keterangan_detail_echo .= $keterangan_detail.", ";    
                            }
                            $keterangan_detail_echo = substr($keterangan_detail_echo, 0,-2);
                            
                            if($arr_data["approval_pending"][$employee_id][$j]["Menunggu Approval"]=="Menunggu Approval")
                            {
                                $keterangan_detail_echo = "Menunggu Approval";
                                //echo "masuk_sini";    
                            }
                            
                            $arr_data["alert_employee"][$employee_id][date("Y-m-d", $j)] = $keterangan_detail_echo;
                            $arr_data["alert_employee_jam"][$employee_id][date("Y-m-d", $j)] = $jam_masuk." - ".$jam_pulang;
                            
                            $arr_data["alert_employee_absensi_cabang"][$absensi_cabang_id][$employee_id][date("Y-m-d", $j)] = $keterangan_detail_echo;
                            $arr_data["alert_employee_absensi_cabang_jam"][$absensi_cabang_id][$employee_id][date("Y-m-d", $j)] = $jam_masuk." - ".$jam_pulang;
                        }
                    }
                
                }
                else 
                {
                    if($hari!="Minggu")
                    {
                        if($employee_id=="12")
                        {
                            //echo date("Y-m-d", $j)."::".$bg_masuk."||".$remarks."<hr>";
                        }
                        
                        
                        if(
                            ($jam_masuk=="" && $remarks == "")
                            ||
                            ($jam_pulang=="" && $remarks == "")
                            ||
                            ($bg_masuk!="" && $remarks == "")
                            ||
                            ($bg_pulang!="" && $remarks == "")
                        )
                        {
                            // masuk ke dalam array u/ alert
                            //echo "Alert ".date("Y-m-d", $j);
                            //echo "<hr>";
                            $absensi_cabang_id = $arr_data["absensi_cabang_id"][$employee_id];
                            
                            //$keterangan = "-";
                            if($jam_masuk=="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Lupa Absen"] = "Lupa Absen";    
                            }
                            
                            if($jam_pulang=="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Lupa Absen"] = "Lupa Absen";
                            }
                            
                            if($bg_masuk!="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Datang Terlambat"] = "Datang Terlambat";
                            }

                            if($bg_pulang!="" && $remarks == "" && $bg_masuk=="")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Ijin Meninggalkan Tempat Kerja"] = "Ijin Meninggalkan Tempat Kerja";
                            }
                            else if($bg_pulang!="" && $remarks == "")
                            {
                                $arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)]["Datang Terlambat"] = "Datang Terlambat";
                            }
                            
                            
                            $keterangan_detail_echo = "";
                            foreach($arr_data["keterangan_detail"][$employee_id][date("Y-m-d", $j)] as $keterangan_detail=>$val)
                            {
                                $keterangan_detail_echo .= $keterangan_detail.", ";    
                            }
                            $keterangan_detail_echo = substr($keterangan_detail_echo, 0,-2);
                            
                            if($arr_data["approval_pending"][$employee_id][$j]["Menunggu Approval"]=="Menunggu Approval")
                            {
                                $keterangan_detail_echo = "Menunggu Approval";
                                //echo "masuk_sini";    
                            }
                            
                            $arr_data["alert_employee"][$employee_id][date("Y-m-d", $j)] = $keterangan_detail_echo;
                            $arr_data["alert_employee_jam"][$employee_id][date("Y-m-d", $j)] = $jam_masuk." - ".$jam_pulang;
                            
                            $arr_data["alert_employee_absensi_cabang"][$absensi_cabang_id][$employee_id][date("Y-m-d", $j)] = $keterangan_detail_echo;
                            $arr_data["alert_employee_absensi_cabang_jam"][$absensi_cabang_id][$employee_id][date("Y-m-d", $j)] = $jam_masuk." - ".$jam_pulang;
                        }
                    }    
                }
                
            } 
        } // end foreach
        
        // kirim email ke orang nya
        
      //  echo "<pre>";
//        print_r($arr_data["alert_employee"]);
//        echo "</pre>";
        
        
        $body = "";
        foreach($arr_data["alert_employee"] as $employee_id=>$val)
        {
            $employee_name = $arr_data["employee_name"][$employee_id];
            $email = $arr_data["email"][$employee_id];
            $gender = $arr_data["gender"][$employee_id];
            
            $body  = "";
            
            if($gender=="Laki - Laki")
            {
                $body .= "Dear Bapak ".$employee_name.",<br><br>";    
            }
            else if($gender=="Perempuan")
            {
                $body .= "Dear Ibu ".$employee_name.",<br><br>";
            }
            else
            {
                $body .= "Dear Bapak / Ibu ".$employee_name.",<br><br>";    
            }
            
            $body .= "Mohon untuk mengisi data presensi online pada tanggal : <br>";
            foreach($arr_data["alert_employee"][$employee_id] as $absensi_date=>$val_keterangan)
            {
                $alert_employee_jam = $arr_data["alert_employee_jam"][$employee_id][$absensi_date];
                
                $body .= format_show_date($absensi_date)." ".$alert_employee_jam." karena ".$val_keterangan."<br>";     
            }
            $body  = substr($body, 0, -4);
            $body .= "<br><br>Sekian. Terima Kasih";     
            
            $email .= ";";
            $employee_name .= ";";
            if($email!=";")
            {
                if($status!="cek_data")
                {
                    $kirim_email = send_email_multiple("Alert Presensi ".$date_oke , $body, $email, $employee_name, "Auto");
                }
            }
        }
        
        
        //echo "<pre>";
        //print_r($arr_data["alert_employee_absensi_cabang"]);
        //echo "</pre>";
        // kirim email ke pic absensi_cabang
        
        
        $body = "";
        foreach($arr_data["alert_employee_absensi_cabang"] as $absensi_cabang_id=>$val)
        {
            $q = "
                    SELECT 
                      ".$db["master"].".employee.employee_id,
                      ".$db["master"].".employee.employee_name,
                      ".$db["master"].".employee.email
                    FROM
                      ".$db["master"].".employee 
                      INNER JOIN ".$db["master"].".absensi_cabang ON
                        ".$db["master"].".employee.employee_id = ".$db["master"].".absensi_cabang.pic
                        AND ".$db["master"].".absensi_cabang.absensi_cabang_id = '".$absensi_cabang_id."'
                    WHERE
                        1
                    LIMIT
                        0,1
            ";
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($pic_id, $pic_name, $pic_email) = $row;
            
            $body  = "";
            $body .= "Dear ".$pic_name.",<br>";
            $body .= "Berikut data Presensi yang belum memiliki Keterangan : <br><br>";
            
            $body .= "
                        <table width='800' align='center'>
                        <tr style='background: #bad0ee; height: 25px; font-weight: bold; text-align: center;'>
                            <td width='300' style='border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>Employee Name</td>                            
                            <td width='300' align='center' style='border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>Tanggal</td>
                            <td width='200' align='center' style='border-right-style: solid; border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>Jam</td>
                        </tr>            
                ";
            
            foreach($arr_data["alert_employee_absensi_cabang"][$absensi_cabang_id] as $employee_id=>$val)
            {
                $employee_name = $arr_data["employee_name"][$employee_id];
                
                $body .= "
                        <tr>
                            <td valign='top' style='border-bottom-style: solid; border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>".$employee_name."</td>                            
                            <td valign='top' align='left' style='border-bottom-style: solid; border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>
                                ";
                                foreach($arr_data["alert_employee_absensi_cabang"][$absensi_cabang_id][$employee_id] as $absensi_date=>$val_keterangan)
                                {
                                    $body .= format_show_date($absensi_date)." : ".$val_keterangan."<br>";
                                }
                                $body = substr($body,0,-4);
                                $body .= "
                            </td>
                            <td valign='top' style='border-right-style: solid; border-bottom-style: solid; border-top-style: solid; border-left-style: solid; border-width: 1px; border-color: #666666;'>
                                ";
                                foreach($arr_data["alert_employee_absensi_cabang_jam"][$absensi_cabang_id][$employee_id] as $absensi_date=>$val_jam)
                                {
                                    $body .= $val_jam."<br>";
                                }
                                $body = substr($body,0,-4);
                                $body .= "
                            </td>
                        </tr>      
                ";            
            }
            
            $body .= "</table>";
            
            //echo $body;
            
            
            
            $pic_email .= ";";
            $pic_name .= ";";
            
            $pic_email .= ";hndtmp@vci.co.id;";
            $pic_name .= ";Hendri Temp;";
            
//            $pic_email = "";
//            $pic_name = "";
//            $pic_email .= "hndtmp@vci.co.id;";
//            $pic_name .= "Hendri Temp;";

            if($pic_email)
            {
                if($status!="cek_data")
                {
                    $kirim_email = send_email_multiple("Alert Presensi HRD ".$date_oke , $body, $pic_email, $pic_name, "Auto");
                }
                //echo $kirim_email;
                
                if($status=="cek_data")
                {
                    echo $body;
                }
            }
        } 
    }
} 

function cron_alert_cpg($date_end, $status="")
{
	global $db;
	
	$arr_date = explode("/",$date_end);
	
	$bulan_start="";
	if($arr_date[1]=="12")
	{
		$bulan_start = "01";	
	}
	else
	{
		$bulan_start = sprintf("%02s",($arr_date[1]*1)-1);	
	}	
	
	$tgl_start = sprintf("%02s",($arr_date[0]*1)+1);
	
	$date_start = $tgl_start."/".$bulan_start."/".$arr_date[2];
	
	$q="
		SELECT 
		  ".$db["master"].".absensi_cabang.absensi_cabang_id,
		  ".$db["master"].".absensi_cabang.absensi_cabang_name,
		  ".$db["master"].".absensi_cabang.pic 
		FROM
		  ".$db["master"].".absensi_cabang 
		WHERE 1 
		ORDER BY 
		  ".$db["master"].".absensi_cabang.absensi_cabang_id ASC		  
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
		list($absensi_cabang_id,$absensi_cabang_name,$pic)=$row;
		
		$arr_data["list_absensi_cabang"][$absensi_cabang_id]=$absensi_cabang_id;
		$arr_data["absensi_cabang_name"][$absensi_cabang_id]=$absensi_cabang_name;
		$arr_data["pic"][$absensi_cabang_id]=$pic;
	}
	
	$body="";
	foreach($arr_data["list_absensi_cabang"] as $absensi_cabang_id => $val)
	{
		$absensi_cabang_name = $arr_data["absensi_cabang_name"][$absensi_cabang_id];
		$pic = $arr_data["pic"][$absensi_cabang_id];
		
		unset($arr_data["list_cpg"]);
		
		// select data cpg
		{
			$q="
				SELECT 
				  ".$db["master"].".absensi_rekap.absensi_rekap_id,
				  ".$db["master"].".absensi_rekap.absensi_rekap_date,
				  ".$db["master"].".absensi_rekap.employee_id,
				  ".$db["master"].".absensi_rekap.type_id,
				  ".$db["master"].".absensi_rekap_type.type_name,
				  ".$db["master"].".absensi_rekap.remarks,
				  ".$db["master"].".absensi_rekap.type_data, 
				  ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id 
				FROM
				  ".$db["master"].".absensi_rekap 
				  INNER JOIN ".$db["master"].".absensi_rekap_type 
				    ON ".$db["master"].".absensi_rekap.type_id = ".$db["master"].".absensi_rekap_type.type_id 
				  INNER JOIN ".$db["master"].".setup_lokasi_kerja 
				    ON ".$db["master"].".absensi_rekap.employee_id = ".$db["master"].".setup_lokasi_kerja.employee_id 
				WHERE 1 
				  AND ".$db["master"].".absensi_rekap.type_id = '14' 
				  AND ".$db["master"].".absensi_rekap.absensi_rekap_date BETWEEN '".format_save_date($date_start)."' AND '".format_save_date($date_end)."' 
				  AND ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id = '".$absensi_cabang_id."'
				ORDER BY 
				  ".$db["master"].".absensi_rekap.absensi_rekap_date ASC,
				  ".$db["master"].".absensi_rekap.absensi_rekap_id DESC,
	  			  ".$db["master"].".absensi_rekap.employee_id ASC
			";
			echo $q."</br>";
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($absensi_rekap_id,$absensi_rekap_date,$employee_id,$type_id,$type_name,$remarks,$type_data,$absensi_cabang_id)=$row;
				
				$arr_data["list_cpg"][$absensi_rekap_id]=$absensi_rekap_id;
				$arr_data["absensi_rekap_date"][$absensi_rekap_id]=$absensi_rekap_date;
				$arr_data["employee_id"][$absensi_rekap_id]=$employee_id;
				$arr_data["type_id"][$absensi_rekap_id]=$type_id;
				$arr_data["type_name"][$absensi_rekap_id]=$type_name;
				$arr_data["remarks"][$absensi_rekap_id]=$remarks;
				$arr_data["type_data"][$absensi_rekap_id]=$type_data;
				$arr_data["absensi_cabang_id"][$absensi_rekap_id]=$absensi_cabang_id;
			}
		}
		
		$list_cpg="";		
		$no=1;
		foreach($arr_data["list_cpg"] as $absensi_rekap_id => $key)
		{
			$absensi_rekap_date = $arr_data["absensi_rekap_date"][$absensi_rekap_id];
			$employee_id = $arr_data["employee_id"][$absensi_rekap_id];
			$type_id = $arr_data["type_id"][$absensi_rekap_id];
			$type_name = $arr_data["type_name"][$absensi_rekap_id];
			$remarks = $arr_data["remarks"][$absensi_rekap_id];
			$type_data = $arr_data["type_data"][$absensi_rekap_id];
			$absensi_cabang_id = $arr_data["absensi_cabang_id"][$absensi_rekap_id];
			
			$data_employee = select_employee($employee_id, format_show_date($absensi_rekap_date));
					
			$list_cpg .='
				<tr>
					<td style="padding: 0 0 0 3px;" align="center">'.$no.'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["employee_name"].'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["company_initial"].'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["cabang_name"].'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["depo_name"].'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["divisi_name"].'</td>
					<td style="padding: 0 0 0 3px;">'.$data_employee["jabatan_name"].'</td>
					<td style="padding: 0 0 0 3px; text-align: center;">'.format_show_date($absensi_rekap_date).'</td>
					<td style="padding: 0 0 0 3px;"><nobr>'.$remarks.'</nobr></td>
				</tr>				
			';
			$no++;
		}
		
		$subject = 'CPG Periode '.$date_start.' s/d '.$date_end.' '.$absensi_cabang_name.' ';
		
		// body email
		{
			$body = '
				<table align="center" cellpadding="0" cellspacing="0" width="90%" style="border: 3px solid #C1CDD8; ">
					<tbody>

						<tr style="background: #bad0ee; height: 50;">
							<td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
								<p style="text-transform: uppercase;">
									Report Cuti Potong Gaji '.$absensi_cabang_name.'
								</p>
							</td>
						</tr>

						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						
						<tr height="25">
							<td colspan="3" style="padding: 0 0 0 10px;">
								Berikut data <b>CPG</b> (Cuti Potong Gaji) Periode '.$date_start.' s/d  '.$date_end.':
							</td>
						</tr>

						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						
						<tr>
							<td colspan="3" align="center">
								<table border="1" cellpadding="0" cellspacing="0" width="98%">
									<tr style="background: #bad0ee;" height="25">
										<td style="padding: 0 0 0 3px;" width="30">No</td>
										<td style="padding: 0 0 0 3px;" width="200">Nama</td>
										<td style="padding: 0 0 0 3px;" width="60">Perusahaan</td>
										<td style="padding: 0 0 0 3px;" width="150">Cabang</td>
										<td style="padding: 0 0 0 3px;" width="80">Depo</td>
										<td style="padding: 0 0 0 3px;" width="60">Divisi</td>
										<td style="padding: 0 0 0 3px;" width="150">Jabatan</td>
										<td style="padding: 0 0 0 3px;" width="100">Absensi Date</td>
										<td style="padding: 0 0 0 3px;" >Remarks</td>
									</tr>
									
									'.$list_cpg.'
									
								</table>
							</td>
						</tr>

						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>

					</tbody>
				</table>
			';
		}
		
		if(count($arr_data["list_cpg"])>0)
		{
			if($status=="cek_data")
			{
				echo $body."</br>";
			}
			else
			{
				send_email_employee($pic, $subject, $body);		
			}
		}
	}
} 
?>