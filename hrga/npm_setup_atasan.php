<?php 
    include("header.php"); 
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    $link_adjust    = "?v_keyword=".$v_keyword."&p=".$p;
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $icon_type_change = "entypo-up-dir";
    $order_type_change = "asc";
    if($order_type=="asc")
    {
        $order_type_change = "desc";
    	$icon_type_change = "entypo-down-dir";
    }
    
    $order_by_content = "";
    if($order_by!="")
    {
        $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
    }
    
    $modul = "Setup Atasan";
    $list  = "npm_setup_atasan.php";
    $htm   = "npm_setup_atasan_form.php";
    $pk    = "setup_atasan_id";
    $title = "employee_name";
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="get">
		<div class="row">
			<div class="col-md-10">
				Search&nbsp;
				<input type="text" size="30" maxlength="30" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>">
				&nbsp;
			</div>
			
			<div class="col-md-2" align="right">
				<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100),get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>')">Tambah<i class="entypo-plus"></i></button>
			</div>
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="30">No</th>
                                        
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_nik&order_type=".$order_type_change; ?>" class="link_menu">NIK<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><center><a href="<?php echo $list.$link_adjust."&order_by=employee.employee_name&order_type=".$order_type_change; ?>" class="link_menu">Nama<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        
                        <th><center>Perusahaan</center></th>
                        <th><center>Cabang</center></th>
                        <th><center>Divisi</center></th>
                        <th><center>Jabatan</center></th>
                        <th><center>Jumlah</center></th>
                        <th width="150"><center>Navigasi</center></th>
					</tr>
				</thead>
				
				<tbody>
					<?php
                    $keyWord = trim($v_keyword);
                  
                    if($keyWord == '')
                    {
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".setup_atasan.setup_atasan_id,
										".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name
                                    FROM 
                                        ".$db["master"].".setup_atasan
                                        INNER JOIN ".$db["master"].".employee ON
                                            ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan.employee_id
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".employee.employee_name ASC,
                                        ".$db["master"].".employee.employee_id ASC,
                                        ".$db["master"].".employee.employee_nik ASC
                                ";
                        $query = mysql_query($sql);
                        $max = ceil(mysql_num_rows($query)/$jml_page);
                        $s = $jml_page * $p;
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".setup_atasan.setup_atasan_id,
                                        ".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name
                                        
                                    FROM 
                                        ".$db["master"].".setup_atasan
                                        INNER JOIN ".$db["master"].".employee ON
                                            ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan.employee_id
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".employee.employee_name ASC,
                                        ".$db["master"].".employee.employee_id ASC,
                                        ".$db["master"].".employee.employee_nik ASC
                                    LIMIT ".$s.", ".$jml_page." 
                                ";
                    }
                    else
                    {
                        
                        unset($arr_keyword);
						$arr_keyword[0] = "employee.employee_name";
                        $arr_keyword[1] = "employee.employee_nik";
                        $arr_keyword[2] = "employee.employee_code_hrd";
						
						$search_keyword = search_keyword($v_keyword, $arr_keyword);
						$where = $search_keyword;
                        
                        $flag=0;
                        for($i=0; $i < strlen($keyWord); $i++)
                        {
                            if($keyWord[$i] == '\'') $flag++;
                            if($keyWord[$i] == '<') $flag++;
                            if($keyWord[$i] == '>') $flag++;
                        }
                        
                        if($flag==0)
                        {
                            $sql = "
                                        SELECT 
                                            ".$db["master"].".setup_atasan.setup_atasan_id,
                                            ".$db["master"].".employee.employee_id,
                                            ".$db["master"].".employee.employee_nik,
                                            ".$db["master"].".employee.employee_code_hrd,
                                            ".$db["master"].".employee.employee_name
                                        FROM 
                                            ".$db["master"].".setup_atasan
                                            INNER JOIN ".$db["master"].".employee ON
                                                ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan.employee_id
                                        WHERE
                                            1
                                            ".$where."
                                        ORDER BY
                                            ".$order_by_content." 
                                            ".$db["master"].".employee.employee_name ASC,
                                            ".$db["master"].".employee.employee_id ASC,
                                            ".$db["master"].".employee.employee_nik ASC
                                   ";
                            $query = mysql_query($sql);
                            $max = ceil(mysql_num_rows($query)/$jml_page);
                            $s = $jml_page * $p;
                            $sql = "
                                        SELECT 
                                            ".$db["master"].".setup_atasan.setup_atasan_id,
                                            ".$db["master"].".employee.employee_id,
                                            ".$db["master"].".employee.employee_nik,
                                            ".$db["master"].".employee.employee_code_hrd,
                                            ".$db["master"].".employee.employee_name
                                        FROM 
                                            ".$db["master"].".setup_atasan
                                            INNER JOIN ".$db["master"].".employee ON
                                                ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan.employee_id
                                            
                                        WHERE
                                            1
                                            ".$where."
                                        ORDER BY
                                            ".$order_by_content." 
                                            ".$db["master"].".employee.employee_name ASC,
                                            ".$db["master"].".employee.employee_id ASC,
                                            ".$db["master"].".employee.employee_nik ASC
                                        LIMIT ".$s.", ".$jml_page." 
                                  ";
                        }
                        else
                        {
                            $msg = "Your input are not allowed!";
                        }
                    }
                    
                    
                    $qry_arr = mysql_query($sql);
                    while($row_arr = mysql_fetch_array($qry_arr))
                    {
                        list($setup_atasan_id, $employee_id, $employee_nik, $employee_code_hrd, $employee_name) = $row_arr;
                        
                        $arr_data["list_employee"][$employee_id] = $employee_id;
                        
                        $arr_data["setup_atasan_id"][$employee_id] = $setup_atasan_id;
                    }
                    
                    if(count($arr_data["list_employee"])*1>0)
                    {
                        $where_employee = where_array($arr_data["list_employee"], "uni.employee_id", "in");
                        
                        $q = "
                                SELECT
                                    uni.employee_id,
                                    uni.company_id,
                                    uni.company_name,
                                    uni.company_initial,
                                    uni.cabang_id,
                                    uni.cabang_name,
                                    uni.depo_id,
                                    uni.depo_name,
                                    uni.divisi_id,
                                    uni.divisi_name,
                                    uni.departemen_id,
                                    uni.departemen_name,
                                    uni.jabatan_id,
                                    uni.jabatan_name
                                FROM
                                (
                                    SELECT
                                      uni2.employee_id,
                                      uni2.company_id,
                                      uni2.company_name,
                                      uni2.company_initial,
                                      uni2.cabang_id,
                                      uni2.cabang_name,
                                      uni2.depo_id,
                                      uni2.depo_name,
                                      uni2.divisi_id,
                                      uni2.divisi_name,
                                      uni2.departemen_id,
                                      uni2.departemen_name,
                                      uni2.jabatan_id,
                                      uni2.jabatan_name
                                    FROM
                                    (
                                        SELECT 
                                          ".$db["master"].".employee_position.employee_id,
                                          ".$db["master"].".company.company_id,
                                          ".$db["master"].".company.company_name,
                                          ".$db["master"].".company.company_initial,
                                          ".$db["master"].".hrd_cabang.cabang_id,
                                          ".$db["master"].".hrd_cabang.cabang_name,
                                          ".$db["master"].".depo.depo_id,
                                          ".$db["master"].".depo.depo_name,
                                          ".$db["master"].".hrd_divisi.divisi_id,
                                          ".$db["master"].".hrd_divisi.divisi_name,
                                          ".$db["master"].".hrd_departemen.departemen_id,
                                          ".$db["master"].".hrd_departemen.departemen_name,
                                          ".$db["master"].".jabatan.jabatan_id,
                                          ".$db["master"].".jabatan.jabatan_name 
                                        FROM
                                          ".$db["master"].".employee_position 
                                          INNER JOIN ".$db["master"].".company 
                                            ON ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id 
                                          INNER JOIN ".$db["master"].".hrd_divisi 
                                            ON ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id 
                                          INNER JOIN ".$db["master"].".hrd_departemen 
                                            ON ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id 
                                          INNER JOIN ".$db["master"].".depo 
                                            ON ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id 
                                          INNER JOIN ".$db["master"].".hrd_cabang 
                                            ON ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id 
                                          INNER JOIN ".$db["master"].".jabatan 
                                            ON ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id 
                                        WHERE 
                                          1
                                        ORDER BY
                                          ".$db["master"].".employee_position.`start_date` DESC
                                    )AS uni2
                                    WHERE
                                        1
                                    GROUP BY
                                        uni2.employee_id
                                )AS uni
                                WHERE
                                    1
                                    ".$where_employee."
                                            
                        ";      
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $employee_id,
                                $company_id,
                                $company_name,
                                $company_initial,
                                $cabang_id,
                                $cabang_name,
                                $depo_id,
                                $depo_name,
                                $divisi_id,
                                $divisi_name,
                                $departemen_id,
                                $departemen_name,
                                $jabatan_id,
                                $jabatan_name
                            ) = $row;
                            
                            $arr_data["company_id"][$employee_id] = $company_id;
                            $arr_data["company_name"][$employee_id] = $company_name;
                            $arr_data["company_initial"][$employee_id] = $company_initial;
                            $arr_data["cabang_id"][$employee_id] = $cabang_id;
                            $arr_data["cabang_name"][$employee_id] = $cabang_name;
                            $arr_data["depo_id"][$employee_id] = $depo_id;
                            $arr_data["depo_name"][$employee_id] = $depo_name;
                            $arr_data["divisi_id"][$employee_id] = $divisi_id;
                            $arr_data["divisi_name"][$employee_id] = $divisi_name;
                            $arr_data["departemen_id"][$employee_id] = $departemen_id;
                            $arr_data["departemen_name"][$employee_id] = $departemen_name;
                            $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
                            $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
                            
                        }
                        
                        // dapatin jumlah bawahan
                        $q = "
                            SELECT
                                ".$db["master"].".setup_atasan_details.setup_atasan_id,
                                COUNT(".$db["master"].".setup_atasan_details.sid) AS jml_bawahan
                            FROM
                                ".$db["master"].".setup_atasan_details
                            WHERE
                                1
                            GROUP BY
                                ".$db["master"].".setup_atasan_details.setup_atasan_id    
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($setup_atasan_id, $jml_bawahan) = $row;
                            
                            $arr_data["jml_bawahan"][$setup_atasan_id] = $jml_bawahan;
                        }      
                    }

                    $query = mysql_query($sql);
                            $sv = 0;
                            $i=1+($jml_page*$p);
                    if(!$row = mysql_num_rows($query))
                    {
                     echo "<tr>";
                     echo "<td align=\"center\" colspan=\"100%\">";
                     echo "No Data";
                     echo "</td>";
                     echo "</tr>";
                    }
                    else
                    {
                        while ($row = mysql_fetch_array($query))
                        {
                            
                            list($setup_atasan_id, $employee_id, $employee_nik, $employee_code_hrd, $employee_name) = $row;
                            
                            $sv++;
                            
                            $bgcolor = "";
                            if($i%2==0)
                            {
                                $bgcolor = "background:#E7E7E7;";
                            }
                            
                            $company_id = $arr_data["company_id"][$employee_id];
                            $company_name = $arr_data["company_name"][$employee_id];
                            $company_initial = $arr_data["company_initial"][$employee_id];
                            $cabang_id = $arr_data["cabang_id"][$employee_id];
                            $cabang_name = $arr_data["cabang_name"][$employee_id];
                            $depo_id = $arr_data["depo_id"][$employee_id];
                            $depo_name = $arr_data["depo_name"][$employee_id];
                            $divisi_id = $arr_data["divisi_id"][$employee_id];
                            $divisi_name = $arr_data["divisi_name"][$employee_id];
                            $departemen_id = $arr_data["departemen_id"][$employee_id];
                            $departemen_name = $arr_data["departemen_name"][$employee_id];
                            $jabatan_id = $arr_data["jabatan_id"][$employee_id];
                            $jabatan_name = $arr_data["jabatan_name"][$employee_id];
                            
                            $jml_bawahan = $arr_data["jml_bawahan"][$setup_atasan_id];
                            
                 ?>
                        <tr title="<?php echo $row[$title]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $i; ?></td>
                            
                            <td align="center" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $employee_nik; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $employee_name; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $company_initial; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $cabang_name; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $divisi_name; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')"><?php echo $jabatan_name; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $setup_atasan_id; ?>')" align="right"><?php echo format_number($jml_bawahan); ?></td>
                            <td align="center">
                            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')">
									<i class="entypo-pencil"></i>
								</button>
                            </td>
                        </tr>
                 <?php
                            $i = $i+1; 
                        }
                    }
                 	?>
				</tbody>
				
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>
		
<?php include("footer.php"); ?>    	