<table class="table table-bordered responsive">
	<thead>	
		<tr class="title_blue">
			<th width="10"><center>No</center></center></th>
		    <th><center>NIK</center></th>
		    <th><center>Kode HRD</center></th>
		    <th><center>Nama</center></th>   
		    <td><center>Perusahaan</center></td>
		    <td><center>Cabang</center></td>    
		    <td><center>Divisi</center></td> 
		    <th><center>Jabatan</center></th>
		    <th><center>Keterangan</center></th>
		    <th><center>Save</center></th>
		    <th><center>Delete</center></th>
		</tr>
	</thead>
	<tbody>
 	<?php
     $no = 1; 
    for($i=1;$i<=1;$i++)
    {
        ?>
            <tr>
                <td>
                    <input type="hidden" name="no[]" value="<?php echo $no; ?>">
                    <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="0">
                    <input type="hidden" name="v_employee_id_<?php echo $no; ?>" id="v_employee_id_<?php echo $no; ?>" value="0">
                </td>
                <td>
                	<nobr>
                		<span id="show_employee_nik_<?php echo $no; ?>"></span>
                		
                		<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" name="btn_emp" id="btn_emp" value="..." onclick="pop_up_search_employee_details('<?php echo $no; ?>')">
							<i class="entypo-dot-3"></i>
						</button>
                	</nobr>
                </td>
                <td id="show_employee_code_hrd_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_employee_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_company_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_cabang_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_divisi_name_<?php echo $no; ?>">&nbsp;</td>
                <td id="show_jabatan_name_<?php echo $no; ?>">&nbsp;</td>
                <td><input type="text" class="form-control-new" size="15" maxlength="255" name="v_remarks_<?php echo $no; ?>" id="v_remarks_<?php echo $no; ?>" value=""></td>
                <td align="center">
	                <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Simpan" title="" name="btn_save_ajax" id="btn_save_ajax" value="Save" onClick="CallAjaxForm('save_ajax','<?php echo $no; ?>')">
						<i class="entypo-floppy"></i>
					</button>
				</td>
                <td>&nbsp;</td>
            </tr>
        <?php
        $no++;
         
    }
	?>

	<?php 
    $q = "
            SELECT 
                ".$db["master"].".setup_atasan_details.sid,
                ".$db["master"].".setup_atasan_details.remarks,
                ".$db["master"].".employee.employee_id,
                ".$db["master"].".employee.employee_nik,
                ".$db["master"].".employee.employee_code_hrd,
                ".$db["master"].".employee.employee_name
                
            FROM 
                ".$db["master"].".setup_atasan_details
                INNER JOIN ".$db["master"].".employee ON
                    ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan_details.employee_id
            WHERE
                1
                AND ".$db["master"].".setup_atasan_details.setup_atasan_id = '".$id."'
            ORDER BY
                ".$db["master"].".employee.employee_name ASC
            
    ";
    $qry = mysql_query($q);
    while($r = mysql_fetch_array($qry))
    {
        list($sid, $remarks, $employee_id, $employee_nik, $employee_code_hrd, $employee_name) = $r;
        
        
        $arr_data["list_sid"][$sid] = $sid;
        $arr_data["list_employee"][$employee_id] = $employee_id;
        
        $arr_data["remarks"][$sid] = $remarks;
        $arr_data["employee_id"][$sid] = $employee_id;
        $arr_data["employee_nik"][$sid] = $employee_nik;
        $arr_data["employee_code_hrd"][$sid] = $employee_code_hrd;
        $arr_data["employee_name"][$sid] = $employee_name;
        
    }
    
    if(count($arr_data["list_employee"])*1>0)
    {
        $where_employee = where_array($arr_data["list_employee"], "uni.employee_id", "in");
        $where_status = where_array($arr_data["list_employee"], "tbl_fixed.employee_id", "in");
        
        // data posisi
        {
        	$q = "
                SELECT
                    uni.employee_id,
                    uni.company_id,
                    uni.company_name,
                    uni.company_initial,
                    uni.cabang_id,
                    uni.cabang_name,
                    uni.depo_id,
                    uni.depo_name,
                    uni.divisi_id,
                    uni.divisi_name,
                    uni.departemen_id,
                    uni.departemen_name,
                    uni.jabatan_id,
                    uni.jabatan_name
                FROM
                (
                    SELECT
                      uni2.employee_id,
                      uni2.company_id,
                      uni2.company_name,
                      uni2.company_initial,
                      uni2.cabang_id,
                      uni2.cabang_name,
                      uni2.depo_id,
                      uni2.depo_name,
                      uni2.divisi_id,
                      uni2.divisi_name,
                      uni2.departemen_id,
                      uni2.departemen_name,
                      uni2.jabatan_id,
                      uni2.jabatan_name
                    FROM
                    (
                        SELECT 
                          ".$db["master"].".employee_position.employee_id,
                          ".$db["master"].".company.company_id,
                          ".$db["master"].".company.company_name,
                          ".$db["master"].".company.company_initial,
                          ".$db["master"].".hrd_cabang.cabang_id,
                          ".$db["master"].".hrd_cabang.cabang_name,
                          ".$db["master"].".depo.depo_id,
                          ".$db["master"].".depo.depo_name,
                          ".$db["master"].".hrd_divisi.divisi_id,
                          ".$db["master"].".hrd_divisi.divisi_name,
                          ".$db["master"].".hrd_departemen.departemen_id,
                          ".$db["master"].".hrd_departemen.departemen_name,
                          ".$db["master"].".jabatan.jabatan_id,
                          ".$db["master"].".jabatan.jabatan_name 
                        FROM
                          ".$db["master"].".employee_position 
                          INNER JOIN ".$db["master"].".company 
                            ON ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id 
                          INNER JOIN ".$db["master"].".hrd_divisi 
                            ON ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id 
                          INNER JOIN ".$db["master"].".hrd_departemen 
                            ON ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id 
                          INNER JOIN ".$db["master"].".depo 
                            ON ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id 
                          INNER JOIN ".$db["master"].".hrd_cabang 
                            ON ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id 
                          INNER JOIN ".$db["master"].".jabatan 
                            ON ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id 
                        WHERE 
                          1
                        ORDER BY
                          ".$db["master"].".employee_position.`start_date` DESC
                    )AS uni2
                    WHERE
                        1
                    GROUP BY
                        uni2.employee_id
                )AS uni
                WHERE
                    1
                    ".$where_employee."
	        ";     
	        $qry = mysql_query($q);
	        while($row = mysql_fetch_array($qry))
	        {
	            list(
	                $employee_id,
	                $company_id,
	                $company_name,
	                $company_initial,
	                $cabang_id,
	                $cabang_name,
	                $depo_id,
	                $depo_name,
	                $divisi_id,
	                $divisi_name,
	                $departemen_id,
	                $departemen_name,
	                $jabatan_id,
	                $jabatan_name
	            ) = $row;
	            
	            $arr_data["company_id"][$employee_id] = $company_id;
	            $arr_data["company_name"][$employee_id] = $company_name;
	            $arr_data["company_initial"][$employee_id] = $company_initial;
	            $arr_data["cabang_id"][$employee_id] = $cabang_id;
	            $arr_data["cabang_name"][$employee_id] = $cabang_name;
	            $arr_data["depo_id"][$employee_id] = $depo_id;
	            $arr_data["depo_name"][$employee_id] = $depo_name;
	            $arr_data["divisi_id"][$employee_id] = $divisi_id;
	            $arr_data["divisi_name"][$employee_id] = $divisi_name;
	            $arr_data["departemen_id"][$employee_id] = $departemen_id;
	            $arr_data["departemen_name"][$employee_id] = $departemen_name;
	            $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
	            $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
	        } 
		}  
		
		// data status
		{
			$q = "
                SELECT 
                    tbl_fixed.employee_id,
                    tbl_fixed.v_status,
                    tbl_fixed.v_date
                FROM
                (
                    SELECT
                        uni.employee_id,
                        uni.v_status,
                        uni.v_date
                    FROM
                    (
                        SELECT
                            ".$db["master"].".employee_join.employee_id,
                            'join' as v_status,
                            ".$db["master"].".employee_join.join_date as v_date
                        FROM
                            ".$db["master"].".employee_join
                        WHERE
                            1
                            
                        UNION ALL
                        
                        SELECT
                            ".$db["master"].".employee_resign.employee_id,
                            'resign' as v_status,
                            ".$db["master"].".employee_resign.resign_date as v_date
                        FROM
                            ".$db["master"].".employee_resign
                        WHERE
                            1
                    ) as uni
                    
                    WHERE
                        1
                    GROUP BY
                        uni.employee_id,
                        uni.v_date
                    ORDER BY
                        uni.employee_id DESC,
                        uni.v_date DESC 
                )AS tbl_fixed
                WHERE
                    1
                    ".$where_status."
                GROUP BY 
                    tbl_fixed.employee_id 
	        ";       
	        $qry = mysql_query($q);
	        while($row = mysql_fetch_array($qry))
	        {
	            list($employee_id, $v_status, $v_date) = $row;   
	            
	            $arr_data["status_employee"][$employee_id] = $v_status;
	            $arr_data["status_date"][$employee_id] = $v_date;
	        }
		}    
    }
    
    $no_view = 1;
    foreach($arr_data["list_sid"] as $sid=>$val)
    {
        $remarks = $arr_data["remarks"][$sid];
        $employee_id = $arr_data["employee_id"][$sid];
        $employee_nik = $arr_data["employee_nik"][$sid];
        $employee_code_hrd = $arr_data["employee_code_hrd"][$sid];
        $employee_name = $arr_data["employee_name"][$sid];
        
        $company_id = $arr_data["company_id"][$employee_id];
        $company_name = $arr_data["company_name"][$employee_id];
        $company_initial = $arr_data["company_initial"][$employee_id];
        $cabang_id = $arr_data["cabang_id"][$employee_id];
        $cabang_name = $arr_data["cabang_name"][$employee_id];
        $depo_id = $arr_data["depo_id"][$employee_id];
        $depo_name = $arr_data["depo_name"][$employee_id];
        $divisi_id = $arr_data["divisi_id"][$employee_id];
        $divisi_name = $arr_data["divisi_name"][$employee_id];
        $departemen_id = $arr_data["departemen_id"][$employee_id];
        $departemen_name = $arr_data["departemen_name"][$employee_id];
        $jabatan_id = $arr_data["jabatan_id"][$employee_id];
        $jabatan_name = $arr_data["jabatan_name"][$employee_id];
        
        $status_employee = $arr_data["status_employee"][$employee_id];
        
        if($status_employee=="resign")
        {
            $bgcolor_status = "background:#FFCCFF;";
        }
        else
        {
            $bgcolor_status = "";    
        }
        
?>
<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
    <td style="<?php echo $bgcolor_status; ?>">
        <?php echo $no_view; ?>
        <input type="hidden" name="no[]" value="<?php echo $no; ?>">
        <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="<?php echo $sid; ?>">
        <input type="hidden" name="v_employee_id_<?php echo $no; ?>" id="v_employee_id_<?php echo $no; ?>" value="<?php echo $employee_id; ?>">
    </td>
    <td style="<?php echo $bgcolor_status; ?>" ><nobr><span id="show_employee_nik_<?php echo $no; ?>"><?php echo $employee_nik; ?></span>
    	
		<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" name="btn_emp" id="btn_emp" value="..." onclick="pop_up_search_employee_details('<?php echo $no; ?>')">
			<i class="entypo-dot-3"></i>
		</button>
   	<td style="<?php echo $bgcolor_status; ?>" id="show_employee_code_hrd_<?php echo $no; ?>"><?php echo $employee_code_hrd; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" id="show_employee_name_<?php echo $no; ?>"><?php echo $employee_name; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" id="show_company_name_<?php echo $no; ?>"><?php echo $company_initial; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" id="show_cabang_name_<?php echo $no; ?>"><?php echo $cabang_name; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" id="show_divisi_name_<?php echo $no; ?>"><?php echo $divisi_name; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" id="show_jabatan_name_<?php echo $no; ?>"><?php echo $jabatan_name; ?>&nbsp;</td>
    <td style="<?php echo $bgcolor_status; ?>" ><input type="text" class="form-control-new" size="15" maxlength="255" name="v_remarks_<?php echo $no; ?>" id="v_remarks_<?php echo $no; ?>" value="<?php echo $remarks; ?>"></td>
    <td style="<?php echo $bgcolor_status; ?>" align="center">
    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Simpan" title="" name="btn_save_ajax" id="btn_save_ajax" value="Save" onClick="CallAjaxForm('save_ajax','<?php echo $no; ?>')">
			<i class="entypo-floppy"></i>
		</button>
	</td>
    <td style="<?php echo $bgcolor_status; ?>" align="center"><input type="checkbox" name="del[]" value="<?php echo $sid; ?>"></td>
</tr>
<?php
        $no_view++;
        $no++; 
    }  
 
    if($id!="")
    {
    	if(count($arr_data["list_sid"])*1>0)
    	{
    ?>
		<tr>     
		<td colspan="10">&nbsp;</td>
		<td align="center">
			
        	<button type="submit" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" name="btn_del_details" id="btn_del_details" value="<?php echo $show_btn; ?>" >
				<i class="entypo-trash"></i>
			</button>
		</td>
		</tr>
    <?php 
    	}
	}
 	?>
 	
	</tbody>
</table>