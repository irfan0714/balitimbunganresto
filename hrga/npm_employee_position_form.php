<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    if(!isset($_GET["v_from"])){ $v_from = isset($_GET["v_from"]); } else { $v_from = $_GET["v_from"]; }
    if(!isset($_GET["v_to"])){ $v_to = isset($_GET["v_to"]); } else { $v_to = $_GET["v_to"]; }
    
    if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
        
    if($ajax)
    {
        if($ajax=="change_ajax_cabang")
        {
            $v_cabang_id = $_GET["v_cabang_id"];
            
            ?>
            <select class="form-control-new" name="v_depo_id" id="v_depo_id" style="width:200px;">
                <?php 
                    $q = "
                            SELECT
                                ".$db["master"].".depo.depo_id,
                                ".$db["master"].".depo.depo_name
                            FROM
                                ".$db["master"].".depo
                            WHERE
                                1
                                AND ".$db["master"].".depo.cabang_id = '".$v_cabang_id."'
                            ORDER BY
                                ".$db["master"].".depo.depo_name ASC
                    ";
                    $qry_depo = mysql_query($q);
                    while($row_depo = mysql_fetch_array($qry_depo))
                    {
                        $selected = "";
                        if($data["depo_id"]==$row_depo["depo_id"])
                        {
                            $selected = "selected='selected'";
                        }
                ?>
                <option <?php echo $selected; ?> value="<?php echo $row_depo["depo_id"]; ?>"><?php echo $row_depo["depo_name"]; ?></option>
                <?php 
                    }
                ?>
            </select>
            <?php
        }
        
        exit();
    }
    
    $link_adjust = "?v_keyword=".$v_keyword."&v_from=".$v_from."&v_to=".$v_to."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&v_from=".$v_from."&v_to=".$v_to."&p=".$p;
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Posisi Karyawan";
    $list  = "npm_employee_position.php";
    $htm   = "npm_employee_position_form.php";
    $pk    = "sid";

    $jml_cek = 0;
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Failed Delete Because relation data with table Cabang";
        }
        else
        {
            $q = "
                    DELETE FROM
                        ".$db["master"].".employee_position
                    WHERE
                        sid = '".$id."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Delete ||".$q;
            }
            else
            {
                header("Location: ".$list.$link_back.$link_order_by);
            }  
        }
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
        $v_status              = save_char($_POST["v_status"]);
        $v_employee_id         = save_char($_POST["v_employee_id"]);
        $v_employee_name       = save_char($_POST["v_employee_name"]);
        $v_start_date          = format_save_date($_POST["v_start_date"]);
        $v_end_date            = format_save_date($_POST["v_end_date"]);
        $v_position_id         = save_char($_POST["v_position"]);
        $v_level_id		       = save_char($_POST["v_level"]);
        
        $v_company_id          = save_char($_POST["v_company_id"]);
        $v_divisi_id           = save_char($_POST["v_divisi_id"]);
        $v_departemen_id       = save_char($_POST["v_departemen_id"]);
        $v_jabatan_id          = save_char($_POST["v_jabatan_id"]);
        $v_depo_id             = save_char($_POST["v_depo_id"]);
        
        $v_remarks             = save_char($_POST["v_remarks"]);
        
		$chk_acting          = $_POST["chk_acting"];
		$v_start_date_acting = $_POST["v_start_date_acting"];
		$v_end_date_acting   = $_POST["v_end_date_acting"];
		$v_remarks_acting    = save_char($_POST["v_remarks_acting"]);
		
		if($chk_acting)
		{
			$where_acting = "
				status_acting = '1',
				start_acting = '".format_save_date($v_start_date_acting)."',
				end_acting = '".format_save_date($v_end_date_acting)."',
				remarks_acting = '".$v_remarks_acting."',
			";
		}
		else
		{
			$where_acting = "
				status_acting = '0',
				start_acting = '',
				end_acting = '',
				remarks_acting = '',
			";
		}
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                        UPDATE
                            ".$db["master"].".employee_position
                        SET
                            employee_id = '".$v_employee_id."',
                            start_date = '".$v_start_date."',
                            end_date = '".$v_end_date."',
                            v_status = '".$v_status."',
                            company_id = '".$v_company_id."',
                            divisi_id = '".$v_divisi_id."',
                            departemen_id = '".$v_departemen_id."',
                            jabatan_id = '".$v_jabatan_id."',
                            level_id = '".$v_level_id."',
                            depo_id = '".$v_depo_id."',
                            remarks = '".$v_remarks."', 
                            ".$where_acting."
                            edited = '".ffclock()."'
                        WHERE
                            sid = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update ||".$q;
                }
                else
                {
                    update_employee_join_resign($v_employee_id);
                    $class_warning = "success";
                    $msg = "Successfull Update ".$v_employee_name."<br>";   
                }
            }
        }
        else
        {   
            $id = get_counter_int($db["master"],"employee_position","sid",100);   
            
            $q = "
                    INSERT INTO
                        ".$db["master"].".employee_position
                    SET
                        sid = '".$id."',
                        employee_id = '".$v_employee_id."',
                        start_date = '".$v_start_date."',
                        end_date = '".$v_end_date."',
                        v_status = '".$v_status."',
                        company_id = '".$v_company_id."',
                        divisi_id = '".$v_divisi_id."',
                        departemen_id = '".$v_departemen_id."',
                        jabatan_id = '".$v_jabatan_id."',
                        level_id = '".$v_level_id."',
                        depo_id = '".$v_depo_id."',
                        remarks = '".$v_remarks."',
                        ".$where_acting."
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert ||".$q;
            }
            else
            {
                $q = "
                        INSERT INTO
                            ".$db["master"].".employee_position_log
                        SET
                            sid = '".$id."',
                            employee_id = '".$v_employee_id."',
                            start_date = '".$v_start_date."',
                            end_date = '".$v_end_date."',
                            position_id = '".$v_position_id."',    
                            remarks = '".$v_remarks."',
                            author = '".ffclock()."',
                            edited = '".ffclock()."'
                ";
                mysql_query($q);
                
                update_employee_join_resign($v_employee_id);
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id);
                
                $class_warning = "success";
                $msg = "<strong>Berhasil menyimpan!</strong>";
            }
        }
    }
    
    $q = "
        SELECT 
            ".$db["master"].".employee_position.sid,
            ".$db["master"].".employee_position.start_date,
            ".$db["master"].".employee_position.end_date,
            ".$db["master"].".employee.employee_id,
            ".$db["master"].".employee.employee_nik,
            ".$db["master"].".employee.employee_code_hrd,
            ".$db["master"].".employee.employee_name,
            ".$db["master"].".employee_position.v_status,
            ".$db["master"].".employee_position.level_id,
            ".$db["master"].".employee_position.remarks,
            ".$db["master"].".employee_position.status_acting,
            ".$db["master"].".employee_position.start_acting,
            ".$db["master"].".employee_position.end_acting,
            ".$db["master"].".employee_position.remarks_acting,
            ".$db["master"].".employee_position.author,
            ".$db["master"].".employee_position.edited,
            
            ".$db["master"].".company.company_id,
            ".$db["master"].".company.company_name,
            ".$db["master"].".company.company_initial,
            
            ".$db["master"].".hrd_divisi.divisi_id,
            ".$db["master"].".hrd_divisi.divisi_name,
            
            ".$db["master"].".hrd_departemen.departemen_id,
            ".$db["master"].".hrd_departemen.departemen_name,
            
            ".$db["master"].".hrd_cabang.cabang_id,
            ".$db["master"].".hrd_cabang.cabang_name,
            
            ".$db["master"].".depo.depo_id,
            ".$db["master"].".depo.depo_name,
            
            ".$db["master"].".jabatan.jabatan_id,
            ".$db["master"].".jabatan.jabatan_name
            
            
        FROM 
            ".$db["master"].".employee_position
            INNER JOIN ".$db["master"].".employee ON
                ".$db["master"].".employee.employee_id = ".$db["master"].".employee_position.employee_id
                
            INNER JOIN ".$db["master"].".company ON
                ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
            
            INNER JOIN ".$db["master"].".hrd_divisi ON
                ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
            
            INNER JOIN ".$db["master"].".hrd_departemen ON
                ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
            
            INNER JOIN ".$db["master"].".depo ON
                ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
            INNER JOIN ".$db["master"].".hrd_cabang ON
                ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
            
            INNER JOIN ".$db["master"].".jabatan ON
                ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
        WHERE
            1
            AND ".$db["master"].".employee_position.sid = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    
    $where_display = "display: none;";
    $where_cheked="";
    if($data["status_acting"]*1==1)
    {
    	$where_cheked = 'checked="checked"';
   	 	$where_display = "";
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
	    function validasi()
	    {
	        if(document.getElementById("v_start_date").value=="")
	        {
	            alert("Mulai harus dipilih");
	            document.getElementById("v_start_date").focus();
	            return false;
	        }
	        else if(document.getElementById("v_employee_id").value=="")
	        {
	            alert("Karyawan harus dipilih");
	            document.getElementById("v_employee_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_company_id").value=="")
	        {
	            alert("Perusahaan harus dipilih");
	            document.getElementById("v_company_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_divisi_id").value=="")
	        {
	            alert("Divisi harus dipilih");
	            document.getElementById("v_divisi_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_departemen_id").value=="")
	        {
	            alert("Departemen harus dipilih");
	            document.getElementById("v_departemen_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_departemen_id").value=="")
	        {
	            alert("Departemen harus dipilih");
	            document.getElementById("v_departemen_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_jabatan_id").value=="")
	        {
	            alert("Jabatan harus dipilih");
	            document.getElementById("v_jabatan_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_cabang_id").value=="")
	        {
	            alert("Cabang harus dipilih");
	            document.getElementById("v_cabang_id").focus();
	            return false;
	        }
	        else if(document.getElementById("v_depo_id").value=="")
	        {
	            alert("Depo harus dipilih");
	            document.getElementById("v_depo_id").focus();
	            return false;
	        }
	        else if(document.getElementById("chk_acting").checked)
	        {
	        	if(document.getElementById("v_start_date_acting").value=="")
		        {
		            alert("Tanggal Mulai harus diisi");
		            document.getElementById("v_start_date_acting").focus();
		            return false;
		        }
	        	else if(document.getElementById("v_end_date_acting").value=="")
		        {
		            alert("Tanggal Akhir harus diisi");
		            document.getElementById("v_end_date_acting").focus();
		            return false;
		        }
	        	else if(document.getElementById("v_remarks_acting").value=="")
		        {
		            alert("Remarks Acting harus diisi");
		            document.getElementById("v_remarks_acting").focus();
		            return false;
		        }
	        }
	        
	        
	    }
	    
	    function start_page()
	    {
	        //document.getElementById("v_start_date").focus();    
	    }
	    
	    function CallAjaxForm(vari,param1,param2){
	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        
	        document.getElementById('show_image_ajax_form').style.display = '';
	        
	        if(vari == 'change_ajax_cabang')
	        {
	            var variabel;
	            
	            variabel  = '';
	            variabel += 'v_cabang_id='+param1;
	            
	            //alert(variabel);
	            xmlhttp.open('get', 'npm_employee_position_form.php?ajax=change_ajax_cabang&'+variabel, true);
	            xmlhttp.onreadystatechange = function() {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)){
	                    document.getElementById('show_depo').innerHTML = xmlhttp.responseText;
	                    document.getElementById('show_image_ajax_form').style.display = 'none';
	                }
	                return false;
	            }
	            xmlhttp.send(null);
	            
	        }
	    }
	    
	    function chk_status_acting()
	    {
	    	var chk_acting = document.getElementById("chk_acting").checked;
	    	
	    	if(chk_acting)
			{
				document.getElementById('tr_acting').style.display    = '';  
			}  
			else
			{
				document.getElementById('tr_acting').style.display    = 'none';  
			}
			
		}
		
	</script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        	
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="v_employee_id_old" id="v_employee_id_old" value="<?php echo $data["employee_id"]; ?>">
                <input type="hidden" name="v_start_date_old" id="v_start_date_old" value="<?php echo format_show_date($data["start_date"]); ?>">
                <input type="hidden" name="v_end_date_old" id="v_end_date_old" value="<?php if($data["end_date"]!="0000-00-00") echo format_show_date($data["end_date"]); ?>">
                <input type="hidden" name="v_position_old" id="v_position_old" value="<?php echo $data["position_id"]; ?>">
                <input type="hidden" name="v_remarks_old" id="v_remarks_old" value="<?php echo $data["remarks"]; ?>">
			    <table class="table table-bordered responsive">
                    
                    <tr>
                        <td class="title_table" width="150">Mulai</td>
                        <td> 
                            <input type="text" class="form-control-new datepicker" size="10" maxlength="10" name="v_start_date" id="v_start_date" value="<?php echo format_show_date($data["start_date"]); ?>">
                            &nbsp;&nbsp;
                            <b>Akhir</b>
                            <input type="text" class="form-control-new datepicker" size="10" maxlength="10" name="v_end_date" id="v_end_date" value="<?php if($data["end_date"]!="0000-00-00") echo format_show_date($data["end_date"]); ?>">
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Status Posisi</td>
                        <td> 
                            <select name="v_status" id="v_status" class="form-control-new">
                                <option value="">-</option>
                                <option value="Mutasi" <?php if($data["v_status"]=="Mutasi") echo "selected='selected'"; ?>>Mutasi</option>
                                <option value="Promosi" <?php if($data["v_status"]=="Promosi") echo "selected='selected'"; ?>>Promosi</option>
                                <option value="Demosi" <?php if($data["v_status"]=="Demosi") echo "selected='selected'"; ?>>Demosi</option>
                            </select>
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Level</b></td>
                        <td> 
                            <select name="v_level" id="v_level" class="form-control-new">
                                <option value="">-</option>
                                <?php
                                $q="
                                	SELECT 
									  * 
									FROM
									  ".$db["master"].".level 
									WHERE 1 
									ORDER BY ".$db["master"].".level.level_id ASC
                                ";
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
									list($level_id,$level_name,$level_pa)=$row;
									
									?>
									<option value="<?php echo $level_id; ?>" <?php if($data["level_id"]==$level_id) echo "selected='selected'"; ?>><?php echo $level_name; ?></option>
									<?php
								}
                                ?>
                            </select>
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">NIK</b></td>
                        <td> 
                            <input type="hidden" value="<?php echo $data["employee_id"]; ?>" name="v_employee_id" id="v_employee_id">
                            <input type="text" class="form-control-new" disabled="" value="<?php echo $data["employee_nik"]; ?>" name="v_employee_nik" id="v_employee_nik" maxlength="255" size="43" title="Cari Karyawan" readonly="readonly" style="cursor:pointer;">
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" name="btn_search_employee" id="btn_search_employee" onclick="windowOpener(600, 800, 'Search Employee', 'npm_employee_search.php', 'Search Employee')">
								<i class="entypo-search"></i>
							</button>
                    	</td>
                    </tr>                                             
                            
                    <tr>
                        <td class="title_table">Kode HRD</b></td>
                        <td> 
                            <input type="text" class="form-control-new" disabled="" value="<?php echo $data["employee_code_hrd"]; ?>" name="v_employee_code_hrd" id="v_employee_code_hrd" maxlength="255" size="50" title="Cari Karyawan" readonly="readonly" style="cursor:pointer;" onclick="windowOpener(600, 800, 'Search Employee', 'employee_search.php', 'Search Employee')">
                        </td>
                    </tr> 
                                                   
                    <tr>
                        <td class="title_table">Nama</b></td>
                        <td> 
                            <input type="text" class="form-control-new" disabled="" value="<?php echo $data["employee_name"]; ?>" name="v_employee_name" id="v_employee_name" maxlength="255" size="50" title="Cari Karyawan" readonly="readonly" style="cursor:pointer;" onclick="windowOpener(600, 800, 'Search Employee', 'employee_search.php', 'Search Employee')">
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Perusahaan</b></td>
                        <td>
                            <select class="form-control-new" name="v_company_id" id="v_company_id" style="width:200px;">
                                <option value="">-Pilih Perusahaan-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".company.company_id,
                                                ".$db["master"].".company.company_name
                                            FROM
                                                ".$db["master"].".company
                                            WHERE
                                                1
                                            ORDER BY
                                                ".$db["master"].".company.company_name ASC
                                    ";
                                    $qry_comp = mysql_query($q);
                                    while($row_comp = mysql_fetch_array($qry_comp))
                                    {
                                        $selected = "";
                                        if($data["company_id"]==$row_comp["company_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_comp["company_id"]; ?>"><?php echo $row_comp["company_name"]; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                            &nbsp;&nbsp;
                            <b>Divisi</b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <select class="form-control-new" name="v_divisi_id" id="v_divisi_id" style="width:200px;">
                                <option value="">-Pilih Divisi-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".hrd_divisi.divisi_id,
                                                ".$db["master"].".hrd_divisi.divisi_name
                                            FROM
                                                ".$db["master"].".hrd_divisi
                                            WHERE
                                                1
                                            ORDER BY
                                                ".$db["master"].".hrd_divisi.divisi_name ASC
                                    ";
                                    $qry_divisi = mysql_query($q);
                                    while($row_divisi = mysql_fetch_array($qry_divisi))
                                    {
                                        $selected = "";
                                        if($data["divisi_id"]==$row_divisi["divisi_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_divisi["divisi_id"]; ?>"><?php echo $row_divisi["divisi_name"]; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Departemen</b></td>
                        <td>
                            <select class="form-control-new" name="v_departemen_id" id="v_departemen_id" style="width:200px;">
                                <option value="">-Pilih Departemen-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".hrd_departemen.departemen_id,
                                                ".$db["master"].".hrd_departemen.departemen_name
                                            FROM
                                                ".$db["master"].".hrd_departemen
                                            WHERE
                                                1
                                            ORDER BY
                                                ".$db["master"].".hrd_departemen.departemen_name ASC
                                    ";
                                    $qry_departemen = mysql_query($q);
                                    while($row_departemen = mysql_fetch_array($qry_departemen))
                                    {
                                        $selected = "";
                                        if($data["departemen_id"]==$row_departemen["departemen_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_departemen["departemen_id"]; ?>"><?php echo $row_departemen["departemen_name"]; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                            &nbsp;&nbsp;
                            <b>Jabatan</b>
                            &nbsp;&nbsp;&nbsp;
                            <select class="form-control-new" name="v_jabatan_id" id="v_jabatan_id" style="width:200px;">
                                <option value="">-Pilih Jabatan-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".jabatan.jabatan_id,
                                                ".$db["master"].".jabatan.jabatan_name
                                            FROM
                                                ".$db["master"].".jabatan
                                            WHERE
                                                1
                                            ORDER BY
                                                ".$db["master"].".jabatan.jabatan_name ASC
                                    ";
                                    $qry_jabatan = mysql_query($q);
                                    while($row_jabatan = mysql_fetch_array($qry_jabatan))
                                    {
                                        $selected = "";
                                        if($data["jabatan_id"]==$row_jabatan["jabatan_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_jabatan["jabatan_id"]; ?>"><?php echo $row_jabatan["jabatan_name"]; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Cabang</b></td>
                        <td>
                             <select class="form-control-new" name="v_cabang_id" id="v_cabang_id" style="width:200px;" onchange="CallAjaxForm('change_ajax_cabang',this.value)">
                                <option value="">-Pilih Cabang-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".hrd_cabang.cabang_id,
                                                ".$db["master"].".hrd_cabang.cabang_name
                                            FROM
                                                ".$db["master"].".hrd_cabang
                                            WHERE
                                                1
                                            ORDER BY
                                                ".$db["master"].".hrd_cabang.cabang_name ASC
                                    ";
                                    $qry_cabang = mysql_query($q);
                                    while($row_cabang = mysql_fetch_array($qry_cabang))
                                    {
                                        $selected = "";
                                        if($data["cabang_id"]==$row_cabang["cabang_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                        
                                        $cabang_name = str_replace("Cabang","",$row_cabang["cabang_name"]);
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_cabang["cabang_id"]; ?>"><?php echo $cabang_name; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                            &nbsp;&nbsp;
                            <b>Depo</b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span id="show_depo">
                            <select class="form-control-new" name="v_depo_id" id="v_depo_id" style="width:200px;">
                                <option value="">-Pilih Depo-</option>
                                <?php 
                                    $q = "
                                            SELECT
                                                ".$db["master"].".depo.depo_id,
                                                ".$db["master"].".depo.depo_name
                                            FROM
                                                ".$db["master"].".depo
                                            WHERE
                                                1
                                                AND ".$db["master"].".depo.cabang_id = '".$data["cabang_id"]."'
                                            ORDER BY
                                                ".$db["master"].".depo.depo_name ASC
                                    ";
                                    $qry_depo = mysql_query($q);
                                    while($row_depo = mysql_fetch_array($qry_depo))
                                    {
                                        $selected = "";
                                        if($data["depo_id"]==$row_depo["depo_id"])
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_depo["depo_id"]; ?>"><?php echo $row_depo["depo_name"]; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Status Acting</b></td>
                        <td>
                        	<label><input type="checkbox" value="1" name="chk_acting" id="chk_acting" onclick="chk_status_acting();" <?php echo $where_cheked; ?>/>&nbsp;Cheklist Jika ada status acting</label>
                        </td>
                    </tr>
                            
                    <tr id="tr_acting" style="<?php echo $where_display; ?>">
                        <td class="title_table">Tanggal Mulai</b></td>
                        <td>
                        	<input type="text" class="form-control-new datepicker" name="v_start_date_acting" id="v_start_date_acting" value="<?php echo format_show_date($data["start_acting"]); ?>" size="10" maxlength="10"/>
                       		&nbsp;&nbsp;&nbsp;<b>Akhir</b>
                       		<input type="text" class="form-control-new datepicker" name="v_end_date_acting" id="v_end_date_acting" value="<?php echo format_show_date($data["end_acting"]); ?>" size="10" maxlength="10"/>
                        	&nbsp;&nbsp;&nbsp;<b>Keterangan</b>
                        	<input type="text" class="form-control-new" value="<?php echo $data["remarks_acting"]; ?>" name="v_remarks_acting" id="v_remarks_acting" maxlength="100" size="50">
                        </td>
                    </tr>
                            
                    <tr>
                        <td class="title_table">Keterangan</b></td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["remarks"]; ?>" name="v_remarks" id="v_remarks" maxlength="255" size="50">
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
			    
			    </table>			    
			    </form>     
        	
        		<?php 
				if($id!="")
				{
				?>
					<ol class="breadcrumb">
						<li><strong><i class="entypo-vcard"></i>Information data</strong></li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		        ?>
		         
        	</div>
    	</div>
    

<?php include("footer.php"); ?>