<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "FA Group Asset";
    $list  = "npm_fa_group_asset.php";
    $htm   = "npm_fa_group_asset_form.php";
    $pk    = "group_asset_id";
    $title = "group_asset_name";
    
	$q_cek = "
		SELECT
			uni.group_asset_id
		FROM
		(
			SELECT
				group_asset_id
			FROM
				fa_type
			WHERE
				group_asset_id = '".$id."'
		) as uni
		LIMIT
			0,1
	";
	
	$qry_cek = mysql_query($q_cek);
	$jml_cek = mysql_num_rows($qry_cek);
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if($v_cek==isset($_GET["cek"]))
    {   
        if($jml_cek*1 > 0)
        {
            $msg = "Data tidak bisa dihapus, karena ada relasi dengan FA TYPE";
        }
        else
        {
	
			$q = "
					DELETE FROM
						fa_group_asset
					WHERE
						group_asset_id = '".$id."'
			";
			if(!mysql_query($q))
			{
				$msg = "Failed Delete";
			}
			else
			{
                $q = "
                    DELETE FROM
                        fa_group_asset_detail
                    WHERE
                        group_asset_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    die("Gagal Delete Detail");
                }
                
				header("Location: ".$list.$link_back.$link_order_by);
			}  
		}
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
        $v_group_asset_name   = save_char($_POST["v_group_asset_name"]);
        $v_employee_id        = save_char($_POST["v_employee_id"]);
        
        $data_delete          = $_POST["data_delete"];
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        fa_group_asset
                    SET
                        group_asset_name = '".$v_group_asset_name."'
                    WHERE
                        group_asset_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update";
                }
                else
                {
                    if($v_employee_id)
                    {
                        $q = "
                            INSERT INTO
                                fa_group_asset_detail
                            SET
                               group_asset_id = '".$id."',
                               employee_id = '".$v_employee_id."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Insert fa_group_asset_detail";
                        }
                    }
                    
                    foreach($data_delete as $key=>$val)
                    {
                        $q = "
                            DELETE FROM
                                fa_group_asset_detail
                            WHERE
                                sid = '".$val."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Delete fa_group_asset_detail";
                        }        
                    }
                    
                    
                    $class_warning = "success";
                    $msg = "Berhasil menyimpan";
                }
            }
        }
        else
        {   
            $id = get_counter_int("","fa_group_asset","group_asset_id",100);   
            
            $q = "
                INSERT INTO
                    fa_group_asset
                SET
                    group_asset_id = '".$id."',
                    group_asset_name = '".$v_group_asset_name."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert";
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            fa_group_asset.`group_asset_id`,
            fa_group_asset.`group_asset_name`
        FROM 
            fa_group_asset
        WHERE
            1
            AND fa_group_asset.group_asset_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
    $q = "
        SELECT 
            fa_group_asset_detail.`sid`,
            fa_group_asset_detail.`employee_id`,
            employee.employee_nik,
            employee.employee_name
        FROM 
            fa_group_asset_detail
            INNER JOIN employee ON
                fa_group_asset_detail.employee_id = employee.employee_id 
        WHERE
            1
            AND fa_group_asset_detail.group_asset_id = '".$id."'
        ORDER BY
            fa_group_asset_detail.`sid` ASC
    ";
    $qry_det = mysql_query($q);
    while($row_det = mysql_fetch_array($qry_det))
    {
        list($sid, $employee_id, $employee_nik, $employee_name) = $row_det;
        
        $arr_data["list_data"][$employee_id] = $employee_id;    
        $arr_data["sid"][$employee_id] = $sid;
        
        $arr_data["employee_nik"][$employee_id] = $employee_nik;    
        $arr_data["employee_name"][$employee_id] = $employee_name;    
    }
    
    if(count($arr_data["list_data"])*1>0)
    {
        $where_employee_id = where_array($arr_data["list_data"], "uni.employee_id");
        
        $q = "
                SELECT
                    uni.employee_id,
                    uni.company_id,
                    uni.company_name,
                    uni.company_initial,
                    uni.cabang_id,
                    uni.cabang_name,
                    uni.depo_id,
                    uni.depo_name,
                    uni.divisi_id,
                    uni.divisi_name,
                    uni.departemen_id,
                    uni.departemen_name,
                    uni.jabatan_id,
                    uni.jabatan_name
                FROM
                (
                    SELECT
                      uni2.employee_id,
                      uni2.company_id,
                      uni2.company_name,
                      uni2.company_initial,
                      uni2.cabang_id,
                      uni2.cabang_name,
                      uni2.depo_id,
                      uni2.depo_name,
                      uni2.divisi_id,
                      uni2.divisi_name,
                      uni2.departemen_id,
                      uni2.departemen_name,
                      uni2.jabatan_id,
                      uni2.jabatan_name
                    FROM
                    (
                        SELECT 
                          hrd.employee_position.employee_id,
                          hrd.company.company_id,
                          hrd.company.company_name,
                          hrd.company.company_initial,
                          hrd.cabang.cabang_id,
                          hrd.cabang.cabang_name,
                          hrd.depo.depo_id,
                          hrd.depo.depo_name,
                          hrd.divisi.divisi_id,
                          hrd.divisi.divisi_name,
                          hrd.departemen.departemen_id,
                          hrd.departemen.departemen_name,
                          hrd.jabatan.jabatan_id,
                          hrd.jabatan.jabatan_name 
                        FROM
                          hrd.employee_position 
                          INNER JOIN hrd.company 
                            ON hrd.employee_position.company_id = hrd.company.company_id 
                          INNER JOIN hrd.divisi 
                            ON hrd.employee_position.divisi_id = hrd.divisi.divisi_id 
                          INNER JOIN hrd.departemen 
                            ON hrd.employee_position.departemen_id = hrd.departemen.departemen_id 
                          INNER JOIN hrd.depo 
                            ON hrd.employee_position.depo_id = hrd.depo.depo_id 
                          INNER JOIN hrd.cabang 
                            ON hrd.depo.cabang_id = hrd.cabang.cabang_id 
                          INNER JOIN hrd.jabatan 
                            ON hrd.employee_position.jabatan_id = hrd.jabatan.jabatan_id 
                        WHERE 
                          1
                        ORDER BY
                          hrd.employee_position.`start_date` DESC
                    )AS uni2
                    WHERE
                        1
                    GROUP BY
                        uni2.employee_id
                )AS uni
                WHERE
                    1
                    ".$where_employee_id."
        ";      
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list(
                $employee_id,
                $company_id,
                $company_name,
                $company_initial,
                $cabang_id,
                $cabang_name,
                $depo_id,
                $depo_name,
                $divisi_id,
                $divisi_name,
                $departemen_id,
                $departemen_name,
                $jabatan_id,
                $jabatan_name
            ) = $row;
            
            $arr_data["company_id"][$employee_id] = $company_id;
            $arr_data["company_name"][$employee_id] = $company_name;
            $arr_data["company_initial"][$employee_id] = $company_initial;
            $arr_data["cabang_id"][$employee_id] = $cabang_id;
            $arr_data["cabang_name"][$employee_id] = $cabang_name;
            $arr_data["depo_id"][$employee_id] = $depo_id;
            $arr_data["depo_name"][$employee_id] = $depo_name;
            $arr_data["divisi_id"][$employee_id] = $divisi_id;
            $arr_data["divisi_name"][$employee_id] = $divisi_name;
            $arr_data["departemen_id"][$employee_id] = $departemen_id;
            $arr_data["departemen_name"][$employee_id] = $departemen_name;
            $arr_data["jabatan_id"][$employee_id] = $jabatan_id;
            $arr_data["jabatan_name"][$employee_id] = $jabatan_name;
            
            $arr_data["list_posisi"][$employee_id] = $employee_id;
        }    
    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
		function validasi()
		{
			if(document.getElementById("v_group_asset_name").value=="")
			{
				alert("Group Asset harus diisi...");
				document.getElementById("v_group_asset_name").focus();
				return false;
			}
		}
		
		function start_page()
		{
			document.getElementById("v_group_asset_name").focus();	
		}
	    
	    function pop_up_employee()
	    {
	        windowOpener(600, 800, 'Search Employee', 'npm_fa_group_asset_pop_up_employee.php', 'Search Employee')
	    }
	     
		function mouseover(target)
		{  
		    if(target.bgColor!="#cafdb5"){        
		        if (target.bgColor=='#ccccff')
		            target.bgColor='#ccccff';
		        else
		            target.bgColor='#c1cdd8';
		    }
		}
		    
		function mouseout(target)
		{
		    if(target.bgColor!="#cafdb5"){ 
		        if (target.bgColor=='#ccccff')
		            target.bgColor='#ccccff';
		        else
		            target.bgColor='#FFFFFF';
		    }    
		}

		function mouseclick(target, idobject, num)
		{            
		    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
		    for(i=0;i<num;i++){
		        if (document.getElementById(idobject+'_'+i) != undefined){
		            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
		            if (target.id == idobject+'_'+i)
		                target.bgColor='#ccccff';
		        }
		    }
		}

		function mouseclick1(target)
		{
		    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
		    if(target.bgColor!="#cafdb5")
		    {
		        target.bgColor="#cafdb5";
		    }
		    else
		    {
		        target.bgColor="#FFFFFF";
		    }
		}     
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    	<tr>
                        <td class="title_table" width="150">Group Asset</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["group_asset_name"]; ?>" name="v_group_asset_name" id="v_group_asset_name" maxlength="255" size="50">
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>

						    <?php 
						        if($show_btn=="Delete")
						        {
						    ?>		
						    		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						    <?php 
						        }
						        else
						        {
						    ?>
						    		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						   	<?php 
						        }
						    ?>
						    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
					</tr>
					
                    <tr>
                        <td colspan="100%">
                            <table class="table table-bordered responsive">
                            	<thead>
									<tr>
	                                    <th width="30">No</th>
	                                    <th>NIK</th>
	                                    <th>Name</th>
	                                    <th>Perusahaan</th>
	                                    <th>Cabang</th>
	                                    <th>Depo</th>
	                                    <th>Divisi</th>
	                                    <th>Jabatan</th>
	                                    <th width="30">Delete</th>
									</tr>
								</thead>
								
								<tbody>
	                                <tr>
	                                    <td width="30">
	                                        <b>ADD</b>
	                                        <input type="hidden" name="v_employee_id" id="v_employee_id" value="" />
	                                    </td>
	                                    <td><span id="td_employee_nik"></span>&nbsp;
                                        	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" onclick="pop_up_employee()">
												<i class="entypo-dot-2"></i>
											</button>
	                                    </td>
	                                    <td id="td_employee_name">&nbsp;</td>
	                                    <td id="td_company_initial">&nbsp;</td>
	                                    <td id="td_cabang_name">&nbsp;</td>
	                                    <td id="td_depo_name">&nbsp;</td>
	                                    <td id="td_divisi_name">&nbsp;</td>
	                                    <td id="td_jabatan_name">&nbsp;</td>
	                                    <td>&nbsp;</td>
	                                </tr>
	                                
	                                <?php
	                                    $no = 1; 
	                                    foreach($arr_data["list_data"] as $employee_id=>$val)
	                                    {
	                                        $sid = $arr_data["sid"][$employee_id];
	                                        $employee_nik = $arr_data["employee_nik"][$employee_id];    
	                                        $employee_name = $arr_data["employee_name"][$employee_id];    
	                                        
	                                        $company_id = $arr_data["company_id"][$employee_id];
	                                        $company_name = $arr_data["company_name"][$employee_id];
	                                        $company_initial = $arr_data["company_initial"][$employee_id];
	                                        $cabang_id = $arr_data["cabang_id"][$employee_id];
	                                        $cabang_name = $arr_data["cabang_name"][$employee_id];
	                                        $depo_id = $arr_data["depo_id"][$employee_id];
	                                        $depo_name = $arr_data["depo_name"][$employee_id];
	                                        $divisi_id = $arr_data["divisi_id"][$employee_id];
	                                        $divisi_name = $arr_data["divisi_name"][$employee_id];
	                                        $departemen_id = $arr_data["departemen_id"][$employee_id];
	                                        $departemen_name = $arr_data["departemen_name"][$employee_id];
	                                        $jabatan_id = $arr_data["jabatan_id"][$employee_id];
	                                        $jabatan_name = $arr_data["jabatan_name"][$employee_id];

	                                        ?>
	                                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	                                            <td><?php echo $no; ?></td>
	                                            <td><?php echo $employee_nik; ?></td>
	                                            <td><?php echo $employee_name; ?></td>
	                                            <td><?php echo $company_initial; ?></td>
	                                            <td><?php echo $cabang_name; ?></td>
	                                            <td><?php echo $depo_name; ?></td>
	                                            <td><?php echo $divisi_name; ?></td>
	                                            <td><?php echo $jabatan_name; ?></td>
	                                            <td align="center">
	                                            	<input type="checkbox" name="data_delete[]" id="data_delete" value="<?php echo $sid; ?>" />
	                                            </td>
	                                        </tr>
	                                        <?php
	                                        $no++;
	                                    }
	                                ?>
									
								</tbody>                                
                            </table>
                        </td>
                    </tr>
                            
			    </table>			    
			    </form>
		         
			</div>
		</div>
		
<?php include("footer.php"); ?>      
                          