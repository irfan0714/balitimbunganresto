<?php 
require "database.php";
 
	//	$path = "D:/Back_Up"; // full server path to the directory where you want the backup files (no trailing slash)
		 
		//$mysqlpath 	= "C:/Program Files/mysql/bin/mysqldump";
		$mysqlpath 	= "mysqldump";
		$path 		= "D:/BackupNPM";
		
		$name = array("/backup_dbnatura_" . date("d-m-YH-i-s"),"/backup_hrd_" . date("d-m-YH-i-s"));
		$dbname = array("db_natura", "hrd");
		
		for($i=0; $i<count($name);$i++){
			$filename = $path . $name[$i] . ".sql";
			$zipName = $path . $name[$i] .".zip";
		
			if ( file_exists($filename) ) unlink($filename);
				exec("$mysqlpath --user=root --password= --host=localhost $dbname[$i] > $filename",$result,$error);
			if( $error > 0 ) { ?>
				<tr bordercolor="#FFFFFF">
				<td align= 'center'> Backup Database Natura Failed</td>
				</tr>
		<?php
			} 
			else{		 	
				$size = filesize($filename);
				switch ($size) {
				  case ($size>=1048576): $size = round($size/1048576) . " MB"; break;
				  case ($size>=1024): $size = round($size/1024) . " KB"; break;
				  default: $size = $size . " bytes"; break;
				}
				$message = "The database backup for db_natura has been run.";
				$msgweb  = $message."<br>";
				$msgfile = $message."\r\n";
				$message = "The return code was: " . $result;
				$msgweb  .= $message."<br>";
				$msgfile .= $message."\r\n";
				$message = "The file path is: " . $filename ;
				$msgweb  .= $message."<br>";
				$msgfile .= $message."\r\n";
				$message = "Size of the backup: " . $size ;
				$msgweb  .= $message."<br>";
				$msgfile .= $message."\r\n";
				$message = "Server time of the backup: " . date(" F d Y h:ia");
				$msgweb  .= $message."<br>";
				$msgfile .= $message."\r\n";
				$msgfile .= "Backup By : Administrator \r\n"; 
				$Handle = fopen($path."\LOG\LogDB.txt", 'a');	
				$Data = $msgfile."\r\n";
				fwrite($Handle, $Data);
				fclose($Handle);

				//mail($emailaddress, "Database Backup Message" , $message, "From: Website <>");
				createZip($filename,$name[$i].".sql",$zipName);
				echo "Backup Database Succeed";
				echo $msgweb;
				?>
	<?php			
			}
		}
?>
<?php
function createZip($dirName,$fileName,$zipName){
	$zip = new ZipArchive();
	if ($zip->open($zipName, ZIPARCHIVE::CREATE) !== TRUE) {
	    die ("Could not open archive");
	}
    $zip->addFile($dirName,$fileName) or die ("ERROR: Could not add file: $fileName");   
	$zip->close();
	unlink($dirName);
}

?>
