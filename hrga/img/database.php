<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = "default";
$active_record = TRUE;

$db['default']['hostname'] = "192.168.0.168";
$db['default']['username'] = "users3";
$db['default']['password'] = "usersukasukses";
$db['default']['database'] = "budget_marketing";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['temp']['hostname'] = "192.168.0.168";
$db['temp']['username'] = "users3";
$db['temp']['password'] = "usersukasukses";
$db['temp']['database'] = "budget_temp";
$db['temp']['dbdriver'] = "mysql";
$db['temp']['dbprefix'] = "";
$db['temp']['pconnect'] = FALSE;
$db['temp']['db_debug'] = TRUE;
$db['temp']['cache_on'] = FALSE;
$db['temp']['cachedir'] = "";
$db['temp']['char_set'] = "utf8";
$dby['temp']['dbcollat'] = "utf8_general_ci";

$db['dist']['hostname'] = "192.168.0.8";
$db['dist']['username'] = "root";
$db['dist']['password'] = "";
$db['dist']['database'] = "s3new";
$db['dist']['dbdriver'] = "mysql";
$db['dist']['dbprefix'] = "";
$db['dist']['pconnect'] = FALSE;
$db['dist']['db_debug'] = TRUE;
$db['dist']['cache_on'] = FALSE;
$db['dist']['cachedir'] = "";
$db['dist']['char_set'] = "utf8";
$db['dist']['dbcollat'] = "utf8_general_ci";
 
 $db['distrbr']['hostname'] = "192.168.0.251";//regbar
 $db['distrbr']['username'] = "users3cabang";
 $db['distrbr']['password'] = "usersukasukses";
 $db['distrbr']['database'] = "dbmarketing";
 $db['distrbr']['dbdriver'] = "mysql";
 $db['distrbr']['dbprefix'] = "";
 $db['distrbr']['pconnect'] = FALSE;
 $db['distrbr']['db_debug'] = TRUE;
 $db['distrbr']['cache_on'] = FALSE;
 $db['distrbr']['cachedir'] = "";
 $db['distrbr']['char_set'] = "utf8";
 $db['distrbr']['dbcollat'] = "utf8_general_ci";
 $db['distrbr']['autoinit'] = FALSE;
 $db['distrbr']['stricton'] = FALSE;

 $db['distjk1']['hostname'] = "192.168.0.250";
 $db['distjk1']['username'] = "users3cabang";
 $db['distjk1']['password'] = "usersukasukses";
 $db['distjk1']['database'] = "dbmarketing";
 $db['distjk1']['dbdriver'] = "mysql";
 $db['distjk1']['dbprefix'] = "";
 $db['distjk1']['pconnect'] = FALSE;
 $db['distjk1']['db_debug'] = TRUE;
 $db['distjk1']['cache_on'] = FALSE;
 $db['distjk1']['cachedir'] = "";
 $db['distjk1']['char_set'] = "utf8";
 $db['distjk1']['dbcollat'] = "utf8_general_ci";
 $db['distjk1']['autoinit'] = TRUE;
 $db['distjk1']['stricton'] = FALSE;

$db['distjk2']['hostname'] = "192.168.5.45";
$db['distjk2']['username'] = "users3cabang";
$db['distjk2']['password'] = "usersukasukses";
$db['distjk2']['database'] = "dbmarketing";
$db['distjk2']['dbdriver'] = "mysql";
$db['distjk2']['dbprefix'] = "";
$db['distjk2']['pconnect'] = FALSE;
$db['distjk2']['db_debug'] = TRUE;
$db['distjk2']['cache_on'] = FALSE;
$db['distjk2']['cachedir'] = "";
$db['distjk2']['char_set'] = "utf8";
$db['distjk2']['dbcollat'] = "utf8_general_ci";
$db['distjk2']['autoinit'] = FALSE;
$db['distjk2']['stricton'] = FALSE;

$db['distbdg']['hostname'] = "192.168.3.1";
$db['distbdg']['username'] = "users3cabang";
$db['distbdg']['password'] = "usersukasukses";
$db['distbdg']['database'] = "dbmarketing";
$db['distbdg']['dbdriver'] = "mysql";
$db['distbdg']['dbprefix'] = "";
$db['distbdg']['pconnect'] = FALSE;
$db['distbdg']['db_debug'] = TRUE;
$db['distbdg']['cache_on'] = FALSE;
$db['distbdg']['cachedir'] = "";
$db['distbdg']['char_set'] = 'utf8';
$db['distbdg']['dbcollat'] = 'utf8_general_ci';
$db['distbdg']['autoinit'] = FALSE;
$db['distbdg']['stricton'] = FALSE;

$db['distmdn']['hostname'] = "192.168.8.1";
$db['distmdn']['username'] = "users3cabang";
$db['distmdn']['password'] = "usersukasukses";
$db['distmdn']['database'] = "dbmarketing";
$db['distmdn']['dbdriver'] = "mysql";
$db['distmdn']['dbprefix'] = "";
$db['distmdn']['pconnect'] = FALSE;
$db['distmdn']['db_debug'] = TRUE;
$db['distmdn']['cache_on'] = FALSE;
$db['distmdn']['cachedir'] = "";
$db['distmdn']['char_set'] = 'utf8';
$db['distmdn']['dbcollat'] = 'utf8_general_ci';
$db['distmdn']['autoinit'] = FALSE;
$db['distmdn']['stricton'] = FALSE;

$db['distrtm']['hostname'] = "192.168.4.22";
$db['distrtm']['username'] = "users3cabang";
$db['distrtm']['password'] = "usersukasukses";
$db['distrtm']['database'] = "dbmarketing";
$db['distrtm']['dbdriver'] = "mysql";
$db['distrtm']['dbprefix'] = "";
$db['distrtm']['pconnect'] = FALSE;
$db['distrtm']['db_debug'] = TRUE;
$db['distrtm']['cache_on'] = FALSE;
$db['distrtm']['cachedir'] = "";
$db['distrtm']['char_set'] = 'utf8';
$db['distrtm']['dbcollat'] = 'utf8_general_ci';
$db['distrtm']['autoinit'] = FALSE;
$db['distrtm']['stricton'] = FALSE;

$db['distsby']['hostname'] = "192.168.4.1";
$db['distsby']['username'] = "users3cabang";
$db['distsby']['password'] = "usersukasukses";
$db['distsby']['database'] = "dbmarketing";
$db['distsby']['dbdriver'] = "mysql";
$db['distsby']['dbprefix'] = "";
$db['distsby']['pconnect'] = FALSE;
$db['distsby']['db_debug'] = TRUE;
$db['distsby']['cache_on'] = FALSE;
$db['distsby']['cachedir'] = "";
$db['distsby']['char_set'] = 'utf8';
$db['distsby']['dbcollat'] = 'utf8_general_ci';
$db['distsby']['autoinit'] = FALSE;
$db['distsby']['stricton'] = FALSE;

$db['distsmg']['hostname'] = "192.168.12.70";
$db['distsmg']['username'] = "users3cabang";
$db['distsmg']['password'] = "usersukasukses";
$db['distsmg']['database'] = "dbmarketing";
$db['distsmg']['dbdriver'] = "mysql";
$db['distsmg']['dbprefix'] = "";
$db['distsmg']['pconnect'] = FALSE;
$db['distsmg']['db_debug'] = TRUE;
$db['distsmg']['cache_on'] = FALSE;
$db['distsmg']['cachedir'] = "";
$db['distsmg']['char_set'] = "utf8";
$db['distsmg']['dbcollat'] = "utf8_general_ci";
$db['distsmg']['autoinit'] = FALSE;
$db['distsmg']['stricton'] = FALSE;

$db['distdps']['hostname'] = "192.168.7.2";
$db['distdps']['username'] = "users3cabang";
$db['distdps']['password'] = "usersukasukses";
$db['distdps']['database'] = "dbmarketing";
$db['distdps']['dbdriver'] = "mysql";
$db['distdps']['dbprefix'] = "";
$db['distdps']['pconnect'] = FALSE;
$db['distdps']['db_debug'] = TRUE;
$db['distdps']['cache_on'] = FALSE;
$db['distdps']['cachedir'] = "";
$db['distdps']['char_set'] = "utf8";
$db['distdps']['dbcollat'] = "utf8_general_ci";
$db['distdps']['autoinit'] = FALSE;
$db['distdps']['stricton'] = FALSE;







// /* End of file database.php */
// /* Location: ./system/application/config/database.php */
