<?php     
require_once('class.phpmailer.php');

function test_aja()
{
    echo "test aja";
}
     
               
function smtpmailer($author, $to, $subject, $message) { 
                         
    global $error;
	global $db;
	
	$q = "
			SELECT
				*
			FROM
				".$db["master"].".email
			WHERE
				".$db["master"].".email.status = 'Active'
			ORDER BY
				".$db["master"].".email.email_id DESC
			LIMIT
				0,1
	";
	$qry = mysql_query($q);
	$row = mysql_fetch_array($qry);
    

    $mail = new PHPMailer();  // create a new object
    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true;  // authentication enabled
    //$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = $row["host"];
    $mail->Port = $row["port"]; 
    $mail->Username = $row["email_address"];  
    $mail->Password = $row["password"];           
    $mail->IsHTML(true); 
    $mail->SetFrom($row["email_address"], $row["subject"]);
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->AddAddress($to, "Hendri");

    
    $mail = new PHPMailer();
    
    $mail->Username = $email;
    $mail->Password = $pass;
    
    if(!$mail->Send()) {
        $error = 'Mail error: '.$mail->ErrorInfo."<br>"; 
    } else {
        $error = 'Message sent!'."<br>";
    }
	
	$ins = "
				INSERT INTO
					".$db["master"].".email_log
				SET
					host = '".$row["host"]."',
					email_from = '".$row["email_address"]."',
					email_to = '".$to."',
					subject = '".$subject."',
					message = '".$message."',
					status = '".$error."',
					author = '".$author." - ".date_now("d/m/Y H:i:s")."',
                    email_date = '".date_now("d/m/Y H:i:s")."'
	";
	mysql_query($ins);

}


function send_email($subject, $body, $to, $to_name, $author)
{
    global $db;
    
    $q = "
            SELECT
                *
            FROM
                ".$db["master"].".email
            WHERE
                ".$db["master"].".email.status = 'Active'
            ORDER BY
                ".$db["master"].".email.email_id DESC
            LIMIT
                0,1
    ";
    //echo $q."<hr/>";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $email_name = $row["subject"];
    $email      = $row["email_address"];
    $pass       = $row["password"];
    $host       = $row["host"];
    
    $mail = new PHPMailer();

    $mail->Username = $email;
    $mail->Password = $pass;
    
    // SMTP server name
    $mail->Host     = $host;
    $mail->Mailer   = "smtp";
    
    $mail->IsHTML(true); 
    
    $mail->Subject = $subject;
    $mail->Body    = $body;
    
    $senderemail     = $email;
    $sendername     = $email_name;
    
    $mail->From     = $senderemail;
    $mail->FromName = $sendername;
    
    $mail->AddAddress($to, $to_name);
    
    if(!$mail->Send()) 
    {
        $results = "Error message";
    }
    else
    {
        $results = "Successfull";
    }
    
    $ins = "
                INSERT INTO
                    ".$db["master"].".email_log
                SET
                    host = '".$host."',
                    email_from = '".$email."',
                    email_to = '".$to."',
                    subject = '".$subject."',
                    message = '".$body."',
                    status = '".$results."',
                    author = '".$author." - ".date_now("d/m/Y H:i:s")."',
                    email_date = NOW()
    ";
    //echo $q."<hr/>";
    mysql_query($ins);
    
    return $results;
}




function format_email($content, $title, $note)
{
    global $config;

    $data = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <style>
            .in_table_data {
                font-family:tahoma,sans-serif; 
                font-size:11px;  
            }
            
            .in_table_data td {
                border-bottom:1px solid #C1CDD8;
                border-right:1px solid #C1CDD8;
                
            }
            
            .title_blue
            {
                font-family:tahoma,sans-serif; 
                height: 20px;
                font-weight: bold;
                color: #15428b;
                padding-left: 5px;
                padding-right: 5px;
            }
        </style>
        </head>
        
        <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td>
                    <table width="600" border="0" cellspacing="0" cellpadding="2" align="center" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:11px; border:solid 1px #E8E8E8;">
                      <tr style="background: #bad0ee;">
                        <td valign="top" width="100"><img src="'.base_url().'logo.png" height="100" width="100"></td>
                        <td valign="top">
                            <span style="font-size: 14px; font-weight: bold;">'.$config["company_name"].'</span><br>
                            ';
                            
                            if($config["address1"]){ $data .= $config["address1"]."<br>"; }
                            if($config["address2"]){ $data .= $config["address2"]."<br>"; }
                            if($config["phone"]){ $data .= "Phone ".$config["phone"]."<br>"; }
                            if($config["fax"]){ $data .= "Fax ".$config["fax"]."<br>"; }
                            if($config["email"]){ $data .= "Email ".$config["email"]."<br>"; }
    $data .= '
                        </td>
                        <td valign="top" width="200">
                            <span style="font-size: 14px; font-weight: bold;">'.$title.'</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td colspan="3" height="200" valign="top">'.$content.'</td>
                      </tr>
                      
                      <tr>
                        <td colspan="3" height="100" bgcolor="#F1F1F1" valign="top">Note : '.$note.'</td>
                      </tr>
                    </table>
               </td>
            </tr>
        </table>    
        
        </body>
        </html>

    ';
    
    return $data;
}


    

?>