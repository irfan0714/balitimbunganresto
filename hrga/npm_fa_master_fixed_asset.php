<?php
include("header.php");

$modul            = "Master Fixed Asset";
$file_current     = "npm_fa_master_fixed_asset.php";
$file_name        = "npm_fa_master_fixed_asset_data.php";

$sess_employee_id = $config_employee["employee_id"] ;

$arr_data["month"][1] = "January";
$arr_data["month"][2] = "February";
$arr_data["month"][3] = "March";
$arr_data["month"][4] = "April";
$arr_data["month"][5] = "May";
$arr_data["month"][6] = "June";
$arr_data["month"][7] = "July";
$arr_data["month"][8] = "August";
$arr_data["month"][9] = "September";
$arr_data["month"][10] = "October";
$arr_data["month"][11] = "November";
$arr_data["month"][12] = "December";

$year_end   = date("Y");
$year_start = date("Y")-3;

$data_employee = select_employee($sess_employee_id);;
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_by = document.getElementById("search_by").value;      
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_fa_code = document.getElementById("search_fa_code").value; 
                    search_no_proposal = document.getElementById("search_no_proposal").value; 
                    search_no_receipt = document.getElementById("search_no_receipt").value; 
                    search_cabang = document.getElementById("search_cabang").value; 
                    search_kategory = document.getElementById("search_kategory").value; 
                    search_period = document.getElementById("search_period").value; 
                    v_mm = document.getElementById("v_mm").value; 
                    v_yyyy = document.getElementById("v_yyyy").value; 
                    v_date_from = document.getElementById("v_date_from").value; 
                    v_date_to = document.getElementById("v_date_to").value; 
                    
                    variabel += "&search_by="+search_by;
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_fa_code="+search_fa_code;
                    variabel += "&search_no_proposal="+search_no_proposal;
                    variabel += "&search_no_receipt="+search_no_receipt;
                    variabel += "&search_cabang="+search_cabang;
                    variabel += "&search_kategory="+search_kategory;
                    variabel += "&search_period="+search_period;
                    variabel += "&v_mm="+v_mm;
                    variabel += "&v_yyyy="+v_yyyy;
                    variabel += "&v_date_from="+v_date_from;
                    variabel += "&v_date_to="+v_date_to;
					variabel += "&v_fixed_asset_id="+param1;
					
					variabel += "&v_fixed_asset_id_curr="+param1;

					//alert(variabel);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='add_data')
				{
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='edit_data')
				{
					variabel += "&v_fixed_asset_id="+param1;

					//alert(variabel);
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_proposal')
				{
					v_no_proposal = document.getElementById("v_no_proposal").value;
					
					variabel += "&v_no_proposal="+v_no_proposal;
					variabel += "&v_receipt_no="+param1;

					//alert(variabel);

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							document.getElementById("td_no_proposal").innerHTML   = arr_data[0];
							document.getElementById("td_tanggal_beli").innerHTML   = arr_data[1];
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_type')
				{
					variabel += "&v_kategory_id="+param1;

					//alert(variabel);
					document.getElementById("show_image_ajax_form").style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("td_type").innerHTML     = xmlhttp.responseText;
							document.getElementById("show_image_ajax_form").style.display = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_sub_type')
				{
					variabel += "&v_type_id="+param1;
					
					//alert(variabel);
					document.getElementById("show_image_ajax_form").style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");

							document.getElementById("td_sub_type").innerHTML   = arr_data[0];
							document.getElementById("td_deskripsi").innerHTML   = arr_data[1];
							document.getElementById("td_tanggal_beli").innerHTML   = arr_data[2];
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_depo')
				{
					variabel += "&v_cabang_id="+param1;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("span_depo").innerHTML     = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_ruangan')
				{
					variabel += "&v_depo_id="+param1;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("td_ruangan_asset").innerHTML     = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='ajax_chk_asuransi')
				{
                    v_fixed_asset_id = document.getElementById("v_fixed_asset_id").value;  
					chk_gudang_par = document.getElementById("chk_gudang_par").checked;
					chk_gudang_earthquaks = document.getElementById("chk_gudang_earthquaks").checked;
					
					variabel += "&v_fixed_asset_id="+v_fixed_asset_id;
					variabel += "&chk_gudang_par="+chk_gudang_par;
					variabel += "&chk_gudang_earthquaks="+chk_gudang_earthquaks;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							if(chk_gudang_par)
							{
								document.getElementById("td_no_asuransi_par_1").innerHTML   = arr_data[0];
								document.getElementById("td_no_asuransi_par_2").innerHTML   = arr_data[1];
								document.getElementById("td_periode_asuransi_par_1").innerHTML   = arr_data[2];
								document.getElementById("td_periode_asuransi_par_2").innerHTML   = arr_data[3];
							}
							else 
							{
								document.getElementById("td_no_asuransi_par_1").innerHTML   = "&nbsp;";
								document.getElementById("td_no_asuransi_par_2").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_asuransi_par_1").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_asuransi_par_2").innerHTML   = "&nbsp;";
							}
							
							if(chk_gudang_earthquaks)
							{
								document.getElementById("td_no_asuransi_earthquake_1").innerHTML   = arr_data[4];
								document.getElementById("td_no_asuransi_earthquake_2").innerHTML   = arr_data[5];
								document.getElementById("td_periode_earthquake_1").innerHTML   = arr_data[6];
								document.getElementById("td_periode_earthquake_2").innerHTML   = arr_data[7];
								document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        				document.getElementById('tr_periode_earthquake').style.display = '';
							}
							else 
							{
								document.getElementById("td_no_asuransi_earthquake_1").innerHTML   = "&nbsp;";
								document.getElementById("td_no_asuransi_earthquake_2").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_earthquake_1").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_earthquake_2").innerHTML   = "&nbsp;";
								document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        				document.getElementById('tr_periode_earthquake').style.display = 'none';
							}
							
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}

			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function change_pj(nilai)
		{
			if(nilai=="umum")
			{
				document.getElementById('td_type').colSpan = "3";
				document.getElementById('td_sub_type').colSpan = "3";
				document.getElementById('td_empl_name_1').style.display    = 'none';
				document.getElementById('td_empl_name_2').style.display    = 'none';
				document.getElementById('td_jabatan_1').style.display    = 'none';
				document.getElementById('td_jabatan_2').style.display    = 'none';
			}
			else
			{
				document.getElementById('td_type').colSpan = "0";
				document.getElementById('td_sub_type').colSpan = "0";
				document.getElementById('td_empl_name_1').style.display    = '';
				document.getElementById('td_empl_name_2').style.display    = '';
				document.getElementById('td_jabatan_1').style.display    = '';
				document.getElementById('td_jabatan_2').style.display    = '';
			}
			
		}
		
		function cek_qty()
		{
			var v_cek_qty = document.getElementById("v_cek_qty").checked;
			
			//alert(v_cek_qty);
			
			if(v_cek_qty)
			{
				document.getElementById('span_qty').style.display    = '';  
				document.getElementById("v_qty").value = '';
				document.getElementById('v_qty').focus();
			}  
			else
			{
				document.getElementById('span_qty').style.display    = 'none';  
			}
			
		}
		
		function pop_up_employee()
		{
			var variabel;
			var search_cabang_id;
			variabel = "";
			search_cabang_id="";
			
			v_cabang_id = document.getElementById("v_cabang_id").value;    
			
			if(v_cabang_id)
			{
				search_cabang_id = "?search_cabang_id="+v_cabang_id;	
			}
			
			variabel += search_cabang_id;
			  
			windowOpener(600, 800, 'Search Employee', 'npm_fa_master_fixed_asset_pop_up_employee.php'+variabel, 'Search Employee')
		}
		
		function view_photo(img)
		{
			var variabel;
			variabel = "";
			
			v_img = "?v_img="+img;
			variabel += v_img;
			
			windowOpener(400, 600, 'Search Photo', 'npm_fa_master_fixed_asset_pop_up_photo.php'+variabel, 'Search Photo')
		}
		
		function change_par()
		{
		    var chk_gedung_par = document.getElementById("chk_gedung_par").checked;
		    
		    if(chk_gedung_par)
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = '';
		        document.getElementById('tr_periode_par').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_par').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = 'none';
		        document.getElementById('tr_periode_par').style.display = 'none';
			} 
		}
		
		function change_earthquake()
		{
		    var chk_gedung_earthquake = document.getElementById("chk_gedung_earthquake").checked;
		    
		    if(chk_gedung_earthquake)
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        document.getElementById('tr_periode_earthquake').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_earthquake').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        document.getElementById('tr_periode_earthquake').style.display = 'none';
			} 
		}

		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin menghapus "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}

		function CheckAll(param, target)
		{
					var field = document.getElementsByName(target);
					var chkall = document.getElementById(param);
					if (chkall.checked == true)
					{
						for (i = 0; i < field.length; i++)
						field[i].checked = true ;
					}else
					{
						for (i = 0; i < field.length; i++)
						field[i].checked = false ;
					}
				}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
			document.getElementById("search_keyword").focus();
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
		
        function change_serach_by(nilai)
        {
            if(nilai=="data")
            {
                document.getElementById('td_keyword').style.display    = '';    
                document.getElementById('td_fa_code').style.display    = 'none';  
                document.getElementById('span_no_proposal').style.display    = 'none';  
                document.getElementById('td_no_receipt').style.display    = 'none';  
                
                document.getElementById('search_keyword').focus();       
            }
            else if(nilai=="fa_code")
            {
                document.getElementById('td_keyword').style.display    = 'none';    
                document.getElementById('td_fa_code').style.display    = '';  
                document.getElementById('span_no_proposal').style.display    = 'none';  
                document.getElementById('td_no_receipt').style.display    = 'none';  
                
                document.getElementById('search_fa_code').focus();        
            }  
            else if(nilai=="no_proposal")
            {
                document.getElementById('td_keyword').style.display    = 'none';    
                document.getElementById('td_fa_code').style.display    = 'none';  
                document.getElementById('span_no_proposal').style.display    = '';  
                document.getElementById('td_no_receipt').style.display    = 'none';  
                
                document.getElementById('search_no_proposal').focus();    
            } 
            else if(nilai=="no_receipt")
            {
                document.getElementById('td_keyword').style.display    = 'none';    
                document.getElementById('td_fa_code').style.display    = 'none';  
                document.getElementById('span_no_proposal').style.display    = 'none';  
                document.getElementById('td_no_receipt').style.display    = '';  
                
                document.getElementById('search_no_receipt').focus();        
            }  
        }
		                             
        function change_serach_period(nilai)
        {
            if(nilai=="all")
            {
                document.getElementById('td_period_monthly').style.display    = 'none';    
                document.getElementById('td_period_range').style.display    = 'none';    
            }
            else if(nilai=="monthly")
            {
                document.getElementById('td_period_monthly').style.display    = '';    
                document.getElementById('td_period_range').style.display    = 'none';   
            }  
            else if(nilai=="range")
            {
                document.getElementById('td_period_monthly').style.display    = 'none';    
                document.getElementById('td_period_range').style.display    = '';  
            }   
        }
	</script>
</head>

<body class="page-body skin-black" onload="start_page();">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-10">
				Search By
                <select name="search_by" id="search_by" class="form-control-new" onchange="change_serach_by(this.value)">
                    <option value="data">Data</option>
                    <option value="fa_code">FA Code</option> 
                    <option value="no_proposal">No Proposal</option> 
                    <option value="no_receipt">No Receipt</option> 
                </select>
                &nbsp;
				<span id="td_keyword">
					<input type="text" class="form-control-new" size="30" maxlength="30" name="search_keyword" id="search_keyword">
				</span>
				<span id="td_fa_code" style="display: none;">
					<input type="text" class="form-control-new" size="30" maxlength="30" name="search_fa_code" id="search_fa_code">
				</span>
				<span id="span_no_proposal" style="display: none;">
					<input type="text" class="form-control-new" size="30" maxlength="30" name="search_no_proposal" id="search_no_proposal">
				</span>
				<span id="td_no_receipt" style="display: none;">
					<input type="text" class="form-control-new" size="30" maxlength="30" name="search_no_receipt" id="search_no_receipt">
				</span>
                &nbsp;
                Cabang
                <select name="search_cabang" id="search_cabang" class="form-control-new" >
                	<?php 
                        $q = "
                            SELECT
                                ".$db["master"].".hrd_cabang.cabang_id,
                                ".$db["master"].".hrd_cabang.cabang_name
                            FROM
                                ".$db["master"].".hrd_cabang
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".hrd_cabang.cabang_name ASC
                        ";
                        $qry_cabang = mysql_query($q);
                        while($row_cabang = mysql_fetch_array($qry_cabang))
                        {
                            $selected = "";
                            if($data["cabang_id"]==$row_cabang["cabang_id"])
                            {
                                $selected = "selected='selected'";
                            }
                            
                            $cabang_name = str_replace("Cabang","",$row_cabang["cabang_name"]);
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_cabang["cabang_id"]; ?>"><?php echo $cabang_name; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Ketegory
                <select name="search_kategory" id="search_kategory" class="form-control-new" >
                    <option value="">All</option>
                    <?php 
                        $q = "
                            SELECT
                                ".$db["master"].".fa_kategory.*
                            FROM
                                ".$db["master"].".fa_kategory
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".fa_kategory.kategory_name ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            $selected = "";
                            if($data["kategory_id"]==$row["kategory_id"])
                            {
                                $selected = "selected='selected'";
                            }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row["kategory_id"]; ?>"><?php echo $row["kategory_name"]; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Period
                <select name="search_period" id="search_period" class="form-control-new" onchange="change_serach_period(this.value)">
                    <option value="all">All</option>
                    <option value="monthly">Monthly</option> 
                    <option value="range">Range</option> 
                </select>
                &nbsp;
				<span id="td_period_monthly" style="display: none;">                               
                <select name="v_mm" id="v_mm" class="form-control-new">
                    <?php               
                        foreach($arr_data["month"] as $key => $val)
                        {    
                            $selected = "";
                            if(date("m")==$key)
                            {
                                $selected = "selected='selected'";    
                            }      
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo sprintf("%02s", $key); ?>"><?php echo $val; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
                <select name="v_yyyy" id="v_yyyy" class="form-control-new">
                    <?php 
                        for($i=$year_start;$i<=$year_end;$i++)
                        {
                            $selected = "";
                            if(date("Y")==$i)
                            {
                                $selected = "selected='selected'";    
                            }
                           
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php 
                        }
                    ?>
                </select>   
                </span>
				<span id="td_period_range" style="display: none;">
					<input type="text" class="form-control-new datepicker" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                    &nbsp;s/d&nbsp;
                    <input type="text" class="form-control-new datepicker" name="v_date_to" id="v_date_to" size="10" maxlength="10"> 
				</span>
	    	</div>
	    
			<div class="col-md-2" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('add_data'),show_loading_bar(100)">Add Data<i class="entypo-plus" ></i></button>
	       	</div>
       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row scrollable" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
    
<?php include("footer.php"); ?>