<?php 
    include("header.php"); 
    
	$modul = "Generate NIK";
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			
		}
	</script>
    <?php
    if(isset($_REQUEST["btn_save"])!="")
    {
        $v_cost_center_no = $_POST["v_cost_center_no"];
              
        // format 2 digit no cabang,2 digit tahun, 4 digit counter
        
        $counter = "0001";
        
        $q = "
            SELECT
                ".$db["master"].".employee_join.join_date
            FROM
                ".$db["master"].".employee_join
            WHERE
                1
                AND ".$db["master"].".employee_join.employee_id = '".$v_employee_id."'
            LIMIT
                0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($join_date) = $row;
        
        if($join_date)
        {
            $arr_date = explode("-", $join_date);
            
            $format = $v_cost_center_no.substr($arr_date[0],2,2);    
        }
        else
        {
            $format = $v_cost_center_no.substr(date_now("Y"),2,2);
        }
        
        $q = "
            SELECT
                ".$db["master"].".employee.employee_nik
            FROM
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_nik <> ''
                AND CHAR_LENGTH(".$db["master"].".employee.employee_nik) = '8'
            ORDER BY
                RIGHT(".$db["master"].".employee.employee_nik,4)*1 DESC
            LIMIT
                0,1
        ";
        $qry  = mysql_query($q);
        $row  = mysql_fetch_array($qry);
        
        $last = $row["employee_nik"];
        
        if($last=="")
        {
            $counter = "0001";
        }
        else
        {
            $counter = sprintf("%04d",substr($last,4,4)*1+1);
        }
        
        if($v_cost_center_no!="")
        {
            $return = $format.$counter;
        }
        
        ?>
            <script>
                window.opener.document.getElementById('v_employee_nik').value = "<?php echo $return; ?>";  
                window.close();
            </script>
        <?php
        
    }
    ?>
    
</head>

<body class="page-body skin-black" onload="start_page()">
<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    <div class="main-content">
		<form method="post">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered responsive">
			    	<tr>
                        <td class="title_table" width="100">Cabang</td>
                        <td> 
                            <select class="form-control-new" name="v_cost_center_no" id="v_cost_center_no">
				                <?php 
				                    $q = "
			                            SELECT
			                                ".$db["master"].".hrd_cabang.cabang_id,
			                                ".$db["master"].".hrd_cabang.cabang_name,
			                                ".$db["master"].".hrd_cabang.cabang_initial,
			                                ".$db["master"].".hrd_cabang.cabang_no
			                            FROM
			                                ".$db["master"].".hrd_cabang
			                            WHERE
			                                1
			                                AND ".$db["master"].".hrd_cabang.cabang_no <> ''
			                            ORDER BY
			                                ".$db["master"].".hrd_cabang.cabang_name ASC
				                    ";
				                    $qry_cost = mysql_query($q);
				                    while($row_cost = mysql_fetch_array($qry_cost))
				                    {
				                    	$cabang_name = str_replace("Cabang","",$row_cost["cabang_name"]);
				                ?>
				                <option value="<?php echo $row_cost["cabang_no"]; ?>"><?php echo $cabang_name; ?></option>
				                <?php 
				                    }
				                ?>
				            </select>
                        </td>
                    </tr>
                            
					<tr>
						<td>&nbsp;</td>
						<td>
						    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Submit">Submit<i class="entypo-check"></i></button>
						</td>
					</tr>
			    
			    </table>
			</div>
		</div>
		</form>
	</div>
</div>
</body>
</html> 