<?php 
    // insert details
    $pk_id          = $id;
    $no_family      = "";
    $no_agreement   = "";
    $no_contract    = "";
    $no_join        = "";
    $no_resign      = "";
    $no_photo       = "";
    
    if(!isset($_POST["no_family"])){ $no_family = isset($_POST["no_family"]); } else { $no_family = $_POST["no_family"]; }
    if(!isset($_POST["no_position"])){ $no_position = isset($_POST["no_position"]); } else { $no_position = $_POST["no_position"]; }
    if(!isset($_POST["no_agreement"])){ $no_agreement = isset($_POST["no_agreement"]); } else { $no_agreement = $_POST["no_agreement"]; }
    if(!isset($_POST["no_contract"])){ $no_contract = isset($_POST["no_contract"]); } else { $no_contract = $_POST["no_contract"]; }
    if(!isset($_POST["no_join"])){ $no_join = isset($_POST["no_join"]); } else { $no_join = $_POST["no_join"]; }
    if(!isset($_POST["no_resign"])){ $no_resign = isset($_POST["no_resign"]); } else { $no_resign = $_POST["no_resign"]; }
    if(!isset($_POST["no_photo"])){ $no_photo = isset($_POST["no_photo"]); } else { $no_photo = $_POST["no_photo"]; }
    
    $hari_ini = parsedate(date("d/m/Y"));
    
    while(list($key1, $val1)=@each($no_family))
    {   
        $v_sid_family       = $_POST["v_sid_family_".$val1];
        $v_relation_id      = save_char($_POST["v_relation_id_".$val1]);
        $v_family_name      = save_char($_POST["v_family_name_".$val1]);
        $v_birthday         = format_save_date($_POST["v_birthday_".$val1]);
        $v_gender           = save_char($_POST["v_gender_".$val1]);
        $v_sts_cerai        = save_char($_POST["v_sts_cerai_".$val1]);
        
        $q = "
            SELECT 
                ".$db["master"].".employee.*
            FROM 
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_id = '".$pk_id."'
            LIMIT 0,1
        ";
        $qry["data"] = mysql_query($q);
        $data = mysql_fetch_array($qry["data"]);
        
        if($v_sid_family*1==0 && $v_relation_id*1!=0 && $v_family_name!="")
        {
            $counter_details = get_counter_int($db["master"],"employee_family","sid",100);
                            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_family
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    relation_id = '".$v_relation_id."',
                    family_name = '".$v_family_name."',
                    birthday = '".$v_birthday."',
                    gender = '".$v_gender."',
                    sts_cerai = '".$v_sts_cerai."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Cabang Exists<br>";
            }
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_family
                SET
                    relation_id = '".$v_relation_id."',
                    family_name = '".$v_family_name."',
                    birthday = '".$v_birthday."',
                    gender = '".$v_gender."',
                    sts_cerai = '".$v_sts_cerai."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_family."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Failed update details ||".$q."<br>";
            }    
        }
    }
    
    while(list($key3, $val3)=@each($no_agreement))
    {   
        $v_sid_agreement        = $_POST["v_sid_agreement_".$val3];
        $v_start_date_agreement = save_char($_POST["v_start_date_agreement_".$val3]);
        $v_end_date_agreement   = save_char($_POST["v_end_date_agreement_".$val3]);
        $v_subject_agreement    = save_char($_POST["v_subject_agreement_".$val3]);
        $v_remarks_agreement    = save_char($_POST["v_remarks_agreement_".$val3]);
        
        // old
        $v_start_date_agreement_old = save_char($_POST["v_start_date_agreement_old_".$val3]);
        $v_end_date_agreement_old   = save_char($_POST["v_end_date_agreement_old_".$val3]);
        $v_subject_agreement_old    = save_char($_POST["v_subject_agreement_old_".$val3]);
        $v_remarks_agreement_old    = save_char($_POST["v_remarks_agreement_old_".$val3]);
        
        $q = "
            SELECT 
                ".$db["master"].".employee.*
            FROM 
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_id = '".$pk_id."'
            LIMIT 0,1
        ";
        $qry["data"] = mysql_query($q);
        $data = mysql_fetch_array($qry["data"]);
        
        if($v_sid_agreement*1==0 && $v_start_date_agreement!="" && $v_end_date_agreement!="")
        {
            $counter_details = get_counter_int($db["master"],"employee_agreement","sid",100);
                            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_agreement
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    start_date = '".format_save_date($v_start_date_agreement)."',
                    end_date = '".format_save_date($v_end_date_agreement)."',
                    subject = '".$v_subject_agreement."',
                    remarks = '".$v_remarks_agreement."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Cabang Exists<br>";
            }
            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_agreement_log
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    start_date = '".format_save_date($v_start_date_agreement)."',
                    end_date = '".format_save_date($v_end_date_agreement)."',
                    subject = '".$v_subject_agreement."',
                    remarks = '".$v_remarks_agreement."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            mysql_query($q);
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_agreement
                SET
                    start_date = '".format_save_date($v_start_date_agreement)."',
                    end_date = '".format_save_date($v_end_date_agreement)."',
                    subject = '".$v_subject_agreement."',
                    remarks = '".$v_remarks_agreement."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_agreement."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Failed update details ||".$q."<br>";
            } 
            
            if
            (
                $v_start_date_agreement_old!=$v_start_date_agreement ||
                $v_end_date_agreement_old!=$v_end_date_agreement ||
                $v_subject_agreement_old!=$v_subject_agreement ||
                $v_remarks_agreement_old!=$v_remarks_agreement
            ) 
            {
                $q = "
                    INSERT INTO
                        ".$db["master"].".employee_agreement_log
                    SET
                        sid = '".$v_sid_agreement."',
                        employee_id = '".$pk_id."',
                        start_date = '".format_save_date($v_start_date_agreement_old)."',
                        end_date = '".format_save_date($v_end_date_agreement_old)."',
                        subject = '".$v_subject_agreement_old."',
                        remarks = '".$v_remarks_agreement_old."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
                ";
                mysql_query($q);
            }
        }
    }
    
    while(list($key4, $val4)=@each($no_contract))
    {
        $v_sid_contract         = $_POST["v_sid_contract_".$val4];
        $v_start_date_contract  = save_char($_POST["v_start_date_contract_".$val4]);
        $v_end_date_contract    = save_char($_POST["v_end_date_contract_".$val4]);
        $v_subject_contract     = save_char($_POST["v_subject_contract_".$val4]);
        $v_remarks_contract     = save_char($_POST["v_remarks_contract_".$val4]);
        
        // old
        $v_start_date_contract_old  = save_char($_POST["v_start_date_contract_old_".$val4]);
        $v_end_date_contract_old    = save_char($_POST["v_end_date_contract_old_".$val4]);
        $v_subject_contract_old     = save_char($_POST["v_subject_contract_old_".$val4]);
        $v_remarks_contract_old     = save_char($_POST["v_remarks_contract_old_".$val4]);
        
        $q = "
            SELECT 
                ".$db["master"].".employee.*
            FROM 
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_id = '".$pk_id."'
            LIMIT 0,1
        ";
        $qry["data"] = mysql_query($q);
        $data = mysql_fetch_array($qry["data"]);
        
        if($v_sid_contract*1==0 && $v_start_date_contract!="" && $v_end_date_contract!="")
        {
            $counter_details = get_counter_int($db["master"],"employee_contract","sid",100);
                            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_contract
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    start_date = '".format_save_date($v_start_date_contract)."',
                    end_date = '".format_save_date($v_end_date_contract)."',
                    subject = '".$v_subject_contract."',
                    remarks = '".$v_remarks_contract."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Cabang Exists<br>";
            }
            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_contract_log
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    start_date = '".format_save_date($v_start_date_contract)."',
                    end_date = '".format_save_date($v_end_date_contract)."',
                    subject = '".$v_subject_contract."',
                    remarks = '".$v_remarks_contract."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            mysql_query($q);
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_contract
                SET
                    start_date = '".format_save_date($v_start_date_contract)."',
                    end_date = '".format_save_date($v_end_date_contract)."',
                    subject = '".$v_subject_contract."',
                    remarks = '".$v_remarks_contract."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_agreement."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Failed update details ||".$q."<br>";
            }   
            
            if(
                $v_start_date_contract_old!=$v_start_date_contract ||
                $v_end_date_contract_old!=$v_end_date_contract ||
                $v_subject_contract_old!=$v_subject_contract ||
                $v_remarks_contract_old!=$v_remarks_contract
            )
            {
                $q = "
                    INSERT INTO
                        ".$db["master"].".employee_contract_log
                    SET
                        sid = '".$v_sid_agreement."',
                        employee_id = '".$pk_id."',
                        start_date = '".format_save_date($v_start_date_contract_old)."',
                        end_date = '".format_save_date($v_end_date_contract_old)."',
                        subject = '".$v_subject_contract_old."',
                        remarks = '".$v_remarks_contract_old."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
                ";
                mysql_query($q);    
            } 
        }
    }   
    
    while(list($key5, $val5)=@each($no_join))
    {   
        $v_sid_join             = $_POST["v_sid_join_".$val5];
        $v_join_date            = save_char($_POST["v_join_date_".$val5]);
        $v_remarks_join         = save_char($_POST["v_remarks_join_".$val5]);
        
        // old
        $v_join_date_old            = save_char($_POST["v_join_date_old_".$val5]);
        $v_remarks_join_old         = save_char($_POST["v_remarks_join_old_".$val5]);
        
        $q = "
            SELECT 
                ".$db["master"].".employee.*
            FROM 
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_id = '".$pk_id."'
            LIMIT 0,1
        ";
        $qry["data"] = mysql_query($q);
        $data = mysql_fetch_array($qry["data"]);
        
        if($v_sid_join*1==0 && $v_join_date!="")
        {
            $counter_details = get_counter_int($db["master"],"employee_join","sid",100);
                            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_join
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    join_date = '".format_save_date($v_join_date)."',
                    remarks = '".$v_remarks_join."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Cabang Exists<br>";
            }
            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_join_log
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    join_date = '".format_save_date($v_join_date)."',
                    remarks = '".$v_remarks_join."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            mysql_query($q);
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_join
                SET
                    join_date = '".format_save_date($v_join_date)."',
                    remarks = '".$v_remarks_join."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_join."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Failed update details ||".$q."<br>";
            }  
            
            if(
                    $v_join_date_old!=$v_join_date ||
                    $v_remarks_join_old!=$v_remarks_join    
            )
            {
                $q = "
                    INSERT INTO
                        ".$db["master"].".employee_join_log
                    SET
                        sid = '".$v_sid_join."',
                        employee_id = '".$pk_id."',
                        join_date = '".format_save_date($v_join_date_old)."',
                        remarks = '".$v_remarks_join_old."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
                ";
                mysql_query($q);    
            } 
        }
    }
    
    while(list($key6, $val6)=@each($no_resign))
    {   
        
        $v_sid_resign           = $_POST["v_sid_resign_".$val6];
        $v_resign_date          = save_char($_POST["v_resign_date_".$val6]);
        $v_remarks_resign       = save_char($_POST["v_remarks_resign_".$val6]);
        
        // old
        $v_resign_date_old          = save_char($_POST["v_resign_date_old_".$val6]);
        $v_remarks_resign_old       = save_char($_POST["v_remarks_resign_old_".$val6]);
        
        $q = "
                SELECT 
                    ".$db["master"].".employee.*
                FROM 
                    ".$db["master"].".employee
                WHERE
                    1
                    AND ".$db["master"].".employee.employee_id = '".$pk_id."'
                LIMIT 0,1
        ";
        $qry["data"] = mysql_query($q);
        $data = mysql_fetch_array($qry["data"]);
        
        if($v_sid_resign*1==0 && $v_resign_date!="")
        {
            $counter_details = get_counter_int($db["master"],"employee_resign","sid",100);
                            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_resign
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    resign_date = '".format_save_date($v_resign_date)."',
                    remarks = '".$v_remarks_resign."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Cabang Exists<br>";
            }
            
            $q = "
                INSERT INTO
                    ".$db["master"].".employee_resign_log
                SET
                    sid = '".$counter_details."',
                    employee_id = '".$pk_id."',
                    resign_date = '".format_save_date($v_resign_date)."',
                    remarks = '".$v_remarks_resign."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            mysql_query($q);
            
            
            if(parsedate($v_resign_date) < $hari_ini && false)
            {
                if($data["username"])
                {
             	    $q = "
                        UPDATE
                            ".$db["master"].".user
                        SET
                            status = 'InActive'
                        WHERE
                            username = '".$data["username"]."'
	                ";
	                if(!mysql_query($q))
	                {
	                    $msg = "Failed Update ||".$q;
	                }
			    }
            }
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_resign
                SET
                    resign_date = '".format_save_date($v_resign_date)."',
                    remarks = '".$v_remarks_resign."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_resign."'
            ";
            if(!mysql_query($q))
            {
                $msg .= "Failed update details ||".$q."<br>";
            }
            
            if(
                $v_resign_date_old!=$v_resign_date ||
                $v_remarks_resign_old!=$v_remarks_resign
            )    
            {
                $q = "
                    INSERT INTO
                        ".$db["master"].".employee_resign_log
                    SET
                        sid = '".$v_sid_resign."',
                        employee_id = '".$pk_id."',
                        resign_date = '".format_save_date($v_resign_date_log)."',
                        remarks = '".$v_remarks_resign_log."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
                ";
                mysql_query($q);    
            }
            
            if( parsedate($v_resign_date) < $hari_ini && false )
            {
                if($data["username"])
                {
             	    $q = "
                        UPDATE
                            ".$db["master"].".user
                        SET
                            status = 'InActive'
                        WHERE
                            username = '".$data["username"]."'
	                ";
	                if(!mysql_query($q))
	                {
	                    $msg = "Failed Update ||".$q;
	                }
			    }
            }
        }
    }
    
    while(list($key7, $val7)=@each($no_photo))
    {  
		$v_dir_upload      = "temp/";
		$v_sid_photo      = $_POST["v_sid_photo_".$val7];
		$v_photo_type     = $_POST["v_photo_type_".$val7];
		$v_photo_data_tmp = $_FILES['v_photo_data_'.$val7]['tmp_name'];
		$v_photo_data_name= strtolower($_FILES['v_photo_data_'.$val7]['name']);
		$extension        = pathinfo($v_photo_data_name, PATHINFO_EXTENSION);
		$v_remarks_photo  = save_char($_POST["v_remarks_photo_".$val7]);
	
		$imgdetails = getimagesize($v_photo_data_tmp);
        $mime_type  = $imgdetails['mime'];
	    
        if ( preg_match("/jpg|jpeg/", $extension) ) $src_img=@imagecreatefromjpeg($v_photo_data_tmp);
		if ( preg_match("/png/", $extension) ) $src_img=@imagecreatefrompng($v_photo_data_tmp);
		
		if(!$src_img) return false;
		
		$src_width = imageSX($src_img);
		$src_height = imageSY($src_img);
				
		if($src_width == $src_height)
		{
			$thumb_width = $src_width;
			$thumb_height= $src_height;
		}
		elseif($src_width < $src_height)
		{
			$thumb_width = $src_width;
			$thumb_height= $src_width;
		}
		elseif($src_width > $src_height)
		{
			$thumb_width = $src_height;
			$thumb_height= $src_height;
		}
		else
		{
			$thumb_width = 100;
			$thumb_height= 100;
		}
		
		$original_aspect = $src_width / $src_height;
		$thumb_aspect = $thumb_width / $thumb_height;

		if ( $original_aspect >= $thumb_aspect ) {

		 // If image is wider than thumbnail (in aspect ratio sense)
		 $new_height = $thumb_height;
		 $new_width = $src_width / ($src_height / $thumb_height);

		}
		else {
		 // If the thumbnail is wider than the image
		 $new_width = $thumb_width;
		 $new_height = $src_height / ($src_width / $thumb_width);
		}

		$im = imagecreatetruecolor( $thumb_width, $thumb_height );
		
		// Resize and crop
		imagecopyresampled($im,
		     $src_img,
		     0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
		     0 - ($new_height - $thumb_height) / 2, // Center the image vertically
		     0, 0,
		     $new_width, $new_height,
		     $src_width, $src_height);
  		
		//Set ukuran gambar hasil perubahan
  		if($src_width>800)
  		{
			$dst_width2 = 800;
		}
		else
		{
			$dst_width2 = $src_width;
		}
		
		$dst_height2 = ($dst_width2/$src_width)*$src_height;

		//proses perubahan ukuran
		$im2 = imagecreatetruecolor($dst_width2,$dst_height2);
		imagecopyresampled($im2, $src_img, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);
		
		if($v_photo_type=="Pas Photo")
		{
			imagejpeg($im,$v_dir_upload . "thumb_" . $v_photo_data_name);
			
		    $vfile_upload = $v_dir_upload . "thumb_" . $v_photo_data_name;
			
		}
		else
		{
			imagejpeg($im2,$v_dir_upload . "medium_" . $v_photo_data_name);
					
			$vfile_upload = $v_dir_upload . "medium_" . $v_photo_data_name;
		}
		
		
        $v_save_photo = addslashes(file_get_contents($vfile_upload));
		
        $exp_photo_name = explode(".", $v_photo_data_name);
        $jml_titik      = count($exp_photo_name);
        $curr_titik     = $jml_titik-1;
        
        $photo_ext      = $exp_photo_name[$curr_titik];
        
        //echo "masuk 1";
        
        //echo $v_sid_photo."<hr>";
        if($v_sid_photo*1==0)
        {   
            if($v_photo_data_name!="")
            {
                if(($photo_ext=='jpeg')||($photo_ext=='jpg')||($photo_ext=='gif')||($photo_ext=='png'))
                {   
                    //echo "masuk 2";
                    // insert    
                    $counter_details = get_counter_int($db["master"],"employee_photo","sid",100);
                    
                    $q = "
                            INSERT INTO
                                ".$db["master"].".employee_photo
                            SET
                                sid = '".$counter_details."',
                                employee_id = '".$pk_id."',
                                photo_type = '".$v_photo_type."',
                                photo_data = '".$v_save_photo."',
                                photo_ext = '".$photo_ext."',
                                remarks = '".$v_remarks_photo."',
                                author = '".ffclock()."',
                                edited = '".ffclock()."'
                    ";
                    mysql_query($q);
                    
                    // hapus photo di folder temp   
                   	if($v_photo_type=="Pas Photo")
					{
                    	unlink($v_dir_upload . "thumb_" . $v_photo_data_name); 
					}
					else
					{
                    	unlink($v_dir_upload . "medium_" . $v_photo_data_name); 
					}
                }
                else
                {
                    $msg .= "Photo harus ber ektension .jpg, .gif, .png";
                }
            }
            else
            {
                $msg .= "Photo harus diisi";
            }
        }
        else
        {
            $q = "
                UPDATE
                    ".$db["master"].".employee_photo
                SET
                    photo_type = '".$v_photo_type."',
                    remarks = '".$v_remarks_photo."',
                    edited = '".ffclock()."'
                WHERE
                    sid = '".$v_sid_photo."'
            ";
            mysql_query($q);
        }
 
		imagedestroy($im);  
    }
?>