<?php
    
    include("header.php");
    
    if(!isset($_GET["kirim_email"])){ $kirim_email = isset($_GET["kirim_email"]); } else { $kirim_email = $_GET["kirim_email"]; }
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Daily";
    
    
    if($v_date_from=="")
    {
        $v_date_from = date("d/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("d/m/Y");
    }
    
    //$ses_login = "hendri1003";
    if($kirim_email=="")
    {
        $menu_validation = menu_validation("report_daily", $ses_login);
        
        if(!$menu_validation["v"])
        {
            die("Anda tidak memiliki Otorisasi");
        }
    }
    
    
    $q   = "
        SELECT 
          kassa.id_kassa,
          divisi.NamaDivisi,
          kassa.SubDivisi 
        FROM
          kassa 
          INNER JOIN divisi 
            ON kassa.KdDivisi = divisi.KdDivisi 
        WHERE 1 
        ORDER BY kassa.id_kassa ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($NoKassa, $NamaDivisi, $SubDivisi) = $row;
        
        if($NamaDivisi=="The Luwus")
        {
            if($SubDivisi==20)
            {
                $arr_data["type_Kasir"][$NoKassa] = "Rice View";           
            }
            else
            {
                $arr_data["type_Kasir"][$NoKassa] = "The Luwus";           
            }      
        }
        else if($NamaDivisi=="Oemah Herborist")
        {
            $arr_data["type_Kasir"][$NoKassa] = "Oemah Herborist";       
        }
        else if($NamaDivisi=="Black Eye")
        {
            if($SubDivisi==10)
            {
                $arr_data["type_Kasir"][$NoKassa] = "BE Store";           
            }
            else
            {
                $arr_data["type_Kasir"][$NoKassa] = "BE Bar";           
            }
        }
        
        
    }
    
    /*
    $q   = "SELECT Kasir FROM `transaksi_header` WHERE 1 GROUP BY Kasir ORDER BY Kasir ASC";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($Kasir) = $row;
        
        $Kasir = strtolower($Kasir);
        
        $arr_data["list_Kasir"][$Kasir] = $Kasir;
        
        if($Kasir=="ariesta" || $Kasir=="dedek" || $Kasir=="degler" || $Kasir=="desakmanik" || $Kasir=="feby" || $Kasir=="puri" || $Kasir=="tutik" || $Kasir=="omingsgv")
        {
            $arr_data["type_Kasir"][$Kasir] = "Oemah Herborist";    
        }
        else if($Kasir=="viorinresto" || $Kasir=="dewiresto" || $Kasir=="ferryresto" || $Kasir=="widiaresto")
        {
            $arr_data["type_Kasir"][$Kasir] = "Luwus Rice";    
        }
        else
        {
            $arr_data["type_Kasir"][$Kasir] = "Black Eye";    
        }
    }
    */
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=repor-daily.xls");
        header("Content-type: application/vnd.ms-excel");
    }
    else
    {
     
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  

function pop_up_pemakaian_voucher(v_date)
{
    try{
        windowOpener(600, 800, 'Pemakaian Voucher', 'report_daily_pop_up_pemakaian_voucher.php?v_date='+v_date, 'Pemakaian Voucher')    
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }
}
    </script>
    <style>
        .link_pop{
            color: black;
            text-decoration: underline;
        }
        
        .link_pop:hover{
            color: black;
            text-decoration: none;
        }
        
    </style>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
    
    <?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
        <ol class="breadcrumb bc-3">
            <li>
                <a href="index.php">
                    <i class="entypo-home"></i>Home
                </a>
            </li>
            <li>NPM</li>
            <li class="active"><strong><?php echo $modul; ?></strong></li>
        </ol>
        
        <hr/>
        <br/>
        
        <form method="POST" name="theform" id="theform">
        
        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            
            <table class="table table-bordered responsive">
                <thead>
                    <tr>
                        <th width="100">Tanggal</th>
                        <th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
                    </tr>
                    
                    
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
                </thead>
                
            </table> 
            <br><br>
            
            <?php 
    }
            
                if($btn_submit || $btn_excel || $kirim_email=="sgv2016")
                {
                    
                    if($kirim_email=="sgv2016")
                    {
                        //$v_date_from = date_now("d/m/Y"); 
                        $v_date_from = "01/".date("m/Y");   
                        $v_date_to = date_now("d/m/Y");  
                        
                        //$v_date_from = "27/06/2016";  
                        //$v_date_to = "01/11/2016";  
                    }
                   
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    
                    
                    for($i=parsedate($v_date_from);$i<=parsedate($v_date_to);$i=$i+86400)
                    {
                        $arr_data["list_date"][$i] = $i;
                    }
                    
                    //echo "<pre>";
                    //print_r($arr_data["list_date"]);
                    //echo "</pre>";
                    
                    // data ticket
                    
                    
                    $q = "
                            SELECT
                                `ticket`.noticket,
                                `ticket`.add_date,
                                `ticket`.jenis,
                                `ticket`.harga
                            FROM
                                `ticket`
                            WHERE
                                1
                                AND `ticket`.add_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                AND ticket.`noidentitas` != '1234' and status=1 
                            ORDER BY
                                `ticket`.noticket ASC
                    ";
                    //echo $q;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($noticket, $add_date, $jenis, $harga) = $row;    
                        
                        if($jenis==1)
                        {
                            $arr_data["lokal"][$add_date]++;                   
                        }
                        elseif($jenis==2)
                        {
                            $arr_data["asia"][$add_date]++;               
                        }else{
							$arr_data["barat"][$add_date]++;               
						}
                        
                        $arr_data["all_ticket"][$add_date]++;
                                                              
                        $arr_data["harga_tiket"][$add_date] += $harga;
                        
                    }
                    
                    // pemakaian voucher dari transaksi_detail_voucher
                    {
                        $q = "
                                SELECT
                                    d.Tanggal,        
                                    SUM(d.NilaiVoucher) AS pemakaian_voucher
                                FROM
                                    transaksi_detail_voucher d inner join transaksi_header h on h.nostruk=d.nostruk
                                WHERE
                                    1
                                    AND d.Tanggal BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                    AND h.status='1'
                                GROUP BY
                                    d.Tanggal
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($Tanggal, $pemakaian_voucher) = $row;
                            
                            $arr_data["pemakaian_voucher_ticket"][$Tanggal] = $pemakaian_voucher;
                        }
                    }
                    
                    
                    
                    //echo "<pre>";
                    //print_r($arr_data["harga_tiket"]);
                    //echo "</pre>";
                    
                    // data sales
                    {
                        $counter = 1;
                        /*
                        $q = "
                                SELECT
                                    `transaksi_header`.NoKassa,
                                    `transaksi_header`.Kasir,
                                    `transaksi_header`.Tanggal,
                                    `transaksi_header`.Waktu,
                                    `transaksi_header`.NoStruk,
                                    (COALESCE(`transaksi_header`.Tunai, NULL, '0') - COALESCE(`transaksi_header`.Kembali, NULL, '0')) AS Tunai,
                                    COALESCE(`transaksi_header`.KDebit, NULL, '0') AS KDebit,
                                    COALESCE(`transaksi_header`.KKredit, NULL, '0') AS KKredit,
                                    COALESCE(`transaksi_header`.Voucher, NULL, '0') AS Voucher,
                                    COALESCE(`transaksi_header`.TotalNilai, NULL, '0') AS TotalNilai,
                                    COALESCE(`transaksi_header`.TotalBayar, NULL, '0') AS TotalBayar
                                FROM
                                    `transaksi_header`
                                WHERE
                                    1
                                    AND `transaksi_header`.Tanggal BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                    AND `transaksi_header`.status = '1'
                                ORDER BY
                                    `transaksi_header`.Kasir ASC,
                                    `transaksi_header`.Tanggal ASC,
                                    `transaksi_header`.Waktu ASC
                        ";
                        */
                        $q = "SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(d.`Service_charge`>0, d.`Netto`*1155/1000, d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								INNER JOIN subdivisi s ON b.`KdSubDivisi`=s.`KdSubDivisi`
								INNER JOIN divisireport r ON s.`KdDivisiReport`=r.`KdDivisiReport`
								WHERE h.`Tanggal` BETWEEN '" . format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' AND h.`Status`=1
								GROUP BY h.`Tanggal`, r.KdDivisiReport";
                        //echo $q;
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($Tanggal, $KdDivisiReport, $NamaDivisReport, $Sales) = $row;
                            
                            $arr_data["total_TotalNilai"][$Tanggal] += $Sales;
                            
                            switch($KdDivisiReport){
								case 10:
								 	$arr_data["total_OH"][$Tanggal] += $Sales;
									break;
								case 20:
									$arr_data["total_The_Luwus"][$Tanggal] += $Sales; 
									break;
								case 21:
									$arr_data["total_Rice_View"][$Tanggal] += $Sales; 
									break;
								case 22:
									$arr_data["total_Juice_Corner"][$Tanggal] += $Sales; 
									break;
								case 23:
									$arr_data["total_Mini_Zoo"][$Tanggal] += $Sales; 
									break;
								case 24:
									$arr_data["total_Burger_Corner"][$Tanggal] += $Sales; 
									break;
								case 30:
									$arr_data["total_BE_Store"][$Tanggal] += $Sales;    
									break;
								case 31:
									$arr_data["total_BE_Bar"][$Tanggal] += $Sales;    
									break;	   
								case 32:
									$arr_data["total_BE_Class"][$Tanggal] += $Sales;    
									break;	   
								case 33:
									$arr_data["total_Gelato"][$Tanggal] += $Sales;    
									break;	   
								case 34:
									$arr_data["total_BE_Store"][$Tanggal] += $Sales;    
									break;	   
								case 35:
									$arr_data["total_BE_Bar"][$Tanggal] += $Sales;    
									break;	   
								default:
									$arr_data["total_Ngaco"][$Tanggal] += $Sales;   
									break;
							}
                            $counter++;
                        }
                    }
                    
                     // data komisi
                     {
                        $q = "
                            SELECT
                                `finance_komisi_header`.NoTransaksi,
                                `finance_komisi_header`.TglTransaksi,
                                `finance_komisi_header`.Total
                            FROM
                                `finance_komisi_header`
                            WHERE
                                1
                                AND `finance_komisi_header`.TglTransaksi BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                            ORDER BY
                                `finance_komisi_header`.TglTransaksi ASC
                        ";
                        //echo $q;
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($NoTransaksi, $TglTransaksi, $Total) = $row;    
                            
                            $arr_data["total_komisi"][$TglTransaksi] += $Total;
                            
                        }
                     }
                           
                    // data compliment
                    {
                        $q = "
                                SELECT 
                                  `transaksi_detail`.NoStruk,
                                  `transaksi_detail`.Tanggal,
                                  SUM(`transaksi_detail`.Qty * `transaksi_detail`.Harga) AS total 
                                FROM
                                  `transaksi_detail` 
                                  INNER JOIN `transaksi_header` ON
                                    `transaksi_detail`.NoStruk = `transaksi_header`.NoStruk
                                    and `transaksi_detail`.nokassa = `transaksi_header`.nokassa
                                    AND `transaksi_header`.Status = '1'  
                                    AND (`transaksi_header`.TotalNilai*1) = '0'  
                                    AND `transaksi_header`.DPP*1 = '0'
                                    and `transaksi_header`.Discount > 0
                                WHERE 1 
                                  AND `transaksi_detail`.Tanggal BETWEEN '".format_save_date($v_date_from)."' 
                                  AND '".format_save_date($v_date_to)."'  
                                GROUP BY
                                  `transaksi_detail`.NoStruk,
                                  `transaksi_detail`.Tanggal
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($NoStruk, $Tanggal, $Total) = $row;    
                            
                            $arr_data["total_compliment"][$Tanggal] += $Total;  
                        }
                    }
                    
                    $table_border = 0;
                    if($btn_excel || $kirim_email)
                    {
                        $table_border = 1;
                    }
                    
                    $echo = '';
                    if($btn_excel)
                    {
                        $echo = '
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="17">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="17">REPORT DAILY '.$v_date_from.' s/d '.$v_date_to.'</td>   
                            </tr>                            
                            <tr>
                                <td colspan="17">&nbsp;</td>   
                            </tr>
                        </table>
                        ';
                    }
                        
                     
                    $echo .= '
                    
                    <table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
                        <thead>
                            <tr>
                                <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
                                <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
                                <th rowspan="2" style="vertical-align: middle;">Hari</th>
                                <th style="vertical-align: middle; text-align: center;" colspan="5">Tiket</th>
                                <th style="vertical-align: middle; text-align: center;" colspan="11">Sales</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Komisi</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Pemakaian Voucher</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Compliment</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Omzet</th>
                            </tr>
                            
                            <tr>
                                <th style="text-align: right;">Lokal</th>
                                <th style="text-align: right;">Asia</th>
                                <th style="text-align: right;">Barat</th>
                                <th style="text-align: right;">Jumlah Ticket</th>
                                <th style="text-align: right;">Total (Rp)</th>
                            
                                <th style="text-align: right;">OH</th>
                                <th style="text-align: right;">The Luwus</th>
                                <th style="text-align: right;">Rice View</th>
                                <th style="text-align: right;">Juice</th>
                                <th style="text-align: right;">Burger</th>
                                <th style="text-align: right;">Mini Zoo</th>
                                <th style="text-align: right;">BE Store</th>
                                <th style="text-align: right;">BE Bar</th>
                                <th style="text-align: right;">BE Class</th>
                                <th style="text-align: right;">Gelato</th>
                                <th style="text-align: right;">Total (Rp)</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        ';
                        
                            $no = 1; 
                            foreach($arr_data["list_date"] as $date=>$val)
                            {
                                $pemakaian_voucher_ticket = $arr_data["pemakaian_voucher_ticket"][date("Y-m-d", $date)];
                                $TotalNilai = $arr_data["total_TotalNilai"][date("Y-m-d", $date)];
                                
                                $total_OH = $arr_data["total_OH"][date("Y-m-d", $date)];
                                $total_The_Luwus = $arr_data["total_The_Luwus"][date("Y-m-d", $date)];
                                $total_Rice_View = $arr_data["total_Rice_View"][date("Y-m-d", $date)];
                                $total_Juice_Corner = $arr_data["total_Juice_Corner"][date("Y-m-d", $date)];
                                $total_Mini_Zoo = $arr_data["total_Mini_Zoo"][date("Y-m-d", $date)];
                 				$total_Burger_Corner = $arr_data["total_Burger_Corner"][date("Y-m-d", $date)];
                                $total_BE_Bar = $arr_data["total_BE_Bar"][date("Y-m-d", $date)];
                                $total_BE_Store = $arr_data["total_BE_Store"][date("Y-m-d", $date)];
                                $total_BE_Class = $arr_data["total_BE_Class"][date("Y-m-d", $date)];
                                $total_Gelato = $arr_data["total_Gelato"][date("Y-m-d", $date)];
								
								//$total_LR = 0;
                                
                                $lokal = $arr_data["lokal"][date("Y-m-d", $date)];
                                $asia = $arr_data["asia"][date("Y-m-d", $date)];
                                $barat = $arr_data["barat"][date("Y-m-d", $date)];
                                $all_ticket = $arr_data["all_ticket"][date("Y-m-d", $date)];
                                $harga_tiket = $arr_data["harga_tiket"][date("Y-m-d", $date)];
                                //$pemakaian_voucher_ticket = 0;
                                $nett_ticket = $harga_tiket;
                                
                                
                                $komisi = $arr_data["total_komisi"][date("Y-m-d", $date)];
                                $compliment = $arr_data["total_compliment"][date("Y-m-d", $date)];
                                
                                $omzet = ($TotalNilai - $komisi) + $nett_ticket - $pemakaian_voucher_ticket;
                                
                                $bg_hari ="";
                                if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
                                {
                                    $bg_hari = "background: #ff99cc;";
                                }
                                
                               
                        
                            $echo .= '
                            <tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td>'.$no.'</td>
                                <td>'.date("d/m/Y", $date).'</td>
                                <td style="'.$bg_hari.'">'.date("l", $date).'</td>
                                <td align="right">'.format_number($lokal, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($asia, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($barat, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($all_ticket, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($nett_ticket, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_OH, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_The_Luwus, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_Rice_View, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_Juice_Corner, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_Burger_Corner, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_Mini_Zoo, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_BE_Store, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_BE_Bar, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_BE_Class, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($total_Gelato, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($TotalNilai, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($komisi, 0, "", "", "ind").'</td>
                                <td align="right"><a class="link_pop" href="javascript:void(0)" onclick="pop_up_pemakaian_voucher(&quot;'.date("d/m/Y", $date).'&quot;)">'.format_number($pemakaian_voucher_ticket, 0, "", "", "ind").'</a></td>
                                <td align="right">'.format_number($compliment, 0, "", "", "ind").'</td>
                                <td align="right">'.format_number($omzet, 0, "", "", "ind").'</td>
                            </tr>
                            ';
                                
                                
                                $arr_total["lokal"] += $lokal;
                                $arr_total["asia"] += $asia;
                                $arr_total["barat"] += $barat;
                                $arr_total["all_ticket"] += $all_ticket;
                                $arr_total["nett_ticket"] += $nett_ticket;
                                $arr_total["OH"] += $total_OH;
                                $arr_total["The_Luwus"] += $total_The_Luwus;
                                $arr_total["Rice_View"] += $total_Rice_View;
                                $arr_total["Juice_Corner"] += $total_Juice_Corner;
                                $arr_total["Burger_Corner"] += $total_Burger_Corner;
                                $arr_total["Mini_Zoo"] += $total_Mini_Zoo;
                                $arr_total["BE_Store"] += $total_BE_Store;
                                $arr_total["BE_Bar"] += $total_BE_Bar;
                                $arr_total["BE_Class"] += $total_BE_Class;
                                $arr_total["Gelato"] += $total_Gelato;
                                $arr_total["Nilai"] += $TotalNilai;
                                $arr_total["Komisi"] += $komisi;
                                $arr_total["pemakaian_voucher"] += $pemakaian_voucher_ticket;
                                $arr_total["Compliment"] += $compliment;
                                $arr_total["Omzet"] += $omzet;
                        
                                $no++;
                            }
                            
                        $echo .= '</tbody>';
                        
                        
                            
                                $echo .= '
                                 <tfoot>
                                    <tr style="text-align: right; font-weight: bold; color: black;">
                                        <td colspan="3">Grand Total</td>
                                        <td align="right">'.format_number($arr_total["lokal"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["asia"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["barat"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["all_ticket"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["nett_ticket"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["OH"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["The_Luwus"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Rice_View"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Juice_Corner"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Burger_Corner"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Mini_Zoo"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["BE_Store"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["BE_Bar"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["BE_Class"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Gelato"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Nilai"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Komisi"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["pemakaian_voucher"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Compliment"], 0, "", "", "ind").'</td>
                                        <td align="right">'.format_number($arr_total["Omzet"], 0, "", "", "ind").'</td>
                                    </tr>
                                </tfoot> 
                                ';
                          
                        
                    
                    $echo .= '</table>';
                    
                    echo $echo;
                    
                    //$echo = "TEST";
                    
                    
                    if($kirim_email=="sgv2016")
                    {
                        $subject = "Report Daily ".$v_date_from." s/d ".$v_date_to;
                        
                        $body = $echo;
                        
                        $to = "";
                        $to_name = "";
                        $q = "
                                SELECT
                                    ".$db["master"].".function_email.email_address,
                                    ".$db["master"].".function_email.email_name
                                FROM
                                    ".$db["master"].".function_email
                                WHERE
                                    1
                                    AND ".$db["master"].".function_email.func_name = 'report_daily'
                                ORDER BY
                                    ".$db["master"].".function_email.email_address ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($email_address, $email_name) = $row;
                            
                            $to .= $email_address.";";
                            $to_name .= $email_name.";";
                        }
                        
                        $author = "Cron Job";
                        
                        $kirim = send_email_multiple($subject, $body, $to, $to_name, $author);   
                        //$kirim = smtpmailer($author, $to, $to_name, $subject, $body);
                        die();
                    }
                    
                }
                
                if(!$btn_excel)
                {
            ?>
            
        
        </div>
        
        </form>
        <?php 
                
        ?>
        
<?php 
            include("footer.php"); 

            }
?>