<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Lokasi Kerja Form";
    $list  = "npm_lokasi_kerja.php";
    $htm   = "npm_lokasi_kerja_form.php";
    $pk    = "absensi_cabang_id";
    $title = "absensi_cabang_name";
    
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
		$v_absensi_cabang_name = save_char($_POST["v_absensi_cabang_name"]);
		$v_address_1           = save_char($_POST["v_address_1"]);
		$v_address_2           = save_char($_POST["v_address_2"]);
		$v_city                = save_char($_POST["v_city"]);
		$v_pic                 = save_char($_POST["v_pic"]);

		$v_format_data         = save_char($_POST["v_format_data"]);
		$v_format_file         = save_char($_POST["v_format_file"]);
		$v_user_id             = save_char($_POST["v_user_id"]);
		$v_user_name           = save_char($_POST["v_user_name"]);
		$v_absensi_type        = save_char($_POST["v_absensi_type"]);
		$v_absensi_date        = save_char($_POST["v_absensi_date"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                    UPDATE
                        ".$db["master"].".absensi_cabang
                    SET
                        absensi_cabang_name = '".$v_absensi_cabang_name."',
                        address_1 = '".$v_address_1."',
                        address_2 = '".$v_address_2."',
                        city = '".$v_city."',
                        pic = '".$v_pic."',
                        format_data = '".$v_format_data."',
                        format_file = '".$v_format_file."',
                        edited = '".ffclock()."'
                    WHERE
                        absensi_cabang_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update ||".$q;
                }
                else
                {
                    $class_warning = "success";
                    $msg = "Berhasil menyimpan ".$v_absensi_cabang_name."<br>";
                }
            }
        }
        else
        {   
            $id = get_counter_int($db["master"],"absensi_cabang","absensi_cabang_id",100);   
            
            $q = "
                INSERT INTO
                    ".$db["master"].".absensi_cabang
                SET
                    absensi_cabang_id = '".$id."',
                    absensi_cabang_name = '".$v_absensi_cabang_name."',
                    address_1 = '".$v_address_1."',
                    address_2 = '".$v_address_2."',
                    city = '".$v_city."',
                    format_data = '".$v_format_data."',
                    format_file = '".$v_format_file."',
                    author = '".ffclock()."',
                    edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert ||".$q;
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
        
    $q = "
        SELECT 
            ".$db["master"].".absensi_cabang.*
        FROM 
            ".$db["master"].".absensi_cabang
        WHERE
            1
            AND ".$db["master"].".absensi_cabang.absensi_cabang_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
		function validasi()
		{
			if(document.getElementById("v_absensi_cabang_name").value=="")
			{
				alert("Nama harus diisi");
				document.getElementById("v_absensi_cabang_name").focus();
				return false;
			}
		}
		
		function start_page()
		{
			document.getElementById("v_absensi_cabang_name").focus();	
		}
	    
	    function change_format_data(nilai)
	    {
	        document.getElementById('show_setup_kolom').style.display = 'none';
	        
	        if(nilai=="finger_print")
	        {
	            document.getElementById('show_setup_kolom').style.display = '';
	            document.getElementById('v_user_id').focus();
	        }
	    }
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
	        	<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    
			    	<tr>
                        <td class="title_table" width="150">Nama</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["absensi_cabang_name"]; ?>" name="v_absensi_cabang_name" id="v_absensi_cabang_name" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Alamat</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["address_1"]; ?>" name="v_address_1" id="v_address_1" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">&nbsp;</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["address_2"]; ?>" name="v_address_2" id="v_address_2" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Kota</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["city"]; ?>" name="v_city" id="v_city" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">PIC Absensi</td>
                        <td> 
                            <select name="v_pic" id="v_pic" class="form-control-new" style="width: 280px;">
                                <option value="">-</option>
                                
                                <?php 
                                    $q="
                                    	SELECT 
										  employee.employee_id,
										  employee.employee_name,
										  absensi_admin.username 
										FROM
										  absensi_admin 
										  INNER JOIN employee 
										    ON absensi_admin.username = employee.username 
										GROUP BY absensi_admin.username
                                    ";
                                    $qry_emp = mysql_query($q);
                                    while($row_emp = mysql_fetch_array($qry_emp))
                                    {     
                                          $selected = "";
                                          if($data["pic"]==$row_emp["employee_id"])
                                          {
                                              $selected = "selected='selected'";
                                          }
                                          
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row_emp["employee_id"]; ?>"><?php echo $row_emp["employee_name"]; ?></option>
                                <?php 
                                    } 
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table">Format Absensi</td>
                        <td> 
                            <?php 
                                if($data["format_data"]=="" || $data["format_data"]=="std")
                                {
                                    $checked_std = "checked='checked'";
                                    $checked_fp  = "";
                                }
                                else
                                {
                                    $checked_std = "";
                                    $checked_fp  = "checked='checked'";
                                }
                            ?>
                            <label><input type="radio" <?php echo $checked_std; ?> name="v_format_data" id="v_format_data" value="std" onchange="change_format_data(this.value)">&nbsp;Standart</label>
                            &nbsp;
                            <label><input type="radio" <?php echo $checked_fp; ?> name="v_format_data" id="v_format_data" value="finger_print" onchange="change_format_data(this.value)">&nbsp;Finger Print</label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table" width="150">Format File</td>
                        <td>
                            <select class="form-control-new" name="v_format_file" id="v_format_file" style="width:77px;">
                                <option <?php if($data["format_file"]=="xls") echo "selected='selected'"; ?> value="xls">xls</option>
                                <option <?php if($data["format_file"]=="txt") echo "selected='selected'"; ?> value="txt">txt</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                     	<td>&nbsp;</td>
                        <td>
                        	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
						 	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
						</td>
                    </tr> 
		            
			    </table>			    
			    </form>
			
				<?php
		            if($id!="")
		            {
		         ?>
		         	<ol class="breadcrumb">
						<li><strong><i class="entypo-vcard"></i>Information data</strong></li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		         ?>
		         
			</div>
		</div>
		
<?php include("footer.php"); ?>
