<?php 
    include("header.php");
    
    $msg = "";
    $class_warning = "error";
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["cek"])){ $cek = isset($_GET["cek"]); } else { $cek = $_GET["cek"]; }
    if(!isset($_GET["option"])){ $option = isset($_GET["option"]); } else { $option = $_GET["option"]; }
    if(!isset($_REQUEST["id"])){ $id = isset($_REQUEST["id"]); } else { $id = $_REQUEST["id"]; }
    if(!isset($_REQUEST["action"])){ $action = isset($_REQUEST["action"]); } else { $action = $_REQUEST["action"]; }
	if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
	
	if(!isset($_REQUEST["ajax"])){ $ajax = isset($_REQUEST["ajax"]); } else { $ajax = $_REQUEST["ajax"]; }
    if(!isset($_POST["del"])){ $del = isset($_POST["del"]); } else { $del = $_POST["del"]; }
	
	if($ajax)
	{
		if($ajax=="save_ajax")
		{
			$pk_id  			= $_GET["pk_id"];
            $id                 = $pk_id;
			$v_sid			    = $_GET["v_sid"];
			$v_employee_id      = save_char($_GET["v_employee_id"]);
            $v_remarks          = save_char($_GET["v_remarks"]);
			
			if($v_sid*1==0)
			{
				$counter_details = get_counter_int($db["master"],"setup_atasan_details","sid",100);
                                
				$q = "
						INSERT INTO
							".$db["master"].".setup_atasan_details
						SET
							sid = '".$counter_details."',
							setup_atasan_id = '".$id."',
                            employee_id = '".$v_employee_id."',
							remarks = '".$v_remarks."'
				";
				if(!mysql_query($q))
				{
					$msg = "Gagal Insert<br>";
				}
			}
			else
			{
				// update
				$q = "
					UPDATE
						".$db["master"].".setup_atasan_details
					SET
						employee_id = '".$v_employee_id."',
                        remarks = '".$v_remarks."'
					WHERE
						sid = '".$v_sid."'
				";
				if(!mysql_query($q))
				{
					$msg .= "Failed update details ||".$q."<br>";
				}    
			}
			
			include("npm_setup_atasan_form_details.php");
		}
		exit();
	}
    
    $link_adjust = "?v_keyword=".$v_keyword."&p=".$p."&action=".$action."&id=".$id;
    $link_back = "?v_keyword=".$v_keyword."&p=".$p;
	$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $modul = "Setup Atasan";
    $list  = "npm_setup_atasan.php";
    $htm   = "npm_setup_atasan_form.php";
    $pk    = "employee_id";
	
    $v_cek = md5($id);
    
    if($action=="edit")
    {
        $show_btn = "Save";
    }
    else if($action=="delete")
    {
        $show_btn = "Delete";
    }
    else
    {
        $show_btn = "Save";
    }
    
    if(isset($_REQUEST["btn_del_details"])!="")
    {   
		while(list($key, $val)=@each($del))
		{
		    $q = "
			    DELETE FROM
				    ".$db["master"].".setup_atasan_details
			    WHERE
				    sid = '".$val."'    
		    ";
		    mysql_query($q);
		}
        
        $class_warning = "success";
        $msg = "Berhasil Menghapus<br>";
    }
    
    if(isset($_REQUEST["btn_save"])!="")
    {
		$v_employee_id    = save_char($_POST["v_employee_id"]);
        $v_remarks        = save_char($_POST["v_remarks"]);
        
        if($id!="")
        {
            if($action=="edit")
            {
                $q = "
                        UPDATE
                            ".$db["master"].".setup_atasan
                        SET
                            remarks = '".$v_remarks."',
                            edited = '".ffclock()."'
                        WHERE
                            setup_atasan_id = '".$id."'
                ";
                if(!mysql_query($q))
                {
                    $msg = "Failed Update ||".$q;
                }
                else
                {
                    $no = $_POST["no"];
                    
                    while(list($key, $val)=@each($no))
                    {
                        $v_sid                = $_POST['v_sid_'.$val];
                        $v_employee_id_det    = $_POST['v_employee_id_'.$val];
                        $v_remarks_det        = save_char($_POST['v_remarks_'.$val]);
                        
                        if($v_employee_id_det)
                        {
                            if($v_sid*1==0)
                            {
                                // insert
                                $counter_details = get_counter_int($db["master"],"setup_atasan_details","sid",100);
                                
                                $q = "
                                        INSERT INTO
                                            ".$db["master"].".setup_atasan_details
                                        SET
                                            sid = '".$counter_details."',
                                            setup_atasan_id = '".$id."',
                                            employee_id = '".$v_employee_id_det."',
                                            remarks = '".$v_remarks_det."'
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg .= "Gagal<br>";
                                }
                            }
                            else
                            {
                                // update
                                $q = "
                                        UPDATE
                                            ".$db["master"].".setup_atasan_details
                                        SET
                                            employee_id = '".$v_employee_id_det."',
                                            remarks = '".$v_remarks_det."'
                                        WHERE
                                            sid = '".$v_sid."'
                                ";
                                
                                if(!mysql_query($q))
                                {
                                    $msg .= "Gagal ||".$q."<br>";
                                }    
                            }
                            
                            //echo $q."<hr>"; 
                        }
                        
                    }

                }
				$class_warning = "success";
                $msg .= "Berhasil menyimpan";
            }
            
        }
        else
        {   
            $id = get_counter_int($db["master"],"setup_atasan","setup_atasan_id",100);   
            
            $q = "
                    INSERT INTO
                        ".$db["master"].".setup_atasan
                    SET
                        setup_atasan_id = '".$id."',
                        employee_id = '".$v_employee_id."',
                        remarks = '".$v_remarks."',
                        author = '".ffclock()."',
                        edited = '".ffclock()."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Failed Insert ||".$q;
            }
            else
            {
                header("Location: ".$htm.$link_back.$link_order_by."&action=edit&id=".$id."");
            }
        }
    }
    
    $q = "
        SELECT 
            ".$db["master"].".employee.employee_id,
            ".$db["master"].".employee.employee_nik,
            ".$db["master"].".employee.employee_code_hrd,
            ".$db["master"].".employee.employee_name,
            
            ".$db["master"].".setup_atasan.author,
            ".$db["master"].".setup_atasan.edited,
            ".$db["master"].".setup_atasan.remarks
            
        FROM 
            ".$db["master"].".setup_atasan
            INNER JOIN ".$db["master"].".employee ON
                ".$db["master"].".employee.employee_id = ".$db["master"].".setup_atasan.employee_id
        WHERE
            1
            AND ".$db["master"].".setup_atasan.setup_atasan_id = '".$id."'
        LIMIT 0,1
    ";
    $qry["data"] = mysql_query($q);
    $data = mysql_fetch_array($qry["data"]);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	    
	<script type="text/javascript">
		function validasi()
		{
			if(document.getElementById("v_employee_id").value=="")
			{
				alert("Atasan harus dipilih");
				document.getElementById("v_employee_id").focus();
				return false;
			}
		}
		
		function start_page()
		{
			
		}
	    
	    function pop_up_search_employee()
	    {
	        var v_date;
	        v_date = "<?php echo date_now("d/m/Y"); ?>";
	        
	        
	        //alert(v_date); 
	        if(v_date=="")
	        {
	            alert("Tanggal Pengajuan harus diisi");
	        }
	        else
	        {
	            windowOpener('600', '800', 'Cari Karyawan', 'npm_pop_up_search_employee_cuti.php?v_date='+v_date, 'Cari Karyawan');
	        }
	    }
	    
	    function pop_up_search_employee_details(nilai)
	    {
	        var v_date;
	        var pk_id;
	        v_date = "<?php echo date_now("d/m/Y"); ?>";
	        pk_id  = "<?php echo $id; ?>";
	        //alert(v_date); 
	        if(v_date=="")
	        {
	            alert("Tanggal Pengajuan harus diisi");
	        }
	        else
	        {
	            windowOpener('600', '800', 'Cari Karyawan', 'npm_setup_atasan_pop_up_employee.php?id='+pk_id+'&v_date='+v_date+'&v_nilai='+nilai, 'Cari Karyawan');
	        }    
	    }

	    function get_employee()
	    {
			windowOpener('600', '800', 'Cari Karyawan', 'npm_setup_atasan_employee.php', 'Cari Karyawan');		
	    }
	    
		function CallAjaxForm(vari,param1)
		{
			if (param1 == undefined) param1 = '';
			
			document.getElementById('show_image_ajax_form').style.display = '';
			
			if (vari == 'save_ajax')
			{
				var variabel;
				
				var pk_id;
				var v_sid;
				var v_employee_id;
	            var v_remarks;
				
				pk_id = '<?php echo $id; ?>';
				
	            v_sid           = document.getElementById("v_sid_"+param1).value;
				v_employee_id   = document.getElementById("v_employee_id_"+param1).value;
	            v_remarks       = document.getElementById("v_remarks_"+param1).value;
				
				variabel  = '';
				variabel += 'pk_id='+pk_id;
				variabel += '&v_sid='+v_sid;
				variabel += '&v_employee_id='+v_employee_id;
	            variabel += '&v_remarks='+v_remarks;
				
				//alert(variabel);
				
				if(v_employee_id*1==0)
				{
					alert('Karyawan harus dipilih...');
					document.getElementById('show_image_ajax_form').style.display = 'none';
				}
				else
				{
					xmlhttp.open('get', 'npm_setup_atasan_form.php?ajax=save_ajax&'+variabel, true);
					xmlhttp.onreadystatechange = function() {
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById('table_details').innerHTML = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display = 'none';
							alert("Berhasil menyimpan");
						}
						return false;
					}
					xmlhttp.send(null);
				}
			}
		}
	    
	    function mouseover(target)
        {  
            if(target.bgColor!="#cafdb5"){        
                if (target.bgColor=='#ccccff')
                    target.bgColor='#ccccff';
                else
                    target.bgColor='#c1cdd8';
            }
        }
            
        function mouseout(target)
        {
            if(target.bgColor!="#cafdb5"){ 
                if (target.bgColor=='#ccccff')
                    target.bgColor='#ccccff';
                else
                    target.bgColor='#FFFFFF';
                    
            }    
        }

        function mouseclick(target, idobject, num)
        {
                           
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
            for(i=0;i<num;i++){
                if (document.getElementById(idobject+'_'+i) != undefined){
                    document.getElementById(idobject+'_'+i).bgColor='#f5faff';
                    if (target.id == idobject+'_'+i)
                        target.bgColor='#ccccff';
                }
            }
        }

        function mouseclick1(target)
        {
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
            if(target.bgColor!="#cafdb5")
            {
                target.bgColor="#cafdb5";
            }
            else
            {
                target.bgColor="#FFFFFF";
            }
        }
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
		<div class="row">
	        <div class="col-md-12" align="left">
	        
				<ol class="breadcrumb">
					<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
					<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
				</ol>
				
				<?php include("warning.php"); ?>
				
				<form method="post" name="theform" id="theform" onSubmit="return validasi()">
			    <input type="hidden" name="id" value="<?php echo $id; ?>">
			    <table class="table table-bordered responsive">
			    	
			    	<tr>
                        <td class="title_table" width="150"><b>Atasan</b></td>
                        <td>
						<?php 
							if($id)
							{
						?>
                            	<input type="hidden" name="v_employee_id" id="v_employee_id" value="<?php echo $data["employee_id"]; ?>">
                            	<?php echo $data["employee_nik"]." :: ".$data["employee_name"]; ?>
						<?php
							}
							else
							{
						?>
								<input type="hidden" name="v_employee_id" id="v_employee_id" value="">
								<span id="td_employee"></span>
								<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Search Employee" title="" onclick="get_employee()">
									<i class="entypo-dot-3"></i>
								</button>	
						<?php
							}
						?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table" >Keterangan</td>
                        <td> 
                            <input type="text" class="form-control-new" value="<?php echo $data["remarks"]; ?>" name="v_remarks" id="v_remarks" maxlength="255" size="50">
                        </td>
                    </tr>
                    
                    <tr>
                     	<td class="title_table" >&nbsp;</td>
                        <td>
                        
                            <?php 
                                if($show_btn=="Delete")
                                {
                            ?>
                            		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="confirm_delete('<?php echo $htm.$link_adjust; ?>&cek=<?php echo md5($id); ?>')" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
                            <?php 
                                }
                                else
                                {
                            ?>
                            		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="<?php echo $show_btn; ?>"><?php echo $show_btn; ?><i class="entypo-check"></i></button>
                             <?php 
                                }
                            ?>
                            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close" onClick="get_url('<?php echo $list.$link_back; ?>')"  value="Close">Close<i class="entypo-cancel"></i></button>
                        </td>
                    </tr>
                    
                    <?php 
					if($id!="")
					{
					?>
	                    <tr>
	                        <td colspan="100%" id="table_details">
	                            <?php 
	                                include("npm_setup_atasan_form_details.php");
	                            ?>    
	                        </td>
	                    </tr>
                  	<?php 
				  	}
				  	?> 
                    
			    </table>			    
			    </form>
			
				<?php
		            if($id!="")
		            {
		         ?>
				    <ol class="breadcrumb">
						<li>
							<a href="javascript:void(0)">
								<i class="entypo-vcard"></i>Information data
							</a>
						</li>
					</ol>
					
			         <table class="table table-bordered responsive">
			            <tr>
			            	<td class="title_table" width="150">Author</td>
				            <td><?php echo $data["author"]; ?></td>
			            </tr>
			            <tr>
			            	<td class="title_table" width="150">Edited</td>
				            <td><?php echo $data["edited"]; ?></td>
			            </tr>
			         </table>		       
		         <?php 
		            }
		         ?>
		         
			</div>
		</div>
		
<?php include("footer.php"); ?>   