<?php 
    include("header.php");
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
    if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }
    
    $link_adjust    = "?v_keyword=".$v_keyword."&p=".$p;
    $link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;
    
    $icon_type_change = "entypo-up-dir";
    $order_type_change = "asc";
    if($order_type=="asc")
    {
        $order_type_change = "desc";
    	$icon_type_change = "entypo-down-dir";
    }
    
    $order_by_content = "";
    if($order_by!="")
    {
        $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
    }
    
    $modul = "FA Type";
    $list  = "npm_fa_type.php";
    $htm   = "npm_fa_type_form.php";
    $pk    = "type_id";
    $title = "type_name";
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>HRGA</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="get">
		<div class="row">
			<div class="col-md-10">
				Search&nbsp;
				<input type="text" size="30" maxlength="30" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>">
				&nbsp;
			</div>
			
			<div class="col-md-2" align="right">
				<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100),get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>')">Tambah<i class="entypo-plus"></i></button>
			</div>
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="30">No</th>
                        <th><a href="<?php echo $list.$link_adjust."&order_by=fa_kategory.kategory_name&order_type=".$order_type_change; ?>" class="link_menu">Kategori<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><a href="<?php echo $list.$link_adjust."&order_by=fa_group_asset.group_asset_name&order_type=".$order_type_change; ?>" class="link_menu">Group Asset<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><a href="<?php echo $list.$link_adjust."&order_by=fa_type.type_name&order_type=".$order_type_change; ?>" class="link_menu">Type Name<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th><a href="<?php echo $list.$link_adjust."&order_by=fa_metode_penyusutan.metode_penyusutan_name&order_type=".$order_type_change; ?>" class="link_menu">Metode Penyusutan<i class="<?php echo $icon_type_change; ?>"></i></a></center></th>
                        <th>Navigasi</th>
                    </tr>
				</thead>
				<tbody>
					
					<?php
                    $keyWord = trim($v_keyword);
                  
                    if($keyWord == '')
                    {
                        $sql = "
                                    SELECT 
                                        fa_type.type_id,
                                        fa_kategory.kategory_name,
                                        fa_group_asset.group_asset_name,
                                        fa_type.type_name,
                                        fa_metode_penyusutan.metode_penyusutan_name
                                    FROM 
                                        fa_type
                                        INNER JOIN fa_kategory ON
                                            fa_type.kategory_id = fa_kategory.kategory_id
                                        INNER JOIN fa_group_asset ON
                                            fa_type.group_asset_id = fa_group_asset.group_asset_id
                                        LEFT JOIN fa_metode_penyusutan ON
                                            fa_type.metode_penyusutan_id = fa_metode_penyusutan.metode_penyusutan_id
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        fa_kategory.kategory_name ASC,
                                        fa_group_asset.group_asset_name ASC,
                                        fa_type.type_name ASC
                                ";
                        $query = mysql_query($sql);
                        $max = ceil(mysql_num_rows($query)/$jml_page);
                        $s = $jml_page * $p;
                        $sql = "
                                    SELECT 
                                        fa_type.type_id,
                                        fa_kategory.kategory_name,
                                        fa_group_asset.group_asset_name,
                                        fa_type.type_name,
                                        fa_metode_penyusutan.metode_penyusutan_name
                                    FROM 
                                        fa_type
                                        INNER JOIN fa_kategory ON
                                            fa_type.kategory_id = fa_kategory.kategory_id
                                        INNER JOIN fa_group_asset ON
                                            fa_type.group_asset_id = fa_group_asset.group_asset_id
                                        LEFT JOIN fa_metode_penyusutan ON
                                            fa_type.metode_penyusutan_id = fa_metode_penyusutan.metode_penyusutan_id
                                    WHERE
                                        1
                                    ORDER BY
                                        ".$order_by_content." 
                                        fa_kategory.kategory_name ASC,
                                        fa_group_asset.group_asset_name ASC,
                                        fa_type.type_name ASC
                                    LIMIT ".$s.", ".$jml_page." 
                                ";
                    }
                    else if($keyWord != '')
                    {
                        
                        unset($arr_keyword);
						$arr_keyword[0] = "fa_type.type_id";
						$arr_keyword[1] = "fa_kategory.kategory_name";
                        $arr_keyword[2] = "fa_group_asset.group_asset_name";
                        $arr_keyword[3] = "fa_type.type_name";
                        $arr_keyword[4] = "fa_metode_penyusutan.metode_penyusutan_name";
						
						$search_keyword = search_keyword($v_keyword, $arr_keyword);
						$where = $search_keyword;
                        
                        $flag=0;
                        for($i=0; $i < strlen($keyWord); $i++)
                        {
                            if($keyWord[$i] == '\'') $flag++;
                            if($keyWord[$i] == '<') $flag++;
                            if($keyWord[$i] == '>') $flag++;
                        }
                        
                        if($flag==0)
                        {
                            $sql = "
                                        SELECT 
                                            fa_type.type_id,
                                            fa_kategory.kategory_name,
                                            fa_group_asset.group_asset_name,
                                            fa_type.type_name,
                                            fa_metode_penyusutan.metode_penyusutan_name
                                        FROM 
                                            fa_type
                                            INNER JOIN fa_kategory ON
                                                fa_type.kategory_id = fa_kategory.kategory_id
                                            INNER JOIN fa_group_asset ON
                                                fa_type.group_asset_id = fa_group_asset.group_asset_id
                                            LEFT JOIN fa_metode_penyusutan ON
                                                fa_type.metode_penyusutan_id = fa_metode_penyusutan.metode_penyusutan_id
                                        WHERE
                                            1
                                            ".$where."
                                        ORDER BY
                                            ".$order_by_content." 
                                            fa_kategory.kategory_name ASC,
                                            fa_group_asset.group_asset_name ASC,
                                            fa_type.type_name ASC
                                   ";
                            $query = mysql_query($sql);
                            $max = ceil(mysql_num_rows($query)/$jml_page);
                            $s = $jml_page * $p;
                            $sql = "
                                         SELECT 
                                            fa_type.type_id,
                                            fa_kategory.kategory_name,
                                            fa_group_asset.group_asset_name,
                                            fa_type.type_name,
                                            fa_metode_penyusutan.metode_penyusutan_name
                                        FROM 
                                            fa_type
                                            INNER JOIN fa_kategory ON
                                                fa_type.kategory_id = fa_kategory.kategory_id
                                            INNER JOIN fa_group_asset ON
                                                fa_type.group_asset_id = fa_group_asset.group_asset_id
                                            LEFT JOIN fa_metode_penyusutan ON
                                                fa_type.metode_penyusutan_id = fa_metode_penyusutan.metode_penyusutan_id
                                        WHERE
                                            1
                                            ".$where."
                                        ORDER BY
                                            ".$order_by_content." 
                                            fa_kategory.kategory_name ASC,
                                            fa_group_asset.group_asset_name ASC,
                                            fa_type.type_name ASC
                                        LIMIT ".$s.", ".$jml_page." 
                                  ";
                        }else
                        {
                            $msg = "Your input are not allowed!";
                        }
                    }
                    
                    $query = mysql_query($sql);
                            $sv = 0;
                            $i=1+($jml_page*$p);
                    if(!$row = mysql_num_rows($query))
                    {
                     echo "<tr>";
                     echo "<td align=\"center\" colspan=\"100%\">";
                     echo "No Data";
                     echo "</td>";
                     echo "</tr>";
                    }
                    else
                    {
                        while ($row = mysql_fetch_array($query))
                        {
                            $sv++;
                            
                            $bgcolor = "";
                            if($i%2==0)
                            {
                                $bgcolor = "background:#E7E7E7;";
                            }
                 ?>
                        <tr title="<?php echo $row[$title]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $i; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["kategory_name"]; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["group_asset_name"]; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["type_name"]; ?></td>
                            <td onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')"><?php echo $row["metode_penyusutan_name"]; ?></td>
                            <td align="center">
                            	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=edit&id=<?php echo $row[$pk]; ?>')">
									<i class="entypo-pencil"></i>
								</button>
								
								<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="get_url('<?php echo $htm.$link_adjust.$link_order_by; ?>&action=delete&id=<?php echo $row[$pk]; ?>')">
									<i class="entypo-trash"></i>
								</button> 
                            </td>
                        </tr>
                 <?php
                            $i = $i+1; 
                        }
                    }
                 ?>
                 	
				</tbody>
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>
		
<?php include("footer.php"); ?>           