<?php 
    include("header.php"); 
    
    if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
    if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
    
    if(!isset($_GET["search_status"])){ $search_status = isset($_GET["search_status"]); } else { $search_status = $_GET["search_status"]; }    
    if(!isset($_GET["search_company_id"])){ $search_company_id = isset($_GET["search_company_id"]); } else { $search_company_id = $_GET["search_company_id"]; }
    if(!isset($_GET["search_cabang_id"])){ $search_cabang_id = isset($_GET["search_cabang_id"]); } else { $search_cabang_id = $_GET["search_cabang_id"]; }
    
    $link_adjust  = "?v_keyword=".$v_keyword."&p=".$p;
    $link_adjust .= "&search_status=".$search_status;
    $link_adjust .= "&search_company_id=".$search_company_id;
    $link_adjust .= "&search_cabang_id=".$search_cabang_id;
    
    $list  = "npm_employee_search.php";
    $pk    = "employee_id";
    $title = "employee_name";
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul HRGA - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
	    function start_page()
	    {
	        document.getElementById("v_keyword").focus();
	    }
		
		function get_choose(employee_id,employee_nik,employee_code_hrd,employee_name)
		{
			window.opener.document.forms["theform"]["v_employee_id"].value          = employee_id;
	        window.opener.document.forms["theform"]["v_employee_nik"].value         = employee_nik;
	        window.opener.document.forms["theform"]["v_employee_code_hrd"].value    = employee_code_hrd;
			window.opener.document.forms["theform"]["v_employee_name"].value        = employee_name;
	        
			self.close() ;
			return; 
		}
	</script>
</head>
<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<form method="get">
		<input type="hidden" name="search_status" value="<?php echo $search_status; ?>">
		<input type="hidden" name="search_company_id" value="<?php echo $search_company_id; ?>">
		<input type="hidden" name="search_cabang_id" value="<?php echo $search_cabang_id; ?>">

		<div class="row">
			<div class="col-md-12">
				
				<select class="form-control-new" name="search_status" id="search_status">
                    <option value="">Status</option>
                    <option <?php if($search_status=="join") echo "selected='selected'"; ?> value="join">Active</option>
                    <option <?php if($search_status=="resign") echo "selected='selected'"; ?> value="resign">Resign</option>
                </select>
                &nbsp;   
                <select class="form-control-new" name="search_company_id" id="search_company_id">
                    <option value="">Perusahaan</option>
                    <?php 
                        $q = "
                                SELECT
                                    ".$db["master"].".company.company_id,
                                    ".$db["master"].".company.company_name,
                                    ".$db["master"].".company.company_initial
                                FROM
                                    ".$db["master"].".company
                                WHERE
                                    1
                                ORDER BY
                                    ".$db["master"].".company.company_initial ASC
                        ";
                        $qry_comp = mysql_query($q);
                        while($row_comp = mysql_fetch_array($qry_comp))
                        {
                            $selected = "";
                            if($search_company_id==$row_comp["company_id"])      
                            {
                                $selected = "selected='selected'";
                            }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_comp["company_id"]; ?>"><?php echo $row_comp["company_initial"]; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
                <select class="form-control-new" name="search_cabang_id" id="search_cabang_id">
                    <option value="">Cabang</option>
                    <?php 
                        $q = "
                                SELECT
                                    ".$db["master"].".hrd_cabang.cabang_id,
                                    ".$db["master"].".hrd_cabang.cabang_name
                                FROM
                                    ".$db["master"].".hrd_cabang
                                WHERE
                                    1
                                ORDER BY
                                    ".$db["master"].".hrd_cabang.cabang_name ASC
                        ";
                        $qry_cab = mysql_query($q);
                        while($row_cab = mysql_fetch_array($qry_cab))
                        {
                            $selected = "";
                            if($search_cabang_id==$row_cab["cabang_id"])      
                            {
                                $selected = "selected='selected'";
                            }
                            
                            $cabang_name = str_replace("Cabang","",$row_cab["cabang_name"]);
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $row_cab["cabang_id"]; ?>"><?php echo $cabang_name; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                &nbsp;
                Pencarian
                &nbsp;
                <input type="text" name="v_keyword" id="v_keyword" class="form-control-new" value="<?php echo $v_keyword; ?>" size="30">
        	
	            <span style="float: right;">
					<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_reload" id="btn_reload" value="Reload">Search<i class="entypo-search"></i></button>
				</span>
				
			</div>	
		</div>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
		                <th width="10"><center>No</center></th>
		                <th><center>NIK</center></th>
		                <th><center>Kode HRD</center></th>
		                <th><center>Nama</center></th>
		                
		                <th><center>Perusahaan</center></th>
		                <th><center>Cabang</center></th>
		                <th><center>Divisi</center></th>
		                <th><center>Jabatan</center></th>
		                <th><center>Pilih</center></th>
		        	</tr>
				</thead>
				<tbody>
				
				<?php
                $keyWord = trim($v_keyword);
              
                if($keyWord == '' && $search_company_id=="" && $search_cabang_id=="" && $search_status=="")
                {
                    $sql = "
                        SELECT 
                            ".$db["master"].".employee.employee_id,
                            ".$db["master"].".employee.employee_nik,
                            ".$db["master"].".employee.employee_code_hrd,
                            ".$db["master"].".employee.employee_name,
                            
                            tbl_position.company_id,
                            tbl_position.company_name,
                            tbl_position.company_initial,
                            tbl_position.cabang_id,
                            tbl_position.cabang_name,
                            tbl_position.depo_id,
                            tbl_position.depo_name,                                                            
                            tbl_position.divisi_id,
                            tbl_position.divisi_name,
                            tbl_position.departemen_id,
                            tbl_position.departemen_name,
                            tbl_position.jabatan_id,
                            tbl_position.jabatan_name
                        FROM 
                            ".$db["master"].".employee
                            LEFT JOIN
                            (
                                SELECT
                                    ".$db["master"].".employee_position.employee_id,
                                    
                                    ".$db["master"].".company.company_id,
                                    ".$db["master"].".company.company_name,
                                    ".$db["master"].".company.company_initial,
                                    
                                    ".$db["master"].".hrd_cabang.cabang_id,
                                    ".$db["master"].".hrd_cabang.cabang_name,
                                    
                                    ".$db["master"].".depo.depo_id,
                                    ".$db["master"].".depo.depo_name,
                                    
                                    ".$db["master"].".hrd_divisi.divisi_id,
                                    ".$db["master"].".hrd_divisi.divisi_name,
                                    
                                    ".$db["master"].".hrd_departemen.departemen_id,
                                    ".$db["master"].".hrd_departemen.departemen_name,
                                    
                                    ".$db["master"].".jabatan.jabatan_id,
                                    ".$db["master"].".jabatan.jabatan_name
                                FROM
                                    ".$db["master"].".employee_position
                                    INNER JOIN ".$db["master"].".company ON
                                        ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                    INNER JOIN ".$db["master"].".hrd_divisi ON
                                        ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                    INNER JOIN ".$db["master"].".hrd_departemen ON
                                        ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                    INNER JOIN ".$db["master"].".depo ON
                                        ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                    INNER JOIN ".$db["master"].".hrd_cabang ON
                                        ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                    INNER JOIN ".$db["master"].".jabatan ON
                                        ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                WHERE
                                    1
                            )as tbl_position ON
                                tbl_position.employee_id = ".$db["master"].".employee.employee_id
                            
                            LEFT JOIN
                            (
                                SELECT 
                                    tbl_fixed.employee_id,
                                    tbl_fixed.v_status,
                                    tbl_fixed.v_date
                                FROM
                                (
                                    SELECT
                                        uni.employee_id,
                                        uni.v_status,
                                        uni.v_date
                                    FROM
                                    (
                                        SELECT
                                            ".$db["master"].".employee_join.employee_id,
                                            'join' as v_status,
                                            ".$db["master"].".employee_join.join_date as v_date
                                        FROM
                                            ".$db["master"].".employee_join
                                        WHERE
                                            1
                                            
                                        UNION ALL
                                        
                                        SELECT
                                            ".$db["master"].".employee_resign.employee_id,
                                            'resign' as v_status,
                                            ".$db["master"].".employee_resign.resign_date as v_date
                                        FROM
                                            ".$db["master"].".employee_resign
                                        WHERE
                                            1
                                    ) as uni
                                    
                                    WHERE
                                        1
                                    GROUP BY
                                        uni.employee_id,
                                        uni.v_date
                                    ORDER BY
                                        uni.employee_id DESC,
                                        uni.v_date DESC 
                                )AS tbl_fixed
                                WHERE
                                    1
                                GROUP BY 
                                    tbl_fixed.employee_id
                            )as tbl_status ON
                                tbl_status.employee_id = ".$db["master"].".employee.employee_id
                                
                        WHERE
                            1
                        ORDER BY
                            ".$db["master"].".employee.employee_name ASC
                    ";
                    $query = mysql_query($sql);
                    $max = ceil(mysql_num_rows($query)/$jml_page);
                    $s = $jml_page * $p;
                    $sql = "
                        SELECT 
                            ".$db["master"].".employee.employee_id,
                            ".$db["master"].".employee.employee_nik,
                            ".$db["master"].".employee.employee_code_hrd,
                            ".$db["master"].".employee.employee_name,
                            
                            tbl_position.company_id,
                            tbl_position.company_name,
                            tbl_position.company_initial,
                            tbl_position.cabang_id,
                            tbl_position.cabang_name,
                            tbl_position.depo_id,
                            tbl_position.depo_name,                                                            
                            tbl_position.divisi_id,
                            tbl_position.divisi_name,
                            tbl_position.departemen_id,
                            tbl_position.departemen_name,
                            tbl_position.jabatan_id,
                            tbl_position.jabatan_name
                        FROM 
                            ".$db["master"].".employee
                            LEFT JOIN
                            (
                                SELECT
                                    ".$db["master"].".employee_position.employee_id,
                                    
                                    ".$db["master"].".company.company_id,
                                    ".$db["master"].".company.company_name,
                                    ".$db["master"].".company.company_initial,
                                    
                                    ".$db["master"].".hrd_cabang.cabang_id,
                                    ".$db["master"].".hrd_cabang.cabang_name,
                                    
                                    ".$db["master"].".depo.depo_id,
                                    ".$db["master"].".depo.depo_name,
                                    
                                    ".$db["master"].".hrd_divisi.divisi_id,
                                    ".$db["master"].".hrd_divisi.divisi_name,
                                    
                                    ".$db["master"].".hrd_departemen.departemen_id,
                                    ".$db["master"].".hrd_departemen.departemen_name,
                                    
                                    ".$db["master"].".jabatan.jabatan_id,
                                    ".$db["master"].".jabatan.jabatan_name
                                FROM
                                    ".$db["master"].".employee_position
                                    INNER JOIN ".$db["master"].".company ON
                                        ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                    INNER JOIN ".$db["master"].".hrd_divisi ON
                                        ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                    INNER JOIN ".$db["master"].".hrd_departemen ON
                                        ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                    INNER JOIN ".$db["master"].".depo ON
                                        ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                    INNER JOIN ".$db["master"].".hrd_cabang ON
                                        ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                    INNER JOIN ".$db["master"].".jabatan ON
                                        ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                WHERE
                                    1
                            )as tbl_position ON
                                tbl_position.employee_id = ".$db["master"].".employee.employee_id
                            
                            LEFT JOIN
                            (
                                SELECT 
                                    tbl_fixed.employee_id,
                                    tbl_fixed.v_status,
                                    tbl_fixed.v_date
                                FROM
                                (
                                    SELECT
                                        uni.employee_id,
                                        uni.v_status,
                                        uni.v_date
                                    FROM
                                    (
                                        SELECT
                                            ".$db["master"].".employee_join.employee_id,
                                            'join' as v_status,
                                            ".$db["master"].".employee_join.join_date as v_date
                                        FROM
                                            ".$db["master"].".employee_join
                                        WHERE
                                            1
                                            
                                        UNION ALL
                                        
                                        SELECT
                                            ".$db["master"].".employee_resign.employee_id,
                                            'resign' as v_status,
                                            ".$db["master"].".employee_resign.resign_date as v_date
                                        FROM
                                            ".$db["master"].".employee_resign
                                        WHERE
                                            1
                                    ) as uni
                                    
                                    WHERE
                                        1
                                    GROUP BY
                                        uni.employee_id,
                                        uni.v_date
                                    ORDER BY
                                        uni.employee_id DESC,
                                        uni.v_date DESC 
                                )AS tbl_fixed
                                WHERE
                                    1
                                GROUP BY 
                                    tbl_fixed.employee_id
                            )as tbl_status ON
                                tbl_status.employee_id = ".$db["master"].".employee.employee_id
                                
                        WHERE
                            1
                        ORDER BY
                            ".$db["master"].".employee.employee_name ASC
                        LIMIT ".$s.", ".$jml_page." 
                ";
                }
                else 
                {
                    $where_company_id = "";
                    if($search_company_id!="")
                    {
                        $where_company_id = " AND tbl_position.company_id = '".$search_company_id."' ";    
                    }
                    
                    $where_cabang_id = "";
                    if($search_cabang_id!="")
                    {
                        $where_cabang_id = " AND tbl_position.cabang_id = '".$search_cabang_id."' ";    
                    }
                                         
                    //echo $search_status;
                    //echo "<hr>";
                    $where_status = "";
                    if($search_status!="")
                    {
                        $where_status = " AND tbl_status.v_status = '".$search_status."'  ";
                    }
                                            
                                            
                    unset($arr_keyword);
                    $arr_keyword[0] = "employee.employee_name";
                    $arr_keyword[1] = "employee.employee_nik";
                    $arr_keyword[2] = "employee.employee_code_hrd";
                    $arr_keyword[3] = "tbl_position.company_name";
                    $arr_keyword[4] = "tbl_position.company_initial";
                    $arr_keyword[5] = "tbl_position.cabang_name";
                    $arr_keyword[6] = "tbl_position.depo_name";
                    $arr_keyword[7] = "tbl_position.divisi_name";
                    $arr_keyword[8] = "tbl_position.departemen_name";
                    $arr_keyword[9] = "tbl_position.jabatan_name";
                    
                    $search_keyword = search_keyword($v_keyword, $arr_keyword);
                    $where = $search_keyword;
                    
                    $flag=0;
                    for($i=0; $i < strlen($keyWord); $i++)
                    {
                        if($keyWord[$i] == '\'') $flag++;
                        if($keyWord[$i] == '<') $flag++;
                        if($keyWord[$i] == '>') $flag++;
                    }
                    
                    if($flag==0)
                    {
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name,
                                        
                                        tbl_position.company_id,
                                        tbl_position.company_name,
                                        tbl_position.company_initial,
                                        tbl_position.cabang_id,
                                        tbl_position.cabang_name,
                                        tbl_position.depo_id,
                                        tbl_position.depo_name,                                                            
                                        tbl_position.divisi_id,
                                        tbl_position.divisi_name,
                                        tbl_position.departemen_id,
                                        tbl_position.departemen_name,
                                        tbl_position.jabatan_id,
                                        tbl_position.jabatan_name
                                    FROM 
                                        ".$db["master"].".employee
                                        LEFT JOIN
                                        (
                                            SELECT
                                                ".$db["master"].".employee_position.employee_id,
                                                
                                                ".$db["master"].".company.company_id,
                                                ".$db["master"].".company.company_name,
                                                ".$db["master"].".company.company_initial,
                                                
                                                ".$db["master"].".hrd_cabang.cabang_id,
                                                ".$db["master"].".hrd_cabang.cabang_name,
                                                
                                                ".$db["master"].".depo.depo_id,
                                                ".$db["master"].".depo.depo_name,
                                                
                                                ".$db["master"].".hrd_divisi.divisi_id,
                                                ".$db["master"].".hrd_divisi.divisi_name,
                                                
                                                ".$db["master"].".hrd_departemen.departemen_id,
                                                ".$db["master"].".hrd_departemen.departemen_name,
                                                
                                                ".$db["master"].".jabatan.jabatan_id,
                                                ".$db["master"].".jabatan.jabatan_name
                                            FROM
                                                ".$db["master"].".employee_position
                                                INNER JOIN ".$db["master"].".company ON
                                                    ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                                INNER JOIN ".$db["master"].".hrd_divisi ON
                                                    ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                                INNER JOIN ".$db["master"].".hrd_departemen ON
                                                    ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                                INNER JOIN ".$db["master"].".depo ON
                                                    ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                                INNER JOIN ".$db["master"].".hrd_cabang ON
                                                    ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                                INNER JOIN ".$db["master"].".jabatan ON
                                                    ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                            WHERE
                                                1
                                        )as tbl_position ON
                                            tbl_position.employee_id = ".$db["master"].".employee.employee_id
                                        
                                        LEFT JOIN
                                        (
                                            SELECT 
                                                tbl_fixed.employee_id,
                                                tbl_fixed.v_status,
                                                tbl_fixed.v_date
                                            FROM
                                            (
                                                SELECT
                                                    uni.employee_id,
                                                    uni.v_status,
                                                    uni.v_date
                                                FROM
                                                (
                                                    SELECT
                                                        ".$db["master"].".employee_join.employee_id,
                                                        'join' as v_status,
                                                        ".$db["master"].".employee_join.join_date as v_date
                                                    FROM
                                                        ".$db["master"].".employee_join
                                                    WHERE
                                                        1
                                                        
                                                    UNION ALL
                                                    
                                                    SELECT
                                                        ".$db["master"].".employee_resign.employee_id,
                                                        'resign' as v_status,
                                                        ".$db["master"].".employee_resign.resign_date as v_date
                                                    FROM
                                                        ".$db["master"].".employee_resign
                                                    WHERE
                                                        1
                                                ) as uni
                                                
                                                WHERE
                                                    1
                                                GROUP BY
                                                    uni.employee_id,
                                                    uni.v_date
                                                ORDER BY
                                                    uni.employee_id DESC,
                                                    uni.v_date DESC 
                                            )AS tbl_fixed
                                            WHERE
                                                1
                                            GROUP BY 
                                                tbl_fixed.employee_id
                                        )as tbl_status ON
                                            tbl_status.employee_id = ".$db["master"].".employee.employee_id
                                            
                                    WHERE
                                        1
                                        ".$where."
                                        ".$where_status."
                                        ".$where_company_id."
                                        ".$where_cabang_id."
                                    ORDER BY
                                        ".$db["master"].".employee.employee_name ASC
                               ";
                        $query = mysql_query($sql);
                        $max = ceil(mysql_num_rows($query)/$jml_page);
                        $s = $jml_page * $p;
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".employee.employee_id,
                                        ".$db["master"].".employee.employee_nik,
                                        ".$db["master"].".employee.employee_code_hrd,
                                        ".$db["master"].".employee.employee_name,
                                        
                                        tbl_position.company_id,
                                        tbl_position.company_name,
                                        tbl_position.company_initial,
                                        tbl_position.cabang_id,
                                        tbl_position.cabang_name,
                                        tbl_position.depo_id,
                                        tbl_position.depo_name,                                                            
                                        tbl_position.divisi_id,
                                        tbl_position.divisi_name,
                                        tbl_position.departemen_id,
                                        tbl_position.departemen_name,
                                        tbl_position.jabatan_id,
                                        tbl_position.jabatan_name
                                    FROM 
                                        ".$db["master"].".employee
                                        LEFT JOIN
                                        (
                                            SELECT
                                                ".$db["master"].".employee_position.employee_id,
                                                
                                                ".$db["master"].".company.company_id,
                                                ".$db["master"].".company.company_name,
                                                ".$db["master"].".company.company_initial,
                                                
                                                ".$db["master"].".hrd_cabang.cabang_id,
                                                ".$db["master"].".hrd_cabang.cabang_name,
                                                
                                                ".$db["master"].".depo.depo_id,
                                                ".$db["master"].".depo.depo_name,
                                                
                                                ".$db["master"].".hrd_divisi.divisi_id,
                                                ".$db["master"].".hrd_divisi.divisi_name,
                                                
                                                ".$db["master"].".hrd_departemen.departemen_id,
                                                ".$db["master"].".hrd_departemen.departemen_name,
                                                
                                                ".$db["master"].".jabatan.jabatan_id,
                                                ".$db["master"].".jabatan.jabatan_name
                                            FROM
                                                ".$db["master"].".employee_position
                                                INNER JOIN ".$db["master"].".company ON
                                                    ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id
                                                INNER JOIN ".$db["master"].".hrd_divisi ON
                                                    ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id
                                                INNER JOIN ".$db["master"].".hrd_departemen ON
                                                    ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id
                                                INNER JOIN ".$db["master"].".depo ON
                                                    ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id
                                                INNER JOIN ".$db["master"].".hrd_cabang ON
                                                    ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id
                                                INNER JOIN ".$db["master"].".jabatan ON
                                                    ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id
                                            WHERE
                                                1
                                        )as tbl_position ON
                                            tbl_position.employee_id = ".$db["master"].".employee.employee_id
                                        
                                        LEFT JOIN
                                        (
                                            SELECT 
                                                tbl_fixed.employee_id,
                                                tbl_fixed.v_status,
                                                tbl_fixed.v_date
                                            FROM
                                            (
                                                SELECT
                                                    uni.employee_id,
                                                    uni.v_status,
                                                    uni.v_date
                                                FROM
                                                (
                                                    SELECT
                                                        ".$db["master"].".employee_join.employee_id,
                                                        'join' as v_status,
                                                        ".$db["master"].".employee_join.join_date as v_date
                                                    FROM
                                                        ".$db["master"].".employee_join
                                                    WHERE
                                                        1
                                                        
                                                    UNION ALL
                                                    
                                                    SELECT
                                                        ".$db["master"].".employee_resign.employee_id,
                                                        'resign' as v_status,
                                                        ".$db["master"].".employee_resign.resign_date as v_date
                                                    FROM
                                                        ".$db["master"].".employee_resign
                                                    WHERE
                                                        1
                                                ) as uni
                                                
                                                WHERE
                                                    1
                                                GROUP BY
                                                    uni.employee_id,
                                                    uni.v_date
                                                ORDER BY
                                                    uni.employee_id DESC,
                                                    uni.v_date DESC 
                                            )AS tbl_fixed
                                            WHERE
                                                1
                                            GROUP BY 
                                                tbl_fixed.employee_id
                                        )as tbl_status ON
                                            tbl_status.employee_id = ".$db["master"].".employee.employee_id
                                            
                                    WHERE
                                        1
                                        ".$where."
                                        ".$where_status."
                                        ".$where_company_id."
                                        ".$where_cabang_id."
                                    ORDER BY
                                        ".$db["master"].".employee.employee_name ASC
                                    LIMIT ".$s.", ".$jml_page." 
                              ";
                    }
                    else
                    {
                        $msg = "Your input are not allowed!";
                    }
                } 
                
                $query = mysql_query($sql);
                        $sv = 0;
                        $i=1+($jml_page*$p);
                
                if(!$row = mysql_num_rows($query))
                {
                 echo "<tr>";
                 echo "<td align=\"center\" colspan=\"100%\">";
                 echo "No Data";
                 echo "</td>";
                 echo "</tr>";
                }
                else
                {
                    while ($row = mysql_fetch_array($query))
                    {
                        $sv++;
                        
                        $bgcolor = "";
                        if($i%2==0)
                        {
                            $bgcolor = "background:#E7E7E7;";
                        }
                        
                        $status_join = get_employee_status_join($row["employee_id"], date_now('d/m/Y'));
                        
                        $bgcolor_status = "";
                        if($status_join["v_status"]=="resign")
                        {
                            $bgcolor_status = "background:#FFCCFF;";
                        }
             			?>
		                    <tr onclick="get_choose('<?php echo $row["employee_id"]; ?>','<?php echo $row["employee_nik"]; ?>','<?php echo $row["employee_code_hrd"]; ?>','<?php echo $row["employee_name"]; ?>')" style="cursor:pointer; <?php echo $bgcolor; ?>" title="<?php echo $row[$title]; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $i; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["employee_nik"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["employee_code_hrd"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["employee_name"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["company_name"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["cabang_name"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["divisi_name"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>"><?php echo $row["jabatan_name"]; ?></td>
		                        <td style="<?php echo $bgcolor_status; ?>" align="center">
		                           	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Pilih" title="" onclick="get_choose('<?php echo $row["employee_id"]; ?>','<?php echo $row["employee_nik"]; ?>','<?php echo $row["employee_code_hrd"]; ?>','<?php echo $row["employee_name"]; ?>')" value="Pilih" >
										<i class="entypo-check"></i>
									</button>
		                        </td>
		                    </tr>
             			<?php
                        $i = $i+1; 
                    }
                }
             	?>
				
				</tbody>
			</table> 
			
			<?php include("paging.php"); ?>
		
		</div>
		
		</form>
		
	</div>

</div>    
</body>
</html>
         