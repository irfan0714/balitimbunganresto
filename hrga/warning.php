<?php 
	if($msg)
	{
		if($class_warning=="success")
		{
			$echo_class="alert-success";
		}
		else if($class_warning=="error")
		{
			$echo_class="alert-danger";
		}
		else if($class_warning=="info")
		{
			$echo_class="alert-info";
		}
?>
	<div class="alert <?php echo $echo_class; ?>"><?php echo $msg; ?></div>
	
<?php 
	}
?>    